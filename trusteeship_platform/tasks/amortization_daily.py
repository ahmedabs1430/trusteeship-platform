from datetime import datetime

import frappe
import pytz

from trusteeship_platform.custom_methods import get_doc_list


def generate_journal_entry():
    timezone = pytz.timezone(frappe.defaults.get_defaults().get("time_zone"))
    today = datetime.now(tz=timezone).date()
    amortization_doc = get_doc_list(
        "Canopi Amortization",
        fields=('["*"]'),
        filters=[
            ["docstatus", "=", 1],
            ["amount", ">", 0],
            ["start_date", "<=", today],
            ["end_date", ">=", today],
        ],
    )

    for doc in amortization_doc:
        sdoc = frappe.get_doc("Sales Invoice", doc.sales_invoice)
        cost_center = sdoc.cost_center
        new_doc = frappe.new_doc("Journal Entry")
        new_doc.voucher_type = "Deferred Revenue"
        new_doc.company = doc.company
        new_doc.posting_date = today
        new_doc.cnp_cost_center = cost_center
        new_doc.append(
            "accounts",
            {
                "account": doc.account_to_be_debited,
                "debit_in_account_currency": doc.amount,
                "canopi_reference_type": "Canopi Amortization",
                "canopi_reference_name": doc.name,
                "cost_center": cost_center,
            },
        )
        new_doc.append(
            "accounts",
            {
                "account": doc.account_to_be_credited,
                "credit_in_account_currency": doc.amount,
                "canopi_reference_type": "Canopi Amortization",
                "canopi_reference_name": doc.name,
                "cost_center": cost_center,
            },
        )
        new_doc.docstatus = 1
        new_doc.insert()
        frappe.db.commit()
