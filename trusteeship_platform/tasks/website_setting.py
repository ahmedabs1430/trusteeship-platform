def update_website_context(context):
    context.update(
        dict(
            splash_image="/assets/trusteeship_platform/images/logo.jpeg",
        ),
    )
    return context
