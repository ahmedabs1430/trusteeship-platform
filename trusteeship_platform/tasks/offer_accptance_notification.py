from datetime import date, timedelta

import frappe
from frappe.utils.data import getdate

from trusteeship_platform.custom_methods import get_doc_list

from trusteeship_platform.overrides.quotation import (  # noqa: 501 isort:skip
    offer_acceptance_notification,
)


def offer_letter_acceptance_notification():
    quotation_doc = get_doc_list(
        "Quotation", fields=('["name"]'), filters=[["docstatus", "=", 1]]
    )
    for quot in quotation_doc:
        quot_doc = frappe.get_doc("Quotation", quot.name)
        if quot_doc.cnp_offer_acceptence_date:
            offer_acceptance_due_date = getdate(
                quot_doc.cnp_offer_acceptence_date
            ) + timedelta(days=2)
            if offer_acceptance_due_date == date.today():
                offer_acceptance_notification(
                    "Quotation",
                    quot_doc.name,
                    quot_doc.cnp_offer_acceptence_date,
                )
