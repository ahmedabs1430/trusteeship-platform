import json
from datetime import datetime, timedelta

import frappe
from frappe.utils.data import today

from trusteeship_platform.custom_methods import get_single_doc


def insert_notification():
    interval = get_single_doc("Canopi Notification Interval")
    post_executions = frappe.db.get_list(
        "Canopi Post Execution Checklist", fields=["*"]
    )
    for post_execution in post_executions:
        if post_execution.type_of_security_details:
            details = json.loads(post_execution.type_of_security_details)
            for asset in details:
                for index, asset_details in enumerate(details[asset]):
                    if asset_details.get("security_doc_post"):
                        security_post_execution = frappe.get_doc(
                            "Canopi Security Documents Post Execution",
                            asset_details.get("security_doc_post"),
                        )
                        if security_post_execution.execution_date:
                            if asset_details.get("document_code"):
                                for (
                                    canopi_notification_interval
                                ) in interval.canopi_notification_interval:
                                    if (
                                        canopi_notification_interval.document_name  # noqa: E501
                                        == asset_details.get("document_code")
                                    ):

                                        start_date = str(
                                            security_post_execution.execution_date  # noqa: E501
                                            - timedelta(
                                                canopi_notification_interval.period  # noqa: E501
                                            )
                                        )
                                        datetime_object = datetime.strptime(
                                            start_date, "%Y-%m-%d"
                                        ).date()
                                        for i in range(
                                            canopi_notification_interval.period
                                        ):
                                            if i == 0:
                                                check_to_insert_notification(
                                                    datetime_object,
                                                    asset_details,
                                                    post_execution,
                                                    canopi_notification_interval,  # noqa: E501
                                                )
                                            else:
                                                datetime_object += timedelta(
                                                    canopi_notification_interval.frequency  # noqa: E501
                                                )
                                                check_to_insert_notification(
                                                    datetime_object,
                                                    asset_details,
                                                    post_execution,
                                                    canopi_notification_interval,  # noqa: E501
                                                )


def check_to_insert_notification(
    datetime_object,
    asset_details,
    post_execution,
    canopi_notification_interval,  # noqa: E501
):
    if today() == str(datetime_object):
        if "file_name" in asset_details:
            if len(asset_details.get("file_name")) != 0:
                notification(post_execution, canopi_notification_interval)


def notification(post_execution, canopi_notification_interval):
    siteurl = (
        frappe.utils.get_site_url(frappe.local.site)
        + "/app/canopi-post-execution-checklist/"
        + post_execution.name
    )
    notification = frappe.new_doc("Notification Log")
    notification.subject = canopi_notification_interval.subject
    notification.email_content = (
        canopi_notification_interval.message
        + " <a href="
        + siteurl
        + ">"
        + siteurl
        + "</a>"
    )
    notification.for_user = post_execution.owner
    notification.type = "Alert"
    notification.insert()
    assigned_to = frappe.db.get_list(
        "ToDo", filters={"reference_name": post_execution.name}, fields=["*"]
    )
    for assign in assigned_to:
        notification = frappe.new_doc("Notification Log")
        notification.subject = canopi_notification_interval.subject
        notification.email_content = (
            canopi_notification_interval.message
            + " <a href="
            + siteurl
            + ">"
            + siteurl
            + "</a>"
        )
        notification.for_user = assign.allocated_to
        notification.type = "Alert"
        notification.insert()
    frappe.db.commit()
