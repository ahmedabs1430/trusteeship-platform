import datetime

import frappe

from trusteeship_platform.custom_methods import get_doc_list


def generate_sales_invoice():
    quotation_doc = get_doc_list(
        "Quotation", fields=('["name"]'), filters=[["docstatus", "=", 1]]
    )
    for quot in quotation_doc:
        quot_doc = frappe.get_doc("Quotation", quot.name)
        next_fee_date = None
        for item in quot_doc.items:
            if item.cnp_type_of_fee == "Annual Fee":
                next_fee_date = item.cnp_next_fee_date
        if datetime.date.today() == next_fee_date:
            sales_doc_list = get_doc_list(
                "Sales Invoice",
                fields=('["name"]'),
                filters=[["cnp_from_quotation", "=", quot_doc.name]],
            )
            months = 99999999
            income_account = ""
            cost_center = ""
            no_of_sales_invoice = 0
            for doc in sales_doc_list:
                sales_doc = frappe.get_doc("Sales Invoice", doc.name)
                if sales_doc.cnp_no_of_sales_invoice > no_of_sales_invoice:
                    no_of_sales_invoice = sales_doc.cnp_no_of_sales_invoice
                if months > sales_doc.cnp_months_pending:
                    months = sales_doc.cnp_months_pending
            for doc in sales_doc_list:
                sales_doc = frappe.get_doc("Sales Invoice", doc.name)

                for r in sales_doc.items:
                    # if(r.cnp_type_of_fee=="Annual Fee"):
                    income_account = r.income_account
                    cost_center = r.cost_center
                sales_new_doc = frappe.new_doc("Sales Invoice")
                sales_new_doc.naming_series = "SINV-.YY.-"
                sales_new_doc.cnp_from_quotation = quot_doc.name
                today = datetime.date.today()
                sales_new_doc.posting_date = today
                sales_new_doc.due_date = today + datetime.timedelta(  # noqa: E501
                    days=15,
                )  # (yyyy, mm, dd)
                sales_new_doc.customer = sales_doc.customer
                sales_new_doc.currency = sales_doc.currency
                sales_new_doc.conversion_rate = sales_doc.conversion_rate
                sales_new_doc.selling_price_list = sales_doc.selling_price_list
                sales_new_doc.price_list_currency = (
                    sales_doc.price_list_currency
                )  # noqa: E501
                sales_new_doc.plc_conversion_rate = (
                    sales_doc.plc_conversion_rate
                )  # noqa: E501
                sales_new_doc.company = sales_doc.company
                sales_new_doc.debit_to = sales_doc.debit_to

                sales_new_doc.taxes_and_charges = sales_doc.taxes_and_charges
                sales_new_doc.taxes = sales_doc.taxes
                sales_new_doc.cnp_no_of_sales_invoice = no_of_sales_invoice + 1

                if sales_doc.cnp_no_of_sales_invoice == no_of_sales_invoice:
                    sales_new_doc.items = []
                    fee = 0
                    for row in quot_doc.items:
                        if row.cnp_type_of_fee == "Initial Fee":
                            pass
                        elif row.cnp_type_of_fee == "Annual Fee":
                            if months > 12 and row.cnp_frequency == "Y":
                                fee = row.rate
                                sales_new_doc.cnp_months_pending = months - 12
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            elif 0 < months < 12 and row.cnp_frequency == "Y":
                                fee = row.rate / 12 * months
                                sales_new_doc.cnp_months_pending = months - 12
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            if months > 6 and row.cnp_frequency == "H":
                                fee = row.rate / 6
                                sales_new_doc.cnp_months_pending = months - 6
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            elif 0 < months < 6 and row.cnp_frequency == "H":
                                fee = row.rate / 12 * months
                                sales_new_doc.cnp_months_pending = months - 6
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            if months > 3 and row.cnp_frequency == "Q":
                                fee = row.rate / 3
                                sales_new_doc.cnp_months_pending = months - 3
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            elif 0 < months < 3 and row.cnp_frequency == "Q":
                                fee = row.rate / 12 * months
                                sales_new_doc.cnp_months_pending = months - 3
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            if months > 1 and row.cnp_frequency == "M":
                                fee = row.rate / 12
                                sales_new_doc.cnp_months_pending = months - 1
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )
                            elif months == 1 and row.cnp_frequency == "M":
                                fee = row.rate / 12 * months
                                sales_new_doc.cnp_months_pending = months - 1
                                insert_document(
                                    sales_new_doc,
                                    row,
                                    fee,
                                    income_account,
                                    cost_center,
                                )


def insert_document(
    sales_new_doc,
    row,
    fee,
    income_account,
    cost_center,
):
    sales_new_doc.append(
        "items",
        {
            "qty": 1,
            "rate": fee,
            "item_code": row.item_code,
            "uom": "Nos",
            "conversion_factor": 1,
            "amount": fee,
            "income_account": income_account,
            "cnp_type_of_fee": row.cnp_type_of_fee,
            "cost_center": cost_center,
        },
    )
    sales_new_doc.insert()
    frappe.db.commit()
