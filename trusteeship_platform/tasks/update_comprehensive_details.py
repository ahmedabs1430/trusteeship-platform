import frappe
import requests

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    fetch_probe_42_details,
    process_response,
)


# trusteeship_platform.tasks.update_comprehensive_details.update_company_details
def update_company_details():
    try:

        filters = [
            ["CPRB Company Details", "has_comprehensive_details", "=", 0],
            ["CPRB Company Details", "request_id", "is", "set"],
        ]
        company_list = frappe.db.get_list(
            "CPRB Company Details", filters, ["name"]
        )  # noqa
        frappe.logger("utils").exception(company_list)
        for doc in company_list:
            cprb_doc = frappe.get_doc("CPRB Company Details", doc.get("name"))
            cin_or_pan = doc.get("name")
            lead_doc = frappe.get_doc("Lead", {"cin_number": cin_or_pan})
            is_pan = (
                lead_doc.get("cnp_type") == "Individual"
                or lead_doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
            )
            cnp_type = "llp" if doc.get("cnp_type") == "LLP" else "companies"  # noqa
            response = _show_update_status(
                cin_or_pan, cprb_doc.request_id, is_pan, cnp_type
            )  # noqa
            status = response.get("data", {}).get("status")
            if status != "FULFILLED":
                return doc

            comprehensive_details = fetch_probe_42_details(
                cin_or_pan, is_pan, cnp_type
            )  # noqa
            if comprehensive_details.get("data"):
                company_details = comprehensive_details
                cprb_doc.has_comprehensive_details = 1
                cprb_doc.company_name = (
                    company_details.get("data", {})
                    .get("company", {})
                    .get("legal_name", "")
                )
                cprb_doc.payload = company_details
                cprb_doc.save()
            return doc
    except Exception as e:
        frappe.logger("utils").exception(e)


def _show_update_status(cin_or_pan, request_id, is_pan, cnp_type):

    """
    Fetch Base Details For a CIN
    """
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    url = f"""{settings_doc.probe_42_url}/{cnp_type}/{cin_or_pan}/get-update-status?request_id={request_id}"""  # noqa: 501

    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.get(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    return response
