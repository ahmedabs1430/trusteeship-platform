import frappe


def execute():
    if not frappe.db.exists("Party Type", "CP2P Merchant"):
        doc = frappe.get_doc(
            {
                "doctype": "Party Type",
                "party_type": "CP2P Merchant",
                "account_type": "Payable",
            }
        )
        doc.insert()
