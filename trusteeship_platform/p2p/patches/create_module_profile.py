import frappe
from frappe.config import get_modules_from_all_apps


def execute():
    if not frappe.db.exists("Module Profile", {"name": "CP2P"}):
        block_modules = [
            {
                "docstatus": 1,
                "doctype": "Block Module",
                "parent": "CP2P Profile",
                "parentfield": "block_modules",
                "parenttype": "Module Profile",
                "module": module.get("module_name"),
            }
            for module in get_modules_from_all_apps()
            if module.get("module_name") != "P2P"
        ]
        module_profile = frappe.get_doc(
            {
                "name": "CP2P Profile",
                "doctype": "Module Profile",
                "module_profile_name": "CP2P",
                "block_modules": block_modules,
            }
        )
        module_profile.insert(ignore_permissions=True)
