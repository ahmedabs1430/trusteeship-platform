import frappe


def execute():
    #  Create a new role
    role_name = "P2P Admin"
    if not frappe.db.exists("Role", role_name):
        role = frappe.get_doc(
            {
                "doctype": "Role",
                "role_name": role_name,
                "desk_access": 1,  # Enable desk access for the role
                # Add other fields or customizations as needed
            }
        )
        role.insert(ignore_permissions=True)
