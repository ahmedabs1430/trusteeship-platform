import frappe
from frappe import _


def execute():
    # Create a new Dashboard document
    create_documents()
    create_dashboard()


def create_dashboard():
    try:
        if frappe.db.exists("Dashboard", "P2P Admin Dashboard"):
            return
        dashboard = frappe.get_doc(
            {
                "doctype": "Dashboard",
                "dashboard_name": "P2P Admin Dashboard",
                "is_standard": 0,
                "is_default": 0,
                "owner": "Administrator",
                "modified_by": "Administrator",
                "cards": [
                    {
                        "doctype": "Number Card Link",
                        "card": "Payouts (This Month)",
                        "idx": 1,
                    },
                    {
                        "doctype": "Number Card Link",
                        "card": "Transactions (This Month)",
                        "idx": 2,
                    },
                    {
                        "doctype": "Number Card Link",
                        "card": "Authorisations (This Month)",
                        "idx": 3,
                    },
                    {
                        "doctype": "Number Card Link",
                        "card": "Errors (This Month)",
                        "idx": 4,
                    },
                    {
                        "doctype": "Number Card Link",
                        "card": "Contacts (This Month)",
                        "idx": 5,
                    },
                    {
                        "doctype": "Number Card Link",
                        "card": "Fund Accounts (This Month)",
                        "idx": 6,
                    },
                ],
                "charts": [
                    {
                        "doctype": "Dashboard Chart Link",
                        "chart": "Incoming Payouts",
                        "width": "Full",
                        "idx": 1,
                    },
                    {
                        "doctype": "Dashboard Chart Link",
                        "chart": "Transactions by MID",  # noqa
                        "width": "Half",
                        "idx": 2,
                    },
                ],
            }
        )

        # Save the Dashboard document
        dashboard.insert(ignore_permissions=True)
        frappe.db.commit()
    except Exception as e:
        return e


def create_dashboard_chart(chart_name, document_type, group_by_based_on):
    if frappe.db.exists("Dashboard Chart", chart_name):
        print(f"Dashboard Chart '{chart_name}' already exists.")  # noqa
        return

    doc = frappe.new_doc("Dashboard Chart")
    doc.name = chart_name
    doc.docstatus = 0
    doc.idx = 0
    doc.is_standard = 0
    doc.chart_name = chart_name
    doc.chart_type = "Group By"
    doc.use_report_chart = 0
    doc.source = ""
    doc.document_type = document_type
    doc.parent_document_type = ""
    doc.based_on = ""
    doc.value_based_on = ""
    doc.group_by_type = "Count"
    doc.group_by_based_on = group_by_based_on
    doc.number_of_groups = 0
    doc.is_public = 0
    doc.timespan = "Last Month"
    doc.time_interval = "Daily"
    doc.timeseries = 0
    doc.type = "Donut"
    doc.filters_json = "[]"
    doc.dynamic_filters_json = "[]"
    doc.doctype = "Dashboard Chart"
    doc.roles = []
    doc.y_axis = []

    try:
        doc.insert(ignore_permissions=True)
        frappe.db.commit()
        print(f"Dashboard Chart '{chart_name}' created successfully.")  # noqa
    except Exception as e:
        frappe.log_error(
            _("Error creating Dashboard Chart '{0}': {1}").format(
                chart_name, str(e)
            )  # noqa
        )


def create_number_card(doc_name, label, document_type, filters_json):
    if frappe.db.exists("Number Card", doc_name):
        print(f"Number Card '{doc_name}' already exists.")  # noqa
        return

    doc = frappe.new_doc("Number Card")
    doc.name = doc_name
    doc.owner = "Administrator"
    doc.creation = "2023-06-29 19:56:01.488476"
    doc.modified = "2023-06-29 19:56:01.488476"
    doc.modified_by = "Administrator"
    doc.docstatus = 0
    doc.idx = 0
    doc.is_standard = 0
    doc.label = label
    doc.type = "Document Type"
    doc.function = "Count"
    doc.aggregate_function_based_on = ""
    doc.document_type = document_type
    doc.parent_document_type = ""
    doc.report_function = "Sum"
    doc.is_public = 0
    doc.show_percentage_stats = 1
    doc.stats_time_interval = "Daily"
    doc.filters_json = filters_json
    doc.dynamic_filters_json = "[]"
    doc.doctype = "Number Card"

    try:
        doc.insert(ignore_permissions=True)
        frappe.db.commit()
        print(f"Number Card '{doc_name}' created successfully.")  # noqa
    except Exception as e:
        frappe.log_error(
            _("Error creating Number Card '{0}': {1}").format(doc_name, str(e))
        )


def create_documents():
    create_dashboard_chart(
        "Incoming Payouts", "CP2P Payout Pending Event", "merchant"
    )  # noqa
    create_dashboard_chart(
        "Transactions by MID", "CP2P Payout Pending Event", "merchant"
    )

    create_number_card(
        "Payouts (This Month)",
        "Payouts (This Month)",
        "CP2P Payout Pending Event",
        '[["CP2P Payout Pending Event","creation","Timespan","this month",false]]',  # noqa
    )
    create_number_card(
        "Transactions (This Month)",
        "Transactions (This Month)",
        "CP2P Transactions",
        '[["CP2P Transactions","creation","Timespan","this month",false]]',
    )
    create_number_card(
        "Authorisations (This Month)",
        "Authorisations (This Month)",
        "CP2P Authorized Payouts",
        '[["CP2P Authorized Payouts","creation","Timespan","this month",false]]',  # noqa
    )
    create_number_card(
        "Errors (This Month)",
        "Errors (This Month)",
        "CP2P Error Log",
        '[["CP2P Error Log","creation","Timespan","this month",false]]',
    )
    create_number_card(
        "Contacts (This Month)",
        "Contacts (This Month)",
        "CP2P Contacts",
        '[["CP2P Contacts","creation","Timespan","this month",false]]',
    )
    create_number_card(
        "Fund Accounts (This Month)",
        "Fund Accounts (This Month)",
        "CP2P Fund Accounts",
        '[["CP2P Fund Accounts","creation","Timespan","this month",false]]',
    )
