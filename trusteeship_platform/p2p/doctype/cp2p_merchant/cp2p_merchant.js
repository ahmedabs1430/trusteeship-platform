// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

frappe.ui.form.on('CP2P Merchant', {
  refresh: function (frm) {
    const webhook_secret = frm.doc.webhook_secret;
    if (webhook_secret === null || webhook_secret === '') {
      frm.add_custom_button(__('Generate Webhook Secret'), function () {
        frm.set_value('webhook_secret', generateRandomString(20));
      });
    } else {
      frm.add_custom_button(__('Regenerate Webhook Secret'), function () {
        frm.set_value('webhook_secret', generateRandomString(20));
      });
    }
  },
});

function generateRandomString(length) {
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    result += characters.charAt(randomIndex);
  }

  return result;
}
