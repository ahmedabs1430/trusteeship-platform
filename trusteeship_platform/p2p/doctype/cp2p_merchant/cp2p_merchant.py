# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt
import json

import frappe
from frappe import generate_hash
from frappe.desk.doctype.workspace.workspace import save_page
from frappe.model.document import Document


class CP2PMerchant(Document):
    def before_save(self):
        if not frappe.db.exists("Workspace", {"title": "CP2P"}):
            workspace = frappe.get_doc(
                {
                    "doctype": "Workspace",
                    "title": "CP2P",
                    "label": "CP2P",
                    "module": "P2P",
                    "public": 1,
                }
            )
            workspace.insert(ignore_permissions=True)

        name = self.merchant_name
        if not frappe.db.exists("Workspace", {"title": name}):
            workspace = frappe.get_doc(
                {
                    "doctype": "Workspace",
                    "title": name,
                    "parent_page": "CP2P",
                    "label": name,
                    "module": "P2P",
                    "public": 1,
                }
            )
            workspace.insert(ignore_permissions=True)

            name = self.merchant_name
            merchant_id = self.merchant_id
            stats_filter = {"merchant_id": ["=", f"{merchant_id}"]}
            new_widgets = {
                "quick_list": [
                    {
                        "document_type": "CP2P Transactions",
                        "label": "Transaction",
                        "quick_list_filter": json.dumps(stats_filter),
                    },
                    {
                        "document_type": "CP2P Payout Pending Event",
                        "label": "Payouts",
                        "quick_list_filter": json.dumps(stats_filter),
                    },
                    {
                        "document_type": "CP2P Contacts",
                        "label": "Contacts",
                        "quick_list_filter": json.dumps(stats_filter),
                    },
                    {
                        "document_type": "CP2P Fund Accounts",
                        "label": "Fund Accounts",
                        "quick_list_filter": json.dumps(stats_filter),
                    },
                ]
            }

            doc = frappe.get_doc("Workspace", name)
            blocks = json.loads(doc.content)
            site_url = frappe.utils.get_url()
            merchant_url = f"{site_url}/app/cp2p-merchant/{self.name}"
            blocks += [
                {
                    "id": generate_hash(length=10),
                    "type": "header",
                    "data": {
                        "text": f'<span class="h2"><b>{name}</b></span>',
                        "col": 12,
                    },
                },
                {
                    "id": generate_hash(length=10),
                    "type": "quick_list",
                    "data": {"quick_list_name": "Transaction", "col": 6},
                },
                {
                    "id": generate_hash(length=10),
                    "type": "quick_list",
                    "data": {"quick_list_name": "Payouts", "col": 6},
                },
                {
                    "id": generate_hash(length=10),
                    "type": "quick_list",
                    "data": {"quick_list_name": "Contacts", "col": 6},
                },
                {
                    "id": generate_hash(length=10),
                    "type": "quick_list",
                    "data": {"quick_list_name": "Fund Accounts", "col": 6},
                },
                {
                    "id": generate_hash(length=10),
                    "type": "header",
                    "data": {
                        "text": '<span class="h4"><b>Shortcuts</b></span>',
                        "col": 12,
                    },
                },
                {
                    "id": generate_hash(length=10),
                    "type": "header",
                    "data": {
                        "text": f"""<span class="h5">
                        <a href='{merchant_url}' style="text-decoration:none;">
                                Merchant Settings
                        </a>
                        </span>""",
                        "col": 12,
                    },
                },
            ]

            save_page(name, 1, json.dumps(new_widgets), json.dumps(blocks))

    def on_trash(self):
        name = self.merchant_name
        if frappe.db.exists("Workspace", {"title": name}):
            frappe.delete_doc("Workspace", name, ignore_permissions=True)
