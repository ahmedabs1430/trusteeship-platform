app_name = "trusteeship_platform"
app_title = "Trusteeship Platform"
app_publisher = "Castlecraft"
app_description = "Trusteeship Platform"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "support@castlecraft.in"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/trusteeship_platform/css/trusteeship_platform.css"
# app_include_js = "/assets/trusteeship_platform/js/trusteeship_platform.js"
app_include_js = "trusteeship.bundle.js"
app_include_css = "trusteeship.bundle.css"

# include js, css files in header of web template
# web_include_css = "/assets/trusteeship_platform/css/trusteeship_platform.css"
# web_include_js = "/assets/trusteeship_platform/js/trusteeship_platform.js"
web_include_css = "trusteeship.bundle.css"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "trusteeship_platform/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
    "Lead": "public/js/lead.js",
    "Opportunity": "public/js/opportunity.js",
    "Customer": "public/js/customer.js",
    "Address": "public/js/address.js",
    "Item": "public/js/item.js",
    "Quotation": "public/js/quotation.js",
    "Contact": "public/js/contact.js",
    "Company": "public/js/company.js",
    "Sales Invoice": "public/js/sales_invoice.js",
    "Purchase Invoice": "public/js/purchase_invoice.js",
    "Note": "public/js/note.js",
    "Journal Entry": "public/js/journal_entry.js",
    "Bank Account": "public/js/bank_account.js",
    "Bank": "public/js/bank.js",
    "Payment Entry": "public/js/payment_entry.js",
    "Fiscal Year": "public/js/fiscal_year.js",
    "Account": "public/js/account.js",
    "User": "public/js/user.js",
    "Cost Center": "public/js/cost_center.js",
}
doctype_list_js = {
    "Lead": "public/js/lead_list.js",
    "Quotation": "public/js/quotation_list.js",
    "Opportunity": "public/js/opportunity_list.js",
}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"
# update_website_context = 'trusteeship_platform.tasks.website_setting.update_website_context'  # noqa
# website user home page (by Role)
# role_home_page = {
# "Role": "home_page"
# }

# Include custom functions in jinja template
# jenv = {
#     "methods": [
#         "str_to_json:trusteeship_platform.custom_methods.str_to_json"
#     ]
# }

jinja = {
    "methods": [
        "trusteeship_platform.custom_methods.str_to_json",
        "trusteeship_platform.custom_methods.get_total_expression_in_letters",
    ]
}

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "trusteeship_platform.install.before_install"
# after_install = "trusteeship_platform.install.after_install"
after_install = "trusteeship_platform.install.after_install"

setup_wizard_complete = "trusteeship_platform.setup.setup_wizard_complete"
# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "trusteeship_platform.notifications.get_notification_config"  # noqa

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

override_doctype_class = {
    # "ToDo": "custom_app.overrides.CustomToDo"
    "Contact": "trusteeship_platform.public.py.Contact.Contact",
    "Opportunity": "trusteeship_platform.public.py.Opportunity.Opportunity",
    "Item Price": "trusteeship_platform.public.py.ItemPrice.ItemPrice",
    "Industry Type": "trusteeship_platform.public.py.IndustryType.IndustryType",  # noqa
    "Note": "trusteeship_platform.public.py.note.note",
    "Account": "trusteeship_platform.public.py.Account.Account",
    "Lead": "trusteeship_platform.public.py.lead.lead",
    "Payment Entry": "trusteeship_platform.public.py.PaymentEntry.Payment",
    "Purchase Invoice": "trusteeship_platform.public.py.PurchaseInvoice.Purchase",  # noqa
    "Journal Entry": "trusteeship_platform.public.py.JournalEntry.Journal",
    "Cost Center": "trusteeship_platform.public.py.CostCenter.costCenter",
}

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
    "Address": {
        "validate": "trusteeship_platform.overrides.address.validate",
    },
    "Sales Invoice": {
        "before_save": "trusteeship_platform.overrides.sales_invoice.before_save",  # noqa
        "validate": "trusteeship_platform.overrides.sales_invoice.validate",  # noqa
        "on_submit": "trusteeship_platform.overrides.sales_invoice.on_submit",  # noqa
        "on_update": "trusteeship_platform.overrides.sales_invoice.on_update",  # noqa
        "before_insert": "trusteeship_platform.overrides.sales_invoice.before_insert",  # noqa
        "after_insert": "trusteeship_platform.overrides.sales_invoice.after_insert",  # noqa
        "on_trash": "trusteeship_platform.overrides.sales_invoice.on_trash",  # noqa
    },
    "Quotation": {
        "before_save": "trusteeship_platform.overrides.quotation.before_save",  # noqa
        "on_cancel": "trusteeship_platform.overrides.quotation.on_cancel",  # noqa
        "after_insert": "trusteeship_platform.overrides.quotation.after_insert",  # noqa
        "validate": "trusteeship_platform.overrides.quotation.validate",  # noqa
        "on_submit": "trusteeship_platform.overrides.quotation.on_submit",  # noqa
        "on_update_after_submit": "trusteeship_platform.overrides.quotation.on_update_after_submit",  # noqa
        "before_insert": "trusteeship_platform.overrides.quotation.before_insert",  # noqa
    },
    # 	"*": {
    # 		"on_update": "method",
    # 		"on_cancel": "method",
    # 		"on_trash": "method"
    # }
}

# Scheduled Tasks
# ---------------

scheduler_events = {
    # "all": [
    #     "trusteeship_platform.tasks.sales_invoice_daily.generate_sales_invoice"
    # ],
    "cron": {
        # Daily but offset by 4 hours
        "0 */4 * * *": [
            "trusteeship_platform.tasks.update_comprehensive_details.update_company_details"  # noqa
        ],
    },
    "daily": [
        "trusteeship_platform.tasks.notification_for_post.insert_notification",
        "trusteeship_platform.tasks.sales_invoice_daily.generate_sales_invoice",  # noqa
        "trusteeship_platform.tasks.amortization_daily.generate_journal_entry",
        "trusteeship_platform.tasks.offer_accptance_notification.offer_letter_acceptance_notification",  # noqa
    ],
    "hourly": ["frappe.sessions.clear"],
    # 	"weekly": [
    # 		"trusteeship_platform.tasks.weekly"
    # 	]
    # 	"monthly": [
    # 		"trusteeship_platform.tasks.monthly"
    # 	]
}

# Testing
# -------

# before_tests = "trusteeship_platform.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "trusteeship_platform.event.get_events"   # noqa
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
override_doctype_dashboards = {
    "Opportunity": "trusteeship_platform.overrides.opportunity_dashboard.get_data",  # noqa
    "Lead": "trusteeship_platform.overrides.lead_dashboard.get_data",
    "Quotation": "trusteeship_platform.overrides.quotation_dashboard.get_data",
}
# override_doctype_dashboards = {
# 	"Task": "trusteeship_platform.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]

default_mail_footer = """ """
# User Data Protection
# --------------------

user_data_fields = [
    {
        "doctype": "{doctype_1}",
        "filter_by": "{filter_by}",
        "redact_fields": ["{field_1}", "{field_2}"],
        "partial": 1,
    },
    {
        "doctype": "{doctype_2}",
        "filter_by": "{filter_by}",
        "partial": 1,
    },
    {
        "doctype": "{doctype_3}",
        "strict": False,
    },
    {"doctype": "{doctype_4}"},
]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"trusteeship_platform.auth.validate"
# ]


fixtures = [
    {
        "dt": "Custom Field",
        "filters": [
            [
                "name",
                "in",
                [
                    "Lead-cnp_servicing_rm",
                    "Lead-cin_number",
                    "Lead-cnp_source_others",
                    "Lead-cnp_product",
                    "Lead-cnp_address_type_others",
                    "Opportunity-cnp_registered_office",
                    "Opportunity-cnp_promoters",
                    "Opportunity-cnp_reg_year",
                    "Opportunity-cnp_acceptance_date",
                    "Opportunity-cnp_security_section_break",
                    "Opportunity-cnp_security_type",
                    "Opportunity-cnp_quotation_section_break",
                    "Opportunity-cnp_terms_section_break",
                    "Opportunity-cnp_additional",
                    "Opportunity-cnp_additional_terms_and_condition",
                    "Opportunity-cnp_terms_and_condition",
                    "Opportunity-cnp_acceptance_details",
                    "Opportunity-cnp_cr_rating",
                    "Opportunity-cnp_cr_rating_section",
                    "Opportunity-cnp_extg_relation_section",
                    "Opportunity-cnp_extg_relation",
                    "Opportunity-cnp_fininfo_section",
                    "Opportunity-cnp_fininfo",
                    "Opportunity-cnp_acceptance_details_section",
                    "Opportunity-cnp_tenor_years",
                    "Opportunity-cnp_tenor_months",
                    "Opportunity-cnp_tenor_days",
                    "Opportunity-cnp_issue_size_in_words",
                    "Opportunity-cnp_facility_amt_in_words",
                    "Opportunity-cnp_pricing_policy_html",
                    "Opportunity-cnp_pricing_policy_section",
                    "Lead-cnp_is_data_fetched",
                    "Opportunity-cin_number",
                    "Opportunity-cnp_is_probe42_data_fetched",
                    "Customer-cin_number",
                    "Opportunity-cnp_product",
                    "Opportunity-column_break_40",
                    "Opportunity-column_break_41",
                    "Opportunity-cnp_director_and_signatory_section",
                    "Opportunity-column_break_47",
                    "Opportunity-cnp_director_details",
                    "Opportunity-cnp_signatory_deatils",
                    "Opportunity-cnp_director_shareholdings",
                    "Opportunity-cnp_authorized_signatories",
                    "Opportunity-cnp_tenor_section",
                    "Opportunity-cnp_facility_amt",
                    "Opportunity-cnp_issue_size",
                    "Opportunity-cnp_cr_rating_html",
                    "Opportunity-cnp_fininfo_html",
                    "Market Segment-cnp_reference",
                    "Market Segment-cnp_links",
                    "Industry Type-cnp_reference",
                    "Industry Type-cnp_links",
                    "Lead-cnp_market_industry_html",
                    "Lead-market_and_industry_segment_section",
                    "Customer-cnp_authorized_signatories",
                    "Customer-cnp_signatory_details",
                    "Customer-column_break_41",
                    "Customer-cnp_director_shareholdings",
                    "Customer-cnp_director_details",
                    "Customer-cnp_director_and_signatory",
                    "Customer-cnp_is_probe42_data_fetched",
                    "Lead-cnp_priority",
                    "Address-cnp_nature_of_business",
                    "Address-cnp_registration_date",
                    "Address-cnp_status",
                    "Address-cnp_centre_jurisdiction",
                    "Address-cnp_state_jurisdiction",
                    "Address-cnp_trade_name",
                    "Address-cnp_company_name",
                    "Item-cnp_effort_and_cost_estimates",
                    "Item-cnp_on_going_efforts",
                    "Item-cnp_one_time_efforts",
                    "Item-cnp_efforts",
                    "Item-column_break_31",
                    "Item-cnp_one_time_cost",
                    "Item-cnp_on_going_cost",
                    "Quotation-cnp_pricing_policy_section",
                    "Quotation-cnp_pricing_policy_html",
                    "Quotation-cnp_opportunity",
                    "Quotation Item-cnp_type_of_fee",
                    "Quotation Item-cnp_calculated_initial_fee",
                    "Quotation Item-cnp_actual_initial_fee",
                    "Quotation Item-cnp_calculated_annual_fee",
                    "Quotation Item-cnp_actual_annual_fee",
                    "Quotation Item-cnp_selected_calculated_fee",
                    "Opportunity Item-cnp_calculated_initial_fee",
                    "Opportunity Item-cnp_calculated_annual_fee",
                    "Opportunity Item-cnp_actual_initial_fee",
                    "Opportunity Item-cnp_actual_annual_fee",
                    "Quotation Item-cnp_deviation",
                    "Quotation-cnp_auto_calculate",
                    "Quotation Item-cnp_one_time_fee_actual",
                    "Quotation Item-cnp_one_time_fee",
                    "Quotation-cnp_facility_amt_in_words",
                    "Quotation-cnp_date_of_entry",
                    "Quotation-cnp_facility_amt",
                    "Quotation Item-details",
                    "Quotation Item-cnp_fee_amount",
                    "Quotation Item-cnp_frequency",
                    "Quotation Item-cnp_calculation_basis",
                    "Quotation Item-cnp_minimum_fee_amount",
                    "Quotation Item-cnp_maximum_fee_amount",
                    "Quotation Item-cnp_additional_information_on_fees",
                    "Quotation Item-cnp_varying_fees",
                    "Quotation Item-cnp_fee_percentage",
                    "Quotation Item-cnp_next_fee_date",
                    "Quotation Item-cnp_fee_on",
                    "Quotation Item-cnp_fee_on_amount",
                    "Quotation Item-cnp_fee_per_tranche",
                    "Quotation Item-cnp_transaction_details",
                    "Quotation Item-column_break_11",
                    "Quotation Item-cnp_fee_code",
                    "Quotation Item-cnp_initial_fee_from_date",
                    "Quotation Item-cnp_initial_fee_from",
                    "Quotation-cnp_offer_letter",
                    "Quotation-cnp_letter_from",
                    "Quotation-cnp_letter_subject",
                    "Quotation-cnp_letter_body",
                    "Quotation-cnp_other_terms_and_conditions",
                    "Quotation-cnp_deviation_change",
                    "Quotation-cnp_initial_fee_zero",
                    "Quotation-cnp_varying_charge_details_section",
                    "Quotation-cnp_varying_charge_details",
                    "Address-cnp_msme_registered_uan",
                    "Address-cnp_corporate_identify_number",
                    "Address-cnp_website",
                    "Quotation-cnp_corporate_address",
                    "Quotation-cnp_registered_address",
                    "Opportunity-cnp_facility_amount_and_issue_size_section",
                    "Quotation-cnp_name_of_officer_1",
                    "Quotation-cnp_name_of_officer_2",
                    "Quotation-cnp_authorised_signatory",
                    "Quotation-cnp_annexure",
                    "Quotation-cnp_annexure_template",
                    "Quotation-cnp_send_for_approval",
                    "Item-cnp_psu",
                    "Item-cnp_charges_by_non_psu",
                    "Item-cnp_non_psu",
                    "Item-cnp_charges_by_psu",
                    "Lead-cnp_type",
                    "Opportunity Item-cnp_one_time_fee",
                    "Opportunity Item-cnp_one_time_fee_actual",
                    "Opportunity-cnp_initial_fee",
                    "Opportunity-cnp_annual_fee",
                    "Opportunity-section_break_38",
                    "Customer-cnp_type",
                    "Quotation-cnp_show_draft",
                    "Sales Order Item-column_break_14",
                    "Sales Order Item-cnp_initial_fee_from",
                    "Sales Order Item-cnp_initial_fee_from_date",
                    "Sales Order Item-cnp_transaction_details",
                    "Sales Order Item-cnp_fee_per_tranche",
                    "Sales Order Item-cnp_fee_on_amount",
                    "Sales Order Item-cnp_fee_on",
                    "Sales Order Item-cnp_next_fee_date",
                    "Sales Order Item-cnp_fee_percentage",
                    "Sales Order Item-cnp_varying_fees",
                    "Sales Order Item-cnp_fee_code",
                    "Sales Order Item-cnp_additional_information_on_fees",
                    "Sales Order Item-cnp_maximum_fee_amount",
                    "Sales Order Item-cnp_minimum_fee_amount",
                    "Sales Order Item-cnp_calculation_basis",
                    "Sales Order Item-cnp_frequency",
                    "Sales Order Item-cnp_fee_amount",
                    "Sales Order Item-details",
                    "Sales Order Item-cnp_type_of_fee",
                    "Quotation-cnp_gst_state",
                    "Sales Order-cnp_facility_amt_in_words",
                    "Sales Order-cnp_facility_amt",
                    "Quotation-cnp_proforma_invoice_details",
                    "Quotation-cnp_proforma_from",
                    "Quotation-cnp_proforma_address",
                    "Quotation-cnp_is_chargeable",
                    "Quotation-cnp_company_bank_details",
                    "Company-cnp_digital_signature",
                    "Company-cnp_button_html",
                    "Company-cnp_sign_secret",
                    "Company-cnp_s3_key",
                    "Quotation Item-cnp_proforma_amt",
                    "Quotation Item-cnp_proforma_period_from",
                    "Quotation Item-cnp_proforma_period_to",
                    "Opportunity-cnp_type",
                    "Opportunity-cnp_one_time_fee",
                    "Opportunity-cnp_type_of_security",
                    "Quotation-cnp_quotation_state",
                    "Sales Invoice Item-cnp_type_of_fee",
                    "Sales Invoice-cnp_from_quotation",
                    "Sales Invoice-cnp_months_pending",
                    "Sales Invoice-cnp_no_of_sales_invoice",
                    "Item-cnp_pre_execution_checklist_section",
                    "Item-cnp_pre_execution_checklist",
                    "Quotation-cnp_payment_schedule",
                    "Quotation-cnp_sales_invoice_schedule",
                    "Item-cnp_post_execution_checklist_section",
                    "Item-cnp_post_execution_checklist",
                    "Quotation-cnp_s3_key",
                    "Quotation-cnp_file_upload_html",
                    "Item-cnp_documentation_section",
                    "Item-cnp_transaction_documents_checklist",
                    "Item-cnp_security_documents_checklist",
                    "Item-cnp_other_documents_checklist",
                    "Journal Entry Account-canopi_amortization",
                    "Quotation-cnp_offer_acceptence_date",
                    "Sales Invoice-cnp_composition_levy",
                    "Quotation-cnp_customer_code",
                    "Opportunity-cnp_customer_code",
                    "Quotation-cnp_invoice_schedule",
                    "Quotation-cnp_invoice_schedule_details",
                    "Purchase Invoice-cnp_composition_levy",
                    "Journal Entry Account-canopi_reference_type",
                    "Journal Entry Account-canopi_reference_name",
                    "Lead-cnp_name_of_group",
                    "Lead-cnp_deal_referred_by",
                    "Lead-cnp_internally_referred_by",
                    "Lead-cnp_expected_deal_closure",
                    "Quotation-cnp_authorised_signatory_designation",
                    "Quotation-cnp_cl_code",
                    "Opportunity-cnp_more_info",
                    "Opportunity-column_break_100",
                    "Opportunity-column_break_101",
                    "Opportunity-column_break_102",
                    "Opportunity-column_break_103",
                    "Opportunity-column_break_104",
                    "Currency-cnp_iso_code",
                    "Currency-cnp_currency_description",
                    "Lead-cnp_primary_details",
                    "Lead-cnp_other_details",
                    "Opportunity-cnp_about_organization",
                    "Quotation-cnp_offer_and_proforma_invoice",
                    "Quotation-cnp_sales_invoice_details",
                    "Quotation-cnp_other_details",
                    "Quotation-cnp_product",
                    "Quotation-column_break_19",
                    "Note-cnp_notes_html",
                    "Sales Invoice-ack_no",
                    "Sales Invoice-ack_date",
                    "Sales Invoice-signed_qr_code",
                    "Sales Invoice-signed_einvoice",
                    "Opportunity-cnp_other_details_tab",
                    "Opportunity-cnp_sectors",
                    "Opportunity-cnp_invit_sub_sectors",
                    "Opportunity-cnp_product_details_section",
                    "Opportunity-cnp_name_of_trust",
                    "Opportunity-cnp_number_of_spv",
                    "Opportunity-cnp_number_of_projects",
                    "Opportunity-cnp_product_type",
                    "Opportunity-cnp_trust_size_in_crores",
                    "Item-cnp_condition_precedent_part_a_checklist",
                    "Item-cnp_condition_precedent_part_b_checklist",
                    "Item-cnp_condition_subsequent_checklist",
                    "Loan-cnp_lender",
                    "Bank Account-cnp_lender",
                    "Bank-cnp_lender",
                    "Bank-cnp_is_benef_corr",
                    "Bank-cnp_corr_swift_number",
                    "Bank-cnp_corr_bank_name",
                    "Bank-cnp_column_break_02",
                    "Bank-cnp_reference",
                    "Bank-cnp_beneficiary_account_no",
                    "Bank-cnp_beneficiary_bank_account",
                    "Bank-cnp_column_break_01",
                    "Bank-cnp_chips_uid",
                    "Bank-cnp_beneficiary_swift_number",
                    "Bank-cnp_beneficiary_bank_name",
                    "Bank-cnp_other_details",
                    "Item-cnp_documents_checklist",
                    "Quotation-cnp_is_tranche_initiated",
                    "Customer-cnp_reference_no",
                    "Quotation-cnp_is_reject_reason_commented",
                    "Customer-cnp_fa_details",
                    "Loan-cnp_fa_details",
                    "Note-cnp_note_details",
                    "Note-cnp_fiscal_year",
                    "Note-cnp_calc_details",
                    "Sales Invoice-cnp_proforma_invoice_details",
                    "Sales Invoice-cnp_proforma_from",
                    "Sales Invoice-cnp_company_bank_details",
                    "Sales Invoice-cnp_authorised_signatory_designation",
                    "Sales Invoice-cnp_proforma_address",
                    "Sales Invoice-cnp_is_chargeable",
                    "Sales Invoice-cnp_is_approved_rejected",
                    "Sales Invoice-cnp_approval",
                    "Payment Entry-cnp_approval",
                    "Quotation-cnp_accounting_dimensions",
                    "Quotation-cnp_cost_center",
                    "Sales Invoice-cnp_cgst",
                    "Sales Invoice-cnp_sgst",
                    "Sales Invoice-cnp_igst",
                    "Sales Invoice-cnp_cl_code",
                    "Journal Entry Account-cnp_cl_code",
                    "Note-cnp_type",
                    "Note-cnp_company",
                    "Payment Entry-cnp_status",
                    "Sales Invoice-cnp_status",
                    "Opportunity-cnp_is_approved_rejected",
                    "Opportunity-cnp_is_reject_reason_commented",
                    "Opportunity-cnp_send_for_approval",
                    "Opportunity-workflow_state",
                    "Opportunity-cnp_get_approver_sender_user",
                    "Journal Entry-cnp_status",
                    "Journal Entry-workflow_state",
                    "Customer-cnp_please_specify",
                    "Sales Invoice-cnp_is_debit_note",
                    "Sales Invoice-cnp_type_of_account",
                    "Lead-cnp_creation_date",
                    "Item-cnp_annexure_b_checklist",
                    "Opportunity-cnp_cl_code",
                    "Sales Invoice-transporter_info",
                    "Quotation-cnp_offer_letter_sequence_atsl",
                    "Quotation-cnp_offer_letter_sequence_axis",
                    "Quotation-cnp_offer_letter_serial_no",
                    "Account-cnp_disable_flag",
                    "Opportunity-cnp_facility_amt_in_words_millions",
                    "Quotation-cnp_facility_amt_in_words_millions",
                    "Account-cnp_importer_flag",
                    "Sales Invoice-cnp_email_token",
                    "Sales Invoice-cnp_email_token_expiry",
                    "Journal Entry-cnp_email_token",
                    "Journal Entry-cnp_email_token_expiry",
                    "Payment Entry-cnp_email_token",
                    "Payment Entry-cnp_email_token_expiry",
                    "Account-cnp_email_token",
                    "Account-cnp_email_token_expiry",
                    "Purchase Invoice-cnp_email_token",
                    "Purchase Invoice-cnp_email_token_expiry",
                    "Opportunity-cnp_name_of_group",
                    "Sales Invoice-cnp_reason_for_issuing_document_others",
                    "Quotation-cnp_is_sales_invoice_generated",
                    "Opportunity-cnp_channel_partner_name",
                    "Lead-cnp_channel_partner_name",
                    "Sales Invoice-cnp_reject_reason_comment",
                    "Sales Invoice-cnp_is_reject_reason",
                    "Sales Invoice-cnp_ifsc_code",
                    "Sales Invoice-cnp_beneficiary_account_number",
                    "Sales Invoice-cnp_beneficiary_bank_and_branch",
                    "Sales Invoice-cnp_beneficiary_name",
                    "Sales Invoice-cnp_debit_note_qr_code",
                    "Sales Invoice-cnp_description_of_service",
                    "Sales Invoice-cnp_debit_note_details",
                    "Sales Invoice-cnp_kind_attn",
                    "Sales Invoice-cnp_debit_note_remarks",
                    "Sales Invoice-cnp_credit_note_qr_code",
                    "Sales Invoice Item-cnp_reversal_start_date",
                    "Sales Invoice Item-cnp_reversal_end_date",
                    "Sales Invoice-cnp_credit_note_remarks",
                    "Sales Invoice-cnp_credit_note_authorized_singatory",
                    "Sales Invoice-cnp_credit_note_description_of_service",
                    "Sales Invoice-cnp_credit_note_format",
                    "Opportunity-cnp_group_of_field",
                    "Lead-cnp_group_of_field",
                    "Quotation-cnp_email_token",
                    "Quotation-cnp_email_token_expiry",
                    "Sales Invoice-cnp_action",
                    "Payment Entry-cnp_action",
                    "Purchase Invoice-cnp_action",
                    "Journal Entry-cnp_action",
                    "Quotation-workflow_state",
                    "Payment Entry-cnp_pan",
                    "Opportunity-cnp_is_int_reset",
                    "Lead-cnp_fetch_base_details",
                    "Opportunity-cnp_fetch_base_details",
                    "Sales Invoice-cnp_sundry_debtors_account",
                    "Cost Center-cnp_addr_section",
                    "Address-cnp_cost_center",
                    "Cost Center-cnp_address_html",
                    "Customer-cnp_fetch_base_details",
                    "Address-cnp_is_default_offer_letter_address",
                    "Item-cnp_sebi_application_checklist",
                    "Item-cnp_stages_of_offer_document_checklist",
                    "Item-cnp_document_based_compliances_checklist",
                    "Item-cnp_event_based_compliances_checklist",
                    "Address-cnp_address_line3",
                    "Sales Invoice-cnp_is_partial_credit_note",
                    "Journal Entry-cnp_accounting_dimensions_section",
                    "Journal Entry-cnp_cost_center",
                    "Quotation-cnp_approval_role",
                    "Cost Center-cnp_default_address",
                    "Cost Center-cnp_not_in_filter",
                    "Quotation-cnp_proforma_no",
                    "Sales Invoice-cnp_proforma_no",
                    "Opportunity-custom_type_of_product",
                    "Opportunity-custom_section_break_mlwpo",
                    "Opportunity-custom_facility_amount",
                    "Opportunity-custom_column_break_vl029",
                    "Opportunity-custom_exchange_rate",
                    "Opportunity-custom_column_break_dqrbb",
                    "Opportunity-custom_currency",
                    "Item-cnp_common_documents_checklist_section",
                    "Item-cnp_common_documents_checklist_table",
                    "Payment Entry-cnp_ineligible_for_itc",
                ],
            ],
        ],
    },
    {
        "dt": "Property Setter",
        "filters": [
            [
                "name",
                "in",
                [
                    "Lead-lead_owner-label",
                    "Lead-address_type-options",
                    "Lead-status-options",
                    "Lead-designation-hidden",
                    "Lead-gender-hidden",
                    "Lead-campaign_name-hidden",
                    "Opportunity-naming_series-label",
                    "Opportunity Item-item_code-label",
                    "Opportunity Item-item_group-label",
                    "Opportunity Item-item_name-label",
                    "Lead-organization_lead-default",
                    "Lead-market_segment-hidden",
                    "Lead-industry-hidden",
                    "Opportunity-contact_person-label",
                    "Opportunity-contact_display-label",
                    "Industry Type-industry-unique",
                    "Opportunity-with_items-hidden",
                    "Opportunity-items_section-hidden",
                    "Opportunity-items-hidden",
                    "Quotation-shipping_address_name-hidden",
                    "Quotation Item-qty-default",
                    "Lead-pincode-mandatory_depends_on",
                    "Quotation-items-label",
                    "Quotation Item-item_code-label",
                    "Quotation Item-item_name-label",
                    "Address-address_type-options",
                    "Quotation-base_net_total-hidden",
                    "Quotation-net_total-hidden",
                    "Quotation Item-quantity_and_rate-hidden",
                    "Quotation Item-item_code-read_only",
                    "Lead-type-hidden",
                    "Quotation Item-rate-label",
                    "Lead-source-description",
                    "Customer-customer_type-hidden",
                    "Sales Order Item-item_code-label",
                    "Sales Order Item-item_name-label",
                    "Sales Order-items-label",
                    "Sales Order Item-rate-label",
                    "Sales Order-items-read_only",
                    "Sales Invoice-items-label",
                    "Quotation-section_break_44-hidden",
                    "Sales Taxes and Charges-rate-label",
                    "Sales Invoice-main-quick_entry",
                    "Quotation Item-cnp_next_fee_date-allow_on_submit",
                    "Customer-naming_series-reqd",
                    "Customer-naming_series-hidden",
                    "Sales Order-tax_id-hidden",
                    "Sales Order-tax_id-print_hide",
                    "Sales Invoice-tax_id-hidden",
                    "Sales Invoice-tax_id-print_hide",
                    "Delivery Note-tax_id-hidden",
                    "Delivery Note-tax_id-print_hide",
                    "Packed Item-rate-read_only",
                    "Customer-main-autoname",
                    "Journal Entry-voucher_type-options",
                    "Opportunity-main-links_order",
                    "Customer-main-links_order",
                    "Lead-first_name-mandatory_depends_on",
                    "Lead-company_name-mandatory_depends_on",
                    "Opportunity-dashboard_tab-show_dashboard",
                    "Note-content-hidden",
                    "Bank-main-quick_entry",
                    "Sales Invoice Item-item_code-read_only",
                    "Sales Invoice Item-item_name-read_only",
                    "Sales Invoice Item-qty-read_only",
                    "Sales Invoice Item-uom-read_only",
                    "Sales Invoice Item-rate-read_only",
                    "Sales Invoice Item-description-read_only",
                    "Payment Entry-party_name-read_only",
                    "Quotation-taxes-read_only",
                    "Quotation-more_info_tab-hidden",
                    "Address-address_title-reqd",
                    "Quotation-customer_address-depends_on",
                    "Quotation-contact_person-depends_on",
                    "Opportunity-source-reqd",
                    "Opportunity-probability-precision",
                    "Quotation-total_qty-hidden",
                    "Contact-status-Options",
                    "Purchase Invoice-project-hidden",
                    "Sales Invoice-total_qty-hidden",
                    "Sales Invoice-project-hidden",
                    "Quotation Item-item_weight_details-hidden",
                    "Quotation Item-reference-hidden",
                    "Quotation Item-item_balance-hidden",
                    "Quotation Item-shopping_cart_section-hidden",
                    "Quotation-main-allow_auto_repeat",
                    "Sales Invoice-subscription_section-hidden",
                    "Sales Invoice-more_information-hidden",
                    "Quotation-main-search_fields",
                    "Sales Invoice-time_sheet_list-hidden",
                    "Sales Invoice-incoterm-hidden",
                    "Sales Invoice-shipping_rule-hidden",
                    "Sales Invoice-loyalty_points_redemption-hidden",
                    "Sales Invoice-section_break2-hidden",
                    "Sales Invoice-section_break_49-hidden",
                    "Payment Entry-cost_center-reqd",
                    "Lead-job_title-label",
                    "Opportunity-job_title-label",
                    "Contact-designation-label",
                    "Sales Invoice-cost_center-reqd",
                    "Purchase Invoice-cost_center-reqd",
                    "Journal Entry Account-cost_center-reqd",
                    "Quotation Item-item_code-columns",
                    "Quotation Item-amount-in_list_view",
                    "Quotation Item-item_code-in_list_view",
                    "Sales Invoice-is_return-in_standard_filter",
                    "Sales Invoice Item-income_account-columns",
                    "Sales Invoice Item-income_account-in_list_view",
                    "Purchase Invoice Item-item_code-columns",
                    "Purchase Invoice Item-rate-columns",
                    "Purchase Invoice Item-expense_account-in_list_view",
                    "Purchase Invoice Item-expense_account-columns",
                    "Sales Invoice Item-serial_no-in_list_view",
                    "Sales Invoice Item-batch_no-in_list_view",
                    "Sales Invoice Item-warehouse-in_list_view",
                    "Sales Invoice Item-rate-columns",
                    "Sales Invoice Item-qty-columns",
                    "Sales Invoice Item-qty-in_list_view",
                    "Sales Invoice Item-amount-columns",
                    "Sales Invoice Item-item_code-columns",
                    "Sales Invoice Item-cost_center-read_only",
                    "Purchase Invoice Item-cost_center-read_only",
                    "Sales Invoice Item-cost_center-default",
                    "Lead-company_name-unique",
                    "Payment Entry-mode_of_payment-reqd",
                ],
            ],
        ],
    },
    {
        "dt": "Role",
        "filters": [
            [
                "name",
                "in",
                [
                    "BCG RM",
                    "BCG Head",
                    "Management Trainee",
                    "LAM Admin",
                    "Ops & Serv Maker",
                    "Admin Approver",
                    "Ops Checker",
                    "Ops Head",
                    "COO Head",
                    "Accounts Head",
                    "ATSL Compliance Maker",
                    "Legal Maker",
                    "MD & CEO",
                    "Accounts Manager",
                ],
            ],
        ],
    },
    {
        "dt": "Custom DocPerm",
        "filters": [
            [
                "role",
                "in",
                [
                    "BCG RM",
                    "BCG Head",
                    "Management Trainee",
                    "LAM Admin",
                    "System Manager",
                    "All",
                    "Accounts Manager",
                    "Auditor",
                    "Purchase User",
                    "Sales User",
                    "Accounts User",
                    "Item Manager",
                    "Stock Manager",
                    "Administrator",
                    "Sales Master Manager",
                    "Sales Manager",
                    "Website Manager",
                    "Quality Manager",
                    "Purchase Master Manager",
                    "Manufacturing User",
                    "Employee",
                    "Projects User",
                    "HR User",
                    "Purchase Manager",
                    "Stock User",
                    "Agriculture Manager",
                    "Agriculture User",
                    "Ops & Serv Maker",
                    "Maintenance User",
                    "Maintenance Manager",
                    "Workspace Manager",
                    "Report Manager",
                    "Dashboard Manager",
                    "Guest",
                    "Customer",
                    "Inbox User",
                    "Ops Checker",
                    "Ops Head",
                    "COO Head",
                    "Accounts Head",
                    "ATSL Compliance Maker",
                    "Legal Maker",
                    "MD & CEO",
                ],
            ],
        ],
    },
    {
        "dt": "Workflow",
        "filters": [
            [
                "name",
                "in",
                [
                    "Opportunity",
                    "Quotation",
                    "Fixed Deposit",
                    "Fixed Deposit Maturity",
                    "Mutual Funds",
                    "Sales Invoice",
                    "Purchase Invoice",
                    "Redemption Accounting",
                    "Fund Closure",
                    "Payment Entry",
                    "Account",
                    "Journal Entry",
                ],
            ],
        ],
    },
    {
        "dt": "Workflow State",
        "filters": [
            [
                "name",
                "in",
                [
                    "Cancelled",
                    "Lost",
                    "Renegotiate",
                    "Submitted",
                    "Accepted By Client",
                    "Pending for Approval",
                    "Draft",
                    "Rejected",
                    "Approved",
                    "Matured",
                    "Closed",
                    "Redeemed",
                    "New",
                ],
            ],
        ],
    },
    {
        "dt": "Workflow Action Master",
        "filters": [
            [
                "name",
                "in",
                [
                    "Renegotiate",
                    "Accepted By Client",
                    "Submit",
                    "Send For Approval",
                    "Reject",
                    "Approve",
                    "Cancel",
                ],
            ],
        ],
    },
    {
        "dt": "Currency",
        "filters": [
            [
                "name",
                "in",
                [
                    "GBP",
                    "INR",
                    "CNY",
                    "JPY",
                    "AED",
                    "EUR",
                    "USD",
                    "CHF",
                    "AUD",
                    "MUR",
                    "MYR",
                    "TWD",
                    "CAD",
                    "MXN",
                    "KYD",
                    "BHD",
                    "SGD",
                ],
            ],
        ],
    },
    {
        "dt": "Workspace",
        "filters": [
            [
                "name",
                "in",
                [
                    "Accounting",
                    "CRM",
                    "Main",
                    "Gift City",
                    "Mumbai",
                    "Delhi",
                    "Axis Bank",
                ],
            ],
        ],
    },
    {"dt": "Letter Head", "filters": [["name", "in", ["Canopi Footer"]]]},
    {
        "dt": "Email Template",
        "filters": [
            [
                "name",
                "in",
                [
                    "No Information Email",
                    "Email to Credit Rating Agency",
                ],
            ],
        ],
    },
    {
        "dt": "Payment Term",
        "filters": [
            [
                "name",
                "in",
                [
                    "Annual Fee",
                    "Initial Fee",
                    "One Time Fee",
                    "100% Advance",
                ],
            ],
        ],
    },
    {
        "dt": "Lead Source",
        "filters": [
            [
                "name",
                "in",
                [
                    "Channel Partner",
                ],
            ],
        ],
    },
    {
        "dt": "List View Settings",
        "filters": [
            [
                "name",
                "in",
                [
                    "Quotation",
                ],
            ],
        ],
    },
]
