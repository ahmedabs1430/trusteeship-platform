import frappe

from frappe.custom.doctype.custom_field.custom_field import (  # noqa: 501 isort:skip
    create_custom_fields,
)


def execute():
    create_custom_field()


def create_custom_field():
    sales_invoice_field = {
        "Sales Invoice Item": [
            {
                "fieldname": "gst_hsn_code",
                "in_list_view": 1,
                "columns": 2,
            },
        ]
    }
    if frappe.get_meta("Sales Invoice Item").has_field("gst_hsn_code") and (
        (
            frappe.get_meta("Sales Invoice Item")
            .get_field("gst_hsn_code")
            .in_list_view  # noqa: 501 isort:skip
            != 1
        )
        or (
            frappe.get_meta("Sales Invoice Item")
            .get_field("gst_hsn_code")
            .columns  # noqa: 501 isort:skip
            != 2  # noqa: 501 isort:skip
        )
    ):
        create_custom_fields(sales_invoice_field, update=True)
