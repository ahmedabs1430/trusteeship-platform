/* global frappe, set_field_options */
/* eslint-env jquery */
frappe.ui.form.on('Opportunity', {
  refresh: frm => {
    addCnpApprovalStatusIndicator(frm);
    initiateOpsFLowButton(frm);
    setFacilityAmtFixHeight(frm);
    setLabelReferencNumber(frm);
    loadPricingTable(frm);
    loadCrRatingTable(frm);
    loadFininfoTable(frm);
    loadDirectorAndSignatory(frm);
    addFetchButton(frm);
    setSubSectorsOptions(frm);
    setTenorBlank(frm);
    disableProduct(frm); // disable product when quotation exists for opportunity
    disableForm(frm);
    frm.toggle_reqd('contact_mobile', false);
    if (frm.doc.contact_person) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.fetch_contact',
        args: { name: frm.doc.contact_person },
        callback: r => {
          frm.toggle_reqd('contact_mobile', false);
          frm.doc.contact_mobile = r.message.number;
          frm.refresh_field('contact_mobile');
          frm.doc.contact_display = r.message.full_name;
          frm.refresh_field('contact_display');
        },
      });
    }
    setSecurityTypeReqd(frm);

    const prevRoute = frappe.get_prev_route();
    if (prevRoute[1] === 'Canopi Pricing Policy') {
      frm.scroll_to_field('cnp_pricing_policy_html');
      frm.refresh_fields();
    }
    if (prevRoute[1] === 'Lead') {
      localStorage.setItem('lead', 0);
    }
    setupTypeOfSecurityField(frm);
    addOperationsFlowBtn(frm);
    addOperationsFlowBtnInConnections(frm);
    hideAddOperationsFlowBtn(frm);
    hideChildTables(frm);
    removeCustomButton(frm);
    typeOfSecurityExpanded(frm);
    hideAboutOrganizationTab(frm);
    initiateOpsFLowMessage(frm);
    emailAction(frm);
    addRejectionComment(frm);
    hideActions(frm);
    filterProduct(frm);
    setDefaultProductDetais(frm);
  },
  before_load: frm => {
    if (
      frm.doc.__islocal &&
      frm.doc.opportunity_from === 'Lead' &&
      frm.doc.party_name
    ) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: frm.doc.party_name, doc_type: 'Lead' },
        callback: r => {
          frm.doc.expected_closing = r.message.cnp_expected_deal_closure;
          frm.refresh_field('expected_closing');
        },
      });
    }
  },

  before_save: frm => {
    if (frm.doc.cnp_type === 'Others') {
      frm.toggle_reqd('cin_number', false);
    } else {
      frm.toggle_reqd('cin_number', true);
    }

    // validate tenor duration
    if (frm.doc.cnp_tenor_months > 12) {
      frappe.throw('Invalid Tenor Months');
    }
    if (frm.doc.cnp_tenor_days > 31) {
      frappe.throw('Invalid Tenor Days');
    }
  },

  validate: frm => {
    if (frm.doc.cin_number) {
      validateReferenceNumber(frm);
    }
    if (frm.doc.opportunity_from === 'Customer' && frm.doc.party_name) {
      saveDirectorSignatoryInCustomer(frm);
    }
  },

  after_save: frm => {
    frm.trigger('onload_post_render');
  },

  onload_post_render: frm => {
    fillPricingPolicy(frm);
  },

  cnp_sectors: frm => {
    setSubSectorsOptions(frm);
  },

  cnp_issue_size: frm => {
    frm.doc.cnp_issue_size = frm.doc.cnp_issue_size.replace(/(l|L)$/, '00000');
    frm.doc.cnp_issue_size = frm.doc.cnp_issue_size.replace(
      /(c|C)$/,
      '0000000',
    );
    frm.doc.cnp_issue_size = frm.doc.cnp_issue_size.replace(/(m|M)$/, '000000');
    frm.doc.cnp_issue_size = frm.doc.cnp_issue_size.replace(
      /(b|B)$/,
      '000000000',
    );
    frm.doc.cnp_issue_size = frm.doc.cnp_issue_size.replace(/[^0-9]/g, '');
    frm.refresh_field('cnp_issue_size');
    if (frm.doc.cnp_issue_size) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.num_in_words',
        args: { integer: frm.doc.cnp_issue_size },
        freeze: true,
        callback: r => {
          frm.toggle_display('cnp_issue_size_in_words', true);
          frm.doc.cnp_issue_size_in_words = r.message;
          frm.refresh_field('cnp_issue_size_in_words');
        },
      });
    } else {
      frm.doc.cnp_issue_size_in_words = '';
      frm.toggle_display('cnp_issue_size_in_words', false);
    }
  },

  cnp_product: frm => {
    setSubSectorsOptions(frm);
    setSecurityTypeReqd(frm);
    loadPricingTable(frm);
    setupTypeOfSecurityField(frm);
    typeOfSecurityExpanded(frm);
  },
  custom_type_of_product: frm => {
    setSecurityTypeReqd(frm);
  },

  party_name: frm => {
    if (frm.doc.opportunity_from === 'Customer' && frm.doc.party_name) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: frm.doc.party_name, doc_type: 'Customer' },
        freeze: true,
        callback: r => {
          frm.doc.cin_number = r.message.cin_number;
          frm.refresh_field('cin_number');
          frm.doc.cnp_director_shareholdings = r.message
            .cnp_director_shareholdings
            ? r.message.cnp_director_shareholdings
            : '';
          frm.refresh_field('cnp_director_shareholdings');
          frm.doc.cnp_authorized_signatories = r.message
            .cnp_authorized_signatories
            ? r.message.cnp_authorized_signatories
            : '';
          frm.refresh_field('cnp_authorized_signatories');
          loadDirectorAndSignatory(frm);
        },
      });
    }
    if (
      frm.doc.__islocal &&
      frm.doc.opportunity_from === 'Lead' &&
      frm.doc.party_name
    ) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: frm.doc.party_name, doc_type: 'Lead' },
        callback: r => {
          frm.doc.expected_closing = r.message.cnp_expected_deal_closure;
          frm.refresh_field('expected_closing');
        },
      });
    }
  },

  cnp_type: frm => {
    setLabelReferencNumber(frm);
  },

  cnp_fetch_base_details: frm => {
    fetchBaseDetails(frm);
  },
  custom_currency: frm => {
    convertToINR(frm);
  },
  custom_exchange_rate: frm => {
    convertToINR(frm);
  },
  custom_facility_amount: frm => {
    convertToINR(frm);
  },
});

function set_word_vlaues(frm) {
  frm.doc.custom_facility_amount = frm.doc.custom_facility_amount.replace(
    /(l|L)$/,
    '00000',
  );
  frm.doc.custom_facility_amount = frm.doc.custom_facility_amount.replace(
    /(c|C)$/,
    '0000000',
  );
  frm.doc.custom_facility_amount = frm.doc.custom_facility_amount.replace(
    /(m|M)$/,
    '000000',
  );
  frm.doc.custom_facility_amount = frm.doc.custom_facility_amount.replace(
    /(b|B)$/,
    '000000000',
  );
  frm.doc.custom_facility_amount = frm.doc.custom_facility_amount.replace(
    /[^0-9]/g,
    '',
  );
  frm.refresh_field('custom_facility_amount');

  if (frm.doc.cnp_facility_amt) {
    frappe.call({
      method:
        'trusteeship_platform.custom_methods.num_in_words_in_lakhs_and_million',
      args: { integer: parseInt(frm.doc.cnp_facility_amt, 10) },
      callback: r => {
        frm.toggle_display('cnp_facility_amt_in_words', true);
        frm.doc.cnp_facility_amt_in_words = r.message.in_lakh;
        frm.refresh_field('cnp_facility_amt_in_words');
        frm.toggle_display('cnp_facility_amt_in_words_millions', true);
        frm.doc.cnp_facility_amt_in_words_millions = r.message.in_million;
        frm.refresh_field('cnp_facility_amt_in_words_millions');
      },
    });
  } else {
    frm.doc.cnp_facility_amt_in_words = '';
    frm.toggle_display('cnp_facility_amt_in_words', false);
    frm.doc.cnp_facility_amt_in_words_millions = '';
    frm.toggle_display('cnp_facility_amt_in_words_millions', false);
  }
}
function convertToINR(frm) {
  set_word_vlaues(frm);
  if (frm.doc.custom_facility_amount) {
    if (frm.doc.custom_currency.toUpperCase() === 'INR') {
      frm.set_value(
        'cnp_facility_amt',
        parseInt(frm.doc.custom_facility_amount, 10),
      );
      frm.refresh_field('cnp_facility_amt');
    }
    frappe.call({
      method: 'erpnext.setup.utils.get_exchange_rate',
      args: {
        from_currency: frm.doc.custom_currency,
        to_currency: 'INR',
      },
      callback: r => {
        frm.set_value(
          'cnp_facility_amt',
          parseInt(frm.doc.custom_facility_amount * r.message, 10),
        );
        frm.refresh_field('cnp_facility_amt');
        frm.set_value('custom_exchange_rate', r.message);
        frm.refresh_field('custom_exchange_rate');
        set_word_vlaues(frm);
      },
    });
  }
}

function setTenorBlank(frm) {
  if (frm.doc.__islocal) {
    if (!frm.doc.cnp_tenor_years) {
      frm.doc.cnp_tenor_years = '';
      frm.refresh_field('cnp_tenor_years');
    }
    if (!frm.doc.cnp_tenor_months) {
      frm.doc.cnp_tenor_months = '';
      frm.refresh_field('cnp_tenor_months');
    }
    if (!frm.doc.cnp_tenor_days) {
      frm.doc.cnp_tenor_days = '';
      frm.refresh_field('cnp_tenor_days');
    }
  }
}

function fetchBaseDetails(frm) {
  if (
    !frm.doc.cnp_is_probe42_data_fetched &&
    (!frappe.user.has_role('BCG Head') ||
      frappe.session.user === 'Administrator')
  ) {
    if (checkRequiredFields(frm)) {
      loadDirectorAndSignatory(frm);
      frappe.call({
        freeze: true,
        method:
          'trusteeship_platform.custom_methods.fetch_base_details_for_opportunity',
        args: {
          opportunity_name: frm.doc.name,
          source: frm.doc.source,
          is_local: frm.doc.__islocal !== undefined ? frm.doc.__islocal : 0,
          cnp_type: frm.doc.cnp_type,
          opportunity_from: frm.doc.opportunity_from,
          party_name: frm.doc.party_name,
          cin_number: frm.doc.cin_number,
          product: frm.doc.cnp_product,
          facility_amt: frm.doc.cnp_facility_amt,
          issue_size: frm.doc.cnp_issue_size,
          tenor_years: frm.doc.cnp_tenor_years,
          tenor_months: frm.doc.cnp_tenor_months,
          tenor_days: frm.doc.cnp_tenor_days,
          security_type: frm.doc.cnp_security_type.length
            ? frm.doc.cnp_security_type
            : [],
        },
        callback: r => {
          if (frm.doc.__islocal) {
            frappe.set_route('Form', 'Opportunity', r.message.name);
          }
          if (frm.doc.cnp_product === '') {
            frappe.throw('Please select product!!');
          }
          if (r.message.cnp_fininfo.length === 0) {
            frappe.show_alert(
              {
                message: 'No financial records available.',
                indicator: 'yellow',
              },
              5,
            );
          } else {
            frappe.show_alert(
              {
                message: 'Fetched successfully.',
                indicator: 'green',
              },
              5,
            );
          }

          frm.reload_doc();
        },
      });
    }
  }
}

function hideAboutOrganizationTab(frm) {
  if (frm.doc.__islocal) {
    frm.fields_dict.cnp_director_details.tab.df.hidden = true;
  } else {
    frm.fields_dict.cnp_director_details.tab.df.hidden = false;
  }
  frm.refresh_fields();
}

function setSubSectorsOptions(frm) {
  if (
    frm.doc.cnp_product?.toUpperCase() === 'INVIT' ||
    frm.doc.cnp_product?.toUpperCase() === 'REIT'
  ) {
    set_field_options(
      'cnp_invit_sub_sectors',
      getSubSectorsOptions(frm).join('\n'),
    );
  }
}

function getSubSectorsOptions(frm) {
  const subSectors = {
    Transport: [
      'Roads and bridges',
      'Ports',
      'Inland Waterways',
      'Airport',
      'Railway Track, tunnels, viaducts, bridges',
      'Urban Public Transport (except rolling stock in case of urbanroad transport)',
    ],

    Energy: [
      'Electricity Generation',
      'Electricity Transmission',
      'Electricity Distribution',
      'Oil pipelines',
      'Oil/Gas/Liquefied Natural Gas (LNG) storage facility',
      'Gas pipelines',
    ],

    'Water & Sanitation': [
      'Solid Waste Management',
      'Water supply pipelines',
      'Water treatment plants',
      'Sewage collection, treatment and disposal system',
      'Irrigation (dams, channels, embankments etc)',
      'Storm Water Drainage System',
      'Slurry Pipelines',
    ],

    Communication: [
      'Telecommunication (Fixed network)',
      'Telecommunication towers',
      'Telecommunication & Telecom Services',
    ],

    'Social and Commercial Infrastructure': [
      'Education Institutions (capital stock)',
      'Hospitals (capital stock)',
      'Three-star or higher category classified hotels located outside cities with population of more than 1 million',
      'Common infrastructure for industrial parks, SEZ, tourism facilities and agriculture markets',
      'Fertilizer (Capital investment)',
      'Post harvest storage infrastructure for agriculture and horticultural produce including cold storage',
      'Terminal markets',
      'Soil-testing laboratories',
      'Cold Chain',
      'Hotels with project cost of more than Rs.200 crores each in any place in India and of any star rating',
      'Convention Centres with project cost of more than Rs.300 crore each',
    ],
  };

  return frm.doc.cnp_sectors ? subSectors[`${frm.doc.cnp_sectors}`] : [];
}

function setDefaultProductDetais(frm) {
  if (!frm.doc.__islocal || frm.doc.cnp_is_int_reset === 1) return;
  frm.set_value('cnp_number_of_spv', undefined);
  frm.set_value('cnp_number_of_projects', undefined);
  frm.set_value('cnp_trust_size_in_crores', undefined);
  frm.set_value('cnp_is_int_reset', 1);
}

function setFacilityAmtFixHeight(frm) {
  const field = frm.get_field('cnp_facility_amt');
  if (field.input) {
    field.input.style.height = 'calc(1.5em + 0.75rem + 2px)';
    field.input.style.resize = 'none';
  }
  const field2 = frm.get_field('custom_facility_amount');
  if (field2.input) {
    field2.input.style.height = 'calc(1.5em + 0.75rem + 2px)';
    field2.input.style.resize = 'none';
  }
}

function setupTypeOfSecurityField(frm) {
  setTypeOfSecurityOptions(frm);
  setTypeOfSecurityFieldName(frm);
}

function setTypeOfSecurityFieldName(frm) {
  if (frm.doc.cnp_product?.toUpperCase() === 'DTE') {
    frm.set_df_property(
      'cnp_type_of_security',
      'label',
      'Nature of Instrument',
    );
  } else if (frm.doc.cnp_product?.toUpperCase() === 'STE') {
    frm.set_df_property('cnp_type_of_security', 'label', 'Type of Facility');
  }
}

function setTypeOfSecurityOptions(frm) {
  if (
    frm.doc.cnp_product?.toUpperCase() === 'DTE' ||
    frm.doc.cnp_product?.toUpperCase() === 'STE'
  ) {
    const options = getTypeOfSecurityOptions(frm);
    frm.doc.cnp_type_of_security = options.includes(
      frm.doc.cnp_type_of_security,
    )
      ? frm.doc.cnp_type_of_security
      : '';
    frm.toggle_display('cnp_type_of_security', true);
    frm.toggle_reqd('cnp_type_of_security', true);
    set_field_options('cnp_type_of_security', options.join('\n'));
  } else {
    frm.doc.cnp_type_of_security = '';
    frm.toggle_display('cnp_type_of_security', false);
    frm.toggle_reqd('cnp_type_of_security', false);
  }
}

function getTypeOfSecurityOptions(frm) {
  if (frm.doc.cnp_product.toUpperCase() === 'STE') {
    return ['Rupee', 'ECB Term Loans', 'Working Capital Facilities'];
  } else if (frm.doc.cnp_product.toUpperCase() === 'DTE') {
    return [
      'Non Convertible',
      'Market Linked',
      'Compulsory Convertible',
      'Fully Convertible',
      'Optionally Convertible',
      'Partially Convertible',
      'Others',
    ];
  }
}

function addOperationsFlowBtn(frm) {
  if (
    frm.doc.opportunity_from === 'Customer' &&
    !frm.custom_buttons['Operations Flow']
  ) {
    frm.add_custom_button(
      'Operations Flow',
      () => {
        frappe.model.open_mapped_doc({
          method:
            'trusteeship_platform.custom_methods.make_operations_flow_for_opportunity',
          frm,
        });
      },
      'Create',
    );
  }
}

function loadPricingTable(frm) {
  $(frm.fields_dict.cnp_pricing_policy_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: { doctype: 'Canopi Pricing Policy' },
      freeze: true,
      callback: r => {
        const product = r.message.find(
          element => element.name === frm.doc.name + '-' + frm.doc.cnp_product,
        );

        if (product) {
          const id = frm.doc.name.replace(/\s+/g, '');

          frm.set_df_property(
            'cnp_pricing_policy_html',
            'options',
            frappe.render_template('pricing_policy_table', {
              doc: product,
              id,
              status: frm.doc.docstatus,
              workflow_state: frm.doc.workflow_state,
            }),
          );
          frm.refresh_field('cnp_pricing_policy_html');
          $('.' + id + '-edit-button').on('click', () => {
            const check = 1;
            fillPricingPolicy(frm, check);
          });
        }
      },
    });
  }
}

function loadDirectorAndSignatory(frm) {
  $(frm.fields_dict.cnp_director_details.wrapper).empty();
  $(frm.fields_dict.cnp_signatory_deatils.wrapper).empty();

  const shareholdings = frm.doc.cnp_director_shareholdings
    ? JSON.parse(frm.doc.cnp_director_shareholdings)
    : [];
  const signatories = frm.doc.cnp_authorized_signatories
    ? JSON.parse(frm.doc.cnp_authorized_signatories)
    : [];
  frm.set_df_property(
    'cnp_director_details',
    'options',
    frappe.render_template('director_and_signatory', {
      doc: {
        name: 'director_details',
        payload: shareholdings,
        id: frm.doc.name,
      },
    }),
  );
  frm.refresh_field('cnp_director_details');
  frm.set_df_property(
    'cnp_signatory_deatils',
    'options',
    frappe.render_template('director_and_signatory', {
      doc: {
        name: 'signatory_details',
        payload: signatories,
        id: frm.doc.name,
      },
    }),
  );
  frm.refresh_field('cnp_signatory_deatils');
  setTimeout(() => {
    addDirector(frm);
    editDirector(frm);
    removeDirector(frm);

    addSignatory(frm);
    editSignatory(frm);
    removeSignatory(frm);
  }, 100);
}

function addFetchButton(frm) {
  // if (frm.doc.__islocal) {
  //   frm.set_df_property('cnp_fetch_base_details', 'hidden', 1);
  // }
  // Set Custom Button To Fetch Comprehensive details
  if (frm.doc.cin_number) {
    frappe.call({
      method: 'frappe.client.get_list',
      args: {
        doctype: 'CPRB Company Details',
        filters: {
          name: frm.doc.cin_number,
        },
        limit: 1,
      },
      callback: function (r) {
        if (r.message && r.message.length > 0) {
          frm.set_df_property('cnp_fetch_base_details', 'hidden', 1);
          frm.add_custom_button(__('Update Details From Probe42'), function () {
            if (checkRequiredFields(frm)) {
              frappe.call({
                freeze: true,
                method:
                  'trusteeship_platform.custom_methods.get_probe_42_comprehensive_details_for_opportunity',
                args: {
                  opportunity_name: frm.doc.name,
                  source: frm.doc.source,
                  is_local:
                    frm.doc.__islocal !== undefined ? frm.doc.__islocal : 0,
                  cnp_type: frm.doc.cnp_type,
                  opportunity_from: frm.doc.opportunity_from,
                  party_name: frm.doc.party_name,
                  cin_number: frm.doc.cin_number,
                  product: frm.doc.cnp_product,
                  facility_amt: frm.doc.cnp_facility_amt,
                  issue_size: frm.doc.cnp_issue_size,
                  tenor_years: frm.doc.cnp_tenor_years,
                  tenor_months: frm.doc.cnp_tenor_months,
                  tenor_days: frm.doc.cnp_tenor_days,
                  security_type: frm.doc.cnp_security_type.length
                    ? frm.doc.cnp_security_type
                    : [],
                },
                callback: r => {
                  if (frm.doc.__islocal) {
                    frappe.set_route('Form', 'Opportunity', r.message.name);
                  }
                  frappe.show_alert(
                    {
                      message: 'Fetched successfully.',
                      indicator: 'green',
                    },
                    5,
                  );

                  frm.reload_doc();
                },
              });
            }
          });
        }
      },
    });
  }
  // if (
  //   !frm.doc.cnp_is_probe42_data_fetched &&
  //   (!frappe.user.has_role('BCG Head') ||
  //     frappe.session.user === 'Administrator')
  // ) {
  //   if (frm.doc.__islocal) {
  //     frm.add_custom_button('Fetch via Probe42 and Save', () => {
  //       fetchOpportunityFromProbe42(frm);
  //     });
  //   } else {
  //     frm.add_custom_button('Fetch via Probe42', () => {
  //       fetchOpportunityFromProbe42(frm);
  //     });
  //   }
  // }
}

function checkRequiredFields(frm) {
  if (
    !frm.doc.opportunity_from ||
    !frm.doc.party_name ||
    !frm.doc.cnp_product
  ) {
    frappe.msgprint({
      message: 'Required fields must be filled in.',
      indicator: 'red',
      title: 'Missing Fields',
    });
    return false;
  } else if (!frm.doc.cin_number) {
    frappe.msgprint({
      message: 'CIN Number is mandatory for fetching.',
      indicator: 'red',
      title: 'Missing Fields',
    });
    return false;
  } else if (
    (frm.doc.cnp_product.toUpperCase() === 'DTE' ||
      frm.doc.cnp_product.toUpperCase() === 'SA' ||
      frm.doc.cnp_product.toUpperCase() === 'STE') &&
    !frm.doc.cnp_security_type[0]?.type_of_security
  ) {
    frappe.msgprint({
      message: 'Security Type is mandatory for selected product.',
      indicator: 'red',
      title: 'Missing Fields',
    });
    return false;
  } else if (!frm.doc.cnp_facility_amt) {
    frappe.msgprint({
      message: 'Facility Amount is mandatory for selected product.',
      indicator: 'red',
      title: 'Missing Fields',
    });
    return false;
  } else {
    return true;
  }
}

function setSecurityTypeReqd(frm) {
  if (
    frm.doc.cnp_product?.toUpperCase() === 'DTE' ||
    frm.doc.cnp_product?.toUpperCase() === 'SA' ||
    frm.doc.cnp_product?.toUpperCase() === 'STE'
  ) {
    if (
      frm.doc.cnp_product?.toUpperCase() === 'DTE' &&
      frm.doc.custom_type_of_product === 'Secured'
    ) {
      frm.toggle_reqd('cnp_security_type', true);
    } else if (
      frm.doc.cnp_product?.toUpperCase() === 'DTE' &&
      frm.doc.custom_type_of_product === 'Unsecured'
    ) {
      frm.toggle_reqd('cnp_security_type', false);
    } else {
      frm.toggle_reqd('cnp_security_type', true);
    }
  } else {
    frm.toggle_reqd('cnp_security_type', false);
  }
}

function addDirector(frm) {
  $('.' + frm.doc.name + '-director-add').unbind();
  $('.' + frm.doc.name + '-director-add').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Director Info',
      fields: [
        {
          label: 'Name',
          fieldname: 'full_name',
          fieldtype: 'Data',
          reqd: true,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        const directorShareholdings = frm.doc.cnp_director_shareholdings
          ? JSON.parse(frm.doc.cnp_director_shareholdings)
          : [];
        directorShareholdings.push({
          full_name: values.full_name,
          designation: values.designation,
        });

        frm.doc.cnp_director_shareholdings = JSON.stringify(
          directorShareholdings,
        );
        frm.dirty();
        frm.refresh();
        d.hide();
        frm.scroll_to_field('cnp_signatory_deatils');
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function editDirector(frm) {
  $('.' + frm.doc.name + '-director-edit').unbind();
  $('.' + frm.doc.name + '-director-edit').on('click', function () {
    const index = $('.' + frm.doc.name + '-director-edit').index(this);
    const shareholdings = JSON.parse(frm.doc.cnp_director_shareholdings);
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Name',
          fieldname: 'full_name',
          fieldtype: 'Data',
          reqd: true,
          default: shareholdings[index].full_name,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
          default: shareholdings[index].designation,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        shareholdings[index].full_name = values.full_name;
        shareholdings[index].designation = values.designation;
        frm.doc.cnp_director_shareholdings = JSON.stringify(shareholdings);

        frm.dirty();
        frm.refresh();
        d.hide();
        frm.scroll_to_field('cnp_signatory_deatils');
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function removeDirector(frm) {
  $('.' + frm.doc.name + '-director-remove').unbind();
  $('.' + frm.doc.name + '-director-remove').on('click', function () {
    const directors = JSON.parse(frm.doc.cnp_director_shareholdings);
    const index = $('.' + frm.doc.name + '-director-remove').index(this);
    directors.splice(index, 1);
    frm.doc.cnp_director_shareholdings = JSON.stringify(directors);
    frm.dirty();
    frm.refresh();
    frm.scroll_to_field('cnp_signatory_deatils');
  });
}

function addSignatory(frm) {
  $('.' + frm.doc.name + '-signatory-add').unbind();
  $('.' + frm.doc.name + '-signatory-add').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Signatory Info',
      fields: [
        {
          label: 'Name',
          fieldname: 'name',
          fieldtype: 'Data',
          reqd: true,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        const signatoryInfo = frm.doc.cnp_authorized_signatories
          ? JSON.parse(frm.doc.cnp_authorized_signatories)
          : [];
        signatoryInfo.push({
          name: values.name,
          designation: values.designation,
        });

        frm.doc.cnp_authorized_signatories = JSON.stringify(signatoryInfo);
        frm.dirty();
        frm.refresh();
        d.hide();
        frm.scroll_to_field('cnp_signatory_deatils');
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function editSignatory(frm) {
  $('.' + frm.doc.name + '-signatory-edit').unbind();
  $('.' + frm.doc.name + '-signatory-edit').on('click', function () {
    const index = $('.' + frm.doc.name + '-signatory-edit').index(this);
    const signatory = JSON.parse(frm.doc.cnp_authorized_signatories);
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Name',
          fieldname: 'name',
          reqd: true,
          fieldtype: 'Data',
          default: signatory[index].name,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
          default: signatory[index].designation,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        signatory[index].name = values.name;
        signatory[index].designation = values.designation;
        frm.doc.cnp_authorized_signatories = JSON.stringify(signatory);
        frm.dirty();
        frm.refresh();
        d.hide();
        frm.scroll_to_field('cnp_signatory_deatils');
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function removeSignatory(frm) {
  $('.' + frm.doc.name + '-signatory-remove').unbind();
  $('.' + frm.doc.name + '-signatory-remove').on('click', function () {
    const index = $('.' + frm.doc.name + '-signatory-remove').index(this);
    const signatories = JSON.parse(frm.doc.cnp_authorized_signatories);
    signatories.splice(index, 1);
    frm.doc.cnp_authorized_signatories = JSON.stringify(signatories);

    frm.dirty();
    frm.refresh();
    frm.scroll_to_field('cnp_signatory_deatils');
  });
}

function loadCrRatingTable(frm) {
  $(frm.fields_dict.cnp_cr_rating_html.wrapper).empty();
  frm.set_df_property(
    'cnp_cr_rating_html',
    'options',
    frappe.render_template('cr_rating_table', {
      doc: frm.doc.cnp_cr_rating ? frm.doc.cnp_cr_rating : [],
    }),
  );
  frm.refresh_field('cnp_cr_rating_html');
  setTimeout(() => {
    addCrTableRow(frm);
    editCrTableRow(frm);
    removeCrTableRow(frm);
  }, 100);
}

function addCrTableRow(frm) {
  $('.cr-download').unbind();
  $('.cr-download').on('click', function () {
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.public.py.Opportunity.downloadCsvCrinfo',
      docname: frm.doc.name,
    });
  });
  $('.cr-add-row').unbind();
  $('.cr-add-row').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Rating Agency',
          fieldname: 'rating_agency',
          fieldtype: 'Data',
        },

        {
          label: 'Rating Date',
          fieldname: 'rating_date',
          fieldtype: 'Date',
        },
        {
          label: 'Current Rating',
          fieldname: 'current_rating',
          fieldtype: 'Data',
        },
        {
          label: 'Type of loan',
          fieldname: 'type_of_loan',
          fieldtype: 'Data',
        },
        {
          label: 'Currency',
          fieldname: 'currency',
          fieldtype: 'Data',
        },
        {
          label: 'Amount',
          fieldname: 'amount',
          fieldtype: 'Int',
        },
        {
          label: 'Status',
          fieldname: 'status',
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        if (frm.doc.__islocal) {
          const row = frm.add_child('cnp_cr_rating');
          row.rating_agency = values.rating_agency ? values.rating_agency : '';
          row.rating_date = values.rating_date ? values.rating_date : null;
          row.current_rating = values.current_rating
            ? values.current_rating
            : '';
          row.type_of_loan = values.type_of_loan ? values.type_of_loan : '';
          row.currency = values.currency ? values.currency : '';
          row.amount = values.amount ? values.amount : null;
          row.status = values.status ? values.status : '';
          d.hide();
          frm.dirty();
          frm.refresh();
          setTimeout(() => {
            frm.fields_dict.cnp_cr_rating_section.collapse(1);
            frm.scroll_to_field('cnp_cr_rating_html');
          }, 100);
        } else {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.cr_table_events',
            args: {
              docname: frm.doc.name,
              values,
              action: 'Add',
              index: null,
            },
            freeze: true,
            callback: r => {
              d.hide();
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.fields_dict.cnp_cr_rating_section.collapse(false);
                  frm.scroll_to_field('cnp_cr_rating_html');
                  frappe.show_alert(
                    {
                      message: 'Saved',
                      indicator: 'green',
                    },
                    5,
                  );
                }, 30);
              });
            },
          });
        }
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });

    d.show();
  });
}

function editCrTableRow(frm) {
  $('.cr-edit-row').unbind();
  $('.cr-edit-row').on('click', function () {
    const index = $('.cr-edit-row').index(this);
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Rating Agency',
          fieldname: 'rating_agency',
          fieldtype: 'Data',
          default: frm.doc.cnp_cr_rating[index].rating_agency
            ? frm.doc.cnp_cr_rating[index].rating_agency
            : '',
        },

        {
          label: 'Rating Date',
          fieldname: 'rating_date',
          fieldtype: 'Date',
          default: frm.doc.cnp_cr_rating[index].rating_date
            ? frm.doc.cnp_cr_rating[index].rating_date
            : '',
        },
        {
          label: 'Current Rating',
          fieldname: 'current_rating',
          fieldtype: 'Data',
          default: frm.doc.cnp_cr_rating[index].current_rating
            ? frm.doc.cnp_cr_rating[index].current_rating
            : '',
        },
        {
          label: 'Type of loan',
          fieldname: 'type_of_loan',
          fieldtype: 'Data',
          default: frm.doc.cnp_cr_rating[index].type_of_loan
            ? frm.doc.cnp_cr_rating[index].type_of_loan
            : '',
        },
        {
          label: 'Currency',
          fieldname: 'currency',
          fieldtype: 'Data',
          default: frm.doc.cnp_cr_rating[index].currency
            ? frm.doc.cnp_cr_rating[index].currency
            : '',
        },
        {
          label: 'Amount',
          fieldname: 'amount',
          fieldtype: 'Int',
          default: frm.doc.cnp_cr_rating[index].amount
            ? frm.doc.cnp_cr_rating[index].amount
            : '',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frm.doc.cnp_cr_rating[index].rating_agency = values.rating_agency;
        frm.doc.cnp_cr_rating[index].rating_date = values.rating_date;
        frm.doc.cnp_cr_rating[index].current_rating = values.current_rating;
        frm.doc.cnp_cr_rating[index].type_of_loan = values.type_of_loan;
        frm.doc.cnp_cr_rating[index].currency = values.currency;
        frm.doc.cnp_cr_rating[index].amount = values.amount;

        if (frm.doc.__islocal) {
          d.hide();
          frm.dirty();
          frm.refresh();
          setTimeout(() => {
            frm.fields_dict.cnp_cr_rating_section.collapse(1);
            frm.scroll_to_field('cnp_cr_rating_html');
          }, 100);
        } else {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.cr_table_events',
            args: {
              docname: frm.doc.name,
              values,
              action: 'Edit',
              index,
            },
            freeze: true,
            callback: r => {
              d.hide();
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.fields_dict.cnp_cr_rating_section.collapse(false);
                  frm.scroll_to_field('cnp_cr_rating_html');
                  frappe.show_alert(
                    {
                      message: 'Saved',
                      indicator: 'green',
                    },
                    5,
                  );
                }, 30);
              });
            },
          });
        }
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });

    d.show();
  });
}

function removeCrTableRow(frm) {
  $('.cr-remove-row').unbind();
  $('.cr-remove-row').on('click', function () {
    const index = $('.cr-remove-row  ').index(this);

    if (frm.doc.__islocal) {
      frm.doc.cnp_cr_rating.splice(index, 1);

      frm.dirty();
      frm.refresh();
      setTimeout(() => {
        frm.fields_dict.cnp_cr_rating_section.collapse(false);
        frm.scroll_to_field('cnp_cr_rating_html');
      }, 100);
    } else {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.cr_table_events',
        args: {
          docname: frm.doc.name,
          values: 'none',
          action: 'Delete',
          index,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.fields_dict.cnp_cr_rating_section.collapse(false);
              frm.scroll_to_field('cnp_cr_rating_html');
              frappe.show_alert(
                {
                  message: 'Saved',
                  indicator: 'green',
                },
                5,
              );
            }, 30);
          });
        },
      });
    }
  });
}

function loadFininfoTable(frm) {
  $(frm.fields_dict.cnp_fininfo_html.wrapper).empty();
  frm.set_df_property(
    'cnp_fininfo_html',
    'options',
    frappe.render_template('fininfo_table', {
      doc: frm.doc.cnp_fininfo ? frm.doc.cnp_fininfo : [],
    }),
  );
  frm.refresh_field('cnp_fininfo_html');
  setTimeout(() => {
    addFininfoTableRow(frm);
    editFininfoTableRow(frm);
    removeFininfoTableRow(frm);
  }, 100);
}

function addFininfoTableRow(frm) {
  $('.fininfo-download').unbind();
  $('.fininfo-download').on('click', function () {
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.public.py.Opportunity.downloadCsvFininfo',
      docname: frm.doc.name,
    });
  });
  $('.fininfo-add-row').unbind();
  $('.fininfo-add-row').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add',
      fields: [
        {
          label: 'Net Revenue',
          fieldname: 'net_revenue',
          fieldtype: 'Data',
        },

        {
          label: 'Operating Profit ( EBITDA )',
          fieldname: 'operating_profit',
          fieldtype: 'Data',
        },
        {
          label: 'Profit from Discontinuing Operation After Tax',
          fieldname: 'profit_from_discontinuing',
          fieldtype: 'Data',
        },
        {
          label: 'Debt / Equity',
          fieldname: 'debt_equity',
          fieldtype: 'Data',
        },
        {
          label: 'Debt Securities',
          fieldname: 'debt_securities',
          fieldtype: 'Data',
        },
        {
          label: 'Loans',
          fieldname: 'loans',
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        if (frm.doc.__islocal) {
          const row = frm.add_child('cnp_fininfo');
          row.net_revenue = values.net_revenue ? values.net_revenue : '';
          row.operating_profit = values.operating_profit
            ? values.operating_profit
            : '';
          row.profit_from_discontinuing = values.profit_from_discontinuing
            ? values.profit_from_discontinuing
            : '';
          row.debt_equity = values.debt_equity ? values.debt_equity : '';
          row.debt_securities = values.debt_securities
            ? values.debt_securities
            : '';
          row.loans = values.loans ? values.loans : '';
          d.hide();
          frm.dirty();
          frm.refresh();
          setTimeout(() => {
            frm.fields_dict.cnp_fininfo_section.collapse(false);
            frm.scroll_to_field('cnp_fininfo_html');
          }, 100);
        } else {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.finifo_events',
            args: {
              docname: frm.doc.name,
              values,
              action: 'Add',
              index: null,
            },
            freeze: true,
            callback: r => {
              d.hide();
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.fields_dict.cnp_fininfo_section.collapse(false);
                  frm.scroll_to_field('cnp_fininfo_html');
                  frappe.show_alert(
                    {
                      message: 'Saved',
                      indicator: 'green',
                    },
                    5,
                  );
                }, 100);
              });
            },
          });
        }
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function editFininfoTableRow(frm) {
  $('.fininfo-edit-row').unbind();
  $('.fininfo-edit-row').on('click', function () {
    const index = $('.fininfo-edit-row').index(this);
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Net Revenue',
          fieldname: 'net_revenue',
          fieldtype: 'Data',
          default: frm.doc.cnp_fininfo[index].net_revenue
            ? frm.doc.cnp_fininfo[index].net_revenue
            : '',
        },

        {
          label: 'Operating Profit ( EBITDA )',
          fieldname: 'operating_profit',
          fieldtype: 'Data',
          default: frm.doc.cnp_fininfo[index].operating_profit
            ? frm.doc.cnp_fininfo[index].operating_profit
            : '',
        },
        {
          label: 'Profit from Discontinuing Operation After Tax',
          fieldname: 'profit_from_discontinuing',
          fieldtype: 'Data',
          default: frm.doc.cnp_fininfo[index].profit_from_discontinuing
            ? frm.doc.cnp_fininfo[index].profit_from_discontinuing
            : '',
        },
        {
          label: 'Debt / Equity',
          fieldname: 'debt_equity',
          fieldtype: 'Data',
          default: frm.doc.cnp_fininfo[index].debt_equity
            ? frm.doc.cnp_fininfo[index].debt_equity
            : '',
        },
        {
          label: 'Debt Securities',
          fieldname: 'debt_securities',
          fieldtype: 'Data',
          default: frm.doc.cnp_fininfo[index].debt_securities
            ? frm.doc.cnp_fininfo[index].debt_securities
            : '',
        },
        {
          label: 'Loans',
          fieldname: 'loans',
          fieldtype: 'Data',
          default: frm.doc.cnp_fininfo[index].loans
            ? frm.doc.cnp_fininfo[index].loans
            : '',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frm.doc.cnp_fininfo[index].net_revenue = values.net_revenue;
        frm.doc.cnp_fininfo[index].operating_profit = values.operating_profit;
        frm.doc.cnp_fininfo[index].profit_from_discontinuing =
          values.profit_from_discontinuing;
        frm.doc.cnp_fininfo[index].debt_equity = values.debt_equity;
        frm.doc.cnp_fininfo[index].debt_securities = values.debt_securities;
        frm.doc.cnp_fininfo[index].loans = values.loans;
        if (frm.doc.__islocal) {
          d.hide();
          frm.dirty();
          setTimeout(() => {
            frm.fields_dict.cnp_fininfo_section.collapse(false);
            frm.scroll_to_field('cnp_fininfo_html');
          }, 100);
        } else {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.finifo_events',
            args: {
              docname: frm.doc.name,
              values,
              action: 'Edit',
              index,
            },
            freeze: true,
            callback: r => {
              d.hide();
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.fields_dict.cnp_fininfo_section.collapse(false);
                  frm.scroll_to_field('cnp_fininfo_html');
                  frappe.show_alert(
                    {
                      message: 'Saved',
                      indicator: 'green',
                    },
                    5,
                  );
                }, 100);
              });
            },
          });
        }
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });

    d.show();
  });
}

function removeFininfoTableRow(frm) {
  $('.fininfo-remove-row').unbind();
  $('.fininfo-remove-row').on('click', function () {
    const index = $('.fininfo-remove-row  ').index(this);
    if (frm.doc.__islocal) {
      frm.doc.cnp_fininfo.splice(index, 1);
      frm.dirty();
      frm.refresh();
      setTimeout(() => {
        frm.fields_dict.cnp_fininfo_section.collapse(false);
        frm.scroll_to_field('cnp_fininfo_html');
      }, 100);
    } else {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.finifo_events',
        args: {
          docname: frm.doc.name,
          values: 'none',
          action: 'Delete',
          index,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.fields_dict.cnp_fininfo_section.collapse(false);
              frm.scroll_to_field('cnp_fininfo_html');
              frappe.show_alert(
                {
                  message: 'Saved',
                  indicator: 'green',
                },
                5,
              );
            }, 100);
          });
        },
      });
    }
  });
}

function saveDirectorSignatoryInCustomer(frm) {
  frappe.call({
    method:
      'trusteeship_platform.custom_methods.save_director_signatory_in_customer',
    args: {
      director: frm.doc.cnp_director_shareholdings
        ? frm.doc.cnp_director_shareholdings
        : '',
      signatory: frm.doc.cnp_authorized_signatories
        ? frm.doc.cnp_authorized_signatories
        : '',
      customer: frm.doc.party_name,
      cin_number: frm.doc.cin_number,
    },
    freeze: true,
    callback: r => {},
  });
}

function fillPricingPolicy(frm, check = 0) {
  if (!frm.doc.__islocal) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Canopi Pricing Policy',
        doc_name: frm.doc.name + '-' + frm.doc.cnp_product,
      },
      callback: r => {
        const policyDoc = r.message;
        if (
          r.message.opportunity === frm.doc.name &&
          !r.message.is_saved_by_user &&
          check === 0
        ) {
          policyDialog(frm, policyDoc, check);
        } else if (check === 1) {
          policyDialog(frm, policyDoc, check);
        }
      },
    });
  }
}

function policyDialog(frm, policyDoc, check) {
  const fields = [];
  if (policyDoc.no_of_securities_check === 1) {
    fields.push({
      label: 'No. of Securities',
      fieldname: 'no_of_securities',
      fieldtype: 'Int',
      default: policyDoc.no_of_securities,
      reqd: frm.doc.custom_type_of_product === 'Secured' ? 1 : 0,
    });
  }
  if (policyDoc.immovable_properties_check === 1) {
    fields.push({
      label: 'Immovable properties',
      fieldname: 'immovable_properties',
      fieldtype: 'Int',
      default: policyDoc.immovable_properties,
    });
  }
  if (policyDoc.states_immovable_properties_check === 1) {
    fields.push({
      label: 'States Immovable Properties',
      fieldname: 'states_immovable_properties',
      fieldtype: 'Int',
      default: policyDoc.states_immovable_properties,
    });
  }
  if (policyDoc.no_of_lenders_check === 1) {
    fields.push({
      label: 'No. of Lenders',
      fieldname: 'no_of_lenders',
      fieldtype: 'Int',
      default: policyDoc.no_of_lenders,
    });
  }
  if (policyDoc.no_of_sellers_check === 1) {
    fields.push({
      fieldname: 'no_of_sellers',
      fieldtype: 'Int',
      label: 'No. of sellers',
      default: policyDoc.no_of_sellers,
    });
  }
  if (policyDoc.assets_check === 1) {
    fields.push({
      fieldname: 'assets',
      fieldtype: 'Int',
      label: 'Assets',
      default: policyDoc.assets,
    });
  }
  if (policyDoc.whether_public_issue_check === 1) {
    fields.push({
      default: policyDoc.whether_public_issue,
      fieldname: 'whether_public_issue',
      fieldtype: 'Select',
      label: 'Whether Public issue',
      options: 'Yes\nNo',
    });
  }
  if (policyDoc.listed_yes_no_check === 1) {
    fields.push({
      default: policyDoc.listed_yes_no,
      fieldname: 'listed_yes_no',
      fieldtype: 'Select',
      label: 'Listed (Yes/No)',
      options: 'Yes\nNo',
    });
  }
  if (policyDoc.fund_category_check === 1) {
    fields.push({
      fieldname: 'fund_category',
      fieldtype: 'Select',
      label: 'Fund category (I/II/III)',
      options: '\nI\nII\nIII',
      default: policyDoc.fund_category,
    });
  }
  if (policyDoc.da_yes_no_check === 1) {
    fields.push({
      fieldname: 'da_yes_no',
      fieldtype: 'Select',
      label: 'DA (Yes/No)',
      options: 'Yes\nNo',
      default: policyDoc.da_yes_no,
    });
  }
  if (policyDoc.payment_frequency_mqh_check === 1) {
    fields.push({
      fieldname: 'payment_frequency',
      fieldtype: 'Select',
      label: 'Payment frequency (M/Q/H)',
      options: '\nM\nQ\nH',
      default: policyDoc.payment_frequency,
    });
  }
  if (policyDoc.fund_size_in_rscr_check === 1) {
    fields.push({
      fieldname: 'fund_size_in_rs_cr',
      fieldtype: 'Long Text',
      label: 'Fund size in Rs/Cr',
      read_only: 1,
      default: frm.doc.cnp_facility_amt,
    });
  }
  if (policyDoc.value_added_services_check === 1) {
    fields.push({
      label: 'Value Added Services',
      fieldname: 'value_added_services',
      fieldtype: 'Int',
      default: policyDoc.value_added_services,
      read_only: 1,
    });
  }
  if (!check) {
    const d = new frappe.ui.Dialog({
      title: 'Fill Pricing Policy',
      static: true,
      fields,
      primary_action: function () {
        dialogData(frm, d, policyDoc);
      },
      primary_action_label: __('Update'),
    });
    d.show();
  } else {
    const d = new frappe.ui.Dialog({
      title: 'Fill Pricing Policy',
      fields,
      primary_action: function () {
        dialogData(frm, d, policyDoc);
      },
      primary_action_label: __('Update'),
      secondary_action_label: 'Close',
      secondary_action: function () {
        d.hide();
      },
    });
    d.show();
  }
}

function dialogData(frm, d, policyDoc) {
  const data = d.get_values();
  if (
    frm.doc.custom_type_of_product === 'Secured' &&
    data.no_of_securities <= 0
  ) {
    frappe.throw('No. of Securities should be non-zero');
  }
  frappe.call({
    method: 'trusteeship_platform.public.py.Opportunity.saveDoc',
    args: {
      data,
      policyDocName: policyDoc.name,
      doctype: 'Canopi Pricing Policy',
    },
    freeze: true,
    callback: function (r) {
      frappe.call({
        method:
          'trusteeship_platform.custom_methods.cal_opportunity_product_fee',
        args: {
          doc_name: frm.doc.name,
          product: frm.doc.cnp_product,
        },
        freeze: true,
        callback: function (r) {
          frm.reload_doc();
        },
      });
      d.hide();
      location.reload();
    },
  });
}

function disableProduct(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'Quotation',
      filters: [['cnp_opportunity', 'in', frm.doc.name]],
    },
    callback: r => {
      if (r.message.length !== 0) {
        frm.set_df_property('cnp_product', 'read_only', true);
      }
    },
  });
}

function setLabelReferencNumber(frm) {
  if (frm.doc.cnp_type === 'Others') {
    frm.toggle_reqd('cin_number', false);
  } else {
    frm.toggle_reqd('cin_number', true);
  }
  if (
    frm.doc.cnp_type === 'Individual' ||
    frm.doc.cnp_type === 'Partnership Firm'
  ) {
    frm.set_df_property('cin_number', 'label', 'Reference Number (PAN)');
    frm.toggle_reqd('lead_name', true);
    frm.toggle_reqd('company_name', false);
  } else if (frm.doc.cnp_type === 'Organization') {
    frm.set_df_property('cin_number', 'label', 'Reference Number (CIN)');
    frm.toggle_reqd('company_name', true);
    frm.toggle_reqd('lead_name', false);
  } else if (frm.doc.cnp_type === 'LLP') {
    frm.set_df_property('cin_number', 'label', 'Reference Number (LLP)');
    frm.toggle_reqd('company_name', true);
    frm.toggle_reqd('lead_name', false);
  } else if (
    frm.doc.cnp_type === 'Trustee' ||
    frm.doc.cnp_type === 'Foreign' ||
    frm.doc.cnp_type === 'Others'
  ) {
    frm.set_df_property(
      'cin_number',
      'label',
      'Reference Number (CIN/PAN/Others)',
    );
    frm.toggle_reqd('lead_name', true);
    frm.toggle_reqd('company_name', false);
  } else if (frm.doc.cnp_type === '') {
    frm.set_df_property('cin_number', 'label', 'Reference Number ');
    frm.toggle_reqd('lead_name', false);
    frm.toggle_reqd('company_name', false);
  }
}

function validateReferenceNumber(frm) {
  if (frm.doc.cin_number) {
    const patternCIN = /[A-Z0-9]{21}/;
    const patternPAN = /[A-Z]{5}[0-9]{4}[A-Z]{1}/;
    const patternLLP = /[A-Z]{3}[0-9]{4}/;
    const cin = frm.doc.cin_number.toUpperCase();
    if (frm.doc.cnp_type === 'Organization') {
      if (cin.match(patternCIN) && cin.length === 21) {
      } else {
        frappe.throw('Invalid CIN number');
      }
    } else if (frm.doc.cnp_type === 'LLP') {
      if (cin.match(patternLLP) && cin.length === 7) {
      } else {
        frappe.throw('Invalid LLP number');
      }
    } else if (
      frm.doc.cnp_type === 'Individual' ||
      frm.doc.cnp_type === 'Partnership Firm'
    ) {
      if (cin.match(patternPAN) && cin.length === 10) {
      } else {
        frappe.throw('Invalid PAN number');
      }
    } else {
      if (
        (cin.match(patternCIN) && cin.length === 21) ||
        (cin.match(patternPAN) && cin.length === 10)
      ) {
      } else {
        frappe.throw('Invalid CIN/PAN number');
      }
    }
    frm.doc.cin_number = cin;
    frm.refresh_field('cin_number');
  }
}

function addOperationsFlowBtnInConnections(frm) {
  $("button[data-doctype='ATSL Mandate List']").parent().parent().hide();
  if (frm.doc.workflow_state === 'Approved') {
    $("button[data-doctype='ATSL Mandate List']").parent().parent().show();
    $("button[data-doctype='ATSL Mandate List']").unbind();
    $("button[data-doctype='ATSL Mandate List']").on('click', function () {
      frappe.model.open_mapped_doc({
        method:
          'trusteeship_platform.custom_methods.make_operations_flow_for_opportunity',
        frm,
      });
    });
  }
}

function hideAddOperationsFlowBtn(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'ATSL Mandate List',
      filters: [['opportunity', '=', frm.doc.name]],
    },
    freeze: true,
    callback: r => {
      if (r.message.length > 0) {
        $("button[data-doctype='ATSL Mandate List']").hide();
      } else {
        $("button[data-doctype='ATSL Mandate List']").show();
      }
    },
  });
}

function hideChildTables(frm) {
  frm.set_df_property('cnp_cr_rating', 'hidden', 1);
  frm.set_df_property('cnp_fininfo', 'hidden', 1);
}

function typeOfSecurityExpanded(frm) {
  if (frm.doc.__islocal) {
    if (
      frm.doc.cnp_product?.toUpperCase() === 'DTE' ||
      frm.doc.cnp_product?.toUpperCase() === 'STE'
    ) {
      frm.fields_dict.cnp_security_section_break.collapse(false);
    }
  }
}

function removeCustomButton(frm) {
  frm.remove_custom_button('Supplier Quotation', 'Create');
  frm.remove_custom_button('Request For Quotation', 'Create');
  frm.remove_custom_button('Customer', 'Create');

  if (
    frappe.user.has_role('Ops & Serv Maker') &&
    frappe.session.user !== 'Administrator'
  ) {
    frm.remove_custom_button('Quotation', 'Create');
    $("button[data-doctype='Quotation']").hide();
  }
}

function initiateOpsFLowButton(frm) {
  if (
    !frm.doc.__islocal &&
    frm.doc.workflow_state === 'Draft' &&
    frappe.user.has_role('BCG RM')
  ) {
    frm.add_custom_button('Initiate Ops Flow', () => {
      sendApprovalMail(frm);
    });
  }
}
function initiateOpsFLowMessage(frm) {
  if (frm.doc.workflow_state === 'Pending for Approval') {
    frm.set_intro(' ');
    frm.set_intro(
      'Request to initiate Operations Flow has been sent. It will be initiated after approval.',
    );
  }
}
function emailAction(frm) {
  if (frm.doc.workflow_state === 'Pending for Approval' && !frm.doc.__islocal) {
    frm.page.add_action_item('Approve', function () {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.email_action_opportunity',
        args: {
          doctype: 'Opportunity',
          docname: frm.doc.name,
          action: 'Approved',
          redirect: false,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc();
        },
      });
    });
    frm.page.add_action_item('Reject', function () {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.email_action_opportunity',
        args: {
          doctype: 'Opportunity',
          docname: frm.doc.name,
          action: 'Rejected',
          redirect: false,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc();
        },
      });
    });
  }
}

function sendApprovalMail(frm) {
  if (frm.doc.cnp_send_for_approval === 0) {
    frappe.call({
      method:
        'trusteeship_platform.custom_methods.send_approval_mail_opportunity',
      args: {
        doc_name: frm.doc.name,
        doc_type: 'Opportunity',
        action: 'Pending for Approval',
      },
      freeze: true,
      callback: r => {
        frm.reload_doc();
      },
    });
  }
}

function addRejectionComment(frm) {
  if (frm.doc.cnp_is_reject_reason_commented === 1) commentDialog(frm);
}

function commentDialog(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Reason to Reject',
    static: true,
    fields: [
      {
        fieldname: 'reason_to_reject',
        fieldtype: 'Text',
        label: 'Comment',
        reqd: 1,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      const reasonRoReject =
        '<b>Reason to reject:</b> ' + data.reason_to_reject;

      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_rejection_comment',
        args: {
          doctype: 'Opportunity',
          docname: frm.doc.name,
          comment: reasonRoReject,
        },
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.reload_doc();
          }
        },
      });
    },
  });
  d.show();
}

function addCnpApprovalStatusIndicator(frm) {
  if (document.getElementById('cnp-approval-status')) {
    document.getElementById('cnp-approval-status').remove();
  }
  if (!frm.doc.__islocal) {
    let titleElement = $(
      `div.flex h3[title="${frm.doc.title.trim()}"]`,
    ).parent();
    titleElement = titleElement[0];

    let pillColor = 'grey';
    switch (frm.doc.workflow_state) {
      case 'Draft':
        pillColor = 'blue';
        break;
      case 'Pending for Approval':
        pillColor = 'orange';
        break;
      case 'Approved':
        pillColor = 'green';
        break;
      case 'Rejected':
        pillColor = 'red';
        break;
    }

    const cnpStatusSpan = document.createElement('span');
    cnpStatusSpan.setAttribute('id', 'cnp-approval-status');
    cnpStatusSpan.innerHTML = frm.doc.workflow_state;
    cnpStatusSpan.classList.add(
      'indicator-pill',
      'whitespace-nowrap',
      pillColor,
    );
    cnpStatusSpan.style.marginLeft = '8px';
    cnpStatusSpan.classList.add(
      'indicator-pill',
      'whitespace-nowrap',
      pillColor,
    );
    titleElement.appendChild(cnpStatusSpan);
  }
}

function hideActions(frm) {
  if (frm.doc.workflow_state === 'Draft') {
    if (!frm.doc.cnp_s3_key || !frm.doc.cnp_offer_acceptence_date) {
      frm.page.hide_actions_menu();
    }
  }
  if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    !frappe.user.has_role('BCG Head')
  ) {
    frm.page.hide_actions_menu();
  }
}

function disableForm(frm) {
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Rejected' ||
    frm.doc.status === 'Quotation' ||
    frm.doc.status === 'Converted'
  ) {
    frm.disable_form();
    frm.disable_save();
  }
}

function filterProduct(frm) {
  frm.set_query('cnp_product', function () {
    return {
      filters: [['is_purchase_item', '=', 0]],
    };
  });
}
