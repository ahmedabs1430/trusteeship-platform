/* global frappe */
frappe.ui.form.on('Bank', {
  refresh: frm => {
    populateForm(frm);
    setBeneficiaryBankFilter(frm);
    setCorrBankFilter(frm);
    setBeneficiaryBankAccountFilter(frm);
    setReadonlyFields(frm);
  },

  cnp_beneficiary_bank_name: frm => {
    setBeneficiarySwiftNumber(frm);
  },

  cnp_beneficiary_bank_account: frm => {
    setBeneficiaryAccountNumber(frm);
  },

  cnp_corr_bank_name: frm => {
    setCorrSwiftNumber(frm);
  },
});

function populateForm(frm) {
  if (frm.doc.__islocal && frappe.get_prev_route()[1] === 'Canopi Lender') {
    frm.set_value('cnp_lender', frappe.get_prev_route()[2]);
    frm.set_value('cnp_is_benef_corr', 0);
  }
}

function setBeneficiaryBankFilter(frm) {
  frm.set_query('cnp_beneficiary_bank_name', filter => {
    return {
      filters: [
        ['name', '!=', frm.doc.name],
        ['cnp_is_benef_corr', '=', 1],
      ],
    };
  });
}

function setCorrBankFilter(frm) {
  frm.set_query('cnp_corr_bank_name', filter => {
    return {
      filters: [
        ['name', '!=', frm.doc.name],
        ['cnp_is_benef_corr', '=', 1],
      ],
    };
  });
}

function setBeneficiaryBankAccountFilter(frm) {
  frm.set_query('cnp_beneficiary_bank_account', filter => {
    return {
      filters: [['bank', '=', frm.doc.cnp_beneficiary_bank_name]],
    };
  });
}

async function setBeneficiarySwiftNumber(frm) {
  if (frm.doc.cnp_beneficiary_bank_name) {
    const bankDoc = await getDoc('Bank', frm.doc.cnp_beneficiary_bank_name);
    frm.set_value('cnp_beneficiary_swift_number', bankDoc.swift_number);
  } else {
    frm.set_value('cnp_beneficiary_swift_number', '');
  }
  frm.set_value('cnp_beneficiary_bank_account', '');
  setReadonlyFields(frm);
}

async function setBeneficiaryAccountNumber(frm) {
  if (frm.doc.cnp_beneficiary_bank_account) {
    const bankAccountDoc = await getDoc(
      'Bank Account',
      frm.doc.cnp_beneficiary_bank_account,
    );
    frm.set_value('cnp_beneficiary_account_no', bankAccountDoc.bank_account_no);
  } else {
    frm.set_value('cnp_beneficiary_account_no', '');
  }
  setReadonlyFields(frm);
}

async function setCorrSwiftNumber(frm) {
  if (frm.doc.cnp_corr_bank_name) {
    const bankDoc = await getDoc('Bank', frm.doc.cnp_corr_bank_name);
    frm.set_value('cnp_corr_swift_number', bankDoc.swift_number);
  } else {
    frm.set_value('cnp_corr_swift_number', '');
  }
  setReadonlyFields(frm);
}

function setReadonlyFields(frm) {
  readonly(
    frm,
    'cnp_beneficiary_swift_number',
    frm.doc.cnp_beneficiary_bank_name && !frm.doc.cnp_beneficiary_swift_number
      ? 0
      : 1,
  );

  readonly(
    frm,
    'cnp_beneficiary_account_no',
    frm.doc.cnp_beneficiary_bank_account && !frm.doc.cnp_beneficiary_account_no
      ? 0
      : 1,
  );

  readonly(
    frm,
    'cnp_corr_swift_number',
    frm.doc.cnp_corr_bank_name && !frm.doc.cnp_corr_swift_number ? 0 : 1,
  );
}

function readonly(frm, field, value) {
  frm.set_df_property(field, 'read_only', value);
}

async function getDoc(doctype, docname) {
  const res = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_name: docname,
      doc_type: doctype,
    },
    freeze: true,
  });

  return res.message;
}
