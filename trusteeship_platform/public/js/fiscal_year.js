// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt

/* global frappe */
frappe.ui.form.on('Fiscal Year', {
  // refresh: function(frm) {
  // },
});

frappe.ui.form.on('Fiscal Year Company', {
  companies_add: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    row.company = '';
    frm.refresh_field('companies');
  },
});
