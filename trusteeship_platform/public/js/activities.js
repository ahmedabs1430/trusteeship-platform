/* global frappe, erpnext */
/* eslint-env jquery */

erpnext.utils.CRMActivities = class TrusteeshipCRMActivities extends (
  erpnext.utils.CRMActivities
) {
  // eslint-disable-next-line no-useless-constructor
  constructor(opts) {
    super(opts);
  }

  create_task() {
    const me = this;
    const _create_task = () => {
      const args = {
        doc: me.frm.doc,
        frm: me.frm,
        title: 'New Task',
      };
      const composer = new frappe.views.InteractionComposer(args);
      composer.dialog.get_field('interaction_type').set_value('ToDo');
      // hide column having interaction type field
      $(composer.dialog.get_field('interaction_type').wrapper)
        .closest('.form-column')
        .hide();
      // hide summary field
      $(composer.dialog.get_field('summary').wrapper)
        .closest('.form-section')
        .hide();

      // custom added
      // restrict past date selection
      composer.dialog.fields_dict.due_date.datepicker.update({
        minDate: new Date(frappe.datetime.get_today()),
      });
    };
    $('.new-task-btn').click(_create_task);
  }

  create_event() {
    const me = this;
    const _create_event = () => {
      const args = {
        doc: me.frm.doc,
        frm: me.frm,
        title: 'New Event',
      };
      const composer = new frappe.views.InteractionComposer(args);
      composer.dialog.get_field('interaction_type').set_value('Event');
      $(composer.dialog.get_field('interaction_type').wrapper).hide();

      // custom added
      // restrict past date selection
      composer.dialog.fields_dict.due_date.datepicker.update({
        minDate: new Date(frappe.datetime.get_today()),
      });
    };
    $('.new-event-btn').click(_create_event);
  }
};
