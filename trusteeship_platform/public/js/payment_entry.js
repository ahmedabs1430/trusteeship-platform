/* global frappe $ */
frappe.ui.form.on('Payment Entry', {
  refresh: function (frm) {
    addApprovalPill(frm);
    if (!frm.doc.__islocal) {
      approval(frm);
    } else if (frm.doc.__islocal && frm.doc.amended_from) {
      frm.doc.cnp_action = 0;
    }
    if (frm.doc.workflow_state === 'Rejected') {
      frm.disable_form();
      frm.disable_save();
    }
    frm.cost_center_initial_value = frm.doc.cost_center;
    setCostCenterFilter(frm);
    setTaxesFilter(frm);
    if (!frm.doc.references && frm.is_new()) {
      frm.set_value('cost_center', ' ');
      frm.refresh_field('cost_center');
    }
  },
  onload_post_render: frm => {
    setCostCenterFilter(frm);
    frm.confirmation_in_progress = false;
  },
  sales_taxes_and_charges_template: frm => {
    frm.doc.taxes = [];
  },
  cost_center(frm) {
    if (
      frm.cost_center_initial_value !== '' &&
      frm.cost_center_initial_value !== undefined
    ) {
      if (frm.doc.references && !frm.is_new()) {
        checkCostCenter(frm);
      }
    }
  },

  party(frm) {
    setPanProperty(frm);
    setPanNumber(frm);
  },

  validate(frm) {
    validatePan(frm);
  },
});

function checkCostCenter(frm) {
  if (
    frm.doc.cost_center === frm.cost_center_initial_value ||
    frm.confirmation_in_progress
  ) {
    return;
  }
  frappe.validated = false;
  frm.confirmation_in_progress = true;

  frappe.confirm(
    `Do you wish to change the Cost Center from ${frm.cost_center_initial_value} to ${frm.doc.cost_center}?`,
    function () {
      frm.cost_center_initial_value = frm.doc.cost_center;
      frappe.validated = true;
      frm.confirmation_in_progress = false;
      if (frm.doc.__islocal) {
        frm.set_value('cost_center', frm.doc.cost_center);
        frm.refresh_field('cost_center');
      } else {
        frm.save();
      }
    },
    function () {
      frm.set_value('cost_center', frm.cost_center_initial_value);
      frm.confirmation_in_progress = false;
    },
  );
}

function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Payment Entry',
      doctype_name: 'payment-entry',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function emailActionReject(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.email_action',
    args: {
      doctype: 'Payment Entry',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'payment-entry',
      redirect: false,
      email_token: frm.doc.cnp_email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function addApprovalPill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let titleElement = $(`h3[title="${frm.doc.party}"]`).parent();
    titleElement = titleElement[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.workflow_state;
    pillspan.appendChild(textspan);

    titleElement.appendChild(pillspan);
  }
}

function approval(frm) {
  if (
    frm.doc.workflow_state === 'New' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.page.add_action_item('Send For Approval', function () {
      sendApprovalMail(frm);
    });
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.page.add_action_item('Approve', function () {
      frm.doc.cnp_action = 1;
      frm.savesubmit();
    });
    frm.page.add_action_item('Reject', function () {
      emailActionReject(frm);
    });
  }
}

function validatePan(frm) {
  if (frm.doc.cnp_pan) {
    const patternPAN = /[A-Z]{5}[0-9]{4}[A-Z]{1}/;
    const pan = frm.doc.cnp_pan.toUpperCase();
    if (!(pan.match(patternPAN) && pan.length === 10)) {
      frappe.throw('Invalid PAN number..');
    }
  }
}

function setPanProperty(frm) {
  if (frm.doc.party_type === 'Supplier') {
    frm.set_df_property('cnp_pan', 'hidden', false);
    frm.set_df_property('cnp_pan', 'reqd', true);
  } else {
    frm.set_df_property('cnp_pan', 'hidden', true);
    frm.set_df_property('cnp_pan', 'reqd', false);
  }
}

function setPanNumber(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: { doc_name: frm.doc.party, doc_type: 'Supplier' },
    callback: r => {
      frm.doc.cnp_pan = r.message.pan;
      frm.refresh_field('cnp_pan');
    },
  });
}

function setCostCenterFilter(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'Cost Center',
      filters: [['cnp_not_in_filter', '=', 1]],
      fields: '["name"]',
      pluck: 'name',
    },
    callback: r => {
      if (r.message.length !== 0) {
        frm.set_query('cost_center', filter => {
          return {
            filters: [
              ['Cost Center', 'name', 'not in', r.message],
              ['Cost Center', 'company', '=', frm.doc.company],
            ],
          };
        });
      } else {
        frm.set_query('cost_center', filter => {
          return {
            filters: [['Cost Center', 'company', '=', frm.doc.company]],
          };
        });
      }
    },
  });
}

function setTaxesFilter(frm) {
  frm.set_query('sales_taxes_and_charges_template', function () {
    return {
      filters: [
        ['Sales Taxes and Charges Template', 'company', '=', frm.doc.company],
      ],
    };
  });
}
