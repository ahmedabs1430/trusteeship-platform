/* global frappe */
frappe.listview_settings.Opportunity = {
  formatters: {
    workflow_state: function (value, doc) {
      let pillColor = 'grey';
      switch (value) {
        case 'Draft':
          pillColor = 'blue';
          break;
        case 'Pending for Approval':
          pillColor = 'orange';
          break;
        case 'Approved':
          pillColor = 'green';
          break;
        case 'Rejected':
          pillColor = 'red';
          break;
      }
      return `<span class="indicator-pill whitespace-nowrap ${pillColor}">${value}</span>`;
    },
  },
};
