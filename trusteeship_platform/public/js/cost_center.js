frappe.ui.form.on('Cost Center', {
  refresh: function (frm) {
    if (!frm.is_new()) {
      render_address(frm);
    }
  },
});

function render_address(frm) {
  // render address
  if (frm.fields_dict.cnp_address_html && 'addr_list' in frm.doc.__onload) {
    $(frm.fields_dict.cnp_address_html.wrapper)
      .html(frappe.render_template('addr_list', frm.doc.__onload))
      .find('.btn-address')
      .on('click', function () {
        frappe.new_doc('Address');
      });
  }
  $('.btn-address').hide();
  for (let i = 0; i < frm.doc.__onload.addr_list.length; i++) {
    $('.btn-default-addr-' + i).on('click', function () {
      frappe.confirm(
        'Are you sure you want set ' +
          frm.doc.__onload.addr_list[i].name +
          ' as default address for ' +
          frm.doc.name +
          '?',
        () => {
          frm.doc.cnp_default_address = frm.doc.__onload.addr_list[i].name;
          frm.refresh_field('cnp_default_address');
          frm.dirty();
          frm.save();
        },
      );
    });
  }
  for (let i = 0; i < frm.doc.__onload.addr_list.length; i++) {
    if (frm.doc.__onload.addr_list[i].name === frm.doc.cnp_default_address) {
      $('.btn-default-addr-' + i).hide();
    }
  }
}
