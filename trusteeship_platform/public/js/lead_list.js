/* global frappe */
frappe.listview_settings.Lead = {
  onload: function (listView) {
    frappe.route_options = {
      name: ['like', '%CRM-LEAD-%'],
      status: ['!=', 'Dormant'],
    };
  },
};
