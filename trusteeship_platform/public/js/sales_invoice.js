/* global frappe __,cur_frm,$ */

frappe.ui.form.on('Sales Invoice', {
  onload: frm => {
    frm.doc.payment_terms_template = '';
    frm.refresh_field('payment_terms_template');
    loadTaxesAndChargesClCode(frm);
  },

  onload_post_render: function (frm) {
    frm.confirmation_in_progress = false;
    setCostCenterFilter(frm);
  },

  is_return: frm => {
    if (frm.doc.is_return) frm.doc.cnp_is_debit_note = !frm.doc.is_return;
    frm.refresh_field('cnp_is_debit_note');
    creditDebitNoteDefaultProperties(frm);
  },

  cnp_is_debit_note: frm => {
    if (frm.doc.cnp_is_debit_note)
      frm.doc.is_return = !frm.doc.cnp_is_debit_note;
    frm.refresh_field('is_return');
    creditDebitNoteDefaultProperties(frm);
  },

  on_submit: frm => {
    frm.reload_doc();
  },

  refresh: async frm => {
    rename_reason_field(frm);
    setNamingSeries(frm);
    setFilterForQuotation(frm);

    setCostCenterFilter(frm);
    removeCustomButton(frm);
    addProformaInvoiceBtn(frm);
    addTaxInvoiceBtn(frm);
    if (!frm.doc.__islocal) {
      approvalAction(frm);
    }

    frm.cost_center_initial_value = frm.doc.cost_center;
    frm.confirmation_in_progress = false;

    addApprovalPill(frm);
    creditDebitNoteDefaultProperties(frm);
    disableCreditDebitNoteCheckbox(frm);
    await einvoice(frm);
    loadAddressOnAddressChange(frm);
    addRejectionComment(frm);
    showRejectionComment(frm);
    if (frm.doc.__islocal && frm.doc.amended_from) {
      frm.doc.cnp_email_token = '';
      frm.doc.cnp_action = 0;
    }
    addDebitNoteBtn(frm);
    addCreditNoteBtn(frm);
    mandateFlowMessage(frm);
  },

  validate: frm => {
    loadDefaultAccounts(frm);
  },

  cnp_from_quotation: frm => {
    mandateFlowMessage(frm);
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: frm.doc.cnp_from_quotation, doc_type: 'Quotation' },
      freeze: true,
      callback: r => {
        if (r.message.workflow_state !== 'Accepted By Client')
          frappe.throw('Quotation is not Accepted By Client');
        frm.set_value('cost_center', r.message.cnp_cost_center);
        frm.set_value('campaign', r.message.campaign);
        frm.set_value('source', r.message.source);
        frm.set_value('cnp_cl_code', r.message.cnp_cl_code);
        frm.doc.items = [];
        r.message.items.forEach(items => {
          const row = frm.add_child('items');
          row.item_code = items.item_code;
          row.item_name = items.item_name;
          row.qty = items.qty;
          row.description = items.item_name;
          row.uom = 'Nos';
          row.rate = items.rate;
          row.cnp_type_of_fee = items.cnp_type_of_fee;
          row.gst_hsn_code = items.gst_hsn_code;
          row.amount = items.amount;
        });
        frm.refresh_field('items');
        frm.refresh_field('cost_center');
        frm.refresh_field('project');
        frm.refresh_field('campaign');
        frm.refresh_field('source');
      },
    });
    if (frm.doc.cnp_is_debit_note) {
      frm.fields_dict.items.grid.update_docfield_property(
        'amount',
        'read_only',
        false,
      );
    }
  },
  customer(frm) {
    setFilterForClCode(frm);
  },
  cost_center(frm) {
    if (frm?.cost_center_initial_value) {
      checkCostCenter(frm);
    }
    setCostCenterItem(frm);
    setDefaultShippingCompanyAddress(frm);
  },

  naming_series: frm => {
    if (frm.doc.naming_series === 'AT-CO-.YY.-DN-.') {
      frm.set_df_property('cnp_is_debit_note', 'hidden', false);
      frm.doc.cnp_is_debit_note = 1;
      frm.refresh_field('cnp_is_debit_note');
      frm.set_df_property('customer', 'reqd', 1);
    } else {
      frm.set_df_property('cnp_is_debit_note', 'hidden', true);
      frm.doc.cnp_is_debit_note = 0;
      frm.refresh_field('cnp_is_debit_note');
      frm.set_df_property('customer', 'reqd', 0);
    }
    creditDebitNoteDefaultProperties(frm);
  },
});

frappe.ui.form.on('Sales Invoice Item', {
  form_render: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    lock_reversal_end_date(frm, row);
    reversalDateDocfield(frm, row);
  },

  items_add: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    reversalDateDocfield(frm, row);
    row.cnp_type_of_fee = 'N/A';
    frm.refresh_field('items');
  },

  amount: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    row.cnp_debit_amount = row.amount;
    frm.refresh_field('items');
  },

  cnp_reversal_end_date: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.cnp_reversal_end_date < frappe.datetime.get_today()) {
      frappe.msgprint(__('You can not select past date in Reversal End Date'));
      frappe.validated = false;
      row.cnp_reversal_end_date = '';
    }
    reversalDateDocfield(frm, row);
    frm.refresh_field('items');
  },

  cnp_reversal_start_date: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    lock_reversal_end_date(frm, row);
    reversalDateDocfield(frm, row);
    frm.refresh_field('items');
  },
});

function rename_reason_field(frm) {
  frm.set_df_property(
    'reason_for_issuing_document',
    'label',
    'Reason of Issuing Credit Note',
  );
}

function reversalDateDocfield(frm, row) {
  if (frm.doc.cnp_is_partial_credit_note === 1) {
    frm.fields_dict.items.grid.update_docfield_property(
      'cnp_reversal_start_date',
      'hidden',
      row.cnp_type_of_fee !== 'Annual Fee',
    );
    frm.fields_dict.items.grid.update_docfield_property(
      'cnp_reversal_end_date',
      'hidden',
      row.cnp_type_of_fee !== 'Annual Fee',
    );
    frm.fields_dict.items.grid.update_docfield_property(
      'cnp_reversal_start_date',
      'reqd',
      row.cnp_type_of_fee === 'Annual Fee',
    );
    frm.fields_dict.items.grid.update_docfield_property(
      'cnp_reversal_end_date',
      'reqd',
      row.cnp_type_of_fee === 'Annual Fee',
    );
  }
}

function lock_reversal_end_date(frm, row) {
  frm.cur_grid?.grid_form?.fields_dict?.cnp_reversal_start_date?.datepicker?.update(
    {
      minDate: new Date(frappe.datetime.get_today()),
    },
  );
  if (row.cnp_reversal_start_date) {
    frm.cur_grid?.grid_form?.fields_dict?.cnp_reversal_end_date?.datepicker?.update(
      {
        minDate: new Date(row.cnp_reversal_start_date),
      },
    );
  } else {
    frm.cur_grid?.grid_form?.fields_dict?.cnp_reversal_end_date?.datepicker?.update(
      {
        minDate: new Date(frappe.datetime.get_today()),
      },
    );
  }
}

function checkCostCenter(frm) {
  if (
    frm.doc.cost_center === frm.cost_center_initial_value ||
    frm.confirmation_in_progress
  ) {
    return;
  }
  frappe.validated = false;
  frm.confirmation_in_progress = true;

  frappe.confirm(
    `Do you wish to change the Cost Center from ${frm.cost_center_initial_value} to ${frm.doc.cost_center}?`,
    function () {
      frm.cost_center_initial_value = frm.doc.cost_center;
      frappe.validated = true;
      frm.confirmation_in_progress = false;
      if (frm.doc.__islocal) {
        frm.set_value('cost_center', frm.doc.cost_center);
        frm.refresh_field('cost_center');
      } else {
        frm.save();
      }
    },
    function () {
      frm.set_value('cost_center', frm.cost_center_initial_value);
      frm.confirmation_in_progress = false;
    },
  );
}
async function einvoice(frm) {
  if (frm.doc.docstatus !== 1) return;

  const res = await frappe.call({
    method:
      'trusteeship_platform.public.india.e_invoice.utils.validate_eligibility',
    args: { doc: frm.doc },
  });
  const invoiceEligible = res.message;

  if (!invoiceEligible) return;

  const {
    doctype,
    irn,
    irnCancelled,
    ewaybill,
    ewayBillCancelled,
    name,
    __unsaved,
  } = frm.doc;

  const addCustomButton = (label, action) => {
    if (!frm.custom_buttons[label]) {
      frm.add_custom_button(label, action, __('E Invoicing'));
    }
  };

  if (!irn && !__unsaved) {
    const action = () => {
      if (frm.doc.__unsaved) {
        frappe.throw(__('Please save the document to generate IRN.'));
      }
      frappe.call({
        method:
          'trusteeship_platform.public.india.e_invoice.utils.get_einvoice',
        args: { doctype, docname: name },
        freeze: true,
        callback: res => {
          const einvoice = res.message;
          showEinvoicePreview(frm, einvoice);
        },
      });
    };

    addCustomButton(__('Generate IRN'), action);
  }

  if (irn && !irnCancelled && !ewaybill) {
    const fields = [
      {
        label: 'Reason',
        fieldname: 'reason',
        fieldtype: 'Select',
        reqd: 1,
        default: '1-Duplicate',
        options: [
          '1-Duplicate',
          '2-Data Entry Error',
          '3-Order Cancelled',
          '4-Other',
        ],
      },
      {
        label: 'Remark',
        fieldname: 'remark',
        fieldtype: 'Data',
        reqd: 1,
      },
    ];
    const action = () => {
      const d = new frappe.ui.Dialog({
        title: __('Cancel IRN'),
        fields,
        primary_action: function () {
          const data = d.get_values();
          frappe.call({
            method:
              'trusteeship_platform.public.india.e_invoice.utils.cancel_irn',
            args: {
              doctype,
              docname: name,
              irn,
              reason: data.reason.split('-')[0],
              remark: data.remark,
            },
            freeze: true,
            callback: () => frm.reload_doc() || d.hide(),
            error: () => d.hide(),
          });
        },
        primary_action_label: __('Submit'),
      });
      d.show();
    };
    addCustomButton(__('Cancel IRN'), action);
  }

  if (irn && !irnCancelled && !ewaybill) {
    const action = () => {
      const d = new frappe.ui.Dialog({
        title: __('Generate E-Way Bill'),
        size: 'large',
        fields: getEwaybillFields(frm),
        primary_action: function () {
          const data = d.get_values();
          frappe.call({
            method:
              'trusteeship_platform.public.india.e_invoice.utils.generate_eway_bill',
            args: {
              doctype,
              docname: name,
              irn,
              ...data,
            },
            freeze: true,
            callback: () => frm.reload_doc() || d.hide(),
            error: () => d.hide(),
          });
        },
        primary_action_label: __('Submit'),
      });
      d.fields_dict.transporter.df.onchange = function () {
        const transporter = d.fields_dict.transporter.value;
        if (transporter) {
          frappe.db
            .get_value('Supplier', transporter, [
              'gst_transporter_id',
              'supplier_name',
            ])
            .then(({ message }) => {
              d.set_value('gst_transporter_id', message.gst_transporter_id);
              d.set_value('transporter_name', message.supplier_name);
            });
        } else {
          d.set_value('gst_transporter_id', '');
          d.set_value('transporter_name', '');
        }
      };
      d.fields_dict.driver.df.onchange = function () {
        const driver = d.fields_dict.driver.value;
        if (driver) {
          frappe.db
            .get_value('Driver', driver, ['full_name'])
            .then(({ message }) => {
              d.set_value('driver_name', message.full_name);
            });
        } else {
          d.set_value('driver_name', '');
        }
      };
      d.show();
    };

    addCustomButton(__('Generate E-Way Bill'), action);
  }

  if (irn && ewaybill && !irnCancelled && !ewayBillCancelled) {
    const action = () => {
      let message =
        __('Cancellation of e-way bill is currently not supported.') + ' ';
      message += '<br><br>';
      message += __(
        'You must first use the portal to cancel the e-way bill and then update the cancelled status in the ERPNext system.',
      );

      const dialog = frappe.msgprint({
        title: __('Update E-Way Bill Cancelled Status?'),
        message,
        indicator: 'orange',
        primary_action: {
          action: function () {
            frappe.call({
              method:
                'trusteeship_platform.public.india.e_invoice.utils.cancel_eway_bill',
              args: { doctype, docname: name },
              freeze: true,
              callback: () => frm.reload_doc() || dialog.hide(),
            });
          },
        },
        primary_action_label: __('Yes'),
      });
    };
    addCustomButton(__('Cancel E-Way Bill'), action);
  }
}
const getEwaybillFields = frm => {
  return [
    {
      fieldname: 'transporter',
      label: 'Transporter',
      fieldtype: 'Link',
      options: 'Supplier',
      default: frm.doc.transporter,
    },
    {
      fieldname: 'gst_transporter_id',
      label: 'GST Transporter ID',
      fieldtype: 'Data',
      default: frm.doc.gst_transporter_id,
    },
    {
      fieldname: 'driver',
      label: 'Driver',
      fieldtype: 'Link',
      options: 'Driver',
      default: frm.doc.driver,
    },
    {
      fieldname: 'lr_no',
      label: 'Transport Receipt No',
      fieldtype: 'Data',
      default: frm.doc.lr_no,
    },
    {
      fieldname: 'vehicle_no',
      label: 'Vehicle No',
      fieldtype: 'Data',
      default: frm.doc.vehicle_no,
    },
    {
      fieldname: 'distance',
      label: 'Distance (in km)',
      fieldtype: 'Float',
      default: frm.doc.distance,
    },
    {
      fieldname: 'transporter_col_break',
      fieldtype: 'Column Break',
    },
    {
      fieldname: 'transporter_name',
      label: 'Transporter Name',
      fieldtype: 'Data',
      read_only: 1,
      default: frm.doc.transporter_name,
      depends_on: 'transporter',
    },
    {
      fieldname: 'mode_of_transport',
      label: 'Mode of Transport',
      fieldtype: 'Select',
      options: '\nRoad\nAir\nRail\nShip',
      default: frm.doc.mode_of_transport,
    },
    {
      fieldname: 'driver_name',
      label: 'Driver Name',
      fieldtype: 'Data',
      fetch_from: 'driver.full_name',
      read_only: 1,
      default: frm.doc.driver_name,
      depends_on: 'driver',
    },
    {
      fieldname: 'lr_date',
      label: 'Transport Receipt Date',
      fieldtype: 'Date',
      default: frm.doc.lr_date,
    },
    {
      fieldname: 'gst_vehicle_type',
      label: 'GST Vehicle Type',
      fieldtype: 'Select',
      options: 'Regular\nOver Dimensional Cargo (ODC)',
      depends_on: 'eval:(doc.mode_of_transport === "Road")',
      default: frm.doc.gst_vehicle_type,
    },
  ];
};
const requestIrnGeneration = frm => {
  frappe.call({
    method: 'trusteeship_platform.public.india.e_invoice.utils.generate_irn',
    args: { doctype: frm.doc.doctype, docname: frm.doc.name },
    freeze: true,
    callback: () => frm.reload_doc(),
  });
};

const getPreviewDialog = (frm, action) => {
  const dialog = new frappe.ui.Dialog({
    title: __('Preview'),
    size: 'large',
    fields: [
      {
        label: 'Preview',
        fieldname: 'preview_html',
        fieldtype: 'HTML',
      },
    ],
    primary_action: () => action(frm) || dialog.hide(),
    primary_action_label: __('Generate IRN'),
  });
  return dialog;
};

const showEinvoicePreview = (frm, einvoice) => {
  const previewDialog = getPreviewDialog(frm, requestIrnGeneration);

  // initialize e-invoice fields
  einvoice.Irn = einvoice.AckNo = '';
  einvoice.AckDt = frappe.datetime.nowdate();
  frm.doc.signed_einvoice = JSON.stringify(einvoice);

  // initialize preview wrapper
  const $previewWrapper = previewDialog.get_field('preview_html').$wrapper;
  $previewWrapper.html(
    `<div>
    <div class="print-preview">
    <div class="print-format"></div>
    </div>
    <div class="page-break-message text-muted text-center text-medium margin-top"></div>
    </div>`,
  );

  frappe.call({
    method: 'frappe.www.printview.get_html_and_style',
    args: {
      doc: frm.doc,
      print_format: 'GST Tax Invoice',
      no_letterhead: 1,
    },
    callback: function (r) {
      if (!r.exc) {
        $previewWrapper.find('.print-format').html(r.message.html);
        const style = `
        .print-format { box-shadow: 0px 0px 5px rgba(0,0,0,0.2); padding: 0.30in; min-height: 80vh; }
        .print-preview { min-height: 0px; }
        .modal-dialog { width: 720px; }`;

        frappe.dom.set_style(style, 'custom-print-style');
        previewDialog.show();
      }
    },
  });
};
function loadTaxesAndChargesClCode(frm) {
  if (frm.doc.cnp_from_quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.cnp_from_quotation,
        doc_type: 'Quotation',
      },
      freeze: true,
      callback: r => {
        const taxes_and_charges = r.message.taxes_and_charges;
        if (taxes_and_charges) {
          frm.doc.taxes_and_charges = taxes_and_charges;
          frm.refresh_field('taxes_and_charges');
          frm.doc.cnp_cl_code = r.message.cnp_cl_code;
          frm.refresh_field('cnp_cl_code');
        }
      },
    });
  }
}

function addTaxInvoiceBtn(frm) {
  if (!frm.doc.cnp_is_debit_note && !frm.doc.is_return) {
    frm.add_custom_button(__('Tax Invoice'), () => {
      if (frm.doc.company) {
        window.open(
          '/api/method/trusteeship_platform.custom_methods.download_tax_invoice?doctype=' +
            frm.doctype +
            '&docname=' +
            frm.doc.name +
            '&company_name=' +
            frm.doc.company,
          '_blank',
        );
      } else {
        frappe.throw('Company is mandatory for Tax Invoice');
      }
    });
    validateTaxInvoiceBtn(frm);
  }
}
function validateTaxInvoiceBtn(frm) {
  if (frm.doc.docstatus === 1 && frm.doc.cnp_from_quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_outstanding',
      args: {
        doctype: 'Sales Invoice',
        docname: frm.doc.name,
        customer: frm.doc.customer,
        quotation: frm.doc.cnp_from_quotation,
      },
      freeze: true,
      callback: r => {
        if (r.message > 0) {
          frm.remove_custom_button('Tax Invoice');
        }
      },
    });
  }
}

function addProformaInvoiceBtn(frm) {
  if (frm.doc.docstatus === 1 && !frm.doc.is_return) {
    frm.doc.items.forEach(item => {
      if (item.cnp_type_of_fee === 'Annual Fee') {
        frm.add_custom_button(
          frm.doc.cnp_is_debit_note ? 'Proforma Debit' : 'Proforma Invoice',
          () => {
            if (frm.doc.company) {
              window.open(
                '/api/method/trusteeship_platform.custom_methods.download_sales_proforma_invoice?doctype=' +
                  frm.doctype +
                  '&docname=' +
                  frm.doc.name +
                  '&company_name=' +
                  frm.doc.company,
                '_blank',
              );
            } else {
              frappe.throw('Company is mandatory for Tax Invoice');
            }
          },
        );
      }
    });
  }
}

function removeCustomButton(frm) {
  setTimeout(() => {
    frm.remove_custom_button('Quality Inspection(s)', 'Create');
    frm.remove_custom_button('Fetch Timesheet');
  }, 100);
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'New' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.page.add_action_item('Send For Approval', function () {
      sendApprovalMail(frm);
    });
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.page.add_action_item('Approve', function () {
      frm.doc.cnp_action = 1;
      frm.savesubmit();
    });
    frm.page.add_action_item('Reject', function () {
      if (frm.doc.is_return === 1) {
        commentDialog(frm, false);
      } else {
        emailActionReject(frm);
      }
    });
  }
}
function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Sales Invoice',
      doctype_name: 'sales-invoice',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function addApprovalPill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let titleElement = $(`h3[title="${frm.get_title().trim()}"]`).parent();
    titleElement = titleElement[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.workflow_state;
    pillspan.appendChild(textspan);
    titleElement.appendChild(pillspan);
  }
}

function loadAddressOnAddressChange(frm) {
  const prevRoute = frappe.get_prev_route();
  if (
    prevRoute[1] === 'Address' &&
    localStorage.getItem('address_update') === '0'
  ) {
    localStorage.setItem('address_update', '1');
    erpnext.utils.get_address_display(
      frm,
      'customer_address',
      'address_display',
      false,
      '',
    );
    frm.save();
  }
}

function loadPaymentDueDate(frm) {
  const date = new Date();
  const dueDate = frappe.datetime.add_days(date, 45);
  frm.doc.due_date = dueDate.toString();
  frm.refresh_field('due_date');
}

function creditDebitNoteDefaultProperties(frm) {
  if (cur_frm.doc.__islocal) loadPaymentDueDate(cur_frm);
  const annual_fee = frm.doc.items.filter(
    item => item.cnp_type_of_fee === 'Annual Fee',
  );
  frm.set_df_property(
    'cnp_type_of_account',
    'hidden',
    !frm.doc.cnp_is_debit_note,
  );

  frm.fields_dict.items.grid.update_docfield_property(
    'cnp_reversal_start_date',
    'reqd',
    frm.doc.cnp_is_partial_credit_note
      ? annual_fee.length !== 0
      : frm.doc.is_return,
  );

  frm.fields_dict.items.grid.update_docfield_property(
    'cnp_reversal_end_date',
    'reqd',
    frm.doc.cnp_is_partial_credit_note
      ? annual_fee.length !== 0
      : frm.doc.is_return,
  );

  frm.fields_dict.items.grid.update_docfield_property(
    'cnp_reversal_start_date',
    'allow_on_submit',
    frm.doc.is_return && frm.doc.docstatus === 0,
  );

  frm.fields_dict.items.grid.update_docfield_property(
    'cnp_reversal_end_date',
    'allow_on_submit',
    frm.doc.is_return && frm.doc.docstatus === 0,
  );

  frm.refresh_field('cnp_type_of_account');

  if (frm.doc.cnp_is_debit_note) {
    frm.fields_dict.items.grid.update_docfield_property(
      'rate',
      'read_only',
      !frm.doc.cnp_is_debit_note,
    );

    frm.fields_dict.items.grid.update_docfield_property(
      'item_code',
      'read_only',
      !frm.doc.cnp_is_debit_note,
    );

    frm.fields_dict.items.grid.update_docfield_property(
      'cnp_type_of_fee',
      'read_only',
      frm.doc.cnp_is_debit_note,
    );

    frm.set_query('item_code', 'items', function () {
      return {
        filters: { is_purchase_item: 1, is_sales_item: 1 },
      };
    });

    frm.set_query('cnp_type_of_account', function () {
      return {
        filters: [['Item', 'is_purchase_item', '=', 0]],
      };
    });

    frm.doc.items.forEach(item => {
      item.cnp_type_of_fee = 'N/A';
    });
  } else if (frm.doc.is_return === 1) {
    frm.fields_dict.items.grid.update_docfield_property(
      'rate',
      'read_only',
      !frm.doc.is_return,
    );
    frm.fields_dict.items.grid.update_docfield_property(
      'item_code',
      'read_only',
      frm.doc.is_return,
    );
  } else {
    frm.fields_dict.items.grid.update_docfield_property(
      'rate',
      'read_only',
      !frm.doc.cnp_is_debit_note,
    );

    frm.fields_dict.items.grid.update_docfield_property(
      'item_code',
      'read_only',
      !frm.doc.cnp_is_debit_note,
    );
  }
  frm.refresh_field('items');
}

function setFilterForQuotation(frm) {
  frm.set_query('cnp_from_quotation', function () {
    return {
      filters: [
        ['docstatus', '=', 1],
        ['workflow_state', '=', 'Accepted By Client'],
        ['cnp_customer_code', '=', frm.doc.customer],
      ],
    };
  });
}
function setFilterForClCode(frm) {
  frm.set_df_property('cnp_cl_code', 'read_only', false);
  frm.set_query('cnp_cl_code', function () {
    return {
      filters: [['cnp_customer_code', '=', frm.doc.customer]],
    };
  });
}

function addRejectionComment(frm) {
  if (frm.doc.cnp_is_reject_reason === 1) commentDialog(frm);
}

function commentDialog(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Reason to Reject',
    static: true,
    fields: [
      {
        fieldname: 'reason_to_reject',
        fieldtype: 'Text',
        label: 'Comment',
        reqd: 1,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      const reasonRoReject =
        '<b>Reason to reject:</b> ' + data.reason_to_reject;

      frappe.call({
        method:
          'trusteeship_platform.custom_methods.add_rejection_comment_sales_invoice',
        args: {
          doctype: 'Sales Invoice',
          docname: frm.doc.name,
          comment: reasonRoReject,
        },
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.reload_doc();
          }
        },
      });
    },
  });
  d.show();
}

function showRejectionComment(frm) {
  if (frm.doc.cnp_reject_reason_comment) {
    frm.set_intro('');
    frm.set_intro(frm.doc.cnp_reject_reason_comment);
  } else {
    frm.set_intro('');
  }
}

function emailActionReject(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.email_action',
    args: {
      doctype: 'Sales Invoice',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'sales-invoice',
      redirect: false,
      email_token: frm.doc.cnp_email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function addDebitNoteBtn(frm) {
  if (
    !frm.doc.__islocal &&
    frm.doc.cnp_is_debit_note === 1 &&
    (frm.doc.workflow_state === 'Approved' ||
      frm.doc.workflow_state === 'Pending for Approval' ||
      frm.doc.workflow_state === 'Rejected')
  ) {
    frm.add_custom_button('Debit Note', () => {
      frappe.call({
        method:
          'trusteeship_platform.overrides.sales_invoice.get_debit_note_qrcode',
        args: { docname: frm.doc.name },
        freeze: true,
        callback: r => {
          frm.reload_doc();
          const api =
            '/api/method/trusteeship_platform.overrides.sales_invoice.download_debit_note_invoice';
          const query = `docname=${frm.doc.name}`;
          window.open(`${api}?${query}`, '_blank');
        },
      });
    });
  }
}

function addCreditNoteBtn(frm) {
  if (
    !frm.doc.__islocal &&
    frm.doc.is_return === 1 &&
    (frm.doc.workflow_state === 'Approved' ||
      frm.doc.workflow_state === 'Pending for Approval' ||
      frm.doc.workflow_state === 'Rejected')
  ) {
    frm.add_custom_button('Credit Note', () => {
      frappe.call({
        method:
          'trusteeship_platform.overrides.sales_invoice.get_credit_note_qrcode',
        args: { docname: frm.doc.name },
        freeze: true,
        callback: r => {
          frm.reload_doc();
          const api =
            '/api/method/trusteeship_platform.overrides.sales_invoice.download_credit_note_invoice';
          const query = `docname=${frm.doc.name}`;
          window.open(`${api}?${query}`, '_blank');
        },
      });
    });
  }
}

function setCostCenterFilter(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'Cost Center',
      filters: [['cnp_not_in_filter', '=', 1]],
      fields: '["name"]',
      pluck: 'name',
    },
    callback: r => {
      if (r.message.length !== 0) {
        frm.set_query('cost_center', filter => {
          return {
            filters: [
              ['Cost Center', 'is_group', '=', 0],
              ['Cost Center', 'name', 'not in', r.message],
              ['Cost Center', 'company', '=', frm.doc.company],
            ],
          };
        });
      } else {
        frm.set_query('cost_center', filter => {
          return {
            filters: [
              ['Cost Center', 'is_group', '=', 0],
              ['Cost Center', 'company', '=', frm.doc.company],
            ],
          };
        });
      }
    },
  });
}

function setNamingSeries(frm) {
  frm.set_df_property(
    'naming_series',
    'options',
    'SINV-.YY.-\nSRET-.YY.-\nACC-SINV-.YYYY.-\nACC-SINV-RET-.YYYY.-\nAT-CO-.YY.-T-.\nAT-CO-.YY.-DN-.\nAT-CO-.YY.-CN-.',
  );
}

function disableCreditDebitNoteCheckbox(frm) {
  const prevRoute = frappe.get_prev_route();
  if (frm.doc.__islocal && prevRoute[1] === 'Quotation') {
    frm.doc.naming_series = 'AT-CO-.YY.-T-.';
    frm.refresh_field('naming_series');
    frm.set_df_property('cnp_is_debit_note', 'hidden', true);
    frm.set_df_property('is_return', 'hidden', true);
  } else if (frm.doc.__islocal) {
    frm.set_df_property('cnp_is_debit_note', 'hidden', false);
    frm.set_df_property('is_return', 'hidden', false);
  } else {
    if (frm.doc.is_return === 1) {
      frm.set_df_property('cnp_is_debit_note', 'hidden', true);
      frm.set_df_property('is_return', 'hidden', false);
    } else if (frm.doc.cnp_is_debit_note === 1) {
      frm.set_df_property('is_return', 'hidden', true);
      frm.set_df_property('cnp_is_debit_note', 'hidden', false);
    } else {
      frm.set_df_property('is_return', 'hidden', true);
      frm.set_df_property('cnp_is_debit_note', 'hidden', true);
    }
    frm.set_df_property('is_return', 'read_only', true);
    frm.set_df_property('cnp_is_debit_note', 'read_only', true);
  }
}

function loadDefaultAccounts(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.default_acct',
    args: {},
    callback: r => {
      frm.doc.items.forEach(item => {
        if (item.cnp_type_of_fee === 'Initial Fee') {
          if (r.message.initial_fee_account) {
            item.income_account = '6020001010 - Initial Acceptance Fee - ATSL';
          } else {
            frappe.throw(
              "Please set Credit account '6020001010 - Initial Acceptance Fee - ATSL' to save the sales invoice",
            );
          }
        }
        if (item.cnp_type_of_fee === 'Annual Fee') {
          if (r.message.income_received) {
            item.income_account =
              '1010010005 - Income Received in Advance - ATSL';
          } else {
            frappe.throw(
              "Please set Credit account '1010010005 - Income Received in Advance - ATSL' to save the sales invoice",
            );
          }
        }
        if (item.cnp_type_of_fee === 'One Time Fee') {
          if (r.message.one_time_account) {
            item.income_account = '6020001015 - One Time Fee - ATSL';
          } else {
            frappe.throw(
              "Please set Credit account '6020001015 - One Time Fee - ATSL' to save the sales invoice",
            );
          }
        }
      });
      frm.refresh_field('items');
    },
  });
}

function setCostCenterItem(frm) {
  frm.doc.items.forEach(item => {
    item.cost_center = frm.doc.cost_center;
  });
  frm.refresh_field('items');
}

function setDefaultShippingCompanyAddress(frm) {
  frm.set_query('shipping_address', function () {
    return {
      filters: {
        link_doctype: 'Company',
        link_name: frm.doc.company,
        cnp_cost_center: frm.doc.cost_center,
      },
    };
  });
  frm.set_query('billing_address', function () {
    return {
      filters: {
        link_doctype: 'Company',
        link_name: frm.doc.company,
        cnp_cost_center: frm.doc.cost_center,
      },
    };
  });
  setDefaultCompanyAddress(frm);
}

function setDefaultCompanyAddress(frm) {
  if (frm.doc.cost_center) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.cost_center_default_address',
      args: {
        cost_center: frm.doc.cost_center,
      },
      callback: r => {
        let default_address = r.message;
        if (default_address === undefined) {
          default_address = '';
        }
        frm.doc.company_address = default_address;
        frm.refresh_field('company_address');
        frm.trigger('company_address');
      },
    });
  }
}

async function mandateFlowMessage(frm) {
  if (frm.doc.cnp_from_quotation) {
    const mandateFlowDone = await isMandateFlowDone(frm);
    if (!mandateFlowDone) {
      if (frm.doc.__islocal) {
        frappe.warn(
          'Warning',
          `According to the revised pricing policy,
        annual fee needs to be generated only after Document Execution.
        This condition is not met, would you still like to proceed?`,
          () => {
            // action to perform if Continue is selected
            setIntroMessage(frm);
          },
          'Continue',
          false, // Sets dialog as minimizable
        );
      } else {
        setIntroMessage(frm);
      }
    }
  }
}

function setIntroMessage(frm) {
  frm.set_intro('');
  frm.set_intro('Annual fee is being generated without Document Execution');
}

async function isMandateFlowDone(frm) {
  const response = await frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.is_mandate_flow_done',
    args: {
      quotation: frm.doc.cnp_from_quotation,
    },
    freeze: true,
  });

  return response.message;
}
