/* global frappe, $ */
/* eslint-env jquery */
frappe.ui.form.on('Account', {
  refresh: frm => {
    approval(frm);
    addApprovalPill(frm);
    frm.set_df_property('disabled', 'read_only', false);
    if (frm.doc.__islocal) {
      frm.doc.disabled = 1;
      frm.doc.cnp_importer_flag = 1;
    }
    if (frm.doc.cnp_importer_flag === 0) ActionApproveForChartofImport(frm);
  },

  after_save: frm => {
    frm.reload_doc();
  },

  disableFlag: frm => {
    const disableFlag = frm.doc.disabled;
    sendApprovalMail(frm, disableFlag);
  },

  account_name: frm => {
    frm.set_df_property('account_number', 'read_only', false);
  },
});

function approval(frm) {
  if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager') &&
    frm.doc.cnp_importer_flag
  ) {
    frm.page.add_action_item('Approve', function () {
      emailActionApprove(frm);
    });
    frm.page.add_action_item('Reject', function () {
      emailActionReject(frm);
    });
  }
}

function addApprovalPill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let titleElement = $(`h3[title="${frm.doc.name}"]`).parent();
    titleElement = titleElement[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.workflow_state;
    pillspan.appendChild(textspan);

    titleElement.appendChild(pillspan);
  }
}

function sendApprovalMail(frm, disableFlag) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Account',
      doctype_name: 'account',
      doc_disable: disableFlag,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function emailActionApprove(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.email_action',
    args: {
      doctype: 'Account',
      docname: frm.doc.name,
      action: '1',
      doctype_name: 'account',
      redirect: false,
      email_token: frm.doc.cnp_email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionReject(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.email_action',
    args: {
      doctype: 'Account',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'account',
      redirect: false,
      email_token: frm.doc.cnp_email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function ActionApproveForChartofImport(frm) {
  frappe.call({
    method: 'trusteeship_platform.public.py.Account.email_action',
    args: {
      docname: frm.doc.name,
      action: '1',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
