/* global frappe */
/* eslint-env jquery */
erpnext.CanopiLeadController = class CanopiLeadController extends (
  erpnext.LeadController
) {
  refresh(doc, dt, dn) {
    super.refresh(doc, dt, dn);
    hideCustomButtons(cur_frm);
  }
};
cur_frm.script_manager.make(erpnext.CanopiLeadController);

frappe.ui.form.on('Lead', {
  onload_post_render: frm => {
    frm.toggle_reqd('first_name', false);
    hideCustomButtons(frm);
  },

  refresh: frm => {
    opportunityButtonInConnections(frm);
    if (!frm.is_new()) {
      frm.clear_custom_buttons();
    }
    // addFetchButton(frm);
    addDeleteButton(frm);
    setPincodeReqd(frm);
    loadMarketIndustry(frm);
    configureDealCloseDatepicker(frm);
    frm.set_df_property('company', 'read_only', true);
    frm.toggle_reqd('first_name', false);
    setLabelReferenceNumber(frm);

    if (localStorage.getItem('lead') === '0') {
      localStorage.setItem('lead', 1);
      frm.reload_doc();
    }
    addOpportunityButton(frm);
    changeAnnualRevenueLabel(frm);
    filterProduct(frm);

    if (frm.doc.cin_number) {
      frappe.call({
        method: 'frappe.client.get_list',
        args: {
          doctype: 'CPRB Company Details',
          filters: {
            name: frm.doc.cin_number,
          },
          fields: ['name', 'last_updated'],
          limit: 1,
        },
        callback: function (r) {
          if (r.message && r.message.length > 0) {
            r.message = r.message[0];
            frm.set_df_property('cnp_fetch_base_details', 'hidden', 1);
            frm.add_custom_button(
              __('Update Details From Probe42'),
              function () {
                frappe.confirm(
                  'The details were last updated on <b>' +
                    r.message.last_updated +
                    '</b> Do you want to continue the fetch?',
                  () => {
                    if (checkRequiredFields(frm)) {
                      // frappe.throw("The Details Were updated at "+r.message.request_datetime)
                      frappe.call({
                        freeze: true,
                        method:
                          'trusteeship_platform.custom_methods.get_probe_42_comprehensive_details',
                        args: {
                          doc: frm.doc,
                        },
                        callback: function (response) {
                          if (frm.doc.__islocal) {
                            frappe.set_route(
                              'Form',
                              'Lead',
                              response.message.name,
                            );
                          }
                          frappe.show_alert(
                            {
                              message: 'Fetched successfully.',
                              indicator: 'green',
                            },
                            5,
                          );
                          frm.reload_doc();
                        },
                      });
                    }
                  },
                  function () {
                    // action to perform if No is selected
                  },
                );
              },
            );
          }
        },
      });
    }
  },

  onload: frm => {
    hideCustomButtons(frm);
    frm.set_query('lead_owner', function () {
      return {
        filters: [['Role', 'role_name', '=', 'BCG RM']],
      };
    });
    setLabelReferenceNumber(frm);
  },
  cnp_fetch_base_details: frm => {
    if (checkRequiredFields(frm)) {
      frappe.call({
        freeze: true,
        method: 'trusteeship_platform.custom_methods.get_probe_42_base_details',
        args: {
          doc: frm.doc,
        },
        callback: r => {
          if (frm.doc.__islocal) {
            frappe.set_route('Form', 'Lead', r.message[0].name);
          }
          frappe.show_alert(
            {
              message: 'Fetched successfully.',
              indicator: 'green',
            },
            5,
          );
          frm.reload_doc();
          frappe.msgprint(r.message[1]);
        },
      });
    }
  },
  before_save: frm => {
    if (frm.doc.cnp_type === 'Others') {
      frm.toggle_reqd('cin_number', false);
    } else {
      frm.toggle_reqd('cin_number', true);
    }
  },

  source: frm => {
    if (frm.doc.source === 'Others') {
      frm.toggle_reqd('cnp_source_others', true);
      frm.toggle_display('cnp_source_others', true);
    } else {
      frm.toggle_reqd('cnp_source_others', false);
      frm.toggle_display('cnp_source_others', false);
    }
  },

  address_title: frm => {
    setPincodeReqd(frm);
  },

  address_type: frm => {
    if (frm.doc.address_type === 'Others') {
      frm.toggle_reqd('cnp_address_type_others', true);
      frm.toggle_display('cnp_address_type_others', true);
    } else {
      frm.toggle_reqd('cnp_address_type_others', false);
      frm.toggle_display('cnp_address_type_others', false);
    }
  },

  cin_number: frm => {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: { doctype: 'Lead' },
      callback: leadList => {
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_doc_list',
          args: { doctype: 'Customer' },

          callback: customerList => {
            const lead = leadList.message.find(
              element => element.cin_number === frm.doc.cin_number,
            );
            const customer = customerList.message.find(
              element => element.cin_number === frm.doc.cin_number,
            );
            if (lead && customer) {
              if (lead.name !== frm.doc.name) {
                const d = new frappe.ui.Dialog({
                  title: 'Confirm',
                  fields: [
                    {
                      fieldtype: 'HTML',
                      options:
                        'This Reference number already exists as a Customer. Would you like to create an Opportunity for this Organization?',
                    },
                  ],
                  primary_action_label: 'Yes',
                  primary_action: () => {
                    openProductDialog('Customer', customer.name);
                    d.hide();
                  },

                  secondary_action_label: 'No',
                  secondary_action: () => {
                    d.hide();
                  },
                });
                d.show();
              }
            } else if (lead && !customer) {
              if (lead.status !== 'Opportunity' && lead.name !== frm.doc.name) {
                const d = new frappe.ui.Dialog({
                  title: 'Confirm',
                  fields: [
                    {
                      fieldtype: 'HTML',
                      options:
                        'This Reference number already exists as a Lead. Would you like to open that Lead?',
                    },
                  ],
                  primary_action_label: 'Yes',
                  primary_action: () => {
                    frappe.set_route('Form', 'Lead', lead.name);
                  },

                  secondary_action_label: 'No',
                  secondary_action: () => {
                    d.hide();
                  },
                });
                d.show();
              } else if (
                lead.status === 'Opportunity' &&
                lead.name !== frm.doc.name
              ) {
                const d = new frappe.ui.Dialog({
                  title: 'Confirm',
                  fields: [
                    {
                      fieldtype: 'HTML',
                      options:
                        'This Reference number already exists as a Lead. Would you like to create an Opportunity for this Lead?',
                    },
                  ],

                  primary_action_label: 'Yes',
                  primary_action: () => {
                    if (!lead.cnp_product) {
                      openProductDialog('Lead', lead.name);
                    } else {
                      createOpportunity('Lead', lead.name, lead.cnp_product);
                    }
                    d.hide();
                  },

                  secondary_action_label: 'No',
                  secondary_action: () => {
                    d.hide();
                  },
                });

                d.show();
              }
            } else if (customer && !lead) {
              const d = new frappe.ui.Dialog({
                title: 'Confirm',
                fields: [
                  {
                    fieldtype: 'HTML',
                    options:
                      'This Reference number already exists as a Customer. Would you like to create an Opportunity or Lead for this Organization?',
                  },
                ],

                primary_action_label: 'Create Opportunity',
                primary_action: () => {
                  openProductDialog('Customer', customer.name);
                  d.hide();
                },

                secondary_action_label: 'Create Lead',
                secondary_action: () => {
                  frappe.call({
                    freeze: true,
                    method: 'trusteeship_platform.custom_methods.make_new_lead',
                    args: {
                      cin_number: customer.cin_number,
                      company_name: customer.customer_name,
                    },
                    callback: r => {
                      frappe.set_route('Form', 'Lead', r.message.name);
                    },
                  });
                  d.hide();
                },
              });
              d.show();
            }
          },
        });
      },
    });
  },

  country: frm => {
    setPincodeReqd(frm);
  },

  validate: frm => {
    validateReferenceNumber(frm);
  },
  cnp_type: frm => {
    setLabelReferenceNumber(frm);
  },

  cnp_expected_deal_closure: frm => {
    validateDealClosureDate(frm);
  },
});

function opportunityButtonInConnections(frm) {
  $("button[data-doctype='Opportunity']").unbind();
  $("button[data-doctype='Opportunity']").on('click', async function () {
    frappe.model.open_mapped_doc({
      method: 'erpnext.crm.doctype.lead.lead.make_opportunity',
      frm,
    });
  });
}

function addDeleteButton(frm) {
  if (
    !frm.doc.__islocal &&
    (!frappe.user.has_role('BCG Head') ||
      frappe.session.user === 'Administrator')
  ) {
    frm.add_custom_button('Delete All', () => {
      frappe.call({
        method:
          'trusteeship_platform.custom_methods.delete_lead_with_connection',
        args: {
          docname: frm.doc.name,
        },
        freeze: true,
        callback: r => {
          frappe.set_route('List', 'Lead');
        },
      });
    });
  }
}

function checkRequiredFields(frm) {
  if (frm.doc.cnp_type !== 'Organization' && !frm.doc.first_name) {
    frappe.msgprint({
      message: 'First name is mandatory.',
      indicator: 'red',
      title: 'Message',
    });
    return false;
  } else if (frm.doc.cnp_type !== 'Organization' && !frm.doc.company_name) {
    frappe.msgprint({
      message: 'Organization name is mandatory.',
      indicator: 'red',
      title: 'Message',
    });
    return false;
  } else if (!frm.doc.cin_number) {
    frappe.msgprint({
      message: 'CIN Number is mandatory.',
      indicator: 'red',
      title: 'Message',
    });

    return false;
  } else if (!frm.doc.cnp_type) {
    frappe.msgprint({
      message: 'Lead Type is mandatory.',
      indicator: 'red',
      title: 'Message',
    });

    return false;
  } else if (
    !frm.doc.pincode &&
    frm.doc.__islocal === 1 &&
    frm.doc.address_title
  ) {
    frappe.msgprint({
      message: 'Postal Code is mandatory.',
      indicator: 'red',
      title: 'Message',
    });
    return false;
  } else if (
    !frm.doc.address_line1 &&
    frm.doc.__islocal === 1 &&
    frm.doc.address_title
  ) {
    frappe.msgprint({
      message: 'Address Line 1 is mandatory.',
      indicator: 'red',
      title: 'Message',
    });
    return false;
  } else if (
    !frm.doc.city &&
    frm.doc.__islocal === 1 &&
    frm.doc.address_title
  ) {
    frappe.msgprint({
      message: 'City/Town is mandatory.',
      indicator: 'red',
      title: 'Message',
    });
    return false;
  } else {
    return true;
  }
}

function setPincodeReqd(frm) {
  if (
    frm.doc.country === 'India' &&
    frm.doc.__islocal === 1 &&
    frm.doc.address_title
  ) {
    frm.toggle_reqd('pincode', true);
  } else {
    frm.toggle_reqd('pincode', false);
  }
}

function loadMarketIndustry(frm) {
  $(frm.fields_dict.cnp_market_industry_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_markert_industry_list',
      args: { lead_link_name: frm.doc.name },
      freeze: true,

      callback: r => {
        $(frm.fields_dict.cnp_market_industry_html.wrapper).empty();
        frm.set_df_property(
          'cnp_market_industry_html',
          'options',
          frappe.render_template('market_industry', {
            doc: r.message,
            id: frm.doc.name,
          }),
        );
        frm.refresh_field('cnp_market_industry_html');
        addIndustry(frm);
        addMarketSegment(frm);
        createMarketSegment(frm);
      },
    });
  }
}
function addIndustry(frm) {
  $('.' + frm.doc.name + '-industry-add').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Industry',
      fields: [
        {
          label: 'Industry Type',
          options: 'Industry Type',
          reqd: 1,
          fieldname: 'industry',
          fieldtype: 'Link',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.custom_methods.update_industry_reference_table',
          args: {
            industry_name: values.industry,
            lead_link_name: frm.doc.name,
          },
          freeze: true,
          callback: r => {
            loadMarketIndustry(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}
function addMarketSegment(frm) {
  $('.' + frm.doc.name + '-segment-add').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Market Segment',
      fields: [
        {
          label: 'Industry Type',
          options: 'Industry Type',
          reqd: 1,
          fieldname: 'industry',
          fieldtype: 'Link',
          filters: {
            link_doctype: 'Lead',
            link_name: frm.doc.name,
          },
        },
        {
          label: 'Market Segment',
          options: 'Market Segment',
          reqd: 1,
          fieldname: 'segment',
          fieldtype: 'Link',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.custom_methods.update_market_segment_reference_table',
          args: {
            segment_name: values.segment,
            lead_link_name: frm.doc.name,
            industry_link_name: values.industry,
          },
          freeze: true,
          callback: r => {
            loadMarketIndustry(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}
function createMarketSegment(frm) {
  $('.' + frm.doc.name + '-segment-create').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Market Segment',
      fields: [
        {
          label: 'Industry Type',
          options: 'Industry Type',
          reqd: 1,
          fieldname: 'industry',
          fieldtype: 'Link',
          filters: {
            link_doctype: 'Lead',
            link_name: frm.doc.name,
          },
        },
        {
          label: 'Market Segment',
          reqd: 1,
          fieldname: 'segment',
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method: 'trusteeship_platform.custom_methods.make_new_market_segment',
          args: {
            segment_name: values.segment,
            lead_link_name: frm.doc.name,
            industry_link_name: values.industry,
          },
          freeze: true,
          callback: r => {
            loadMarketIndustry(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}
function openProductDialog(opportunityFrom, partyName) {
  const selectionDialog = new frappe.ui.Dialog({
    title: 'Mandatory Field Required',
    fields: [
      {
        label: 'Product',
        options: 'Item',
        reqd: 1,
        fieldname: 'cnp_product',
        fieldtype: 'Link',
      },
    ],

    primary_action_label: 'Create',
    primary_action: values => {
      createOpportunity(opportunityFrom, partyName, values.cnp_product);
      selectionDialog.hide();
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      selectionDialog.hide();
    },
  });
  selectionDialog.show();
}

function createOpportunity(opportunityFrom, partyName, product) {
  frappe.call({
    freeze: true,
    method: 'trusteeship_platform.custom_methods.make_new_opportunity',
    args: {
      opportunity_from: opportunityFrom,
      party_name: partyName,
      product,
    },
    callback: r => {
      frappe.set_route('Form', 'Opportunity', r.message.name);
    },
  });
}

function setLabelReferenceNumber(frm) {
  if (frm.doc.cnp_type === 'Others' || frm.doc.cnp_type === '') {
    frm.set_df_property('cin_number', 'reqd', false);
  } else {
    frm.set_df_property('cin_number', 'reqd', true);
  }
  frm.refresh_field('cin_number');

  // set labels of reference number for lead type
  if (
    frm.doc.cnp_type === 'Individual' ||
    frm.doc.cnp_type === 'Partnership Firm'
  ) {
    frm.set_df_property('cin_number', 'label', 'Reference Number (PAN)');
  } else if (frm.doc.cnp_type === 'Organization') {
    frm.set_df_property('cin_number', 'label', 'Reference Number (CIN)');
  } else if (frm.doc.cnp_type === 'LLP') {
    frm.set_df_property('cin_number', 'label', 'Reference Number (LLP)');
  } else if (
    frm.doc.cnp_type === 'Trustee' ||
    frm.doc.cnp_type === 'Foreign' ||
    frm.doc.cnp_type === 'Others'
  ) {
    frm.set_df_property(
      'cin_number',
      'label',
      'Reference Number (CIN/PAN/Others)',
    );
  } else if (frm.doc.cnp_type === '') {
    frm.set_df_property('cin_number', 'label', 'Reference Number ');
  }

  // set mandatory fields for lead types
  if (
    frm.doc.cnp_type === 'Individual' ||
    frm.doc.cnp_type === 'Partnership Firm' ||
    frm.doc.cnp_type === 'Trustee' ||
    frm.doc.cnp_type === 'Others'
  ) {
    frm.set_df_property('first_name', 'reqd', true);
    frm.set_df_property('company_name', 'reqd', false);
  } else if (
    frm.doc.cnp_type === 'Organization' ||
    frm.doc.cnp_type === 'Foreign'
  ) {
    frm.set_df_property('first_name', 'reqd', false);
    frm.set_df_property('company_name', 'reqd', true);
  } else if (frm.doc.cnp_type === 'LLP') {
    frm.set_df_property('first_name', 'reqd', true);
    frm.set_df_property('company_name', 'reqd', true);
  } else if (frm.doc.cnp_type === '') {
    frm.set_df_property('first_name', 'reqd', false);
    frm.set_df_property('company_name', 'reqd', false);
  }
  frm.refresh_field('first_name');
  frm.refresh_field('company_name');
}

function validateReferenceNumber(frm) {
  if (frm.doc.cin_number) {
    const patternCIN = /[A-Z0-9]{21}/;
    const patternPAN = /[A-Z]{5}[0-9]{4}[A-Z]{1}/;
    const patternLLP = /[A-Z]{3}[0-9]{4}/;
    const cin = frm.doc.cin_number.toUpperCase();
    if (frm.doc.cnp_type === 'Organization') {
      if (cin.match(patternCIN) && cin.length === 21) {
      } else {
        frappe.throw('Invalid CIN number');
      }
    } else if (frm.doc.cnp_type === 'LLP') {
      if (cin.match(patternLLP) && cin.length === 7) {
      } else {
        frappe.throw('Invalid LLP number');
      }
    } else if (
      frm.doc.cnp_type === 'Individual' ||
      frm.doc.cnp_type === 'Partnership Firm'
    ) {
      if (cin.match(patternPAN) && cin.length === 10) {
      } else {
        frappe.throw('Invalid PAN number');
      }
    } else {
      if (
        (cin.match(patternCIN) && cin.length === 21) ||
        (cin.match(patternPAN) && cin.length === 10)
      ) {
      } else {
        frappe.throw('Invalid CIN/PAN number');
      }
    }
    frm.doc.cin_number = cin;
    frm.refresh_field('cin_number');
  }
}

function hideCustomButtons(frm) {
  frm.remove_custom_button('Quotation', 'Create');
  frm.remove_custom_button('Prospect', 'Create');
  frm.remove_custom_button('Customer', 'Create');
  frm.remove_custom_button('Add to Prospect', 'Action');
  frm.page.menu
    .find(`span[data-label="${encodeURI('Create > Quotation')}"]`)
    .parent()
    .css({
      display: 'none',
    });
  frm.page.menu
    .find(`span[data-label="${encodeURI('Create > Prospect')}"]`)
    .parent()
    .css({
      display: 'none',
    });

  frm.page.menu
    .find(`span[data-label="${encodeURI('Create > Customer')}"]`)
    .parent()
    .css({
      display: 'none',
    });
  frm.page.menu
    .find(`span[data-label="${encodeURI('Action > Add to Prospect')}"]`)
    .parent()
    .css({
      display: 'none',
    });
}

function addOpportunityButton(frm) {
  if (!frm.doc.__islocal) {
    frm.add_custom_button(
      'Opportunity',
      () => {
        frappe.model.open_mapped_doc({
          method: 'erpnext.crm.doctype.lead.lead.make_opportunity',
          frm,
        });
      },
      'Create',
    );
  }
}

function changeAnnualRevenueLabel(frm) {
  frm.set_df_property('annual_revenue', 'label', 'Annual Revenue (in Cr.)');
}

function filterProduct(frm) {
  frm.set_query('cnp_product', function () {
    return {
      filters: [['is_purchase_item', '=', 0]],
    };
  });
}

function configureDealCloseDatepicker(frm) {
  frm.fields_dict.cnp_expected_deal_closure.datepicker.update({
    maxDate: new Date(
      frappe.datetime.add_days(frappe.datetime.get_today(), 180),
    ),
  });
}

function validateDealClosureDate(frm) {
  const expectedDealClosureDate = frm.doc.cnp_expected_deal_closure;
  const maxAllowedDate = frappe.datetime.add_days(
    frappe.datetime.get_today(),
    180,
  );

  if (expectedDealClosureDate > maxAllowedDate) {
    frappe.msgprint({
      title: __('Error'),
      message: __(
        'Expected Deal Closure date cannot be more than 180 days from the current date.',
      ),
      indicator: 'red',
    });

    frm.set_value('cnp_expected_deal_closure', '');
  }
}
