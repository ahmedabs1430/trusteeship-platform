/* global frappe */
/* eslint-env jquery */
frappe.ui.form.on('Note', {
  refresh: function (frm) {
    removeHidden(frm);
    makeFullPage(frm);
    hideEditButton(frm);
    template(frm);
  },
  cnp_fiscal_year: function (frm) {
    checkColumns(frm);
  },
});

function template(frm) {
  const template =
    '<div class="data-table-tree-container demo-target-3"></div>';
  const id = frm.doc.name;
  let columns = [];
  if (frm.doc.name === 'Note 6') {
    columns = getColumns(frm, columns);
  } else if (frm.doc.name === 'Note 7') {
    columns = getColumns(frm, columns);
  } else {
    let quarterMonths = lastDayOfEachMonth(
      frm,
      frm.doc.cnp_fiscal_year,
      'quarter',
    );
    let periodMonths = lastDayOfEachMonth(
      frm,
      frm.doc.cnp_fiscal_year,
      'period',
    );
    let yearMonth = lastDayOfEachMonth(frm, frm.doc.cnp_fiscal_year, 'year');

    quarterMonths = getMonths(frm, quarterMonths);
    periodMonths = getMonths(frm, periodMonths);
    yearMonth = getMonths(frm, yearMonth);
    const elementsToAdd = [
      {
        name: 'Particulars',
        width: 270,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
    ];
    quarterMonths.splice(0, 0, ...elementsToAdd);
    columns = [...quarterMonths, ...periodMonths, ...yearMonth];
  }
  if (frm.doc.cnp_fiscal_year) {
    datatable(frm, columns, template, id);
  }
}

function getColumns(frm, columns) {
  if (frm.doc.name === 'Note 6') {
    columns = [
      {
        name: 'Particulars',
        width: 275,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Computers',
        width: 150,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Computer Server',
        width: 150,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Mobile',
        width: 150,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Furniture and Fixtures',
        width: 150,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Office Equipment',
        width: 150,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Total',
        width: 150,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
    ];
  } else {
    columns = [
      {
        name: 'Particulars',
        width: 600,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
      {
        name: 'Computer Software',
        width: 580,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      },
    ];
  }
  return columns;
}

function datatable(frm, columns, template, id) {
  const data = getFieldValues(frm).year;

  frm.set_df_property(
    'cnp_notes_html',
    'options',
    frappe.render_template(template, {
      doc: frm.doc,
      id: frm.doc.__islocal === 1 ? '' : id,
      data,
      columns,
    }),
  );
  const element = $('.demo-target-3')[0];
  // eslint-disable-next-line no-new
  new DataTable(element, {
    columns,
    data,
    treeView: true,
    getEditor(colIndex, rowIndex, value, parent, column, row, data) {
      if (setReadonly(frm, colIndex, rowIndex)) {
        return null;
      } else {
        const dialog = new frappe.ui.Dialog({
          title: __('Edit Row'),
          fields: [
            {
              label: __('Particulars'),
              fieldname: 'Particulars',
              fieldtype: 'Data',
              read_only: true,
              default: data.Particulars,
            },
            {
              label: __('Amount'),
              fieldname: getData(frm, colIndex),
              fieldtype: 'Currency',
              reqd: 1,
              default: data[getData(frm, colIndex)],
            },
          ],
          primary_action_label: __('Update'),
          primary_action: function () {
            const values = dialog.get_values();
            const details = getFieldValues(frm).year;
            /* eslint-disable indent */
            const year = frm.doc.cnp_fiscal_year
              ? frm.doc.cnp_fiscal_year.replace(/-/g, '_')
              : frappe.defaults
                  .get_user_default('fiscal_year')
                  .replace(/-/g, '_');
            /* eslint-enable indent */

            for (let i = 0; i < details.length; i++) {
              if (details[i].id === data.id) {
                details[i][getData(frm, colIndex)] =
                  values[getData(frm, colIndex)];
              }
            }
            const cnp_note_details =
              frm.doc.cnp_note_details !== undefined
                ? JSON.parse(frm.doc.cnp_note_details)
                : {};
            cnp_note_details[year] = details;
            frm.doc.cnp_note_details = JSON.stringify(cnp_note_details);
            dialog.hide();
            frm.dirty();
            frm.save();
            getFieldValues(frm);
          },
          secondary_action_label: 'Cancel',
          secondary_action: () => {
            dialog.hide();
          },
        });
        dialog.show();
      }
    },
  });
}

function getMonths(frm, months) {
  months.sort(function (a, b) {
    const dateA = new Date(a.name);
    const dateB = new Date(b.name);

    if (dateA > dateB) {
      return -1;
    }
    if (dateA < dateB) {
      return 1;
    }
    return 0;
  });
  return months;
}

function removeHidden(frm) {
  frm.set_df_property('cnp_notes_html', 'hidden', 0);
  frm.set_df_property('cnp_fiscal_year', 'hidden', 0);
}

function hideEditButton(frm) {
  $("[data-label='Edit']").hide();
}

function setReadonly(frm, colIndex, rowIndex) {
  if (frm.doc.name === 'Note 1 to 5') {
    if (
      (colIndex === 2 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 12 ||
      rowIndex === 13 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 23 ||
      rowIndex === 24 ||
      rowIndex === 30 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 40 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      (colIndex === 3 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 12 ||
      rowIndex === 13 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 23 ||
      rowIndex === 24 ||
      rowIndex === 30 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 40 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      (colIndex === 4 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 12 ||
      rowIndex === 13 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 23 ||
      rowIndex === 24 ||
      rowIndex === 30 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 40 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      (colIndex === 5 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 12 ||
      rowIndex === 13 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 23 ||
      rowIndex === 24 ||
      rowIndex === 30 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 40 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      (colIndex === 6 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 12 ||
      rowIndex === 13 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 23 ||
      rowIndex === 24 ||
      rowIndex === 30 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 40 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      (colIndex === 7 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 12 ||
      rowIndex === 13 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 23 ||
      rowIndex === 24 ||
      rowIndex === 30 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 40 ||
      rowIndex === 50 ||
      rowIndex === 51
    ) {
      return true;
    } else {
      return false;
    }
  }

  if (frm.doc.name === 'Note 6') {
    if (
      (colIndex === 2 && rowIndex === 0) ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 10 ||
      rowIndex === 12 ||
      (colIndex === 3 && rowIndex === 0) ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 10 ||
      rowIndex === 12 ||
      (colIndex === 4 && rowIndex === 0) ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 10 ||
      rowIndex === 12 ||
      (colIndex === 5 && rowIndex === 0) ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 10 ||
      rowIndex === 12 ||
      (colIndex === 6 && rowIndex === 0) ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 10 ||
      rowIndex === 12
    ) {
      return true;
    } else {
      return false;
    }
  }

  if (frm.doc.name === 'Note 8 to 10') {
    if (
      (colIndex === 2 && rowIndex === 0) ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 26 ||
      rowIndex === 28 ||
      rowIndex === 33 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 41 ||
      rowIndex === 42 ||
      rowIndex === 47 ||
      rowIndex === 48 ||
      rowIndex === 49 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      rowIndex === 52 ||
      rowIndex === 54 ||
      rowIndex === 55 ||
      rowIndex === 57 ||
      rowIndex === 58 ||
      (colIndex === 3 && rowIndex === 0) ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 26 ||
      rowIndex === 28 ||
      rowIndex === 33 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 41 ||
      rowIndex === 42 ||
      rowIndex === 47 ||
      rowIndex === 48 ||
      rowIndex === 49 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      rowIndex === 52 ||
      rowIndex === 54 ||
      rowIndex === 55 ||
      rowIndex === 57 ||
      rowIndex === 58 ||
      (colIndex === 4 && rowIndex === 0) ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 26 ||
      rowIndex === 28 ||
      rowIndex === 33 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 41 ||
      rowIndex === 42 ||
      rowIndex === 47 ||
      rowIndex === 48 ||
      rowIndex === 49 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      rowIndex === 52 ||
      rowIndex === 54 ||
      rowIndex === 55 ||
      rowIndex === 57 ||
      rowIndex === 58 ||
      (colIndex === 5 && rowIndex === 0) ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 26 ||
      rowIndex === 28 ||
      rowIndex === 33 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 41 ||
      rowIndex === 42 ||
      rowIndex === 47 ||
      rowIndex === 48 ||
      rowIndex === 49 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      rowIndex === 52 ||
      rowIndex === 54 ||
      rowIndex === 55 ||
      rowIndex === 57 ||
      rowIndex === 58 ||
      (colIndex === 6 && rowIndex === 0) ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 26 ||
      rowIndex === 28 ||
      rowIndex === 33 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 41 ||
      rowIndex === 42 ||
      rowIndex === 47 ||
      rowIndex === 48 ||
      rowIndex === 49 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      rowIndex === 52 ||
      rowIndex === 54 ||
      rowIndex === 55 ||
      rowIndex === 57 ||
      rowIndex === 58 ||
      (colIndex === 7 && rowIndex === 0) ||
      rowIndex === 3 ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 8 ||
      rowIndex === 9 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 26 ||
      rowIndex === 28 ||
      rowIndex === 33 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 37 ||
      rowIndex === 38 ||
      rowIndex === 41 ||
      rowIndex === 42 ||
      rowIndex === 47 ||
      rowIndex === 48 ||
      rowIndex === 49 ||
      rowIndex === 50 ||
      rowIndex === 51 ||
      rowIndex === 52 ||
      rowIndex === 54 ||
      rowIndex === 55 ||
      rowIndex === 57 ||
      rowIndex === 58
    ) {
      return true;
    } else {
      return false;
    }
  }

  if (frm.doc.name === 'Note 7') {
    if (
      (colIndex === 2 && rowIndex === 0) ||
      rowIndex === 4 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 10 ||
      rowIndex === 12
    ) {
      return true;
    } else {
      return false;
    }
  }

  if (frm.doc.name === 'Note 13 to 15') {
    if (
      (colIndex === 2 && rowIndex === 4) ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 39 ||
      (colIndex === 3 && rowIndex === 4) ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 39 ||
      (colIndex === 4 && rowIndex === 4) ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 39 ||
      (colIndex === 5 && rowIndex === 4) ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 39 ||
      (colIndex === 6 && rowIndex === 4) ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 39 ||
      (colIndex === 7 && rowIndex === 4) ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 35 ||
      rowIndex === 36 ||
      rowIndex === 39
    ) {
      return true;
    } else {
      return false;
    }
  }

  if (frm.doc.name === 'Note 11 to 12') {
    if (
      (colIndex === 2 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 13 ||
      rowIndex === 14 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 21 ||
      rowIndex === 22 ||
      rowIndex === 23 ||
      (colIndex === 3 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 13 ||
      rowIndex === 14 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 21 ||
      rowIndex === 22 ||
      rowIndex === 23 ||
      (colIndex === 4 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 13 ||
      rowIndex === 14 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 21 ||
      rowIndex === 22 ||
      rowIndex === 23 ||
      (colIndex === 5 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 13 ||
      rowIndex === 14 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 21 ||
      rowIndex === 22 ||
      rowIndex === 23 ||
      (colIndex === 6 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 13 ||
      rowIndex === 14 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 21 ||
      rowIndex === 22 ||
      rowIndex === 23 ||
      (colIndex === 7 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 13 ||
      rowIndex === 14 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      rowIndex === 20 ||
      rowIndex === 21 ||
      rowIndex === 22 ||
      rowIndex === 23
    ) {
      return true;
    } else {
      return false;
    }
  }
}

function getData(frm, colIndex) {
  const fiscalYearStart = frm.doc.cnp_fiscal_year.split('-')[0];
  const fiscalYearEnd = frm.doc.cnp_fiscal_year.split('-')[1];
  let data;
  if (frm.doc.name === 'Note 6') {
    /* eslint-disable indent */
    data =
      colIndex === 2
        ? 'Computers'
        : colIndex === 3
        ? 'Computer Server'
        : colIndex === 4
        ? 'Mobile'
        : colIndex === 5
        ? 'Furniture and Fixtures'
        : colIndex === 6
        ? 'Office Equipment'
        : colIndex === 7
        ? 'Total'
        : 'amount';
  } else if (frm.doc.name === 'Note 7') {
    data = colIndex === 2 ? 'Computer Software' : 'amount';
  } else {
    data =
      colIndex === 2
        ? 'Sep 30,  ' + fiscalYearEnd
        : colIndex === 3
        ? 'June 30,  ' + fiscalYearEnd
        : colIndex === 4
        ? 'Sep 30,  ' + fiscalYearStart
        : colIndex === 5
        ? 'Sep 30, ' + fiscalYearEnd
        : colIndex === 6
        ? 'Sep 30, ' + fiscalYearStart
        : colIndex === 7
        ? 'Mar 31, ' + fiscalYearEnd
        : 'amount';
  }
  return data;
}

function formatFileName(cell, row, frm) {
  let indent;
  for (let index = 0; index < row.length; index++) {
    const element = row[index];
    const formattedNumber = cell.toLocaleString('en-US', {
      style: 'currency',
      currency: 'INR',
    });
    if (frm.doc.name === 'Note 6') {
      if (element.colIndex === 7) {
        element.column.editable = false;
      }
    }
    if (element.colIndex === 1) {
      element.column.editable = false;
    }
    if (
      element.indent === 0 ||
      (frm.doc.name === 'Note 1 to 5' &&
        (element.rowIndex === 3 ||
          element.rowIndex === 6 ||
          element.rowIndex === 12 ||
          element.rowIndex === 18 ||
          element.rowIndex === 23 ||
          element.rowIndex === 30 ||
          element.rowIndex === 36 ||
          element.rowIndex === 40 ||
          element.rowIndex === 50))
    ) {
      indent =
        '<a row="' +
        element.colIndex +
        '-' +
        element.rowIndex +
        '"><b><span style="margin-left:' +
        row.indent * 20 +
        'px;">' +
        formattedNumber +
        '</span></b></a>';
    } else {
      indent =
        '<a row="' +
        element.colIndex +
        '-' +
        element.rowIndex +
        '">' +
        formattedNumber +
        '</span></a>';
    }
  }

  return indent;
}

function getFieldValues(frm) {
  frm.doc.cnp_note_details = frm.doc.cnp_note_details
    ? frm.doc.cnp_note_details
    : JSON.stringify({});
  const details =
    frm.doc.cnp_note_details !== undefined
      ? JSON.parse(frm.doc.cnp_note_details)
      : {};
  let year = frm.doc.cnp_fiscal_year
    ? frm.doc.cnp_fiscal_year.replace(/-/g, '_')
    : frappe.defaults.get_user_default('fiscal_year').replace(/-/g, '_');
  year = details[year];

  return {
    year,
  };
}

function makeFullPage(frm) {
  frm.page
    .set_primary_action(__('Save'), function () {
      frm.save();
    })
    .addClass('btn-primary');
  frm.page.clear_user_actions();
  frm.page.set_title(__(frm.doc.name));
  frm.page.sidebar.hide();
  frm.page.wrapper.find('.navbar').toggle(false);
  frm.page.wrapper.find('.page-footer').toggle(false);
  frm.page.full_page = 1;
  frm.page.wrapper.find('.comment-box').css({ display: 'none' });
}
function lastDayOfEachMonth(frm, fiscalYear, columnType) {
  const fiscalYearStart = new Date(`${fiscalYear.split('-')[0]}-01-01`);
  const fiscalYearEnd = new Date(`${fiscalYear.split('-')[1]}-12-31`);

  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  const lastDays = [];
  for (
    let year = fiscalYearStart.getFullYear();
    year <= fiscalYearEnd.getFullYear();
    year++
  ) {
    for (let month = 0; month < 12; month++) {
      const lastDay = new Date(year, month + 1, 0);
      if (lastDay >= fiscalYearStart && lastDay <= fiscalYearEnd) {
        getVisible(
          frm,
          monthNames[lastDay.getMonth()],
          lastDay.getDate(),
          lastDay.getFullYear(),
          lastDays,
          columnType,
          fiscalYearStart.getFullYear(),
          fiscalYearEnd.getFullYear(),
        );
      }
    }
  }
  return lastDays;
}

function getVisible(
  frm,
  month,
  date,
  year,
  lastDays,
  columnType,
  fiscalYearEnd,
  fiscalYearStart,
) {
  if (columnType === 'quarter') {
    if (
      month + ' ' + date + ',  ' + year === 'Sep 30, ' + ' ' + fiscalYearEnd ||
      month + ' ' + date + ',  ' + year ===
        'June 30, ' + ' ' + fiscalYearStart ||
      month + ' ' + date + ',  ' + year === 'Sep 30, ' + ' ' + fiscalYearStart
    ) {
      lastDays.push({
        name: month + ' ' + date + ',  ' + year,
        fieldtype: 'Currency',
        width: 150,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  } else if (columnType === 'period') {
    if (
      month + ' ' + date + ', ' + year === 'Sep 30, ' + fiscalYearEnd ||
      month + ' ' + date + ', ' + year === 'Sep 30, ' + fiscalYearStart
    ) {
      lastDays.push({
        name: month + ' ' + date + ', ' + year,
        fieldtype: 'Currency',
        width: 150,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  } else if (columnType === 'year') {
    if (month + ' ' + date + ', ' + year === 'Mar 31, ' + fiscalYearStart) {
      lastDays.push({
        name: month + ' ' + date + ', ' + year,
        fieldtype: 'Currency',
        width: 150,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, default_formatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  }
}

function checkColumns(frm) {
  const fiscalYearStart = frm.doc.cnp_fiscal_year.split('-')[0];
  const fiscalYearEnd = frm.doc.cnp_fiscal_year.split('-')[1];
  frm.doc.cnp_note_details = frm.doc.cnp_note_details
    ? frm.doc.cnp_note_details
    : JSON.stringify({});
  const details =
    frm.doc.cnp_note_details === undefined
      ? {}
      : JSON.parse(frm.doc.cnp_note_details);
  const year = frm.doc.cnp_fiscal_year
    ? frm.doc.cnp_fiscal_year.replace(/-/g, '_')
    : frappe.defaults.get_user_default('fiscal_year').replace(/-/g, '_');

  if (details[year] === undefined && frm.doc.cnp_fiscal_year) {
    if (frm.doc.name === 'Note 1 to 5') {
      details[year] = [
        {
          Particulars: 'Note 1: Share Capital',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'Authorised',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 2,
        },
        {
          Particulars: '50,00,000 Equity Shares of Rs.10/- each',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 3,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 4,
        },
        {
          Particulars: 'Subscribed and Fully Paid-up',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 5,
        },
        {
          Particulars: '1,500,000 Equity Shares of Rs. 10/- each',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 6,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 7,
        },
        {
          Particulars:
            'The above Equity Shares are entirely held by Holding Company and its nominees.',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 8,
        },
        {
          Particulars: 'Note 2: Reserves And Surplus',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 9,
        },
        {
          Particulars: 'General Reserve',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 10,
        },
        {
          Particulars: 'As per last Account',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 11,
        },
        {
          Particulars: 'Add: Transfer from Profit & Loss Account',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 12,
        },
        {
          Particulars: 'a',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 13,
        },
        {
          Particulars: 'Surplus',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 14,
        },
        {
          Particulars: 'As per last Account',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 15,
        },
        {
          Particulars: 'Profit/(Loss) during the period',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 16,
        },
        {
          Particulars: 'Proposed Dividend',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 17,
        },
        {
          Particulars: 'Transfer to General Reserve',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 18,
        },
        {
          Particulars: 'b',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 19,
        },
        {
          Particulars: '(a+b)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 20,
        },
        {
          Particulars: 'Note 3: Other Long Term Liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 21,
        },
        {
          Particulars: 'Unearned Revenue',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 22,
        },
        {
          Particulars: 'Lease Equalisation Account',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 23,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 24,
        },
        {
          Particulars: 'Note 4: Provisions',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 25,
        },
        {
          Particulars: 'Long Term',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 26,
        },
        {
          Particulars: 'Provision for gratuity',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 27,
        },
        {
          Particulars: 'Provision for Leave Benefits',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 28,
        },
        {
          Particulars: 'Retention Pay',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 29,
        },
        {
          Particulars: 'Provision for Employee Benefits (Variable Pay)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 30,
        },
        {
          Particulars: 'a',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 31,
        },
        {
          Particulars: 'Short Term',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 32,
        },
        {
          Particulars:
            'Provision for Employee Benefits (Variable Pay)Incl.Deputed',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 33,
        },
        {
          Particulars: 'Provision for gratuity',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 34,
        },
        {
          Particulars: 'Provision for Leave Benefits',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 35,
        },
        {
          Particulars: 'Provision For Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 36,
        },
        {
          Particulars: 'b',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 37,
        },
        {
          Particulars: '(a + b)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 38,
        },
        {
          Particulars: 'Note 5: Other Current Liabilites',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 39,
        },
        {
          Particulars: 'Trades Payable',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 40,
        },
        {
          Particulars: 'a',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 41,
        },
        {
          Particulars: 'Other Liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 42,
        },
        {
          Particulars: 'Unearned Revenue',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 43,
        },
        {
          Particulars: 'Other Advances',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 44,
        },
        {
          Particulars: 'Employees Contribution to PF/ESIC',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 45,
        },
        {
          Particulars: 'Professional Tax',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 46,
        },
        {
          Particulars: 'Due to the Holding Company - Axis Bank Limited',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 47,
        },
        {
          Particulars: 'TDS Payable',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 48,
        },
        {
          Particulars: 'Service/GST Tax Payable',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 49,
        },
        {
          Particulars: 'Income Tax Liability',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 50,
        },
        {
          Particulars: 'b',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 51,
        },
        {
          Particulars: '(a + b)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 52,
        },
      ];
    }
    if (frm.doc.name === 'Note 6') {
      details[year] = [
        {
          Particulars: 'Gross block',
          Computers: ' ',
          'Computer Server': ' ',
          Mobile: ' ',
          'Furniture and Fixtures': ' ',
          'Office Equipment': ' ',
          Total: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'At April 1,  ' + fiscalYearEnd,
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 1,
          id: 2,
        },
        {
          Particulars: 'Additions',
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 2,
          id: 3,
        },
        {
          Particulars: 'Disposals',
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 2,
          id: 4,
        },
        {
          Particulars: 'At Sep 30,  ' + fiscalYearEnd,
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 0,
          id: 5,
        },
        {
          Particulars: ' ',
          Computers: ' ',
          'Computer Server': ' ',
          Mobile: ' ',
          'Furniture and Fixtures': ' ',
          'Office Equipment': ' ',
          Total: ' ',
          indent: 0,
          id: 6,
        },
        {
          Particulars: 'Depreciation',
          Computers: ' ',
          'Computer Server': ' ',
          Mobile: ' ',
          'Furniture and Fixtures': ' ',
          'Office Equipment': ' ',
          Total: ' ',
          indent: 0,
          id: 7,
        },
        {
          Particulars: 'At April 1,  ' + fiscalYearEnd,
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 1,
          id: 8,
        },
        {
          Particulars: 'Charge for the period',
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 2,
          id: 9,
        },
        {
          Particulars: 'Disposals',
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 2,
          id: 10,
        },
        {
          Particulars: 'At Sep 30,  ' + fiscalYearEnd,
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 0,
          id: 11,
        },
        {
          Particulars: ' ',
          Computers: ' ',
          'Computer Server': ' ',
          Mobile: ' ',
          'Furniture and Fixtures': ' ',
          'Office Equipment': ' ',
          Total: ' ',
          indent: 0,
          id: 12,
        },
        {
          Particulars: 'Net Block',
          Computers: ' ',
          'Computer Server': ' ',
          Mobile: ' ',
          'Furniture and Fixtures': ' ',
          'Office Equipment': ' ',
          Total: ' ',
          indent: 0,
          id: 13,
        },
        {
          Particulars: 'At Sep 30,  ' + fiscalYearEnd,
          Computers: 0,
          'Computer Server': 0,
          Mobile: 0,
          'Furniture and Fixtures': 0,
          'Office Equipment': 0,
          Total: 0,
          indent: 1,
          id: 14,
        },
      ];
    }
    if (frm.doc.name === 'Note 7') {
      details[year] = [
        {
          Particulars: 'Gross block',
          'Computer Software': ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'At April 1,  ' + fiscalYearEnd,
          'Computer Software': 0,
          indent: 1,
          id: 2,
        },
        {
          Particulars: 'Additions',
          'Computer Software': 0,
          indent: 2,
          id: 3,
        },
        {
          Particulars: 'Disposals',
          'Computer Software': 0,
          indent: 2,
          id: 4,
        },
        {
          Particulars: 'At Sep 30,  ' + fiscalYearEnd,
          'Computer Software': 0,
          indent: 0,
          id: 5,
        },
        {
          Particulars: ' ',
          'Computer Software': ' ',
          indent: 0,
          id: 6,
        },
        {
          Particulars: 'Depreciation',
          'Computer Software': ' ',
          indent: 0,
          id: 7,
        },
        {
          Particulars: 'At April 1,  ' + fiscalYearEnd,
          'Computer Software': 0,
          indent: 1,
          id: 8,
        },
        {
          Particulars: 'Charge for the period',
          'Computer Software': 0,
          indent: 2,
          id: 9,
        },
        {
          Particulars: 'Disposals',
          'Computer Software': 0,
          indent: 2,
          id: 10,
        },
        {
          Particulars: 'At Sep 30,  ' + fiscalYearEnd,
          'Computer Software': 0,
          indent: 0,
          id: 11,
        },
        {
          Particulars: ' ',
          'Computer Software': ' ',
          indent: 0,
          id: 12,
        },
        {
          Particulars: 'Net Block',
          'Computer Software': ' ',
          indent: 0,
          id: 13,
        },
        {
          Particulars: 'At Sep 30,  ' + fiscalYearEnd,
          'Computer Software': 0,
          indent: 1,
          id: 14,
        },
      ];
    }
    if (frm.doc.name === 'Note 11 to 12') {
      details[year] = [
        {
          Particulars: 'Note 11: Revenue from Operations',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'Trusteeship Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 2,
        },
        {
          Particulars: 'Initial Acceptance Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 3,
        },
        {
          Particulars: 'Annual Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 4,
        },
        {
          Particulars: 'Servicing Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 5,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 6,
        },
        {
          Particulars: 'Note 12 : Other Income',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 7,
        },
        {
          Particulars: 'Interest on Fixed Deposits',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 8,
        },
        {
          Particulars: 'Capital Gain on Mutual Fund',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 9,
        },
        {
          Particulars: 'Profit of FA',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 10,
        },
        {
          Particulars: 'FC Exchange Gain',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 11,
        },
        {
          Particulars: 'Insurance claim received',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 12,
        },
        {
          Particulars: 'Recovery of Bad Debts',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 13,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 14,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 15,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 16,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 17,
        },
        {
          Particulars: 'Initial Acceptance Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 18,
        },
        {
          Particulars: 'Annual Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 19,
        },
        {
          Particulars: 'Servicing Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 20,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 21,
        },
        {
          Particulars: 'Adjusted Operating Income',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 22,
        },
        {
          Particulars: 'Add GST @ 12%',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 23,
        },
        {
          Particulars: 'Total',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 24,
        },
      ];
    }
    if (frm.doc.name === 'Note 8 to 10') {
      details[year] = [
        {
          Particulars: '8. Investments',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'Current Non- Trade Investments',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 2,
        },
        {
          Particulars: 'Investments in Liquid Funds',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 3,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 4,
        },
        {
          Particulars: '8.1  Loans and Advances',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 5,
        },
        {
          Particulars: 'Non-Current',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 6,
        },
        {
          Particulars: 'Advance Payament of Taxes',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 7,
        },
        {
          Particulars: 'Less Shown Separtaely in other current liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 8,
        },
        {
          Particulars: 'a',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 9,
        },
        {
          Particulars: 'Current',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 10,
        },
        {
          Particulars: 'Prepaid Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 11,
        },
        {
          Particulars: 'Deposit with Central Registry',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 12,
        },
        {
          Particulars: 'Advance with Corporate Credit Card',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 13,
        },
        {
          Particulars: 'Other Advances',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 14,
        },
        {
          Particulars: 'Service Tax/GST Receivable',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 15,
        },
        {
          Particulars: 'Advance to Reliance Commercial Finance',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 16,
        },
        {
          Particulars: 'b',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 17,
        },
        {
          Particulars: '(a+b)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 18,
        },
        {
          Particulars: 'Note 9:  Trade Receivables',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 19,
        },
        {
          Particulars: 'Current',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 20,
        },
        {
          Particulars: 'Unsecured, considered good unless stated otherwise',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 21,
        },
        {
          Particulars: 'Outstanding for a period exceeding six months from ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 22,
        },
        {
          Particulars: 'the date they are due for payment',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 23,
        },
        {
          Particulars: 'Secured, Considered good',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 24,
        },
        {
          Particulars: 'Unsecured, Considered good',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 25,
        },
        {
          Particulars: 'Doubtful',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 26,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 27,
        },
        {
          Particulars: 'Provision for doubtful receivables',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 28,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 29,
        },
        {
          Particulars: 'Other receivables( less than 180 days)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 30,
        },
        {
          Particulars: 'Secured, Considered good',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 31,
        },
        {
          Particulars: 'Unsecured, Considered good',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 32,
        },
        {
          Particulars: 'Doubtful',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 33,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 34,
        },
        {
          Particulars: 'Provision for doubtful receivables',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 35,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 36,
        },
        {
          Particulars: 'Total',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 37,
        },
        {
          Particulars: '9.2 Other Assets',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 38,
        },
        {
          Particulars: 'Non-Current',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 39,
        },
        {
          Particulars: 'Unsecured, considered good unless stated otherwise',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 40,
        },
        {
          Particulars:
            'Non-current Bank Balances (FD maturing after 12 months)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 41,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 42,
        },
        {
          Particulars: 'Current',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 43,
        },
        {
          Particulars: 'Due from the Holding Company - Axis Bank Limited',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 44,
        },
        {
          Particulars: 'Due from other subsidiaries',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 45,
        },
        {
          Particulars: 'Other Deposits',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 46,
        },
        {
          Particulars: 'Interest Accrued on Fixed Deposits',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 47,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 48,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 49,
        },
        {
          Particulars: 'Note 10: Cash & Bank Balances',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 50,
        },
        {
          Particulars: 'Current',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 51,
        },
        {
          Particulars: 'Cash and Cash Equivalents',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 52,
        },
        {
          Particulars: 'Balances with banks:',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 53,
        },
        {
          Particulars: 'On current Account',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 54,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 55,
        },
        {
          Particulars: 'Other Bank Balances :',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 56,
        },
        {
          Particulars:
            'Deposit with remaining maturity for less than 12 months',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 57,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 58,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 59,
        },
      ];
    }
    if (frm.doc.name === 'Note 13 to 15') {
      details[year] = [
        {
          Particulars: 'Note 13 : Employee Benefit Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'Salaries & Wages',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 2,
        },
        {
          Particulars: 'Contribution to Provident, Gratuity and Other Funds',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 3,
        },
        {
          Particulars: 'Staff Welfare Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 4,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 5,
        },
        {
          Particulars: 'Note 14 : Other Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 6,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 7,
        },
        {
          Particulars: 'Rent',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 8,
        },
        {
          Particulars: 'Rates and Taxes',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 9,
        },
        {
          Particulars: 'Printing and Stationery',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 10,
        },
        {
          Particulars: 'Travelling & Conveyance',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 11,
        },
        {
          Particulars: 'Conference Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 12,
        },
        {
          Particulars: 'Communication Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 13,
        },
        {
          Particulars: 'Legal & Professional Charges',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 14,
        },
        {
          Particulars: 'Auditors Remuneration:',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 15,
        },
        {
          Particulars: 'As Auditors',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 16,
        },
        {
          Particulars: 'In other capacity for Certificates and other Services',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 17,
        },
        {
          Particulars: 'Directors Sitting Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 18,
        },
        {
          Particulars: 'Electricity Charges',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 19,
        },
        {
          Particulars: 'Bank Charges',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 20,
        },
        {
          Particulars: 'Doubtful Debts Written Off',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 21,
        },
        {
          Particulars: 'Provision for Doubtful Debts',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 22,
        },
        {
          Particulars: 'DP Charges',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 23,
        },
        {
          Particulars: 'Office Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 24,
        },
        {
          Particulars: 'Advertisement & Business Promotion Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 25,
        },
        {
          Particulars: 'Annual Maintainence Charges of Software',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 26,
        },
        {
          Particulars: 'Registeration Fee',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 27,
        },
        {
          Particulars: 'Contribution for CSR activities',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 28,
        },
        {
          Particulars: 'Royalty Charges',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 29,
        },
        {
          Particulars: 'Referral Fees',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 30,
        },
        {
          Particulars: 'Other Payments',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 31,
        },
        {
          Particulars: 'Exchange Loss',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 32,
        },
        {
          Particulars: 'Microsoft Licence Fees / website Development',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 33,
        },
        {
          Particulars: 'Bad debts recognised',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 34,
        },
        {
          Particulars: 'Less : write Off',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 35,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 36,
        },
        {
          Particulars: 'Note 15: Depreciation & Amortisation',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 37,
        },
        {
          Particulars: 'On Tangible Fixed Assets',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 38,
        },
        {
          Particulars: 'On Intangible Assets',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 39,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 40,
        },
      ];
    }

    frm.doc.cnp_note_details = JSON.stringify(details);
    frm.save();
  }
}
