/* global frappe */
frappe.ui.form.on('Address', {
  refresh: frm => {
    setPincodeReqd(frm);
    checkPrevRoute(frm);
    hideDefaultAddressCheckbox(frm);
  },

  country: frm => {
    setPincodeReqd(frm);
  },

  address_type: frm => {
    hideDefaultAddressCheckbox(frm);
  },

  is_your_company_address: frm => {
    hideDefaultAddressCheckbox(frm);
  },
});

function checkPrevRoute() {
  const prevRoute = frappe.get_prev_route();
  if (prevRoute[1] === 'Quotation' || prevRoute[1] === 'Sales Invoice') {
    localStorage.setItem('address_update', '0');
  }
}

function setPincodeReqd(frm) {
  if (frm.doc.country === 'India') {
    frm.toggle_reqd('pincode', true);
  } else {
    frm.toggle_reqd('pincode', false);
  }
}

function hideDefaultAddressCheckbox(frm) {
  const isHidden = !(
    frm.doc.links.some(x => x.link_doctype === 'Company') &&
    frm.doc.address_type === 'Registered Office'
  );

  frm.set_df_property(
    'cnp_is_default_offer_letter_address',
    'hidden',
    isHidden,
  );
}

frappe.ui.form.on('Dynamic Link', {
  links_add: (frm, cdt, cdn) => {
    hideDefaultAddressCheckbox(frm);
  },

  links_edit: (frm, cdt, cdn) => {
    hideDefaultAddressCheckbox(frm);
  },

  links_remove: (frm, cdt, cdn) => {
    hideDefaultAddressCheckbox(frm);
  },

  link_doctype: (frm, cdt, cdn) => {
    hideDefaultAddressCheckbox(frm);
  },
});
