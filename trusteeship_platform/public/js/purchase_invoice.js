/* global frappe */

frappe.ui.form.on('Purchase Invoice', {
  refresh: frm => {
    setNamingSeries(frm);
    frm.refresh_field('naming_series');
    if (!frm.doc.__islocal) {
      approvalAction(frm);
    } else if (frm.doc.__islocal && frm.doc.amended_from) {
      frm.doc.cnp_action = 0;
    }
    addApprovalPill(frm);
    frm.set_df_property('company', 'hidden', false);
    frm.cost_center_initial_value = frm.doc.cost_center;
    frm.confirmation_in_progress = false;
    setCostCenterFilter(frm);
  },

  after_save: frm => {
    frm.reload_doc();
  },

  onload_post_render: frm => {
    frm.set_df_property('company', 'hidden', false);
    frm.set_query('item_code', 'items', function () {
      return {
        filters: { is_purchase_item: 1, is_sales_item: 0 },
      };
    });
    clearBillingShippingAddress(frm);
    setCostCenterFilter(frm);
  },
  cost_center(frm) {
    if (frm?.cost_center_initial_value) {
      checkCostCenter(frm);
    }
    setDefaultShippingBillingAddress(frm);
    setCostCenterItem(frm);
    setDefaultCompanyAddress(frm);
  },
  /* eslint-disable */
  onload: function (frm) {
    /* eslint-enable */
    if (
      frm.selected_doc.tax_withholding_category &&
      frm.selected_doc.apply_tds
    ) {
      frappe.db
        .get_doc(
          'Tax Withholding Category',
          frm.selected_doc.tax_withholding_category,
        )
        .then(doc => {
          const company = frm.selected_doc.company;
          const postingDate = frm.selected_doc.posting_date;

          const account = doc.accounts.find(
            account => account.company === company,
          )?.account;
          const taxWithholdingRates = doc.rates
            .filter(
              rate =>
                rate.from_date <= postingDate && postingDate <= rate.to_date,
            )
            .map(rate => rate.tax_withholding_rate);

          const rate = Math.max(...taxWithholdingRates);

          frm.selected_doc.taxes.forEach(tax => {
            if (tax.account_head === account) {
              tax.rate = rate;
            }
          });

          cur_frm.refresh_field('taxes');
        });
    }
  },
});

function checkCostCenter(frm) {
  if (
    frm.doc.cost_center === frm.cost_center_initial_value ||
    frm.confirmation_in_progress
  ) {
    return;
  }
  frappe.validated = false;
  frm.confirmation_in_progress = true;

  frappe.confirm(
    `Do you wish to change the Cost Center from ${frm.cost_center_initial_value} to ${frm.doc.cost_center}?`,
    function () {
      frm.cost_center_initial_value = frm.doc.cost_center;
      frappe.validated = true;
      frm.confirmation_in_progress = false;
      if (frm.doc.__islocal) {
        frm.set_value('cost_center', frm.doc.cost_center);
        frm.refresh_field('cost_center');
      } else {
        frm.save();
      }
    },
    function () {
      frm.set_value('cost_center', frm.cost_center_initial_value);
      frm.confirmation_in_progress = false;
    },
  );
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'Draft' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.page.add_action_item('Send For Approval', function () {
      sendApprovalMail(frm);
    });
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.page.add_action_item('Approve', function () {
      frm.doc.cnp_action = 1;
      frm.savesubmit();
    });
    frm.page.add_action_item('Reject', function () {
      emailActionReject(frm);
    });
  }
}
function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Purchase Invoice',
      doctype_name: 'purchase-invoice',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function emailActionReject(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.email_action',
    args: {
      doctype: 'Purchase Invoice',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'purchase-invoice',
      redirect: false,
      email_token: frm.doc.cnp_email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function addApprovalPill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let titleElement = $(`h3[title="${frm.doc.supplier_name}"]`).parent();
    titleElement = titleElement[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.workflow_state;
    pillspan.appendChild(textspan);

    titleElement.appendChild(pillspan);
  }
}

function setCostCenterFilter(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'Cost Center',
      filters: [['cnp_not_in_filter', '=', 1]],
      fields: '["name"]',
      pluck: 'name',
    },
    callback: r => {
      if (r.message.length !== 0) {
        frm.set_query('cost_center', filter => {
          return {
            filters: [
              ['Cost Center', 'is_group', '=', 0],
              ['Cost Center', 'name', 'not in', r.message],
              ['Cost Center', 'company', '=', frm.doc.company],
            ],
          };
        });
      } else {
        frm.set_query('cost_center', filter => {
          return {
            filters: [
              ['Cost Center', 'is_group', '=', 0],
              ['Cost Center', 'company', '=', frm.doc.company],
            ],
          };
        });
      }
    },
  });
}

function setNamingSeries(frm) {
  frm.set_df_property(
    'naming_series',
    'options',
    'AT-CO-.YY.-P-.\nPINV-.YY.-\nPRET-.YY.-\nACC-PINV-.YYYY.-\nACC-PINV-RET-.YYYY.-',
  );
  frm.doc.naming_series = 'AT-CO-.YY.-P-.';
  frm.refresh_field('naming_series');
}

function clearBillingShippingAddress(frm) {
  if (frm.doc.__islocal) {
    frm.doc.shipping_address = '';
    frm.refresh_field('shipping_address');
    frm.doc.shipping_address_display = '';
    frm.refresh_field('shipping_address_display');
    frm.doc.shipping_address = '';
    frm.refresh_field('billing_address');
    frm.doc.shipping_address_display = '';
    frm.refresh_field('billing_address_display');
  }
}

function setDefaultShippingBillingAddress(frm) {
  frm.set_query('shipping_address', function () {
    return {
      filters: {
        link_doctype: 'Company',
        link_name: frm.doc.company,
        cnp_cost_center: frm.doc.cost_center,
      },
    };
  });
  frm.set_query('billing_address', function () {
    return {
      filters: {
        link_doctype: 'Company',
        link_name: frm.doc.company,
        cnp_cost_center: frm.doc.cost_center,
      },
    };
  });
}

function setDefaultCompanyAddress(frm) {
  if (frm.doc.cost_center) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.cost_center_default_address',
      args: {
        cost_center: frm.doc.cost_center,
      },
      callback: r => {
        let default_address = r.message;
        if (default_address === undefined) {
          default_address = '';
        }
        frm.doc.shipping_address = default_address;
        frm.refresh_field('shipping_address');
        frm.trigger('shipping_address');
        frm.doc.billing_address = default_address;
        frm.refresh_field('billing_address');
        frm.trigger('billing_address');
      },
    });
  }
}
function setCostCenterItem(frm) {
  frm.doc.items.forEach(item => {
    item.cost_center = frm.doc.cost_center;
  });
  frm.refresh_field('items');
}
