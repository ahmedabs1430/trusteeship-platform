/* global frappe, locals */
/* eslint-env jquery */
frappe.ui.form.on('Journal Entry', {
  refresh: frm => {
    setNamingSeries(frm);
    frm.set_df_property('finance_book', 'read_only', false);
    addApprovalPill(frm);
    if (!frm.doc.__islocal) {
      approvalAction(frm);
    } else if (frm.doc.__islocal && frm.doc.amended_from) {
      frm.doc.cnp_action = 0;
    }
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: {
        doctype: 'Cost Center',
        filters: [['cnp_not_in_filter', '=', 1]],
        fields: '["name"]',
        pluck: 'name',
      },
      callback: r => {
        if (r.message.length !== 0) {
          frm.set_query('cnp_cost_center', filter => {
            return {
              filters: [
                ['Cost Center', 'is_group', '=', 0],
                ['Cost Center', 'name', 'not in', r.message],
                ['Cost Center', 'company', '=', frm.doc.company],
              ],
            };
          });
        } else {
          frm.set_query('cnp_cost_center', filter => {
            return {
              filters: [
                ['Cost Center', 'is_group', '=', 0],
                ['Cost Center', 'company', '=', frm.doc.company],
              ],
            };
          });
        }
      },
    });
  },
  onload_post_render: frm => {
    frm.confirmation_in_progress = false;
    frm.previous_cost_centers = {};
    frm.doc.accounts.forEach(acc => {
      frm.previous_cost_centers[acc.name] = acc.cost_center;
    });
  },
});
frappe.ui.form.on('Journal Entry Account', {
  cost_center: function (frm, cdt, cdn) {
    checkCostCenter(frm, cdt, cdn);
  },
  accounts_add: function (frm, cdt, cdn) {
    const childDoc = locals[cdt][cdn];
    frm.previous_cost_centers[cdn] = childDoc.cost_center;
  },
  accounts_remove: function (frm, cdt, cdn) {
    delete frm.previous_cost_centers[cdn];
  },
});

function checkCostCenter(frm, cdt, cdn) {
  const childDoc = locals[cdt][cdn];
  let previousCostCenter = frm.previous_cost_centers[cdn];
  if (
    previousCostCenter &&
    previousCostCenter !== childDoc.cost_center &&
    !frm.confirmation_in_progress
  ) {
    frappe.validated = false;
    frm.confirmation_in_progress = true;
    frappe.confirm(
      `Do you wish to change the Cost Center from ${previousCostCenter} to ${childDoc.cost_center}?`,
      function () {
        frm.previous_cost_centers[cdn] = childDoc.cost_center;
        previousCostCenter = childDoc.cost_center;
        frm.confirmation_in_progress = false;
        frappe.validated = true;
        if (frm.doc.__islocal) {
          frm.refresh_field('accounts');
        } else {
          frm.save().then(() => {
            const row = frm.fields_dict.accounts.grid.grid_rows_by_docname[cdn];
            row.show_form();
          });
        }
      },
      function () {
        childDoc.cost_center = previousCostCenter;
        frm.confirmation_in_progress = false;
        frm.refresh_field('accounts');
      },
    );
  }
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'New' &&
    frm.doc.voucher_type !== 'Deferred Revenue' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.page.add_action_item('Send For Approval', function () {
      sendApprovalMail(frm);
    });
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frm.doc.voucher_type !== 'Deferred Revenue' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.page.add_action_item('Approve', function () {
      frm.doc.cnp_action = 1;
      frm.savesubmit();
    });
    frm.page.add_action_item('Reject', function () {
      emailActionReject(frm);
    });
  } else if (
    frm.doc.workflow_state === 'New' &&
    frm.doc.voucher_type === 'Deferred Revenue' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.page.add_action_item('Submit', function () {
      submitDoc(frm);
    });
  }
}

function submitDoc(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.submitDoc',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Journal Entry',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Journal Entry',
      doctype_name: 'journal-entry',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function emailActionReject(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.email_action',
    args: {
      doctype: 'Journal Entry',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'journal-entry',
      redirect: false,
      email_token: frm.doc.cnp_email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function addApprovalPill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let titleElement = $(`h3[title="${frm.doc.accounts[0].account}"]`).parent();
    titleElement = titleElement[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.workflow_state;
    pillspan.appendChild(textspan);

    titleElement.appendChild(pillspan);
  }
}

function setNamingSeries(frm) {
  frm.set_df_property(
    'naming_series',
    'options',
    'AT-CO-.YY.-JV-.\nACC-JV-.yyyy.-',
  );
  frm.doc.naming_series = 'AT-CO-.YY.-JV-.';
  frm.refresh_field('naming_series');
}
