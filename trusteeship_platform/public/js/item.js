/* global frappe */
/* open_url_post */
let keys = [];
frappe.ui.form.on('Item', {
  onload: frm => {
    frm.set_query('document_code', 'cnp_pre_execution_checklist', function () {
      return {
        filters: [
          ['Canopi Document Code', 'pre_execution_checklist', '=', 1],
          ['Canopi Document Code', 'product', '=', frm.doc.name],
        ],
      };
    });
    frm.refresh_field('cnp_pre_execution_checklist');

    frm.set_query('document_code', 'cnp_post_execution_checklist', function () {
      return {
        filters: [
          ['Canopi Document Code', 'post_execution_checklist', '=', 1],
          ['Canopi Document Code', 'product', '=', frm.doc.name],
        ],
      };
    });
    frm.refresh_field('cnp_post_execution_checklist');

    frm.set_query(
      'document_code',
      'cnp_transaction_documents_checklist',
      function () {
        return {
          filters: [
            ['Canopi Document Code', 'transaction_documents_checklist', '=', 1],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_transaction_documents_checklist');

    frm.set_query(
      'document_code',
      'cnp_security_documents_checklist',
      function () {
        return {
          filters: [
            ['Canopi Document Code', 'security_documents_checklist', '=', 1],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_security_documents_checklist');

    frm.set_query(
      'document_code',
      'cnp_other_documents_checklist',
      function () {
        return {
          filters: [
            ['Canopi Document Code', 'other_documents_checklist', '=', 1],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_other_documents_checklist');

    frm.set_query(
      'document_code',
      'cnp_condition_precedent_part_a_checklist',
      function () {
        return {
          filters: [
            [
              'Canopi Document Code',
              'condition_precedent_part_a_checklist',
              '=',
              1,
            ],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_condition_precedent_part_a_checklist');

    frm.set_query(
      'document_code',
      'cnp_condition_precedent_part_b_checklist',
      function () {
        return {
          filters: [
            [
              'Canopi Document Code',
              'condition_precedent_part_b_checklist',
              '=',
              1,
            ],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_condition_precedent_part_b_checklist');

    frm.set_query(
      'document_code',
      'cnp_condition_subsequent_checklist',
      function () {
        return {
          filters: [
            ['Canopi Document Code', 'condition_subsequent_checklist', '=', 1],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_condition_subsequent_checklist');

    frm.set_query('document_code', 'cnp_documents_checklist', function () {
      return {
        filters: [
          ['Canopi Document Code', 'document_checklist', '=', 1],
          ['Canopi Document Code', 'product', '=', frm.doc.name],
        ],
      };
    });
    frm.refresh_field('cnp_documents_checklist');

    frm.set_query(
      'document_code',
      'cnp_sebi_application_checklist',
      function () {
        return {
          filters: [
            ['Canopi Document Code', 'sebi_application_checklist', '=', 1],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_sebi_application_checklist');

    frm.set_query(
      'document_code',
      'cnp_stages_of_offer_document_checklist',
      function () {
        return {
          filters: [
            [
              'Canopi Document Code',
              'stages_of_offer_document_checklist',
              '=',
              1,
            ],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_stages_of_offer_document_checklist');

    frm.set_query(
      'document_code',
      'cnp_document_based_compliances_checklist',
      function () {
        return {
          filters: [
            [
              'Canopi Document Code',
              'document_based_compliances_checklist',
              '=',
              1,
            ],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_document_based_compliances_checklist');

    frm.set_query(
      'document_code',
      'cnp_event_based_compliances_checklist',
      function () {
        return {
          filters: [
            [
              'Canopi Document Code',
              'event_based_compliances_checklist',
              '=',
              1,
            ],
            ['Canopi Document Code', 'product', '=', frm.doc.name],
          ],
        };
      },
    );
    frm.refresh_field('cnp_event_based_compliances_checklist');

    frm.set_query(
      'document_code',
      'cnp_common_documents_checklist_table',
      function () {
        return {
          filters: [
            ['Canopi Document Code', 'common_documents_checklist', '=', 1],
          ],
        };
      },
    );
    frm.refresh_field('cnp_common_documents_checklist_table');

    if (
      frm.doc.item_code.toUpperCase() === 'FA' ||
      frm.doc.item_code.toUpperCase() === 'EA'
    ) {
      frm.toggle_display('cnp_security_documents_checklist', false);
    } else {
      frm.toggle_display('cnp_condition_precedent_part_a_checklist', false);
      frm.toggle_display('cnp_condition_precedent_part_b_checklist', false);
      frm.toggle_display('cnp_condition_subsequent_checklist', false);
    }
    if (
      frm.doc.item_code.toUpperCase() === 'REIT' ||
      frm.doc.item_code.toUpperCase() === 'INVIT'
    ) {
      frm.toggle_display('cnp_transaction_documents_checklist', false);
      frm.toggle_display('cnp_security_documents_checklist', false);
      frm.toggle_display('cnp_other_documents_checklist', false);
      frm.toggle_display('cnp_condition_precedent_part_a_checklist', false);
      frm.toggle_display('cnp_condition_precedent_part_b_checklist', false);
      frm.toggle_display('cnp_condition_subsequent_checklist', false);
    } else {
      frm.toggle_display('cnp_documents_checklist', false);
      frm.toggle_display('cnp_sebi_application_checklist', false);
      frm.toggle_display('cnp_stages_of_offer_document_checklist', false);
      frm.toggle_display('cnp_document_based_compliances_checklist', false);
      frm.toggle_display('cnp_event_based_compliances_checklist', false);
    }
  },
  after_save: frm => {
    if (keys.length > 0) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.delete_multiple_from_s3',
        freeze: true,
        args: { keys },
        callback: r => {
          keys = [];
        },
      });
    }
  },
  cnp_one_time_efforts: function (frm) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_single_doc',
      args: { doc_type: 'Canopi Criterion' },
      freeze: true,
      callback: r => {
        let oneTimeCost = 0;
        if (frm.doc.name === 'CUSTA' || frm.doc.name === 'SEC') {
          oneTimeCost =
            (frm.doc.cnp_one_time_efforts * r.message.total_per_hour_cost) /
              60 +
            r.message.bcg_employee_mandate_cost;
        } else {
          oneTimeCost =
            (frm.doc.cnp_one_time_efforts * r.message.total_per_hour_cost) /
              60 +
            r.message.bcg_employee_mandate_cost +
            r.message.operating_mandate_cost;
        }
        frm.doc.cnp_one_time_cost = oneTimeCost;
        frm.refresh_field('cnp_one_time_cost');
      },
    });
  },
  cnp_on_going_efforts: function (frm) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_single_doc',
      args: { doc_type: 'Canopi Criterion' },
      freeze: true,
      callback: r => {
        let ongoingCost = 0;
        if (frm.doc.name === 'CUSTA' || frm.doc.name === 'SEC') {
          ongoingCost =
            (frm.doc.cnp_on_going_efforts * r.message.total_per_hour_cost) /
              60 +
            r.message.operating_mandate_cost;
        } else {
          ongoingCost =
            (frm.doc.cnp_on_going_efforts * r.message.total_per_hour_cost) /
              60 +
            r.message.operating_mandate_cost +
            r.message.mandate_cost_of_capital_dt_license;
        }
        frm.doc.cnp_on_going_cost = ongoingCost;
        frm.refresh_field('cnp_on_going_cost');
      },
    });
  },
});

frappe.ui.form.on('Canopi Common Documents Checklist Description', {
  form_render: (frm, cdt, cdn) => {
    renderTemplate(frm, cdt, cdn, 'Common Documents Checklist');
  },

  before_cnp_pre_execution_checklist_remove: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.standard_template_s3_key) {
      keys.push(row.standard_template_s3_key);
    }
  },

  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_common_documents_checklist');
        },
      });
    }
  },

  select_all: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    selectAllTypeOfSecurity(row);
    frm.refresh_field('cnp_pre_execution_checklist');
  },
});

frappe.ui.form.on('Canopi Pre Execution Checklist Description', {
  cnp_pre_execution_checklist_add: (frm, cdt, cdn) => {
    hidePrePostTypeOfSecuritySection(
      frm,
      cdt,
      cdn,
      'cnp_pre_execution_checklist',
    );
  },

  form_render: (frm, cdt, cdn) => {
    renderTemplate(frm, cdt, cdn, 'Pre-Execution Checklist');
    hideFieldsForStePrePostDocumentation(
      frm,
      cdt,
      cdn,
      'cnp_pre_execution_checklist',
    );
    hidePrePostTypeOfSecuritySection(
      frm,
      cdt,
      cdn,
      'cnp_pre_execution_checklist',
    );
  },

  before_cnp_pre_execution_checklist_remove: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.standard_template_s3_key) {
      keys.push(row.standard_template_s3_key);
    }
  },

  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_pre_execution_checklist');
        },
      });
    }
  },

  select_all: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    selectAllTypeOfSecurity(row);
    frm.refresh_field('cnp_pre_execution_checklist');
  },
});

frappe.ui.form.on('Canopi Post Execution Checklist Description', {
  cnp_post_execution_checklist_add: (frm, cdt, cdn) => {
    hidePrePostTypeOfSecuritySection(
      frm,
      cdt,
      cdn,
      'cnp_post_execution_checklist',
    );
  },
  form_render: (frm, cdt, cdn) => {
    hideFieldsForStePrePostDocumentation(
      frm,
      cdt,
      cdn,
      'cnp_post_execution_checklist',
    );
    hidePrePostTypeOfSecuritySection(
      frm,
      cdt,
      cdn,
      'cnp_post_execution_checklist',
    );
  },
  before_cnp_post_execution_checklist_remove: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.standard_template_s3_key) {
      keys.push(row.standard_template_s3_key);
    }
  },

  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_post_execution_checklist');
        },
      });
    }
  },
});
frappe.ui.form.on('Canopi Security Documents Checklist Description', {
  form_render: (frm, cdt, cdn) => {
    hideFieldsForStePrePostDocumentation(
      frm,
      cdt,
      cdn,
      'cnp_security_documents_checklist',
    );
  },

  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_security_documents_checklist');
        },
      });
    }
  },

  select_all: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    selectAllTypeOfSecurity(row);
    frm.refresh_field('cnp_security_documents_checklist');
  },
});

frappe.ui.form.on('Canopi Other Documents Checklist Description', {
  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_other_documents_checklist');
        },
      });
    }
  },
});

frappe.ui.form.on('Canopi Condition Precedent Part A Checklist Description', {
  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_condition_precedent_part_a_checklist');
        },
      });
    }
  },
});

frappe.ui.form.on('Canopi Condition Precedent Part B Checklist Description', {
  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_condition_precedent_part_b_checklist');
        },
      });
    }
  },
});

frappe.ui.form.on('Canopi Condition Subsequent Checklist Description', {
  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_condition_subsequent_checklist');
        },
      });
    }
  },
});

frappe.ui.form.on('Canopi Transaction Documents Checklist Description', {
  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          row.is_dta = r.message.is_dta;
          row.is_for_document_execution = r.message.is_for_document_execution;
          frm.refresh_field('cnp_transaction_documents_checklist');
        },
      });
    }
  },
});

function selectAllTypeOfSecurity(row) {
  if (row.select_all === 1) {
    row.immovable = 1;
    row.movable = 1;
    row.pledge = 1;
    row.personal_guarantee = 1;
    row.corporate_guarantee = 1;
    row.ndu = 1;
    row.intangible = 1;
    row.motor = 1;
    row.financial = 1;
    row.intangible = 1;
    row.assignment_of_rights = 1;
    row.current = 1;
    row.dsra = 1;
    row.others = 1;
  }
}

function hideFieldsForStePrePostDocumentation(frm, cdt, cdn, field) {
  const row = locals[cdt][cdn];
  const currentRow = frm.fields_dict[field].grid.grid_rows_by_docname[row.name];
  if (frm.doc.item_code.toUpperCase() === 'STE') {
    currentRow.toggle_display('listed', false);
    currentRow.toggle_display('whether_public_issue', false);
  } else {
    currentRow.toggle_display('listed', true);
    currentRow.toggle_display('whether_public_issue', true);
  }
}

function hidePrePostTypeOfSecuritySection(frm, cdt, cdn, field) {
  const row = locals[cdt][cdn];
  if (
    frm.doc.item_code.toUpperCase() === 'EA' ||
    frm.doc.item_code.toUpperCase() === 'FA' ||
    frm.doc.item_code.toUpperCase() === 'INVIT' ||
    frm.doc.item_code.toUpperCase() === 'REIT'
  ) {
    row.is_type_of_security_visible = false;
    frm.refresh_field(field);
  }
}

function renderTemplate(frm, cdt, cdn, child_table) {
  const row = locals[cdt][cdn];
  const id = row.name;

  const htmlFieldDict =
    frm.fields_dict[row.parentfield].grid.grid_rows_by_docname[cdn].grid_form
      .fields_dict.standard_template_html;
  $(htmlFieldDict.wrapper).empty();
  htmlFieldDict.html(
    frappe.render_template('pre_execution_standard_template', {
      id,
      doc: row,
      filename: row.standard_template_s3_key
        ? row.standard_template_s3_key.split('/').pop()
        : '',
    }),
  );
  $('.' + id + '-upload-button').unbind();
  $('.' + id + '-attach-input').unbind();
  $('.' + id + '-download-button').unbind();
  $('.' + id + '-delete-button').unbind();

  $('.' + id + '-upload-button').on('click', function () {
    validatePreExecutionTable(frm);
    $('.' + id + '-attach-input').click();
  });

  $('.' + id + '-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      let base64 = await getBase64(file[0]);
      base64 = base64.split(',')[1]; // remove data:image/png;base64,
      const key = `${frm.doctype}/${frm.docname}/Pre Execution Checklist Template/${row.document_code}/${file[0].name}`;
      frm.dirty();
      frm.save().then(() => {
        frappe.call({
          method:
            'trusteeship_platform.custom_methods.upload_standard_template',
          freeze: true,
          args: {
            child_table,
            file: base64,
            docname: frm.doc.name,
            key,
            index: row.idx,
          },
          callback: r => {
            frm.reload_doc().then(() => {
              const tableGrid = frm.get_field(
                'cnp_pre_execution_checklist',
              ).grid;
              const curRow = tableGrid.get_row(id);
              curRow.toggle_view();
            });
          },
        });
      });
    }
  });

  $('.' + id + '-download-button').on('click', function () {
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: row.standard_template_s3_key,
    });
  });

  $('.' + id + '-delete-button').on('click', function () {
    document.activeElement.blur(); // removes focus from active element
    setTimeout(() => {
      validatePreExecutionTable(frm);
      const key = row.standard_template_s3_key
        ? row.standard_template_s3_key
        : frappe.throw('S3 key not found.');
      row.standard_template_s3_key = '';
      frm.dirty();
      frm.save().then(() => {
        frappe.call({
          method: 'trusteeship_platform.custom_methods.delete_from_s3',
          freeze: true,
          args: { key },
          callback: r => {
            frm.reload_doc().then(() => {
              const tableGrid = frm.get_field(
                'cnp_pre_execution_checklist',
              ).grid;
              const curRow = tableGrid.get_row(id);
              curRow.toggle_view();
            });
          },
        });
      });
    }, 100);
  });
}

function validatePreExecutionTable(frm) {
  if (
    frm.doc.cnp_pre_execution_checklist.filter(
      element => !element.document_code,
    ).length > 0
  ) {
    frappe.throw(
      'Mandatory field <b>Document Code</b> required in table <b>Pre-Execution Checklist</b>',
    );
  }
  const documentCodes = new Set(
    frm.doc.cnp_pre_execution_checklist.map(element => element.document_code),
  );
  if (
    frm.doc.cnp_pre_execution_checklist.length !== [...documentCodes].length
  ) {
    frappe.throw('<b>Document Code</b> must be unique.');
  }
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
