/* eslint-env jquery */
/* global frappe, erpnext */

/* eslint-disable-next-line no-unused-vars */
function showActivities(frm) {
  $(frm.fields_dict.open_activities_html.wrapper).empty();

  const crmActivities = new erpnext.utils.CRMActivities({
    frm,
    open_activities_wrapper: $(frm.fields_dict.open_activities_html.wrapper),
    all_activities_wrapper: $(frm.fields_dict.all_activities_html.wrapper),
    form_wrapper: $(frm.wrapper),
  });
  crmActivities.refresh();

  setTimeout(() => {
    // remove extra activities table
    if ($('.open-activities').length > 1) {
      crmActivities.refresh();
    }

    // hide new task btn, new events btn and mark as closed checkbox
    if (frm.doc.docstatus === 1) {
      $('.new-btn').addClass('hidden');
      $("input[title='Mark As Closed']").parent().addClass('hidden');
    }
  }, 400);
}

/* eslint-disable-next-line no-unused-vars */
function showNotes(frm) {
  $(frm.fields_dict.notes_html.wrapper).empty();

  const crmNotes = new erpnext.utils.CRMNotes({
    frm,
    notes_wrapper: $(frm.fields_dict.notes_html.wrapper),
  });
  crmNotes.refresh();

  $('.new-note-btn').unbind();
  $('.notes-section').find('.edit-note-btn').unbind();
  $('.notes-section').find('.delete-note-btn').unbind();

  if (frm.doc.docstatus === 1) {
    // hide new, edit, delete btn
    $('.new-btn').addClass('hidden');
    $('.notes-section').find('.edit-note-btn').addClass('hidden');
    $('.notes-section').find('.delete-note-btn').addClass('hidden');
  } else {
    $('.new-note-btn').on('click', function () {
      addNote(frm);
    });

    $('.notes-section')
      .find('.edit-note-btn')
      .on('click', function () {
        editNote(this, frm);
      });

    $('.notes-section')
      .find('.delete-note-btn')
      .on('click', function () {
        deleteNote(this, frm);
      });
  }
}

function addNote(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Add a Note',
    fields: [
      {
        label: 'Note',
        fieldname: 'note',
        fieldtype: 'Text Editor',
        reqd: 1,
        enable_mentions: true,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_note',
        args: {
          doctype: frm.doctype,
          docname: frm.doc.name,
          note: data.note,
        },
        freeze: true,
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.refresh_fields();
          }
        },
      });
    },
    primary_action_label: 'Add',
  });
  d.show();
}

function editNote(editBtn, frm) {
  const row = $(editBtn).closest('.comment-content');
  const rowId = row.attr('name');
  const rowContent = $(row).find('.content').html();
  if (rowContent) {
    const d = new frappe.ui.Dialog({
      title: 'Edit Note',
      fields: [
        {
          label: 'Note',
          fieldname: 'note',
          fieldtype: 'Text Editor',
          default: rowContent,
        },
      ],
      primary_action: function () {
        const data = d.get_values();
        frappe.call({
          method: 'trusteeship_platform.custom_methods.edit_note',
          args: {
            doctype: frm.doctype,
            docname: frm.doc.name,
            note: data.note,
            rowId,
          },
          freeze: true,
          callback: function (r) {
            if (!r.exc) {
              d.hide();
              frm.reload_doc();
            }
          },
        });
      },
      primary_action_label: 'Done',
    });
    d.show();
  }
}

function deleteNote(deleteBtn, frm) {
  const rowId = $(deleteBtn).closest('.comment-content').attr('name');
  frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_note',
    args: {
      doctype: frm.doctype,
      docname: frm.doc.name,
      rowId,
    },
    freeze: true,
    callback: function (r) {
      if (!r.exc) {
        frm.refresh_fields();
      }
    },
  });
}

/* eslint-disable-next-line no-unused-vars */
function updateBreadcrumbs(workspace) {
  const breadcrumbs = frappe.breadcrumbs.all[frappe.breadcrumbs.current_page()];
  breadcrumbs.workspace = workspace;
  frappe.breadcrumbs.update();
}
