/* global frappe */
frappe.ui.form.on('User', {
  refresh: function (frm) {
    frm.set_df_property('enabled', 'read_only', 1);
  },
  enabled: function (frm) {
    let custom_btn_label = null;
    if (frm.doc.enabled === 1) {
      custom_btn_label = 'Disable User';
    } else {
      custom_btn_label = 'Enable User';
    }
    frm.add_custom_button(__(custom_btn_label), function () {
      if (frm.doc.enabled === 1) {
        const justification = new frappe.ui.Dialog({
          title: 'Justification',
          fields: [
            {
              label: 'Justification',
              fieldname: 'justification',
              fieldtype: 'Data',
              reqd: 1,
            },
          ],
          primary_action_label: 'Submit',
          primary_action(values) {
            frappe.call({
              method: 'frappe.desk.form.utils.add_comment',
              args: {
                reference_doctype: frm.doctype,
                reference_name: frm.docname,
                content: values.justification,
                comment_email: frappe.session.user,
                comment_by: frappe.session.user_fullname,
              },
            });
            justification.hide();
            frappe.msgprint(
              'Justification is added in comments below this document',
            );
            frm.set_value('enabled', 0);
            frm.save();
          },
        });
        justification.show();
      } else {
        frm.set_value('enabled', 1);
        frm.save();
      }
    });
  },
});
