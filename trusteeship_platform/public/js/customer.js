/* global frappe */
/* eslint-env jquery */
frappe.ui.form.on('Customer', {
  refresh: frm => {
    addFetchButton(frm);
    loadDirectorAndSignatory(frm);
    requiredCinPan(frm);
  },

  validate: frm => {
    validateCinNumber(frm);
  },
  cnp_type: frm => {
    requiredCinPan(frm);
  },

  cnp_fetch_base_details: frm => {
    if (checkRequiredFields(frm)) {
      frappe.call({
        freeze: true,
        method:
          'trusteeship_platform.custom_methods.fetch_base_details_for_customer',
        args: {
          doc: frm.doc,
          customer_name: frm.doc.name,
          is_local: frm.doc.__islocal !== undefined ? frm.doc.__islocal : 0,
          cin_number: frm.doc.cin_number,
        },
        callback: r => {
          if (frm.doc.__islocal) {
            frappe.set_route('Form', 'Customer', r.message[0].name);
          }
          frappe.show_alert(
            {
              message: 'Fetched successfully.',
              indicator: 'green',
            },
            5,
          );
          frm.reload_doc();
          frappe.msgprint(r.message[1]);
        },
      });
    }
  },
});

function addFetchButton(frm) {
  // Set Custom Button To Fetch Comprehensive details
  if (frm.doc.cin_number) {
    frappe.call({
      method: 'frappe.client.get_list',
      args: {
        doctype: 'CPRB Company Details',
        filters: {
          name: frm.doc.cin_number,
        },
        limit: 1,
      },
      callback: function (r) {
        if (r.message && r.message.length > 0) {
          frm.set_df_property('cnp_fetch_base_details', 'hidden', 1);
          frm.add_custom_button(__('Update Details From Probe42'), function () {
            if (checkRequiredFields(frm)) {
              frappe.call({
                freeze: true,
                method:
                  'trusteeship_platform.custom_methods.fetch_comprehensive_details_for_customer',
                args: {
                  doc: frm.doc,
                  customer_name: frm.doc.name,
                  is_local:
                    frm.doc.__islocal !== undefined ? frm.doc.__islocal : 0,
                  cin_number: frm.doc.cin_number,
                },
                callback: r => {
                  if (frm.doc.__islocal) {
                    frappe.set_route('Form', 'Customer', r.message.name);
                  }
                  frappe.show_alert(
                    {
                      message: 'Fetched successfully.',
                      indicator: 'green',
                    },
                    5,
                  );
                  frm.reload_doc();
                },
              });
            }
          });
        }
      },
    });
  }
}

function checkRequiredFields(frm) {
  if (!frm.doc.customer_name) {
    frappe.msgprint({
      message: 'Required fields must be filled in.',
      indicator: 'red',
      title: 'Missing Fields',
    });
    return false;
  } else if (!frm.doc.cin_number) {
    frappe.msgprint({
      message: 'CIN Number is mandatory for fetching.',
      indicator: 'red',
      title: 'Missing Fields',
    });
    return false;
  } else {
    return true;
  }
}

function loadDirectorAndSignatory(frm) {
  $(frm.fields_dict.cnp_director_details.wrapper).empty();
  $(frm.fields_dict.cnp_signatory_details.wrapper).empty();

  const shareholdings = frm.doc.cnp_director_shareholdings
    ? JSON.parse(frm.doc.cnp_director_shareholdings)
    : [];
  const signatories = frm.doc.cnp_authorized_signatories
    ? JSON.parse(frm.doc.cnp_authorized_signatories)
    : [];
  frm.set_df_property(
    'cnp_director_details',
    'options',
    frappe.render_template('director_and_signatory', {
      doc: {
        name: 'director_details',
        payload: shareholdings,
        id: frm.doc.name,
      },
    }),
  );
  frm.refresh_field('cnp_director_details');
  frm.set_df_property(
    'cnp_signatory_details',
    'options',
    frappe.render_template('director_and_signatory', {
      doc: {
        name: 'signatory_details',
        payload: signatories,
        id: frm.doc.name,
      },
    }),
  );
  frm.refresh_field('cnp_signatory_details');

  addDirector(frm);
  editDirector(frm);
  removeDirector(frm);

  addSignatory(frm);
  editSignatory(frm);
  removeSignatory(frm);
}

function addDirector(frm) {
  $('.' + frm.doc.name + '-director-add').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Director Info',
      fields: [
        {
          label: 'Name',
          fieldname: 'full_name',
          fieldtype: 'Data',
          reqd: true,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        const directorShareholdings = frm.doc.cnp_director_shareholdings
          ? JSON.parse(frm.doc.cnp_director_shareholdings)
          : [];
        directorShareholdings.push({
          full_name: values.full_name,
          designation: values.designation,
        });

        frm.doc.cnp_director_shareholdings = JSON.stringify(
          directorShareholdings,
        );
        frm.dirty();
        frm.refresh();
        d.hide();
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function editDirector(frm) {
  $('.' + frm.doc.name + '-director-edit').on('click', function () {
    const index = $('.' + frm.doc.name + '-director-edit').index(this);
    const shareholdings = JSON.parse(frm.doc.cnp_director_shareholdings);
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Name',
          fieldname: 'full_name',
          fieldtype: 'Data',
          reqd: true,
          default: shareholdings[index].full_name,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
          default: shareholdings[index].designation,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        shareholdings[index].full_name = values.full_name;
        shareholdings[index].designation = values.designation;
        frm.doc.cnp_director_shareholdings = JSON.stringify(shareholdings);

        frm.dirty();
        frm.refresh();
        d.hide();
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function removeDirector(frm) {
  $('.' + frm.doc.name + '-director-remove').on('click', function () {
    const directors = JSON.parse(frm.doc.cnp_director_shareholdings);
    const index = $('.' + frm.doc.name + '-director-remove').index(this);
    directors.splice(index, 1);
    frm.doc.cnp_director_shareholdings = JSON.stringify(directors);

    frm.dirty();
    frm.refresh();
  });
}

function addSignatory(frm) {
  $('.' + frm.doc.name + '-signatory-add').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Add Signatory Info',
      fields: [
        {
          label: 'Name',
          fieldname: 'name',
          fieldtype: 'Data',
          reqd: true,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        const signatoryInfo = frm.doc.cnp_authorized_signatories
          ? JSON.parse(frm.doc.cnp_authorized_signatories)
          : [];
        signatoryInfo.push({
          name: values.name,
          designation: values.designation,
        });

        frm.doc.cnp_authorized_signatories = JSON.stringify(signatoryInfo);
        frm.dirty();
        frm.refresh();
        d.hide();
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function editSignatory(frm) {
  $('.' + frm.doc.name + '-signatory-edit').on('click', function () {
    const index = $('.' + frm.doc.name + '-signatory-edit').index(this);
    const director = JSON.parse(frm.doc.cnp_authorized_signatories);
    const d = new frappe.ui.Dialog({
      title: 'Edit',
      fields: [
        {
          label: 'Name',
          fieldname: 'name',
          reqd: true,
          fieldtype: 'Data',
          default: director[index].name,
        },
        {
          label: 'Designation',
          fieldname: 'designation',
          reqd: true,
          fieldtype: 'Data',
          default: director[index].designation,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        director[index].name = values.name;
        director[index].designation = values.designation;
        frm.doc.cnp_authorized_signatories = JSON.stringify(director);
        frm.dirty();
        frm.refresh();
        d.hide();
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    d.show();
  });
}

function removeSignatory(frm) {
  $('.' + frm.doc.name + '-signatory-remove').on('click', function () {
    const index = $('.' + frm.doc.name + '-signatory-remove').index(this);
    const signatories = JSON.parse(frm.doc.cnp_authorized_signatories);
    signatories.splice(index, 1);
    frm.doc.cnp_authorized_signatories = JSON.stringify(signatories);

    frm.dirty();
    frm.refresh();
  });
}

function requiredCinPan(frm) {
  frm.set_df_property(
    'pan',
    'reqd',
    frm.doc.cnp_type === 'Individual' ||
      frm.doc.cnp_type === 'Partnership Firm',
  );
  frm.set_df_property(
    'cin_number',
    'reqd',
    frm.doc.cnp_type === 'Organization' || frm.doc.cnp_type === 'LLP',
  );

  frm.set_df_property(
    'cnp_reference_no',
    'reqd',
    frm.doc.cnp_type === 'Trustee' || frm.doc.cnp_type === 'Foreign',
  );
}

function validateCinNumber(frm) {
  if (frm.doc.cnp_type === 'Organization' || frm.doc.cnp_type === 'LLP') {
    const cin = frm.doc.cin_number.toUpperCase();
    const pattern = /[A-Z0-9]{21}/;
    if (cin.match(pattern) && cin.length === 21) {
      frm.doc.cin_number = cin;
    } else {
      frappe.throw('Invalid CIN number. Please Enter Valid CIN number');
    }
  }
}
