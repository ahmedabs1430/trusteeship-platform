/* global frappe */
frappe.ui.form.on('Bank Account', {
  refresh: frm => {
    if (frm.doc.__islocal && frappe.get_prev_route()[1] === 'Canopi Lender') {
      frm.set_value('cnp_lender', frappe.get_prev_route()[2]);
    }
  },
});
