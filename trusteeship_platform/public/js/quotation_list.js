// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
frappe.listview_settings.Quotation = {
  refresh: function (frm) {
    changeBreadcrumbs();
  },
};

function changeBreadcrumbs() {
  if (frappe.user.has_role('BCG Head') || frappe.user.has_role('BCG RM')) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('CRM');
    });
  }
}
