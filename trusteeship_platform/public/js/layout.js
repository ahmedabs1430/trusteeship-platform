/* global frappe */
frappe.ui.form.Layout = class TrusteeshipPlatformLayout extends (
  frappe.ui.form.Layout
) {
  // eslint-disable-next-line no-useless-constructor
  constructor(opts) {
    super(opts);
  }

  setup_tab_events() {
    this.wrapper.on('keydown', ev => {
      if (ev.which === 9) {
        const current = $(ev.target);
        const doctype = current.attr('data-doctype');
        const fieldname = current.attr('data-fieldname');
        if (doctype) {
          return this.handle_tab(doctype, fieldname, ev.shiftKey);
        } else if (this.get_open_grid_row()) {
          // hide opened grid
          this.get_open_grid_row().hide_form();
        }
      }
    });
  }

  focus_on_next_field(start_idx, fields) {
    // loop to find next eligible fields
    for (let i = start_idx + 1, len = fields.length; i < len; i++) {
      const field = fields[i];
      if (this.is_visible(field)) {
        if (field.df.fieldtype === 'Table') {
          // set focus to next tab break
          if (field.tab) {
            field.tab.set_active();
          }

          // enable next collapsed section
          if (field.section) {
            field.section.collapse(false);
          }

          // open table grid
          if (!(field.grid.grid_rows && field.grid.grid_rows.length)) {
            // empty grid, add a new row
            field.grid.add_new_row();
          }
          // show grid row (if exists)
          field.grid.grid_rows[0].show_form();
          return true;
        } else if (!in_list(frappe.model.no_value_type, field.df.fieldtype)) {
          this.set_focus(field);
          return true;
        }
      }
    }
  }
};
