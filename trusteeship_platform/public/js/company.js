// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe */
/* eslint-env jquery */

frappe.ui.form.on('Company', {
  refresh: frm => {
    if (!frm.doc.__islocal) {
      renderAttachButton(frm);
    }
    if (frm.doc.cnp_s3_key) {
      frm.toggle_reqd('cnp_sign_secret', true);
    } else {
      frm.toggle_reqd('cnp_sign_secret', false);
    }
  },
});

async function renderAttachButton(frm) {
  $(frm.fields_dict.cnp_button_html.wrapper).empty();
  const id = frm.doc.name.replace(/\s+/g, '');

  frm.set_df_property(
    'cnp_button_html',
    'options',
    frappe.render_template('s3_buttons', { doc: frm.doc, id }),
  );
  frm.refresh_field('cnp_button_html');

  $(`#${id}_attach_button`).click(function () {
    $(`#${id}_attach_file`).click();
  });

  $(`#${id}_attach_file`).change(async event => {
    const file = { ...event.target.files };
    if (file) {
      const ext = event.target.value.match(/[^/.]+\.([^/.]+)$/)[1];
      if (ext !== 'pfx') {
        frappe.throw('Only PFX file allowed');
      }
      event.target.value = '';
      let base64 = await getBase64(file[0]);
      base64 = base64.split(',')[1]; // remove data:image/png;base64,
      frappe.call({
        method: 'trusteeship_platform.custom_methods.send_file_to_s3',
        freeze: true,
        args: {
          file: base64,
          file_name: file[0].name,
          docname: frm.doc.name,
          doctype: frm.doc.doctype,
        },
        callback: r => {
          frm.reload_doc();
          frappe.show_alert(
            {
              message: 'Uploaded',
              indicator: 'green',
            },
            5,
          );
        },
      });
    }
  });

  $(`#${id}_delete_button`).click(() => {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.delete_pfx',
      freeze: true,
      args: { docname: frm.doc.name, doctype: frm.doc.doctype },
      callback: r => {
        frm.reload_doc();
        frappe.show_alert(
          {
            message: 'Deleted',
            indicator: 'green',
          },
          5,
        );
      },
    });
  });
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
