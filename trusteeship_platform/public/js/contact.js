/* global frappe */
frappe.ui.form.on('Contact', {
  refresh: frm => {
    frm.set_df_property('email_id', 'read_only', false);
  },

  onload: frm => {
    const prevRoute = frappe.get_prev_route();
    if (prevRoute[1] === 'Lead') {
      frm.set_df_property('phone_nos', 'reqd', true);
    } else {
      frm.set_df_property('phone_nos', 'reqd', false);
    }
  },
});
