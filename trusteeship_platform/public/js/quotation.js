/* global frappe, updateBreadcrumbs, erpnext, locals, moment, in_list, cur_frm, open_url_post */
/* eslint-env jquery */

erpnext.selling.CanopiQuotationController = class CanopiQuotationController extends (
  erpnext.selling.QuotationController
) {
  refresh(doc, dt, dn) {
    super.refresh(doc, dt, dn);
    updateCreateDropdownBtn(cur_frm);
  }
};
cur_frm.script_manager.make(erpnext.selling.CanopiQuotationController);

frappe.ui.form.on('Quotation', {
  refresh: frm => {
    changeBreadcrumbs(frm);
    addRejectionComment(frm);
    initiateTrancheFlow(frm);
    setFacilityAmtFixHeight(frm);
    if (!frm.is_new()) {
      frm.clear_custom_buttons();
    }
    hideMenuItems(frm);

    if (frm.doc.cnp_payment_schedule) {
      frm.fields_dict.cnp_payment_schedule?.grid.wrapper
        ?.find('.btn-open-row')
        ?.hide();
    }

    addSalesInvoice(frm);
    hideActions(frm);
    hidePrintIcon();
    addReSubmitButton(frm);
    addOfferLetterBtn(frm);
    addCustomEmailIcon(frm);
    addProformaInvoiceBtn(frm);
    if (frm.doc.docstatus === 0) {
      makeOfferLetterDetailsReadonly(frm);
      addEditButton(frm);
    }
    loadPricingTable(frm);
    setOpportunityFilter(frm);
    setAnnexureTemplateFilter(frm);
    setDigitalSinaturesFilter(frm);
    setCostCenterFilter(frm);
    quotationReloadOnPolicyUpdate(frm);
    if (frm.doc.__islocal !== 1) {
      approval(frm);
    }
    loadAddressOnAddressChange(frm);

    const prevRoute = frappe.get_prev_route();
    if (prevRoute[1] === 'Canopi Pricing Policy') {
      frm.scroll_to_field('cnp_pricing_policy_html');
      setLetterSub(frm);
    }

    hideAddSalesInvoiceBtn(frm);
    addOperationsFlowBtnInConnections(frm);
    hideAddOperationsFlowBtn(frm);
    offerLetterUpload(frm);
    setIntro(frm);
    hideCancelButton(frm);
    hideCnpOfferAcceptenceDate(frm);
    hideConnections(frm);

    frm.fields_dict.items.grid.update_docfield_property(
      'rate',
      'label',
      'Fees (' + frm.doc.currency + ')',
    );

    emptyFieldIfNoTypeOfFee(frm);
    hideFields(frm);
    changeLabelOfferDate(frm);
    setDefaultRegisteredAddress(frm);
    setNamingSeries(frm);
  },
  cnp_cost_center(frm) {
    setDefaultCompanyAddress(frm);
  },
  company: frm => {
    setDefaultRegisteredAddress(frm);
  },

  onload: function (frm) {
    hideConnectionSalesOrder();
    setGstState(frm);
    setRegisteredAddressFilter(frm);
    setCorporateAddressFilter(frm);
    disableAutoCalculate(frm);
  },

  cnp_offer_acceptence_date: frm => {
    validateAcceptanceDate(frm);
  },

  after_save: frm => {
    setQuotationFacilityAmt(frm);
  },

  timeline_refresh: frm => {
    hideItemPriceMessages(frm);
    hideConnectionTab(frm);
  },

  onload_post_render: frm => {
    frm.fields_dict.items.grid.update_docfield_property(
      'rate',
      'label',
      'Fees (' + frm.doc.currency + ')',
    );
  },

  validate: function (frm) {
    mandatoryNextFeeDateAndFrequency(frm);
  },

  before_save: function (frm) {
    deviationChange(frm);
    calProformaInvoiceAmt(frm);
    setQuotationState(frm);
    validateTypeOfFee(frm);
    if (frm.doc.__islocal) frm.doc.cnp_is_sales_invoice_generated = 0;
  },

  before_load: frm => {
    const qty = frappe.meta.get_docfield('Quotation Item', 'qty', frm.doc.name);
    qty.hidden = 1;
    const uom = frappe.meta.get_docfield('Quotation Item', 'uom', frm.doc.name);
    uom.hidden = 1;
    const conversionFactor = frappe.meta.get_docfield(
      'Quotation Item',
      'conversion_factor',
      frm.doc.name,
    );
    conversionFactor.hidden = 1;
    frm.refresh_fields();
    if (frm.doc.__islocal === 1) {
      setLetterSub(frm);
      loadTermsAndConditions(frm);
      loadOtherTermsAndConditions(frm);
    }
  },

  cnp_letter_from: frm => {
    loadOtherTermsAndConditions(frm);
  },

  cnp_facility_amt: frm => {
    frm.doc.cnp_facility_amt = frm.doc.cnp_facility_amt.replace(
      /(l|L)$/,
      '00000',
    );
    frm.doc.cnp_facility_amt = frm.doc.cnp_facility_amt.replace(
      /(c|C)$/,
      '0000000',
    );
    frm.doc.cnp_facility_amt = frm.doc.cnp_facility_amt.replace(
      /(m|M)$/,
      '000000',
    );
    frm.doc.cnp_facility_amt = frm.doc.cnp_facility_amt.replace(
      /(b|B)$/,
      '000000000',
    );
    frm.doc.cnp_facility_amt = frm.doc.cnp_facility_amt.replace(/[^0-9]/g, '');
    frm.refresh_field('cnp_facility_amt');

    setLetterSub(frm);
    loadOtherTermsAndConditions(frm);

    if (frm.doc.cnp_facility_amt) {
      frappe.call({
        method:
          'trusteeship_platform.custom_methods.num_in_words_in_lakhs_and_million',
        args: { integer: frm.doc.cnp_facility_amt },
        freeze: true,
        callback: r => {
          frm.toggle_display('cnp_facility_amt_in_words', true);
          frm.doc.cnp_facility_amt_in_words = r.message.in_lakh;
          frm.refresh_field('cnp_facility_amt_in_words');

          frm.toggle_display('cnp_facility_amt_in_words_millions', true);
          frm.doc.cnp_facility_amt_in_words_millions = r.message.in_million;
          frm.refresh_field('cnp_facility_amt_in_words_millions');
        },
      });
    } else {
      frm.doc.cnp_facility_amt_in_words = '';
      frm.toggle_display('cnp_facility_amt_in_words', false);

      frm.doc.cnp_facility_amt_in_words_millions = '';
      frm.toggle_display('cnp_facility_amt_in_words_millions', false);
    }
  },

  quotation_to: frm => {
    frm.doc.cnp_opportunity = '';
    frm.refresh_field('cnp_opportunity');
    setOpportunityFilter(frm);
  },

  party_name: frm => {
    frm.doc.cnp_opportunity = '';
    frm.refresh_field('cnp_opportunity');
    setOpportunityFilter(frm);
  },

  cnp_opportunity: frm => {
    if (frm.doc.cnp_opportunity) {
      checkOpportunityPricingPolicy(frm);
      getItemFromOpportunity(frm);
    } else {
      frm.doc.items = [];
      frm.add_child('items');
      frm.refresh_field('items');
      loadPricingTable(frm);
      disableAutoCalculate(frm);
    }
  },

  cnp_auto_calculate: function (frm) {
    if (frm.doc.cnp_auto_calculate) {
      frm.doc.items.forEach(item => {
        item.rate = 0;
        item.cnp_selected_calculated_fee = 0;
        item.cnp_deviation = 0;
      });
    } else {
      frm.doc.items.forEach(item => {
        setDefaultFeesValues(item);
      });
      convertFeeToCurrencySelected(frm);
    }
    frm.refresh_field('items');
  },

  conversion_rate: frm => {
    frm.trigger('cnp_auto_calculate');
  },

  cnp_registered_address: frm => {
    setCorporateAddressFilter(frm);
  },

  cnp_annexure_template: frm => {
    if (frm.doc.cnp_annexure_template) {
      const docs = [
        {
          docname: frm.doc.cnp_annexure_template,
          doctype: 'Canopi Annexure Template',
        },
        { docname: frm.doc.cnp_opportunity, doctype: 'Opportunity' },
        {
          docname: frm.doc.cnp_opportunity + '-' + frm.doc?.items[0]?.item_code,
          doctype: 'Canopi Pricing Policy',
        },
      ];

      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_multiple_doc',
        args: {
          docs,
        },
        freeze: true,
        callback: r => {
          const securityTypes = r.message.opportunity.cnp_security_type.map(
            element => element.type_of_security,
          );
          r.message.canopi_annexure_template.annexure.forEach(element => {
            if (
              checkConditions(
                getTypeOfSecurityCondition(element),
                securityTypes,
                getPricingPolicyCondition(element),
                r.message.canopi_pricing_policy,
              )
            ) {
              const row = frm.add_child('cnp_annexure');
              row.content = element.content;
            }
          });
          frm.refresh_field('cnp_annexure');
        },
      });
    }
  },

  cnp_name_of_officer_1: frm => {
    if (frm.doc.cnp_name_of_officer_1) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_name: frm.doc.cnp_name_of_officer_1,
          doc_type: 'ATSL Digital Signatures',
        },
        freeze: true,
        callback: r => {
          if (!r.message.cnp_s3_key) {
            frm.doc.cnp_name_of_officer_1 = '';
            frm.refresh_field('cnp_name_of_officer_1');
            frappe.throw('Signature file not attached.');
          }
        },
      });
    }
  },

  cnp_name_of_officer_2: frm => {
    if (frm.doc.cnp_name_of_officer_2) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_name: frm.doc.cnp_name_of_officer_2,
          doc_type: 'ATSL Digital Signatures',
        },
        freeze: true,
        callback: r => {
          if (!r.message.cnp_s3_key) {
            frm.doc.cnp_name_of_officer_2 = '';
            frm.refresh_field('cnp_name_of_officer_2');
            frappe.throw('Signature file not attached.');
          }
        },
      });
    }
  },
});

function renegotiateDialog(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Reason For Renegotiation',
    static: false,
    fields: [
      {
        fieldname: 'reason_to_renegotiate',
        fieldtype: 'Text',
        label: 'Detailed Reason',
        reqd: 1,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      const reasonRoReject =
        '<b>Reason to Renegotiate:</b> ' + data.reason_to_renegotiate;
      d.hide();
      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_renegotiation_comment',
        args: {
          doctype: 'Quotation',
          docname: frm.doc.name,
          comment: reasonRoReject,
        },
        freeze: true,
        callback: function (r) {
          if (!r.exc) {
            frappe.set_route('Form', 'Quotation', r.message);
          }
        },
      });
    },
  });
  d.show();
}

function hideFields(frm) {
  frm.toggle_display('shipping_rule', false);
  frm.toggle_display('incoterm', false);
}

function notificationAcceptanceDate(frm) {
  frappe.call({
    method:
      'trusteeship_platform.overrides.quotation.offer_acceptance_notification',
    args: {
      doctype: 'Quotation',
      docname: frm.doc.name,
      acceptance_date: frm.doc.cnp_offer_acceptence_date,
    },
    freeze: false,
    callback: r => {},
  });
}

async function initiateTrancheFlow(frm) {
  const prevRoute = frappe.get_prev_route();

  if (
    prevRoute[1] === 'ATSL Mandate List' &&
    localStorage.getItem('initiate_tranche_flow') === '1'
  ) {
    localStorage.setItem('initiate_tranche_flow', 0);
    if (frm.doc.docstatus === 2) {
      frm.amend_doc();
    } else {
      await cancelDoc(frm);
      frm.amend_doc();
    }
  }
}

function setFacilityAmtFixHeight(frm) {
  const field = frm.get_field('cnp_facility_amt');
  if (field.input) {
    field.input.style.height = 'calc(1.5em + 0.75rem + 2px)';
    field.input.style.resize = 'none';
  }
}

function setDigitalSinaturesFilter(frm) {
  frm.set_query('cnp_name_of_officer_1', filter => {
    return {
      filters: [
        [
          'ATSL Digital Signatures',
          'name',
          '!=',
          frm.doc.cnp_name_of_officer_2,
        ],
      ],
    };
  });

  frm.set_query('cnp_name_of_officer_2', filter => {
    return {
      filters: [
        [
          'ATSL Digital Signatures',
          'name',
          '!=',
          frm.doc.cnp_name_of_officer_1,
        ],
      ],
    };
  });
}

function addOperationsFlowBtn(frm) {
  if (frm.doc.workflow_state === 'Accepted By Client') {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: {
        doctype: 'ATSL Mandate List',
        fields: '["opportunity"]',
        filters: [['opportunity', '=', frm.doc.cnp_opportunity]],
      },
      freeze: true,
      callback: r => {
        if (r.message.length === 0) {
          frm.add_custom_button(
            'Operations Flow',
            () => {
              frappe.model.open_mapped_doc({
                method:
                  'trusteeship_platform.custom_methods.make_operations_flow_for_quotation',
                frm,
              });
            },
            'Create',
          );
        }
      },
    });
  }
}

function renderAttachmentTemplate(frm, file) {
  const id = frm.doc.name.replace(/\s+/g, '');
  frm.set_df_property(
    'cnp_file_upload_html',
    'options',
    frappe.render_template('offer_letter_attachment', {
      frm,
      id,
      file,
      filename: frm.doc.cnp_s3_key ? frm.doc.cnp_s3_key.split('/').pop() : '',
    }),
  );
  frm.refresh_field('cnp_file_upload_html');
  uploadFile(frm);
}

function uploadFile(frm) {
  const deleteButton = '.' + frm.doc.name + '-delete-button';
  const download = '.' + frm.doc.name + '-download';
  const preview = '.' + frm.doc.name + '-preview-file';
  const id = '.' + frm.doc.name + '-attach-file';
  const input = '.' + frm.doc.name + '-attach-input';
  const upload = '.' + frm.doc.name + '-upload';
  const remove = '.' + frm.doc.name + '-remove-file';
  let s3ButtonIndex;

  $(id).on('click', function () {
    s3ButtonIndex = $(id).index(this);
    $(input)[s3ButtonIndex].click();
  });

  $(download).on('click', function () {
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: frm.doc.cnp_s3_key,
    });
  });

  $(deleteButton).on('click', function () {
    // document.activeElement.blur()
    // removes focus from active element
    setTimeout(() => {
      const key = frm.doc.cnp_s3_key
        ? frm.doc.cnp_s3_key
        : frappe.throw('S3 key not found.');
      frappe.call({
        method: 'trusteeship_platform.custom_methods.delete_from_s3',
        args: {
          key,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            setTimeout(() => {
              const file = ' ';
              renderAttachmentTemplate(frm, file);
              frm.doc.cnp_s3_key = '';
              frm.save('Update');
            }, 1500);
            $(id).remove();
          });
          frappe.show_alert(
            {
              message: 'Deleted',
              indicator: 'green',
            },
            5,
          );
        },
      });
    }, 100);
  });

  $(input).change(async event => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    if (file) {
      event.target.value = '';
      renderAttachmentTemplate(frm, file);
      const blobUrl = URL.createObjectURL(file);
      $(preview).on('click', async function () {
        window.open(blobUrl, '_blank');
      });
      $(remove).on('click', async function () {
        const file = ' ';
        renderAttachmentTemplate(frm, file);
      });
      $(upload).on('click', async function () {
        frappe.dom.freeze();
        const cnpOfferAcceptanceDate = frm.doc.cnp_offer_acceptence_date;
        const xhr = new XMLHttpRequest();
        const api =
          '/api/method/trusteeship_platform.custom_methods.replace_existing_s3_file';
        const formData = new FormData();
        formData.append('file', file);
        formData.append('file_name', file.name);
        formData.append('docname', frm.doc.name);
        formData.append('doctype', frm.doc.doctype);
        xhr.open('POST', api, true);
        xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
        xhr.onreadystatechange = function (e) {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              frappe.dom.unfreeze();
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  const file = ' ';
                  renderAttachmentTemplate(frm, file);
                  frm.set_value(
                    'cnp_offer_acceptence_date',
                    cnpOfferAcceptanceDate,
                  );
                  frm.save('Update');
                }, 1500);
                $(id).remove();
              });
              frappe.show_alert(
                {
                  message: 'Uploaded',
                  indicator: 'green',
                },
                5,
              );
            } else {
              frappe.dom.unfreeze();
              try {
                const response = JSON.parse(xhr.responseText);
                const errorMessage = JSON.parse(response?._server_messages);
                if (errorMessage) {
                  frappe.msgprint(errorMessage, 'Error');
                }
              } catch (error) {
                if (xhr.status === 413) {
                  displayHtmlDialog();
                }
              }
            }
          }
        };
        xhr.send(formData);
      });
      return false;
    }
  });
}

function displayHtmlDialog() {
  if (frappe.boot.max_file_size) {
    frappe.msgprint({
      title: __('File too big'),
      indicator: 'red',
      message: __(
        'File size exceeded the maximum allowed size of ' +
          bytesToMB(frappe.boot.max_file_size),
      ),
    });
  }
}

function bytesToMB(bytes) {
  if (bytes === 0) return '0 MB';

  const mb = bytes / (1024 * 1024);
  return `${mb.toFixed(0)} MB`;
}

function addProformaInvoiceBtn(frm) {
  if (frm.doc.docstatus === 1) {
    frm.add_custom_button('Proforma Invoice', () => {
      if (frm.doc.company) {
        const api =
          '/api/method/trusteeship_platform.custom_methods.download_proforma_invoice';
        const query = `doctype=${frm.doctype}&docname=${frm.doc.name}&company_name=${frm.doc.company}`;
        window.open(`${api}?${query}`, '_blank');
      } else {
        frappe.throw('Company is mandatory for Proforma Invoice');
      }
    });
  }
}

function setQuotationFacilityAmt(frm) {
  if (frm.doc.opportunity) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.set_quotation_facility_amt',
      args: {
        doc_name: frm.doc.opportunity,
        facility_amt: frm.doc.cnp_facility_amt,
      },
      freeze: true,
      callback: r => {},
    });
  }
}

frappe.ui.form.on('Quotation Item', {
  form_render: function (frm, cdt, cdn) {
    if (
      frm.doc.workflow_state === 'Submitted' ||
      frm.doc.workflow_state === 'Approved' ||
      frm.doc.workflow_state === 'Accepted By Client'
    ) {
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_frequency',
        'read_only',
        true,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_actual_initial_fee',
        'read_only',
        true,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_actual_annual_fee',
        'read_only',
        true,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_calculated_annual_fee',
        'read_only',
        true,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_calculated_initial_fee',
        'read_only',
        true,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_selected_calculated_fee',
        'read_only',
        true,
      );
    } else {
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_frequency',
        'read_only',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_actual_initial_fee',
        'read_only',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_actual_annual_fee',
        'read_only',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_calculated_annual_fee',
        'read_only',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_calculated_initial_fee',
        'read_only',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_selected_calculated_fee',
        'read_only',
        false,
      );
    }
    if (
      frm.doc.workflow_state === 'Approved' ||
      frm.doc.workflow_state === 'Submitted' ||
      frm.doc.workflow_state === 'Accepted By Client'
    ) {
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_frequency',
        'allow_on_submit',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_actual_initial_fee',
        'allow_on_submit',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_actual_annual_fee',
        'allow_on_submit',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_calculated_annual_fee',
        'allow_on_submit',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_calculated_initial_fee',
        'allow_on_submit',
        false,
      );
      frm.fields_dict.items.grid.update_docfield_property(
        'cnp_selected_calculated_fee',
        'allow_on_submit',
        false,
      );
    }
  },
  item_code: function (frm, cdt, cdn) {
    if (frm.doc.cnp_opportunity) {
      loadPricingTable(frm);

      const row = locals[cdt][cdn];
      setLetterSub(frm);
      if (row.item_code.toUpperCase() === 'STE') {
        frappe.call({
          method:
            'trusteeship_platform.custom_methods.cal_security_trustee_sell_fee',
          args: {
            item_code: row.item_code,
            opportunity: frm.doc.cnp_opportunity,
          },
          freeze: true,
          callback: r => {
            row.cnp_calculated_initial_fee = r.message.initial_cost;
            row.cnp_calculated_annual_fee = r.message.annual_cost;
            row.cnp_actual_initial_fee = r.message.initial_cost;
            row.cnp_actual_annual_fee = r.message.annual_cost;
            row.qty = 1;
          },
        });
      } else if (row.item_code.toUpperCase() === 'DTE') {
        frappe.call({
          method:
            'trusteeship_platform.custom_methods.cal_debanture_trustee_sell_fee',
          args: {
            item_code: row.item_code,
            opportunity: frm.doc.cnp_opportunity,
          },
          freeze: true,
          callback: r => {
            row.cnp_calculated_initial_fee = r.message.initial_cost;
            row.cnp_calculated_annual_fee = r.message.annual_cost;
            row.cnp_actual_initial_fee = r.message.initial_cost;
            row.cnp_actual_annual_fee = r.message.annual_cost;
            row.qty = 1;
          },
        });
      } else {
        row.cnp_calculated_initial_fee = null;
        row.cnp_calculated_annual_fee = null;
        row.cnp_actual_initial_fee = null;
        row.cnp_actual_annual_fee = null;
        row.qty = 1;
      }
    } else {
      frappe.msgprint({
        message:
          'Please select a Customer/Lead and its opportunity to add/edit a product.',
        indicator: 'red',
        title: 'Missing Fields',
      });
      frm.doc.items[0].item_code = '';
      frm.refresh_field('items');
      loadPricingTable(frm);
    }
  },

  cnp_varying_fees: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (
      row.cnp_type_of_fee === 'Annual Fee' &&
      row.cnp_varying_fees === 'Yes'
    ) {
      frm.toggle_reqd('cnp_varying_charge_details', true);
      frm.fields_dict.items.grid.grid_rows_by_docname[
        row.name
      ].docfields.forEach(df => {
        if (df.fieldname === 'cnp_frequency') {
          df.reqd = 1;
        }
        if (df.fieldname === 'cnp_next_fee_date') {
          df.reqd = 1;
        }
      });
    } else {
      frm.toggle_reqd('cnp_varying_charge_details', false);
      frm.fields_dict.items.grid.grid_rows_by_docname[
        row.name
      ].docfields.forEach(df => {
        if (df.fieldname === 'cnp_frequency') {
          df.reqd = 0;
        }
        if (df.fieldname === 'cnp_next_fee_date') {
          df.reqd = 0;
        }
      });
    }
  },

  cnp_next_fee_date: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    validateNextFeeDate(frm, row);
  },

  cnp_type_of_fee: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (
      row.cnp_type_of_fee === 'Annual Fee' &&
      row.cnp_varying_fees === 'Yes'
    ) {
      frm.toggle_reqd('cnp_varying_charge_details', true);
    } else {
      frm.toggle_reqd('cnp_varying_charge_details', false);
    }

    if (row.cnp_type_of_fee === 'Annual Fee') {
      addNextFeeDate(frm);
    }

    setFees(frm, row);
    updateTotalGrandTotalTaxes(frm);
    if (row.cnp_type_of_fee === 'Initial Fee') {
      addPaymentTermDetails(frm, row);
    }
    if (row.cnp_type_of_fee === 'Annual Fee') {
      addPaymentTermDetails(frm, row);
    }
    if (row.cnp_type_of_fee === 'One Time Fee') {
      addPaymentTermDetails(frm, row);
    }
    removePaymentTermDetails(frm);
  },

  rate: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];

    calculatCnpDeviation(row);
    let total = 0;
    frm.doc.items.forEach(item => {
      total += item.rate;
    });
    frm.doc.total = total;
    frm.refresh_field('total');
    frm.refresh_field('items');
    setEditButtonEvent(frm, frm.doc.name.replace(/\s+/g, ''));
  },

  items_remove: (frm, cdt, cdn) => {
    loadPricingTable(frm);
    let total = 0;
    frm.doc.items.forEach(item => {
      total += item.rate;
    });
    frm.doc.total = total;
    frm.refresh_field('total');
    updateTotalGrandTotalTaxes(frm);
    removePaymentTermDetails(frm);
  },

  items_add: (frm, cdt, cdn) => {
    if (!frm.doc.items[0].item_code) {
      getItemFromOpportunity(frm);
    }
    const row = locals[cdt][cdn];
    addDefaultRowValues(frm, row);

    frm.refresh_field('items');
    let total = 0;
    frm.doc.items.forEach(item => {
      total += item.rate;
    });
    frm.doc.total = total;
    frm.refresh_field('total');
    updateTotalGrandTotalTaxes(frm);
  },

  cnp_frequency: (frm, cdt, cdn) => {
    addNextFeeDate(frm);
  },
});

function updateTotalGrandTotalTaxes(frm) {
  let total = 0;
  frm.doc.items.forEach(item => {
    total += item.rate;
  });
  frm.doc.total = total;
  let taxTotal = total;
  let totalTaxAmount = 0;
  frm.doc.taxes.forEach(tax => {
    if (tax.rate) {
      tax.tax_amount = (total * tax.rate) / 100;
    }
    taxTotal += tax.tax_amount;
    tax.total = taxTotal;
    totalTaxAmount += tax.tax_amount;
  });
  frm.doc.total_taxes_and_charges = totalTaxAmount;
  frm.refresh_field('total_taxes_and_charges');
  let grandTotal = 0;
  frm.doc.items.forEach(item => {
    grandTotal += item.rate;
  });
  grandTotal += frm.doc.total_taxes_and_charges;

  frm.doc.grand_total = grandTotal;
  frm.doc.rounded_total = Math.round(grandTotal);
  frm.doc.rounding_adjustment = frm.doc.grand_total - frm.doc.rounded_total;
  frm.refresh_field('taxes');
  frm.refresh_field('total');
  frm.refresh_field('grand_total');
  frm.refresh_field('rounded_total');
  frm.refresh_field('rounding_adjustment');
  frm.refresh_field('items');
}

function disableAutoCalculate(frm) {
  const products = [
    'DTE',
    'STE',
    'EA',
    'LAG',
    'FA',
    'INVIT',
    'REIT',
    'SEC',
    'SA',
    'CUSTA',
    'MAG',
    'ERA',
    'MTST',
    'CAG',
    'ESOP',
    'FTE',
    'SPT',
    'NDUA',
    'P2P',
    'AIF',
  ];
  if (products.includes(frm?.doc.cnp_product?.toUpperCase())) {
    frm.doc.cnp_auto_calculate = 0;
    frm.set_df_property('cnp_auto_calculate', 'read_only', false);
    frm.refresh_field('cnp_auto_calculate');
  } else {
    frm.doc.cnp_auto_calculate = 1;
    frm.set_df_property('cnp_auto_calculate', 'read_only', true);
    frm.refresh_field('cnp_auto_calculate');
  }
}

function checkOpportunityPricingPolicy(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'Canopi Pricing Policy',
      doc_name: frm.doc.name + '-' + frm.doc.cnp_product,
    },
    callback: r => {
      if (
        r.message.opportunity === frm.doc.name &&
        ((r.message.no_of_securities_check && !r.message.no_of_securities) ||
          (r.message.immovable_properties_check &&
            !r.message.immovable_properties) ||
          (r.message.states_immovable_properties_check &&
            !r.message.states_immovable_properties) ||
          (r.message.no_of_lenders_check && !r.message.no_of_lenders) ||
          (r.message.listed_yes_no_check && !r.message.listed_yes_no) ||
          (r.message.no_of_sellers_check && !r.message.no_of_sellers) ||
          (r.message.fund_category_check && !r.message.fund_category) ||
          (r.message.assets_check && !r.message.assets) ||
          (r.message.da_yes_no_check && !r.message.da_yes_no) ||
          (r.message.fund_size_in_rscr_check &&
            !r.message.fund_size_in_rs_cr) ||
          (r.message.payment_frequency_mqh_check &&
            !r.message.payment_frequency) ||
          (r.message.whether_public_issue_check &&
            !r.message.whether_public_issue) ||
          (r.message.value_added_services_check &&
            !r.message.value_added_services))
      ) {
        frm.disable_save();
        $("[data-doctype='Quotation']").hide();
        frappe.hide_msgprint();
        frappe.msgprint({
          title: 'Warning',
          message: 'Please fill in the pricing policy parameters',
          primary_action: {
            label: 'Proceed',
            action() {
              localStorage.setItem('opportunity_reload', '1');
              frappe.set_route('Form', 'Canopi Pricing Policy', r.message.name);
            },
          },
        });
      }
    },
  });
}

function getItemFromOpportunity(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: { doc_name: frm.doc.cnp_opportunity, doc_type: 'Opportunity' },
    freeze: true,
    callback: r => {
      frm.doc.cnp_facility_amt = r.message.cnp_facility_amt;
      frm.refresh_field('cnp_facility_amt');
      frm.doc.cnp_facility_amt_in_words = r.message.cnp_facility_amt_in_words;
      frm.toggle_display(
        'cnp_facility_amt_in_words',
        !!r.message.cnp_facility_amt,
      );
      frm.refresh_field('cnp_facility_amt_in_words');

      frm.doc.cnp_facility_amt_in_words_millions =
        r.message.cnp_facility_amt_in_words_millions;
      frm.toggle_display(
        'cnp_facility_amt_in_words_millions',
        !!r.message.cnp_facility_amt,
      );
      frm.refresh_field('cnp_facility_amt_in_words_millions');

      frm.doc.cnp_product = r.message.cnp_product;
      frm.refresh_field('cnp_product');

      frm.doc.items = [];
      const row = frm.add_child('items');
      row.item_code = r.message.items[0].item_code;
      row.item_name = r.message.items[0].item_name;
      row.qty = r.message.items[0].qty;
      row.description = r.message.items[0].item_name;
      row.uom = 'Nos';
      row.cnp_actual_annual_fee = r.message.items[0].cnp_actual_annual_fee;
      row.cnp_calculated_annual_fee =
        r.message.items[0].cnp_calculated_annual_fee;
      row.cnp_actual_initial_fee = r.message.items[0].cnp_actual_initial_fee;
      row.cnp_calculated_initial_fee =
        r.message.items[0].cnp_calculated_initial_fee;
      row.cnp_one_time_fee_actual = r.message.items[0].cnp_one_time_fee_actual;
      disableAutoCalculate(frm);

      frm.refresh_field('items');
      loadPricingTable(frm);
      setLetterSub(frm);
      loadOtherTermsAndConditions(frm);
    },
  });
}

function setDefaultFeesValues(row) {
  if (row.cnp_type_of_fee === 'Initial Fee') {
    row.cnp_selected_calculated_fee = row.cnp_calculated_initial_fee;
    row.rate = row.cnp_actual_initial_fee;
  } else if (row.cnp_type_of_fee === 'Annual Fee') {
    row.cnp_selected_calculated_fee = row.cnp_calculated_annual_fee;
    row.rate = row.cnp_actual_annual_fee;
  } else if (row.cnp_type_of_fee === 'One Time Fee') {
    row.cnp_selected_calculated_fee = row.cnp_one_time_fee;
    row.rate = row.cnp_one_time_fee_actual;
  }
}

function loadPricingTable(frm) {
  $(frm.fields_dict.cnp_pricing_policy_html.wrapper).empty();
  if (frm.doc.items.length !== 0 && frm.doc.items[0].item_code !== undefined) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: { doctype: 'Canopi Pricing Policy' },
      freeze: true,
      callback: r => {
        const product = r.message.find(
          element =>
            element.name ===
            frm.doc.cnp_opportunity + '-' + frm.doc?.items[0]?.item_code,
        );
        if (product) {
          const id = frm.doc.name.replace(/\s+/g, '');
          frm.set_df_property(
            'cnp_pricing_policy_html',
            'options',
            frappe.render_template('pricing_policy_table', {
              doc: product,
              id,
              status: frm.doc.docstatus,
              workflow_state: frm.doc.workflow_state,
            }),
          );
          frm.refresh_field('cnp_pricing_policy_html');
          setEditButtonEvent(frm, id);
        }
      },
    });
  }
}

function setEditButtonEvent(frm, id) {
  setTimeout(() => {
    $('.' + id + '-edit-button').on('click', () => {
      redirectToPricingpolicy(frm);
    });
  }, 1000);
}

function redirectToPricingpolicy(frm) {
  if (frm.is_dirty()) {
    frm
      .save()
      .then(() =>
        frappe.set_route(
          'Form',
          'Canopi Pricing Policy',
          frm.doc.cnp_opportunity + '-' + frm.doc?.items[0]?.item_code,
        ),
      );
  } else {
    frappe.set_route(
      'Form',
      'Canopi Pricing Policy',
      frm.doc.cnp_opportunity + '-' + frm.doc?.items[0]?.item_code,
    );
  }
}

function setOpportunityFilter(frm) {
  if (frm.doc.quotation_to && frm.doc.party_name) {
    frm.set_df_property('cnp_opportunity', 'read_only', false);
    frm.set_query('cnp_opportunity', filter => {
      return {
        filters: [
          ['Opportunity', 'opportunity_from', '=', frm.doc.quotation_to],
          ['Opportunity', 'party_name', '=', frm.doc.party_name],
        ],
      };
    });
  } else {
    frm.set_df_property('cnp_opportunity', 'read_only', true);
  }
}

function setAnnexureTemplateFilter(frm) {
  frm.set_query('cnp_annexure_template', filter => {
    return {
      filters: [
        [
          'Canopi Annexure Template',
          'product',
          '=',
          frm.doc.items[0].item_code,
        ],
      ],
    };
  });
}

function setFees(frm, row) {
  if (row.cnp_type_of_fee === 'Initial Fee' && !frm.doc.cnp_auto_calculate) {
    row.cnp_calculated_initial_fee =
      frm.doc.items[0].cnp_calculated_initial_fee;
    row.cnp_selected_calculated_fee = row.cnp_calculated_initial_fee;
  } else if (
    row.cnp_type_of_fee === 'Annual Fee' &&
    !frm.doc.cnp_auto_calculate
  ) {
    row.cnp_calculated_annual_fee = frm.doc.items[0].cnp_calculated_annual_fee;
    row.cnp_selected_calculated_fee = row.cnp_calculated_annual_fee;
  } else if (
    row.cnp_type_of_fee === 'One Time Fee' &&
    !frm.doc.cnp_auto_calculate
  ) {
    row.cnp_one_time_fee = frm.doc.items[0].cnp_one_time_fee;
    row.cnp_selected_calculated_fee = row.cnp_one_time_fee_actual;
  } else {
    row.cnp_selected_calculated_fee = 0;
    row.rate = 0;
  }
  calculatCnpDeviation(row);
}

function calculatCnpDeviation(row) {
  const differenceInFees = row.rate - row.cnp_selected_calculated_fee;
  row.cnp_deviation =
    (differenceInFees / row.cnp_selected_calculated_fee) * 100 === Infinity ||
    (differenceInFees / row.cnp_selected_calculated_fee) * 100 === -Infinity
      ? 0
      : (differenceInFees / row.cnp_selected_calculated_fee) * 100;
}

function convertFeeToCurrencySelected(frm) {
  if (frm.doc.currency === 'INR') {
    frm.doc.items.forEach(row => {
      setFees(frm, row);
    });
  } else {
    frm.doc.items.forEach(row => {
      row.cnp_selected_calculated_fee = Math.round(
        row.cnp_selected_calculated_fee / frm.doc.conversion_rate,
      );
      row.rate = Math.round(row.rate / frm.doc.conversion_rate);
      calculatCnpDeviation(row);
    });
  }
  frm.refresh_field('items');
}

function setLetterSub(frm) {
  const itemCode = frm.doc.items[0]?.item_code
    ? frm.doc.items[0].item_code
    : '';
  let facilityAmt = frm.doc.cnp_facility_amt ? frm.doc.cnp_facility_amt : '';
  if (facilityAmt > 0) {
    facilityAmt = facilityAmt.replace(/(0000000)$/, ' CR');
    facilityAmt = facilityAmt.replace(/(00000)$/, ' L');
  }
  if (frm.doc.cnp_opportunity) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: frm.doc.cnp_opportunity, doc_type: 'Opportunity' },
      freeze: true,
      callback: opportunity => {
        let customer_name = '';
        if (frm.doc.customer_name) {
          customer_name = frm.doc.customer_name;
        } else {
          customer_name = frm.doc.contact_display;
        }

        if (opportunity.message.cnp_product.toUpperCase() === 'STE') {
          frm.doc.cnp_letter_subject =
            'Offer to act as ' +
            itemCode +
            ' for ' +
            opportunity.message.cnp_type_of_security +
            ' to Rs. ' +
            facilityAmt;
          frm.refresh_field('cnp_letter_subject');
        } else if (opportunity.message.cnp_product.toUpperCase() === 'DTE') {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: {
              doc_name:
                frm.doc.cnp_opportunity + '-' + opportunity.message.cnp_product,
              doc_type: 'Canopi Pricing Policy',
            },
            freeze: true,
            callback: pricingPolicy => {
              const isListed =
                pricingPolicy.message.listed_yes_no === 'Yes'
                  ? 'listed '
                  : 'Un listed ';
              const isSecured =
                pricingPolicy.message.no_of_securities > 0
                  ? 'Secured '
                  : 'Unsecured ';
              frm.doc.cnp_letter_subject =
                'Offer to act as ' +
                itemCode +
                ' for ' +
                isListed +
                isSecured +
                opportunity.message.cnp_type_of_security +
                ' debentures (NCDs) aggregating up to Rs. ' +
                facilityAmt +
                ' proposed to be issued by ' +
                frm.doc.company +
                ' Issuer Company.';
              frm.refresh_field('cnp_letter_subject');
            },
          });
        } else if (opportunity.message.cnp_product.toUpperCase() === 'REIT') {
          frm.doc.cnp_letter_subject =
            customer_name +
            ' (Company) - Offer to act as Escrow Agent for transactions viz. ' +
            frm.doc.cnp_facility_amt;
        } else if (opportunity.message.cnp_product.toUpperCase() === 'INVIT') {
          frm.doc.cnp_letter_subject =
            customer_name +
            ' (Company) - Offer to act as Trustee for Proposed Listed Privately/Public Placed Infrastructure Investment Trust (INVIT) upto Rs. ' +
            frm.doc.cnp_facility_amt;
        } else if (opportunity.message.cnp_product.toUpperCase() === 'EA') {
          frm.doc.cnp_letter_subject =
            customer_name +
            ' (Company) - Offer to act as Escrow Agent for transactions viz. ' +
            frm.doc.cnp_facility_amt;
        } else {
          frm.doc.cnp_letter_subject = '';
          frm.refresh_field('cnp_letter_subject');
        }
        frm.refresh_field('cnp_letter_subject');
      },
    });
  }
}

function loadTermsAndConditions(frm) {
  frm.doc.cnp_letter_body = [];

  /* eslint-disable max-len */
  const termsAndConditions = [
    'Fee Schedule',
    `GST Registration Number and Billing Address:<br />
   <br />
   Kindly provide Company's GST registration number (provisional / final) and the billing address (if the billing address is different from the address in this offer letter) alongwith your acceptance of the offer.
   <br />
   <br />
   Please note that in absence of any advice from you for raising of invoices on a specific address, ATSL shall be issuing all the invoices on the address mentioned in the offer letter and shall not be liable or responsible for any additional tax levies or claims arising on account of change in billing address.`,
    'The One Time Fee plus all applicable taxes shall be payable within 30 days from the date of issuance of invoice or from the date of acceptance of the offer letter, whichever of earlier.',
    'The Initial Fee plus all applicable taxes shall be payable within 15 days from the date of issuance of invoice. The initial fee is non-refundable. The Initial Fee shall not be subject to execution of finance and security documents/transaction documents or completion of the transaction.',
    'The Annual Fee plus all applicable taxes shall be payable within 30 days from the date of issuance of invoice. The first annual fee will cover the period from the date of acceptance of the offer till end of the Financial Year. The subsequent annual fee shall be payable Financial Year wise until cessation of our services and/or satisfaction of charges, if any, on the security (ies) to the transaction and issuance of No Dues Certificate/ No Objection Certificate by us. The Annual Fee may be revised as per the mutually agreed terms between the Company and ATSL, from time to time.',
    'All out of pocket expenses (excluding those set out in Sr.no. 7) like documentation execution related expenses to the extent of Rs.10,000/- per instance shall be borne by the Company.',
    'The legal counsel fees, traveling expenses, inspection charges, audit expenses etc. in connection with the transaction will be paid by the Company on an actual basis alongwith all applicable taxes and reimbursed to ATSL within a period of 30 days from the date of issuance of invoice.',
    'The CERSAI filing charges shall be borne by the Company on actual basis. Further, CERSAI entries over and above 50 in number, shall be chargeable with an additional cost of Rs. 100/- per entry plus all applicable taxes or as may be agreed mutually.',
    'All payments shall be made within the timelines specified in the Offer Letter, failing which the Company shall be liable to pay interest as per the provisions of the Micro, Small and Medium Enterprises Development Act, 2006.',
    'In the event the Lenders/Investors declare an event of default and instruct ATSL to initiate, commence or assist in any enforcement proceedings/action on their behalf, then additional fees, as may be determined at ATSL’s sole discretion shall be chargeable for such enforcement services. This clause shall be considered to form an integral part of the finance and security documents/transaction documents executed in connection with the facility by the lenders/investors.',
    'This Offer is valid for a period of 15 days from the date of the letter.',
  ];
  /* eslint-enable max-len */

  termsAndConditions.forEach(element => {
    const row = frm.add_child('cnp_letter_body');
    row.content = element;
  });

  frm.refresh_fields('cnp_letter_body');
}

function loadOtherTermsAndConditions(frm) {
  let letterFrom = '';
  if (frm.doc.cnp_letter_from === 'ATSL') {
    letterFrom = 'ATSL';
  } else if (frm.doc.cnp_letter_from === 'AXIS') {
    letterFrom = 'AXIS';
  }
  const facilityAmt = frm.doc.cnp_facility_amt ? frm.doc.cnp_facility_amt : '';
  frm.doc.cnp_other_terms_and_conditions = [];

  /* eslint-disable max-len */
  const otherTermsAndConditions = [
    'The terms of this Offer Letter shall be applicable to the captioned facility/facilities aggregating to Rs. ' +
      facilityAmt +
      ' Crores extended by the current set of lenders/investors and the documentation entered into in relation thereto. Any enhancement in facility/facilities, extension of security to new lenders, further/additional issuance of debentures, additional documentation over and above contemplated under this Offer Letter shall be charged separately.',
    'This letter may be amended, revised, modified (and the provisions hereof may only be waived) by agreement in writing by the parties hereto.',
    'The draft of the documents shall be provided by the Company at least 2 working days prior to the proposed date of execution. The Company shall execute all the requisite documents, as may be necessary, as per the agreed drafts.',
    letterFrom +
      ', its officers, employees, directors, and agents as a Service Provider shall have no liability, save and except in case of gross negligence and wilful misconduct, as may be finally determined by a court of competent jurisdiction.',
    'The Offer Letter shall form an integral part of the finance and security/transaction documents and the terms & conditions hereunder shall be construed to form part and parcel of the transaction documents to be executed. In the event of any conflict or contradicting terms & conditions, the terms of this Offer Letter shall prevail.',
    'This offer is subject to the acceptance of the terms enumerated herein and KYC clearance as per the ' +
      letterFrom +
      "'s policy.",
    letterFrom +
      ' shall commence the provision of its services, upon receipt of the duly accepted offer letter and due diligence documents as per Annexure hereto.',
  ];
  /* eslint-enable max-len */

  otherTermsAndConditions.forEach(element => {
    const row = frm.add_child('cnp_other_terms_and_conditions');
    row.content = element;
  });
  frm.refresh_field('cnp_other_terms_and_conditions');
}

function hideMenuItems(frm) {
  const elements = document.querySelectorAll('a.grey-link ');
  elements.forEach(element => {
    // hide Print option from menu dropdown
    if (element.firstElementChild.innerText === 'Print') {
      element.hidden = true;
    }

    // hide Email option from menu dropdown
    if (element.firstElementChild.innerText === 'Email') {
      element.hidden = true;
    }
  });
}

function hidePrintIcon() {
  const buttons = document.querySelectorAll(
    '.text-muted.btn.btn-default.icon-btn',
  );
  buttons.forEach(element => {
    if (
      element.attributes.getNamedItem('data-original-title')?.value === 'Print'
    ) {
      element.hidden = true;
    }
  });
}

function hideConnectionSalesOrder() {
  $("[data-doctype='Sales Order']").parent().hide();
}

function hideConnectionTab(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'ATSL Mandate List',
      fields: '["quotation"]',
      filters: [['quotation', '=', frm.doc.name]],
    },
    freeze: true,
    callback: r => {
      if (r.message.length === 0 && frm.doc.docstatus === 0) {
        frm.dashboard.hide();
      } else {
        frm.dashboard.show();
      }
    },
  });
}

function addSalesInvoice(frm) {
  $("button[data-doctype='Sales Invoice']").unbind();
  $("button[data-doctype='Sales Invoice']").on('click', function () {
    frappe.model.open_mapped_doc({
      method: 'trusteeship_platform.custom_methods.make_sales_invoice',
      frm,
    });
  });
}

function hideActions(frm) {
  const isApprovedOrSubmitted =
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Submitted';
  const isS3KeyEmpty = !frm.doc.cnp_s3_key;
  const isAcceptanceDateUndefined = !frm.doc.cnp_offer_acceptence_date;
  if (isApprovedOrSubmitted && isS3KeyEmpty && isAcceptanceDateUndefined) {
    $("[data-label='Actions']").parent().parent().hide();
  } else {
    $("[data-label='Actions']").parent().parent().show();
  }
}

function addOfferLetterBtn(frm) {
  if (!frm.doc.__islocal) {
    frm.add_custom_button('Offer Letter', () => {
      const api =
        '/api/method/trusteeship_platform.custom_methods.download_signed_pdf';
      const query = `doctype=${frm.doctype}&docname=${frm.doc.name}`;
      window.open(`${api}?${query}`, '_blank');
    });
  }
}

function makeOfferLetterDetailsReadonly(frm) {
  if (!frm.doc.cnp_send_for_approval) {
    frm.set_df_property('cnp_letter_subject', 'read_only', true);
    frm.set_df_property('cnp_letter_body', 'read_only', true);
    frm.set_df_property('cnp_other_terms_and_conditions', 'read_only', true);
    frm.set_df_property('cnp_annexure', 'read_only', true);
    frm.set_df_property('cnp_annexure_template', 'read_only', true);
  }
}

function addEditButton(frm) {
  if (frm.doc.docstatus === 0 && frm.doc.cnp_send_for_approval !== 1) {
    frm.add_custom_button('Edit T&C', () => {
      frm.fields_dict.cnp_offer_letter.collapse(false);
      frm.scroll_to_field('cnp_letter_subject');

      frm.doc.cnp_send_for_approval = 1;
      frm.dirty();
      frm.set_df_property('cnp_letter_subject', 'read_only', false);
      frm.set_df_property('cnp_letter_body', 'read_only', false);
      frm.set_df_property('cnp_other_terms_and_conditions', 'read_only', false);
      frm.set_df_property('cnp_annexure', 'read_only', false);
      frm.set_df_property('cnp_annexure_template', 'read_only', false);

      if (!frm.doc.cnp_letter_body.length) {
        frm.add_child('cnp_letter_body');
        frm.refresh_field('cnp_letter_body');
      }

      if (!frm.doc.cnp_other_terms_and_conditions.length) {
        frm.add_child('cnp_other_terms_and_conditions');
        frm.refresh_field('cnp_other_terms_and_conditions');
      }

      frm.doc.cnp_annexure = [];
      frm.refresh_field('cnp_annexure');
      frm.remove_custom_button('Edit T&C');
    });
  }
}

function addCustomEmailIcon(frm) {
  if (frm.doc.docstatus !== 0) {
    frm.page.add_action_icon('mail', () => {
      emailDialog();
    });
  }
}

function setRegisteredAddressFilter(frm) {
  frm.set_query('cnp_registered_address', function () {
    return {
      filters: [
        ['Address', 'is_your_company_address', '=', 1],
        ['Address', 'address_type', '=', 'Registered Office'],
      ],
    };
  });
}

function setCorporateAddressFilter(frm) {
  if (frm.doc.cnp_registered_address) {
    frm.toggle_reqd('cnp_corporate_address', true);
    frm.toggle_display('cnp_corporate_address', true);
    frm.set_df_property('cnp_corporate_address', 'label', 'Corporate Office');
    frm.set_query('cnp_corporate_address', function () {
      return {
        filters: [
          ['Address', 'is_your_company_address', '=', 1],
          ['Address', 'address_type', '=', 'Corporate Office'],
        ],
      };
    });
  } else {
    frm.toggle_display('cnp_corporate_address', false);
    frm.toggle_reqd('cnp_corporate_address', false);
    frm.doc.cnp_corporate_address = '';
  }
}

function setDefaultCompanyAddress(frm) {
  if (frm.doc.cnp_cost_center) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.cost_center_default_address',
      args: {
        cost_center: frm.doc.cnp_cost_center,
      },
      callback: r => {
        let default_address = r.message;
        if (default_address === undefined) {
          default_address = '';
        }
        frm.doc.company_address = default_address;
        frm.refresh_field('company_address');
        frm.trigger('company_address');
      },
    });
  }
}

const dialog = new frappe.ui.Dialog({
  title: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
  no_submit_on_enter: true,
  fields: getFields(),
  primary_action_label: 'Send',
  primary_action() {
    sendAction();
  },
  secondary_action_label: 'Discard',
  secondary_action() {
    dialog.hide();
  },
  size: 'large',
  minimizable: true,
});

function emailDialog() {
  $(dialog.$wrapper.find('.form-section').get(0)).addClass('to_section');
  prepare();
  dialog.show();
}

function prepare() {
  setupMultiselectQueries();
  setupAttach();
  setupEmail();
  setupEmailTemplate();
}

function setupMultiselectQueries() {
  ['recipients', 'cc', 'bcc'].forEach(field => {
    dialog.fields_dict[field].get_data = () => {
      const data = dialog.fields_dict[field].get_value();
      const txt = data.match(/[^,\s*]*$/)[0] || '';

      frappe.call({
        method: 'frappe.email.get_contact_list',
        args: { txt },
        freeze: true,
        callback: r => {
          dialog.fields_dict[field].set_data(r.message);
        },
      });
    };
  });
}

let attachments = [];

function setupAttach() {
  const fields = dialog.fields_dict;
  const attach = $(fields.select_attachments.wrapper);

  if (!attachments) {
    attachments = [];
  }

  let args = {
    folder: 'Home/Attachments',
    on_success: attachment => {
      attachments.push(attachment);
      renderAttachmentRows(attachment);
    },
  };

  if (cur_frm) {
    args = {
      doctype: cur_frm.doctype,
      docname: cur_frm.docname,
      folder: 'Home/Attachments',
      on_success: attachment => {
        cur_frm.attachments.attachment_uploaded(attachment);
        renderAttachmentRows(attachment);
      },
    };
  }

  $(`
    <label class="control-label">
      ${'Select Attachments'}
    </label>
    <div class='attach-list'></div>
    <p class='add-more-attachments'>
      <button class='btn btn-xs btn-default'>
        ${frappe.utils.icon('small-add', 'xs')}&nbsp;
        ${'Add Attachment'}
      </button>
    </p>
  `).appendTo(attach.empty());

  attach
    .find('.add-more-attachments button')
    .on('click', () => new frappe.ui.FileUploader(args));
  renderAttachmentRows();
}

function renderAttachmentRows(attachment = null) {
  const selectAttachments = dialog.fields_dict.select_attachments;
  const attachmentRows = $(selectAttachments.wrapper).find('.attach-list');
  if (attachment) {
    attachmentRows.append(getAttachmentRow(attachment, true));
  } else {
    let files = [];
    if (attachments && attachments.length) {
      files = files.concat(attachments);
    }
    if (cur_frm) {
      files = files.concat(cur_frm.get_files());
    }

    if (files.length) {
      $.each(files, (i, f) => {
        if (!f.file_name) return;
        if (!attachmentRows.find(`[data-file-name="${f.name}"]`).length) {
          f.file_url = frappe.urllib.get_full_url(f.file_url);
          attachmentRows.append(getAttachmentRow(f));
        }
      });
    }
  }
}

function getAttachmentRow(attachment, checked = null) {
  return $(`<p class="checkbox flex">
    <label class="ellipsis" title="${attachment.file_name}">
      <input
        type="checkbox"
        data-file-name="${attachment.name}"
        ${checked ? 'checked' : ''}>
      </input>
      <span class="ellipsis">${attachment.file_name}</span>
    </label>
    &nbsp;
    <a href="${attachment.file_url}" target="_blank" class="btn-linkF">
      ${frappe.utils.icon('link-url')}
    </a>
  </p>`);
}

function setupEmail() {
  // email
  const fields = dialog.fields_dict;

  $(fields.send_me_a_copy.input).on('click', () => {
    // update send me a copy (make it sticky)
    const val = fields.send_me_a_copy.get_value();
    frappe.db.set_value('User', frappe.session.user, 'send_me_a_copy', val);
    frappe.boot.user.send_me_a_copy = val;
  });
}

function setupEmailTemplate() {
  dialog.fields_dict.email_template.df.onchange = () => {
    const emailTemplate = dialog.fields_dict.email_template.get_value();
    if (!emailTemplate) return;

    frappe.call({
      method:
        'frappe.email.doctype.email_template.email_template.get_email_template',
      args: {
        template_name: emailTemplate,
        doc: cur_frm.doc,
        _lang: dialog.get_value('language_sel'),
      },
      freeze: true,
      callback(r) {
        prependReply(r.message, emailTemplate);
      },
    });
  };
}

let replyAdded = '';
function prependReply(reply, emailTemplate) {
  if (replyAdded === emailTemplate) return;
  const contentField = dialog.fields_dict.content;
  const subjectField = dialog.fields_dict.subject;

  let content = contentField.get_value() || '';
  content = content.split('<!-- salutation-ends -->')[1] || content;

  contentField.set_value(`${reply.message}<br>${content}`);
  subjectField.set_value(reply.subject);

  replyAdded = emailTemplate;
}

function sendAction() {
  const formValues = getValues();
  if (!formValues) return;

  const selectedAttachments = $.map(
    $(dialog.wrapper).find('[data-file-name]:checked'),
    function (element) {
      return $(element).attr('data-file-name');
    },
  );
  sendEmail(formValues, selectedAttachments);
}

function getValues() {
  const formValues = dialog.get_values();

  // cc
  for (let i = 0, l = dialog.fields.length; i < l; i++) {
    const df = dialog.fields[i];

    if (df.is_cc_checkbox) {
      // concat in cc
      if (formValues[df.fieldname]) {
        formValues.cc =
          (formValues.cc ? formValues.cc + ', ' : '') + df.fieldname;
        formValues.bcc =
          (formValues.bcc ? formValues.bcc + ', ' : '') + df.fieldname;
      }

      delete formValues[df.fieldname];
    }
  }

  return formValues;
}

function sendEmail(formValues, selectedAttachments) {
  dialog.hide();

  if (!formValues.recipients) {
    frappe.msgprint('Enter Email Recipient(s)');
    return;
  }

  if (cur_frm && !frappe.model.can_email(cur_frm.doc.doctype, cur_frm)) {
    frappe.msgprint(
      'You are not allowed to send emails related to this document',
    );
    return;
  }

  frappe.call({
    method: 'trusteeship_platform.custom_methods.send_mail',
    args: {
      recipients: formValues.recipients,
      cc: formValues.cc,
      bcc: formValues.bcc,
      subject: formValues.subject,
      content: formValues.content,
      doctype: cur_frm.doc.doctype,
      name: cur_frm.doc.name,
      send_me_a_copy: formValues.send_me_a_copy,
      attachments: selectedAttachments,
      read_receipt: formValues.send_read_receipt,
      attach_offer_letter: formValues.attach_offer_letter,
      attach_proforma_invoice: formValues.attach_proforma_invoice,
      sender: formValues.sender,
    },
    freeze: true,
    freeze_message: 'Sending',

    callback: r => {
      if (!r.exc) {
        frappe.utils.play_sound('email');

        if (r.message?.emails_not_sent_to) {
          frappe.msgprint(
            ('Email not sent to {0} (unsubscribed / disabled)',
            [frappe.utils.escape_html(r.message.emails_not_sent_to)]),
          );
        }

        if (cur_frm) {
          cur_frm.reload_doc();
        }
      } else {
        frappe.msgprint(
          'There were errors while sending email. Please try again.',
        );
      }
    },
  });
}

function getFields() {
  const fields = [
    {
      label: 'To',
      fieldtype: 'MultiSelect',
      reqd: 0,
      fieldname: 'recipients',
    },
    {
      fieldtype: 'Button',
      label: frappe.utils.icon('down'),
      fieldname: 'option_toggle_button',
      click: () => {
        toggleMoreOptions(false);
      },
    },
    {
      fieldtype: 'Section Break',
      hidden: 1,
      fieldname: 'more_options',
    },
    {
      label: 'CC',
      fieldtype: 'MultiSelect',
      fieldname: 'cc',
    },
    {
      label: 'BCC',
      fieldtype: 'MultiSelect',
      fieldname: 'bcc',
    },
    {
      label: 'Email Template',
      fieldtype: 'Link',
      options: 'Email Template',
      fieldname: 'email_template',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Subject',
      fieldtype: 'Data',
      reqd: 1,
      fieldname: 'subject',
      default: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
      length: 524288,
    },
    {
      label: 'Message',
      fieldtype: 'Text Editor',
      fieldname: 'content',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Send me a copy',
      fieldtype: 'Check',
      fieldname: 'send_me_a_copy',
      default: frappe.boot.user.send_me_a_copy,
    },
    {
      label: 'Send Read Receipt',
      fieldtype: 'Check',
      fieldname: 'send_read_receipt',
    },
    {
      label: 'Attach Offer Letter',
      fieldtype: 'Check',
      fieldname: 'attach_offer_letter',
      default: 1,
    },
    {
      label: 'Attach Proforma Invoice',
      fieldtype: 'Check',
      fieldname: 'attach_proforma_invoice',
      default: 1,
    },
    { fieldtype: 'Column Break' },
    {
      label: 'Select Attachments',
      fieldtype: 'HTML',
      fieldname: 'select_attachments',
    },
  ];

  // add from if user has access to multiple email accounts
  const emailAccounts = frappe.boot.email_accounts.filter(account => {
    return (
      !in_list(
        ['All Accounts', 'Sent', 'Spam', 'Trash'],
        account.email_account,
      ) && account.enable_outgoing
    );
  });

  if (emailAccounts.length) {
    fields.unshift({
      label: 'From',
      fieldtype: 'Select',
      reqd: 1,
      fieldname: 'sender',
      options: emailAccounts.map(function (e) {
        return e.email_id;
      }),
    });
  }

  return fields;
}

function toggleMoreOptions(showOptions) {
  showOptions = showOptions || dialog.fields_dict.more_options.df.hidden;
  dialog.set_df_property('more_options', 'hidden', !showOptions);

  const label = frappe.utils.icon(showOptions ? 'up-line' : 'down');
  dialog.get_field('option_toggle_button').set_label(label);
}

function getPricingPolicyCondition(annexure) {
  const conditions = [];
  const pricingPolicyList = {
    no_of_securities: 'no_of_securities',
    immovable_properties: 'immovable_properties',
    states_immovable_properties: 'states_immovable_properties',
    no_of_lenders: 'no_of_lenders',
    listed_yes_no: 'listed_yes_no',
    no_of_sellers: 'no_of_sellers',
    fund_category: 'fund_category',
    assets: 'assets',
    da_yes_no: 'da_yes_no',
    fund_size_in_rs_cr: 'fund_size_in_rs_cr',
    payment_frequency: 'payment_frequency',
    whether_public_issue: 'whether_public_issue',
    value_added_services: 'value_added_services',
  };

  if (annexure.no_of_securities) {
    conditions.push(pricingPolicyList.no_of_securities);
  }
  if (annexure.immovable_properties) {
    conditions.push(pricingPolicyList.immovable_properties);
  }
  if (annexure.states_immovable_properties) {
    conditions.push(pricingPolicyList.states_immovable_properties);
  }
  if (annexure.no_of_lenders) {
    conditions.push(pricingPolicyList.no_of_lenders);
  }
  if (annexure.listed_yes_no) {
    conditions.push(pricingPolicyList.listed_yes_no);
  }
  if (annexure.no_of_sellers) {
    conditions.push(pricingPolicyList.no_of_sellers);
  }
  if (annexure.fund_category) {
    conditions.push(pricingPolicyList.fund_category);
  }
  if (annexure.assets) {
    conditions.push(pricingPolicyList.assets);
  }
  if (annexure.da_yes_no) {
    conditions.push(pricingPolicyList.da_yes_no);
  }
  if (annexure.fund_size_in_rs_cr) {
    conditions.push(pricingPolicyList.fund_size_in_rs_cr);
  }
  if (annexure.payment_frequency) {
    conditions.push(pricingPolicyList.payment_frequency);
  }
  if (annexure.whether_public_issue) {
    conditions.push(pricingPolicyList.whether_public_issue);
  }
  if (annexure.value_added_services) {
    conditions.push(pricingPolicyList.value_added_services);
  }

  return conditions;
}

function getTypeOfSecurityCondition(annexure) {
  const conditions = [];
  const typeOfSecurityList = {
    immovable: 'Immovable Asset',
    movable: 'Movable Asset',
    pledge: 'Pledge',
    personal_guarantee: 'Personal Guarantee',
    corporate_guarantee: 'Corporate Guarantee',
    ndu: 'NDU',
    intangible: 'Intangible Asset',
    motor_vehicle: 'Motor Vehicle',
    financial_asset: 'Financial Asset',
    assignment_of_rights: 'Assignment of Rights',
    current_asset: 'Current Asset',
    dsra: 'Debt Service Reserve Account (DSRA)',
    others: 'Others',
  };

  if (annexure.immovable) {
    conditions.push(typeOfSecurityList.immovable);
  }
  if (annexure.movable) {
    conditions.push(typeOfSecurityList.movable);
  }
  if (annexure.pledge) {
    conditions.push(typeOfSecurityList.pledge);
  }
  if (annexure.personal_guarantee) {
    conditions.push(typeOfSecurityList.personal_guarantee);
  }
  if (annexure.corporate_guarantee) {
    conditions.push(typeOfSecurityList.corporate_guarantee);
  }
  if (annexure.ndu) {
    conditions.push(typeOfSecurityList.ndu);
  }
  if (annexure.intangible) {
    conditions.push(typeOfSecurityList.intangible);
  }
  if (annexure.motor_vehicle) {
    conditions.push(typeOfSecurityList.motor_vehicle);
  }
  if (annexure.financial_asset) {
    conditions.push(typeOfSecurityList.financial_asset);
  }
  if (annexure.assignment_of_rights) {
    conditions.push(typeOfSecurityList.assignment_of_rights);
  }
  if (annexure.current_asset) {
    conditions.push(typeOfSecurityList.current_asset);
  }
  if (annexure.dsra) {
    conditions.push(typeOfSecurityList.dsra);
  }
  if (annexure.others) {
    conditions.push(typeOfSecurityList.others);
  }
  return conditions;
}

function checkConditions(
  typeOfSecurityCondition,
  typeOfSecurity,
  pricingPolicyCondition,
  pricingPolicy,
) {
  let isSecurityTypeValid = typeOfSecurityCondition.length === 0;
  let isPricingPolicyValid = pricingPolicyCondition.length === 0;

  if (typeOfSecurityCondition.length !== 0) {
    isSecurityTypeValid = typeOfSecurityCondition.some(element =>
      typeOfSecurity.includes(element),
    );
  }

  pricingPolicyCondition.forEach(element => {
    if (
      ((element === 'no_of_securities' ||
        element === 'immovable_properties' ||
        element === 'states_immovable_properties' ||
        element === 'no_of_lenders' ||
        element === 'no_of_sellers' ||
        element === 'assets' ||
        element === 'fund_size_in_rs_cr' ||
        element === 'value_added_services') &&
        pricingPolicy[`${element}`] !== 0) ||
      ((element === 'listed_yes_no' ||
        element === 'da_yes_no' ||
        element === 'whether_public_issue') &&
        pricingPolicy[`${element}`] === 'Yes') ||
      ((element === 'fund_category' || element === 'payment_frequency') &&
        pricingPolicy[`${element}`] !== '')
    ) {
      isPricingPolicyValid = true;
    } else {
      isPricingPolicyValid = false;
    }
  });

  return isPricingPolicyValid && isSecurityTypeValid;
}

function deviationChange(frm) {
  let deviation = 0.0;
  // eslint-disable-next-line guard-for-in
  for (const row in frm.doc.items) {
    if (
      frm.doc.items[row].rate === 0 &&
      frm.doc.items[row].cnp_type_of_fee === 'Initial Fee'
    ) {
      frm.doc.cnp_initial_fee_zero = -1;
      frm.refresh_field('cnp_initial_fee_zero');
    }
    if (deviation > frm.doc.items[row].cnp_deviation) {
      deviation = frm.doc.items[row].cnp_deviation;
    }
  }
  frm.doc.cnp_deviation_change = deviation;
  frm.refresh_field('cnp_deviation_change');
}

function setGstState(frm) {
  if (!!frm.doc.customer_address && frm.doc.docstatus === 0) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: frm.doc.customer_address, doc_type: 'Address' },
      freeze: true,
      callback: r => {
        frm.doc.cnp_gst_state = r.message.gst_state;
        frm.refresh_field('cnp_gst_state');
      },
    });
  }
}

function calProformaInvoiceAmt(frm) {
  const items = frm.doc.items;
  items.forEach(element => {
    const periodFrom = moment(frm.doc.transaction_date);
    let periodTo;
    if (element.cnp_type_of_fee === 'Annual Fee') {
      let diff;

      if (!element.cnp_frequency) {
        frappe.throw('Frequency is mandatory field.');
      }

      if (element.cnp_frequency === 'Y') {
        const march = moment(new Date(moment().year(), 3, 0));

        periodTo = periodFrom.isAfter(march) ? march.add(1, 'years') : march;
        diff = getMonthDifference(periodFrom, periodTo);

        element.cnp_proforma_amt = (element.rate / 12) * diff;
      } else if (element.cnp_frequency === 'H') {
        const march = moment(new Date(moment().year(), 3, 0));
        const september = moment(new Date(moment().year(), 9, 0));

        periodTo = periodFrom.isAfter(march)
          ? periodFrom.isAfter(september)
            ? march.add(1, 'years')
            : september
          : march;
        diff = getMonthDifference(periodFrom, periodTo);

        element.cnp_proforma_amt = (element.rate / 12) * diff;
      } else if (element.cnp_frequency === 'Q') {
        const march = moment(new Date(moment().year(), 3, 0));
        const june = moment(new Date(moment().year(), 6, 0));
        const september = moment(new Date(moment().year(), 9, 0));
        const december = moment(new Date(moment().year(), 12, 0));

        periodTo = periodFrom.isAfter(march)
          ? periodFrom.isAfter(june)
            ? periodFrom.isAfter(september)
              ? december
              : september
            : june
          : march;
        diff = getMonthDifference(periodFrom, periodTo);

        element.cnp_proforma_amt = (element.rate / 12) * diff;
      } else if (element.cnp_frequency === 'M') {
        element.cnp_proforma_amt = element.rate / 12;
        diff = 1;
      }

      if (diff === 1) {
        element.cnp_proforma_period_from = periodFrom.format('MMM YYYY');
        element.cnp_proforma_period_to = '';
      } else {
        element.cnp_proforma_period_from = periodFrom.format('MMM YYYY');
        element.cnp_proforma_period_to = periodTo.format('MMM YYYY');
      }
    } else {
      element.cnp_proforma_period_from = '';
      element.cnp_proforma_period_to = '';
    }
  });
}

function getMonthDifference(startDate, endDate) {
  return (
    endDate.month() -
    startDate.month() +
    12 * (endDate.year() - startDate.year()) +
    1
  );
}

function quotationReloadOnPolicyUpdate(frm) {
  if (localStorage.getItem('quotation_reload') === '1') {
    frm.reload_doc().then(() => {
      localStorage.setItem('quotation_reload', '0');
      frm.doc.items.forEach(item => {
        setFees(frm, item);
      });
      frm.refresh_field('items');
      frm.dirty();
      frm.save();
    });
  }
}

function setQuotationState(frm) {
  if (frm.doc.cnp_auto_calculate === 1 || frm.doc.cnp_deviation_change) {
    frm.doc.cnp_quotation_state = 1;
  } else {
    frm.doc.cnp_quotation_state = 0;
  }
}

function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Quotation',
      action: 'Pending for Approval',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function submitQuotation(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.submitDoc',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Quotation',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function approvalAction(frm) {
  if (frm.doc.workflow_state === 'Draft') {
    frm.page.add_action_item('Send For Approval', function () {
      sendApprovalMail(frm);
    });
  }
}

function approval(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_single_doc',
    args: {
      doc_type: 'Approval Settings',
    },
    freeze: true,
    callback: r => {
      const doc = r.message;
      if (!doc) {
        if (
          frm.doc.docstatus === 0 &&
          frm.doc.workflow_state !== 'Pending for Approval' &&
          frm.doc.cnp_send_for_approval !== 1
        ) {
          frm.page.add_action_item('Submit', function () {
            submitQuotation(frm);
          });
        }
        return;
      }

      const row = doc?.deviation_conditions.find(row =>
        isApprovalDeviationRulesValid(row, frm.doc.cnp_deviation_change),
      );

      if (row) {
        const isBcgrm = row.role === 'BCG RM' && frappe.user.has_role(row.role);
        if (
          frm.doc.docstatus === 0 &&
          frm.doc.workflow_state !== 'Pending for Approval' &&
          frm.doc.cnp_send_for_approval !== 1 &&
          isBcgrm
        ) {
          frm.page.add_action_item('Submit', function () {
            submitQuotation(frm);
          });
        } else if (
          frm.doc.docstatus === 0 &&
          frappe.user.has_role(row.approval) &&
          frm.doc.workflow_state !== 'Pending for Approval'
        ) {
          approvalAction(frm);
        } else if (frappe.user.has_role(row.role)) {
          emailAction(frm);
        }
      }
    },
  });

  if (
    frm.doc.cnp_send_for_approval === 1 &&
    (frappe.user.has_role('BCG RM') ||
      frappe.user.has_role('Management Trainee'))
  ) {
    approvalAction(frm);
  }

  if (
    frm.doc.cnp_send_for_approval === 1 &&
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('BCG Head')
  ) {
    emailAction(frm);
  }

  if (
    (frm.doc.workflow_state === 'Submitted' ||
      frm.doc.workflow_state === 'Approved') &&
    frm.doc.cnp_offer_acceptence_date &&
    frm.doc.cnp_s3_key
  ) {
    frm.page.add_action_item('Accepted By Client', function () {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.email_action',
        args: {
          doctype: 'Quotation',
          docname: frm.doc.name,
          action: 'Accepted By Client',
          redirect: false,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc();
          notificationAcceptanceDate(frm);
        },
      });
    });

    frm.page.add_action_item('Renegotiate', function () {
      renegotiateDialog(frm);
    });
  }
}

function isApprovalDeviationRulesValid(row, deviation) {
  return (
    (row.condition === 'Less than' && deviation < row.deviation) ||
    (row.condition === 'Range' &&
      row.from_dev <= deviation &&
      row.to_dev >= deviation) ||
    (row.condition === 'Greater than' && deviation > row.deviation) ||
    (row.condition === 'Equal to' && deviation === row.deviation) ||
    (row.condition === 'Not Equal to' && deviation !== row.deviation)
  );
}

function emailAction(frm) {
  if (frm.doc.workflow_state === 'Pending for Approval') {
    frm.page.add_action_item('Approve', function () {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.email_action',
        args: {
          doctype: 'Quotation',
          docname: frm.doc.name,
          action: 'Approved',
          redirect: false,
          email_token: frm.doc.cnp_email_token,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc();
        },
      });
    });
    frm.page.add_action_item('Reject', function () {
      commentDialog(frm, false);
    });
  }
}

function addSalesInvoiceButton(frm) {
  if (
    frm.doc.docstatus === 1 &&
    frm.doc.workflow_state === 'Accepted By Client'
  ) {
    frm.add_custom_button(
      'Sales Invoice',
      () => {
        frappe.model.open_mapped_doc({
          method: 'trusteeship_platform.custom_methods.make_sales_invoice',
          frm,
        });
      },
      'Create',
    );
  }
}

function hideItemPriceMessages(frm) {
  const deskAlert = document.querySelectorAll('.desk-alert');
  if (deskAlert.length > 0) {
    deskAlert.forEach(element => {
      if (element.innerText.includes('Item Price')) {
        element.hidden = true;
      }
    });
  }

  const alertMessage = document.querySelectorAll('.alert-message');
  if (alertMessage.length > 0) {
    alertMessage.forEach(element => {
      if (element.innerText.includes('editable due to a Workflow')) {
        element.hidden = true;
      }
    });
  }
}

function validateTypeOfFee(frm) {
  const typeOfFee = [];
  let initialFee = 0;
  let oneTimeFee = 0;
  let annualFee = 0;
  frm.doc.items.forEach(item => {
    typeOfFee.push(item.cnp_type_of_fee);
    initialFee =
      item.cnp_type_of_fee === 'Initial Fee' ? initialFee + 1 : initialFee;
    annualFee =
      item.cnp_type_of_fee === 'Annual Fee' ? annualFee + 1 : annualFee;
    oneTimeFee =
      item.cnp_type_of_fee === 'One Time Fee' ? oneTimeFee + 1 : oneTimeFee;
  });
  if (
    (typeOfFee.includes('Initial Fee') || typeOfFee.includes('Annual Fee')) &&
    typeOfFee.includes('One Time Fee')
  ) {
    frappe.throw('One Time Fee cannot be selected with other type of fees...');
  }
  if (initialFee > 1) {
    frappe.throw('Multiple initial fees not allowed');
  }
  if (annualFee > 1) {
    frappe.throw('Multiple annual fees not allowed');
  }
  if (oneTimeFee > 1) {
    frappe.throw('Multiple one time fees not allowed');
  }
}

function validateAcceptanceDate(frm) {
  const creationDate = new Date(frm.doc.creation);
  let dd = creationDate.getDate();
  let mm = creationDate.getMonth() + 1;
  const yyyy = creationDate.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  const formatedCreationDate = yyyy + '-' + mm + '-' + dd;

  if (
    formatedCreationDate > frm.doc.cnp_offer_acceptence_date &&
    formatedCreationDate !== frm.doc.cnp_offer_acceptence_date
  ) {
    frm.doc.cnp_offer_acceptence_date = null;
    frm.refresh_field('cnp_offer_acceptence_date');
    frappe.throw(
      'Offer Letter Acceptance date should be after quotation creation date',
    );
  }
}

function addOperationsFlowBtnInConnections(frm) {
  $("button[data-doctype='ATSL Mandate List']").unbind();
  $("button[data-doctype='ATSL Mandate List']").on('click', function () {
    frappe.model.open_mapped_doc({
      method:
        'trusteeship_platform.custom_methods.make_operations_flow_for_quotation',
      frm,
    });
  });
}

function hideAddOperationsFlowBtn(frm) {
  if (frm.doc.workflow_state !== 'Accepted By Client') {
    $("button[data-doctype='ATSL Mandate List']").hide();
  } else {
    $("button[data-doctype='ATSL Mandate List']").show();
  }
}

function hideAddSalesInvoiceBtn(frm) {
  if (
    frm.doc.workflow_state !== 'Accepted By Client' ||
    frm.doc.workflow_state === 'Renegotiate'
  ) {
    $("button[data-doctype='Sales Invoice']").hide();
  } else {
    $("button[data-doctype='Sales Invoice']").show();
  }
}

function offerLetterUpload(frm) {
  $(frm.fields_dict.cnp_file_upload_html.wrapper).empty();
  frm.refresh_field('cnp_file_upload_html');
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Submitted' ||
    frm.doc.workflow_state === 'Accepted By Client'
  ) {
    setTimeout(() => {
      const file = ' ';
      renderAttachmentTemplate(frm, file);
    }, 1500);
  }
}

function setIntro(frm) {
  if (
    !frm.doc.cnp_s3_key &&
    frm.doc.docstatus === 1 &&
    !frappe.user.has_role('BCG Head') &&
    frm.doc.workflow_state !== 'Rejected'
  ) {
    frm.set_intro('');
    frm.set_intro('Please Upload Attachment for Offer letter');
  }

  if (
    frm.doc.cnp_s3_key &&
    (frm.doc.workflow_state === 'Submitted' ||
      frm.doc.workflow_state === 'Approved')
  ) {
    frm.set_intro('');
    frm.set_intro('Select an Action in the Action button to proceed further');
  }
}

function hideCancelButton(frm) {
  if (frm.doc.workflow_state === 'Renegotiate') {
    $("button[data-label='Cancel']").hide();
  }
}

function cancelDoc(frm) {
  return frappe.call({
    method: 'frappe.desk.form.save.cancel',
    args: {
      doctype: frm.doctype,
      name: frm.doc.name,
    },
    freeze: true,
  });
}

function hideCnpOfferAcceptenceDate(frm) {
  if (
    frm.doc.workflow_state === 'Draft' ||
    frm.doc.workflow_state === 'Accepted By Client'
  ) {
    frm.set_df_property('cnp_offer_acceptence_date', 'read_only', true);
  } else {
    frm.set_df_property('cnp_offer_acceptence_date', 'read_only', false);
  }

  if (
    frm.doc.workflow_state === 'Submitted' ||
    frm.doc.workflow_state === 'Approved'
  ) {
    frm.set_df_property('cnp_offer_acceptence_date', 'reqd', true);
  } else {
    frm.set_df_property('cnp_offer_acceptence_date', 'reqd', false);
  }
  if (frm.doc.workflow_state === 'Renegotiate')
    frm.set_df_property('cnp_offer_acceptence_date', 'hidden', true);
  else frm.set_df_property('cnp_offer_acceptence_date', 'hidden', false);
}

function hideConnections(frm) {
  if (frappe.user.has_role('BCG RM')) {
    $("button[data-doctype='Sales Invoice']").hide();
    $("button[data-doctype='Operations Flow']").hide();
  }
}

function mandatoryNextFeeDateAndFrequency(frm) {
  frm.doc.items.forEach(item => {
    if (item.cnp_type_of_fee === 'Annual Fee' && !item.cnp_frequency) {
      frappe.throw('Frequency is mandatory field for Annual Fee.');
    }
    if (item.cnp_type_of_fee === 'Annual Fee' && !item.cnp_next_fee_date) {
      frappe.throw('Next Fee Date is mandatory field for Annual Fee.');
    }
  });
}

function validateNextFeeDate(frm, row) {
  if (
    frappe.datetime.nowdate() > row.cnp_next_fee_date &&
    frappe.datetime.nowdate() !== row.cnp_next_fee_date
  ) {
    row.cnp_next_fee_date = null;
    frappe.throw('Next Fee date should be after ' + frappe.datetime.nowdate());
  }
}

function addRejectionComment(frm) {
  if (frm.doc.cnp_is_reject_reason_commented === 1) commentDialog(frm);
}

function commentDialog(frm, isStatic = true) {
  const d = new frappe.ui.Dialog({
    title: 'Reason to Reject',
    static: isStatic,
    fields: [
      {
        fieldname: 'reason_to_reject',
        fieldtype: 'Text',
        label: 'Comment',
        reqd: 1,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      const reasonRoReject =
        '<b>Reason to reject:</b> ' + data.reason_to_reject;

      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_rejection_comment',
        args: {
          doctype: 'Quotation',
          docname: frm.doc.name,
          comment: reasonRoReject,
          email_token: frm.doc.cnp_email_token,
        },
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.reload_doc();
          }
        },
      });
    },
  });
  d.show();
}

function emptyFieldIfNoTypeOfFee(frm) {
  if (frm.doc.__islocal) {
    frm.doc.items.forEach(item => {
      if (item.cnp_type_of_fee === '') {
        item.rate = 0;
        item.cnp_selected_calculated_fee = 0;
      }
    });
    frm.refresh_field('items');
  }
}

function addDefaultRowValues(frm, row) {
  row.item_code = frm.doc.items[0].item_code;
  row.item_name = frm.doc.items[0].item_name;

  row.cnp_actual_annual_fee = frm.doc.items[0].cnp_actual_annual_fee;
  row.cnp_actual_initial_fee = frm.doc.items[0].cnp_actual_initial_fee;
  row.cnp_one_time_fee_actual = frm.doc.items[0].cnp_one_time_fee_actual;
  // mandatory fields
  row.uom = frm.doc.items[0].uom;
  row.description = frm.doc.items[0].description;
  row.qty = 1;

  return row;
}

function addNextFeeDate(frm) {
  frm.doc.items.forEach(item => {
    if (item.cnp_type_of_fee === 'Annual Fee') {
      frappe.call({
        method:
          'trusteeship_platform.overrides.quotation.calculate_next_fee_date',
        args: {
          type_of_fee: 'Annual Fee',
          frequency: item.cnp_frequency,
        },
        freeze: true,
        callback: r => {
          item.cnp_next_fee_date = r.message;
          frm.refresh_field('items');
        },
      });
    }
  });
}

function changeBreadcrumbs() {
  if (frappe.user.has_role('BCG Head') || frappe.user.has_role('BCG RM')) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('CRM');
    });
  }
}

function loadAddressOnAddressChange(frm) {
  const prevRoute = frappe.get_prev_route();
  if (
    prevRoute[1] === 'Address' &&
    localStorage.getItem('address_update') === '0'
  ) {
    localStorage.setItem('address_update', '1');
    erpnext.utils.get_address_display(
      frm,
      'customer_address',
      'address_display',
      false,
      '',
    );
    frm.save();
  }
}

function updateCreateDropdownBtn(frm) {
  frm.remove_custom_button('Sales Order', 'Create');
  frm.remove_custom_button('Subscription', 'Create');

  addOperationsFlowBtn(frm);
  addSalesInvoiceButton(frm);
  frm.page.set_inner_btn_group_as_primary('Create');

  if (
    (frm.doc.workflow_state !== 'Accepted By Client' &&
      frm.doc.workflow_state !== 'Renegotiate') ||
    frm.doc.workflow_state === 'Rejected' ||
    (frappe.session.user !== 'Administrator' &&
      (frappe.user.has_role('BCG RM') ||
        frappe.user.has_role('Management Trainee')))
  ) {
    frm.page.get_or_add_inner_group_button('Create').hide();
  }
}

function changeLabelOfferDate(frm) {
  if (frm.doc.workflow_state === 'Accepted By Client') {
    frm.set_df_property(
      'cnp_offer_acceptence_date',
      'label',
      'Offer Acceptance Date',
    );
  }
}
frappe.ui.form.on('Payment Schedule', {
  payment_term: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    paymentScheduleDueDate(frm, row);
  },
  due_date: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    paymentScheduleDueDate(frm, row);
  },
});

function paymentScheduleDueDate(frm, row) {
  const today = frappe.datetime.now_date();
  if (
    ['100% Advance', 'Annual Fee', 'One Time Fee'].includes(row.payment_term)
  ) {
    row.due_date = frappe.datetime.add_days(today, 30);
  } else if (row.payment_term === 'Initial Fee') {
    row.due_date = frappe.datetime.add_days(today, 15);
  }
  frm.refresh_field('payment_schedule');
}

function addPaymentTermDetails(frm, row) {
  const existingPaymentTerm = frm.doc.payment_schedule.find(
    item => item.payment_term === row.cnp_type_of_fee,
  );
  if (existingPaymentTerm) {
    return;
  }
  frappe.call({
    method: 'erpnext.controllers.accounts_controller.get_payment_term_details',
    args: {
      term: row.cnp_type_of_fee,
      posting_date: frm.doc.posting_date,
      grand_total: frm.doc.grand_total,
      base_grand_total: frm.doc.base_grand_total,
    },
    freeze: true,
    callback: r => {
      const child = frm.add_child('payment_schedule');
      const today = frappe.datetime.now_date();
      if (
        ['100% Advance', 'Annual Fee', 'One Time Fee'].includes(
          row.cnp_type_of_fee,
        )
      ) {
        child.due_date = frappe.datetime.add_days(today, 30);
      } else if (row.cnp_type_of_fee === 'Initial Fee') {
        child.due_date = frappe.datetime.add_days(today, 15);
      }
      child.payment_term = row.cnp_type_of_fee;
      child.invoice_portion = r.message.invoice_portion;
      child.description = r.message.description;
      child.discount_type = r.message.discount_type;
      child.base_payment_amount = r.message.base_payment_amount;
      child.discount = r.message.discount;
      child.outstanding = r.message.outstanding;
      child.payment_amount = r.message.payment_amount;
      frm.refresh_field('payment_schedule');
    },
  });
}

function removePaymentTermDetails(frm) {
  const existingPaymentTermIndex_initial = frm.doc.payment_schedule.findIndex(
    item => item.payment_term === 'Initial Fee',
  );
  const existingPaymentTermIndex_annual = frm.doc.payment_schedule.findIndex(
    item => item.payment_term === 'Annual Fee',
  );
  const existingPaymentTermIndex_one_time_fee =
    frm.doc.payment_schedule.findIndex(
      item => item.payment_term === 'One Time Fee',
    );
  removePaymentTerm(frm, existingPaymentTermIndex_initial);
  removePaymentTerm(frm, existingPaymentTermIndex_annual);
  removePaymentTerm(frm, existingPaymentTermIndex_one_time_fee);
}

function removePaymentTerm(frm, existingPaymentTermIndex) {
  if (existingPaymentTermIndex !== -1) {
    frm.doc.payment_schedule.splice(existingPaymentTermIndex, 1);
    frm.refresh_field('payment_schedule');
  }
}

function addReSubmitButton(frm) {
  if (frm.doc.docstatus === 1 && frm.doc.workflow_state === 'Rejected') {
    frm.add_custom_button('Resubmit', () => {
      const default_values = { cnp_send_for_approval: 0 };
      frappe.call({
        method: 'trusteeship_platform.custom_methods.cancel_and_amend_doc',
        args: {
          doctype: frm.doctype,
          docname: frm.doc.name,
          default_values,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(x => {
            frappe.set_route('Form', 'Quotation', r.message);
          });
        },
      });
    });
  }
}

function setCostCenterFilter(frm) {
  frm.set_query('cnp_cost_center', filter => {
    return {
      filters: [
        ['Cost Center', 'is_group', '=', 0],
        ['Cost Center', 'name', 'not like', '%main%'],
        ['Cost Center', 'company', '=', frm.doc.company],
      ],
    };
  });
}

function setDefaultRegisteredAddress(frm) {
  if (frm.doc.__islocal === 1) {
    const filters = [
      ['address_type', '=', 'Registered Office'],
      ['cnp_is_default_offer_letter_address', '=', 1],
      ['Dynamic Link', 'link_doctype', '=', 'Company'],
      ['Dynamic Link', 'link_name', '=', frm.doc.company],
    ];
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: {
        doctype: 'Address',
        filters,
        fields: 'name',
      },
      freeze: true,
      callback: r => {
        if (r.message?.length) {
          const [{ name: address }] = r.message;
          frm.set_value('cnp_registered_address', address);
        } else {
          frm.set_value('cnp_registered_address', '');
        }
      },
    });
  }
}

function setNamingSeries(frm) {
  frm.set_df_property(
    'naming_series',
    'options',
    'AT-CO-.YY.-Quo-.\nSAL-QTN-.YYYY.-',
  );
  frm.doc.naming_series = 'AT-CO-.YY.-Quo-.';
  frm.refresh_field('naming_series');
}
