from erpnext.accounts.doctype.payment_entry.payment_entry import PaymentEntry

from trusteeship_platform.overrides.sales_invoice import email_action


class Payment(PaymentEntry):
    def on_submit(self):
        super().on_submit()
        if self.cnp_action == 1:
            email_action(
                self.doctype,
                self.name,
                "1",
                "payment-entry",
                redirect=False,
                email_token=self.cnp_email_token,
            )
