import frappe
from erpnext.accounts.doctype.account.account import Account

from trusteeship_platform.overrides.sales_invoice import send_approval_mail


class Account(Account):
    def after_insert(self):
        if self.cnp_importer_flag:
            send_approval_mail(self.name, self.doctype, "account")

    def on_update(self):
        if self.cnp_disable_flag:
            frappe.db.set_value(
                self.doctype,
                self.name,
                "workflow_state",
                "Pending for Approval",  # noqa: 501
            )
            frappe.db.set_value(
                self.doctype,
                self.name,
                "cnp_disable_flag",
                0,  # noqa: 501
            )
            frappe.db.commit()
            if self.disabled:
                send_approval_mail(
                    self.name, self.doctype, "account", doc_disable=True
                )  # noqa: 501
            else:
                send_approval_mail(
                    self.name, self.doctype, "account", doc_disable=False
                )

    def before_save(self):
        if not self.is_new():
            doc = frappe.get_doc(self.doctype, self.name)
            if doc.disabled != self.disabled:
                self.cnp_disable_flag = 1


@frappe.whitelist()
def email_action(
    docname,
    action,
):  # noqa: 501
    if action == "1":
        status = "Approved"
    frappe.db.set_value("Account", docname, "workflow_state", status)
    frappe.db.set_value("Account", docname, "cnp_importer_flag", 1)
    frappe.db.commit()
