import functools

import frappe
from erpnext.accounts.doctype.cost_center.cost_center import CostCenter
from frappe.contacts.doctype.address.address import get_address_display


class costCenter(CostCenter):
    def onload(self):
        self.load_address()

    def load_address(self):
        """Loads address list and contact list in `__onload`"""

        filters = [
            ["Address", "cnp_cost_center", "=", self.name],
        ]
        address_list = frappe.get_list(
            "Address", filters=filters, fields=["*"], order_by="creation asc"
        )

        address_list = [
            a.update({"display": get_address_display(a)}) for a in address_list
        ]

        address_list = sorted(
            address_list,
            key=functools.cmp_to_key(
                lambda a, b: (int(a.is_primary_address - b.is_primary_address))
                or (1 if a.modified - b.modified else 0)
            ),
            reverse=True,
        )

        self.set_onload("addr_list", address_list)
