import frappe
from erpnext.selling.doctype.industry_type.industry_type import IndustryType


class IndustryType(IndustryType):
    def validate(self):
        self.remove_not_exist_lead()

    def remove_not_exist_lead(self):
        for links in self.cnp_links:
            if links.link_doctype == "Lead":
                link_name = links.link_name
                if not frappe.db.exists("Lead", link_name):
                    self.cnp_links.remove(links)
