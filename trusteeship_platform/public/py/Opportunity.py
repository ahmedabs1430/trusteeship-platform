import csv
import json
import os

import frappe
from erpnext.crm.doctype.opportunity.opportunity import Opportunity

from trusteeship_platform.custom_methods import cal_opportunity_product_fee


class Opportunity(Opportunity):
    def validate(self):
        super().validate()
        validate_product(self)

    def on_update(self):
        if self.custom_type_of_product == "Secured":
            check = frappe.db.exists(
                "Canopi Pricing Policy",
                {"opportunity": self.name, "no_of_securities": 0},
            )  # noqa: ignore=E501
            if check:
                frappe.throw(
                    """The number of securities should
                        not be set to 0.
                        To update this value,
                      please scroll down and click on the
                        Pricing Policy Parameter section."""
                )  # noqa: ignore=E501

        update_pricing_policy(self)
        if self.cnp_product and self.party_name:
            cal_opportunity_product_fee(self.name, self.cnp_product, self)

    def before_insert(self):
        if self.opportunity_from == "Customer":
            quotation_list = frappe.get_list(
                "Quotation",
                filters=[
                    ["cnp_customer_code", "=", self.party_name],
                    ["workflow_state", "=", "Accepted By Client"],
                ],
            )  # noqa: 501
            for quot in quotation_list:
                doc = frappe.get_doc("Quotation", quot.name)
                opp_doc = frappe.get_doc("Opportunity", doc.cnp_opportunity)
                mandate_name = ""
                initial_fee = 0
                annual_fee = 0
                if doc.cnp_cl_code:
                    mandate_name = doc.cnp_cl_code
                for item in doc.items:
                    if item.cnp_type_of_fee == "Initial Fee":
                        initial_fee = item.rate
                    if item.cnp_type_of_fee == "Annual Fee":
                        annual_fee = item.rate
                self.append(
                    "cnp_extg_relation",
                    {
                        "mandate": mandate_name,
                        "quotation": doc.name,
                        "size_rsincr": opp_doc.cnp_issue_size,
                        "initial_fees_rsincr": initial_fee,
                        "annual_fee_rsincr": annual_fee,
                    },
                )

    def before_save(self):
        quotation_list = frappe.get_list(
            "Quotation",
            filters=[["cnp_opportunity", "=", self.name]],  # noqa: 501
        )
        if len(quotation_list) == 0:
            self.status = "Open"

    def on_trash(self):
        quotation_list = frappe.get_list(
            "Quotation",
            fields=('["*"]'),
            filters=[["cnp_opportunity", "=", self.name]],  # noqa: 501
        )
        if quotation_list:
            frappe.throw(
                (
                    "Cannot delete {} {} because Quotation has been created for this Opportunity."  # noqa: 501
                ).format(self.doctype, self.name),
                frappe.LinkExistsError,
            )
        else:
            if frappe.db.exists(
                {
                    "doctype": "Canopi Pricing Policy",
                    "name": self.name + "-" + self.cnp_product,
                }
            ):
                frappe.delete_doc(
                    "Canopi Pricing Policy",
                    self.name + "-" + self.cnp_product,  # noqa: 501
                )


def validate_product(self):
    if frappe.db.get_value("Item", self.cnp_product, "is_purchase_item") == 1:
        frappe.throw("Selected product allow purchase shold be unchecked")


def update_pricing_policy(opportunity):
    quotation_list = frappe.get_all(
        "Quotation", filters=[["cnp_opportunity", "=", opportunity.name]]
    )

    fund_size_in_rs_cr = frappe.db.get_value(
        "Canopi Pricing Policy",
        opportunity.name + "-" + opportunity.cnp_product,
        "fund_size_in_rs_cr",
    )

    if opportunity.cnp_facility_amt != fund_size_in_rs_cr:
        if quotation_list:
            opportunity.cnp_facility_amt = fund_size_in_rs_cr
        else:
            frappe.db.set_value(
                "Canopi Pricing Policy",
                opportunity.name + "-" + opportunity.cnp_product,
                "fund_size_in_rs_cr",
                opportunity.cnp_facility_amt,
            )


@frappe.whitelist()
def saveDoc(data, policyDocName, doctype):
    data2 = json.loads(data)
    policyDoc = frappe.get_doc(doctype, policyDocName)
    if data2:
        for key, value in data2.items():
            if key == "value_added_services":
                policyDoc.value_added_services = value
            if key == "no_of_sellers":
                policyDoc.no_of_sellers = value
            if key == "no_of_securities":
                policyDoc.no_of_securities = value
            if key == "immovable_properties":
                policyDoc.immovable_properties = value
            if key == "states_immovable_properties":
                policyDoc.states_immovable_properties = value
            if key == "no_of_lenders":
                policyDoc.no_of_lenders = value
            if key == "assets":
                policyDoc.assets = value
            if key == "whether_public_issue":
                policyDoc.whether_public_issue = value
            if key == "listed_yes_no":
                policyDoc.listed_yes_no = value
            if key == "fund_category":
                policyDoc.fund_category = value
            if key == "da_yes_no":
                policyDoc.da_yes_no = value
            if key == "payment_frequency":
                policyDoc.payment_frequency = value
            if key == "fund_size_in_rs_cr":
                policyDoc.fund_size_in_rs_cr = value
            policyDoc.is_saved_by_user = 1
        policyDoc.save()


@frappe.whitelist()
def downloadCsvFininfo(docname):
    data = frappe.db.sql(
        """select net_revenue,operating_profit,profit_from_discontinuing,debt_equity,debt_securities,loans from `tabCanopi Opportunity Fininfo` where parent = %s""",  # noqa: 501
        docname,
    )
    heading = [
        "Net Revenue",
        "Operating Profit ( EBITDA )",
        "Profit from Discontinuing Operation After Tax",
        "Debt / Equity",
        "Debt Securities",
        "Loans",
    ]
    fininfo = []
    fininfo.append(heading)
    for d in data:
        fininfo.append(list(d))

    # File name
    filename = docname + " Fininfo.csv"

    # Open file for writing
    with open(filename, "w", newline="") as csvfile:
        # Create CSV writer object
        csvwriter = csv.writer(csvfile)

        # Write data to CSV file
        for row in fininfo:
            csvwriter.writerow(row)

    # Serve file as download
    with open(filename) as file:
        csv_content = file.read()
    os.remove(filename)

    frappe.local.response.filename = filename
    frappe.local.response.filecontent = csv_content
    frappe.local.response.type = "download"


@frappe.whitelist()
def downloadCsvCrinfo(docname):
    data = frappe.db.sql(
        """select rating_agency,rating_date,current_rating,type_of_loan,currency,amount,status from `tabCanopi Opportunity CR Rating` where parent = %s""",  # noqa: 501
        docname,
    )
    heading = [
        "Rating Agency",
        "Rating Date",
        "Current Rating",
        "Type of loan",
        "Currency",
        "Amount",
        "Status",
    ]
    crinfo = []
    crinfo.append(heading)
    for d in data:
        crinfo.append(list(d))

    # File name
    filename = docname + " CrInfo.csv"

    # Open file for writing
    with open(filename, "w", newline="") as csvfile:
        # Create CSV writer object
        csvwriter = csv.writer(csvfile)

        # Write data to CSV file
        for row in crinfo:
            csvwriter.writerow(row)

    # Serve file as download
    with open(filename) as file:
        csv_content = file.read()
    os.remove(filename)

    frappe.local.response.filename = filename
    frappe.local.response.filecontent = csv_content
    frappe.local.response.type = "download"
