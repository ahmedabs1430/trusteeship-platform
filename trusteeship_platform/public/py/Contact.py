import frappe
from frappe.contacts.doctype.contact.contact import Contact


class Contact(Contact):
    def validate(self):
        name = frappe.db.sql(
            """
                select
                    name
                from
                    `tabOpportunity`
                where
                    contact_person=%s
        """,
            (self.name),
        )
        salutation = self.salutation if self.salutation else ""
        first_name = self.first_name if self.first_name else ""
        middle_name = self.middle_name if self.middle_name else ""
        last_name = self.last_name if self.last_name else ""
        full_name = (
            salutation + " " + first_name + " " + middle_name + " " + last_name
        )  # noqa: 501
        if name:
            for names in name:
                doc = frappe.get_doc("Opportunity", names[0])
                if self.phone_nos:
                    doc.contact_mobile = self.phone_nos[0].phone
                doc.contact_display = full_name
                doc.save()
                frappe.db.commit()
