from erpnext.accounts.doctype.journal_entry.journal_entry import JournalEntry

from trusteeship_platform.overrides.sales_invoice import email_action


class Journal(JournalEntry):
    def on_submit(self):
        super().on_submit()
        if self.cnp_action == 1:
            email_action(
                self.doctype,
                self.name,
                "1",
                "journal-entry",
                redirect=False,
                email_token=self.cnp_email_token,
            )
