from erpnext.accounts.doctype.purchase_invoice.purchase_invoice import (  # noqa: 501 isort: skip
    PurchaseInvoice,
)
import frappe

from trusteeship_platform.custom_methods import get_doc_list
from trusteeship_platform.overrides.sales_invoice import email_action


class Purchase(PurchaseInvoice):
    def on_submit(self):
        super().on_submit()
        if self.cnp_action == 1:
            email_action(
                self.doctype,
                self.name,
                "1",
                "purchase-invoice",
                redirect=False,
                email_token=self.cnp_email_token,
            )

    def validate(self):
        super().validate()
        validate_account(self)


def validate_account(self):
    for item in self.items:
        expense_doc = frappe.get_doc("Account", item.expense_account)
        if expense_doc.disabled == 1:
            frappe.throw(
                "Cannot create accounting entries against disabled account :"
                + expense_doc.name
            )


@frappe.whitelist()
def get_address(cost_center):
    cost_center_list = get_doc_list(
        "Address",
        fields=('["name"]'),
        filters=[["cnp_cost_center", "=", cost_center]],  # noqa: 501
    )
    return cost_center_list
