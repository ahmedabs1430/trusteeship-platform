from erpnext.crm.doctype.lead.lead import Lead


class lead(Lead):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.contact_doc = None

    def before_insert(self):
        self.cnp_creation_date = self.creation
