import frappe
from frappe.model.document import Document

NON_ITEM_PRICE_LIST = [
    "STE",
    "DTE",
    "EA",
    "REIT",
    "NDUA",
    "AIF",
    "CAG",
    "CUSTA",
    "ERA",
    "ESOP",
    "FA",
    "FTE",
    "INVIT",
    "LAG",
    "MAG",
    "MTST",
    "NDUA",
]


class ItemPrice(Document):
    def before_insert(self):
        doc = frappe.get_doc("Item", self.item_code)
        if self.item_code == doc.item_code:
            self.item_name = doc.item_name

    def after_insert(self):
        if self.item_code.upper() in NON_ITEM_PRICE_LIST:
            frappe.delete_doc("Item Price", self.name)
