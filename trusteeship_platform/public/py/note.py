import json
import re
from datetime import datetime

import frappe
from frappe.desk.doctype.note.note import Note


class note(Note):
    def before_save(self):
        note_details(self)

    def on_update(self):
        report_data(self)


def note_details(self):
    if not self.get("__islocal"):
        if self.cnp_note_details != "":
            if not self.cnp_calc_details:
                self.cnp_calc_details = "{}"
            calc_details = json.loads(self.cnp_calc_details)
            note_details = json.loads(self.cnp_note_details)
            note_calc_details = note_calc(self)
            if self.cnp_type == "Note 1 to 5":
                note_details_calc = note1to5(
                    self, note_details, note_calc_details, calc_details
                )
            if self.cnp_type == "Note 6":
                note_details_calc = note6(
                    self, note_details, note_calc_details, calc_details
                )
            if self.cnp_type == "Note 7":
                note_details_calc = note7(
                    self, note_details, note_calc_details, calc_details
                )
            if self.cnp_type == "Note 8 to 10":
                note_details_calc = note8to10(
                    self, note_details, note_calc_details, calc_details
                )
            if self.cnp_type == "Note 11 to 12":
                note_details_calc = note11to12(
                    self, note_details, note_calc_details, calc_details
                )
            if self.cnp_type == "Note 13 to 15":
                note_details_calc = note13to15(
                    self, note_details, note_calc_details, calc_details
                )
            self.cnp_note_details = json.dumps(
                note_details_calc.get("note_details")
            )  # noqa: 501
            self.cnp_calc_details = json.dumps(
                note_details_calc.get("calc_details")
            )  # noqa: 501


def report_data(self):
    if not self.get("__islocal"):
        formatted_report_string = format_company_name(
            self.cnp_fiscal_year, self.cnp_company
        )

        if frappe.db.exists(
            "Canopi Reports", "report_balance_sheet_" + formatted_report_string
        ):
            report_doc = frappe.get_doc(
                "Canopi Reports",
                "report_balance_sheet_" + formatted_report_string,  # noqa: 501
            )
            report_details = json.loads(report_doc.report_details)
            report_doc_calc_details = json.loads(report_doc.calc_details)
            report_doc.report_details = json.dumps(report_details)
            report_doc.calc_details = json.dumps(report_doc_calc_details)
            report_doc.save()
        if frappe.db.exists(
            "Canopi Reports", "report_profit_loss_" + formatted_report_string
        ):
            report_doc = frappe.get_doc(
                "Canopi Reports",
                "report_profit_loss_" + formatted_report_string,  # noqa: 501
            )
            report_details = json.loads(report_doc.report_details)
            report_doc_calc_details = json.loads(report_doc.calc_details)
            report_doc.report_details = json.dumps(report_details)
            report_doc.calc_details = json.dumps(report_doc_calc_details)
            report_doc.save()
        if frappe.db.exists(
            "Canopi Reports", "report_cash_flow_" + formatted_report_string
        ):
            report_doc = frappe.get_doc(
                "Canopi Reports",
                "report_cash_flow_" + formatted_report_string,  # noqa: 501
            )
            report_details = json.loads(report_doc.report_details)
            report_doc_calc_details = json.loads(report_doc.calc_details)
            report_doc.report_details = json.dumps(report_details)
            report_doc.calc_details = json.dumps(report_doc_calc_details)
            report_doc.save()
        frappe.db.commit()


def notes_details(self):
    if frappe.db.exists("Note", "Note 1 to 5"):
        note1to5 = frappe.get_doc("Note", "Note 1 to 5")
        note1to5_calc_details = json.loads(note1to5.cnp_calc_details)
    else:
        note1to5_calc_details = {}
    if frappe.db.exists("Note", "Note 6"):
        note6 = frappe.get_doc("Note", "Note 6")
        note6_calc_details = json.loads(note6.cnp_calc_details)
    else:
        note6_calc_details = {}

    if frappe.db.exists("Note", "Note 7"):
        note7 = frappe.get_doc("Note", "Note 7")
        note7_calc_details = json.loads(note7.cnp_calc_details)
    else:
        note7_calc_details = {}

    if frappe.db.exists("Note", "Note 8 to 10"):
        note8to10 = frappe.get_doc("Note", "Note 8 to 10")
        note8to10_calc_details = json.loads(note8to10.cnp_calc_details)
    else:
        note8to10_calc_details = {}

    return {
        "note1to5_calc_details": note1to5_calc_details,
        "note6_calc_details": note6_calc_details,
        "note7_calc_details": note7_calc_details,
        "note8to10_calc_details": note8to10_calc_details,
    }


def note8to10(self, note_details, note_calc_details, calc_details):
    filter_year = frappe.get_doc("Fiscal Year", self.cnp_fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note_details = json.loads(self.cnp_note_details)
    for note in note_details:
        for i in range(2, 4):
            if note.get("id") == i:
                note_calc_details["note8_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note8_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note8_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note8_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note8_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note8_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 4:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note8_year"
            )  # noqa: 501

        for i in range(7, 9):
            if note.get("id") == i:
                note_calc_details["note8_1_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note8_1_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note8_1_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note8_1_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note8_1_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note8_1_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 9:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_1_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_1_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_1_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_1_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_1_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note8_1_year"
            )
        for i in range(11, 17):
            if note.get("id") == i:
                note_calc_details["note8_2_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note8_2_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note8_2_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note8_2_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note8_2_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note8_2_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 17:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_2_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_2_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_2_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_2_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_2_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note8_2_year"
            )
        if note.get("id") == 18:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_1_quarter1"
            ) + note_calc_details.get("note8_2_quarter1")
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_1_quarter2"
            ) + note_calc_details.get(
                "note8_2_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_1_quarter3"
            ) + note_calc_details.get(
                "note8_2_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note8_1_period1"
            ) + note_calc_details.get("note8_2_period1")
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note8_1_period2"
            ) + note_calc_details.get(
                "note8_2_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note8_1_year"
            ) + note_calc_details.get("note8_2_year")
        for i in range(21, 27):
            if note.get("id") == i:
                note_calc_details["note9_1_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_1_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note9_1_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_1_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_1_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_1_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 27:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_1_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_1_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_1_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_1_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_1_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note9_1_year"
            )
        if note.get("id") == 28:
            note_calc_details["sub1_note9_quarter1"] = note[
                "quarter_ended_sep_30,_" + end_year
            ]
            note_calc_details["sub1_note9_quarter2"] = note[
                "quarter_ended_june_30,_" + end_year
            ]
            note_calc_details["sub1_note9_quarter3"] = note[
                "quarter_ended_sep_30,_" + start_year
            ]
            note_calc_details["sub1_note9_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["sub1_note9_period2"] = note[
                "period_ended_sep_30,_" + start_year
            ]
            note_calc_details["sub1_note9_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]
        if note.get("id") == 29:
            note["quarter_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note9_1_quarter1")
                - note_calc_details["sub1_note9_quarter1"]
            )
            note["quarter_ended_june_30,_" + end_year] = (
                note_calc_details.get("note9_1_quarter2")
                - note_calc_details["sub1_note9_quarter2"]
            )
            note["quarter_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note9_1_quarter3")
                - note_calc_details["sub1_note9_quarter3"]
            )
            note["period_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note9_1_period1")
                - note_calc_details["sub1_note9_period1"]
            )
            note["period_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note9_1_period2")
                - note_calc_details["sub1_note9_period2"]
            )
            note["year_ended_mar_31,_" + end_year] = (
                note_calc_details.get("note9_1_year")
                - note_calc_details["sub1_note9_year"]
            )
        for i in range(30, 34):
            if note.get("id") == i:
                note_calc_details["note9_2_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_2_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note9_2_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_2_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_2_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_2_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 34:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note9_2_year"
            )
        if note.get("id") == 35:
            note_calc_details["sub2_note9_quarter1"] = note[
                "quarter_ended_sep_30,_" + end_year
            ]
            note_calc_details["sub2_note9_quarter2"] = note[
                "quarter_ended_june_30,_" + end_year
            ]
            note_calc_details["sub2_note9_quarter3"] = note[
                "quarter_ended_sep_30,_" + start_year
            ]
            note_calc_details["sub2_note9_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["sub2_note9_period2"] = note[
                "period_ended_sep_30,_" + start_year
            ]
            note_calc_details["sub2_note9_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]
        if note.get("id") == 36:
            note["quarter_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note9_2_quarter1")
                - note_calc_details["sub2_note9_quarter1"]
            )
            note["quarter_ended_june_30,_" + end_year] = (
                note_calc_details.get("note9_2_quarter2")
                - note_calc_details["sub2_note9_quarter2"]
            )
            note["quarter_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note9_2_quarter3")
                - note_calc_details["sub2_note9_quarter3"]
            )
            note["period_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note9_2_period1")
                - note_calc_details["sub2_note9_period1"]
            )
            note["period_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note9_2_period2")
                - note_calc_details["sub2_note9_period2"]
            )
            note["year_ended_mar_31,_" + end_year] = (
                note_calc_details.get("note9_2_year")
                - note_calc_details["sub2_note9_year"]
            )
        if note.get("id") == 37:
            note["quarter_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note9_1_quarter1")
                - note_calc_details["sub1_note9_quarter1"]
            ) + (
                note_calc_details.get("note9_2_quarter1")
                - note_calc_details["sub2_note9_quarter1"]
            )
            note["quarter_ended_june_30,_" + end_year] = (
                note_calc_details.get("note9_1_quarter2")
                - note_calc_details["sub1_note9_quarter2"]
            ) + (
                note_calc_details.get("note9_2_quarter2")
                - note_calc_details["sub2_note9_quarter2"]
            )
            note["quarter_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note9_1_quarter3")
                - note_calc_details["sub1_note9_quarter3"]
            ) + (
                note_calc_details.get("note9_2_quarter3")
                - note_calc_details["sub2_note9_quarter3"]
            )
            note["period_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note9_1_period1")
                - note_calc_details["sub1_note9_period1"]
            ) + (
                note_calc_details.get("note9_2_period1")
                - note_calc_details["sub2_note9_period1"]
            )
            note["period_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note9_1_period2")
                - note_calc_details["sub1_note9_period2"]
            ) + (
                note_calc_details.get("note9_2_period2")
                - note_calc_details["sub2_note9_period2"]
            )
            note["year_ended_mar_31,_" + end_year] = (
                note_calc_details.get("note9_1_year")
                - note_calc_details["sub1_note9_year"]
            ) + (
                note_calc_details.get("note9_2_year")
                - note_calc_details["sub2_note9_year"]
            )
        for i in range(40, 42):
            if note.get("id") == i:
                note_calc_details["note9_2_non_current_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_2_non_current_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note9_2_non_current_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_2_non_current_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_2_non_current_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_2_non_current_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 42:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_non_current_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_non_current_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_non_current_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note9_2_non_current_year"
            )
        if note.get("id") == 47:
            note_calc_details["note9_2_interest_fd_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note9_2_interest_fd_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]
        for i in range(44, 48):
            if note.get("id") == i:
                note_calc_details["note9_2_current_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_2_current_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note9_2_current_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_2_current_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note9_2_current_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note9_2_current_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 48:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_current_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_current_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_current_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_current_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_current_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note9_2_current_year"
            )
        if note.get("id") == 49:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_non_current_quarter1"
            ) + note_calc_details.get("note9_2_current_quarter1")
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter2"
            ) + note_calc_details.get(
                "note9_2_current_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter3"
            ) + note_calc_details.get(
                "note9_2_current_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note9_2_non_current_period1"
            ) + note_calc_details.get("note9_2_current_period1")
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note9_2_non_current_period2"
            ) + note_calc_details.get(
                "note9_2_current_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note9_2_non_current_year"
            ) + note_calc_details.get("note9_2_current_year")
        if note.get("id") == 54:
            note_calc_details["note10_1_quarter1"] = note[
                "quarter_ended_sep_30,_" + end_year
            ]
            note_calc_details["note10_1_quarter2"] = note[
                "quarter_ended_june_30,_" + end_year
            ]
            note_calc_details["note10_1_quarter3"] = note[
                "quarter_ended_sep_30,_" + start_year
            ]
            note_calc_details["note10_1_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note10_1_period2"] = note[
                "period_ended_sep_30,_" + start_year
            ]
            note_calc_details["note10_1_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]  # noqa: 501
        if note.get("id") == 55:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note10_1_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_1_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_1_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note10_1_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_1_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note10_1_year"
            )
        if note.get("id") == 57:
            note_calc_details["note10_2_quarter1"] = note[
                "quarter_ended_sep_30,_" + end_year
            ]
            note_calc_details["note10_2_quarter2"] = note[
                "quarter_ended_june_30,_" + end_year
            ]
            note_calc_details["note10_2_quarter3"] = note[
                "quarter_ended_sep_30,_" + start_year
            ]
            note_calc_details["note10_2_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note10_2_period2"] = note[
                "period_ended_sep_30,_" + start_year
            ]
            note_calc_details["note10_2_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]  # noqa: 501
        if note.get("id") == 58:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note10_2_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_2_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_2_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note10_2_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_2_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note10_2_year"
            )
        if note.get("id") == 59:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note10_1_quarter1"
            ) + note_calc_details.get("note10_2_quarter1")
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_1_quarter2"
            ) + note_calc_details.get(
                "note10_2_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_1_quarter3"
            ) + note_calc_details.get(
                "note10_2_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note10_1_period1"
            ) + note_calc_details.get("note10_2_period1")
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note10_1_period2"
            ) + note_calc_details.get(
                "note10_2_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note10_1_year"
            ) + note_calc_details.get("note10_2_year")
    calc_details = {
        "note8_quarter1": note_calc_details.get("note8_quarter1"),
        "note8_quarter2": note_calc_details.get("note8_quarter2"),
        "note8_quarter3": note_calc_details.get("note8_quarter3"),
        "note8_period1": note_calc_details.get("note8_period1"),
        "note8_period2": note_calc_details.get("note8_period2"),
        "note8_year": note_calc_details.get("note8_year"),
        "note8_1_quarter1": note_calc_details.get("note8_1_quarter1"),
        "note8_1_quarter2": note_calc_details.get("note8_1_quarter2"),
        "note8_1_quarter3": note_calc_details.get("note8_1_quarter3"),
        "note8_1_period1": note_calc_details.get("note8_1_period1"),
        "note8_1_period2": note_calc_details.get("note8_1_period2"),
        "note8_1_year": note_calc_details.get("note8_1_year"),
        "note8_2_quarter1": note_calc_details.get("note8_2_quarter1"),
        "note8_2_quarter2": note_calc_details.get("note8_2_quarter2"),
        "note8_2_quarter3": note_calc_details.get("note8_2_quarter3"),
        "note8_2_period1": note_calc_details.get("note8_2_period1"),
        "note8_2_period2": note_calc_details.get("note8_2_period2"),
        "note8_2_year": note_calc_details.get("note8_2_year"),
        "note9_1_quarter1": note_calc_details.get("note9_1_quarter1"),
        "note9_1_quarter2": note_calc_details.get("note9_1_quarter2"),
        "note9_1_quarter3": note_calc_details.get("note9_1_quarter3"),
        "note9_1_period1": note_calc_details.get("note9_1_period1"),
        "note9_1_period2": note_calc_details.get("note9_1_period2"),
        "note9_1_year": note_calc_details.get("note9_1_year"),
        "note9_2_quarter1": note_calc_details.get("note9_2_quarter1"),
        "note9_2_quarter2": note_calc_details.get("note9_2_quarter2"),
        "note9_2_quarter3": note_calc_details.get("note9_2_quarter3"),
        "note9_2_period1": note_calc_details.get("note9_2_period1"),
        "note9_2_period2": note_calc_details.get("note9_2_period2"),
        "note9_2_year": note_calc_details.get("note9_2_year"),
        "note9_2_non_current_quarter1": note_calc_details.get(
            "note9_2_non_current_quarter1"
        ),
        "note9_2_non_current_quarter2": note_calc_details.get(
            "note9_2_non_current_quarter2"
        ),
        "note9_2_non_current_quarter3": note_calc_details.get(
            "note9_2_non_current_quarter3"
        ),
        "note9_2_non_current_period1": note_calc_details.get(
            "note9_2_non_current_period1"
        ),
        "note9_2_non_current_period2": note_calc_details.get(
            "note9_2_non_current_period2"
        ),
        "note9_2_non_current_year": note_calc_details.get(
            "note9_2_non_current_year"
        ),  # noqa: 501
        "note9_2_current_quarter1": note_calc_details.get(
            "note9_2_current_quarter1"
        ),  # noqa: 501
        "note9_2_current_quarter2": note_calc_details.get(
            "note9_2_current_quarter2"
        ),  # noqa: 501
        "note9_2_current_quarter3": note_calc_details.get(
            "note9_2_current_quarter3"
        ),  # noqa: 501
        "note9_2_current_period1": note_calc_details.get(
            "note9_2_current_period1"
        ),  # noqa: 501
        "note9_2_current_period2": note_calc_details.get(
            "note9_2_current_period2"
        ),  # noqa: 501
        "note9_2_current_year": note_calc_details.get("note9_2_current_year"),
        "sub1_note9_quarter1": note_calc_details.get("sub1_note9_quarter1"),
        "sub1_note9_quarter2": note_calc_details.get("sub1_note9_quarter2"),
        "sub1_note9_quarter3": note_calc_details.get("sub1_note9_quarter3"),
        "sub1_note9_period1": note_calc_details.get("sub1_note9_period1"),
        "sub1_note9_period2": note_calc_details.get("sub1_note9_period2"),
        "sub1_note9_year": note_calc_details.get("sub1_note9_year"),
        "sub2_note9_quarter1": note_calc_details.get("sub2_note9_quarter1"),
        "sub2_note9_quarter2": note_calc_details.get("sub2_note9_quarter2"),
        "sub2_note9_quarter3": note_calc_details.get("sub2_note9_quarter3"),
        "sub2_note9_period1": note_calc_details.get("sub2_note9_period1"),
        "sub2_note9_period2": note_calc_details.get("sub2_note9_period2"),
        "sub2_note9_year": note_calc_details.get("sub2_note9_year"),
        "note10_1_quarter1": note_calc_details.get("note10_1_quarter1"),
        "note10_1_quarter2": note_calc_details.get("note10_1_quarter2"),
        "note10_1_quarter3": note_calc_details.get("note10_1_quarter3"),
        "note10_1_period1": note_calc_details.get("note10_1_period1"),
        "note10_1_period2": note_calc_details.get("note10_1_period2"),
        "note10_1_year": note_calc_details.get("note10_1_year"),
        "note10_2_quarter1": note_calc_details.get("note10_2_quarter1"),
        "note10_2_quarter2": note_calc_details.get("note10_2_quarter2"),
        "note10_2_quarter3": note_calc_details.get("note10_2_quarter3"),
        "note10_2_period1": note_calc_details.get("note10_2_period1"),
        "note10_2_period2": note_calc_details.get("note10_2_period2"),
        "note10_2_year": note_calc_details.get("note10_2_year"),
        "note9_2_interest_fd_period1": note_calc_details.get(
            "note9_2_interest_fd_period1"
        ),
        "note9_2_interest_fd_year": note_calc_details.get(
            "note9_2_interest_fd_year"
        ),  # noqa: 501
    }
    note_calc_details["note8_quarter1"] = 0
    note_calc_details["note8_quarter2"] = 0
    note_calc_details["note8_quarter3"] = 0
    note_calc_details["note8_period1"] = 0
    note_calc_details["note8_period2"] = 0
    note_calc_details["note8_year"] = 0
    note_calc_details["note8_1_quarter1"] = 0
    note_calc_details["note8_1_quarter2"] = 0
    note_calc_details["note8_1_quarter3"] = 0
    note_calc_details["note8_1_period1"] = 0
    note_calc_details["note8_1_period2"] = 0
    note_calc_details["note8_1_year"] = 0
    note_calc_details["note8_2_quarter1"] = 0
    note_calc_details["note8_2_quarter2"] = 0
    note_calc_details["note8_2_quarter3"] = 0
    note_calc_details["note8_2_period1"] = 0
    note_calc_details["note8_2_period2"] = 0
    note_calc_details["note8_2_year"] = 0
    note_calc_details["note9_1_quarter1"] = 0
    note_calc_details["note9_1_quarter2"] = 0
    note_calc_details["note9_1_quarter3"] = 0
    note_calc_details["note9_1_period1"] = 0
    note_calc_details["note9_1_period2"] = 0
    note_calc_details["note9_1_year"] = 0
    note_calc_details["sub1_note9_quarter1"] = 0
    note_calc_details["sub1_note9_quarter2"] = 0
    note_calc_details["sub1_note9_quarter3"] = 0
    note_calc_details["sub1_note9_period1"] = 0
    note_calc_details["sub1_note9_period2"] = 0
    note_calc_details["sub1_note9_year"] = 0
    note_calc_details["note9_2_quarter1"] = 0
    note_calc_details["note9_2_quarter2"] = 0
    note_calc_details["note9_2_quarter3"] = 0
    note_calc_details["note9_2_period1"] = 0
    note_calc_details["note9_2_period2"] = 0
    note_calc_details["note9_2_year"] = 0

    note_calc_details["sub2_note9_quarter1"] = 0
    note_calc_details["sub2_note9_quarter2"] = 0
    note_calc_details["sub2_note9_quarter3"] = 0
    note_calc_details["sub2_note9_period1"] = 0
    note_calc_details["sub2_note9_period2"] = 0
    note_calc_details["sub2_note9_year"] = 0
    note_calc_details["note9_2_non_current_quarter1"] = 0
    note_calc_details["note9_2_non_current_quarter2"] = 0
    note_calc_details["note9_2_non_current_quarter3"] = 0
    note_calc_details["note9_2_non_current_period1"] = 0
    note_calc_details["note9_2_non_current_period2"] = 0
    note_calc_details["note9_2_non_current_year"] = 0
    note_calc_details["note9_2_current_quarter1"] = 0
    note_calc_details["note9_2_current_quarter2"] = 0
    note_calc_details["note9_2_current_quarter3"] = 0
    note_calc_details["note9_2_current_period1"] = 0
    note_calc_details["note9_2_current_period2"] = 0
    note_calc_details["note9_2_current_year"] = 0

    note_calc_details["note10_1_quarter1"] = 0
    note_calc_details["note10_1_quarter2"] = 0
    note_calc_details["note10_1_quarter3"] = 0
    note_calc_details["note10_1_period1"] = 0
    note_calc_details["note10_1_period2"] = 0
    note_calc_details["note10_1_year"] = 0

    note_calc_details["note10_2_quarter1"] = 0
    note_calc_details["note10_2_quarter2"] = 0
    note_calc_details["note10_2_quarter3"] = 0
    note_calc_details["note10_2_period1"] = 0
    note_calc_details["note10_2_period2"] = 0
    note_calc_details["note10_2_year"] = 0
    note_calc_details["note9_2_interest_fd_period1"] = 0
    note_calc_details["note9_2_interest_fd_year"] = 0
    return {"note_details": note_details, "calc_details": calc_details}


def note11to12(self, note_details, note_calc_details, calc_details):
    filter_year = frappe.get_doc("Fiscal Year", self.cnp_fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note_details = json.loads(self.cnp_note_details)
    for note in note_details:
        for i in range(3, 6):
            if note.get("id") == i:
                if note.get("id") == 3:
                    copy1_quater1 = note["quarter_ended_sep_30,_" + end_year]
                    copy1_quarter2 = note[
                        "quarter_ended_june_30,_" + end_year
                    ]  # noqa: 501
                    copy1_quarter3 = note[
                        "quarter_ended_sep_30,_" + start_year
                    ]  # noqa: 501
                    copy1_period1 = note["period_ended_sep_30,_" + end_year]
                    copy1_period2 = note["period_ended_sep_30,_" + start_year]
                    copy1_year = note["year_ended_mar_31,_" + end_year]
                if note.get("id") == 4:
                    copy2_quater1 = note["quarter_ended_sep_30,_" + end_year]
                    copy2_quarter2 = note[
                        "quarter_ended_june_30,_" + end_year
                    ]  # noqa: 501
                    copy2_quarter3 = note[
                        "quarter_ended_sep_30,_" + start_year
                    ]  # noqa: 501
                    copy2_period1 = note["period_ended_sep_30,_" + end_year]
                    copy2_period2 = note["period_ended_sep_30,_" + start_year]
                    copy2_year = note["year_ended_mar_31,_" + end_year]
                if note.get("id") == 5:
                    copy3_quater1 = note["quarter_ended_sep_30,_" + end_year]
                    copy3_quarter2 = note[
                        "quarter_ended_june_30,_" + end_year
                    ]  # noqa: 501
                    copy3_quarter3 = note[
                        "quarter_ended_sep_30,_" + start_year
                    ]  # noqa: 501
                    copy3_period1 = note["period_ended_sep_30,_" + end_year]
                    copy3_period2 = note["period_ended_sep_30,_" + start_year]
                    copy3_year = note["year_ended_mar_31,_" + end_year]

                note_calc_details["note11_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note11_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note11_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note11_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note11_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note11_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 6:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note11_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note11_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note11_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note11_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note11_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note11_year"
            )
        for i in range(8, 14):
            if note.get("id") == i:
                note_calc_details["note12_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note12_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note12_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note12_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note12_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note12_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
            if note.get("id") == 8:
                note_calc_details["note12_period1_cash_flow_interest"] = note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note12_year_cash_flow_interest"] = note[
                    "year_ended_mar_31,_" + end_year
                ]
            if note.get("id") == 9:
                note_calc_details["note12_period1_cash_flow_net_gain"] = note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note12_year_cash_flow_net_gain"] = note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 14:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note12_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note12_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note12_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note12_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note12_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note12_year"
            )

        for i in range(18, 21):
            if note.get("id") == i:
                if note.get("id") == 18:
                    note["quarter_ended_sep_30,_" + end_year] = copy1_quater1
                    note[
                        "quarter_ended_june_30,_" + end_year
                    ] = copy1_quarter2  # noqa: 501
                    note[
                        "quarter_ended_sep_30,_" + start_year
                    ] = copy1_quarter3  # noqa: 501
                    note["period_ended_sep_30,_" + end_year] = copy1_period1
                    note["period_ended_sep_30,_" + start_year] = copy1_period2
                    note["year_ended_mar_31,_" + end_year] = copy1_year
                if note.get("id") == 19:
                    note["quarter_ended_sep_30,_" + end_year] = copy2_quater1
                    note[
                        "quarter_ended_june_30,_" + end_year
                    ] = copy2_quarter2  # noqa: 501
                    note[
                        "quarter_ended_sep_30,_" + start_year
                    ] = copy2_quarter3  # noqa: 501
                    note["period_ended_sep_30,_" + end_year] = copy2_period1
                    note["period_ended_sep_30,_" + start_year] = copy2_period2
                    note["year_ended_mar_31,_" + end_year] = copy2_year
                if note.get("id") == 20:
                    note["quarter_ended_sep_30,_" + end_year] = copy3_quater1
                    note[
                        "quarter_ended_june_30,_" + end_year
                    ] = copy3_quarter2  # noqa: 501
                    note[
                        "quarter_ended_sep_30,_" + start_year
                    ] = copy3_quarter3  # noqa: 501
                    note["period_ended_sep_30,_" + end_year] = copy3_period1
                    note["period_ended_sep_30,_" + start_year] = copy3_period2
                    note["year_ended_mar_31,_" + end_year] = copy3_year
        if note.get("id") == 21:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note11_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note11_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note11_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note11_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note11_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note11_year"
            )
        if note.get("id") == 22:
            note["quarter_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note11_quarter1") * 0.0015
            )
            note["quarter_ended_june_30,_" + end_year] = (
                note_calc_details.get("note11_quarter2") * 0.0015
            )
            note["quarter_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note11_quarter3") * 0.0015
            )
            note["period_ended_sep_30,_" + end_year] = (
                note_calc_details.get("note11_period1") * 0.0015
            )
            note["period_ended_sep_30,_" + start_year] = (
                note_calc_details.get("note11_period2") * 0.0015
            )
            note["year_ended_mar_31,_" + end_year] = (
                note_calc_details.get("note11_year") * 0.0015
            )
            adjusted_income_quarter1 = (
                note_calc_details.get("note11_quarter1") * 0.0015
            )  # noqa: 501
            adjusted_income_quarter2 = (
                note_calc_details.get("note11_quarter2") * 0.0015
            )  # noqa: 501
            adjusted_income_quarter3 = (
                note_calc_details.get("note11_quarter3") * 0.0015
            )  # noqa: 501
            adjusted_income_period1 = (
                note_calc_details.get("note11_period1") * 0.0015
            )  # noqa: 501
            adjusted_income_period2 = (
                note_calc_details.get("note11_period2") * 0.0015
            )  # noqa: 501
            adjusted_income_year = (
                note_calc_details.get("note11_year") * 0.0015
            )  # noqa: 501
        if note.get("id") == 23:
            note["quarter_ended_sep_30,_" + end_year] = (
                adjusted_income_quarter1 * 0.12
            )  # noqa: 501
            note["quarter_ended_june_30,_" + end_year] = (
                adjusted_income_quarter2 * 0.12
            )  # noqa: 501
            note["quarter_ended_sep_30,_" + start_year] = (
                adjusted_income_quarter3 * 0.12
            )
            note["period_ended_sep_30,_" + end_year] = (
                adjusted_income_period1 * 0.12
            )  # noqa: 501
            note["period_ended_sep_30,_" + start_year] = (
                adjusted_income_period2 * 0.12
            )  # noqa: 501
            note["year_ended_mar_31,_" + end_year] = (
                adjusted_income_year * 0.12
            )  # noqa: 501
            total_adjusted_income_gst_quarter1 = (
                adjusted_income_quarter1 * 0.12
                + note_calc_details.get("note11_quarter1") * 0.0015
            )
            total_adjusted_income_gst_quarter2 = (
                adjusted_income_quarter2 * 0.12
                + note_calc_details.get("note11_quarter2") * 0.0015
            )
            total_adjusted_income_gst_quarter3 = (
                adjusted_income_quarter3 * 0.12
                + note_calc_details.get("note11_quarter3") * 0.0015
            )
            total_adjusted_income_gst_period1 = (
                adjusted_income_period1 * 0.12
                + note_calc_details.get("note11_period1") * 0.0015
            )
            total_adjusted_income_gst_period2 = (
                adjusted_income_period2 * 0.12
                + note_calc_details.get("note11_period2") * 0.0015
            )
            total_adjusted_income_gst_year = (
                adjusted_income_year * 0.12
                + note_calc_details.get("note11_year") * 0.0015
            )
        if note.get("id") == 24:
            note[
                "quarter_ended_sep_30,_" + end_year
            ] = total_adjusted_income_gst_quarter1
            note[
                "quarter_ended_june_30,_" + end_year
            ] = total_adjusted_income_gst_quarter2
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = total_adjusted_income_gst_quarter3
            note[
                "period_ended_sep_30,_" + end_year
            ] = total_adjusted_income_gst_period1  # noqa: 501
            note[
                "period_ended_sep_30,_" + start_year
            ] = total_adjusted_income_gst_period2
            note[
                "year_ended_mar_31,_" + end_year
            ] = total_adjusted_income_gst_year  # noqa: 501
    calc_details = {
        "note12_period1_cash_flow_interest": note_calc_details.get(
            "note12_period1_cash_flow_interest"
        ),
        "note12_year_cash_flow_interest": note_calc_details.get(
            "note12_year_cash_flow_interest"
        ),
        "note12_period1_cash_flow_net_gain": note_calc_details.get(
            "note12_period1_cash_flow_net_gain"
        ),
        "note12_year_cash_flow_net_gain": note_calc_details.get(
            "note12_year_cash_flow_net_gain"
        ),
        "note11_quarter1": note_calc_details.get("note11_quarter1"),
        "note11_quarter2": note_calc_details.get("note11_quarter2"),
        "note11_quarter3": note_calc_details.get("note11_quarter3"),
        "note11_period1": note_calc_details.get("note11_period1"),
        "note11_period2": note_calc_details.get("note11_period2"),
        "note11_year": note_calc_details.get("note11_year"),
        "note12_quarter1": note_calc_details.get("note12_quarter1"),
        "note12_quarter2": note_calc_details.get("note12_quarter2"),
        "note12_quarter3": note_calc_details.get("note12_quarter3"),
        "note12_period1": note_calc_details.get("note12_period1"),
        "note12_period2": note_calc_details.get("note12_period2"),
        "note12_year": note_calc_details.get("note12_year"),
    }
    note_calc_details["note12_period1_cash_flow_net_gain"] = 0
    note_calc_details["note12_year_cash_flow_net_gain"] = 0
    note_calc_details["note12_period1_cash_flow_interest"] = 0
    note_calc_details["note12_year_cash_flow_interest"] = 0
    note_calc_details["note11_quarter1"] = 0
    note_calc_details["note11_quarter2"] = 0
    note_calc_details["note11_quarter3"] = 0
    note_calc_details["note11_period2"] = 0
    note_calc_details["note11_year"] = 0
    note_calc_details["note12_quarter1"] = 0
    note_calc_details["note12_quarter2"] = 0
    note_calc_details["note12_quarter3"] = 0
    note_calc_details["note12_period1"] = 0
    note_calc_details["note12_period2"] = 0
    note_calc_details["note12_year"] = 0
    return {"note_details": note_details, "calc_details": calc_details}


def note6(self, note_details, note_calc_details, calc_details):
    note_details = json.loads(self.cnp_note_details)
    for note in note_details:
        for i in range(2, 5):
            if note.get("id") == i:
                note_calc_details["note6_gross_block_computers"] += note[
                    "computers"
                ]  # noqa: 501
                note_calc_details[
                    "note6_gross_block_computer_server"
                ] += note[  # noqa: 501
                    "computer_server"
                ]
                note_calc_details["note6_gross_block_mobile"] += note[
                    "mobile"
                ]  # noqa: 501
                note_calc_details[
                    "note6_gross_block_furniture_and_fixture"
                ] += note[  # noqa: 501
                    "furniture_and_fixtures"
                ]
                note_calc_details[
                    "note6_gross_block_office_equipment"
                ] += note[  # noqa: 501
                    "office_equipment"
                ]
                note_calc_details["note6_gross_block_total"] += note["total"]
        if note.get("id") == 5:
            note["computers"] = note_calc_details[
                "note6_gross_block_computers"
            ]  # noqa: 501
            note["computer_server"] = note_calc_details[
                "note6_gross_block_computer_server"
            ]
            note["mobile"] = note_calc_details["note6_gross_block_mobile"]
            note["furniture_and_fixtures"] = note_calc_details[
                "note6_gross_block_furniture_and_fixture"
            ]
            note["office_equipment"] = note_calc_details[
                "note6_gross_block_office_equipment"
            ]
            note["total"] = note_calc_details["note6_gross_block_total"]
        for i in range(8, 11):
            if note.get("id") == i:
                note_calc_details["note6_depreciation_computers"] += note[
                    "computers"
                ]  # noqa: 501
                note_calc_details[
                    "note6_depreciation_computer_server"
                ] += note[  # noqa: 501
                    "computer_server"
                ]
                note_calc_details["note6_depreciation_mobile"] += note[
                    "mobile"
                ]  # noqa: 501
                note_calc_details[
                    "note6_depreciation_furniture_and_fixture"
                ] += note[  # noqa: 501
                    "furniture_and_fixtures"
                ]
                note_calc_details[
                    "note6_depreciation_office_equipment"
                ] += note[  # noqa: 501
                    "office_equipment"
                ]
                note_calc_details["note6_depreciation_total"] += note["total"]
        if note.get("id") == 11:
            note["computers"] = note_calc_details[
                "note6_depreciation_computers"
            ]  # noqa: 501
            note["computer_server"] = note_calc_details[
                "note6_depreciation_computer_server"
            ]
            note["mobile"] = note_calc_details["note6_depreciation_mobile"]
            note["furniture_and_fixtures"] = note_calc_details[
                "note6_depreciation_furniture_and_fixture"
            ]
            note["office_equipment"] = note_calc_details[
                "note6_depreciation_office_equipment"
            ]
        if note.get("id") == 14:
            note_calc_details["note6_net_block_computers"] = note["computers"]
            note_calc_details["note6_net_block_computer_server"] = note[
                "computer_server"
            ]
            note_calc_details["note6_net_block_mobile"] = note["mobile"]
            note_calc_details["note6_net_block_furniture_and_fixture"] = note[
                "furniture_and_fixtures"
            ]
            note_calc_details["note6_net_block_office_equipment"] = note[
                "office_equipment"
            ]
            note_calc_details["note6_net_block_total"] = note["total"]

        if note.get("total") != " ":
            total = (
                note["computers"]
                + note["computer_server"]
                + note["mobile"]
                + note["furniture_and_fixtures"]
                + note["office_equipment"]
            )
            note["total"] = total
            note_calc_details["note6_net_block_total"] = total
        calc_details = {
            "note6_gross_block_computers": note_calc_details.get(
                "note6_gross_block_computers"
            ),
            "note6_gross_block_computer_server": note_calc_details.get(
                "note6_gross_block_computer_server"
            ),
            "note6_gross_block_mobile": note_calc_details.get(
                "note6_gross_block_mobile"
            ),
            "note6_gross_block_furniture_and_fixture": note_calc_details.get(
                "note6_gross_block_furniture_and_fixture"
            ),
            "note6_gross_block_office_equipment": note_calc_details.get(
                "note6_gross_block_office_equipment"
            ),
            "note6_gross_block_total": note_calc_details.get(
                "note6_gross_block_total"
            ),  # noqa: 501
            "note6_depreciation_computers": note_calc_details.get(
                "note6_depreciation_computers"
            ),
            "note6_depreciation_computer_server": note_calc_details.get(
                "note6_depreciation_computer_server"
            ),
            "note6_depreciation_mobile": note_calc_details.get(
                "note6_depreciation_mobile"
            ),
            "note6_depreciation_furniture_and_fixture": note_calc_details.get(
                "note6_depreciation_furniture_and_fixture"
            ),
            "note6_depreciation_office_equipment": note_calc_details.get(
                "note6_depreciation_office_equipment"
            ),
            "note6_depreciation_total": note_calc_details.get(
                "note6_depreciation_total"
            ),
            "note6_net_block_computers": note_calc_details.get(
                "note6_net_block_computers"
            ),
            "note6_net_block_computer_server": note_calc_details.get(
                "note6_net_block_computer_server"
            ),
            "note6_net_block_mobile": note_calc_details.get(
                "note6_net_block_mobile"
            ),  # noqa: 501
            "note6_net_block_furniture_and_fixture": note_calc_details.get(
                "note6_net_block_furniture_and_fixture"
            ),
            "note6_net_block_office_equipment": note_calc_details.get(
                "note6_net_block_office_equipment"
            ),
            "note6_net_block_total": note_calc_details.get(
                "note6_net_block_total"
            ),  # noqa: 501
        }
    note_calc_details["note6_gross_block_computers"] = 0
    note_calc_details["note6_gross_block_computer_server"] = 0
    note_calc_details["note6_gross_block_mobile"] = 0
    note_calc_details["note6_gross_block_furniture_and_fixture"] = 0
    note_calc_details["note6_gross_block_office_equipment"] = 0
    note_calc_details["note6_gross_block_total"] = 0
    note_calc_details["note6_depreciation_computers"] = 0
    note_calc_details["note6_depreciation_computer_server"] = 0
    note_calc_details["note6_depreciation_mobile"] = 0
    note_calc_details["note6_depreciation_furniture_and_fixture"] = 0
    note_calc_details["note6_depreciation_office_equipment"] = 0
    note_calc_details["note6_depreciation_total"] = 0
    note_calc_details["note6_net_block_computers"] = 0
    note_calc_details["note6_net_block_computer_server"] = 0
    note_calc_details["note6_net_block_mobile"] = 0
    note_calc_details["note6_net_block_furniture_and_fixture"] = 0
    note_calc_details["note6_net_block_office_equipment"] = 0
    note_calc_details["note6_net_block_total"] = 0
    return {"note_details": note_details, "calc_details": calc_details}


def note7(self, note_details, note_calc_details, calc_details):
    for note in note_details:
        for i in range(2, 5):
            if note.get("id") == i:
                note_calc_details[
                    "note7_gross_block_computer_software"
                ] += note[  # noqa: 501
                    "computer_software"
                ]

        if note.get("id") == 5:
            note["computer_software"] = note_calc_details[
                "note7_gross_block_computer_software"
            ]
        for i in range(8, 11):
            if note.get("id") == i:
                note_calc_details[
                    "note7_depreciation_computer_software"
                ] += note[  # noqa: 501
                    "computer_software"
                ]
        if note.get("id") == 11:
            note["computer_software"] = note_calc_details[
                "note7_depreciation_computer_software"
            ]
        if note.get("id") == 14:
            note_calc_details["note7_net_block_computer_software"] = note[
                "computer_software"
            ]

    calc_details = {
        "note7_gross_block_computer_software": note_calc_details.get(
            "note7_gross_block_computer_software"
        ),
        "note7_depreciation_computer_software": note_calc_details.get(
            "note7_depreciation_computer_software"
        ),
        "note7_net_block_computer_software": note_calc_details.get(
            "note7_net_block_computer_software"
        ),
    }
    note_calc_details["note7_gross_block_computer_software"] = 0
    note_calc_details["note7_depreciation_computer_software"] = 0
    note_calc_details["note7_net_block_computer_software"] = 0
    return {"note_details": note_details, "calc_details": calc_details}


def note13to15(self, note_details, note_calc_details, calc_details):
    filter_year = frappe.get_doc("Fiscal Year", self.cnp_fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note_details = json.loads(self.cnp_note_details)
    for note in note_details:
        for i in range(2, 5):
            if note.get("id") == i:
                note_calc_details["note13_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note13_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note13_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note13_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note13_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note13_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 5:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note13_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note13_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note13_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note13_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note13_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note13_year"
            )
        for i in range(8, 36):
            if note.get("id") == i:
                note_calc_details["note14_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note14_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note14_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note14_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note14_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note14_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 22:
            note_calc_details[
                "note14_period1_provisions_doubtful_debts"
            ] = note[  # noqa: 501
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note14_year_provisions_doubtful_debts"] = note[
                "year_ended_mar_31,_" + end_year
            ]

        if note.get("id") == 36:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note14_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note14_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note14_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note14_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note14_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note14_year"
            )
        for i in range(38, 40):
            if note.get("id") == i:
                note_calc_details["note15_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note15_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note15_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note15_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note15_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note15_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 40:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note15_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note15_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note15_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note15_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note15_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note15_year"
            )
    calc_details = {
        "note14_period1_provisions_doubtful_debts": note_calc_details.get(
            "note14_period1_provisions_doubtful_debts"
        ),
        "note14_year_provisions_doubtful_debts": note_calc_details.get(
            "note14_year_provisions_doubtful_debts"
        ),
        "note13_quarter1": note_calc_details.get("note13_quarter1"),
        "note13_quarter2": note_calc_details.get("note13_quarter2"),
        "note13_quarter3": note_calc_details.get("note13_quarter3"),
        "note13_period1": note_calc_details.get("note13_period1"),
        "note13_period2": note_calc_details.get("note13_period2"),
        "note13_year": note_calc_details.get("note13_year"),
        "note14_quarter1": note_calc_details.get("note14_quarter1"),
        "note14_quarter2": note_calc_details.get("note14_quarter2"),
        "note14_quarter3": note_calc_details.get("note14_quarter3"),
        "note14_period1": note_calc_details.get("note14_period1"),
        "note14_period2": note_calc_details.get("note14_period2"),
        "note14_year": note_calc_details.get("note14_year"),
        "note15_quarter1": note_calc_details.get("note15_quarter1"),
        "note15_quarter2": note_calc_details.get("note15_quarter2"),
        "note15_quarter3": note_calc_details.get("note15_quarter3"),
        "note15_period1": note_calc_details.get("note15_period1"),
        "note15_period2": note_calc_details.get("note15_period2"),
        "note15_year": note_calc_details.get("note15_year"),
    }
    note_calc_details["note14_period1_provisions_doubtful_debts"] = 0
    note_calc_details["note14_year_provisions_doubtful_debts"] = 0
    note_calc_details["note13_quarter1"] = 0
    note_calc_details["note13_quarter2"] = 0
    note_calc_details["note13_quarter3"] = 0
    note_calc_details["note13_period1"] = 0
    note_calc_details["note13_period2"] = 0
    note_calc_details["note13_year"] = 0
    note_calc_details["note14_quarter1"] = 0
    note_calc_details["note14_quarter2"] = 0
    note_calc_details["note14_quarter3"] = 0
    note_calc_details["note14_period1"] = 0
    note_calc_details["note14_period2"] = 0
    note_calc_details["note14_year"] = 0
    note_calc_details["note15_quarter1"] = 0
    note_calc_details["note15_quarter2"] = 0
    note_calc_details["note15_quarter3"] = 0
    note_calc_details["note15_period1"] = 0
    note_calc_details["note15_period2"] = 0
    note_calc_details["note15_year"] = 0
    return {"note_details": note_details, "calc_details": calc_details}


def note1to5(self, note_details, note_calc_details, calc_details):
    filter_year = frappe.get_doc("Fiscal Year", self.cnp_fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note_details = json.loads(self.cnp_note_details)
    for note in note_details:
        if note.get("id") == 3:
            copy1_quater1 = note["quarter_ended_sep_30,_" + end_year]
            copy1_quarter2 = note["quarter_ended_june_30,_" + end_year]
            copy1_quarter3 = note["quarter_ended_sep_30,_" + start_year]
            copy1_period1 = note["period_ended_sep_30,_" + end_year]
            copy1_period2 = note["period_ended_sep_30,_" + start_year]
            copy1_year = note["year_ended_mar_31,_" + end_year]
        if note.get("id") == 4:
            note["quarter_ended_sep_30,_" + end_year] = copy1_quater1
            note["quarter_ended_june_30,_" + end_year] = copy1_quarter2
            note["quarter_ended_sep_30,_" + start_year] = copy1_quarter3
            note["period_ended_sep_30,_" + end_year] = copy1_period1
            note["period_ended_sep_30,_" + start_year] = copy1_period2
            note["year_ended_mar_31,_" + end_year] = copy1_year
        if note.get("id") == 6:
            copy2_quater1 = note["quarter_ended_sep_30,_" + end_year]
            copy2_quarter2 = note["quarter_ended_june_30,_" + end_year]
            copy2_quarter3 = note["quarter_ended_sep_30,_" + start_year]
            copy2_period1 = note["period_ended_sep_30,_" + end_year]
            copy2_period2 = note["period_ended_sep_30,_" + start_year]
            copy2_year = note["year_ended_mar_31,_" + end_year]
        if note.get("id") == 7:
            note["quarter_ended_sep_30,_" + end_year] = copy2_quater1
            note["quarter_ended_june_30,_" + end_year] = copy2_quarter2
            note["quarter_ended_sep_30,_" + start_year] = copy2_quarter3
            note["period_ended_sep_30,_" + end_year] = copy2_period1
            note["period_ended_sep_30,_" + start_year] = copy2_period2
            note["year_ended_mar_31,_" + end_year] = copy2_year
            if copy1_quater1 or copy2_quater1:
                note_calc_details["note1_quarter1"] = (
                    copy1_quater1 + copy2_quater1
                )  # noqa: 501
            if copy1_quarter2 or copy2_quarter2:
                note_calc_details["note1_quarter2"] = (
                    copy1_quarter2 + copy2_quarter2
                )  # noqa: 501
            if copy1_quarter3 or copy2_quarter3:
                note_calc_details["note1_quarter3"] = (
                    copy1_quarter3 + copy2_quarter3
                )  # noqa: 501
            if copy1_period1 or copy2_period1:
                note_calc_details["note1_period1"] = (
                    copy1_period1 + copy2_period1
                )  # noqa: 501
            if copy1_period2 or copy2_period2:
                note_calc_details["note1_period2"] = (
                    copy1_period2 + copy2_period2
                )  # noqa: 501
            if copy1_year or copy1_year:
                note_calc_details["note1_year"] = copy1_year + copy2_year
        for i in range(11, 13):
            if note.get("id") == i:
                note_calc_details["note2_quarter1_a"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note2_quarter2_a"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note2_quarter3_a"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note2_period1_a"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note2_period2_a"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note2_year_a"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 13:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note2_quarter1_a"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note2_quarter2_a"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note2_quarter3_a"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note2_period1_a"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note2_period2_a"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note2_year_a"
            )  # noqa: 501
            a_quarter_1 = note["quarter_ended_sep_30,_" + end_year]
            a_quarter_2 = note["quarter_ended_june_30,_" + end_year]
            a_quarter_3 = note["quarter_ended_sep_30,_" + start_year]
            a_period_1 = note["period_ended_sep_30,_" + end_year]
            a_period_2 = note["period_ended_sep_30,_" + start_year]
            a_year = note["year_ended_mar_31,_" + end_year]
        for i in range(15, 19):
            if note.get("id") == i:
                note_calc_details["note2_quarter1_b"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note2_quarter2_b"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note2_quarter3_b"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note2_period1_b"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note2_period2_b"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note2_year_b"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 17:
            note_calc_details["note2_proposed_dividend_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note2_proposed_dividend_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]
        if note.get("id") == 19:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note2_quarter1_b"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note2_quarter2_b"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note2_quarter3_b"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note2_period1_b"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note2_period2_b"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note2_year_b"
            )  # noqa: 501
            b_quarter_1 = note["quarter_ended_sep_30,_" + end_year]
            b_quarter_2 = note["quarter_ended_june_30,_" + end_year]
            b_quarter_3 = note["quarter_ended_sep_30,_" + start_year]
            b_period_1 = note["period_ended_sep_30,_" + end_year]
            b_period_2 = note["period_ended_sep_30,_" + start_year]
            b_year = note["year_ended_mar_31,_" + end_year]

        if note.get("id") == 20:
            note["quarter_ended_sep_30,_" + end_year] = (
                a_quarter_1 + b_quarter_1
            )  # noqa: 501
            note["quarter_ended_june_30,_" + end_year] = (
                a_quarter_2 + b_quarter_2
            )  # noqa: 501
            note["quarter_ended_sep_30,_" + start_year] = (
                a_quarter_3 + b_quarter_3
            )  # noqa: 501
            note["period_ended_sep_30,_" + end_year] = a_period_1 + b_period_1
            note["period_ended_sep_30,_" + start_year] = (
                a_period_2 + b_period_2
            )  # noqa: 501
            note["year_ended_mar_31,_" + end_year] = a_year + b_year
            note_calc_details["note2_quarter1"] = note[
                "quarter_ended_sep_30,_" + end_year
            ]
            note_calc_details["note2_quarter2"] = note[
                "quarter_ended_june_30,_" + end_year
            ]
            note_calc_details["note2_quarter3"] = note[
                "quarter_ended_sep_30,_" + start_year
            ]
            note_calc_details["note2_period1"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note2_period2"] = note[
                "period_ended_sep_30,_" + start_year
            ]
            note_calc_details["note2_year"] = note[
                "year_ended_mar_31,_" + end_year
            ]  # noqa: 501
        for i in range(22, 24):
            if note.get("id") == i:
                note_calc_details["note3_quarter1"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note3_quarter2"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note3_quarter3"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note3_period1"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note3_period2"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note3_year"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 24:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note3_quarter1"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note3_quarter2"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note3_quarter3"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note3_period1"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note3_period2"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note3_year"
            )  # noqa: 501

        for i in range(26, 30):
            if note.get("id") == i:
                note_calc_details["note4_quarter1_long_term"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note4_quarter2_long_term"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note4_quarter3_long_term"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note4_period1_long_term"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note4_period2_long_term"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note4_year_long_term"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 31:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note4_quarter1_long_term"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note4_quarter2_long_term"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note4_quarter3_long_term"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note4_period1_long_term"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note4_period2_long_term"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note4_year_long_term"
            )
            a_quarter_1 = note["quarter_ended_sep_30,_" + end_year]
            a_quarter_2 = note["quarter_ended_june_30,_" + end_year]
            a_quarter_3 = note["quarter_ended_sep_30,_" + start_year]
            a_period_1 = note["period_ended_sep_30,_" + end_year]
            a_period_2 = note["period_ended_sep_30,_" + start_year]
            a_year = note["year_ended_mar_31,_" + end_year]
        for i in range(32, 37):
            if note.get("id") == i:
                note_calc_details["note4_quarter1_short_term"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note4_quarter2_short_term"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note4_quarter3_short_term"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note4_period1_short_term"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note4_period2_short_term"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note4_year_short_term"] += note[
                    "year_ended_mar_31,_" + end_year
                ]
        if note.get("id") == 37:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note4_quarter1_short_term"
            )
            note[
                "quarter_ended_june_30,_" + end_year
            ] = note_calc_details.get(  # noqa: 501
                "note4_quarter2_short_term"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note4_quarter3_short_term"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note4_period1_short_term"
            )
            note[
                "period_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note4_period2_short_term"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note4_year_short_term"
            )
            b_quarter_1 = note["quarter_ended_sep_30,_" + end_year]
            b_quarter_2 = note["quarter_ended_june_30,_" + end_year]
            b_quarter_3 = note["quarter_ended_sep_30,_" + start_year]
            b_period_1 = note["period_ended_sep_30,_" + end_year]
            b_period_2 = note["period_ended_sep_30,_" + start_year]
            b_year = note["year_ended_mar_31,_" + end_year]
        if note.get("id") == 38:
            note["quarter_ended_sep_30,_" + end_year] = (
                a_quarter_1 + b_quarter_1
            )  # noqa: 501
            note["quarter_ended_june_30,_" + end_year] = (
                a_quarter_2 + b_quarter_2
            )  # noqa: 501
            note["quarter_ended_sep_30,_" + start_year] = (
                a_quarter_3 + b_quarter_3
            )  # noqa: 501
            note["period_ended_sep_30,_" + end_year] = a_period_1 + b_period_1
            note["period_ended_sep_30,_" + start_year] = (
                a_period_2 + b_period_2
            )  # noqa: 501
            note["year_ended_mar_31,_" + end_year] = a_year + b_year
        if note.get("id") == 40:
            a_quarter_1 = note["quarter_ended_sep_30,_" + end_year]
            a_quarter_2 = note["quarter_ended_june_30,_" + end_year]
            a_quarter_3 = note["quarter_ended_sep_30,_" + start_year]
            a_period_1 = note["period_ended_sep_30,_" + end_year]
            a_period_2 = note["period_ended_sep_30,_" + start_year]
            a_year = note["year_ended_mar_31,_" + end_year]
            note_calc_details["note5_quarter1_trade_payables"] = a_quarter_1
            note_calc_details["note5_quarter2_trade_payables"] = a_quarter_2
            note_calc_details["note5_quarter3_trade_payables"] = a_quarter_3
            note_calc_details["note5_period1_trade_payables"] = a_period_1
            note_calc_details["note5_period2_trade_payables"] = a_period_2
            note_calc_details["note5_year_trade_payables"] = a_year
        if note.get("id") == 41:
            note["quarter_ended_sep_30,_" + end_year] = a_quarter_1
            note["quarter_ended_june_30,_" + end_year] = a_quarter_2
            note["quarter_ended_sep_30,_" + start_year] = a_quarter_3
            note["period_ended_sep_30,_" + end_year] = a_period_1
            note["period_ended_sep_30,_" + start_year] = a_period_2
            note["year_ended_mar_31,_" + end_year] = a_year
        for i in range(42, 51):
            if note.get("id") == i:
                note_calc_details["note5_quarter1_other_liabilities"] += note[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_calc_details["note5_quarter2_other_liabilities"] += note[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_calc_details["note5_quarter3_other_liabilities"] += note[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_calc_details["note5_period1_other_liabilities"] += note[
                    "period_ended_sep_30,_" + end_year
                ]
                note_calc_details["note5_period2_other_liabilities"] += note[
                    "period_ended_sep_30,_" + start_year
                ]
                note_calc_details["note5_year_other_liabilities"] += note[
                    "year_ended_mar_31,_" + end_year
                ]

        if note.get("id") == 50:
            note_calc_details["note5_period1_income_tax_liabilty"] = note[
                "period_ended_sep_30,_" + end_year
            ]
            note_calc_details["note5_year_income_tax_liabilty"] = note[
                "year_ended_mar_31,_" + end_year
            ]
        if note.get("id") == 51:
            note["quarter_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note5_quarter1_other_liabilities"
            )
            note["quarter_ended_june_30,_" + end_year] = note_calc_details.get(
                "note5_quarter2_other_liabilities"
            )
            note[
                "quarter_ended_sep_30,_" + start_year
            ] = note_calc_details.get(  # noqa: 501
                "note5_quarter3_other_liabilities"
            )
            note["period_ended_sep_30,_" + end_year] = note_calc_details.get(
                "note5_period1_other_liabilities"
            )
            note["period_ended_sep_30,_" + start_year] = note_calc_details.get(
                "note5_period2_other_liabilities"
            )
            note["year_ended_mar_31,_" + end_year] = note_calc_details.get(
                "note5_year_other_liabilities"
            )
            b_quarter_1 = note["quarter_ended_sep_30,_" + end_year]
            b_quarter_2 = note["quarter_ended_june_30,_" + end_year]
            b_quarter_3 = note["quarter_ended_sep_30,_" + start_year]
            b_period_1 = note["period_ended_sep_30,_" + end_year]
            b_period_2 = note["period_ended_sep_30,_" + start_year]
            b_year = note["year_ended_mar_31,_" + end_year]
        if note.get("id") == 52:
            note["quarter_ended_sep_30,_" + end_year] = (
                a_quarter_1 + b_quarter_1
            )  # noqa: 501
            note["quarter_ended_june_30,_" + end_year] = (
                a_quarter_2 + b_quarter_2
            )  # noqa: 501
            note["quarter_ended_sep_30,_" + start_year] = (
                a_quarter_3 + b_quarter_3
            )  # noqa: 501
            note["period_ended_sep_30,_" + end_year] = a_period_1 + b_period_1
            note["period_ended_sep_30,_" + start_year] = (
                a_period_2 + b_period_2
            )  # noqa: 501
            note["year_ended_mar_31,_" + end_year] = a_year + b_year
    calc_details = {
        "note1_quarter1": note_calc_details.get("note1_quarter1"),
        "note1_quarter2": note_calc_details.get("note1_quarter2"),
        "note1_quarter3": note_calc_details.get("note1_quarter3"),
        "note1_period1": note_calc_details.get("note1_period1"),
        "note1_period2": note_calc_details.get("note1_period2"),
        "note1_year": note_calc_details.get("note1_year"),
        "note2_quarter1": note_calc_details.get("note2_quarter1"),
        "note2_quarter2": note_calc_details.get("note2_quarter2"),
        "note2_quarter3": note_calc_details.get("note2_quarter3"),
        "note2_period1": note_calc_details.get("note2_period1"),
        "note2_period2": note_calc_details.get("note2_period2"),
        "note2_year": note_calc_details.get("note2_year"),
        "note2_quarter1_a": note_calc_details.get("note2_quarter1_a"),
        "note2_quarter2_a": note_calc_details.get("note2_quarter2_a"),
        "note2_quarter3_a": note_calc_details.get("note2_quarter3_a"),
        "note2_period1_a": note_calc_details.get("note2_period1_a"),
        "note2_period2_a": note_calc_details.get("note2_period2_a"),
        "note2_year_a": note_calc_details.get("note2_year_a"),
        "note2_quarter1_b": note_calc_details.get("note2_quarter1_b"),
        "note2_quarter2_b": note_calc_details.get("note2_quarter2_b"),
        "note2_quarter3_b": note_calc_details.get("note2_quarter3_b"),
        "note2_period1_b": note_calc_details.get("note2_period1_b"),
        "note2_period2_b": note_calc_details.get("note2_period2_b"),
        "note2_year_b": note_calc_details.get("note2_year_b"),
        "note3_quarter1": note_calc_details.get("note3_quarter1"),
        "note3_quarter2": note_calc_details.get("note3_quarter2"),
        "note3_quarter3": note_calc_details.get("note3_quarter3"),
        "note3_period1": note_calc_details.get("note3_period1"),
        "note3_period2": note_calc_details.get("note3_period2"),
        "note3_year": note_calc_details.get("note3_year"),
        "note4_quarter1_long_term": note_calc_details.get(
            "note4_quarter1_long_term"
        ),  # noqa: 501
        "note4_quarter2_long_term": note_calc_details.get(
            "note4_quarter2_long_term"
        ),  # noqa: 501
        "note4_quarter3_long_term": note_calc_details.get(
            "note4_quarter3_long_term"
        ),  # noqa: 501
        "note4_period1_long_term": note_calc_details.get(
            "note4_period1_long_term"
        ),  # noqa: 501
        "note4_period2_long_term": note_calc_details.get(
            "note4_period2_long_term"
        ),  # noqa: 501
        "note4_year_long_term": note_calc_details.get("note4_year_long_term"),
        "note4_quarter1_short_term": note_calc_details.get(
            "note4_quarter1_short_term"
        ),  # noqa: 501
        "note4_quarter2_short_term": note_calc_details.get(
            "note4_quarter2_short_term"
        ),  # noqa: 501
        "note4_quarter3_short_term": note_calc_details.get(
            "note4_quarter3_short_term"
        ),  # noqa: 501
        "note4_period1_short_term": note_calc_details.get(
            "note4_period1_short_term"
        ),  # noqa: 501
        "note4_period2_short_term": note_calc_details.get(
            "note4_period2_short_term"
        ),  # noqa: 501
        "note4_year_short_term": note_calc_details.get(
            "note4_year_short_term"
        ),  # noqa: 501
        "note5_quarter1_trade_payables": note_calc_details.get(
            "note5_quarter1_trade_payables"
        ),
        "note5_quarter2_trade_payables": note_calc_details.get(
            "note5_quarter2_trade_payables"
        ),
        "note5_quarter3_trade_payables": note_calc_details.get(
            "note5_quarter3_trade_payables"
        ),
        "note5_period1_trade_payables": note_calc_details.get(
            "note5_period1_trade_payables"
        ),
        "note5_period2_trade_payables": note_calc_details.get(
            "note5_period2_trade_payables"
        ),
        "note5_year_trade_payables": note_calc_details.get(
            "note5_year_trade_payables"
        ),  # noqa: 501
        "note5_quarter1_other_liabilities": note_calc_details.get(
            "note5_quarter1_other_liabilities"
        ),
        "note5_quarter2_other_liabilities": note_calc_details.get(
            "note5_quarter2_other_liabilities"
        ),
        "note5_quarter3_other_liabilities": note_calc_details.get(
            "note5_quarter3_other_liabilities"
        ),
        "note5_period1_other_liabilities": note_calc_details.get(
            "note5_period1_other_liabilities"
        ),
        "note5_period2_other_liabilities": note_calc_details.get(
            "note5_period2_other_liabilities"
        ),
        "note5_year_other_liabilities": note_calc_details.get(
            "note5_year_other_liabilities"
        ),
        "note5_period1_income_tax_liabilty": note_calc_details.get(
            "note5_period1_income_tax_liabilty"
        ),
        "note5_year_income_tax_liabilty": note_calc_details.get(
            "note5_year_income_tax_liabilty"
        ),
        "note2_proposed_dividend_period1": note_calc_details.get(
            "note2_proposed_dividend_period1"
        ),
        "note2_proposed_dividend_year": note_calc_details.get(
            "note2_proposed_dividend_year"
        ),
    }
    note_calc_details["note1_quarter1"] = 0
    note_calc_details["note1_quarter2"] = 0
    note_calc_details["note1_quarter3"] = 0
    note_calc_details["note1_period1"] = 0
    note_calc_details["note1_period2"] = 0
    note_calc_details["note1_year"] = 0

    note_calc_details["note2_quarter1"] = 0
    note_calc_details["note2_quarter2"] = 0
    note_calc_details["note2_quarter3"] = 0
    note_calc_details["note2_period1"] = 0
    note_calc_details["note2_period2"] = 0
    note_calc_details["note2_year"] = 0

    note_calc_details["note2_quarter1_a"] = 0
    note_calc_details["note2_quarter2_a"] = 0
    note_calc_details["note2_quarter3_a"] = 0
    note_calc_details["note2_period1_a"] = 0
    note_calc_details["note2_period2_a"] = 0
    note_calc_details["note2_year_a"] = 0

    note_calc_details["note2_quarter1_b"] = 0
    note_calc_details["note2_quarter2_b"] = 0
    note_calc_details["note2_quarter3_b"] = 0
    note_calc_details["note2_period1_b"] = 0
    note_calc_details["note2_period2_b"] = 0
    note_calc_details["note2_year_b"] = 0

    note_calc_details["note3_quarter1"] = 0
    note_calc_details["note3_quarter2"] = 0
    note_calc_details["note3_quarter3"] = 0
    note_calc_details["note3_period1"] = 0
    note_calc_details["note3_period2"] = 0
    note_calc_details["note3_year"] = 0

    note_calc_details["note4_quarter1_long_term"] = 0
    note_calc_details["note4_quarter2_long_term"] = 0
    note_calc_details["note4_quarter3_long_term"] = 0
    note_calc_details["note4_period1_long_term"] = 0
    note_calc_details["note4_period2_long_term"] = 0
    note_calc_details["note4_year_long_term"] = 0

    note_calc_details["note4_quarter1_short_term"] = 0
    note_calc_details["note4_quarter2_short_term"] = 0
    note_calc_details["note4_quarter3_short_term"] = 0
    note_calc_details["note4_period1_short_term"] = 0
    note_calc_details["note4_period2_short_term"] = 0
    note_calc_details["note4_year_short_term"] = 0

    note_calc_details["note5_quarter1_trade_payables"] = 0
    note_calc_details["note5_quarter2_trade_payables"] = 0
    note_calc_details["note5_quarter3_trade_payables"] = 0
    note_calc_details["note5_period1_trade_payables"] = 0
    note_calc_details["note5_period2_trade_payables"] = 0
    note_calc_details["note5_year_trade_payables"] = 0

    note_calc_details["note5_quarter1_other_liabilities"] = 0
    note_calc_details["note5_quarter2_other_liabilities"] = 0
    note_calc_details["note5_quarter3_other_liabilities"] = 0
    note_calc_details["note5_period1_other_liabilities"] = 0
    note_calc_details["note5_period2_other_liabilities"] = 0
    note_calc_details["note5_year_other_liabilities"] = 0

    note_calc_details["note5_period1_income_tax_liabilty"] = 0
    note_calc_details["note5_year_income_tax_liabilty"] = 0

    note_calc_details["note2_proposed_dividend_period1"] = 0
    note_calc_details["note2_proposed_dividend_year"] = 0
    return {"note_details": note_details, "calc_details": calc_details}


def note_calc(self):
    if self.cnp_type == "Note 1 to 5":
        return {
            "note1_quarter1": 0,
            "note1_quarter2": 0,
            "note1_quarter3": 0,
            "note1_period1": 0,
            "note1_period2": 0,
            "note1_year": 0,
            "note2_quarter1": 0,
            "note2_quarter2": 0,
            "note2_quarter3": 0,
            "note2_period1": 0,
            "note2_period2": 0,
            "note2_year": 0,
            "note2_quarter1_a": 0,
            "note2_quarter2_a": 0,
            "note2_quarter3_a": 0,
            "note2_period1_a": 0,
            "note2_period2_a": 0,
            "note2_year_a": 0,
            "note2_quarter1_b": 0,
            "note2_quarter2_b": 0,
            "note2_quarter3_b": 0,
            "note2_period1_b": 0,
            "note2_period2_b": 0,
            "note2_year_b": 0,
            "note3_quarter1": 0,
            "note3_quarter2": 0,
            "note3_quarter3": 0,
            "note3_period1": 0,
            "note3_period2": 0,
            "note3_year": 0,
            "note4_quarter1_long_term": 0,
            "note4_quarter2_long_term": 0,
            "note4_quarter3_long_term": 0,
            "note4_period1_long_term": 0,
            "note4_period2_long_term": 0,
            "note4_year_long_term": 0,
            "note4_quarter1_short_term": 0,
            "note4_quarter2_short_term": 0,
            "note4_quarter3_short_term": 0,
            "note4_period1_short_term": 0,
            "note4_period2_short_term": 0,
            "note4_year_short_term": 0,
            "note5_quarter1_trade_payables": 0,
            "note5_quarter2_trade_payables": 0,
            "note5_quarter3_trade_payables": 0,
            "note5_period1_trade_payables": 0,
            "note5_period2_trade_payables": 0,
            "note5_year_trade_payables": 0,
            "note5_quarter1_other_liabilities": 0,
            "note5_quarter2_other_liabilities": 0,
            "note5_quarter3_other_liabilities": 0,
            "note5_period1_other_liabilities": 0,
            "note5_period2_other_liabilities": 0,
            "note5_year_other_liabilities": 0,
            "note5_period1_income_tax_liabilty": 0,
            "note5_year_income_tax_liabilty": 0,
            "note2_proposed_dividend_period1": 0,
            "note2_proposed_dividend_year": 0,
        }
    if self.cnp_type == "Note 6":
        return {
            "note6_gross_block_computers": 0,
            "note6_gross_block_computer_server": 0,
            "note6_gross_block_mobile": 0,
            "note6_gross_block_furniture_and_fixture": 0,
            "note6_gross_block_office_equipment": 0,
            "note6_gross_block_total": 0,
            "note6_depreciation_computers": 0,
            "note6_depreciation_computer_server": 0,
            "note6_depreciation_mobile": 0,
            "note6_depreciation_furniture_and_fixture": 0,
            "note6_depreciation_office_equipment": 0,
            "note6_depreciation_total": 0,
            "note6_net_block_computers": 0,
            "note6_net_block_computer_server": 0,
            "note6_net_block_mobile": 0,
            "note6_net_block_furniture_and_fixture": 0,
            "note6_net_block_office_equipment": 0,
            "note6_net_block_total": 0,
        }
    if self.cnp_type == "Note 7":
        return {
            "note7_gross_block_computer_software": 0,
            "note7_depreciation_computer_software": 0,
            "note7_net_block_computer_software": 0,
        }
    if self.cnp_type == "Note 8 to 10":
        return {
            "note8_quarter1": 0,
            "note8_quarter2": 0,
            "note8_quarter3": 0,
            "note8_period1": 0,
            "note8_period2": 0,
            "note8_year": 0,
            "note8_1_quarter1": 0,
            "note8_1_quarter2": 0,
            "note8_1_quarter3": 0,
            "note8_1_period1": 0,
            "note8_1_period2": 0,
            "note8_1_year": 0,
            "note8_2_quarter1": 0,
            "note8_2_quarter2": 0,
            "note8_2_quarter3": 0,
            "note8_2_period1": 0,
            "note8_2_period2": 0,
            "note8_2_year": 0,
            "note9_1_quarter1": 0,
            "note9_1_quarter2": 0,
            "note9_1_quarter3": 0,
            "note9_1_period1": 0,
            "note9_1_period2": 0,
            "note9_1_year": 0,
            "note9_2_quarter1": 0,
            "note9_2_quarter2": 0,
            "note9_2_quarter3": 0,
            "note9_2_period1": 0,
            "note9_2_period2": 0,
            "note9_2_year": 0,
            "note9_2_non_current_quarter1": 0,
            "note9_2_non_current_quarter2": 0,
            "note9_2_non_current_quarter3": 0,
            "note9_2_non_current_period1": 0,
            "note9_2_non_current_period2": 0,
            "note9_2_non_current_year": 0,
            "note9_2_current_quarter1": 0,
            "note9_2_current_quarter2": 0,
            "note9_2_current_quarter3": 0,
            "note9_2_current_period1": 0,
            "note9_2_current_period2": 0,
            "note9_2_current_year": 0,
            "sub1_note9_quarter1": 0,
            "sub1_note9_quarter2": 0,
            "sub1_note9_quarter3": 0,
            "sub1_note9_period1": 0,
            "sub1_note9_period2": 0,
            "sub1_note9_year": 0,
            "sub2_note9_quarter1": 0,
            "sub2_note9_quarter2": 0,
            "sub2_note9_quarter3": 0,
            "sub2_note9_period1": 0,
            "sub2_note9_period2": 0,
            "sub2_note9_year": 0,
            "note10_1_quarter1": 0,
            "note10_1_quarter2": 0,
            "note10_1_quarter3": 0,
            "note10_1_period1": 0,
            "note10_1_period2": 0,
            "note10_1_year": 0,
            "note10_2_quarter1": 0,
            "note10_2_quarter2": 0,
            "note10_2_quarter3": 0,
            "note10_2_period1": 0,
            "note10_2_period2": 0,
            "note10_2_year": 0,
            "note9_2_interest_fd_period1": 0,
            "note9_2_interest_fd_year": 0,
        }
    if self.cnp_type == "Note 11 to 12":
        return {
            "note12_period1_cash_flow_interest": 0,
            "note12_year_cash_flow_interest": 0,
            "note12_period1_cash_flow_net_gain": 0,
            "note12_year_cash_flow_net_gain": 0,
            "note11_quarter1": 0,
            "note11_quarter2": 0,
            "note11_quarter3": 0,
            "note11_period1": 0,
            "note11_period2": 0,
            "note11_year": 0,
            "note12_quarter1": 0,
            "note12_quarter2": 0,
            "note12_quarter3": 0,
            "note12_period1": 0,
            "note12_period2": 0,
            "note12_year": 0,
        }
    if self.cnp_type == "Note 13 to 15":
        return {
            "note14_period1_provisions_doubtful_debts": 0,
            "note14_year_provisions_doubtful_debts": 0,
            "note13_quarter1": 0,
            "note13_quarter2": 0,
            "note13_quarter3": 0,
            "note13_period1": 0,
            "note13_period2": 0,
            "note13_year": 0,
            "note14_quarter1": 0,
            "note14_quarter2": 0,
            "note14_quarter3": 0,
            "note14_period1": 0,
            "note14_period2": 0,
            "note14_year": 0,
            "note15_quarter1": 0,
            "note15_quarter2": 0,
            "note15_quarter3": 0,
            "note15_period1": 0,
            "note15_period2": 0,
            "note15_year": 0,
        }


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    # Replace the '-' in fiscal_year with '_'
    # and concatenate with formattedCompany
    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def insert_report(self, type, formatted_string):
    canopi_reports = frappe.new_doc("Canopi Reports")
    canopi_reports.title = formatted_string
    canopi_reports.report_details = report_json(self, type)
    canopi_reports.calc_details = report_calc(type)
    canopi_reports.type = type
    canopi_reports.fiscal_year = self.cnp_fiscal_year
    canopi_reports.company = self.cnp_company
    canopi_reports.insert()
    frappe.db.commit()
    return canopi_reports


def report_calc(type):
    calc_details = "{}"
    calc_details = json.loads(calc_details)
    if type == "Profit Loss":
        calc_details = {
            "profit_before_tax_quarter1": 0,
            "profit_before_tax_quarter2": 0,
            "profit_before_tax_quarter3": 0,
            "profit_before_tax_period1": 0,
            "profit_before_tax_period2": 0,
            "profit_before_tax_year": 0,
            "total_revenue2_quarter1": 0,
            "total_revenue2_quarter2": 0,
            "total_revenue2_quarter3": 0,
            "total_revenue2_period1": 0,
            "total_revenue2_period2": 0,
            "total_revenue2_year": 0,
            "tax_revenue_quarter1": 0,
            "tax_revenue_quarter2": 0,
            "tax_revenue_quarter3": 0,
            "tax_revenue_period1": 0,
            "tax_revenue_period2": 0,
            "tax_revenue_year": 0,
            "current_tax_period1": 0,
        }
    if type == "Balance Sheet":
        calc_details = {
            "bs_non_current_assets_quarter1": 0,
            "bs_non_current_assets_quarter2": 0,
            "bs_non_current_assets_quarter3": 0,
            "bs_non_current_assets_period1": 0,
            "bs_non_current_assets_period2": 0,
            "bs_non_current_assets_year": 0,
            "bs_current_assets_quarter1": 0,
            "bs_current_assets_quarter2": 0,
            "bs_current_assets_quarter3": 0,
            "bs_current_assets_period1": 0,
            "bs_current_assets_period2": 0,
            "bs_current_assets_year": 0,
            "bs_fixed_assets_period1": 0,
            "bs_fixed_assets_year": 0,
        }
    if type == "Cash Flow":
        calc_details = {
            "cash_flow_operating_profit_quarter": 0,
            "cash_flow_operating_profit_year": 0,
            "cash_generated_quarter": 0,
            "cash_generated_year": 0,
            "net_cash_flow_quarter": 0,
            "net_cash_flow_year": 0,
            "maturity_bank_deposits_quarter": 0,
            "cash_flow_investing_profit_quarter": 0,
            "cash_flow_investing_profit_year": 0,
            "cash_flow_financing_profit_quarter": 0,
            "cash_flow_financing_profit_year": 0,
        }
    calc_details = json.dumps(calc_details)
    return calc_details


def report_json(self, type):
    filter_year = frappe.get_doc("Fiscal Year", self.cnp_fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    if type == "Balance Sheet":
        report_details = [
            {
                "particulars": "Equity and liabilities",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Shareholders funds",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 2,
            },
            {
                "particulars": "Share capital",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "particulars": "Reserves & surplus",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 4,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 5,
            },
            {
                "particulars": "",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": "Non-current liabilites",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 7,
            },
            {
                "particulars": "Other long term liabilities",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "Long term provisions",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 9,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 11,
            },
            {
                "particulars": "Current liabilities",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 12,
            },
            {
                "particulars": "Trade payables",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 13,
            },
            {
                "particulars": "Other current liabilities",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "particulars": "Short term provisions",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 16,
            },
            {
                "particulars": "Total",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 17,
            },
            {
                "particulars": "Assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 18,
            },
            {
                "particulars": "Non-current assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 19,
            },
            {
                "particulars": "Fixed assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 20,
            },
            {
                "particulars": "Tangible assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 21,
            },
            {
                "particulars": "Intangible assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 22,
            },
            {
                "particulars": "Intangible asset under development",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 23,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 24,
            },
            {
                "particulars": "Deferred Tax Asset (net)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 25,
            },
            {
                "particulars": "Other non-current assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 26,
            },
            {
                "particulars": "Loans &  Advances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 27,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 28,
            },
            {
                "particulars": "Current assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 29,
            },
            {
                "particulars": "Current Investments",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 30,
            },
            {
                "particulars": "Loans &  Adavances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 31,
            },
            {
                "particulars": "Trade receivables",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 32,
            },
            {
                "particulars": "Cash and Bank balances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 33,
            },
            {
                "particulars": "Other current assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 34,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 35,
            },
            {
                "particulars": "Total",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 36,
            },
        ]
    if type == "Profit Loss":
        report_details = [
            {
                "particulars": "Income",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Revenue from operations",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Other income",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "particulars": "Total revenue (I)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 4,
            },
            {
                "particulars": "",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: "",
                "quarter_ended_sep_30,_" + start_year: "",
                "period_ended_sep_30,_" + end_year: "",
                "period_ended_sep_30,_" + start_year: "",
                "year_ended_mar_31,_" + end_year: "",
                "indent": 0,
                "id": 5,
            },
            {
                "particulars": "Expenses",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": "Employee benefit expense",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 7,
            },
            {
                "particulars": "Other expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "CSR Contribution",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 9,
            },
            {
                "particulars": "Depreciation and amortisation",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "Total expenses (II)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 11,
            },
            {
                "particulars": "Profit before  tax  (I-II)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 12,
            },
            {
                "particulars": "Tax for earlier Years",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 13,
            },
            {
                "particulars": "Current Tax",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "particulars": "Deferred Tax",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "particulars": "Tax expense",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 16,
            },
            {
                "particulars": "Profit/(Loss) after tax for the Year",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 17,
            },
            {
                "particulars": "Earning Per Share",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 18,
            },
            {
                "particulars": "Basic",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 19,
            },
            {
                "particulars": "Diluted",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 20,
            },
        ]
    if type == "Cash Flow":
        report_details = [
            {
                "cash_flow_from_operating_activities": "Profit before tax from continuing operations",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 1,
            },
            {
                "cash_flow_from_operating_activities": "Adjustment to reconcile profit before tax to net cash flows",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 2,
            },
            {
                "cash_flow_from_operating_activities": "Depreciation/ amortization on continuing operation",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "cash_flow_from_operating_activities": "Provision for doubtful debts (net)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 4,
            },
            {
                "cash_flow_from_operating_activities": "Loss/(Profit) on disposal/ write off on property,plant & equipment / intangible assets pertaining to   continuing operations ",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 5,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 6,
            },
            {
                "cash_flow_from_operating_activities": "Net Gain on sale of current investments",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 2445,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 7,
            },
            {
                "cash_flow_from_operating_activities": "Interest  Income",
                "for_the_quarter_ended_sep,_" + end_year: 3334,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "cash_flow_from_operating_activities": "Operating profit before working capital changes",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 60558,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 9,
            },
            {
                "cash_flow_from_operating_activities": "Movements in working capital :",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 10,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ (decrease) in trade payables",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 11,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ (decrease) in long-term provisions",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 12,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ (decrease) in short-term provisions",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 13,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ (decrease) in other current liabilities",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ (decrease) in other long-term liabilities",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "cash_flow_from_operating_activities": "Decrease / (increase) in trade receivables",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 16,
            },
            {
                "cash_flow_from_operating_activities": "Decrease / (increase) in Loans & Advances",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -44442,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 17,
            },
            {
                "cash_flow_from_operating_activities": "Decrease / (increase) in other current assets",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -218891,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 18,
            },
            {
                "cash_flow_from_operating_activities": "Cash generated from operations",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -202775,
                "for_the_year_ended_mar_31,_" + end_year: 2475467040,
                "indent": 0,
                "id": 19,
            },
            {
                "cash_flow_from_operating_activities": "Direct taxes paid (net of refunds)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 1387920,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 20,
            },
            {
                "cash_flow_from_operating_activities": "Net cash_flow_from_operating_activities (A)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 1185145,
                "for_the_year_ended_mar_31,_" + end_year: 2784900420,
                "indent": 0,
                "id": 21,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 22,
            },
            {
                "cash_flow_from_operating_activities": "Cash flows from investing activities",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 23,
            },
            {
                "cash_flow_from_operating_activities": "Purchase of fixed assets, including CWIP and capital advances",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -230322,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 24,
            },
            {
                "cash_flow_from_operating_activities": "Proceeds from Sale of Fixed Assets",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 25,
            },
            {
                "cash_flow_from_operating_activities": "Investments in bank deposits (having original maturity of more than twelve months)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -977,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 26,
            },
            {
                "cash_flow_from_operating_activities": "Redemption/ maturity of bank deposits (having original maturity of more than twelve months)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -345552,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 27,
            },
            {
                "cash_flow_from_operating_activities": "Purchase of current investments",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 28,
            },
            {
                "cash_flow_from_operating_activities": "Proceeds from sale/maturity of current investments",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -45553,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 29,
            },
            {
                "cash_flow_from_operating_activities": "Interest  income",
                "for_the_quarter_ended_sep,_" + end_year: 222225,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 30,
            },
            {
                "cash_flow_from_operating_activities": "Net cash flow from/ (used in) investing activities (B)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: -400179,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 31,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 32,
            },
            {
                "cash_flow_from_operating_activities": "Cash flows from financing activities",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 33,
            },
            {
                "cash_flow_from_operating_activities": "Dividend paid on equity shares",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 223,
                "for_the_year_ended_mar_31,_" + end_year: 45452,
                "indent": 1,
                "id": 34,
            },
            {
                "cash_flow_from_operating_activities": "Tax on equity dividend paid",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 35,
            },
            {
                "cash_flow_from_operating_activities": "Net cash flow from/ (used in) in financing activities (C)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 223,
                "for_the_year_ended_mar_31,_" + end_year: 45452,
                "indent": 0,
                "id": 36,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 37,
            },
            {
                "cash_flow_from_operating_activities": "Net increase/(decrease) in cash and cash equivalents (A + B + C)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 38,
            },
            {
                "cash_flow_from_operating_activities": "Cash and cash equivalents at the beginning of the year",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 39,
            },
            {
                "cash_flow_from_operating_activities": "Cash and cash equivalents at the end of the year",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 40,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 41,
            },
            {
                "cash_flow_from_operating_activities": "Components of cash and cash equivalents",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 42,
            },
            {
                "cash_flow_from_operating_activities": "With banks- on current account incl. Cash in Hand",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 43,
            },
            {
                "cash_flow_from_operating_activities": "Total cash and cash equivalents (note 15)",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 44,
            },
        ]
    report_details = json.dumps(report_details)
    return report_details


@frappe.whitelist()
def notes_events(row_data, fiscal_year, company, note_type, note_name):
    filter_year = frappe.get_doc("Fiscal Year", fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    row_data = json.loads(row_data)
    note = frappe.get_doc("Note", note_name)
    note_details = json.loads(note.cnp_note_details)
    if (
        note_type == "Note 1 to 5"
        or note_type == "Note 8 to 10"
        or note_type == "Note 11 to 12"
        or note_type == "Note 13 to 15"
    ):
        for note_detail in note_details:
            if note_detail.get("id") == row_data.get("id"):
                columns = [
                    "quarter_ended_sep_30,_" + end_year,
                    "quarter_ended_june_30,_" + end_year,
                    "quarter_ended_sep_30,_" + start_year,
                    "period_ended_sep_30,_" + end_year,
                    "period_ended_sep_30,_" + start_year,
                    "year_ended_mar_31,_" + end_year,
                ]

                for column in columns:
                    if not isinstance(row_data[column], (int, float)):
                        frappe.throw(
                            "Value should not be a character or empty for column "  # noqa: 501
                            + format_string(column, note_type)
                            + " at row "
                            + str(row_data.get("id"))
                        )

                note_detail["quarter_ended_sep_30,_" + end_year] = row_data[
                    "quarter_ended_sep_30,_" + end_year
                ]
                note_detail["quarter_ended_june_30,_" + end_year] = row_data[
                    "quarter_ended_june_30,_" + end_year
                ]
                note_detail["quarter_ended_sep_30,_" + start_year] = row_data[
                    "quarter_ended_sep_30,_" + start_year
                ]
                note_detail["period_ended_sep_30,_" + end_year] = row_data[
                    "period_ended_sep_30,_" + end_year
                ]
                note_detail["period_ended_sep_30,_" + start_year] = row_data[
                    "period_ended_sep_30,_" + start_year
                ]
                note_detail["year_ended_mar_31,_" + end_year] = row_data[
                    "year_ended_mar_31,_" + end_year
                ]
                break
    if note_type == "Note 6":
        for note_detail in note_details:
            if note_detail.get("id") == row_data.get("id"):
                columns = [
                    "computers",
                    "computer_server",
                    "mobile",
                    "furniture_and_fixtures",
                    "office_equipment",
                ]

                for column in columns:
                    if not isinstance(row_data[column], (int, float)):
                        frappe.throw(
                            "Value should not be a character or empty for column "  # noqa: 501
                            + format_string(column, note_type)
                            + " at row "
                            + str(row_data.get("id"))
                        )
                note_detail["computers"] = row_data["computers"]
                note_detail["computer_server"] = row_data["computer_server"]
                note_detail["mobile"] = row_data["mobile"]
                note_detail["furniture_and_fixtures"] = row_data[
                    "furniture_and_fixtures"
                ]
                note_detail["office_equipment"] = row_data["office_equipment"]
                break
    if note_type == "Note 7":
        for note_detail in note_details:
            if note_detail.get("id") == row_data.get("id"):
                columns = ["computer_software"]

                for column in columns:
                    if not isinstance(row_data[column], (int, float)):
                        frappe.throw(
                            "Value should not be a character or empty for column "  # noqa: 501
                            + format_string(column, note_type)
                            + " at row "
                            + str(row_data.get("id"))
                        )
                note_detail["computer_software"] = row_data[
                    "computer_software"
                ]  # noqa: 501
                break
    note.cnp_note_details = json.dumps(note_details)
    note.save()
    return note


def format_string(input_string, note_type):
    # Split the input string by underscores and comma
    parts = input_string.split("_")
    if note_type == "Note 1 to 5":
        # Extract the relevant parts
        type_of = parts[0].capitalize()
        text = parts[1].capitalize()
        month = parts[2].capitalize()
        day = parts[3]
        year = parts[4]
        # Build the formatted string
        formatted_string = f"{type_of} {text} {month} {day} {year}"
    if note_type == "Note 6":
        type_of = parts[0].capitalize()
        formatted_string = f"{type_of}"
    if note_type == "Note 7":
        type_of = parts[0].capitalize()
        formatted_string = f"{type_of}"
    return formatted_string
