import base64
import io
import json
import math
import ntpath
import os
import string
from datetime import date, datetime, timedelta
from json import JSONDecodeError
from re import sub

import boto3
import botocore.exceptions
import frappe
import pyqrcode
import pytz
import requests
from botocore.exceptions import ClientError
from dateutil import relativedelta
from erpnext.accounts.doctype.sales_invoice.sales_invoice import SalesInvoice
from erpnext.crm.utils import CRMNote
from frappe.desk.form import assign_to, document_follow
from frappe.desk.form.utils import add_comment
from frappe.model.mapper import get_mapped_doc
from frappe.utils import cstr, split_emails
from frappe.utils.data import get_url
from frappe.utils.pdf import get_pdf
from pyhanko.pdf_utils.incremental_writer import IncrementalPdfFileWriter
from pyhanko.sign import signers
from pyhanko.sign.fields import SigFieldSpec, append_signature_field

from trusteeship_platform.public.india import states as STATES

from trusteeship_platform.exceptions import (  # noqa: 501 isort:skip
    BadGatewayError,
    NotImplementedError,
)

from frappe.desk.doctype.notification_log.notification_log import (  # noqa: 501 isort:skip
    enqueue_create_notification,
    get_title,
    get_title_html,
)


@frappe.whitelist()
def num_in_words(integer, in_million=False):
    """
    Returns string in words for the given integer.
    """
    from num2words import num2words

    locale = "en_IN" if not in_million else frappe.local.lang
    integer = int(integer)
    try:
        ret = num2words(integer, lang=locale)
    except NotImplementedError:
        ret = num2words(integer, lang="en")
    except OverflowError:
        ret = num2words(integer, lang="en")
    return ret.replace("-", " ")


@frappe.whitelist()
def num_in_words_in_lakhs_and_million(integer):
    return {
        "in_lakh": num_in_words(integer),
        "in_million": num_in_words(integer, True),
    }  # noqa: 501


@frappe.whitelist()
def fetch_lead_from_probe42(doc):
    doc = json.loads(doc)

    lead_doc = {}
    if doc.get("__islocal"):
        lead_doc = frappe.new_doc("Lead")
        lead_doc.lead_name = doc.get("lead_name")
        lead_doc.cin_number = doc.get("cin_number")
        lead_doc.organization_lead = doc.get("organization_lead")
        lead_doc.cnp_type = doc.get("cnp_type")
    else:
        lead_doc = frappe.get_doc("Lead", doc.get("name"))
    is_pan = (
        lead_doc.cnp_type == "Individual"
        or lead_doc.cnp_type == "Partnership Firm"  # noqa: 501
    )
    response = fetch_probe42_details(lead_doc.cin_number, is_pan)
    lead_doc.company_name = response["data"]["company"]["legal_name"]
    lead_doc.website = response["data"]["company"]["website"]
    lead_doc.cnp_is_data_fetched = 1
    lead_doc.save()
    org_type = "llp" if lead_doc.cnp_type == "LLP" else "company"  # noqa
    create_lead_contact(response["data"], lead_doc.name, org_type)
    create_lead_address(doc, lead_doc.name)
    create_lead_fetched_address(response["data"]["company"], lead_doc.name)
    create_lead_gst_address(response["data"]["gst_details"], lead_doc.name)
    create_industry_segment(
        response["data"]["industry_segments"], lead_doc.name
    )  # noqa

    return lead_doc


@frappe.whitelist()
def fetch_opportunity_from_probe42(
    response,
    opportunity_name,
    source,
    is_local,
    security_type,
    opportunity_from="",
    party_name="",
    cnp_type="",
    cin_number="",
    product="",
    facility_amt=0,
    issue_size=0,
    tenor_years=0,
    tenor_months=0,
    tenor_days=0,
):
    opportunity_doc = prepare_opportunity_doc(
        is_local,
        opportunity_name,
        source,
        opportunity_from,
        party_name,
        cnp_type,
        cin_number,
        product,
        facility_amt,
        issue_size,
        tenor_years,
        tenor_months,
        tenor_days,
        security_type,
    )
    if isinstance(response, str):
        response = json.loads(response)
    fin_info_row = prepare_fin_info_row(response, cnp_type)
    if bool(fin_info_row):
        opportunity_doc.append("cnp_fininfo", fin_info_row)

    prepare_credit_ratings(opportunity_doc, response)

    update_opportunity_doc(opportunity_doc, response)

    opportunity_doc.save()

    return opportunity_doc


def prepare_opportunity_doc(
    is_local,
    opportunity_name,
    source,
    opportunity_from,
    party_name,
    cnp_type,
    cin_number,
    product,
    facility_amt,
    issue_size,
    tenor_years,
    tenor_months,
    tenor_days,
    security_type,
):
    security_type = json.loads(security_type)
    cnp_type = (
        "LLP" if (cnp_type == "llps" or cnp_type == "LLP") else "Organization"
    )  # noqa
    if bool(int(is_local)) is True:
        opportunity_doc = frappe.new_doc("Opportunity")
        opportunity_doc.opportunity_from = opportunity_from
        opportunity_doc.party_name = party_name
        opportunity_doc.cin_number = cin_number
        opportunity_doc.cnp_type = cnp_type
        opportunity_doc.cnp_product = product
        opportunity_doc.cnp_facility_amt = facility_amt
        opportunity_doc.cnp_issue_size = issue_size
        opportunity_doc.cnp_tenor_years = tenor_years
        opportunity_doc.cnp_tenor_months = tenor_months
        opportunity_doc.cnp_tenor_days = tenor_days
        opportunity_doc.source = source
        if security_type:
            for element in security_type:
                if "type_of_security" in element:
                    opportunity_doc.append(
                        "cnp_security_type",
                        {"type_of_security": element["type_of_security"]},
                    )
    else:
        opportunity_doc = frappe.get_doc("Opportunity", opportunity_name)

    return opportunity_doc


def prepare_fin_info_row(response, org_type):
    if org_type == "llps":
        pnl_key = "statement_of_income_and_expenditure"
        bs_key = "statement_of_assets_and_liabilities"
    else:
        pnl_key = "pnl"
        bs_key = "bs"

    fin_info_row = {}
    for finance in response["data"]["financials"]:
        fin_info_row["net_revenue"] = finance[pnl_key]["lineItems"][
            "net_revenue"
        ]  # noqa: 501 isort:skip
        fin_info_row["operating_profit"] = finance[pnl_key]["lineItems"][
            "operating_profit"
        ]
        fin_info_row["profit_from_discontinuing"] = finance[pnl_key][
            "lineItems"
        ][  # noqa: 501 isort:skip
            "profit_from_discontinuing_operation_after_tax"
        ]
        try:
            fin_info_row["debt_equity"] = (
                finance[bs_key]["liabilities"].get("long_term_borrowings", 0)
                + finance[bs_key]["liabilities"].get("short_term_borrowings", 0)  # noqa
            ) / finance[bs_key]["subTotals"].get("total_equity", 0)
        except Exception as e:
            frappe.logger("utils").exception(e)
            return

    for nbfc_finance in response["data"]["nbfc_financials"]:
        fin_info_row["debt_securities"] = nbfc_finance[bs_key]["liabilities"][
            "debt_securities"
        ]
        fin_info_row["loans"] = nbfc_finance[bs_key]["assets"]["loans"]

    return fin_info_row


def prepare_credit_ratings(opportunity_doc, response):
    for rating in response["data"]["credit_ratings"]:
        opportunity_doc.append(
            "cnp_cr_rating",
            {
                "rating_agency": rating["rating_agency"],
                "rating_date": rating["rating_date"],
                "current_rating": rating["rating"],
                "type_of_loan": rating["type_of_loan"],
                "currency": rating["currency"],
                "amount": rating["amount"],
                "status": rating["rating_details"][0]["action"],
            },
        )


def update_opportunity_doc(opportunity_doc, response):
    try:
        org_type = "llp" if opportunity_doc.cnp_type == "llps" else "company"  # noqa
        opportunity_doc.cnp_reg_year = response["data"][org_type][
            "incorporation_date"
        ]  # noqa: 501 isort:skip
        opportunity_doc.cnp_director_shareholdings = json.dumps(
            response["data"]["director_shareholdings"]
        )
        opportunity_doc.cnp_authorized_signatories = json.dumps(
            response["data"]["authorized_signatories"]
        )
    except Exception as e:
        frappe.logger("utils").exception(e)

    opportunity_doc.cnp_is_probe42_data_fetched = 1


@frappe.whitelist()
def fetch_customer_from_probe42(
    customer_name, response, is_local, is_comprehensive, cin_number=""
):
    customer_doc = {}
    if bool(int(is_local)) is True:
        customer_doc = frappe.new_doc("Customer")
        customer_doc.customer_name = customer_name
        customer_doc.cin_number = cin_number
    else:
        customer_doc = frappe.get_doc(
            "Customer", {"customer_name": customer_name}
        )  # noqa
    incorporation_date = response["data"]["company"]["incorporation_date"]
    customer_doc.cnp_reg_year = incorporation_date
    if is_comprehensive:
        customer_doc.cnp_director_shareholdings = json.dumps(
            response["data"]["director_shareholdings"]
        )
    customer_doc.cnp_authorized_signatories = json.dumps(
        response["data"]["authorized_signatories"]
    )
    customer_doc.cnp_is_probe42_data_fetched = 1

    customer_doc.save()

    return customer_doc


@frappe.whitelist()
def get_doc_list(doctype, fields=('["*"]'), filters=[], order_by="", pluck=""):
    list = frappe.get_list(
        doctype, fields=fields, filters=filters, order_by=order_by, pluck=pluck
    )  # noqa: 501 isort:skip
    return list


@frappe.whitelist()
def make_new_lead(cin_number, company_name):
    doc = frappe.new_doc("Lead")
    doc.cin_number = cin_number
    doc.company_name = company_name
    doc.insert()
    return doc


@frappe.whitelist()
def make_new_opportunity(opportunity_from, party_name, product):
    doc = frappe.new_doc("Opportunity")
    doc.opportunity_from = opportunity_from
    doc.party_name = party_name
    doc.cnp_product = product
    doc.insert()
    return doc


def fetch_probe42_details(cin_or_pan, is_pan=False):
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    if not settings_doc.probe_42_url:
        frappe.throw("Please set Probe42 Url to proceed with Probe42 fetch")
    url = f"""{settings_doc.probe_42_url}/companies/{cin_or_pan}/comprehensive-details"""  # noqa: 501

    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.get(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    return response


def process_response(response):
    reason = response.reason
    try:
        if getattr(response, "json", None):
            reason = response.json()
        if response.status_code < 300 and response.status_code > 199:
            return response.json()
    except JSONDecodeError:
        return {
            "status_code": response.status_code,
            "reason": reason,
        }

    frappe.throw(
        "Bad Gateway Error: " + json.dumps(reason), BadGatewayError(reason)
    )  # noqa: 501 isort:skip


def create_lead_address(doc, lead_name):
    address = {}
    if doc.get("address_title"):
        address["address_type"] = doc.get("address_type")
        address["address_title"] = doc.get("address_title")
        address["address_line1"] = doc.get("address_line1")
        address["address_line2"] = (
            doc.get("address_line2") if doc.get("address_line2") else ""
        )
        address["city"] = doc.get("city")
        address["county"] = doc.get("county") if doc.get("county") else ""
        address["state"] = doc.get("state").title() if doc.get("state") else ""
        address["country"] = doc.get("country") if doc.get("country") else ""
        address["pincode"] = doc.get("pincode")

    if address:
        states_lowercase = {s.lower(): s for s in STATES}
        address_doc = frappe.new_doc("Address")
        address_doc.address_type = address["address_type"]
        address_doc.address_title = address["address_title"]
        address_doc.address_line1 = address["address_line1"]
        address_doc.address_line2 = address["address_line2"]
        address_doc.city = address["city"]
        address_doc.county = address["county"]
        state = address["state"].lower()
        address_doc.state = states_lowercase[state]
        address_doc.country = address["country"]
        address_doc.pincode = address["pincode"]
        address_doc.append(
            "links", {"link_doctype": "Lead", "link_name": lead_name}
        )  # noqa: 501 isort:skip
        address_doc.insert()


def create_lead_fetched_address(payload, lead_name):
    try:
        states_lowercase = {s.lower(): s for s in STATES}

        if "business_address" in payload:
            business_add_doc = frappe.new_doc("Address")
            business_add_doc.address_title = "Business Address"

            business_add_doc.address_line1 = payload["business_address"][
                "address_line1"
            ]  # noqa: 501 isort:skip
            business_add_doc.address_line2 = payload["business_address"][
                "address_line2"
            ]  # noqa: 501 isort:skip

            if len(business_add_doc.address_line1) > 140:
                business_add_doc.address_line2 = (
                    business_add_doc.address_line1[140:]
                    + ", "
                    + business_add_doc.address_line2
                )  # noqa: 501 isort:skip
                business_add_doc.address_line1 = business_add_doc.address_line1[  # noqa
                    :140
                ]  # noqa: 501 isort:skip

            if len(business_add_doc.address_line2) > 140:
                business_add_doc.cnp_address_line3 = (  # noqa: 501 isort:skip
                    business_add_doc.address_line2[140:]
                )
                business_add_doc.address_line2 = business_add_doc.address_line2[  # noqa
                    :140
                ]  # noqa: 501 isort:skip

            business_add_doc.city = payload["business_address"]["city"]
            state = payload["business_address"]["state"].lower()
            business_add_doc.state = states_lowercase[state]
            business_add_doc.pincode = payload["business_address"]["pincode"]
            business_add_doc.append(
                "links", {"link_doctype": "Lead", "link_name": lead_name}
            )
            business_add_doc.insert()

        if "registered_address" in payload:
            registered_add_doc = frappe.new_doc("Address")
            registered_add_doc.address_title = "Registered Address"

            registered_add_doc.address_line1 = payload["registered_address"][
                "address_line1"
            ]  # noqa: 501 isort:skip
            registered_add_doc.address_line2 = payload["registered_address"][
                "address_line2"
            ]  # noqa: 501 isort:skip

            if len(registered_add_doc.address_line1) > 140:
                registered_add_doc.address_line2 = (
                    registered_add_doc.address_line1[140:]
                    + ", "
                    + registered_add_doc.address_line2
                )  # noqa: 501 isort:skip
                registered_add_doc.address_line1 = (  # noqa: 501 isort:skip
                    registered_add_doc.address_line1[:140]
                )

            if len(registered_add_doc.address_line2) > 140:
                registered_add_doc.cnp_address_line3 = (  # noqa: 501 isort:skip
                    registered_add_doc.address_line2[140:]
                )
                registered_add_doc.address_line2 = (  # noqa: 501 isort:skip
                    registered_add_doc.address_line2[:140]
                )

            registered_add_doc.city = payload["registered_address"]["city"]
            state = payload["registered_address"]["state"].lower()
            registered_add_doc.state = states_lowercase[state]
            registered_add_doc.pincode = payload["registered_address"][
                "pincode"
            ]  # noqa
            registered_add_doc.append(
                "links", {"link_doctype": "Lead", "link_name": lead_name}
            )
            registered_add_doc.insert()
    except Exception as e:
        frappe.logger("utils").exception(e)
        return


def create_lead_gst_address(payload, lead_name):
    try:
        states_lowercase = {s.lower(): s for s in STATES}
        for index, address in enumerate(payload):
            address_doc = frappe.new_doc("Address")
            address_doc.address_title = "GST Address " + str(index + 1)
            address_doc.type = "Billing"
            address_doc.country = "India"
            address_doc.city = "NA"
            address_doc.address_line1 = "NA"
            address_doc.pincode = "NA"
            address_doc.gstin = address["gstin"]
            address_doc.gst_state = (
                address["state"].title() if address.get("state") else ""
            )  # noqa: 501 isort:skip
            address_doc.cnp_nature_of_business = (
                address["nature_of_business_activities"]
                if address.get("nature_of_business_activities")
                else ""
            )

            if address.get("gstin"):
                gstin_details = fetch_gstin_from_canopi(address["gstin"])

                if gstin_details["data"].get("dty"):
                    if not frappe.db.exists(
                        "Tax Category", gstin_details["data"]["dty"]
                    ):  # noqa: 501 isort:skip
                        category = frappe.new_doc("Tax Category")
                        category.title = gstin_details["data"]["dty"]
                        category.insert()
                    address_doc.tax_category = gstin_details["data"]["dty"]

                address_doc.cnp_company_name = (
                    gstin_details["data"]["lgnm"]
                    if gstin_details["data"].get("lgnm")
                    else ""
                )
                address_doc.cnp_trade_name = (
                    gstin_details["data"]["tradeNam"]
                    if gstin_details["data"].get("tradeNam")
                    else ""
                )
                address_doc.cnp_state_jurisdiction = (
                    gstin_details["data"]["stj"]
                    if gstin_details["data"].get("stj")
                    else ""  # noqa: 501 isort:skip
                )
                address_doc.cnp_centre_jurisdiction = (
                    gstin_details["data"]["ctj"]
                    if gstin_details["data"].get("ctj")
                    else ""  # noqa: 501 isort:skip
                )
                address_doc.cnp_registration_date = (
                    (
                        datetime.strptime(
                            gstin_details["data"]["rgdt"], "%d/%m/%Y"
                        )  # noqa: 501 isort:skip
                    ).date()  # noqa: 501 isort:skip
                    if gstin_details["data"].get("rgdt")
                    else ""
                )
                address_doc.cnp_status = (
                    gstin_details["data"]["sts"]
                    if gstin_details["data"].get("sts")
                    else ""  # noqa: 501 isort:skip
                )

                if gstin_details["data"].get("pradr"):
                    if gstin_details["data"]["pradr"].get("addr"):
                        address_doc.address_line1 = (
                            get_address_line1(
                                gstin_details["data"]["pradr"][
                                    "addr"
                                ]  # noqa: 501 isort:skip
                            )  # noqa: 501 isort:skip
                            if get_address_line1(
                                gstin_details["data"]["pradr"]["addr"]
                            )  # noqa: 501 isort:skip
                            else "NA"
                        )
                        address_doc.address_line2 = get_address_line2(
                            gstin_details["data"]["pradr"]["addr"]
                        )
                        address_doc.state = gstin_details["data"]["pradr"][
                            "addr"
                        ][  # noqa
                            "stcd"
                        ].title()
                        address_doc.pincode = (
                            gstin_details["data"]["pradr"]["addr"]["pncd"]
                            if gstin_details["data"]["pradr"]["addr"]["pncd"]
                            else "NA"
                        )
                        address_doc.city = (
                            gstin_details["data"]["pradr"]["addr"]["city"]
                            if gstin_details["data"]["pradr"]["addr"].get(
                                "city"
                            )  # noqa
                            else gstin_details["data"]["pradr"]["addr"]["dst"]
                            if gstin_details["data"]["pradr"]["addr"].get("dst")  # noqa
                            else "NA"
                        )

            address_doc.append(
                "links", {"link_doctype": "Lead", "link_name": lead_name}
            )  # noqa: 501 isort:skip
            address_doc.gst_category = "Registered Regular"
            if address_doc.state is not None:
                state = address_doc.state.lower()
                address_doc.state = states_lowercase[state]
                address_doc.insert()
    except Exception as e:
        return e


def get_address_line1(gstin_details_data):
    address_line_1 = {}
    address_line_1["flno"] = (
        gstin_details_data["flno"] + " "
        if gstin_details_data.get("flno")
        else ""  # noqa: 501 isort:skip
    )
    address_line_1["bno"] = (
        gstin_details_data["bno"] + " "
        if gstin_details_data.get("bno")
        else ""  # noqa: 501 isort:skip
    )
    address_line_1["bnm"] = (
        gstin_details_data["bnm"] + " "
        if gstin_details_data.get("bnm")
        else ""  # noqa: 501 isort:skip
    )
    return (
        address_line_1["flno"] + address_line_1["bno"] + address_line_1["bnm"]
    )  # noqa: 501 isort:skip


def get_address_line2(gstin_details_data):
    address_line_2 = {}
    address_line_2["st"] = (
        gstin_details_data["st"] + " " if gstin_details_data.get("st") else ""
    )
    address_line_2["loc"] = (
        gstin_details_data["loc"] + " " if gstin_details_data.get("loc") else ""  # noqa
    )
    address_line_2["lt"] = (
        gstin_details_data["lt"] + " " if gstin_details_data.get("lt") else ""
    )
    address_line_2["lg"] = (
        gstin_details_data["lg"] + " " if gstin_details_data.get("lg") else ""
    )

    return (
        address_line_2["st"]
        + address_line_2["loc"]
        + address_line_2["lt"]
        + address_line_2["lg"]
    )


def create_lead_contact(payload, lead_name, org_type):
    try:
        first_name = payload[org_type]["legal_name"]
        if frappe.db.exists("Contact", f"{first_name}-{lead_name}"):
            return
        contact_doc = frappe.new_doc("Contact")
        contact_doc.first_name = first_name
        for email in payload.get("contact_details", {}).get("email", {}):
            contact_doc.append("email_ids", {"email_id": email["emailId"]})
        for phone in payload.get("contact_details", {}).get("phone", {}):
            contact_doc.append("phone_nos", {"phone": phone["phoneNumber"]})
        contact_doc.append(
            "links", {"link_doctype": "Lead", "link_name": lead_name}
        )  # noqa: 501 isort:skip
        contact_doc.insert()
    except Exception as e:
        frappe.logger("utils").exception(e)
        return


def create_industry_segment(payload, lead_name):
    for industry_segment in payload:
        if frappe.db.exists("Industry Type", industry_segment.get("industry")):
            update_industry_reference_table(
                industry_segment["industry"], lead_name
            )  # noqa: 501 isort:skip
        else:
            make_new_industry_type(industry_segment["industry"], lead_name)

        for segment in industry_segment["segments"]:
            if frappe.db.exists("Market Segment", segment):
                update_market_segment_reference_table(
                    segment, lead_name, industry_segment["industry"]
                )
            else:
                make_new_market_segment(
                    segment, lead_name, industry_segment["industry"]
                )


@frappe.whitelist()
def delete_lead_with_connection(docname):
    filters = [["party_name", "=", docname]]
    quotation = frappe.get_list("Quotation", filters=filters)
    if quotation:
        frappe.throw(
            (
                "Cannot delete Lead {} because Quotation has been created for selected Lead."  # noqa: 501 isort:skip
            ).format(docname),
            frappe.LinkExistsError,
        )
    else:
        delete_lead_connected_address(docname)
        delete_lead_connected_contacts(docname)
        delete_lead_connected_industry_type(docname)
        delete_lead_connected_market_segment(docname)
        delete_lead_connected_opportunity(docname)
        frappe.delete_doc("Lead", docname)


def delete_lead_connected_address(docname):
    filters = [["link_doctype", "=", "Lead"], ["link_name", "=", docname]]
    address_list = get_doc_list("Address", filters=filters)
    for address in address_list:
        doc = frappe.get_doc("Address", address.name)
        if len(doc.links) > 1:
            delete_link_table_row("Address", address.name, docname)
        else:
            frappe.delete_doc("Address", address.name)


def delete_lead_connected_contacts(docname):
    filters = [["link_doctype", "=", "Lead"], ["link_name", "=", docname]]
    contacts_list = get_doc_list("Contact", filters=filters)
    for contact in contacts_list:
        doc = frappe.get_doc("Contact", contact.name)
        if len(doc.links) > 1:
            delete_link_table_row("Contact", contact.name, docname)
        else:
            frappe.delete_doc("Contact", contact.name)


def delete_lead_connected_market_segment(docname):
    market_list = get_doc_list(
        "Market Segment",
        filters=[["link_doctype", "=", "Lead"], ["link_name", "=", docname]],
    )
    for market in market_list:
        doc = frappe.get_doc("Market Segment", market.name)
        for links in doc.cnp_links:
            if links.link_name == docname:
                doc.cnp_links.remove(links)
        doc.save()


def delete_lead_connected_industry_type(docname):
    industry_list = get_doc_list(
        "Industry Type",
        filters=[["link_doctype", "=", "Lead"], ["link_name", "=", docname]],
    )
    for industry in industry_list:
        doc = frappe.get_doc("Industry Type", industry.name)
        for links in doc.cnp_links:
            if links.link_name == docname:
                doc.cnp_links.remove(links)
        doc.save()


def delete_link_table_row(doctype, docname, link_docname):
    doc = frappe.get_doc(doctype, docname)
    links = list(filter(lambda x: (x.link_name != link_docname), doc.links))
    doc.links = links
    doc.save()


def delete_lead_connected_opportunity(docname):
    filters = [["opportunity_from", "=", "Lead"], ["party_name", "=", docname]]
    opportunity_list = get_doc_list(
        "Opportunity",
        filters=filters,
    )
    for opportunity in opportunity_list:
        frappe.delete_doc(
            "Canopi Pricing Policy",
            opportunity.name + "-" + opportunity.cnp_product,  # noqa: 501 isort:skip
        )
        frappe.delete_doc("Opportunity", opportunity.name)


@frappe.whitelist()
def make_new_industry_type(industry_name, lead_link_name):
    industry_doc = frappe.new_doc("Industry Type")
    industry_doc.industry = industry_name
    industry_doc.append(
        "cnp_links", {"link_doctype": "Lead", "link_name": lead_link_name}
    )
    industry_doc.insert()


@frappe.whitelist()
def update_industry_reference_table(industry_name, lead_link_name):
    industry_doc = frappe.get_doc("Industry Type", industry_name)
    if (
        contains(
            industry_doc.cnp_links, lambda x: x.link_name == lead_link_name
        )  # noqa: 501 isort:skip
        is False
    ):
        industry_doc.append(
            "cnp_links", {"link_doctype": "Lead", "link_name": lead_link_name}
        )
        industry_doc.save()


@frappe.whitelist()
def make_new_market_segment(segment_name, lead_link_name, industry_link_name):
    market_segment_doc = frappe.new_doc("Market Segment")
    market_segment_doc.market_segment = segment_name
    market_segment_doc.append(
        "cnp_links", {"link_doctype": "Lead", "link_name": lead_link_name}
    )
    market_segment_doc.append(
        "cnp_links",
        {
            "link_doctype": "Industry Type",
            "link_name": industry_link_name,
        },  # noqa: 501 isort:skip
    )
    market_segment_doc.insert()


@frappe.whitelist()
def update_market_segment_reference_table(
    segment_name, lead_link_name, industry_link_name
):
    market_segment_doc = frappe.get_doc("Market Segment", segment_name)
    if (
        contains(
            market_segment_doc.cnp_links,
            lambda x: x.link_name == lead_link_name,  # noqa: 501 isort:skip
        )  # noqa: 501 isort:skip
        is False
    ):
        market_segment_doc.append(
            "cnp_links", {"link_doctype": "Lead", "link_name": lead_link_name}
        )

    if (
        contains(
            market_segment_doc.cnp_links,
            lambda x: x.link_name == industry_link_name,  # noqa: 501 isort:skip
        )
        is False
    ):
        market_segment_doc.append(
            "cnp_links",
            {"link_doctype": "Industry Type", "link_name": industry_link_name},
        )

    market_segment_doc.save()


@frappe.whitelist()
def get_doc(doc_name, doc_type):
    if frappe.db.exists(doc_type, doc_name):
        doc = frappe.get_doc(doc_type, doc_name)
        return doc


@frappe.whitelist()
def get_single_doc(doc_type):
    doc = frappe.get_single(doc_type)
    return doc


@frappe.whitelist()
def get_multiple_doc(docs):
    docs = json.loads(docs)
    document = {}
    for doc in docs:
        if doc["doctype"] and doc["docname"]:
            document[snake_case(doc["doctype"])] = frappe.get_doc(
                doc["doctype"], doc["docname"]
            )
    return document


@frappe.whitelist()
def get_docs_with_linked_childs(docs):
    # Note: all parent doctypes in docs
    # should be sorted above of all child doctpyes

    # syntax: docs = [
    # {doctype, docname}, #for non-linked child docs
    # {doctype, parent_doctype, child_link_field} #for linked child docs
    # ]

    docs = json.loads(docs)
    document = {}

    # separate linked child doctypes
    docs_without_links = filter(lambda x: "docname" in x, docs)
    docs_with_links = filter(
        lambda x: ("parent_doctype" in x) and ("child_link_field" in x), docs
    )

    # gets all non-linked child documents
    for doc in docs_without_links:
        if doc["doctype"] and doc["docname"]:
            if frappe.db.exists(doc["doctype"], doc["docname"]):
                document[snake_case(doc["doctype"])] = frappe.get_doc(
                    doc["doctype"], doc["docname"]
                )
            else:
                document[snake_case(doc["doctype"])] = None

    # gets all linked child documents
    for doc in docs_with_links:
        if doc["doctype"]:
            # if linked child doctypes have multiple fields
            # in its naming series
            if type(doc["child_link_field"]) == list:
                parent = document[snake_case(doc["parent_doctype"])]
                child_doc_name = "-".join(
                    parent.get(str(x)) for x in doc["child_link_field"]
                )
            else:
                parent = document[snake_case(doc["parent_doctype"])]
                child_doc_name = parent.get(doc["child_link_field"])
            if child_doc_name:
                if frappe.db.exists(doc["doctype"], child_doc_name):
                    document[snake_case(doc["doctype"])] = frappe.get_doc(
                        doc["doctype"], child_doc_name
                    )
                else:
                    document[snake_case(doc["doctype"])] = None

    return document


def snake_case(s):
    return "_".join(
        sub(
            "([A-Z][a-z]+)",
            r" \1",
            sub("([A-Z]+)", r" \1", s.replace("-", " ")),  # noqa: 501 isort:skip
        ).split()
    ).lower()


@frappe.whitelist()
def fetch_contact(name):
    if name:
        doc = frappe.get_last_doc("Contact", filters={"name": name})

        salutation = doc.salutation if doc.salutation else ""
        first_name = doc.first_name if doc.first_name else ""
        middle_name = doc.middle_name if doc.middle_name else ""
        last_name = doc.last_name if doc.last_name else ""
        full_name = (
            salutation + " " + first_name + " " + middle_name + " " + last_name
        )  # noqa: 501 isort:skip
        if doc.phone_nos:
            return {"number": doc.phone_nos[0].phone, "full_name": full_name}
        else:
            return {"number": "", "full_name": full_name}


def contains(list, filter):
    for x in list:
        if filter(x):
            return True
    return False


@frappe.whitelist()
def get_markert_industry_list(lead_link_name):
    industry_list = frappe.get_all(
        doctype="Dynamic Link",
        filters=[
            ["parenttype", "=", "Industry Type"],
            ["link_doctype", "=", "Lead"],
            ["link_name", "=", lead_link_name],
        ],
        fields=('["*"]'),
    )
    industry_and_market_list = []
    for industry in industry_list:
        doc = frappe.get_all(
            doctype="Dynamic Link",
            filters=[
                ["parenttype", "=", "Market Segment"],
                ["link_doctype", "=", "Industry Type"],
                ["link_name", "=", industry.parent],
            ],
            fields=('["*"]'),
        )
        market_segment_list = []

        for segment in doc:
            market_segment_list.append(segment.parent)

        industry_and_market_list.append(
            {"industry": industry.parent, "segments": market_segment_list}
        )

    return industry_and_market_list


@frappe.whitelist()
def save_director_signatory_in_customer(
    director, signatory, customer, cin_number
):  # noqa: 501 isort:skip
    if customer:
        doc = frappe.get_doc("Customer", customer)
        if (
            (doc.cnp_director_shareholdings != director)
            or (doc.cnp_authorized_signatories != signatory)
            or (doc.cin_number != cin_number)
        ):
            doc.cnp_director_shareholdings = director
            doc.cnp_authorized_signatories = signatory
            doc.cin_number = cin_number
            doc.save()


def fetch_gstin_from_canopi(gstin):
    settings_doc = frappe.get_single("Trusteeship Platform Settings")

    token_response = get_canopi_access_token(settings_doc)
    gstin_details = process_response(
        requests.get(
            settings_doc.search_gstin_url + "/" + gstin,
            headers={
                "Authorization": "Bearer " + token_response.get("access_token")
            },  # noqa: 501 isort:skip
        )
    )
    return gstin_details


def get_canopi_access_token(settings_doc):
    cached_token = frappe.cache().get_value("token")
    if cached_token:
        return json.loads(cached_token)
    else:
        token_response = process_response(
            requests.post(
                settings_doc.access_token_url,
                json={
                    "emailAddress": settings_doc.email_id,
                    "password": settings_doc.get_password("email_password"),
                },
                headers={"Content-Type": "application/json"},
            )
        )
        frappe.cache().set_value(
            "token",
            json.dumps(token_response),
            expires_in_sec=token_response["expires_in"],
        )
        return token_response


@frappe.whitelist()
def cal_security_trustee_sell_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    no_of_securties = None
    immovable_properties = None
    states_immovable_properties = None
    no_of_lenders = None
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    # Adjustment of Pricing Policy Parameters
    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.no_of_securities_check = 1
        doc.immovable_properties_check = 1
        doc.states_immovable_properties_check = 1
        doc.no_of_lenders_check = 1
        doc.value_added_services_check = 1
        doc.is_insertable = 1
        doc.save()

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )
    no_of_securties = (
        1
        if pricing_policy_doc.no_of_securities < 3
        else 1.2
        if pricing_policy_doc.no_of_securities < 5
        else 1.5
    )
    immovable_properties = (
        1
        if pricing_policy_doc.immovable_properties < 1
        else 1.2
        if pricing_policy_doc.immovable_properties < 3
        else 1.5
        if pricing_policy_doc.immovable_properties < 11
        else 2
    )
    states_immovable_properties = (
        1
        if pricing_policy_doc.states_immovable_properties < 2
        else 1.1
        if pricing_policy_doc.states_immovable_properties < 3
        else 1.2
        if pricing_policy_doc.states_immovable_properties < 4
        else 1.3
        if pricing_policy_doc.states_immovable_properties < 5
        else 1.4
        if pricing_policy_doc.states_immovable_properties < 6
        else 1.5
    )
    no_of_lenders = (
        1
        if pricing_policy_doc.no_of_lenders < 3
        else 1.2
        if pricing_policy_doc.no_of_lenders < 6
        else 1.5
        if pricing_policy_doc.no_of_lenders < 11
        else 1.8
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (initial_cost - criterion_doc.bcg_employee_mandate_cost)
                * no_of_securties
                * immovable_properties
                * states_immovable_properties
                * no_of_lenders
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * no_of_securties
        * immovable_properties
        * states_immovable_properties
        * no_of_lenders
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_debenture_trustee_sell_fee(item_code, opportunity, facility_amt):
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.no_of_securities_check = 1
        doc.immovable_properties_check = 1
        doc.states_immovable_properties_check = 1
        doc.listed_yes_no_check = 1
        doc.fund_size_in_rscr_check = 1
        doc.whether_public_issue_check = 1
        doc.value_added_services_check = 1
        doc.is_insertable = 1
        doc.fund_size_in_rs_cr = facility_amt
        doc.save()

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    # If 'Listed (Yes/No)' from canopi pricing policy whether issue is No
    if pricing_policy_doc.listed_yes_no == "No":
        values = calculate_selling_cost_if_no(pricing_policy_doc, item_code)
        final_selling_initial_cost = values["initial"]
        final_selling_annual_cost = values["annual"]

    # If 'Listed (Yes/No)' from canopi pricing policy whether issue is Yes
    else:
        no_of_securities = pricing_policy_doc.no_of_securities
        public_issue = pricing_policy_doc.whether_public_issue
        opp = frappe.get_doc("Opportunity", opportunity)
        item_doc = frappe.get_doc("Item", item_code)
        values = calculate_selling_cost_if_yes(
            opp, item_doc, public_issue, no_of_securities
        )
        final_selling_initial_cost = values["initial"]
        final_selling_annual_cost = values["annual"]

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_escrow_agent_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    no_of_sellers = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.no_of_sellers_check = 1
        doc.value_added_services_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    no_of_sellers = (
        1
        if pricing_policy_doc.no_of_sellers < 3
        else 1.2
        if pricing_policy_doc.no_of_sellers < 5
        else 1.5
        if pricing_policy_doc.no_of_sellers < 11
        else 1.8
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * no_of_sellers
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost * no_of_sellers * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_facility_agent_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    no_of_lenders = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    payment_frequency = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.no_of_lenders_check = 1
        doc.value_added_services_check = 1
        doc.payment_frequency_mqh_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    no_of_lenders = (
        1
        if pricing_policy_doc.no_of_lenders < 3
        else 1.2
        if pricing_policy_doc.no_of_lenders < 5
        else 1.5
        if pricing_policy_doc.no_of_lenders < 11
        else 1.8
    )

    payment_frequency = (
        1.5
        if pricing_policy_doc.payment_frequency == "M"
        else 1.2
        if pricing_policy_doc.payment_frequency == "Q"
        else 1
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * no_of_lenders
                * payment_frequency
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * no_of_lenders
        * payment_frequency
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_alternate_investment_fund_fee(item_code, opportunity, facility_amt):
    initial_cost = 0
    annual_cost = 0
    fund_category = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    fund_size_in_rs_cr = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.fund_category_check = 1
        doc.fund_size_in_rs_cr = facility_amt
        doc.value_added_services_check = 1
        doc.fund_size_in_rscr_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    fund_size_in_rs_cr = (
        1
        if int(pricing_policy_doc.fund_size_in_rs_cr) <= 100
        else 1.1
        if int(pricing_policy_doc.fund_size_in_rs_cr) <= 200
        else 1.2
        if int(pricing_policy_doc.fund_size_in_rs_cr) <= 500
        else 1.5
    )

    fund_category = (
        1.2
        if pricing_policy_doc.fund_category == "III"
        else 1
        if pricing_policy_doc.fund_category == "II"
        else 1
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * fund_category
                * fund_size_in_rs_cr
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * fund_category
        * fund_size_in_rs_cr
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_lenders_agent_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    no_of_lenders = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.no_of_lenders_check = 1
        doc.value_added_services_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    no_of_lenders = (
        1
        if pricing_policy_doc.no_of_lenders < 3
        else 1.2
        if pricing_policy_doc.no_of_lenders < 5
        else 1.5
        if pricing_policy_doc.no_of_lenders < 11
        else 1.8
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * no_of_lenders
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost * no_of_lenders * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_infrastructure_investment_fund_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    assets = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    whether_public_issue = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.assets_check = 1
        doc.value_added_services_check = 1
        doc.whether_public_issue_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    assets = (
        1
        if int(pricing_policy_doc.assets) < 6
        else 1.2
        if int(pricing_policy_doc.assets) < 11
        else 1.5
        if int(pricing_policy_doc.assets) < 16
        else 1.8
    )

    whether_public_issue = (
        1.2 if pricing_policy_doc.whether_public_issue == "Yes" else 1
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * assets
                * whether_public_issue
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * assets
        * whether_public_issue
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


def cal_common_products_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.value_added_services_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost * pricing_policy_doc.value_added_services, 1000
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_real_estate_investment_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    assets = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    whether_public_issue = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.whether_public_issue = "Yes"
        doc.assets_check = 1
        doc.value_added_services_check = 1
        doc.whether_public_issue_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    assets = (
        1
        if int(pricing_policy_doc.assets) < 6
        else 1.2
        if int(pricing_policy_doc.assets) < 11
        else 1.5
        if int(pricing_policy_doc.assets) < 16
        else 1.8
    )

    whether_public_issue = (
        1.2 if pricing_policy_doc.whether_public_issue == "Yes" else 1
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * assets
                * whether_public_issue
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * assets
        * whether_public_issue
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


@frappe.whitelist()
def cal_securitization_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    listed_yes_no = 0
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0
    payment_frequency = 0
    da_yes_no = 0

    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.listed_yes_no_check = 1
        doc.value_added_services_check = 1
        doc.payment_frequency_mqh_check = 1
        doc.da_yes_no_check = 1
        doc.is_insertable = 1
        doc.save()

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )

    listed_yes_no = 1.3 if pricing_policy_doc.no_of_lenders == "Yes" else 1

    payment_frequency = (
        1.5
        if pricing_policy_doc.payment_frequency == "M"
        else 1.2
        if pricing_policy_doc.payment_frequency == "Q"
        else 1
    )

    da_yes_no = 0.75 if pricing_policy_doc.no_of_lenders == "Yes" else 1

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * listed_yes_no
                * payment_frequency
                * da_yes_no
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * listed_yes_no
        * payment_frequency
        * da_yes_no
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (1 + 0.2)
    final_selling_annual_cost = final_buying_annual_cost * (1 + 0.2)

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


def cal_security_agent_fee(item_code, opportunity):
    initial_cost = 0
    annual_cost = 0
    no_of_securties = None
    immovable_properties = None
    states_immovable_properties = None
    no_of_lenders = None
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    final_selling_initial_cost = 0
    final_selling_annual_cost = 0

    # Calculation of Initial And Annual Cost (Buying Price)
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    # Adjustment of Pricing Policy Parameters
    if not frappe.db.exists(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    ):  # noqa: 501 isort:skip
        doc = frappe.new_doc("Canopi Pricing Policy")
        doc.product_name = item_code
        doc.opportunity = opportunity
        doc.no_of_securities_check = 1
        doc.immovable_properties_check = 1
        doc.states_immovable_properties_check = 1
        doc.no_of_lenders_check = 1
        doc.value_added_services_check = 1
        doc.is_insertable = 1
        doc.save()

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + item_code
    )
    no_of_securties = (
        1
        if pricing_policy_doc.no_of_securities < 3
        else 1.2
        if pricing_policy_doc.no_of_securities < 5
        else 1.5
    )
    immovable_properties = (
        1
        if pricing_policy_doc.immovable_properties < 1
        else 1.2
        if pricing_policy_doc.immovable_properties < 3
        else 1.5
        if pricing_policy_doc.immovable_properties < 11
        else 2
    )
    states_immovable_properties = (
        1
        if pricing_policy_doc.states_immovable_properties < 2
        else 1.1
        if pricing_policy_doc.states_immovable_properties < 3
        else 1.2
        if pricing_policy_doc.states_immovable_properties < 4
        else 1.3
        if pricing_policy_doc.states_immovable_properties < 5
        else 1.4
        if pricing_policy_doc.states_immovable_properties < 6
        else 1.5
    )
    no_of_lenders = (
        1
        if pricing_policy_doc.no_of_lenders < 3
        else 1.2
        if pricing_policy_doc.no_of_lenders < 6
        else 1.5
        if pricing_policy_doc.no_of_lenders < 11
        else 1.8
    )

    # Calculation of Initial and Annual Cost (Final Buying Price)
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                (
                    initial_cost
                    - criterion_doc.bcg_employee_mandate_cost
                    - criterion_doc.mandate_cost_of_capital_dt_license
                )
                * no_of_securties
                * immovable_properties
                * states_immovable_properties
                * no_of_lenders
                * pricing_policy_doc.value_added_services
            )
            + criterion_doc.bcg_employee_mandate_cost
            + criterion_doc.mandate_cost_of_capital_dt_license
        ),
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * no_of_securties
        * immovable_properties
        * states_immovable_properties
        * no_of_lenders
        * pricing_policy_doc.value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    final_selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )

    return {
        "initial_cost": final_selling_initial_cost,
        "annual_cost": final_selling_annual_cost,
    }


def calculate_selling_cost_if_no(pricing_policy_doc, item_code):
    # Calculation of Initial And Annual Cost (Buying Price)
    initial_cost = 0
    annual_cost = 0
    no_of_securties = None
    immovable_properties = None
    states_immovable_properties = None
    final_buying_initial_cost = 0
    final_buying_annual_cost = 0
    selling_initial_cost = 0
    selling_annual_cost = 0
    item_doc = frappe.get_doc("Item", item_code)
    initial_cost = cal_excel_floor(item_doc.cnp_one_time_cost, 1000)
    annual_cost = cal_excel_floor(item_doc.cnp_on_going_cost, 1000)

    # Adjustment of Pricing Policy Parameters
    no_of_securties = (
        0.6
        if pricing_policy_doc.no_of_securities < 1
        else 1
        if pricing_policy_doc.no_of_securities < 3
        else 1.2
        if pricing_policy_doc.no_of_securities < 5
        else 1.5
    )
    immovable_properties = (
        1
        if pricing_policy_doc.immovable_properties < 1
        else 1.2
        if pricing_policy_doc.immovable_properties < 3
        else 1.5
        if pricing_policy_doc.immovable_properties < 11
        else 2
    )
    states_immovable_properties = (
        1
        if pricing_policy_doc.states_immovable_properties < 2
        else 1.1
        if pricing_policy_doc.states_immovable_properties < 3
        else 1.2
        if pricing_policy_doc.states_immovable_properties < 4
        else 1.3
        if pricing_policy_doc.states_immovable_properties < 5
        else 1.4
        if pricing_policy_doc.states_immovable_properties < 6
        else 1.5
    )
    listed_DT = 1.25 if pricing_policy_doc.listed_yes_no == "Yes" else 1
    value_added_services = pricing_policy_doc.value_added_services

    # Calculation of Initial and Annual Cost (Final Buying Price)
    initial_fee_1 = (
        75000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 51
        else 125000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 101
        else 150000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 201
        else 200000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 501
        else 250000
    )
    initial_fee = (
        initial_fee_1
        if pricing_policy_doc.no_of_securities < 1
        else 100000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 51
        else 150000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 101
        else 200000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 201
        else 250000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 501
        else 300000
    )
    annual_fee = (
        75000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 51
        else 125000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 101
        else 150000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 201
        else 200000
        if int(pricing_policy_doc.fund_size_in_rs_cr) < 501
        else 300000
    )
    criterion_doc = frappe.get_single("Canopi Criterion")
    final_buying_initial_cost = cal_excel_floor(
        (
            (
                initial_cost
                - criterion_doc.bcg_employee_mandate_cost
                - criterion_doc.mandate_cost_of_capital_dt_license
            )
            * no_of_securties
            * immovable_properties
            * states_immovable_properties
            * listed_DT
            * value_added_services
        )
        + criterion_doc.bcg_employee_mandate_cost
        + criterion_doc.mandate_cost_of_capital_dt_license,
        1000,
    )
    final_buying_annual_cost = cal_excel_floor(
        annual_cost
        * no_of_securties
        * immovable_properties
        * states_immovable_properties
        * listed_DT
        * value_added_services,
        1000,
    )

    # Calculation of Final Initial and Annual Fee (Selling price)
    selling_initial_cost = final_buying_initial_cost * (
        1 + criterion_doc.profit_percentage
    )
    selling_annual_cost = final_buying_annual_cost * (
        1 + criterion_doc.profit_percentage
    )
    final_selling_initial_cost = max(initial_fee, selling_initial_cost)
    final_selling_annual_cost = max(annual_fee, selling_annual_cost)
    return {
        "annual": final_selling_annual_cost,
        "initial": final_selling_initial_cost,
    }  # noqa: 501 isort:skip


def calculate_selling_cost_if_yes(
    opp, item_doc, public_issue, no_of_securities
):  # noqa: 501 isort:skip
    # for number of securities=0 row selected should be
    # 'Unsecured' and for non-zero 'secured'

    # If 'whether public issue' from canopi pricing policy
    # whether issue is Yes
    values = {"annual": 0, "initial": 0}
    if public_issue == "Yes":
        if item_doc.cnp_psu:
            for row in item_doc.cnp_psu:
                if (
                    row.condition == "Greater Than"
                    and float(opp.cnp_issue_size) > row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Greater Than"
                    and float(opp.cnp_issue_size) > row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities == 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less than Equal to"
                    and float(opp.cnp_issue_size) <= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less than Equal to"
                    and float(opp.cnp_issue_size) <= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities == 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Range"
                    and row.from_issue_size * 10000000
                    <= int(opp.cnp_issue_size)  # noqa: 501 isort:skip
                    and int(opp.cnp_issue_size) <= row.to_issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Range"
                    and row.from_issue_size * 10000000
                    <= int(opp.cnp_issue_size)  # noqa: 501 isort:skip
                    and int(opp.cnp_issue_size) <= row.to_issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities == 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less Than"
                    and float(opp.cnp_issue_size) < row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less Than"
                    and float(opp.cnp_issue_size) < row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Greater than Equal to"
                    and float(opp.cnp_issue_size) >= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Greater than Equal to"
                    and float(opp.cnp_issue_size) >= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Equal to"
                    and float(opp.cnp_issue_size) == row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Equal to"
                    and float(opp.cnp_issue_size) == row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Not Equal to"
                    and float(opp.cnp_issue_size) != row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Not Equal to"
                    and float(opp.cnp_issue_size) != row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
    # If 'whether public issue' from canopi pricing policy whether issue is No
    else:
        if item_doc.cnp_non_psu:
            for row in item_doc.cnp_non_psu:
                if (
                    row.condition == "Greater Than"
                    and float(opp.cnp_issue_size) > row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Greater Than"
                    and float(opp.cnp_issue_size) > row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities == 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less than Equal to"
                    and float(opp.cnp_issue_size) <= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less than Equal to"
                    and float(opp.cnp_issue_size) <= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities == 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Range"
                    and row.from_issue_size * 10000000
                    <= int(opp.cnp_issue_size)  # noqa: 501 isort:skip
                    and int(opp.cnp_issue_size) <= row.to_issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Range"
                    and row.from_issue_size * 10000000
                    <= int(opp.cnp_issue_size)  # noqa: 501 isort:skip
                    and int(opp.cnp_issue_size) <= row.to_issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities == 0
                ):
                    values = cost_rs_per(row, opp)

                if (
                    row.condition == "Less Than"
                    and float(opp.cnp_issue_size) < row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Less Than"
                    and float(opp.cnp_issue_size) < row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Greater than Equal to"
                    and float(opp.cnp_issue_size) >= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Greater than Equal to"
                    and float(opp.cnp_issue_size) >= row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Equal to"
                    and float(opp.cnp_issue_size) == row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Equal to"
                    and float(opp.cnp_issue_size) == row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Not Equal to"
                    and float(opp.cnp_issue_size) != row.issue_size * 10000000
                    and row.secured_or_unsecured == "Secured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)
                if (
                    row.condition == "Not Equal to"
                    and float(opp.cnp_issue_size) != row.issue_size * 10000000
                    and row.secured_or_unsecured == "Unsecured"
                    and no_of_securities != 0
                ):
                    values = cost_rs_per(row, opp)

    final_selling_initial_cost = values["initial"]
    final_selling_annual_cost = values["annual"]
    return {
        "annual": final_selling_annual_cost,
        "initial": final_selling_initial_cost,
    }  # noqa: 501 isort:skip


def cost_rs_per(row, opp):
    if row.rs_or_percent == "Rs in Lac":
        final_selling_initial_cost = row.initial_fee
        final_selling_annual_cost = row.annual_fee
    elif row.rs_or_percent == "Percentage":
        final_selling_initial_cost = (
            float(opp.cnp_issue_size) * row.initial_fee / 100
        ) * 10000000
        final_selling_annual_cost = (
            float(opp.cnp_issue_size) * row.annual_fee / 100
        ) * 10000000
    return {
        "annual": final_selling_annual_cost,
        "initial": final_selling_initial_cost,
    }  # noqa: 501 isort:skip


@frappe.whitelist()
def set_quotation_facility_amt(doc_name, facility_amt):
    doc = frappe.get_doc("Opportunity", doc_name)
    cnp_facility_amt = doc.cnp_facility_amt

    if cnp_facility_amt != facility_amt:
        doc.cnp_facility_amt = facility_amt
        doc.save()


def update_items_and_calculate_one_time_fee(doc, calculated_fees, product):
    if doc.cnp_tenor_days:
        doc.cnp_tenor_days = int(doc.cnp_tenor_days)
    else:
        doc.cnp_tenor_days = 0
    if doc.cnp_tenor_months:
        doc.cnp_tenor_months = int(doc.cnp_tenor_months)
    else:
        doc.cnp_tenor_months = 0
    if doc.cnp_tenor_years:
        doc.cnp_tenor_years = int(doc.cnp_tenor_years)
    else:
        doc.cnp_tenor_years = 0

    if (
        doc.cnp_tenor_years
        and (not doc.cnp_tenor_months)
        and (not doc.cnp_tenor_days)  # noqa: 501 isort:skip
    ):  # noqa: 501 isort:skip
        fee_actual = calculated_fees["annual_cost"] * doc.cnp_tenor_years
        fee_calculated = calculated_fees["annual_cost"] * doc.cnp_tenor_years
    else:
        no_of_days = (
            doc.cnp_tenor_days
            + doc.cnp_tenor_months * 30
            + doc.cnp_tenor_years * 365  # noqa: 501 isort:skip
        )
        fee_actual = (calculated_fees["annual_cost"] / 365) * no_of_days
        fee_calculated = (calculated_fees["annual_cost"] / 365) * no_of_days
    doc.items = []
    doc.append(
        "items",
        {
            "item_code": doc.cnp_product,
            "rate": 0,
            "qty": 1,
            "cnp_calculated_initial_fee": calculated_fees["initial_cost"],
            "cnp_actual_initial_fee": calculated_fees["initial_cost"],
            "cnp_calculated_annual_fee": calculated_fees["annual_cost"],
            "cnp_actual_annual_fee": calculated_fees["annual_cost"],
            "cnp_one_time_fee": fee_calculated,
            "cnp_one_time_fee_actual": fee_actual,
        },
    )
    doc.cnp_initial_fee = calculated_fees["initial_cost"]
    doc.cnp_annual_fee = calculated_fees["annual_cost"]
    doc.cnp_one_time_fee = fee_calculated
    calculate_quotation_product_fee(doc, product, calculated_fees)


@frappe.whitelist()
def cal_opportunity_product_fee(doc_name, product, doc=None):
    # delete all pricing policies for opportunity except selected product
    delete_pricing_policy(doc_name, product)
    # update Opportunity Item table on pricing policy change
    save_doc = False
    if not doc:
        doc = frappe.get_doc("Opportunity", doc_name)
        save_doc = True

    doc.with_items = 1

    calculated_fees = get_calculated_fees(doc, product)

    update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    if save_doc:
        doc.save()


def get_calculated_fees(doc, product):
    calculated_fees = {}

    product_upper = doc.cnp_product.upper()
    if product_upper == "STE":
        calculated_fees = cal_security_trustee_sell_fee(
            doc.cnp_product, doc.name
        )  # noqa
    elif product_upper == "DTE":
        calculated_fees = cal_debenture_trustee_sell_fee(
            doc.cnp_product, doc.name, doc.cnp_facility_amt
        )
    elif doc.cnp_product.upper() == "EA":
        calculated_fees = cal_escrow_agent_fee(doc.cnp_product, doc.name)
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "FA":
        calculated_fees = cal_facility_agent_fee(doc.cnp_product, doc.name)
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "AIF":
        calculated_fees = cal_alternate_investment_fund_fee(
            doc.cnp_product, doc.name, doc.cnp_facility_amt
        )
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "LAG":
        calculated_fees = cal_lenders_agent_fee(doc.cnp_product, doc.name)
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "INVIT":
        calculated_fees = cal_infrastructure_investment_fund_fee(
            doc.cnp_product, doc.name
        )
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "REIT":
        calculated_fees = cal_real_estate_investment_fee(
            doc.cnp_product, doc.name
        )  # noqa: 501 isort:skip
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "SEC":
        calculated_fees = cal_securitization_fee(doc.cnp_product, doc.name)
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif doc.cnp_product.upper() == "SA":
        calculated_fees = cal_security_agent_fee(doc.cnp_product, doc.name)
        update_items_and_calculate_one_time_fee(doc, calculated_fees, product)

    elif product_upper in (
        "CUSTA",
        "MAG",
        "ERA",
        "MTST",
        "CAG",
        "ESOP",
        "FTE",
        "SPT",
        "NDUA",
        "P2P",
        "SK",
        "EAG",
    ):
        calculated_fees = cal_common_products_fee(doc.cnp_product, doc.name)
    else:
        create_pricing_policy_if_not_exists(doc, product)
        doc.cnp_initial_fee = 0
        doc.cnp_annual_fee = 0
        doc.cnp_one_time_fee = 0

    return calculated_fees


def create_pricing_policy_if_not_exists(doc, product):
    if not frappe.db.exists("Canopi Pricing Policy", doc.name + "-" + product):
        pricing_policy = frappe.new_doc("Canopi Pricing Policy")
        pricing_policy.product_name = product
        pricing_policy.opportunity = doc.name
        pricing_policy.is_insertable = 1
        pricing_policy.save()

        doc.append(
            "items",
            {
                "item_code": doc.cnp_product,
                "rate": 0,
                "qty": 1,
                "cnp_calculated_initial_fee": 0,
                "cnp_actual_initial_fee": 0,
                "cnp_calculated_annual_fee": 0,
                "cnp_actual_annual_fee": 0,
                "cnp_one_time_fee": 0,
                "cnp_one_time_fee_actual": 0,
            },
        )


def calculate_quotation_product_fee(opportunity, product, calculated_fees):
    # update quotation item table on pricing policy change
    quotation_docs = get_quotation_docs(opportunity)

    for quot_doc in quotation_docs:
        quot = frappe.get_doc("Quotation", quot_doc.name)
        update_quotation_facility_amount(quot, opportunity)
        update_quotation_item_fees(quot, opportunity, product, calculated_fees)


def get_quotation_docs(opportunity):
    return get_doc_list(
        "Quotation",
        fields=('["name"]'),
        filters=[
            ["opportunity", "=", opportunity.name],
            ["workflow_state", "in", "Draft"],
        ],
    )


def update_quotation_facility_amount(quot, opportunity):
    quot.cnp_facility_amt = opportunity.cnp_facility_amt
    quot.save()


def update_fee_values(row, calculated_fees):
    row.cnp_calculated_initial_fee = calculated_fees["initial_cost"]
    row.cnp_actual_initial_fee = calculated_fees["initial_cost"]
    row.cnp_calculated_annual_fee = calculated_fees["annual_cost"]
    row.cnp_actual_annual_fee = calculated_fees["annual_cost"]


def update_selected_calculated_fee(row):
    if row.cnp_type_of_fee == "Initial Fee":
        row.rate = row.cnp_actual_initial_fee
        row.cnp_selected_calculated_fee = row.cnp_calculated_initial_fee
    elif row.cnp_type_of_fee == "Annual Fee":
        row.rate = row.cnp_actual_annual_fee
        row.cnp_selected_calculated_fee = row.cnp_calculated_annual_fee
    elif row.cnp_type_of_fee == "One Time Fee":
        row.rate = row.cnp_one_time_fee
        row.cnp_selected_calculated_fee = row.cnp_one_time_fee_actual


def update_quotation_item_fees(quot, opportunity, product, calculated_fees):
    for row in quot.items:
        if (
            row.item_code == product
            and quot.cnp_opportunity == opportunity.name  # noqa
        ):  # noqa
            update_fee_values(row, calculated_fees)

            (
                row.cnp_one_time_fee_actual,
                row.cnp_one_time_fee,
            ) = calculate_one_time_fee(row, opportunity)

            update_selected_calculated_fee(row)

            quot.save()


def calculate_one_time_fee(row, opportunity):
    years = opportunity.cnp_tenor_years
    months = opportunity.cnp_tenor_months
    days = opportunity.cnp_tenor_days

    if years and (not months) and (not days):
        one_time_fee_actual = row.cnp_calculated_annual_fee * years
        one_time_fee = row.cnp_calculated_annual_fee * years
    else:
        no_of_days = days + months * 30 + years * 365
        one_time_fee_actual = row.cnp_calculated_annual_fee / 365 * no_of_days
        one_time_fee = row.cnp_calculated_annual_fee / 365 * no_of_days

    return one_time_fee_actual, one_time_fee


@frappe.whitelist()
def get_years_months_days(opportunity):
    doc = frappe.get_doc("Opportunity", opportunity)
    years = doc.cnp_tenor_years
    months = doc.cnp_tenor_months
    days = doc.cnp_tenor_days
    return {"years": years, "months": months, "days": days}


def cal_excel_floor(value, factor):
    return math.floor(value / factor) * factor


@frappe.whitelist()
def download_consent_letter(doctype, docname):
    frappe.local.response.filename = "{name}-consent_letter.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    frappe.local.response.filecontent = get_consent_letter(doctype, docname)
    frappe.local.response.type = "pdf"


def get_consent_letter(doctype, docname):
    doc = frappe.get_doc(doctype, docname)
    html = frappe.get_print(doctype, docname, "Consent Letter")
    file = get_pdf(html)
    file = io.BytesIO(file)
    out = None
    w = IncrementalPdfFileWriter(file)
    # sign for authorised signatory on page 2
    if doc.authorised_signatory:
        out = sign_authorised_signatory(doc.authorised_signatory, w, 43, 165)

    return out.getbuffer() if out else get_pdf(html)


def sign_authorised_signatory(signatory_name, w, x1, y1):
    signatory = frappe.get_doc("ATSL Digital Signatures", signatory_name)
    if signatory.cnp_s3_key:
        x2 = x1 + 140
        y2 = y1 + 54
        file_path = get_pfx_from_s3(signatory.cnp_s3_key)
        signatory_signer = get_authorised_signer(signatory, file_path)

        append_signature_field(
            w,
            SigFieldSpec(
                sig_field_name=signatory.name,
                box=(x1, y1, x2, y2),
            ),
        )
        out = signers.sign_pdf(
            w,
            signers.PdfSignatureMetadata(field_name=signatory.name),
            signer=signatory_signer,
        )
        os.remove(file_path)

        return out


def get_authorised_signer(signatory, file_path):
    signer = signers.SimpleSigner.load_pkcs12(
        pfx_file=file_path,
        passphrase=(signatory.get_password("cnp_sign_secret")).encode(),
    )
    return signer


def get_signed_pdf(doctype, docname):
    doc = frappe.get_doc(doctype, docname)
    html = frappe.get_print(doctype, docname, "Offer Letter")
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)

    p = None
    if doc.cnp_name_of_officer_1:
        p = sign_officer(doc.cnp_name_of_officer_1, w, 43, 296, 1)

    if doc.cnp_name_of_officer_2:
        p = sign_officer(doc.cnp_name_of_officer_2, w, 353, 296, 1)

    return p.getbuffer() if p else get_pdf(html)


def sign_officer(officer_name, w, x1, y1, page):
    officer = frappe.get_doc("ATSL Digital Signatures", officer_name)
    if officer.cnp_s3_key:
        x2 = x1 + 140
        y2 = y1 + 54
        file_path = get_pfx_from_s3(officer.cnp_s3_key)
        officer_signer = get_officer_signer(officer, file_path)

        append_signature_field(
            w,
            SigFieldSpec(
                sig_field_name=officer.name,
                box=(x1, y1, x2, y2),
                on_page=page,
            ),
        )
        out = signers.sign_pdf(
            w,
            signers.PdfSignatureMetadata(field_name=officer.name),
            signer=officer_signer,
        )
        os.remove(file_path)

        return out


def get_officer_signer(officer, file_path):
    signer = signers.SimpleSigner.load_pkcs12(
        pfx_file=file_path,
        passphrase=(officer.get_password("cnp_sign_secret")).encode(),
    )
    return signer


@frappe.whitelist()
def download_signed_pdf(doctype, docname):
    frappe.local.response.filename = "{name}-signed.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    frappe.local.response.filecontent = get_signed_pdf(doctype, docname)
    frappe.local.response.type = "pdf"


@frappe.whitelist()
def download_proforma_invoice(doctype, docname, company_name):
    frappe.local.response.filename = "{name}-proforma-invoice.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    frappe.local.response.filecontent = get_signed_proforma(
        company_name, doctype, docname
    )
    frappe.local.response.type = "pdf"


def get_signed_proforma(company_name, doctype, docname):
    doc = frappe.get_doc("Company", company_name)

    html = frappe.get_print(doctype, docname, "Proforma Invoice")
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)
    out = None

    if doc.cnp_s3_key:
        x1 = 410
        y1 = 100
        x2 = x1 + 140
        y2 = y1 + 54
        file_path = get_pfx_from_s3(doc.cnp_s3_key)
        signer = signers.SimpleSigner.load_pkcs12(
            pfx_file=file_path,
            passphrase=(doc.get_password("cnp_sign_secret")).encode(),
        )
        if not signer:
            frappe.throw("ValueError: Invalid password")
        append_signature_field(
            w, SigFieldSpec(sig_field_name=doc.name, box=(x1, y1, x2, y2))
        )
        out = signers.sign_pdf(
            w, signers.PdfSignatureMetadata(field_name=doc.name), signer=signer
        )
        os.remove(file_path)
    return out.getbuffer() if out else get_pdf(html)


@frappe.whitelist()
def download_sales_proforma_invoice(doctype, docname, company_name):
    frappe.local.response.filename = "{name}-proforma-invoice.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    frappe.local.response.filecontent = get_sales_proforma_invoice(
        company_name, doctype, docname
    )
    frappe.local.response.type = "pdf"


def get_sales_proforma_invoice(company_name, doctype, docname):
    doc = frappe.get_doc("Company", company_name)
    html = frappe.get_print(doctype, docname, "Canopi Sales Proforma Invoice")
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)
    out = None

    if doc.cnp_s3_key:
        x1 = 410
        y1 = 100
        x2 = x1 + 140
        y2 = y1 + 54
        file_path = get_pfx_from_s3(doc.cnp_s3_key)
        signer = signers.SimpleSigner.load_pkcs12(
            pfx_file=file_path,
            passphrase=(doc.get_password("cnp_sign_secret")).encode(),
        )
        append_signature_field(
            w, SigFieldSpec(sig_field_name=doc.name, box=(x1, y1, x2, y2))
        )
        out = signers.sign_pdf(
            w, signers.PdfSignatureMetadata(field_name=doc.name), signer=signer
        )
        os.remove(file_path)
    return out.getbuffer() if out else get_pdf(html)


@frappe.whitelist()
def get_outstanding(doctype, docname, customer, quotation):
    outstanding = 0
    invoice_entry = frappe.get_all(
        "Sales Invoice",
        filters={
            "customer": customer,
            "cnp_from_quotation": quotation,
            "docstatus": 1,
            "name": ["!=", docname],
        },
        fields=["outstanding_amount"],
    )
    for invoice in invoice_entry:
        if outstanding:
            outstanding += invoice.outstanding_amount

    return outstanding


@frappe.whitelist()
def send_mail(
    recipients,
    subject,
    content,
    doctype,
    name,
    send_me_a_copy,
    attachments,
    read_receipt,
    attach_offer_letter,
    attach_proforma_invoice,
    sender=None,
    cc=[],
    bcc=[],
):
    attach = []
    recipients = split_emails(recipients)
    cc = split_emails(cc) if cc else []
    bcc = split_emails(bcc) if bcc else []
    attachments = json.loads(attachments)
    for file in attachments:
        doc = frappe.get_doc("File", file)
        attach.append({"fname": doc.file_name, "fcontent": doc.get_content()})

    if int(attach_offer_letter) == 1:
        doc = frappe.get_doc(doctype, name)
        doc.cnp_show_draft = 0
        doc.save()
        file = get_signed_pdf(doctype, name)
        attach.append(
            {
                "fname": "{name}-offer-letter-signed.pdf".format(
                    name=name.replace(" ", "-").replace("/", "-")
                ),
                "fcontent": file,
            }
        )
        doc.cnp_show_draft = 1
        doc.save()

    if int(attach_proforma_invoice) == 1:
        doc = frappe.get_doc(doctype, name)
        file = get_signed_proforma(doc.company, doctype, name)
        attach.append(
            {
                "fname": "{name}-proforma-signed.pdf".format(
                    name=name.replace(" ", "-").replace("/", "-")
                ),
                "fcontent": file,
            }
        )

    if int(send_me_a_copy) == 1:
        email = get_doc_list("Email Account", filters={"default_outgoing": 1})
        if email[0].email_id not in cc:
            cc.append(email[0].email_id)
    frappe.sendmail(
        sender=sender,
        recipients=recipients,
        subject=subject,
        content=content,
        attachments=attach,
        read_receipt=int(read_receipt),
        cc=cc,
        bcc=bcc,
        expose_recipients="header",
    )


@frappe.whitelist()
def send_mail_consentletter(
    recipients,
    subject,
    content,
    doctype,
    name,
    send_me_a_copy,
    attachments,
    read_receipt,
    attach_consent_letter,
    sender=None,
    cc=[],
    bcc=[],
):
    attach = []
    recipients = split_emails(recipients)
    cc = split_emails(cc) if cc else []
    bcc = split_emails(bcc) if bcc else []
    attachments = json.loads(attachments)
    for file in attachments:
        file_doc = frappe.get_doc("File", file)
        attach.append(
            {"fname": file_doc.file_name, "fcontent": file_doc.get_content()}
        )  # noqa: 501

    doc = frappe.get_doc(doctype, name)
    if doc.product_name.upper() == "DTE" and int(attach_consent_letter) == 1:
        doc.cnp_show_draft = 0
        doc.save()
        file = get_consent_letter(doctype, name)
        attach.append(
            {
                "fname": "{name}-consent-letter-signed.pdf".format(
                    name=name.replace(" ", "-").replace("/", "-")
                ),
                "fcontent": file,
            }
        )
        doc.cnp_show_draft = 1
        doc.save()

    if int(send_me_a_copy) == 1:
        email = get_doc_list("Email Account", filters={"default_outgoing": 1})
        if email[0].email_id not in cc:
            cc.append(email[0].email_id)
    frappe.sendmail(
        sender=sender,
        recipients=recipients,
        subject=subject,
        content=content,
        attachments=attach,
        read_receipt=int(read_receipt),
        cc=cc,
        bcc=bcc,
        expose_recipients="header",
    )


@frappe.whitelist()
def get_roles():
    return frappe.get_roles(frappe.session.user)


@frappe.whitelist()
def delete_pricing_policy(doc_name, product):
    filters = [["opportunity", "=", doc_name], ["product_name", "!=", product]]
    pricing_list = get_doc_list(
        "Canopi Pricing Policy",
        filters=filters,
    )
    for pricing in pricing_list:
        frappe.delete_doc("Canopi Pricing Policy", pricing.name)


def validate_s3_settings():
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    if (
        not settings_doc.s3_endpoint_url
        or not settings_doc.s3_region_name
        or not settings_doc.s3_bucket
        or not settings_doc.aws_access_key_id
        or not settings_doc.get_password("aws_secret_access_key")
    ):
        frappe.throw(
            "Please enter all mandatory fields for S3 Details in Trusteeship Platform Settings"  # noqa
        )


def get_s3_client(settings_doc):
    aws_secret_access_key = settings_doc.get_password("aws_secret_access_key")
    return boto3.client(
        service_name="s3",
        endpoint_url=settings_doc.s3_endpoint_url,
        region_name=settings_doc.s3_region_name,
        aws_access_key_id=settings_doc.aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
    )


@frappe.whitelist()
def send_file_to_s3(file, file_name, doctype, docname):
    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    doc = frappe.get_doc(doctype, docname)

    key = (
        cstr(frappe.local.site)
        + "/"
        + doctype
        + "/"
        + doc.name
        + "/"
        + file_name  # noqa: 501 isort:skip
    )  # noqa: 501 isort:skip

    file_data = file["file"].read()

    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        doc.cnp_s3_key = key
        doc.save()


def upload_file_to_s3(settings, key, file):
    s3_client = get_s3_client(settings)

    # check if bucket exists
    try:
        s3_client.head_bucket(Bucket=settings.s3_bucket)
    except botocore.exceptions.ClientError as error:
        if (
            error.response["Error"]["Code"] == "404"
            and error.response["Error"]["Message"] == "Not Found"
        ):
            frappe.throw(
                ("Bucket {} not found").format(frappe.bold(settings.s3_bucket))
            )
        else:
            frappe.throw(error.response["Error"]["Message"])

    client_action = "put_object"  # options: 'get_object' || 'put_object'

    url = generate_presigned_url(
        s3_client,
        client_action,
        {"Bucket": settings.s3_bucket, "Key": key},
        1000,  # noqa: 501 isort:skip
    )
    response = process_response(requests.put(url, data=file))
    return response


def generate_presigned_url(
    s3_client, client_method, method_parameters, expires_in
):  # noqa: 501 isort:skip
    """
    Generate a presigned Amazon S3 URL that can be used to perform an action.

    :param s3_client: A Boto3 Amazon S3 client.
    :param client_method: The name of the client method that the URL performs.
    :param method_parameters: The parameters of the specified client method.
    :param expires_in: The number of seconds the presigned URL is valid for.
    :return: The presigned URL.
    """
    try:
        url = s3_client.generate_presigned_url(
            ClientMethod=client_method,
            Params=method_parameters,
            ExpiresIn=expires_in,  # noqa: 501 isort:skip
        )
    except ClientError:
        frappe.throw(
            "Couldn't get a presigned URL for client method '%s'.",
            client_method,  # noqa: 501 isort:skip
        )
        raise
    return url


@frappe.whitelist()
def delete_pfx(docname, doctype):
    doc = frappe.get_doc(doctype, docname)
    if doc.cnp_s3_key:
        delete_from_s3(doc.cnp_s3_key)
    else:
        frappe.throw("S3 key not found.")
    doc.cnp_s3_key = ""
    doc.cnp_sign_secret = ""
    doc.save()


@frappe.whitelist()
def delete_from_s3(key):
    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)
    s3_client.delete_object(Bucket=settings_doc.s3_bucket, Key=key)


@frappe.whitelist()
def delete_multiple_from_s3(keys):
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)
    if type(keys) == str:
        keys = json.loads(keys)
    keys = map(lambda x: {"Key": x}, keys)
    s3_client.delete_objects(
        Bucket=settings_doc.s3_bucket, Delete={"Objects": list(keys)}
    )


def get_pfx_from_s3(key):
    validate_s3_settings()

    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)

    try:
        path = "/tmp/" + ntpath.basename(key)
        s3_client.download_file(settings_doc.s3_bucket, key, path)
        return path
    except botocore.exceptions.ClientError:
        frappe.throw("Failed to get signature file.", title="Error")


@frappe.whitelist()
def make_operations_flow_for_quotation(source_name, target_doc=None):
    def set_missing_values(source, target):
        target.mandate_name = source.customer_name
        target.person_name = source.contact_display
        if source.cnp_opportunity:
            target.opportunity = source.cnp_opportunity
            opportunity = frappe.get_doc("Opportunity", source.cnp_opportunity)
            target.product_name = opportunity.cnp_product
            for element in opportunity.cnp_security_type:
                target.append(
                    "security_type",
                    {
                        "existing_security_type": element.type_of_security,
                        "change_in_security": "No",
                    },
                )

    doclist = get_mapped_doc(
        "Quotation",
        source_name,
        {
            "Quotation": {
                "doctype": "ATSL Mandate List",
                "validation": {"docstatus": ["=", 1]},
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


@frappe.whitelist()
def make_operations_flow_for_opportunity(source_name, target_doc=None):
    def set_missing_values(source, target):
        target.mandate_name = source.customer_name
        if source.name:
            target.opportunity = source.name
            target.product_name = source.cnp_product
            state = "Draft,Renegotiate,Submitted,Approved,Accepted by Client"
            quotations = frappe.get_list(
                "Quotation",
                filters=[
                    ["opportunity", "in", source.name],
                    [
                        "workflow_state",
                        "in",
                        state,
                    ],
                ],
            )
            if quotations:
                target.quotation = quotations[0].get("name")
            for element in source.cnp_security_type:
                target.append(
                    "security_type",
                    {
                        "existing_security_type": element.type_of_security,
                        "change_in_security": "No",
                    },
                )

    doclist = get_mapped_doc(
        "Opportunity",
        source_name,
        {
            "Opportunity": {
                "doctype": "ATSL Mandate List",
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


@frappe.whitelist()
def submitDoc(doc_name, doc_type):
    doc = frappe.get_doc(doc_type, doc_name)
    doc.submit()
    frappe.db.set_value(doc_type, doc_name, "workflow_state", "Submitted")
    frappe.db.commit()


@frappe.whitelist()
def send_approval_mail(doc_name, doc_type, action):
    url = get_url()
    file = get_signed_pdf(doc_type, doc_name)
    my_attachments = prepare_attachments(doc_name, file)
    doc = frappe.get_doc(doc_type, doc_name)
    token = frappe.generate_hash()
    token_expiry = datetime.now() + timedelta(days=90)
    frappe.db.set_value(doc_type, doc_name, "cnp_email_token", token)
    frappe.db.set_value(
        doc_type, doc_name, "cnp_email_token_expiry", token_expiry
    )  # noqa: 501
    recipients = get_quotation_approval_recipients(doc)

    if recipients:
        frappe.db.set_value(doc_type, doc_name, "workflow_state", action)
        frappe.db.commit()
    content1 = prepare_content1(doc)
    content2 = prepare_content2(url, doc)
    content3 = prepare_content3(url, doc_name)
    cont = prepare_styles()

    frappe.sendmail(
        recipients=recipients,
        subject="Test for deviation ",
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )


def prepare_attachments(doc_name, file):
    return [
        {
            "fname": "{doc_name}.pdf".format(
                doc_name=doc_name.replace(" ", "-").replace("/", "-")
            ),
            "fcontent": file,
        }
    ]


def prepare_content1(quot_doc):
    return f"""
    <h2>This is a demo template</h2>
    <p>Customer :{quot_doc.customer_name}</p>
    <p>Opportunity :{quot_doc.cnp_opportunity}</p>
    """


def prepare_content3(url, doc_name):
    doc = frappe.get_doc("Quotation", doc_name)
    return f"""
        <p><a href= "{url}/api/method/trusteeship_platform.custom_methods.email_action?doctype=Quotation&docname={doc.name}&action=Approved&email_token={doc.cnp_email_token}" class="button">Approve</a>
        <a href= "{url}/api/method/trusteeship_platform.custom_methods.email_action?doctype=Quotation&docname={doc.name}&action=Rejected&email_token={doc.cnp_email_token}" class="button_1">Reject</a></p>"""  # noqa


def prepare_styles():
    return """
        <style>
            .button {
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }
            .button_1 {
                background-color: red;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }
        </style>
    """


def get_quotation_approval_recipients(doc):
    approval_settings = frappe.get_single("Approval Settings")
    recipients = []
    if doc.cnp_send_for_approval == 1:
        recipients.extend(get_recipients_by_role("BCG Head"))

    else:
        for row in approval_settings.deviation_conditions:
            if (
                row.condition == "Less than"
                and doc.cnp_deviation_change < row.deviation
            ):
                recipients.extend(get_recipients_by_role(row.role))
            elif (
                row.condition == "Range"
                and row.from_dev <= doc.cnp_deviation_change
                and row.to_dev >= doc.cnp_deviation_change
            ):
                recipients.extend(get_recipients_by_role(row.role))

    return recipients


def get_recipients_by_role(role):
    recipients = []
    users = frappe.get_all("User")
    for usr in users:
        user_roles = frappe.get_roles(usr["name"])
        if role in user_roles:
            user_email = frappe.get_doc("User", usr["name"]).email
            recipients.append(user_email)
    return recipients


def prepare_content2(url, quot_doc):
    return (
        f""" <p><a href="{url}/app/quotation/{quot_doc.name}">"""
        f"""{quot_doc.name} </a></p>"""
    )


@frappe.whitelist()
def send_approval_mail_opportunity(doc_name, doc_type, action):
    url = get_url()
    opp_doc = frappe.get_doc(doc_type, doc_name)
    content1 = f"""
        <h2>Operations Flow initiation request</h2>
        <p>Opportunity :{opp_doc.name}</p>"""
    content3 = f"""
        <p><a href= "{url}/api/method/trusteeship_platform.custom_methods.email_action_opportunity?doctype=Opportunity&docname={doc_name}&action=Approved" class="button">Approve</a>
        <a href= "{url}/api/method/trusteeship_platform.custom_methods.email_action_opportunity?doctype=Opportunity&docname={doc_name}&action=Rejected" class="button_1">Reject</a></p>"""  # noqa: 501 isort:skip
    cont = """
        <style>
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        .button_1 {
            background-color: red;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
        </style>
    """
    recipient = []
    frappe.db.set_value(doc_type, doc_name, "cnp_send_for_approval", 1)
    frappe.db.set_value(
        doc_type, doc_name, "cnp_get_approver_sender_user", frappe.session.user
    )
    frappe.db.set_value(doc_type, doc_name, "workflow_state", action)
    frappe.db.commit()

    Users = frappe.get_all("User")
    for usr in Users:
        role = frappe.get_roles(usr["name"])
        if "BCG Head" in role:
            eid = frappe.get_doc("User", usr["name"])
            recipient.append(eid.email)
    content2 = (
        f""" <p><a href="{url}/app/opportunity/{opp_doc.name}">"""
        f"""{opp_doc.name} </a></p>"""
    )
    frappe.sendmail(
        recipients=recipient,
        subject="Operations Flow initiation request",
        message=cont + content1 + content2 + content3,
    )


@frappe.whitelist(allow_guest=True)
def set_opp_rejected(doctype, docname, action, rejected_field_name):
    if frappe.db.get_value(doctype, docname, "cnp_is_approved_rejected") == 0:
        frappe.db.set_value(doctype, docname, "cnp_is_approved_rejected", 1)
        frappe.db.set_value(doctype, docname, "workflow_state", action)
        frappe.db.set_value(doctype, docname, rejected_field_name, 1)


@frappe.whitelist(allow_guest=True)
def set_opp_approved(doctype, docname, action):
    if frappe.db.get_value(doctype, docname, "cnp_is_approved_rejected") == 0:
        frappe.db.set_value(doctype, docname, "cnp_is_approved_rejected", 1)
        frappe.db.set_value(doctype, docname, "workflow_state", action)


@frappe.whitelist(allow_guest=True)
def email_action_opportunity(doctype, docname, action, redirect=True):
    rejected_field_name = "cnp_is_reject_reason_commented"
    if action == "Approved":
        set_opp_approved(doctype, docname, action)
    if action == "Rejected":
        set_opp_rejected(doctype, docname, action, rejected_field_name)

    url = get_url()
    notification = frappe.new_doc("Notification Log")
    notification.subject = (
        "Operations Flow initiation request has been "
        + action
        + " for "
        + docname  # noqa
    )
    notification.email_content = (
        "Operations Flow initiation request has been "
        + action
        + " for <a href="
        + url
        + "/app/opportunity/"
        + docname
        + ">"
        + docname
        + "</a>"
    )
    notification.for_user = frappe.db.get_value(
        doctype, docname, "cnp_get_approver_sender_user"
    )
    notification.type = "Alert"
    notification.insert()
    if action == "Approved":
        users_ops_serv_maker = frappe.get_all(
            "User",
            filters=[["Has Role", "role", "=", "Ops & Serv Maker"]],
            fields=["email"],
        )
        for user in users_ops_serv_maker:
            try:
                assign_to.add(
                    {
                        "assign_to": [user.email],
                        "doctype": "Opportunity",
                        "name": docname,
                        "description": "",
                        "priority": "High",
                        "notify": 1,
                    }
                )
            except assign_to.DuplicateToDoError:
                frappe.message_log.pop()
                pass
            break
    frappe.db.commit()
    if redirect is True:
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = url + "/app/opportunity/" + docname


@frappe.whitelist(allow_guest=True)
def set_approved_reject_action(doctype, docname, action):
    frappe.db.set_value(doctype, docname, "workflow_state", action)
    frappe.db.set_value(doctype, docname, "docstatus", 1)


@frappe.whitelist()
def email_action(doctype, docname, action, redirect=True, email_token=""):
    if action == "Accepted By Client":
        handle_accepted_by_client_action(doctype, docname, action)
        return

    validate_email_token(action, docname, email_token)
    frappe.db.set_value(doctype, docname, "cnp_email_token", "")
    frappe.db.set_value(doctype, docname, "cnp_email_token_expiry", None)  # noqa: 501

    if action == "Approved":
        set_approved_reject_action(doctype, docname, action)

    if action == "Rejected":
        handle_rejected_action(doctype, docname, action)

    frappe.db.commit()
    if redirect is True:
        url = get_url()
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = url + "/app/quotation/" + docname


def validate_email_token(action, docname, email_token):
    doc = frappe.get_doc("Quotation", docname)
    if action in ["Approved", "Rejected"]:  # noqa: 501  # noqa: 501
        validate_quotation_approval_user(docname)
        if (
            doc.cnp_email_token != email_token
            or doc.cnp_email_token_expiry <= datetime.now()  # noqa: 501
        ):  # noqa: 501
            frappe.throw("The link you followed has expired")


def handle_rejected_action(doctype, docname, action):
    frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(
        doctype, docname, "cnp_is_reject_reason_commented", 1
    )  # noqa: 501


def get_field_value(doctype, docname, fieldname):
    return frappe.db.get_value(doctype, docname, fieldname)


def handle_accepted_by_client_action(doctype, docname, action):
    frappe.db.set_value(doctype, docname, "workflow_state", action)
    frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(doctype, docname, "cnp_show_draft", 0)
    users_ops_serv_maker = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Ops & Serv Maker"],
            ["User", "name", "!=", "Administrator"],
        ],
        fields=["email"],
    )
    users_accounts_user = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Accounts User"],
            ["User", "name", "!=", "Administrator"],
        ],
        fields=["email"],
    )
    user_name = ""
    for user in users_ops_serv_maker:
        try:
            user_name = user.email
            assign_to.add(
                {
                    "assign_to": [user.email],
                    "doctype": "Quotation",
                    "name": docname,
                    "description": "",
                    "priority": "High",
                    "notify": 1,
                }
            )
        except assign_to.DuplicateToDoError:
            frappe.message_log.pop()
            pass
        break
    for user in users_accounts_user:
        if user_name != user.email:
            try:
                assign_to.add(
                    {
                        "assign_to": [user.email],
                        "doctype": "Quotation",
                        "name": docname,
                        "description": "",
                        "priority": "High",
                        "notify": 1,
                    }
                )
            except assign_to.DuplicateToDoError:
                frappe.message_log.pop()
                pass

    quoation = frappe.get_doc(doctype, docname)
    opportunity = frappe.get_doc("Opportunity", quoation.cnp_opportunity)
    if opportunity.opportunity_from == "Lead":
        lead = frappe.get_doc("Lead", opportunity.party_name)
        customer_name = lead.lead_name
    else:
        customer = frappe.get_doc("Customer", opportunity.cnp_customer_code)
        customer_name = customer.customer_name

    if opportunity.cnp_cl_code:
        frappe.db.set_value(
            "Quotation", docname, "cnp_cl_code", opportunity.cnp_cl_code
        )
        frappe.db.commit()

    if not quoation.cnp_cl_code and not opportunity.cnp_cl_code:
        ops_doc = frappe.new_doc("ATSL Mandate List")
        ops_doc.opportunity = opportunity.name
        ops_doc.quotation = docname
        ops_doc.company_name = customer_name
        ops_doc.address = quoation.customer_address
        ops_doc.address_details = quoation.address_display
        ops_doc.product_name = opportunity.cnp_product
        ops_doc.mandate_name = customer_name
        ops_doc.cnp_customer_code = opportunity.cnp_customer_code
        ops_doc.person_name = customer_name

        if opportunity.cnp_security_type:
            for element in opportunity.cnp_security_type:
                ops_doc.append(
                    "security_type",
                    {
                        "existing_security_type": element.type_of_security,
                        "change_in_security": "No",
                    },
                )

        if (
            opportunity.cnp_product.upper() == "DTE"
            or opportunity.cnp_product.upper() == "STE"
            or opportunity.cnp_product.upper() == "EA"
            or opportunity.cnp_product.upper() == "REIT"
            or opportunity.cnp_product.upper() == "INVIT"
        ):
            if opportunity.cnp_product.upper() == "DTE":
                full_name = "Debenture Trustee"
                full_name_plural = ["Debenture Trustees", "Debentures Trustee"]
                first_name = "Debenture"
                first_name_plural = "Debentures"
            elif opportunity.cnp_product.upper() == "STE":
                full_name = "Security Trustee"
                full_name_plural = ["Security Trustees", "Securities Trustee"]
                first_name = "Security"
                first_name_plural = "Securities"

            elif opportunity.cnp_product.upper() == "EA":
                full_name = "Escrow"
                full_name_plural = ["Escrow", "Escrow"]
                first_name = "Escrow"
                first_name_plural = "Escrow"

            elif opportunity.cnp_product.upper() == "REIT":
                full_name = "REIT"
                full_name_plural = ["REIT", "REIT"]
                first_name = "REIT"
                first_name_plural = "REIT"

            elif opportunity.cnp_product.upper() == "INVIT":
                full_name = "INVIT"
                full_name_plural = ["INVIT", "INVIT"]
                first_name = "INVIT"
                first_name_plural = "INVIT"

            letter_body = (
                "We, Axis Trustee Services Limited, hereby give our consent to act as the "  # noqa: 501 isort:skip
                + full_name
                + " for the above mentioned issue of "
                + first_name_plural
                + " having a tenure of more than one year and are agreeable to the inclusion of our name as "  # noqa: 501 isort:skip
                + full_name
                + " in the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum and/or application to be made to the Stock Exchange for the listing of the said "  # noqa: 501 isort:skip
                + first_name_plural
                + ".\n\nAxis Trustee Services Limited (ATSL) consenting to act as "  # noqa: 501 isort:skip
                + full_name_plural[0]
                + " is purely its business decision and not an indication on the Issuer Company's standing or on the "  # noqa: 501 isort:skip
                + first_name
                + " Issue. By consenting to act as "
                + full_name_plural[0]
                + ", ATSL does not make nor deems to have made any representation on the Issuer Company, its Operations, the details and projections about the Issuer Company or the "  # noqa: 501 isort:skip
                + first_name_plural
                + " under Offer made in the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum / Offer Document. Applicants / Investors are advised to read carefully the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum / Offer Document and make their own enquiry, carry out due diligence and analysis about the Issuer Company, its performance and profitability and details in the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum / Offer Document before taking their investment decision. ATSL shall not be responsible for the investment decision and its consequence.\n\nWe also confirm that we are not disqualified to be appointed as "  # noqa: 501 isort:skip
                + full_name_plural[1]
                + " within the meaning of Rule 18(2)(c) of the Companies (Share Capital and "  # noqa: 501 isort:skip
                + first_name_plural
                + ") Rules, 2014."
            )
            ops_doc.letter_body = letter_body

        ops_doc.save()


@frappe.whitelist()
def set_fields_after_approve_reject(doctype, docname, action):
    frappe.db.set_value(doctype, docname, "cnp_is_approved_rejected", 1)
    frappe.db.set_value(doctype, docname, "workflow_state", action)
    frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(doctype, docname, "cnp_is_reject_reason_commented", 1)


@frappe.whitelist()
def add_rejection_comment(doctype, docname, comment, email_token):
    validate_quotation_approval_user(docname)
    doc = frappe.get_doc(doctype, docname)
    if (
        doc.cnp_email_token != email_token
        or doc.cnp_email_token_expiry <= datetime.now()  # noqa: 501
    ):  # noqa: 501
        frappe.throw("The link you followed has expired")
    frappe.db.set_value(doctype, docname, "cnp_email_token", "")
    frappe.db.set_value(doctype, docname, "cnp_email_token_expiry", None)  # noqa: 501
    frappe.db.set_value(doctype, docname, "cnp_is_reject_reason_commented", 0)
    frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(doctype, docname, "workflow_state", "Rejected")
    return add_comment(
        reference_doctype=doctype,
        reference_name=docname,
        content=comment,
        comment_email=frappe.session.user,
        comment_by=frappe.session.user_fullname,
    )


def validate_quotation_approval_user(docname):
    doc = frappe.get_doc("Quotation", docname)
    recipients = get_quotation_approval_recipients(doc)
    if (
        frappe.session.user == "Administrator"
        or frappe.session.user in recipients  # noqa: 501
    ):
        return

    frappe.throw("You are not allowed to perform this action")


@frappe.whitelist()
def add_renegotiation_comment(doctype, docname, comment):
    add_comment(
        reference_doctype=doctype,
        reference_name=docname,
        content=comment,
        comment_email=frappe.session.user,
        comment_by=frappe.session.user_fullname,
    )
    old_doc = frappe.get_doc("Quotation", docname)
    new_doc = frappe.copy_doc(old_doc)
    new_doc.workflow_state = "Draft"
    new_doc.docstatus = 0
    new_doc.amended_from = docname
    new_doc.insert()
    frappe.db.set_value(doctype, docname, "workflow_state", "Cancelled")
    frappe.db.set_value(doctype, docname, "docstatus", 2)
    frappe.db.commit()
    return new_doc.name


@frappe.whitelist()
def make_sales_invoice(source_name, target_doc=None, ignore_permissions=False):
    def set_missing_values(source, target):
        from erpnext.selling.doctype.quotation import quotation

        sales_invoice_exists = frappe.db.exists(
            "Sales Invoice",
            {"cnp_from_quotation": source.name, "docstatus": ["!=", 2]},
        )
        if sales_invoice_exists:
            frappe.throw("Sales Invoice already created for this Quotation...")
        customer = quotation._make_customer(source_name, ignore_permissions)
        target.customer = customer.name
        target.cnp_from_quotation = source.name
        target.cost_center = source.cnp_cost_center
        target.cnp_cl_code = source.cnp_cl_code
        target.flags.ignore_permissions = ignore_permissions
        target.run_method("set_missing_values")
        target.run_method("calculate_taxes_and_totals")
        target.due_date = datetime.today() + timedelta(days=45)
        fiscal_doc_list = get_doc_list(
            "Fiscal Year", fields=('["name"]'), filters=[]
        )  # noqa: 501 isort:skip
        for fiscal in fiscal_doc_list:
            fiscal_doc = frappe.get_doc("Fiscal Year", fiscal)

        oppo_doc = frappe.get_doc("Opportunity", source.cnp_opportunity)
        total_tenor_months = (
            oppo_doc.cnp_tenor_years * 12
            + oppo_doc.cnp_tenor_months
            + (1 if oppo_doc.cnp_tenor_days else 0)
        )
        target.payment_terms_template = source.payment_terms_template
        target.cnp_months_pending = (
            total_tenor_months
            - relativedelta.relativedelta(
                fiscal_doc.year_end_date, datetime.today()
            ).months
            + 1
        )
        # target.taxes_and_charges=source.taxes_and_charges
        # target.run_method("calculate_taxes_and_totals")
        # target.taxes=source.taxes

    def update_item(source, target, source_parent):
        if target.cnp_type_of_fee == "Annual Fee":
            res = calculate_tenor_fee(source_parent, source.cnp_frequency)
            target.rate = res["fees"]
            target.amount = res["fees"]

    doclist = get_mapped_doc(
        "Quotation",
        source_name,
        {
            "Quotation": {
                "doctype": "Sales Invoice",
                "validation": {"docstatus": ["=", 1]},
            },
            "Quotation Item": {
                "doctype": "Sales Invoice Item",
                "field_map": {
                    "parent": "prevdoc_docname",
                    "parenttype": "prevdoc_doctype",
                },
                "postprocess": update_item,
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


def calculate_tenor_fee(quot, frequency):
    doc = frappe.get_doc("Quotation", quot.name)
    fiscal_doc_list = get_doc_list(
        "Fiscal Year", fields=('["name"]'), filters=[]
    )  # noqa: 501 isort:skip
    for fiscal in fiscal_doc_list:
        fiscal_doc = frappe.get_doc("Fiscal Year", fiscal)

        if (
            fiscal_doc.year_start_date <= date.today()
            and fiscal_doc.year_end_date >= date.today()
        ):
            for item in doc.items:
                if item.cnp_type_of_fee == "Annual Fee":
                    year_end_date = fiscal_doc.year_end_date
                    timedelta_date = timedelta(days=1)
                    if item.cnp_frequency == "Y":
                        item.cnp_next_fee_date = year_end_date + timedelta_date
                    if item.cnp_frequency == "H":
                        half_year = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=6)
                        )
                        now = date.today()
                        if now < half_year:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, half_year)
                                )
                            )
                        else:
                            item.cnp_next_fee_date = (
                                fiscal_doc.year_end_date + timedelta(days=1)
                            )
                    if item.cnp_frequency == "Q":
                        quarter_1 = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=3)
                        )
                        quarter_2 = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=6)
                        )
                        quarter_3 = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=9)
                        )
                        now = date.today()
                        if date.today() < quarter_1:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, quarter_1)
                                )
                            )
                        elif date.today() < quarter_2:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, quarter_2)
                                )
                            )
                        elif date.today() < quarter_3:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, quarter_3)
                                )
                            )
                        else:
                            item.cnp_next_fee_date = (
                                fiscal_doc.year_end_date + timedelta(days=1)
                            )
                    if item.cnp_frequency == "M":
                        item.cnp_next_fee_date = (
                            date.today()
                            + relativedelta.relativedelta(
                                months=1
                            )  # noqa: 501 isort:skip
                        )
                    fee = (
                        item.rate
                        / 12
                        * date_diff_in_months(
                            date.today(), item.cnp_next_fee_date
                        )  # noqa: 501 isort:skip
                    )
        doc.save()
    return {"fees": fee}


def date_diff_in_months(start_date, end_date):
    return (
        end_date.month
        - start_date.month
        + 12 * (end_date.year - start_date.year)  # noqa: 501 isort:skip
    )


@frappe.whitelist()
def pre_execution_and_exception_tracking_s3_upload():
    validate_s3_settings()
    file = frappe.request.files
    file_name = frappe.request.form.get("file_name")
    row = frappe.request.form.get("row")
    doctype = frappe.request.form.get("doctype")
    docname = frappe.request.form.get("docname")
    assetName = frappe.request.form.get("assetName")
    detailsKey = frappe.request.form.get("detailsKey")
    row = json.loads(row)
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    doc = frappe.get_doc(doctype, docname)
    defaults = frappe.defaults.get_defaults()
    timezone = pytz.timezone(defaults.get("time_zone"))

    key = f"{cstr(frappe.local.site)}/{doctype.replace('Canopi ', '')}/{doc.name}/{'Exception Tracking/' if doctype == 'Canopi Post Execution Checklist' else ''}{assetName.replace('_', ' ').title()}/{row['document_code']}/{str(datetime.now(tz=timezone))}/{row['file_name']}"  # noqa
    file_data = file["file"].read()
    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        prefix_key = f"{cstr(frappe.local.site)}/{doctype.replace('Canopi ', '')}/{doc.name}/{'Exception Tracking/' if doctype == 'Canopi Post Execution Checklist' else ''}{assetName.replace('_', ' ').title()}/{row['document_code']}"  # noqa
        delete_old_s3_files(prefix_key)
        row["s3_key"] = key
        row["file_name"] = file_name
        row["upload_date"] = str(datetime.now().date())
    details = json.loads(doc.get(detailsKey))

    details[assetName] = [
        row if item["document_code"] == row["document_code"] else item
        for item in details[assetName]
    ]
    frappe.db.set_value(doctype, docname, detailsKey, json.dumps(details))
    frappe.db.commit()


def delete_old_s3_files(key_prefix):
    file_lists = get_s3_folders(key_prefix)

    if len(file_lists["Contents"]) > 5:
        # sort object in asc order of LastModified
        file_lists = sorted(
            file_lists["Contents"],
            key=lambda d: d["LastModified"],
            reverse=False,  # noqa: 501 isort:skip
        )

        # remove latest five keys
        keys = file_lists[: len(file_lists) - 5]
        keys = map(lambda x: x["Key"], keys)
        delete_multiple_from_s3(keys)


def get_s3_folders(key_prefix=None):
    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)
    response = s3_client.list_objects_v2(
        Bucket=settings_doc.s3_bucket, Prefix=key_prefix
    )
    return response


@frappe.whitelist()
def post_execution_s3_upload():
    validate_s3_settings()
    file = frappe.request.files
    file_name = frappe.request.form.get("file_name")
    row = frappe.request.form.get("row")
    doctype = frappe.request.form.get("doctype")
    docname = frappe.request.form.get("docname")
    assetName = frappe.request.form.get("assetName")
    detailsKey = frappe.request.form.get("detailsKey")
    index = int(frappe.request.form.get("index"))
    row = json.loads(row)
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    doc = frappe.get_doc(doctype, docname)
    doc_asset_details = json.loads(doc.get(detailsKey))
    defaults = frappe.defaults.get_defaults()
    timezone = pytz.timezone(defaults.get("time_zone"))
    keys = []
    i = 0
    key = (
        cstr(frappe.local.site)
        + "/"
        + doctype.replace("Canopi ", "")
        + "/"
        + doc.name
        + "/"
        + assetName.replace("_", " ").title()
        + "/"
        + row["document_code"]
        + "/"
        + str(datetime.now(tz=timezone))
        + "/"
        + file_name
    )
    keys.insert(i, key)
    file_names = []
    file_data = file["file"].read()
    file_names.insert(i, file_name)
    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        if not doc_asset_details[assetName][index].get("file_name"):
            doc_asset_details[assetName][index]["file_name"] = []
        if len(doc_asset_details[assetName][index].get("file_name")) != 0:
            old_file_names = doc_asset_details.get(assetName)[index].get(
                "file_name"
            )  # noqa: 501 isort:skip
            row["file_name"] = old_file_names + file_names
            old_s3_keys = doc_asset_details.get(assetName)[index].get(
                "s3_key"
            )  # noqa: 501 isort:skip
            row["s3_key"] = old_s3_keys + keys
        else:
            row["file_name"] = file_names
            row["s3_key"] = keys
        row["upload_date"] = str(datetime.now().date())
        doc_asset_details[assetName][index] = row
        details = json.loads(doc.get(detailsKey))
        details[assetName] = doc_asset_details[assetName]
        frappe.db.set_value(doctype, docname, detailsKey, json.dumps(details))
        frappe.db.commit()


@frappe.whitelist()
def get_s3_key(keys, doc_asset_details, asset_name, index):
    s3_key = keys + doc_asset_details.get(asset_name)[index].get("s3_key")
    return s3_key


@frappe.whitelist()
def upload_standard_template(child_table, file, docname, key, index):
    validate_s3_settings()

    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    doc = frappe.get_doc("Item", docname)
    key = cstr(frappe.local.site) + "/" + key

    file_data = base64.b64decode(file)

    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        if child_table == "Pre-Execution Checklist":
            doc.cnp_pre_execution_checklist[
                int(index) - 1
            ].standard_template_s3_key = key  # noqa: 501 isort:skip
        if child_table == "Common Documents Checklist":
            doc.cnp_common_documents_checklist_table[
                int(index) - 1
            ].standard_template_s3_key = key  # noqa: 501 isort:skip
        doc.save()


@frappe.whitelist()
def download_s3_file(key, download_type="download"):
    if download_type not in ["download", "pdf"]:
        frappe.throw("Type must be either <b>download</b> or <b>pdf</b>")

    validate_s3_settings()

    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)

    basename = os.path.basename(key)
    path = "/tmp/" + basename
    try:
        s3_client.download_file(
            Bucket=settings_doc.s3_bucket, Key=key, Filename=path
        )  # noqa: 501 isort:skip
    except botocore.exceptions.ClientError:
        frappe.throw("Failed to download file.", title="Error")

    file_bytes = None
    with open(path, "rb") as data:
        file_bytes = io.BytesIO(data.read())

    os.remove(path)
    frappe.local.response.filename = basename
    frappe.local.response.filecontent = file_bytes.getbuffer()
    frappe.local.response.type = download_type


@frappe.whitelist()
def documentation_s3_upload():
    validate_s3_settings()
    file = frappe.request.files
    row_name = frappe.request.form.get("row_name")
    doctype = frappe.request.form.get("doctype")
    docname = frappe.request.form.get("docname")
    file_name = frappe.request.form.get("file_name")
    settings_doc = frappe.get_single("Trusteeship Platform Settings")

    timezone = pytz.timezone(frappe.defaults.get_defaults().get("time_zone"))
    cdoc = frappe.get_doc(doctype, row_name)
    document_code = cdoc.document_code
    if (
        doctype == "Canopi Security Documents"
        or doctype == "Canopi Security Documents Post Execution"
    ):
        security_type = cdoc.security_type
    if (
        doctype == "Canopi Condition Precedent Part A"
        or doctype == "Canopi Condition Precedent Part B"
        or doctype == "Canopi Condition Subsequent"
    ):
        security_type = cdoc.category
    if document_code == "":
        document_code = row_name

    file_name = file_name
    if file_name and file:
        if (
            doctype == "Canopi Security Documents"
            or doctype == "Canopi Security Documents Post Execution"
            or doctype == "Canopi Condition Precedent Part A"
            or doctype == "Canopi Condition Precedent Part B"
            or doctype == "Canopi Condition Subsequent"
        ):
            key = (
                cstr(frappe.local.site)
                + "/Canopi Documentation/"
                + doctype
                + "/"
                + docname
                + "/"
                + security_type
                + "/"
                + document_code
                + "/"
                + str(str(datetime.now(tz=timezone)))
                + "/"
                + file_name
            )
        else:
            key = (
                cstr(frappe.local.site)
                + "/Canopi Documentation/"
                + doctype
                + "/"
                + docname
                + "/"
                + document_code
                + "/"
                + str(str(datetime.now(tz=timezone)))
                + "/"
                + file_name
            )
        file_data = file["file"].read()
        response = upload_file_to_s3(settings_doc, key, file_data)
        if response["status_code"] == 200:
            if (
                doctype == "Canopi Security Documents"
                or doctype == "Canopi Security Documents Post Execution"
                or doctype == "Canopi Condition Precedent Part A"
                or doctype == "Canopi Condition Precedent Part B"
                or doctype == "Canopi Condition Subsequent"
            ):
                prefix_key = f"{cstr(frappe.local.site)}/{'Canopi Documentation'}/{doctype}/{docname}/{security_type}/{document_code}"  # noqa: 501 isort:skip
            else:
                prefix_key = f"{cstr(frappe.local.site)}/{'Canopi Documentation'}/{doctype}/{docname}/{document_code}"  # noqa: 501 isort:skip
            delete_old_s3_files(prefix_key)
            today = str(date.today())

            frappe.db.set_value(doctype, row_name, "file_path", key)
            frappe.db.set_value(doctype, row_name, "file_name", file_name)
            frappe.db.set_value(doctype, row_name, "upload_date", today)
            frappe.db.commit()
    calculate_completion_percent(docname)


@frappe.whitelist()
def invit_post_execution_s3_upload():
    validate_s3_settings()
    file = frappe.request.files
    row_name = frappe.request.form.get("row_name")
    doctype = frappe.request.form.get("doctype")
    docname = frappe.request.form.get("docname")
    file_name = frappe.request.form.get("file_name")
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    timezone = pytz.timezone(frappe.defaults.get_defaults().get("time_zone"))

    cdoc = frappe.get_doc(doctype, row_name)
    document_code = cdoc.document_code

    if document_code == "":
        document_code = row_name
    file = file["file"].read()

    key = (
        cstr(frappe.local.site)
        + "/Canopi Post Execution Checklist/"
        + doctype
        + "/"
        + docname
        + "/"
        + document_code
        + "/"
        + str(str(datetime.now(tz=timezone)))
        + "/"
        + file_name
    )
    file_data = file
    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        prefix_key = f"{cstr(frappe.local.site)}/{'Canopi Post Execution Checklist'}/{doctype}/{docname}/{document_code}"  # noqa: 501 isort:skip
        delete_old_s3_files(prefix_key)
    today = str(date.today())
    frappe.db.set_value(doctype, row_name, "file_path", key)
    frappe.db.set_value(doctype, row_name, "file_name", file_name)
    frappe.db.set_value(doctype, row_name, "upload_date", today)
    frappe.db.commit()


@frappe.whitelist()
def documentation_send_mail(
    recipients,
    subject,
    content,
    child_doctype,
    after_email_update_field,
    send_me_a_copy,
    attachments,
    s3_attachments,
    read_receipt,
    sender=None,
    cc=[],
    bcc=[],
):
    attach = []
    recipients = split_emails(recipients)
    cc = split_emails(cc) if cc else []
    bcc = split_emails(bcc) if bcc else []
    attachments = json.loads(attachments)
    s3_attachments = json.loads(s3_attachments)

    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)

    for key, value in enumerate(s3_attachments):
        child_doc = value
        key = s3_attachments[value].get("file_url")

        basename = os.path.basename(key)
        path = "/tmp/" + basename
        try:
            s3_client.download_file(
                Bucket=settings_doc.s3_bucket, Key=key, Filename=path
            )
        except botocore.exceptions.ClientError:
            frappe.throw("Failed to download file.", title="Error")

        file_bytes = None
        with open(path, "rb") as data:
            file_bytes = io.BytesIO(data.read())

        os.remove(path)
        attach.append({"fname": basename, "fcontent": file_bytes.getbuffer()})

    for file in attachments:
        if frappe.db.exists("File", file):
            doc = frappe.get_doc("File", file)
            attach.append(
                {"fname": doc.file_name, "fcontent": doc.get_content()}
            )  # noqa: 501 isort:skip

    if int(send_me_a_copy) == 1:
        email = get_doc_list("Email Account", filters={"default_outgoing": 1})
        if email[0].email_id not in cc:
            cc.append(email[0].email_id)
    frappe.sendmail(
        sender=sender,
        recipients=recipients,
        subject=subject,
        content=content,
        attachments=attach,
        read_receipt=int(read_receipt),
        cc=cc,
        bcc=bcc,
        expose_recipients="header",
    )
    for key, value in enumerate(s3_attachments):
        child_doc = value
        doc = frappe.get_doc(child_doctype, child_doc)
        if after_email_update_field == "send_to_legal":
            doc.send_to_legal = "Yes"
        if after_email_update_field == "send_email":
            doc.send_email = "Yes"
        doc.save()


@frappe.whitelist()
def download_documentation_annexure_b(docname):
    frappe.local.response.filename = "{name}-annexure-b.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    html = frappe.get_print("Canopi Documentation", docname, "Annexure B")

    frappe.local.response.filecontent = get_pdf(html)
    frappe.local.response.type = "pdf"


@frappe.whitelist()
def pre_execution_checklist_send_mail(
    recipients,
    subject,
    content,
    send_me_a_copy,
    attachments,
    read_receipt,
    sender=None,
    cc=[],
    bcc=[],
):
    attach = []
    recipients = split_emails(recipients)
    cc = split_emails(cc) if cc else []
    bcc = split_emails(bcc) if bcc else []
    attachments = json.loads(attachments)

    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)

    for key in attachments["s3_key"]:
        basename = os.path.basename(key)
        path = "/tmp/" + basename
        try:
            s3_client.download_file(
                Bucket=settings_doc.s3_bucket, Key=key, Filename=path
            )
        except botocore.exceptions.ClientError:
            frappe.throw("Failed to download file.", title="Error")

        file_bytes = None
        with open(path, "rb") as data:
            file_bytes = io.BytesIO(data.read())

        os.remove(path)
        attach.append({"fname": basename, "fcontent": file_bytes.getbuffer()})

    for file in attachments["file_url"]:
        doc = frappe.get_doc("File", file)
        attach.append({"fname": doc.file_name, "fcontent": doc.get_content()})

    if int(send_me_a_copy) == 1:
        email = get_doc_list("Email Account", filters={"default_outgoing": 1})
        if email[0].email_id not in cc:
            cc.append(email[0].email_id)
    frappe.sendmail(
        sender=sender,
        recipients=recipients,
        subject=subject,
        content=content,
        attachments=attach,
        read_receipt=int(read_receipt),
        cc=cc,
        bcc=bcc,
        expose_recipients="header",
    )


@frappe.whitelist()
def download_pre_execution_annexure_a(docname):
    html = frappe.get_print(
        "Canopi Pre Execution Checklist", docname, "Annexure A"
    )  # noqa: 501 isort:skip
    frappe.local.response.filename = "{name}-annexure-a.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    frappe.local.response.filecontent = get_pdf(html)
    frappe.local.response.type = "pdf"


@frappe.whitelist()
def replace_existing_s3_file():
    validate_user("BCG RM")
    validate_s3_settings()
    file = frappe.request.files
    file_name = frappe.request.form.get("file")
    file_name = frappe.request.form.get("file_name")
    doctype = frappe.request.form.get("doctype")
    docname = frappe.request.form.get("docname")
    doc = frappe.get_doc(doctype, docname)
    if doc.cnp_s3_key:
        delete_from_s3(doc.cnp_s3_key)
        doc.cnp_s3_key = ""
        doc.save()
    send_file_to_s3(file, file_name, doctype, docname)


@frappe.whitelist()
def move_to_track_exception(docname, row, append_to):
    row = json.loads(row)
    if row["track_exception"] == "Yes":
        pre_execution_doc = frappe.get_doc(
            "Canopi Pre Execution Checklist", docname
        )  # noqa: 501 isort:skip
        post_execution_doc = get_doc_list(
            doctype="Canopi Post Execution Checklist",
            filters=[["operations", "=", pre_execution_doc.operations]],
        )
        for post in post_execution_doc:
            doc = frappe.get_doc("Canopi Post Execution Checklist", post.name)
            exception_details = (
                json.loads(doc.exception_tracking_details)
                if doc.exception_tracking_details
                else {}
            )

            if not exception_details.get(append_to):
                exception_details[append_to] = []
            if row.get("s3_key"):
                source_key = row["s3_key"]
                timezone = pytz.timezone(
                    frappe.defaults.get_defaults().get("time_zone")
                )
                destination_key = (
                    cstr(frappe.local.site)
                    + "/Post Execution Checklist/"
                    + doc.name
                    + "/Exception Tracking/"
                    + append_to.replace("_", " ").title()
                    + "/"
                    + row.get("document_code")
                    + "/"
                    + str(datetime.now(tz=timezone))
                    + "/"
                    + row["file_name"]
                )
                copy_s3_object(source_key, destination_key)
                row["s3_key"] = destination_key
                row["upload_date"] = str(datetime.now().date())

            exception_details[append_to].append(row)
            doc.exception_tracking_details = json.dumps(exception_details)
            doc.save()


def copy_s3_object(source, destination):
    validate_s3_settings()

    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    s3_client = get_s3_client(settings_doc)
    s3_client.copy_object(
        Bucket=settings_doc.s3_bucket,
        CopySource={"Bucket": settings_doc.s3_bucket, "Key": source},
        Key=destination,
    )


@frappe.whitelist()
def make_journal_entry(source_name, target_doc=None, ignore_permissions=False):
    def set_missing_values(source, target):
        target.voucher_type = "Deferred Revenue"
        adoc = frappe.get_doc("Canopi Amortization", source_name)
        sdoc = frappe.get_doc("Sales Invoice", adoc.sales_invoice)
        cost_center = sdoc.cost_center
        target.cnp_cost_center = cost_center
        target.append(
            "accounts",
            {
                "account": source.account_to_be_debited,
                "debit_in_account_currency": source.amount,
                "canopi_reference_type": "Canopi Amortization",
                "canopi_reference_name": source.name,
                "cost_center": cost_center,
            },
        )
        target.append(
            "accounts",
            {
                "account": source.account_to_be_credited,
                "credit_in_account_currency": source.amount,
                "canopi_reference_type": "Canopi Amortization",
                "canopi_reference_name": source.name,
                "cost_center": cost_center,
            },
        )

    doclist = get_mapped_doc(
        "Canopi Amortization",
        source_name,
        {
            "Canopi Amortization": {
                "doctype": "Journal Entry",
                "validation": {"docstatus": ["=", 1]},
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


@frappe.whitelist()
def download_tax_invoice(doctype, docname, company_name):
    frappe.local.response.filename = "{name}-tax-invoice.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )
    frappe.local.response.filecontent = get_tax_invoice(
        company_name, doctype, docname
    )  # noqa: 501 isort:skip
    frappe.local.response.type = "pdf"


def get_tax_invoice(company_name, doctype, docname):
    doc = frappe.get_doc("Company", company_name)
    html = frappe.get_print(doctype, docname, "Canopi Tax Invoice")
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)
    out = None

    if doc.cnp_s3_key:
        x1 = 410
        y1 = 70
        x2 = x1 + 140
        y2 = y1 + 54
        file_path = get_pfx_from_s3(doc.cnp_s3_key)
        signer = signers.SimpleSigner.load_pkcs12(
            pfx_file=file_path,
            passphrase=(doc.get_password("cnp_sign_secret")).encode(),
        )
        append_signature_field(
            w, SigFieldSpec(sig_field_name=doc.name, box=(x1, y1, x2, y2))
        )
        out = signers.sign_pdf(
            w, signers.PdfSignatureMetadata(field_name=doc.name), signer=signer
        )
        os.remove(file_path)
    return out.getbuffer() if out else get_pdf(html)


@frappe.whitelist()
def generate_qrcode(txt):
    qrcode = pyqrcode.create(txt)
    path = "/tmp/image.png"
    qrcode.png(path, scale=2)
    with open(path, "rb") as data:
        base64_string = base64.b64encode(data.read())
        base64_string = base64_string.decode("utf-8")
    os.remove(path)
    return base64_string


@frappe.whitelist()
def make_fixed_deposit_maturity(source_name, target_doc=None):
    def set_missing_values(source, target):
        target.fd_opening = source.name

    doclist = get_mapped_doc(
        "Canopi Fixed Deposit Opening Entry",
        source_name,
        {
            "Canopi Fixed Deposit Opening Entry": {
                "doctype": "Canopi Fixed Deposit Maturity",
                "validation": {"docstatus": ["=", 1]},
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


@frappe.whitelist()
def make_fund_closure_entry(source_name, target_doc=None):
    def set_missing_values(source, target):
        target.fd_maturity = source.name

    doclist = get_mapped_doc(
        "Canopi Fixed Deposit Maturity",
        source_name,
        {
            "Canopi Fixed Deposit Maturity": {
                "doctype": "Canopi Fund Closure Entry",
                "validation": {"docstatus": ["=", 1]},
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


@frappe.whitelist()
def make_redemption_accounting(source_name, target_doc=None):
    def set_missing_values(source, target):
        target.mf_opening = source.name

    doclist = get_mapped_doc(
        "Canopi Mutual Funds Opening Entry",
        source_name,
        {
            "Canopi Mutual Funds Opening Entry": {
                "doctype": "Canopi Redemption Accounting",
                "validation": {"docstatus": ["=", 1]},
            },
        },
        target_doc,
        set_missing_values,
    )
    return doclist


@frappe.whitelist()
def str_to_json(json_string):
    return json.loads(json_string) if json_string else []


@frappe.whitelist()
def get_total_expression_in_letters(list_length):
    alphabets = string.ascii_lowercase
    return "+".join(alphabets[:list_length])


@frappe.whitelist()
def update_documentation_loa_details(docname, doclist):
    doclist = json.loads(doclist)

    documentation = frappe.get_doc("Canopi Documentation", docname)
    documentation.loa_serial_no = get_loa_serial_number(documentation)

    file_name = f"{documentation.loa_serial_no}.pdf".replace("/", "-")
    timezone = pytz.timezone(frappe.defaults.get_defaults().get("time_zone"))
    s3_key = (
        f"{cstr(frappe.local.site)}"
        f"/Documentation/{docname}/Generated Loa/"
        f"{str(datetime.now(tz=timezone))}/"
        f"{file_name}"
    )

    generated_loa_history = (
        json.loads(documentation.generated_loa_history)
        if documentation.generated_loa_history
        else []
    )
    generated_loa_history.append(
        {
            "file_name": file_name,
            "documents": doclist,
            "s3_key": s3_key,
            "status": "Pending for Approval",
        }
    )
    documentation.generated_loa_history = json.dumps(generated_loa_history)
    documentation.is_loa_draft = 1
    documentation.save()

    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    upload_file_to_s3(settings_doc, s3_key, get_signed_loa(docname))

    attach = []
    s3_client = get_s3_client(settings_doc)
    basename = os.path.basename(s3_key)
    path = "/tmp/" + basename
    try:
        s3_client.download_file(
            Bucket=settings_doc.s3_bucket, Key=s3_key, Filename=path
        )
    except botocore.exceptions.ClientError:
        frappe.throw("Failed to download file.", title="Error")

    file_bytes = None
    with open(path, "rb") as data:
        file_bytes = io.BytesIO(data.read())

    os.remove(path)
    attach.append({"fname": basename, "fcontent": file_bytes.getbuffer()})
    documentation_doc = frappe.get_doc("Canopi Documentation", docname)
    approver_role = "Ops Checker"

    approval = frappe.new_doc("Canopi Approval")
    approval.status = "Pending for Approval"
    approval.is_loa = 1
    approval.approver_role = approver_role
    approval.approver = documentation.tl_representative

    approval.append(
        "approval_status",
        {
            "sent_by": frappe.session.user,
            "approver_role": approver_role,
            "approver": documentation.tl_representative,
            "status": "Pending",
        },
    )

    approval.email_token = frappe.generate_hash()
    approval.email_token_expiry = datetime.now() + timedelta(days=90)
    approval.documentation = documentation.name
    approval.operations = documentation.operations
    approval.client_name = documentation.client_name
    approval.product_name = documentation.product_name
    approval.cnp_facility_amt = documentation.cnp_facility_amt
    approval.cnp_facility_amt_in_words = (
        documentation.cnp_facility_amt_in_words
    )  # noqa: 501
    approval.cnp_facility_amt_in_words_millions = (
        documentation.cnp_facility_amt_in_words_millions
    )  # noqa: 501
    approval.insert()

    doc_reference = documentation.name
    url = get_url()
    content1 = f"""
        <h2>Approval</h2>
        <p>Document Reference :{doc_reference}</p>
        <p>Client :{approval.client_name}</p>"""
    content2 = f"""
        <p><a href="{url}/app/canopi-approval/{approval.name}">{approval.name} </a></p>"""  # noqa: 501

    content3 = f"""
    <p><a href= "{url}/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.email_action?token={approval.email_token}&action_code=1&docname={approval.name}" class="button">Approve</a>"""  # noqa: 501
    content3 += f"""
    <a href= "{url}/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.email_action?token={approval.email_token}&action_code=2&docname={approval.name}" class="button_1">Reject</a></p>"""  # noqa: 501
    cont = """
        <style>
            .button {
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }
            .button_1 {
                background-color: red;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
            }
        </style>
    """

    frappe.sendmail(
        recipients=documentation_doc.tl_representative,
        subject="Approval For LOA",
        attachments=attach,
        message=cont + content1 + content2 + content3,
    )
    # change each row status to LOA Generated
    for x in doclist:
        doc = frappe.get_doc(x["doctype"], x["name"])
        doc.status = "Pending for Approval"
        doc.save()
    return s3_key


@frappe.whitelist()
def get_signed_loa(docname):
    doc = frappe.get_doc("Canopi Documentation", docname)
    lot = "Letter Of Authority"
    html = frappe.get_print("Canopi Documentation", docname, lot)
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)
    out = None
    if doc.authorised_signatory and doc.is_loa_draft == 0:
        x1 = 50
        y1 = 119
        x2 = x1 + 140
        y2 = y1 + 54
        authorised_signatory = frappe.get_doc(
            "ATSL Digital Signatures", doc.authorised_signatory
        )
        if authorised_signatory.cnp_s3_key:
            file_path = get_pfx_from_s3(authorised_signatory.cnp_s3_key)
            authorised_signatory_signer = signers.SimpleSigner.load_pkcs12(
                pfx_file=file_path,
                passphrase=(
                    authorised_signatory.get_password("cnp_sign_secret")
                ).encode(),
            )
            append_signature_field(
                w,
                SigFieldSpec(
                    sig_field_name=authorised_signatory.name,
                    box=(x1, y1, x2, y2),
                ),
            )
            signature_metadata = signers.PdfSignatureMetadata(
                field_name=authorised_signatory.name
            )
            out = signers.sign_pdf(
                w,
                signature_metadata,
                signer=authorised_signatory_signer,
            )
            os.remove(file_path)
    return out.getbuffer() if out else get_pdf(html)


def get_loa_serial_number(doc):
    if not doc.loa_serial_link:
        frappe.throw("Serial Number is mandatory field.")

    serial_doc = frappe.get_doc("Canopi Serial Number", doc.loa_serial_link)
    defaults = frappe.defaults.get_defaults()

    current_fiscal_end_date = defaults.get("year_end_date")
    current_fiscal_end_date = datetime.strptime(
        current_fiscal_end_date, "%Y-%m-%d"
    ).date()
    current_fiscal_start_date = defaults.get("year_start_date")

    current_fiscal_start_date = datetime.strptime(
        current_fiscal_start_date, "%Y-%m-%d"
    ).date()

    # reset serial number if fiscal year changes
    if current_fiscal_end_date > serial_doc.fiscal_end_date:
        serial_doc.serial_no = "000000"
        serial_doc.fiscal_end_date = current_fiscal_end_date
    serial_no = int(serial_doc.serial_no) + 1
    serial_doc.serial_no = str(serial_no).rjust(6, "0")
    serial_doc.save()
    start_year = str(current_fiscal_start_date.year)[2:]
    end_year = str(current_fiscal_end_date.year)[2:]
    return (
        f"ATSL/CO/{doc.operations}/"
        f"{start_year}-{end_year}/"
        f"{str(serial_no).rjust(6, '0')}"
    )


@frappe.whitelist()
def update_documents_status(
    docs,
    status="Open",
    approver_role="",
    is_post_execution=False,
    is_annexure_a=False,
    is_loa=False,
    is_annexure_b=False,
):
    if "Approved" in status or "Rejected" in status:
        status = validate_user_and_get_status(status, approver_role)

    if isinstance(docs, (str)):
        docs = json.loads(docs)
    for x in docs:
        doc = frappe.get_doc(x["doctype"], x["docname"])

        if is_post_execution:
            doc.status = status

        if is_annexure_a:
            doc.annexure_a_status = (
                "Approved"
                if "Approved" in status
                else "Rejected"
                if "Rejected" in status
                else status
            )

        if is_annexure_b:
            doc.annexure_b_status = (
                "Approved"
                if "Approved" in status
                else "Rejected"
                if "Rejected" in status
                else status
            )

        if is_loa:
            generated_loa_history = (
                json.loads(doc.generated_loa_history)
                if doc.generated_loa_history
                else []
            )
            loa_status = (
                "Approved"
                if "Approved" in status
                else "Rejected"
                if "Rejected" in status
                else status
            )
            update_loa = [generated_loa_history[-1]]
            generated_loa_history.pop()

            generated_loa_history.append(
                {
                    "file_name": update_loa[0]["file_name"],
                    "documents": update_loa[0]["documents"],
                    "s3_key": update_loa[0]["s3_key"],
                    "status": loa_status,
                }
            )
            for z in update_loa[0]["documents"]:
                if frappe.db.exists(z["doctype"], z["name"]):
                    if loa_status == "Approved":
                        loa_doc_status = "LOA Generated"
                    else:
                        loa_doc_status = "LOA Rejected"
                    det_doc = frappe.get_doc(z["doctype"], z["name"])
                    det_doc.status = loa_doc_status
                    det_doc.save()

            doc.generated_loa_history = json.dumps(generated_loa_history)
            if loa_status == "Approved":
                doc.is_loa_draft = 0

        doc.save()
        if is_loa and loa_status == "Approved":
            settings_doc = frappe.get_single("Trusteeship Platform Settings")
            upload_file_to_s3(
                settings_doc, update_loa[0]["s3_key"], get_signed_loa(doc.name)
            )
    return status


def validate_user_and_get_status(status: str, role):
    if role in frappe.get_roles(frappe.session.user):
        return status
    elif "Admin Approver" in frappe.get_roles(frappe.session.user):
        return f'{" ".join(status.split()[:2])} Admin'

    frappe.throw("You are not allowed to perform this action")


def validate_user(role):
    if role in frappe.get_roles(frappe.session.user):
        return
    frappe.throw("You are not allowed to perform this action")


@frappe.whitelist()
def isin_send_mail(
    recipients,
    subject,
    content,
    send_me_a_copy,
    attachments,
    read_receipt,
    sender=None,
    cc=[],
    bcc=[],
):
    attach = []
    recipients = split_emails(recipients)
    cc = split_emails(cc) if cc else []
    bcc = split_emails(bcc) if bcc else []
    attachments = json.loads(attachments)

    for file in attachments:
        if frappe.db.exists("File", file):
            doc = frappe.get_doc("File", file)
            attach.append(
                {"fname": doc.file_name, "fcontent": doc.get_content()}
            )  # noqa: 501 isort:skip

    if int(send_me_a_copy) == 1:
        email = get_doc_list("Email Account", filters={"default_outgoing": 1})
        if email[0].email_id not in cc:
            cc.append(email[0].email_id)
    frappe.sendmail(
        sender=sender,
        recipients=recipients,
        subject=subject,
        content=content,
        attachments=attach,
        read_receipt=int(read_receipt),
        cc=cc,
        bcc=bcc,
        expose_recipients="header",
    )


@frappe.whitelist()
def calculate_completion_percent(doc_name):
    total_transaction_documents = frappe.get_list(
        "Canopi Transaction Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )
    completed_transaction_documents = frappe.get_list(
        "Canopi Transaction Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )

    total_transaction_documents_for_post = frappe.get_list(
        "Canopi Transaction Documents Post Execution",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_transaction_documents_for_post = frappe.get_list(
        "Canopi Transaction Documents Post Execution",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total_security_documents = frappe.get_list(
        "Canopi Security Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )
    completed_security_documents = frappe.get_list(
        "Canopi Security Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )

    total_security_documents_for_post = frappe.get_list(
        "Canopi Security Documents Post Execution",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_security_documents_for_post = frappe.get_list(
        "Canopi Security Documents Post Execution",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total_other_documents = frappe.get_list(
        "Canopi Other Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )
    completed_other_documents = frappe.get_list(
        "Canopi Other Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )

    total_other_documents_for_post = frappe.get_list(
        "Canopi Other Documents Post Execution",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_other_documents_for_post = frappe.get_list(
        "Canopi Other Documents Post Execution",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total_condition_precedent_part_a = frappe.get_list(
        "Canopi Condition Precedent Part A",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_condition_precedent_part_a = frappe.get_list(
        "Canopi Condition Precedent Part A",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total_condition_precedent_part_b = frappe.get_list(
        "Canopi Condition Precedent Part B",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_condition_precedent_part_b = frappe.get_list(
        "Canopi Condition Precedent Part B",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total_canopi_condition_subsequent = frappe.get_list(
        "Canopi Condition Subsequent",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_canopi_condition_subsequent = frappe.get_list(
        "Canopi Condition Subsequent",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total_pre_execution_documents = frappe.get_list(
        "Canopi Pre Execution Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )
    completed_pre_execution_documents = frappe.get_list(
        "Canopi Pre Execution Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
            ["retain_waive", "!=", "Waive"],
            ["for_post_execution", "!=", "Yes"],
        ],
    )

    total_post_execution_documents = frappe.get_list(
        "Canopi Post Execution Documents",
        fields=["file_name"],
        filters=[["canopi_documentation_name", "=", doc_name]],
    )
    completed_post_execution_documents = frappe.get_list(
        "Canopi Post Execution Documents",
        fields=["file_name"],
        filters=[
            ["canopi_documentation_name", "=", doc_name],
            ["file_name", "!=", ""],
        ],
    )

    total = (
        len(total_transaction_documents)
        + len(total_transaction_documents_for_post)
        + len(total_security_documents)
        + len(total_security_documents_for_post)
        + len(total_other_documents)
        + len(total_other_documents_for_post)
        + len(total_condition_precedent_part_a)
        + len(total_condition_precedent_part_b)
        + len(total_canopi_condition_subsequent)
        + len(total_pre_execution_documents)
        + len(total_post_execution_documents)
    )

    completed = (
        len(completed_transaction_documents)
        + len(completed_transaction_documents_for_post)
        + len(completed_security_documents)
        + len(completed_security_documents_for_post)
        + len(completed_other_documents)
        + len(completed_other_documents_for_post)
        + len(completed_condition_precedent_part_a)
        + len(completed_condition_precedent_part_b)
        + len(completed_canopi_condition_subsequent)
        + len(completed_pre_execution_documents)
        + len(completed_post_execution_documents)
    )

    if total != 0:
        percent = (completed / total) * 100
        frappe.db.set_value(
            "Canopi Documentation",
            doc_name,
            "completion_percent",
            round(percent, 2),  # noqa: 501 isort:skip
        )
    else:
        frappe.db.set_value(
            "Canopi Documentation", doc_name, "completion_percent", 0
        )  # noqa: 501 isort:skip


@frappe.whitelist()
def add_note(doctype, docname, note):
    doc = frappe.get_doc(doctype, docname)
    CRMNote.add_note(doc, note)


@frappe.whitelist()
def edit_note(doctype, docname, note, rowId):
    doc = frappe.get_doc(doctype, docname)
    CRMNote.edit_note(doc, note, rowId)


@frappe.whitelist()
def delete_note(doctype, docname, rowId):
    doc = frappe.get_doc(doctype, docname)
    CRMNote.delete_note(doc, rowId)


@frappe.whitelist()
def transaction_details_s3_upload(docname, details, doctype):
    validate_s3_settings()
    file_list = []
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    details = json.loads(details)

    doc = frappe.get_doc(doctype, docname)
    old_file = doc.file_name
    if old_file:
        file_list = old_file.split(",")

    for key, value in enumerate(details):
        file_name = details[value].get("file_name")
        file = details[value].get("file")
        if file_name and file:
            key = (
                cstr(frappe.local.site)
                + "/"
                + doctype
                + "/"
                + docname
                + "/"
                + file_name
            )
            base64_image_str = file
            base64_image_str = base64_image_str[
                base64_image_str.find(",") + 1 :  # noqa: 501 isort:skip
            ]  # noqa: 501 isort:skip
            file_data = base64.b64decode(base64_image_str)
            response = upload_file_to_s3(settings_doc, key, file_data)
            if response["status_code"] == 200:
                if file_name not in file_list:
                    file_list.append(file_name)

    files = ",".join(file_list)
    doc.file_name = files
    doc.save()


@frappe.whitelist()
def transaction_details_delete_file(docname, file_name, doctype):
    key = (
        cstr(frappe.local.site)
        + "/"
        + doctype
        + "/"
        + docname
        + "/"
        + file_name  # noqa: 501 isort:skip
    )  # noqa: 501 isort:skip
    delete_from_s3(key)

    doc = frappe.get_doc(doctype, docname)
    files = doc.file_name
    fArr = files.split(",")
    fArr.remove(file_name)
    file_names = ",".join(fArr)
    doc.file_name = file_names
    doc.save()


@frappe.whitelist()
def get_linked_docs(doctype, name, linkinfo):
    from frappe.desk.form.linked_with import get_linked_docs

    return get_linked_docs(doctype, name, linkinfo)


@frappe.whitelist()
def chk_req_for_documentation(data, operations):
    data = json.loads(data)
    filters = {"operations": operations}
    post = frappe.db.get_value(
        "Canopi Post Execution Checklist",
        filters=filters,
        fieldname=["*"],
        as_dict=True,
    )
    if post:
        details = json.loads(post.type_of_security_details)
        for asset in details:
            for index, asset_details in enumerate(details[asset]):
                security_doc_post = asset_details.get("security_doc_post")
                if security_doc_post:
                    if security_doc_post == data.get("name"):
                        return 1
                    else:
                        return 0


@frappe.whitelist()
def create_prospect_and_contacts(docname, data):
    data = json.loads(data)
    doc = frappe.get_doc("Lead", docname)
    doc.create_prospect_and_contact(data=data)


@frappe.whitelist()
def finifo_events(docname, values, action, index):
    doc = frappe.get_doc("Opportunity", docname)
    if action == "Add":
        values = json.loads(values)
        fininfo = doc.append("cnp_fininfo")
        fininfo.net_revenue = values.get("net_revenue")
        fininfo.operating_profit = values.get("operating_profit")
        profit_from_discontinuing = values.get("profit_from_discontinuing")
        fininfo.profit_from_discontinuing = profit_from_discontinuing
        fininfo.debt_equity = values.get("debt_equity")
        fininfo.debt_securities = values.get("debt_securities")
        fininfo.loans = values.get("loans")
    elif action == "Edit":
        values = json.loads(values)
        index = int(index)
        doc.cnp_fininfo[index].net_revenue = values.get("net_revenue")
        doc.cnp_fininfo[index].operating_profit = values.get("operating_profit")  # noqa
        doc.cnp_fininfo[index].profit_from_discontinuing = values.get(
            "profit_from_discontinuing"
        )
        doc.cnp_fininfo[index].debt_equity = values.get("debt_equity")
        doc.cnp_fininfo[index].debt_securities = values.get("debt_securities")
        doc.cnp_fininfo[index].loans = values.get("loans")
    elif action == "Delete":
        index = int(index)
        doc.cnp_fininfo.pop(index)
    doc.save()
    return doc


@frappe.whitelist()
def cr_table_events(docname, values, action, index):
    doc = frappe.get_doc("Opportunity", docname)
    if action == "Add":
        values = json.loads(values)
        cr_rating = doc.append("cnp_cr_rating")
        cr_rating.rating_agency = values.get("rating_agency")
        cr_rating.rating_date = values.get("rating_date")
        cr_rating.current_rating = values.get("current_rating")
        cr_rating.type_of_loan = values.get("type_of_loan")
        cr_rating.currency = values.get("currency")
        cr_rating.amount = values.get("amount")
        cr_rating.status = values.get("status")
    elif action == "Edit":
        values = json.loads(values)
        index = int(index)
        doc.cnp_cr_rating[index].rating_agency = values.get("rating_agency")
        doc.cnp_cr_rating[index].rating_date = values.get("rating_date")
        doc.cnp_cr_rating[index].current_rating = values.get("current_rating")
        doc.cnp_cr_rating[index].type_of_loan = values.get("type_of_loan")
        doc.cnp_cr_rating[index].currency = values.get("currency")
        doc.cnp_cr_rating[index].amount = values.get("amount")
        doc.cnp_cr_rating[index].status = values.get("status")
    elif action == "Delete":
        index = int(index)
        doc.cnp_cr_rating.pop(index)
    doc.save()
    return doc


@frappe.whitelist()
def set_status(docstatus):
    status = {"0": "Draft", "1": "Submitted", "2": "Cancelled"}
    return status.get(str(docstatus))


@frappe.whitelist()
def update_tl_ops(document, doc_type_list):
    for doctype in doc_type_list:
        doc = frappe.db.get_value(
            doctype,
            {"operations": document.operations, "docstatus": 0},
            ["*"],
            as_dict=True,
        )
        if doc:
            if document.tl_representative != doc.tl_representative:
                frappe.db.set_value(
                    doctype,
                    doc.get("name"),
                    "tl_representative",
                    document.tl_representative,
                )  # noqa

            if document.ops_servicing_rm != doc.ops_servicing_rm:
                frappe.db.set_value(
                    doctype,
                    doc.get("name"),
                    "ops_servicing_rm",
                    document.ops_servicing_rm,
                )  # noqa


@frappe.whitelist()
def add_rejection_comment_sales_invoice(doctype, docname, comment):
    validate_user("Accounts Manager")
    frappe.db.set_value(doctype, docname, "cnp_is_reject_reason", 0)
    frappe.db.set_value(doctype, docname, "cnp_reject_reason_comment", comment)
    frappe.db.set_value(doctype, docname, "docstatus", 2)
    frappe.db.set_value(doctype, docname, "workflow_state", "Rejected")
    SalesInvoice.on_submit(frappe.get_doc("Sales Invoice", docname))
    url = get_url()
    my_attachments = []

    content1 = f"""<h2>{doctype} {"Rejected"}</h2>"""
    content3 = f"""
    <p>{docname} in {doctype} is {"Rejected"}</p>"""
    cont = """<style></style>"""

    recipient = []

    Users = frappe.get_all("User")
    for usr in Users:
        role = frappe.get_roles(usr["name"])
        if "Accounts User" in role:
            eid = frappe.get_doc("User", usr["name"])
            recipient.append(eid.email)
    content2 = f""" <p><a href="{url}/app/{doctype}/{docname}">{docname} </a></p>"""  # noqa: 501
    frappe.sendmail(
        recipients=recipient,
        subject=doctype + " " + "Rejected",
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )


@frappe.whitelist()
def cancel_and_amend_doc(doctype, docname, default_values={}):
    if isinstance(default_values, str):
        default_values = json.loads(default_values)

    doc = frappe.get_doc(doctype, docname)
    doc.cancel()

    amendment = frappe.copy_doc(doc)
    amendment.docstatus = 0
    amendment.status = ""
    amendment.amended_from = doc.name
    amendment.workflow_state = ""
    for key, value in default_values.items():
        amendment.set(key, value)

    amendment.save()

    return amendment.name


@frappe.whitelist()
def asign_task(args=None):
    if not args:
        args = frappe.local.form_dict

    shared_with_users = []
    for assign_to_users in frappe.parse_json(args.get("assign_to")):
        from frappe.utils import nowdate

        if not args.get("description"):
            args["description"] = ("Assignment for {} {}").format(
                args["doctype"], args["name"]
            )

        d = frappe.get_doc(
            {
                "doctype": "ToDo",
                "allocated_to": assign_to_users,
                "reference_type": args["doctype"],
                "reference_name": args["name"],
                "description": args.get("description"),
                "priority": args.get("priority", "Medium"),
                "status": "Open",
                "date": args.get("date", nowdate()),
                "assigned_by": args.get("assigned_by", frappe.session.user),
                "assignment_rule": args.get("assignment_rule"),
            }
        ).insert(ignore_permissions=True)

        # set assigned_to if field exists
        if frappe.get_meta(args["doctype"]).get_field("assigned_to"):
            frappe.db.set_value(
                args["doctype"], args["name"], "assigned_to", assign_to_users
            )

        doc = frappe.get_doc(args["doctype"], args["name"])
        # if assignee have permissions, share or inform
        if frappe.has_permission(doc=doc, user=assign_to_users):
            frappe.share.add(doc.doctype, doc.name, assign_to_users)
            shared_with_users.append(assign_to_users)

        # make this document followed by assigned user
        if frappe.get_cached_value(
            "User", assign_to_users, "follow_assigned_documents"
        ):
            document_follow.follow_document(
                args["doctype"], args["name"], assign_to_users
            )

        # notify
        if args["action"] == "":
            assign_to.notify_assignment(
                d.assigned_by,
                d.allocated_to,
                d.reference_type,
                d.reference_name,
                action="ASSIGN",
                description=args.get("description"),
            )
        else:
            notify_approval(
                d.assigned_by,
                d.allocated_to,
                d.reference_type,
                d.reference_name,
                action=args["action"],
                description=args.get("description"),
            )

    if shared_with_users:
        user_list = assign_to.format_message_for_assign_to(
            shared_with_users
        )  # noqa: 501
        if doc.doctype == "Canopi Documentation":
            frappe.msgprint(
                ("Shared with the following Users with Read access:{}").format(
                    user_list
                )  # noqa: 501
            )
        else:
            if doc.workflow_state == "Pending for Approval":
                frappe.msgprint(
                    (
                        "Shared with the following Users with Read access:{}"  # noqa: 501 isort:skip
                    ).format(user_list)
                )

    return assign_to.get(args)


@frappe.whitelist()
def notify_approval(
    assigned_by,
    allocated_to,
    doc_type,
    doc_name,
    action="CLOSE",
    description=None,  # noqa: 501
):
    if not (assigned_by and allocated_to and doc_type and doc_name):
        return

    # return if self assigned or user disabled
    if assigned_by == allocated_to or not frappe.db.get_value(
        "User", allocated_to, "enabled"
    ):
        return

    # Search for email address in description -- i.e. assignee
    user_name = frappe.get_cached_value(
        "User", frappe.session.user, "full_name"
    )  # noqa: 501
    title = get_title(doc_type, doc_name)
    description_html = f"<div>{description}</div>" if description else None

    if action == "CLOSE":
        subject = ("Your assignment on {} {} has been removed by {}").format(
            frappe.bold(doc_type),
            get_title_html(title),
            frappe.bold(user_name),  # noqa: 501
        )
    else:
        user_name = frappe.bold(user_name)
        document_type = frappe.bold(doc_type)
        title = get_title_html(title)
        subject = ("{} {} {} {}").format(
            user_name, action, document_type, title
        )  # noqa: 501

    notification_doc = {
        "type": "Assignment",
        "document_type": doc_type,
        "subject": subject,
        "document_name": doc_name,
        "from_user": frappe.session.user,
        "email_content": description_html,
    }

    enqueue_create_notification(allocated_to, notification_doc)


@frappe.whitelist()
def get_probe_42_base_details(doc):
    try:
        doc = json.loads(doc)
        cin_number = doc.get("cin_number")
        is_pan = (
            doc.get("cnp_type") == "Individual"
            or doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
        )
        cnp_type = "llps" if doc.get("cnp_type") == "LLP" else "companies"
        org_type = "llp" if cnp_type == "llps" else "company"  # noqa
        if frappe.db.exists("CPRB Company Details", cin_number):
            cprb_doc = frappe.get_doc("CPRB Company Details", cin_number)
            company_details = cprb_doc.payload
        else:
            company_details = fetch_probe_42_base_details(
                cin_number, is_pan, cnp_type
            )  # noqa
            cprb_doc = frappe.get_doc(
                {
                    "doctype": "CPRB Company Details",
                    "company_cin": cin_number,
                    "company_name": company_details.get("data", {})
                    .get(org_type, {})
                    .get("legal_name", ""),
                    "payload": company_details,
                }
            ).insert(ignore_permissions=True)

        # Check For Comprehensive Details
        comprehensive_details = fetch_probe_42_details(
            cin_number, is_pan, cnp_type
        )  # noqa
        is_comprehensive = False
        if comprehensive_details.get("data"):
            message = "Comprehensive Details Fetched Immediately!"
            company_details = comprehensive_details
            cprb_doc.has_comprehensive_details = 1

            cprb_doc.company_name = (
                company_details.get("data", {})
                .get(org_type, {})
                .get("legal_name", "")  # noqa
            )
            cprb_doc.payload = company_details
            cprb_doc.last_updated = frappe.utils.now_datetime()
            cprb_doc.save()
            is_comprehensive = True
        else:
            # Create Update Request For Comprehensive Details For a Company
            message = "Request is in Queue. Comprehensive details are being fetched from Probe42 database. Please try again after sometime."  # noqa

            create_update_request(cin_number, is_pan, cprb_doc, cnp_type)

        return (
            fetch_lead_from_probe_42(doc, company_details, is_comprehensive),
            message,
        )  # noqa
    except Exception as e:
        frappe.logger("utils").exception(e)


@frappe.whitelist()
def get_probe_42_comprehensive_details(doc):
    doc = json.loads(doc)
    cprb_doc = frappe.get_doc("CPRB Company Details", doc.get("cin_number"))
    cin_or_pan = cprb_doc.get("name")
    is_pan = (
        doc.get("cnp_type") == "Individual"
        or doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
    )
    cnp_type = "llps" if doc.get("cnp_type") == "LLP" else "companies"
    if not cprb_doc.has_comprehensive_details:
        response = _show_update_status(
            cin_or_pan, cprb_doc.request_id, is_pan, cnp_type
        )
        status = response.get("data", {}).get("status")
        if status != "FULFILLED":
            frappe.throw(
                "Request is in Queue. Comprehensive details are being fetched from Probe42 database. Please try again after sometime."  # noqa
            )
        comprehensive_details = fetch_probe_42_details(
            cin_or_pan, is_pan, cnp_type
        )  # noqa
        if comprehensive_details.get("data"):
            company_details = comprehensive_details
            cprb_doc.has_comprehensive_details = 1
            org_type = "llp" if cnp_type == "llps" else "company"  # noqa
            cprb_doc.company_name = (
                company_details.get("data", {})
                .get(org_type, {})
                .get("legal_name", "")  # noqa
            )
            cprb_doc.payload = company_details
            cprb_doc.last_updated = frappe.utils.now_datetime()
            cprb_doc.save()
    is_comprehensive = True
    return fetch_lead_from_probe_42(doc, cprb_doc.payload, is_comprehensive)


@frappe.whitelist()
def get_probe_42_comprehensive_details_for_opportunity(
    opportunity_name,
    source,
    is_local,
    security_type,
    cnp_type="",
    opportunity_from="",
    party_name="",
    cin_number="",
    product="",
    facility_amt=0,
    issue_size=0,
    tenor_years=0,
    tenor_months=0,
    tenor_days=0,
):
    opportunity_doc = prepare_opportunity_doc(
        is_local,
        opportunity_name,
        source,
        opportunity_from,
        party_name,
        cnp_type,
        cin_number,
        product,
        facility_amt,
        issue_size,
        tenor_years,
        tenor_months,
        tenor_days,
        security_type,
    )
    is_pan = (
        opportunity_doc.get("cnp_type") == "Individual"
        or opportunity_doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
    )
    cprb_doc = frappe.get_doc(
        "CPRB Company Details", opportunity_doc.get("cin_number")
    )  # noqa
    cin_or_pan = cprb_doc.get("name")
    cnp_type = "llps" if cnp_type == "LLP" else "companies"  # noqa
    if not cprb_doc.has_comprehensive_details:
        response = _show_update_status(
            cin_or_pan, cprb_doc.request_id, is_pan, cnp_type
        )
        status = response.get("data", {}).get("status")
        if status != "FULFILLED":
            frappe.throw(
                "Request is in Queue. Comprehensive details are being fetched from Probe42 database. Try again After Sometime"  # noqa
            )
        frappe.logger("utils").exception(cin_or_pan)
        comprehensive_details = fetch_probe_42_details(
            cin_or_pan, is_pan, cnp_type
        )  # noqa

        if comprehensive_details.get("data"):
            company_details = comprehensive_details
            cprb_doc.has_comprehensive_details = 1
            org_type = "llp" if cnp_type == "llps" else "company"  # noqa
            cprb_doc.company_name = (
                company_details.get("data", {})
                .get(org_type, {})
                .get("legal_name", "")  # noqa
            )
            cprb_doc.payload = company_details
            cprb_doc.last_updated = frappe.utils.now_datetime()
            cprb_doc.save()
    else:
        company_details = cprb_doc.payload
    return fetch_opportunity_from_probe42(
        company_details,
        opportunity_name,
        source,
        is_local,
        security_type,
        opportunity_from,
        party_name,
        cnp_type,
        cin_number,
        product,
        facility_amt,
        issue_size,
        tenor_years,
        tenor_months,
        tenor_days,
    )


def _show_update_status(cin_or_pan, request_id, is_pan, cnp_type):
    """
    Fetch Base Details For a CIN
    """
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    if not settings_doc.probe_42_url:
        frappe.throw("Please set Probe42 Url to proceed with Probe42 fetch")
    url = f"""{settings_doc.probe_42_url}/{cnp_type}/{cin_or_pan}/get-update-status?request_id={request_id}"""  # noqa: 501

    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.get(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    return response


def create_update_request(cin_or_pan, is_pan, cprb_doc, cnp_type):
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    if not settings_doc.probe_42_url:
        frappe.throw("Please set Probe42 Url to proceed with Probe42 fetch")
    url = f"""{settings_doc.probe_42_url}/{cnp_type}/{cin_or_pan}/update"""  # noqa: 501

    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.post(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    request_id = response.get("data", {}).get("request_id")
    if request_id:
        cprb_doc.request_id = response.get("data", {}).get("request_id")
        cprb_doc.request_dateime = frappe.utils.now_datetime()
        cprb_doc.last_updated = frappe.utils.now_datetime()
        cprb_doc.save()


def fetch_lead_from_probe_42(doc, response, is_comprehensive):
    if isinstance(response, str):
        response = json.loads(response)
    lead_doc = {}
    if doc.get("__islocal"):
        lead_doc = frappe.new_doc("Lead")
        lead_doc.lead_name = doc.get("lead_name")
        lead_doc.cin_number = doc.get("cin_number")
        lead_doc.cnp_type = doc.get("cnp_type")
    else:
        lead_doc = frappe.get_doc("Lead", doc.get("name"))
    org_type = "llp" if lead_doc.cnp_type == "LLP" else "company"  # noqa
    company_name = (
        response.get("data", {}).get(org_type, {}).get("legal_name", "")
    )  # noqa
    lead_doc.company_name = company_name
    lead_doc.first_name = company_name
    lead_doc.website = (
        response.get("data", {}).get(org_type, {}).get("website", "")
    )  # noqa
    lead_doc.cnp_is_data_fetched = 1
    lead_doc.save()

    create_lead_contact(response["data"], lead_doc.name, org_type)
    create_lead_address(doc, lead_doc.name)
    create_lead_fetched_address(response["data"][org_type], lead_doc.name)
    if is_comprehensive:
        create_lead_gst_address(response["data"]["gst_details"], lead_doc.name)  # noqa
        create_industry_segment(
            response["data"].get("industry_segments", []), lead_doc.name
        )  # noqa

    return lead_doc


def fetch_probe_42_base_details(cin_or_pan, is_pan, cnp_type):
    """
    Fetch Base Details For a CIN
    """
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    if not settings_doc.probe_42_url:
        frappe.throw("Please set Probe42 Url to proceed with Probe42 fetch")
    url = f"""{settings_doc.probe_42_url}/{cnp_type}/{cin_or_pan}/base-details"""  # noqa: 501
    frappe.logger("utils").exception(url)
    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.get(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    return response


def fetch_probe_42_details(cin_or_pan, is_pan, cnp_type):
    """
    Fetch Comprehensive Details For a CIN.
    """
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    if not settings_doc.probe_42_url:
        frappe.throw("Please set Probe42 Url to proceed with Probe42 fetch")
    url = f"""{settings_doc.probe_42_url}/{cnp_type}/{cin_or_pan}/comprehensive-details"""  # noqa: 501
    frappe.logger("utils").exception(url)
    if is_pan:
        url += "?identifier_type=PAN"
    response = requests.get(
        url,
        headers={
            "x-api-key": settings_doc.get_password("api_secret"),
            "x-api-version": settings_doc.api_version,
        },
    )
    frappe.logger("utils").exception(response.json())
    return response.json()


@frappe.whitelist()
def fetch_base_details_for_opportunity(
    opportunity_name,
    source,
    is_local,
    security_type,
    cnp_type="",
    opportunity_from="",
    party_name="",
    cin_number="",
    product="",
    facility_amt=0,
    issue_size=0,
    tenor_years=0,
    tenor_months=0,
    tenor_days=0,
):
    try:
        opportunity_doc = prepare_opportunity_doc(
            is_local,
            opportunity_name,
            source,
            opportunity_from,
            party_name,
            cnp_type,
            cin_number,
            product,
            facility_amt,
            issue_size,
            tenor_years,
            tenor_months,
            tenor_days,
            security_type,
        )
        cnp_type = (
            "llps" if opportunity_doc.get("cnp_type") == "LLP" else "companies"
        )  # noqa
        org_type = "llp" if cnp_type == "llps" else "company"  # noqa
        is_pan = (
            opportunity_doc.get("cnp_type") == "Individual"
            or opportunity_doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
        )
        if frappe.db.exists("CPRB Company Details", cin_number):
            cprb_doc = frappe.get_doc("CPRB Company Details", cin_number)
            company_details = cprb_doc.payload
        else:
            company_details = fetch_probe_42_base_details(
                cin_number, is_pan, cnp_type
            )  # noqa
            cprb_doc = frappe.get_doc(
                {
                    "doctype": "CPRB Company Details",
                    "company_cin": cin_number,
                    "company_name": company_details.get("data", {})
                    .get(org_type, {})
                    .get("legal_name", ""),
                    "payload": company_details,
                }
            ).insert(ignore_permissions=True)

        # Check For Comprehensive Details
        comprehensive_details = fetch_probe_42_details(
            cin_number, is_pan, cnp_type
        )  # noqa
        if comprehensive_details.get("data"):
            frappe.msgprint("Comprehensive Details Fetched Immediately!")
            company_details = comprehensive_details
            cprb_doc.has_comprehensive_details = 1
            cprb_doc.company_name = (
                company_details.get("data", {})
                .get(org_type, {})
                .get("legal_name", "")  # noqa
            )  # noqa
            cprb_doc.payload = company_details
            cprb_doc.last_updated = frappe.utils.now_datetime()
            cprb_doc.save()
        else:
            # Create Update Request For Comprehensive Details For a Company
            frappe.msgprint(
                "Request is in Queue. Comprehensive details are being fetched from Probe42 database. Please try again after sometime."  # noqa
            )
            create_update_request(cin_number, is_pan, cprb_doc, cnp_type)
        opp_doc = fetch_opportunity_from_probe42(
            company_details,
            opportunity_name,
            source,
            is_local,
            security_type,
            opportunity_from,
            party_name,
            cnp_type,
            cin_number,
            product,
            facility_amt,
            issue_size,
            tenor_years,
            tenor_months,
            tenor_days,
        )
        return opp_doc
    except Exception as e:
        frappe.logger("utils").exception(e)


@frappe.whitelist()
def fetch_base_details_for_customer(
    doc, customer_name, is_local, cin_number=""
):  # noqa
    try:
        doc = json.loads(doc)
        cin_number = doc.get("cin_number")
        is_pan = (
            doc.get("cnp_type") == "Individual"
            or doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
        )
        cnp_type = "llps" if doc.get("cnp_type") == "LLP" else "companies"
        if frappe.db.exists("CPRB Company Details", cin_number):
            cprb_doc = frappe.get_doc("CPRB Company Details", cin_number)
            company_details = cprb_doc.payload
        else:
            company_details = fetch_probe_42_base_details(
                cin_number, is_pan, cnp_type
            )  # noqa
            cprb_doc = frappe.get_doc(
                {
                    "doctype": "CPRB Company Details",
                    "company_cin": cin_number,
                    "company_name": company_details.get("data", {})
                    .get("company", {})
                    .get("legal_name", ""),
                    "payload": company_details,
                }
            ).insert(ignore_permissions=True)

        # Check For Comprehensive Details
        comprehensive_details = fetch_probe_42_details(
            cin_number, is_pan, cnp_type
        )  # noqa
        is_comprehensive = False
        if comprehensive_details.get("data"):
            message = "Comprehensive Details Fetched Immediately!"
            company_details = comprehensive_details
            cprb_doc.has_comprehensive_details = 1
            cprb_doc.company_name = (
                company_details.get("data", {})
                .get("company", {})
                .get("legal_name", "")  # noqa
            )
            cprb_doc.payload = company_details
            cprb_doc.last_updated = frappe.utils.now_datetime()
            cprb_doc.save()
            is_comprehensive = True
        else:
            # Create Update Request For Comprehensive Details For a Company
            message = "Request is in Queue. Comprehensive details are being fetched from Probe42 database. Please try again after sometime."  # noqa

            create_update_request(cin_number, is_pan, cprb_doc, cnp_type)
        customer_name = (
            company_details.get("data", {})
            .get("company", {})
            .get("legal_name", "")  # noqa
        )
        return (
            fetch_customer_from_probe42(
                customer_name,
                company_details,
                is_local,
                is_comprehensive,
                cin_number,  # noqa
            ),
            message,
        )
    except Exception as e:
        frappe.logger("utils").exception(e)


@frappe.whitelist()
def fetch_comprehensive_details_for_customer(
    doc, customer_name, is_local, cin_number=""
):
    doc = json.loads(doc)
    cprb_doc = frappe.get_doc("CPRB Company Details", doc.get("cin_number"))
    cin_or_pan = cprb_doc.get("name")
    is_pan = (
        doc.get("cnp_type") == "Individual"
        or doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
    )
    cnp_type = "llps" if doc.get("cnp_type") == "LLP" else "companies"
    company_details = json.loads(cprb_doc.payload)
    if not cprb_doc.has_comprehensive_details:
        response = _show_update_status(
            cin_or_pan, cprb_doc.request_id, is_pan, cnp_type
        )
        status = response.get("data", {}).get("status")
        if status != "FULFILLED":
            frappe.throw(
                "Request is in Queue. Comprehensive details are being fetched from Probe42 database. The details will be updated automatically when it is available"  # noqa
            )
        comprehensive_details = fetch_probe_42_details(
            cin_or_pan, is_pan, cnp_type
        )  # noqa
        if comprehensive_details.get("data"):
            company_details = comprehensive_details
            cprb_doc.has_comprehensive_details = 1
            cprb_doc.company_name = (
                company_details.get("data", {})
                .get("company", {})
                .get("legal_name", "")  # noqa
            )
            cprb_doc.payload = company_details
            cprb_doc.last_updated = frappe.utils.now_datetime()
            cprb_doc.save()
    is_comprehensive = True
    customer_name = (
        company_details.get("data", {}).get("company", {}).get("legal_name", "")  # noqa
    )
    return fetch_customer_from_probe42(
        customer_name, company_details, is_local, is_comprehensive, cin_number
    )


@frappe.whitelist()
def calculate_tenor_fee_for_sales_invoice(quot):
    doc = frappe.get_doc("Quotation", quot)
    fiscal_doc_list = get_doc_list(
        "Fiscal Year", fields=('["name"]'), filters=[]
    )  # noqa: 501 isort:skip
    for fiscal in fiscal_doc_list:
        fiscal_doc = frappe.get_doc("Fiscal Year", fiscal)

        if (
            fiscal_doc.year_start_date <= date.today()
            and fiscal_doc.year_end_date >= date.today()
        ):
            for item in doc.items:
                if item.cnp_type_of_fee == "Annual Fee":
                    year_end_date = fiscal_doc.year_end_date
                    timedelta_date = timedelta(days=1)
                    if item.cnp_frequency == "Y":
                        item.cnp_next_fee_date = year_end_date + timedelta_date
                    if item.cnp_frequency == "H":
                        half_year = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=6)
                        )
                        now = date.today()
                        if now < half_year:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, half_year)
                                )
                            )
                        else:
                            item.cnp_next_fee_date = (
                                fiscal_doc.year_end_date + timedelta(days=1)
                            )
                    if item.cnp_frequency == "Q":
                        quarter_1 = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=3)
                        )
                        quarter_2 = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=6)
                        )
                        quarter_3 = (
                            fiscal_doc.year_start_date
                            + relativedelta.relativedelta(months=9)
                        )
                        now = date.today()
                        if date.today() < quarter_1:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, quarter_1)
                                )
                            )
                        elif date.today() < quarter_2:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, quarter_2)
                                )
                            )
                        elif date.today() < quarter_3:
                            item.cnp_next_fee_date = (
                                date.today()
                                + relativedelta.relativedelta(
                                    months=date_diff_in_months(now, quarter_3)
                                )
                            )
                        else:
                            item.cnp_next_fee_date = (
                                fiscal_doc.year_end_date + timedelta(days=1)
                            )
                    if item.cnp_frequency == "M":
                        item.cnp_next_fee_date = (
                            date.today()
                            + relativedelta.relativedelta(
                                months=1
                            )  # noqa: 501 isort:skip
                        )
                    fee = (
                        item.rate
                        / 12
                        * date_diff_in_months(
                            date.today(), item.cnp_next_fee_date
                        )  # noqa: 501 isort:skip
                    )
    return {"fees": fee}


@frappe.whitelist()
def cost_center_default_address(cost_center):
    doc = frappe.get_doc("Cost Center", cost_center)
    if doc.cnp_default_address:
        return doc.cnp_default_address
    else:
        doc = get_doc_list(
            "Address",
            fields=('["name"]'),
            filters=[
                ["cnp_cost_center", "=", cost_center],
            ],
        )
        if len(doc) == 1:
            return doc[0].name
    return ""


def get_next_series_number(doctype, fieldname, filters=[]):
    last_entry = frappe.get_all(
        doctype,
        filters=filters,
        fields=[fieldname],
        order_by="creation DESC",
        limit_page_length=1,
    )
    if len(last_entry) != 0:
        last_number = last_entry[0].get(fieldname, "")
        last_number = last_number.split("/")[-1]
    else:
        last_number = None
    if last_number is not None:
        next_number = int(last_number) + 1
    else:
        next_number = 1
    return next_number
