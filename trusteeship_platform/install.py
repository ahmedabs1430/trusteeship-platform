import frappe


def after_install():
    create_property_setters()


def create_property_setters():
    for property_setter in get_property_setters():
        frappe.make_property_setter(property_setter)


def get_property_setters():
    return [
        {
            "doctype": "Address",
            "doctype_or_field": "DocType",
            "property": "quick_entry",
            "property_type": "Check",
            "value": "0",
        }
    ]
