# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

from datetime import datetime

import frappe
from frappe.model.document import Document
from frappe.utils.csvutils import getlink


class CanopiRedemptionAccounting(Document):
    def before_insert(self):
        validate_mutual_fund_opening_entry(self)
        validate_redemption_accounting(self)

    def validate(self):
        validate_redeemed_no_of_units(self)

    def on_submit(self):
        if not self.transaction_details_table:
            frappe.throw("Transaction table can not be blank")

        frappe.db.set_value(
            "Canopi Mutual Funds Opening Entry",
            self.mf_opening,
            "workflow_state",
            "Redeemed",
        )

        new_doc = frappe.new_doc("Journal Entry")
        new_doc.voucher_type = "Mutual Funds"
        new_doc.company = self.office
        new_doc.posting_date = datetime.today()
        for element in self.transaction_details_table:
            if element.dc == "Debit":
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "debit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Redemption Accounting",  # noqa: 501
                        "canopi_reference_name": self.name,
                    },
                )
            else:
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "credit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Redemption Accounting",  # noqa: 501
                        "canopi_reference_name": self.name,
                    },
                )
        new_doc.insert()
        frappe.db.commit()


def validate_redeemed_no_of_units(self):
    if len(self.nav_calculation) and len(self.capital_gain_loss_calculation):
        bought_no_of_units = next(
            nav.no_of_units for nav in self.nav_calculation if nav
        )

        redeemed_no_of_units = next(
            cap.redeemed_no_of_units
            for cap in self.capital_gain_loss_calculation
            if cap
        )

        if redeemed_no_of_units > bought_no_of_units:
            frappe.throw(
                "<b>Redeemed No. of Units</b> cannot be more than <b>No. of Units bought</b>",  # noqa: 501
                title="Error",
            )


def validate_mutual_fund_opening_entry(self):
    mf_list = frappe.get_list(
        self.doctype, filters=[["mf_opening", "=", self.mf_opening]]
    )
    if len(mf_list) > 0:
        frappe.throw(
            "Redemption Accounting already created for selected Mutual Fund Opening Entry"  # noqa: 501
        )


def validate_redemption_accounting(self):
    query = """
    SELECT
        c.parent AS name
    FROM
        `tabCanopi NAV Calculation` c
    INNER JOIN
        `tabCanopi Mutual Funds Opening Entry` p
    ON
        c.parent = p.name
    WHERE
        c.parentfield = 'nav_calculation'
        AND c.parenttype = 'Canopi Mutual Funds Opening Entry'
        AND p.is_redeemed = 0
    ORDER BY
        c.date_of_buying,
        CASE
            WHEN c.date_of_buying = (
                SELECT MAX(c2.date_of_buying)
                FROM `tabCanopi NAV Calculation` c2
                WHERE c2.parent = c.parent
                )
            THEN c.creation
            ELSE NULL
        END;
    """
    mf_list = frappe.db.sql(query, as_dict=1)

    # check first mutual fund opening entry from list
    if self.mf_opening != mf_list[0].name:
        redemption_accounting_entry = frappe.get_list(
            "Canopi Redemption Accounting",
            filters=[["mf_opening", "=", mf_list[0].name]],
        )
        if len(redemption_accounting_entry) > 0:
            frappe.throw(
                (
                    "Units can be redeemed only according to FIFO, please finish redeeming units from {}."  # noqa: 501
                ).format(
                    getlink(
                        "Canopi Redemption Accounting",
                        redemption_accounting_entry[0].name,
                    )
                )
            )

        frappe.throw(
            (
                "According to FIFO, please create Redemption entry against {} and Redeem units from there"  # noqa: 501
            ).format(
                getlink("Canopi Mutual Funds Opening Entry", mf_list[0].name)
            )  # noqa: 501
        )
