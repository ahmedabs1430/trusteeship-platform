// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, locals, updateBreadcrumbs */
frappe.ui.form.on('Canopi Redemption Accounting', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Investments');
    });
    setMFOpeningFilter(frm);

    setAccountNoFilter(frm);
    setGlAccountFilter(frm);
    if (!frm.doc.__islocal) {
      approvalAction(frm);
    }
    hideConnections(frm);
    configureCapitalGainLossFontColor(frm);
  },

  mf_opening: frm => {
    getFields(frm);
  },
});

function setAccountNoFilter(frm) {
  frm.set_query('account_no', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.fields_dict.transaction_details_table.grid.get_field(
    'account_number',
  ).get_query = function (doc, cdt, cdn) {
    return {
      filters: [['Bank Account', 'company', '=', frm.doc.office]],
    };
  };
}

function setGlAccountFilter(frm) {
  frm.set_query('brn_gl_access_code', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });

  frm.fields_dict.transaction_details_table.grid.get_field(
    'gl_account',
  ).get_query = function (doc, cdt, cdn) {
    return {
      filters: [['Account', 'company', '=', frm.doc.office]],
    };
  };
}
function getFields(frm) {
  if (frm.doc.mf_opening) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Canopi Mutual Funds Opening Entry',
        doc_name: frm.doc.mf_opening,
      },
      freeze: true,
      callback: r => {
        frm.set_value('office', r.message.office);
        frm.set_value('branch_code', r.message.branch_code);
        frm.set_value('account_no', r.message.account_no);
        frm.set_value('currency', r.message.currency);
        frm.set_value('schema', r.message.schema);
        frm.doc.nav_calculation = [];
        if (r.message.nav_calculation.length) {
          frm.add_child('nav_calculation', {
            date_of_buying: r.message.nav_calculation[0].date_of_buying,
            no_of_units: r.message.nav_calculation[0].no_of_units,
            nav: r.message.nav_calculation[0].nav,
            amount: r.message.nav_calculation[0].amount,
          });
        }
        calculateTotalOutstandingUnits(frm);
        frm.refresh_fields();
      },
    });
  }
}

frappe.ui.form.on('Canopi Capital Gain Loss Calculation', {
  form_render: (frm, cdt, cdn) => {
    configureCapitalGainLossFontColor(frm);
  },

  redeemed_no_of_units: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    validateRedeemedNoOfUnits(frm, row);

    calculateRedeemedAmount(row);
    calculateCapitalGainLoss(frm, row);
    calculateRowOutstandingUnits(frm, row);
    calculateTotalOutstandingUnits(frm);
    frm.refresh_fields();

    configureCapitalGainLossFontColor(frm);
  },

  redeemed_nav: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    calculateRedeemedAmount(row);
    calculateCapitalGainLoss(frm, row);

    frm.refresh_fields();
    configureCapitalGainLossFontColor(frm);
  },
});

function validateRedeemedNoOfUnits(frm, row) {
  if (
    frm.doc.nav_calculation.length &&
    frm.doc.capital_gain_loss_calculation.length
  ) {
    const bought_no_of_units = frm.doc.nav_calculation.find(
      x => x,
    )?.no_of_units;
    const redeemed_no_of_units = frm.doc.capital_gain_loss_calculation.reduce(
      (total, obj) => total + obj.redeemed_no_of_units,
      0,
    );

    if (redeemed_no_of_units > bought_no_of_units) {
      row.redeemed_no_of_units = 0;
      calculateRedeemedAmount(row);
      calculateCapitalGainLoss(frm, row);
      calculateRowOutstandingUnits(frm, row);
      calculateTotalOutstandingUnits(frm);

      frm.refresh_fields();
      frappe.throw(
        'Total Redeemed No. of Units cannot exceed No. of Units bought.',
      );
    }
  }
}

function calculateRowOutstandingUnits(frm, row) {
  const noOfUnits =
    row.idx > 1
      ? frm.doc.capital_gain_loss_calculation[row.idx - 2]?.outstanding_units
      : frm.doc.nav_calculation?.find(x => x)?.no_of_units;
  row.outstanding_units =
    noOfUnits - (row.redeemed_no_of_units ? row.redeemed_no_of_units : 0);
}

function calculateTotalOutstandingUnits(frm) {
  const redeemed_no_of_units = frm.doc.capital_gain_loss_calculation.reduce(
    (total, obj) => total + obj.redeemed_no_of_units,
    0,
  );
  frm.set_value(
    'total_outstanding_units',
    frm.doc.nav_calculation?.find(x => x)?.no_of_units - redeemed_no_of_units,
  );
}

function calculateRedeemedAmount(row) {
  row.amount = !isNaN(row.redeemed_no_of_units * row.redeemed_nav)
    ? row.redeemed_no_of_units * row.redeemed_nav
    : 0;
}

function calculateCapitalGainLoss(frm, row) {
  const navCalculation = frm.doc.nav_calculation.find(x => x);
  const capitalGainLoss =
    row.redeemed_no_of_units * row.redeemed_nav -
    row.redeemed_no_of_units * navCalculation.nav;
  row.capital_gain_loss = !isNaN(capitalGainLoss) ? capitalGainLoss : 0;
}

function configureCapitalGainLossFontColor(frm) {
  frm.doc?.capital_gain_loss_calculation?.forEach(x => {
    let rowElement = $(`div[data-name=${x.name}]`);
    const fieldDiv = rowElement?.find(
      'div[data-fieldname="capital_gain_loss"]',
    );
    let fieldInput = rowElement?.find(
      'input[data-fieldname="capital_gain_loss"]',
    );

    if (!fieldInput.length) {
      $("div[data-fieldname='capital_gain_loss']").click();
      rowElement = $(`div[data-name=${x.name}]`);
      fieldInput = rowElement?.find(
        'input[data-fieldname="capital_gain_loss"]',
      );
    }

    if (x.capital_gain_loss > 0) {
      fieldDiv.css('color', 'green', '!important');
      fieldInput.css('color', 'green', '!important');
    } else {
      fieldDiv.css('color', 'red', '!important');
      fieldInput.css('color', 'red', '!important');
    }
  });
}

frappe.ui.form.on('Canopi Redemption Accounting Transaction Details', {
  amount: function (frm, cdt, cdn) {
    setTotalDeitCredit(frm, cdt, cdn);
    setBaseCurrencyAmount(frm, cdt, cdn);
  },

  dc: function (frm, cdt, cdn) {
    setTotalDeitCredit(frm, cdt, cdn);
  },

  transaction_details_table_add: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    const transactionAmount = frm.doc.transaction_amount;
    let glAccount = '';
    if (row.idx === 1) {
      frappe.model.set_value(cdt, cdn, 'amount', transactionAmount);
      frm.refresh_fields('amount');

      frappe.model.set_value(cdt, cdn, 'dc', 'Debit');
      frm.refresh_fields('dc');

      glAccount = '2020005010 - Revenue A/c 695010200000134 - ATSL';
      frappe.model.set_value(cdt, cdn, 'gl_account', glAccount);
      frm.refresh_fields('gl_account');
    }

    if (row.idx === 2) {
      frappe.model.set_value(cdt, cdn, 'dc', 'Credit');
      frm.refresh_fields('dc');

      glAccount = '2000001025 - Investment in Axis Liquid Fund - ATSL';
      frappe.model.set_value(cdt, cdn, 'gl_account', glAccount);
      frm.refresh_fields('gl_account');
    }

    if (row.idx === 3) {
      frappe.model.set_value(cdt, cdn, 'dc', 'Credit');
      frm.refresh_fields('dc');

      glAccount = '6010001001 - Capital Gain on Liquid Fund - ATSL';
      frappe.model.set_value(cdt, cdn, 'gl_account', glAccount);
      frm.refresh_fields('gl_account');
    }

    setTotalDeitCredit(frm, cdt, cdn);
  },

  transaction_details_table_remove: function (frm, cdt, cdn) {
    setTotalDeitCredit(frm, cdt, cdn);
  },
});

function setTotalDeitCredit(frm, cdt, cdn) {
  let totalDebit = 0;
  let totalCredit = 0;
  frm.doc.transaction_details_table.forEach(function (d) {
    if (d.dc === 'Debit') totalDebit += d.amount;
    else totalCredit += d.amount;
  });
  frm.set_value('total_debit', totalDebit);
  frm.refresh_field('total_debit');
  frm.set_value('total_credit', totalCredit);
  frm.refresh_field('total_credit');
}

function setBaseCurrencyAmount(frm, cdt, cdn) {
  const amount = frappe.model.get_value(cdt, cdn, 'amount');
  let baseCurrencyAmount = amount;

  let companyCurrency = frappe.get_doc(
    ':Company',
    frm.doc.office,
  ).default_currency;

  if (companyCurrency === '') {
    companyCurrency = 'INR';
  }
  if (companyCurrency === frm.doc.currency) {
    frappe.model.set_value(
      cdt,
      cdn,
      'base_currency_amount',
      baseCurrencyAmount,
    );
    frm.refresh_fields('base_currency_amount');
  } else {
    frappe.call({
      method: 'erpnext.setup.utils.get_exchange_rate',
      args: {
        from_currency: frm.doc.currency,
        to_currency: companyCurrency,
      },
      callback: function (r, rt) {
        baseCurrencyAmount = amount * r.message;
        frappe.model.set_value(
          cdt,
          cdn,
          'base_currency_amount',
          baseCurrencyAmount,
        );
        frm.refresh_fields('base_currency_amount');
      },
    });
  }
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Rejected'
  ) {
    frm.disable_form();
    frm.disable_save();
  }
  if (
    frm.doc.workflow_state === 'Draft' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.add_custom_button(
      'Send For Approval',
      function () {
        sendApprovalMail(frm);
      },
      __('Action'),
    );
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.add_custom_button(
      'Approve',
      function () {
        emailActionApprove(frm);
      },
      __('Action'),
    );
    frm.add_custom_button(
      'Reject',
      function () {
        emailActionReject(frm);
      },
      __('Action'),
    );
  }
  const buttons = document.querySelectorAll('.ellipsis');

  buttons.forEach(element => {
    if (element.innerHTML.includes('Action')) {
      element.style.backgroundColor = '#ed1164';
      element.style.color = 'white';
    }
  });
}
function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Canopi Redemption Accounting',
      doctype_name: 'canopi-redemption-accounting',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionApprove(frm) {
  let action_url =
    'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.';
  action_url += 'canopi_fixed_deposit_opening_entry.email_action';

  frappe.call({
    method: action_url,
    args: {
      doctype: 'Canopi Redemption Accounting',
      docname: frm.doc.name,
      action: '1',
      doctype_name: 'canopi-redemption-accounting',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionReject(frm) {
  let action_url =
    'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.';
  action_url += 'canopi_fixed_deposit_opening_entry.email_action';

  frappe.call({
    method: action_url,
    args: {
      doctype: 'Canopi Redemption Accounting',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'canopi-redemption-accounting',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function hideConnections(frm) {
  if (frm.doc.workflow_state === 'Rejected')
    $("[data-doctype='Journal Entry']").hide();
  else $("[data-doctype='Journal Entry']").show();
}

function setMFOpeningFilter(frm) {
  frm.set_query('mf_opening', function () {
    return {
      filters: [['workflow_state', '!=', 'Rejected']],
    };
  });
}
