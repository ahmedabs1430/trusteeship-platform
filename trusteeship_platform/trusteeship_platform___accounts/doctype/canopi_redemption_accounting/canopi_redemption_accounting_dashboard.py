from frappe import _


def get_data():
    return {
        "fieldname": "canopi_reference_name",
        "transactions": [
            {
                "label": _(""),
                "items": ["Journal Entry"],
            },
        ],
    }
