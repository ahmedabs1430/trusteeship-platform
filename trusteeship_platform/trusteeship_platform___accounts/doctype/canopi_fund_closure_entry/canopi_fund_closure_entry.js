// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs */
/* eslint-env jquery */
frappe.ui.form.on('Canopi Fund Closure Entry', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Investments');
    });

    setAccountNumberFilter(frm);
    setFDMaturityFilter(frm);
    setGlAccountFilter(frm);
    if (frm.doc.__islocal !== 1) {
      interestCertificateButton(frm);
    }
    if (!frm.doc.__islocal) {
      approvalAction(frm);
      frm.doc.certificate_key = 0;
    }
  },

  fd_maturity: frm => {
    getFields(frm);
  },
  account_no: frm => {
    getBranchCode(frm);
  },
});
function setAccountNumberFilter(frm) {
  if (frm.doc.office !== '') {
    frm.fields_dict.transaction_details_table.grid.get_field(
      'account_number',
    ).get_query = function (doc, cdt, cdn) {
      return {
        filters: [['Bank Account', 'company', '=', frm.doc.office]],
      };
    };
  }
}
function setGlAccountFilter(frm) {
  if (frm.doc.office !== '') {
    frm.fields_dict.transaction_details_table.grid.get_field(
      'gl_account',
    ).get_query = function (doc, cdt, cdn) {
      return {
        filters: [['Account', 'company', '=', frm.doc.office]],
      };
    };
  }
}
function getFields(frm) {
  if (frm.doc.fd_maturity) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Canopi Fixed Deposit Maturity',
        doc_name: frm.doc.fd_maturity,
      },
      freeze: true,
      callback: r => {
        frm.doc.office = r.message.office;
        frm.doc.branch_code = r.message.branch_code;
        frm.doc.account_no = r.message.account_no;
        frm.doc.currency = r.message.currency;
        frm.refresh_field('office');
        frm.refresh_field('branch_code');
        frm.refresh_field('account_no');
        frm.refresh_field('currency');
      },
    });
  }
}
function getBranchCode(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'Bank Account',
      doc_name: frm.doc.account_no,
    },
    freeze: true,
    callback: r => {
      frm.doc.branch_code = r.message.branch_code;
      frm.refresh_field('branch_code');
    },
  });
}
frappe.ui.form.on('Canopi Fund Closure Transaction Details', {
  amount: function (frm, cdt, cdn) {
    let totalDebit = 0;
    let totalCredit = 0;
    frm.doc.transaction_details_table.forEach(function (d) {
      if (d.dc === 'Debit') totalDebit += d.amount;
      else totalCredit += d.amount;
    });
    frm.set_value('total_debit', totalDebit);
    frm.set_value('total_credit', totalCredit);
    setBaseCurrencyAmount(frm, cdt, cdn);
  },
  dc: function (frm, cdt, cdn) {
    let totalDebit = 0;
    let totalCredit = 0;
    frm.doc.transaction_details_table.forEach(function (d) {
      if (d.dc === 'Debit') totalDebit += d.amount;
      else totalCredit += d.amount;
    });
    frm.set_value('total_debit', totalDebit);
    frm.set_value('total_credit', totalCredit);
  },
  transaction_details_table_remove: function (frm, cdt, cdn) {
    let totalDebit = 0;
    let totalCredit = 0;
    frm.doc.transaction_details_table.forEach(function (d) {
      if (d.dc === 'Debit') totalDebit += d.amount;
      else totalCredit += d.amount;
    });
    frm.set_value('total_debit', totalDebit);
    frm.set_value('total_credit', totalCredit);
  },
});
function setBaseCurrencyAmount(frm, cdt, cdn) {
  const amount = frappe.model.get_value(cdt, cdn, 'amount');
  let baseCurrencyAmount = amount;

  let companyCurrency = frappe.get_doc(
    ':Company',
    frm.doc.office,
  ).default_currency;
  if (companyCurrency === '') {
    companyCurrency = 'INR';
  }
  if (companyCurrency === frm.doc.currency) {
    frappe.model.set_value(
      cdt,
      cdn,
      'base_currency_amount',
      baseCurrencyAmount,
    );
    frm.refresh_fields('base_currency_amount');
  } else {
    frappe.call({
      method: 'erpnext.setup.utils.get_exchange_rate',
      args: {
        from_currency: frm.doc.currency,
        to_currency: companyCurrency,
      },
      callback: function (r, rt) {
        baseCurrencyAmount = amount * r.message;
        frappe.model.set_value(
          cdt,
          cdn,
          'base_currency_amount',
          baseCurrencyAmount,
        );
        frm.refresh_fields('base_currency_amount');
      },
    });
  }
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Rejected'
  ) {
    frm.disable_form();
    frm.disable_save();
  }
  if (
    frm.doc.workflow_state === 'New' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.add_custom_button(
      'Send For Approval',
      function () {
        sendApprovalMail(frm);
      },
      __('Action'),
    );
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.add_custom_button(
      'Approve',
      function () {
        emailActionApprove(frm);
      },
      __('Action'),
    );
    frm.add_custom_button(
      'Reject',
      function () {
        emailActionReject(frm);
      },
      __('Action'),
    );
  } else if (
    frm.doc.workflow_state === 'Approved' &&
    frappe.user.has_role('Accounts User') &&
    frm.doc.certificate_key === 1
  ) {
    frm.add_custom_button(
      'Submit',
      function () {
        emailActionSubmit(frm);
      },
      __('Action'),
    );
  }
  const buttons = document.querySelectorAll('.ellipsis');

  buttons.forEach(element => {
    if (element.innerHTML.includes('Action')) {
      element.style.backgroundColor = '#ed1164';
      element.style.color = 'white';
    }
  });
}
function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Canopi Fund Closure Entry',
      doctype_name: 'canopi-fund-closure-entry',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionApprove(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fund_closure_entry.canopi_fund_closure_entry.email_action',
    args: {
      doctype: 'Canopi Fund Closure Entry',
      docname: frm.doc.name,
      action: '1',
      doctype_name: 'canopi-fund-closure-entry',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionReject(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fund_closure_entry.canopi_fund_closure_entry.email_action',
    args: {
      doctype: 'Canopi Fund Closure Entry',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'canopi-fund-closure-entry',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function emailActionSubmit(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fund_closure_entry.canopi_fund_closure_entry.email_action',
    args: {
      doctype: 'Canopi Fund Closure Entry',
      docname: frm.doc.name,
      action: '3',
      doctype_name: 'canopi-fund-closure-entry',
      redirect: false,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function interestCertificateButton(frm) {
  frm.add_custom_button('Interest Certificate Upload', function () {
    const x = document.createElement('INPUT');
    x.setAttribute('type', 'file');
    x.setAttribute('multiple', '');
    x.setAttribute('id', 'upload');
    $('body').append(x);
    x.click();
    $('#upload').change(async event => {
      const base = [];
      const files = [...event.target.files].map(async file => {
        base.push({
          filename: file.name,
          file: await getBase64(file),
        });
      });
      await Promise.all(files);
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fund_closure_entry.canopi_fund_closure_entry.send_file_to_s3',
        freeze: true,
        args: {
          base_arr: base,
          docname: frm.doc.name,
          doctype: frm.doc.doctype,
        },
        callback: r => {
          x.remove();
          frm.reload_doc();
          frappe.show_alert(
            {
              message: 'Uploaded',
              indicator: 'green',
            },
            5,
          );
        },
      });
    });
  });

  if (frm.doc.__islocal) {
    frm.doc.certificate_key = 0;
    frm.refresh_field('certificate_key');
  }
  if (frm.doc.certificate_key === 0 && frm.doc.workflow_state === 'Approved') {
    frm.set_intro(' ');
    frm.set_intro('Upload Interest Certificate to proceed...');
  }
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

function setFDMaturityFilter(frm) {
  frm.set_query('fd_maturity', function () {
    return {
      filters: [['workflow_state', '!=', 'Rejected']],
    };
  });
}
