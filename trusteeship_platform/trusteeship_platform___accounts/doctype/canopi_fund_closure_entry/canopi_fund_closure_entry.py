# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import base64
import json
from datetime import datetime

import frappe
from frappe.model.document import Document
from frappe.utils import validate_url
from frappe.utils.data import cstr, get_url

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    asign_task,
    upload_file_to_s3,
    validate_s3_settings,
)


class CanopiFundClosureEntry(Document):
    def on_submit(self):
        if not self.transaction_details_table:
            frappe.throw("Transaction table can not be blank")

        if self.fd_maturity:
            frappe.db.set_value(
                "Canopi Fixed Deposit Maturity",
                self.fd_maturity,
                "workflow_state",
                "Closed",
            )

            doc = frappe.get_doc(
                "Canopi Fixed Deposit Maturity", self.fd_maturity
            )  # noqa: 501
            fd_opening = doc.fd_opening

            frappe.db.set_value(
                "Canopi Fixed Deposit Opening Entry",
                fd_opening,
                "workflow_state",
                "Closed",
            )

        new_doc = frappe.new_doc("Journal Entry")
        new_doc.voucher_type = "Fixed Deposit"
        new_doc.company = self.office
        new_doc.posting_date = datetime.today()
        for element in self.transaction_details_table:
            if element.dc == "Debit":
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "debit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Fund Closure Entry",
                        "canopi_reference_name": self.name,
                    },
                )
            else:
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "credit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Fund Closure Entry",
                        "canopi_reference_name": self.name,
                    },
                )
        new_doc.insert()
        frappe.db.commit()


@frappe.whitelist()
def send_file_to_s3(base_arr, doctype, docname):
    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    doc = frappe.get_doc(doctype, docname)
    json_object = json.loads(base_arr)
    for file in json_object:
        key = (
            cstr(frappe.local.site)
            + "/"
            + doctype
            + "/"
            + doc.name
            + "/"
            + file["filename"]
        )
        file_data = file["file"]
        file_data = file_data[
            file_data.find(",") + 1 :  # noqa: E203
        ]  # noqa: 501 isort:skip
        new_file_data = base64.b64decode(file_data)
        upload_file_to_s3(settings_doc, key, new_file_data)

        file_doc = frappe.utils.file_manager.save_file(
            fname=file["filename"],
            content=new_file_data,
            dt=doctype,
            dn=docname,
            folder=None,
            is_private=0,
        )
        doc.append("attachments", file_doc)
    doc.certificate_key = 1
    doc.save()


@frappe.whitelist(allow_guest=True)
def email_action(
    doctype, docname, action, doctype_name, redirect=True, email_token=""
):  # noqa: 501
    doc = frappe.get_doc(doctype, docname)
    if action == "1":
        status = "Approved"
    if action == "2":
        status = "Rejected"
    if action == "3":
        status = "Submitted"
    validate_url("Accounts Manager")
    if action != "3":
        if (
            doc.email_token != email_token
            or doc.email_token_expiry <= datetime.now()  # noqa: 501
        ):  # noqa: 501
            frappe.throw("The link you followed has expired")
    frappe.db.set_value(doctype, docname, "email_token", "")
    frappe.db.set_value(doctype, docname, "email_token_expiry", None)  # noqa: 501
    if status == "Submitted":
        frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(doctype, docname, "workflow_state", status)

    frappe.db.commit()
    url = get_url()
    my_attachments = []

    content1 = f"""<h2>{doctype} {status}</h2>"""
    content3 = f"""
        <p>{docname} in {doctype} is {status}</p>"""
    cont = """<style>

    </style>"""

    recipient = []

    Users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Accounts User"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for usr in Users:
        recipient.append(usr.email)
        asign_task(
            {
                "assign_to": [usr.email],
                "doctype": doctype,
                "name": docname,
                "description": "",
                "priority": "High",
                "notify": 1,
                "action": status,
            }
        )
    content2 = f""" <p><a href="{url}/app/{doctype_name}/{doc.name}">{doc.name} </a></p>"""  # noqa: 501
    frappe.sendmail(
        recipients=recipient,
        subject=doctype + " " + status,
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )
    if redirect is True:
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = (
            url + "/app/" + doctype_name + "/" + docname
        )  # noqa: 501
