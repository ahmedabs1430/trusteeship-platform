# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

from datetime import datetime

import frappe
from frappe.model.document import Document
from frappe.utils.data import get_url

from trusteeship_platform.custom_methods import asign_task, validate_user


class CanopiFixedDepositOpeningEntry(Document):
    def on_submit(self):
        if not self.transaction_details:
            frappe.throw("Transaction table can not be blank")
        new_doc = frappe.new_doc("Journal Entry")
        new_doc.voucher_type = "Fixed Deposit"
        new_doc.company = self.office
        new_doc.posting_date = datetime.today()
        for element in self.transaction_details:
            if element.dc == "Debit":
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "debit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Fixed Deposit Opening Entry",  # noqa: 501
                        "canopi_reference_name": self.name,
                    },
                )
            else:
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "credit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Fixed Deposit Opening Entry",  # noqa: 501
                        "canopi_reference_name": self.name,
                    },
                )
        new_doc.insert()
        frappe.db.commit()


# Needs improvments
@frappe.whitelist(allow_guest=True)
def email_action(
    doctype,
    docname,
    action,
    doctype_name,
    redirect=True,
    email_token="",
):  # noqa: 501
    doc = frappe.get_doc(doctype, docname)
    validate_user("Accounts Manager")
    if action == "1":
        status = "Approved"
    if action == "2":
        status = "Rejected"

    if (
        doctype == "Canopi Fixed Deposit Opening Entry"
        or doctype == "Canopi Mutual Funds Opening Entry"
        or doctype == "Canopi Redemption Accounting"
    ):  # noqa: 501
        if (
            doc.email_token != email_token
            or doc.email_token_expiry <= datetime.now()  # noqa: 501
        ):  # noqa: 501
            frappe.throw("The link you followed has expired")
        frappe.db.set_value(doctype, docname, "email_token", "")
        frappe.db.set_value(doctype, docname, "email_token_expiry", None)  # noqa: 501

    frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(doctype, docname, "workflow_state", status)

    # temporary solution
    if doctype == "Canopi Redemption Accounting" and status == "Approved":
        frappe.db.set_value(
            "Canopi Mutual Funds Opening Entry",
            doc.mf_opening,
            "is_redeemed",
            1,  # noqa: 501
        )

    if status == "Rejected":
        frappe.db.set_value(doctype, docname, "docstatus", 2)

    frappe.db.commit()
    url = get_url()
    my_attachments = []

    content1 = f"""<h2>{doctype} {status}</h2>"""
    content3 = f"""
        <p>{docname} in {doctype} is {status}</p>"""
    cont = """<style>

    </style>"""

    recipient = []

    Users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Accounts User"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for usr in Users:
        recipient.append(usr.email)
        asign_task(
            {
                "assign_to": [usr.email],
                "doctype": doctype,
                "name": docname,
                "description": "",
                "priority": "High",
                "notify": 1,
                "action": status,
            }
        )
    content2 = f""" <p><a href="{url}/app/{doctype_name}/{doc.name}">{doc.name} </a></p>"""  # noqa: 501
    frappe.sendmail(
        recipients=recipient,
        subject=doctype + " " + status,
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )
    if redirect is True:
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = (
            url + "/app/" + doctype_name + "/" + docname
        )  # noqa: 501
