// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, locals, updateBreadcrumbs */
frappe.ui.form.on('Canopi Fixed Deposit Opening Entry', {
  refresh: function (frm) {
    if (!frm.doc.__islocal) {
      approvalAction(frm);
    }
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Investments');
    });

    setAccountNoFilter(frm);
    setGlAccountFilter(frm);
    addFixedDepositMaturityBtn(frm);
    hideConnections(frm);
  },

  account_no: frm => {
    getFields(frm);
  },
  effective_date: frm => {
    calculateMaturityDate(frm);
  },
  tenor_month: frm => {
    calculateMaturityDate(frm);
    calculateMaturityValue(frm);
  },
  tenor_days: frm => {
    calculateMaturityDate(frm);
    calculateMaturityValue(frm);
  },
  deposit_amount: frm => {
    calculateMaturityValue(frm);
  },
  interest_rate: frm => {
    calculateMaturityValue(frm);
  },
});

function calculateMaturityValue(frm) {
  let tmonth = frm.doc.tenor_month;
  let tdays = frm.doc.tenor_days;
  if (!tmonth) {
    tmonth = 0;
  }
  if (!tdays) {
    tdays = 0;
  }
  const depositAmount = frm.doc.deposit_amount;
  const interestRate = frm.doc.interest_rate;
  let tenor = parseFloat(tmonth) / 12 + parseFloat(tdays) / 365;
  tenor = tenor.toFixed(2);
  let intrest = (parseFloat(depositAmount) * parseFloat(interestRate)) / 100;
  intrest = intrest * tenor;
  const maturityValue = parseFloat(depositAmount) + intrest;
  frm.doc.maturity_value = maturityValue;
  frm.refresh_field('maturity_value');
}
function calculateMaturityDate(frm) {
  const edate = frm.doc.effective_date;
  const tmonth = frm.doc.tenor_month;
  const tdays = frm.doc.tenor_days;
  let maturityDate = edate;
  if (edate !== '' && tmonth > 0) {
    maturityDate = addMonths(tmonth, edate);
  }
  if (edate !== '' && tdays > 0) {
    maturityDate = addDays(tdays, maturityDate);
  }
  frm.doc.maturity_date = maturityDate;
  frm.refresh_field('maturity_date');
}
function addMonths(numOfMonths, edate) {
  const date = new Date(edate);
  date.setMonth(date.getMonth() + numOfMonths);
  const newDate =
    date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  return newDate;
}
function addDays(numOfDays, edate) {
  const date = new Date(edate);
  date.setDate(date.getDate() + numOfDays);
  const newDate =
    date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
  return newDate;
}
function setAccountNoFilter(frm) {
  frm.set_query('account_no', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.fields_dict.transaction_details.grid.get_field(
    'account_number',
  ).get_query = function (doc, cdt, cdn) {
    return {
      filters: [['Bank Account', 'company', '=', frm.doc.office]],
    };
  };
}
function setGlAccountFilter(frm) {
  frm.set_query('source_gl_access_code', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.set_query('source_of_renewal', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.fields_dict.transaction_details.grid.get_field('gl_account').get_query =
    function (doc, cdt, cdn) {
      return {
        filters: [['Account', 'company', '=', frm.doc.office]],
      };
    };
}
function addFixedDepositMaturityBtn(frm) {
  if (frm.doc.docstatus === '1') {
    frm.add_custom_button('Fixed Deposit Maturity', () => {
      frappe.model.open_mapped_doc({
        method:
          'trusteeship_platform.custom_methods.make_fixed_deposit_maturity',
        frm,
      });
    });
  }
}
function getFields(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'Bank Account',
      doc_name: frm.doc.account_no,
    },
    freeze: true,
    callback: r => {
      frm.doc.account_type = r.message.account_type;
      frm.doc.branch_code = r.message.branch_code;
      frm.doc.bank_code = r.message.bank;
      frm.refresh_field('account_type');
      frm.refresh_field('branch_code');
      frm.refresh_field('bank_code');
    },
  });
}
frappe.ui.form.on(
  'Canopi Fixed Deposit Transaction Details',
  'transaction_details_add',
  function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    const depositAmount = frm.doc.deposit_amount;
    frappe.model.set_value(cdt, cdn, 'amount', depositAmount);
    frm.refresh_fields('amount');
    if (row.idx === 2) {
      frappe.model.set_value(cdt, cdn, 'dc', 'Credit');
      frm.refresh_fields('dc');
    }
    const companyCurrency = frappe.get_doc(
      ':Company',
      frm.doc.office,
    ).default_currency;
    let baseCurrencyAmount = depositAmount;
    if (companyCurrency === frm.doc.currency) {
      frappe.model.set_value(
        cdt,
        cdn,
        'base_currency_amount',
        baseCurrencyAmount,
      );
      frm.refresh_fields('base_currency_amount');
    } else {
      frappe.call({
        method: 'erpnext.setup.utils.get_exchange_rate',
        args: {
          from_currency: frm.doc.currency,
          to_currency: companyCurrency,
          transaction_date: frm.doc.entered_on_date,
        },
        callback: function (r, rt) {
          baseCurrencyAmount = depositAmount * r.message;
          frappe.model.set_value(
            cdt,
            cdn,
            'base_currency_amount',
            baseCurrencyAmount,
          );
          frm.refresh_fields('base_currency_amount');
        },
      });
    }
  },
);
frappe.ui.form.on('Canopi Fixed Deposit Transaction Details', {
  amount: function (frm, cdt, cdn) {
    setBaseCurrencyAmount(frm, cdt, cdn);
  },
});
function setBaseCurrencyAmount(frm, cdt, cdn) {
  const amount = frappe.model.get_value(cdt, cdn, 'amount');
  let baseCurrencyAmount = amount;

  let companyCurrency = frappe.get_doc(
    ':Company',
    frm.doc.office,
  ).default_currency;
  if (companyCurrency === '') {
    companyCurrency = 'INR';
  }
  if (companyCurrency === frm.doc.currency) {
    frappe.model.set_value(
      cdt,
      cdn,
      'base_currency_amount',
      baseCurrencyAmount,
    );
    frm.refresh_fields('base_currency_amount');
  } else {
    frappe.call({
      method: 'erpnext.setup.utils.get_exchange_rate',
      args: {
        from_currency: frm.doc.currency,
        to_currency: companyCurrency,
      },
      callback: function (r, rt) {
        baseCurrencyAmount = amount * r.message;
        frappe.model.set_value(
          cdt,
          cdn,
          'base_currency_amount',
          baseCurrencyAmount,
        );
        frm.refresh_fields('base_currency_amount');
      },
    });
  }
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Rejected'
  ) {
    frm.disable_form();
    frm.disable_save();
  }
  if (
    frm.doc.workflow_state === 'Draft' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.add_custom_button(
      'Send For Approval',
      function () {
        sendApprovalMail(frm);
      },
      __('Action'),
    );
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.add_custom_button(
      'Approve',
      function () {
        emailActionApprove(frm);
      },
      __('Action'),
    );
    frm.add_custom_button(
      'Reject',
      function () {
        emailActionReject(frm);
      },
      __('Action'),
    );
  }
  const buttons = document.querySelectorAll('.ellipsis');

  buttons.forEach(element => {
    if (element.innerHTML.includes('Action')) {
      element.style.backgroundColor = '#ed1164';
      element.style.color = 'white';
    }
  });
}
function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Canopi Fixed Deposit Opening Entry',
      doctype_name: 'canopi-fixed-deposit-opening-entry',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionApprove(frm) {
  let action_url =
    'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.';
  action_url += 'canopi_fixed_deposit_opening_entry.email_action';

  frappe.call({
    method: action_url,
    args: {
      doctype: 'Canopi Fixed Deposit Opening Entry',
      docname: frm.doc.name,
      action: '1',
      doctype_name: 'canopi-fixed-deposit-opening-entry',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionReject(frm) {
  let action_url =
    'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.';
  action_url += 'canopi_fixed_deposit_opening_entry.email_action';

  frappe.call({
    method: action_url,
    args: {
      doctype: 'Canopi Fixed Deposit Opening Entry',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'canopi-fixed-deposit-opening-entry',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function hideConnections(frm) {
  if (frm.doc.workflow_state === 'Rejected') {
    $("[data-doctype='Journal Entry']").hide();
    $("[data-doctype='Canopi Fixed Deposit Maturity']").hide();
  } else {
    $("[data-doctype='Journal Entry']").show();
    $("[data-doctype='Canopi Fixed Deposit Maturity']").show();
  }
}
