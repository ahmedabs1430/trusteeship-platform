# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt


from datetime import datetime

import frappe
from frappe.model.document import Document
from frappe.utils.data import get_url

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    asign_task,
    get_doc_list,
    validate_user,
)


class CanopiFixedDepositMaturity(Document):
    def validate(self):
        if self.closure_date and self.effective_date:
            if (self.closure_date) <= (self.effective_date):
                frappe.throw(
                    "Closure date should not be equal or early compare to the effective date"  # noqa: 501
                )

    def on_submit(self):
        frappe.db.set_value(
            "Canopi Fixed Deposit Opening Entry",
            self.fd_opening,
            "workflow_state",
            "Matured",
        )
        frappe.db.commit()

    def before_save(self):
        filters = [
            ["workflow_state", "!=", "Rejected"],
            ["fd_opening", "=", self.fd_opening],
        ]
        doc_list = get_doc_list(
            "Canopi Fixed Deposit Maturity",
            filters=filters,
        )
        if doc_list:
            frappe.throw("FD Opening must be unique...")


@frappe.whitelist(allow_guest=True)
def email_action(
    doctype,
    docname,
    action,
    doctype_name,
    email_token,
    redirect=True,
):  # noqa: 501
    doc = frappe.get_doc(doctype, docname)
    if (
        doc.email_token != email_token
        or doc.email_token_expiry <= datetime.now()  # noqa: 501
    ):  # noqa: 501
        frappe.throw("The link you followed has expired")
    frappe.db.set_value(doctype, docname, "email_token", "")
    frappe.db.set_value(doctype, docname, "email_token_expiry", None)  # noqa: 501
    validate_user("Accounts Manager")
    if action == "1":
        status = "Approved"
    if action == "2":
        status = "Rejected"
    frappe.db.set_value(doctype, docname, "docstatus", 1)
    frappe.db.set_value(doctype, docname, "workflow_state", status)

    if status == "Rejected":
        frappe.db.set_value(doctype, docname, "docstatus", 2)

    frappe.db.commit()
    url = get_url()
    my_attachments = []

    content1 = f"""<h2>{doctype} {status}</h2>"""
    content3 = f"""
        <p>{docname} in {doctype} is {status}</p>"""
    cont = """<style>

    </style>"""

    recipient = []
    Users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Accounts User"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for usr in Users:
        recipient.append(usr.email)
        asign_task(
            {
                "assign_to": [usr.email],
                "doctype": doctype,
                "name": docname,
                "description": "",
                "priority": "High",
                "notify": 1,
                "action": status,
            }
        )  # isort:skip
    content2 = f""" <p><a href="{url}/app/{doctype_name}/{doc.name}">{doc.name} </a></p>"""  # noqa: 501
    frappe.sendmail(
        recipients=recipient,
        subject=doctype + " " + status,
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )
    if redirect is True:
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = (
            url + "/app/" + doctype_name + "/" + docname
        )  # noqa: 501
