// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs */
frappe.ui.form.on('Canopi Fixed Deposit Maturity', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Investments');
    });
    hideConnections(frm);
    setAccountFilter(frm);
    setFDOpeningFilter(frm);

    addFundClosureEntryBtn(frm);

    updateClosureDateField(frm);

    if (!frm.doc.__islocal) {
      approvalAction(frm);
    }
  },

  fd_opening: frm => {
    getFields(frm);
  },
  premature_early_redemption: frm => {
    updateClosureDateField(frm);
    updateTenor(frm);
  },
  closure_date: frm => {
    updateTenor(frm);
  },
  effective_date: frm => {
    validateClosureDate(frm);
  },
});
function setAccountFilter(frm) {
  frm.set_query('interest_income_gl', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.set_query('renewal_transit_gl', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.set_query('tds_paid_head', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
}
function addFundClosureEntryBtn(frm) {
  if (frm.doc.docstatus === '1') {
    frm.add_custom_button('Fund Closure Entry', () => {
      frappe.model.open_mapped_doc({
        method: 'trusteeship_platform.custom_methods.make_fund_closure_entry',
        frm,
      });
    });
  }
}
function getFields(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'Canopi Fixed Deposit Opening Entry',
      doc_name: frm.doc.fd_opening,
    },
    freeze: true,
    callback: r => {
      frm.doc.office = r.message.office;
      frm.doc.deposit_amount = r.message.deposit_amount;
      frm.doc.branch_code = r.message.branch_code;
      frm.doc.account_no = r.message.account_no;
      frm.doc.contract_number = r.message.contract_number;
      frm.doc.maturity_date = r.message.maturity_date;
      frm.doc.maturity_value = r.message.maturity_value;
      frm.doc.effective_date = r.message.effective_date;
      frm.doc.interest_rate = r.message.interest_rate;
      frm.doc.currency = r.message.currency;
      frm.doc.closure_date = r.message.maturity_date;
      frm.doc.tenor_month = r.message.tenor_month;
      frm.doc.tenor_days = r.message.tenor_days;
      frm.refresh_field('tenor_month');
      frm.refresh_field('tenor_days');
      frm.refresh_field('office');
      frm.refresh_field('deposit_amount');
      frm.refresh_field('account_type');
      frm.refresh_field('branch_code');
      frm.refresh_field('account_no');
      frm.refresh_field('contract_number');
      frm.refresh_field('maturity_date');
      frm.refresh_field('maturity_value');
      frm.refresh_field('effective_date');
      frm.refresh_field('interest_rate');
      frm.refresh_field('currency');
      frm.set_df_property('closure_date', 'read_only', true);
      frm.refresh_field('closure_date');
      validateClosureDate(frm);
    },
  });
}

function validateClosureDate(frm) {
  if (frm.doc.effective_date) {
    frm.fields_dict.closure_date.datepicker.update({
      minDate: frappe.datetime.str_to_obj(frm.doc.effective_date),
    });
  }
}

function updateClosureDateField(frm) {
  if (frm.doc.premature_early_redemption === 'No') {
    frm.doc.closure_date = frm.doc.maturity_date;
    frm.set_df_property('closure_date', 'read_only', true);
    frm.toggle_reqd('closure_date', false);
    frm.refresh_field('closure_date');
  } else {
    frm.set_df_property('closure_date', 'read_only', false);
    frm.toggle_reqd('closure_date', true);
    frm.refresh_field('closure_date');
  }
}

function updateTenor(frm) {
  const diffDate = new Date(
    new Date(frm.doc.closure_date) - new Date(frm.doc.effective_date),
  );
  const year = diffDate.toISOString().slice(0, 4) - 1970;
  let month = diffDate.getMonth();
  const days = diffDate.getDate() - 1;
  month = month + year * 12;
  frm.doc.tenor_month = month;
  frm.doc.tenor_days = days;
  frm.refresh_field('tenor_month');
  frm.refresh_field('tenor_days');
  calculateMaturityValue(frm);
}

function calculateMaturityValue(frm) {
  let tmonth = frm.doc.tenor_month;
  let tdays = frm.doc.tenor_days;
  if (!tmonth) {
    tmonth = 0;
  }
  if (!tdays) {
    tdays = 0;
  }
  const depositAmount = frm.doc.deposit_amount;
  const interestRate = frm.doc.interest_rate;
  let tenor = parseFloat(tmonth) / 12 + parseFloat(tdays) / 365;
  tenor = tenor.toFixed(2);
  let intrest = (parseFloat(depositAmount) * parseFloat(interestRate)) / 100;
  intrest = intrest * tenor;
  const maturityValue = parseFloat(depositAmount) + intrest;
  frm.doc.maturity_value = maturityValue;
  frm.refresh_field('maturity_value');
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Rejected'
  ) {
    frm.disable_form();
    frm.disable_save();
  }
  if (
    frm.doc.workflow_state === 'Draft' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.add_custom_button(
      'Send For Approval',
      function () {
        sendApprovalMail(frm);
      },
      __('Action'),
    );
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.add_custom_button(
      'Approve',
      function () {
        emailActionApprove(frm);
      },
      __('Action'),
    );
    frm.add_custom_button(
      'Reject',
      function () {
        emailActionReject(frm);
      },
      __('Action'),
    );
  }
  const buttons = document.querySelectorAll('.ellipsis');

  buttons.forEach(element => {
    if (element.innerHTML.includes('Action')) {
      element.style.backgroundColor = '#ed1164';
      element.style.color = 'white';
    }
  });
}
function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Canopi Fixed Deposit Maturity',
      doctype_name: 'canopi-fixed-deposit-maturity',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionApprove(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_maturity.canopi_fixed_deposit_maturity.email_action',
    args: {
      doctype: 'Canopi Fixed Deposit Maturity',
      docname: frm.doc.name,
      action: '1',
      doctype_name: 'canopi-fixed-deposit-maturity',
      email_token: frm.doc.email_token,
      redirect: false,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionReject(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_maturity.canopi_fixed_deposit_maturity.email_action',
    args: {
      doctype: 'Canopi Fixed Deposit Maturity',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'canopi-fixed-deposit-maturity',
      email_token: frm.doc.email_token,
      redirect: false,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function hideConnections(frm) {
  if (frm.doc.workflow_state === 'Rejected')
    $("[data-doctype='Canopi Fund Closure Entry']").hide();
  else $("[data-doctype='Canopi Fund Closure Entry']").show();
}

function setFDOpeningFilter(frm) {
  frm.set_query('fd_opening', function () {
    return {
      filters: [['workflow_state', '!=', 'Rejected']],
    };
  });
}
