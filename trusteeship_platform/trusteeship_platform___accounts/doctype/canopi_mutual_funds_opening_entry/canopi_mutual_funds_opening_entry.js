// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, locals, updateBreadcrumbs */
frappe.ui.form.on('Canopi Mutual Funds Opening Entry', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Investments');
    });

    setAccountNoFilter(frm);
    setGlAccountFilter(frm);
    if (!frm.doc.__islocal) {
      approvalAction(frm);
    }
    hideConnections(frm);
    configureNavCalculationTable(frm);
  },

  account_no: frm => {
    getFields(frm);
  },
});

frappe.ui.form.on('Canopi NAV Calculation', {
  nav_calculation_add: (frm, cdt, cdn) => {
    configureNavCalculationTable(frm);
  },

  nav_calculation_remove: (frm, cdt, cdn) => {
    configureNavCalculationTable(frm);
  },

  no_of_units: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    calculateNavAmount(row);
  },

  nav: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    calculateNavAmount(row);
  },
});

function configureNavCalculationTable(frm) {
  frm.get_field('nav_calculation').grid.cannot_add_rows =
    frm.doc.nav_calculation?.length;
  frm.refresh_fields();
}

function calculateNavAmount(row) {
  frappe.model.set_value(
    'Canopi NAV Calculation',
    row.name,
    'amount',
    !isNaN(row.no_of_units * row.nav) ? row.no_of_units * row.nav : 0,
  );
}

function setAccountNoFilter(frm) {
  frm.set_query('account_no', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.fields_dict.transaction_details_table.grid.get_field(
    'account_number',
  ).get_query = function (doc, cdt, cdn) {
    return {
      filters: [['Bank Account', 'company', '=', frm.doc.office]],
    };
  };
}

function setGlAccountFilter(frm) {
  frm.set_query('source_gl_access_code', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.set_query('source_of_renewal', function () {
    return {
      filters: [['company', '=', frm.doc.office]],
    };
  });
  frm.fields_dict.transaction_details_table.grid.get_field(
    'gl_account',
  ).get_query = function (doc, cdt, cdn) {
    return {
      filters: [['Account', 'company', '=', frm.doc.office]],
    };
  };
}

function getFields(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'Bank Account',
      doc_name: frm.doc.account_no,
    },
    freeze: true,
    callback: r => {
      frm.doc.account_type = r.message.account_type;
      frm.doc.branch_code = r.message.branch_code;
      frm.doc.bank_code = r.message.bank;
      frm.refresh_field('account_type');
      frm.refresh_field('branch_code');
      frm.refresh_field('bank_code');
    },
  });
}

frappe.ui.form.on('Canopi Mutual Funds Transaction Details', {
  amount: function (frm, cdt, cdn) {
    setTotalDeitCredit(frm, cdt, cdn);
    setBaseCurrencyAmount(frm, cdt, cdn);
  },
  dc: function (frm, cdt, cdn) {
    const d = locals[cdt][cdn];

    let glAccount = '';
    if (d.dc === 'Debit')
      glAccount = '2000001025 - Investment in Axis Liquid Fund - ATSL';
    else
      glAccount =
        '2020005005 - EXP Axis Bank Church Branch A/c 695010200000170 - ATSL';

    frappe.model.set_value(cdt, cdn, 'gl_account', glAccount);
    frm.refresh_fields('gl_account');

    setTotalDeitCredit(frm, cdt, cdn);
  },

  transaction_details_table_add: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    const depositAmount = frm.doc.deposit_amount;
    frappe.model.set_value(cdt, cdn, 'amount', depositAmount);
    frm.refresh_fields('amount');
    if (row.idx === 2) {
      frappe.model.set_value(cdt, cdn, 'dc', 'Credit');
      frm.refresh_fields('dc');
    } else {
      const glAccount = '2000001025 - Investment in Axis Liquid Fund - ATSL';
      frappe.model.set_value(cdt, cdn, 'gl_account', glAccount);
      frm.refresh_fields('gl_account');
    }
    setTotalDeitCredit(frm, cdt, cdn);
  },

  transaction_details_table_remove: function (frm, cdt, cdn) {
    setTotalDeitCredit(frm, cdt, cdn);
  },
});

function setTotalDeitCredit(frm, cdt, cdn) {
  let totalDebit = 0;
  let totalCredit = 0;
  frm.doc.transaction_details_table.forEach(function (d) {
    if (d.dc === 'Debit') totalDebit += d.amount;
    else totalCredit += d.amount;
  });
  frm.set_value('total_debit', totalDebit);
  frm.refresh_field('total_debit');
  frm.set_value('total_credit', totalCredit);
  frm.refresh_field('total_credit');
}

function setBaseCurrencyAmount(frm, cdt, cdn) {
  const amount = frappe.model.get_value(cdt, cdn, 'amount');
  let baseCurrencyAmount = amount;

  let companyCurrency = frappe.get_doc(
    ':Company',
    frm.doc.office,
  ).default_currency;
  if (companyCurrency === '') {
    companyCurrency = 'INR';
  }
  if (companyCurrency === frm.doc.currency) {
    frappe.model.set_value(
      cdt,
      cdn,
      'base_currency_amount',
      baseCurrencyAmount,
    );
    frm.refresh_fields('base_currency_amount');
  } else {
    frappe.call({
      method: 'erpnext.setup.utils.get_exchange_rate',
      args: {
        from_currency: frm.doc.currency,
        to_currency: companyCurrency,
      },
      callback: function (r, rt) {
        baseCurrencyAmount = amount * r.message;
        frappe.model.set_value(
          cdt,
          cdn,
          'base_currency_amount',
          baseCurrencyAmount,
        );
        frm.refresh_fields('base_currency_amount');
      },
    });
  }
}

function approvalAction(frm) {
  if (
    frm.doc.workflow_state === 'Approved' ||
    frm.doc.workflow_state === 'Rejected'
  ) {
    frm.disable_form();
    frm.disable_save();
  }
  if (
    frm.doc.workflow_state === 'Draft' &&
    frappe.user.has_role('Accounts User')
  ) {
    frm.add_custom_button(
      'Send For Approval',
      function () {
        sendApprovalMail(frm);
      },
      __('Action'),
    );
  } else if (
    frm.doc.workflow_state === 'Pending for Approval' &&
    frappe.user.has_role('Accounts Manager')
  ) {
    frm.add_custom_button(
      'Approve',
      function () {
        emailActionApprove(frm);
      },
      __('Action'),
    );
    frm.add_custom_button(
      'Reject',
      function () {
        emailActionReject(frm);
      },
      __('Action'),
    );
  }
  const buttons = document.querySelectorAll('.ellipsis');

  buttons.forEach(element => {
    if (element.innerHTML.includes('Action')) {
      element.style.backgroundColor = '#ed1164';
      element.style.color = 'white';
    }
  });
}
function emailActionApprove(frm) {
  let action_url =
    'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.';
  action_url += 'canopi_fixed_deposit_opening_entry.email_action';

  frappe.call({
    method: action_url,
    args: {
      doctype: 'Canopi Mutual Funds Opening Entry',
      docname: frm.doc.name,
      action: '1',
      doctype_name: 'canopi-mutual-funds-opening-entry',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}
function emailActionReject(frm) {
  let action_url =
    'trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.';
  action_url += 'canopi_fixed_deposit_opening_entry.email_action';

  frappe.call({
    method: action_url,
    args: {
      doctype: 'Canopi Mutual Funds Opening Entry',
      docname: frm.doc.name,
      action: '2',
      doctype_name: 'canopi-mutual-funds-opening-entry',
      redirect: false,
      email_token: frm.doc.email_token,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function sendApprovalMail(frm) {
  frappe.call({
    method: 'trusteeship_platform.overrides.sales_invoice.send_approval_mail',
    args: {
      doc_name: frm.doc.name,
      doc_type: 'Canopi Mutual Funds Opening Entry',
      doctype_name: 'canopi-mutual-funds-opening-entry',
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

function hideConnections(frm) {
  if (frm.doc.workflow_state === 'Rejected') {
    $("[data-doctype='Canopi Redemption Accounting']").hide();
    $("[data-doctype='Journal Entry']").hide();
  } else {
    $("[data-doctype='Canopi Redemption Accounting']").show();
    $("[data-doctype='Journal Entry']").show();
  }
}
