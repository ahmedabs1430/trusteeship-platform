# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

from datetime import datetime

import frappe
from frappe.model.document import Document


class CanopiMutualFundsOpeningEntry(Document):
    def validate(self):
        self.validate_nav_calculation()

    def on_submit(self):
        if not self.transaction_details_table:
            frappe.throw("Transaction table can not be blank")

        new_doc = frappe.new_doc("Journal Entry")
        new_doc.voucher_type = "Mutual Funds"
        new_doc.company = self.office
        new_doc.posting_date = datetime.today()
        for element in self.transaction_details_table:
            if element.dc == "Debit":
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "debit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Mutual Funds Opening Entry",  # noqa: 501
                        "canopi_reference_name": self.name,
                    },
                )
            else:
                new_doc.append(
                    "accounts",
                    {
                        "account": element.gl_account,
                        "credit_in_account_currency": element.amount,
                        "canopi_reference_type": "Canopi Mutual Funds Opening Entry",  # noqa: 501
                        "canopi_reference_name": self.name,
                    },
                )
        new_doc.insert()
        frappe.db.commit()

    def validate_nav_calculation(self):
        if len(self.nav_calculation) > 1:
            frappe.throw(
                "<b>NAV Calculation</b> cannot have more than one row"
            )  # noqa: 501
