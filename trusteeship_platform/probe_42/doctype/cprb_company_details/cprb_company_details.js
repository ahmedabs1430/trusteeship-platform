// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

frappe.ui.form.on('CPRB Company Details', {
  refresh: function (frm) {
    if (!frm.doc.has_comprehensive_details && frm.doc.company_cin) {
      frm.add_custom_button(__('Fetch Comprehensive Details'), () =>
        frm.trigger('fetch_comprehensive_details'),
      );
    }

    if (frm.doc.has_comprehensive_details) {
      frm.add_custom_button(__('Force Fetch Comprehensive Details'), () =>
        frappe.confirm(
          'This option will check the Probe42 database and update the Comprehensive Details saved in the system. Do you want to continue?',
          () => {
            frm.trigger('force_fetch_comprehensive_details');
          },
          () => {
            // action to perform if No is selected
          },
        ),
      );
    }
  },

  fetch_comprehensive_details: function (frm) {
    frappe.call({
      method:
        'trusteeship_platform.probe_42.doctype.cprb_company_details.cprb_company_details.fetch_comprehensive_details',
      args: {
        doc: frm.doc,
      },
      callback: function (response) {
        if (frm.doc.__islocal) {
          frappe.set_route(
            'Form',
            'CPRB Company Details',
            response.message.name,
          );
        }
        frappe.show_alert(
          {
            message: 'Fetched successfully.',
            indicator: 'green',
          },
          5,
        );

        frm.reload_doc();
      },
    });
  },

  force_fetch_comprehensive_details: function (frm) {
    frappe.call({
      method:
        'trusteeship_platform.probe_42.doctype.cprb_company_details.cprb_company_details.force_fetch_comprehensive_details',
      args: {
        doc: frm.doc,
      },
      callback: function (response) {
        if (frm.doc.__islocal) {
          frappe.set_route(
            'Form',
            'CPRB Company Details',
            response.message.name,
          );
        }
        frappe.show_alert(
          {
            message: 'Fetched successfully.',
            indicator: 'green',
          },
          5,
        );

        frm.reload_doc();
      },
    });
  },
});
