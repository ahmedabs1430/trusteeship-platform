# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json

import frappe
import requests
from frappe.model.document import Document

from trusteeship_platform.custom_methods import process_response


class CPRBCompanyDetails(Document):
    def before_save(self):
        if not self.request_id:
            self.request_id = "Immediate"
            self.request_datetime = frappe.utils.now_datetime()


# trusteeship_platform.probe_42.doctype.cprb_company_details.cprb_company_details.fetch_comprehensive_details
@frappe.whitelist()
def fetch_comprehensive_details(doc):
    doc = json.loads(doc)
    cprb_doc = frappe.get_doc("CPRB Company Details", doc.get("name"))
    cin_or_pan = doc.get("name")
    lead_doc = frappe.get_doc("Lead", {"cin_number": cin_or_pan})
    is_pan = (
        lead_doc.get("cnp_type") == "Individual"
        or lead_doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
    )
    cnp_type = "llps" if lead_doc.get("cnp_type") == "LLP" else "companies"
    if not cprb_doc.request_id:
        return create_update_request(cin_or_pan, is_pan, cprb_doc)
    else:
        response = _show_update_status(
            cin_or_pan, cprb_doc.request_id, is_pan, cnp_type
        )
        status = response.get("data", {}).get("status")
        if status != "FULFILLED":
            frappe.throw(
                "Request is in Queue. Comprehensive details are being fetched from Probe42 database. The details will be updated automatically when it is available"  # noqa
            )

    frappe.msgprint("Comprehensive Details Fetched !")
    from trusteeship_platform.custom_methods import fetch_probe_42_details

    comprehensive_details = fetch_probe_42_details(cin_or_pan, is_pan, cnp_type)  # noqa
    if comprehensive_details.get("data"):
        company_details = comprehensive_details
        cprb_doc.has_comprehensive_details = 1
        cprb_doc.company_name = (
            company_details.get("data", {})
            .get("company", {})
            .get("legal_name", "")  # noqa
        )
        cprb_doc.payload = company_details
        cprb_doc.last_updated = frappe.utils.now_datetime()
        cprb_doc.save()
    return doc


# trusteeship_platform.probe_42.doctype.cprb_company_details.cprb_company_details.fetch_comprehensive_details
@frappe.whitelist()
def force_fetch_comprehensive_details(doc):
    doc = json.loads(doc)
    cprb_doc = frappe.get_doc("CPRB Company Details", doc.get("name"))
    cin_or_pan = doc.get("name")
    lead_doc = frappe.get_doc("Lead", {"cin_number": cin_or_pan})
    is_pan = (
        lead_doc.get("cnp_type") == "Individual"
        or lead_doc.get("cnp_type") == "Partnership Firm"  # noqa: 501
    )

    from trusteeship_platform.custom_methods import fetch_probe_42_details

    cnp_type = "llp" if lead_doc.get("cnp_type") == "LLP" else "companies"
    comprehensive_details = fetch_probe_42_details(cin_or_pan, is_pan, cnp_type)  # noqa
    if comprehensive_details.get("data"):
        company_details = comprehensive_details
        cprb_doc.has_comprehensive_details = 1
        cprb_doc.company_name = (
            company_details.get("data", {})
            .get("company", {})
            .get("legal_name", "")  # noqa
        )
        cprb_doc.payload = company_details
        cprb_doc.last_updated = frappe.utils.now_datetime()
        cprb_doc.save()
    return doc


def _show_update_status(cin_or_pan, request_id, is_pan, cnp_type):

    """
    Fetch Base Details For a CIN
    """
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    url = f"""{settings_doc.probe_42_url}/{cnp_type}/{cin_or_pan}/get-update-status?request_id={request_id}"""  # noqa: 501

    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.get(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    return response


def create_update_request(cin_or_pan, is_pan, cprb_doc):
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    url = f"""{settings_doc.probe_42_url}/companies/{cin_or_pan}/update"""  # noqa: 501

    if is_pan:
        url += "?identifier_type=PAN"
    response = process_response(
        requests.post(
            url,
            headers={
                "x-api-key": settings_doc.get_password("api_secret"),
                "x-api-version": settings_doc.api_version,
            },
        )
    )
    request_id = response.get("data", {}).get("request_id")
    frappe.msgprint(request_id)
    if request_id:
        cprb_doc.request_id = response.get("data", {}).get("request_id")
        cprb_doc.last_updated = frappe.utils.now_datetime()
        cprb_doc.save()
