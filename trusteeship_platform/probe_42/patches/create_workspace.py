import frappe


def execute():
    data = {
        "name": "Probe42",
        "owner": "Administrator",
        "creation": "2023-08-07 11:38:57.461236",
        "modified": "2023-08-07 11:39:30.433684",
        "modified_by": "Administrator",
        "docstatus": 0,
        "idx": 0,
        "label": "Probe42",
        "title": "Probe42",
        "sequence_id": 40,
        "for_user": "",
        "parent_page": "",
        "icon": "quantity-1",
        "hide_custom": 0,
        "public": 1,
        "is_hidden": 0,
        "content": '[{"id":"W2leAK18BH","type":"header","data":{"text":"<span class=\\"h4\\">Probe42</span>","col":12}},{"id":"RJ94a_R38P","type":"quick_list","data":{"quick_list_name":"CPRB Company Details","col":4}}]',  # noqa
        "doctype": "Workspace",
        "charts": [],
        "quick_lists": [
            {
                "name": "16b653448b",
                "owner": "Administrator",
                "creation": "2023-08-07 11:38:57.461236",
                "modified": "2023-08-07 11:39:30.433684",
                "modified_by": "Administrator",
                "docstatus": 0,
                "idx": 1,
                "document_type": "CPRB Company Details",
                "label": "CPRB Company Details",
                "parent": "Probe42",
                "parentfield": "quick_lists",
                "parenttype": "Workspace",
                "doctype": "Workspace Quick List",
            }
        ],
        "roles": [],
        "custom_blocks": [],
        "number_cards": [],
        "shortcuts": [],
        "links": [],
        "__last_sync_on": "2023-08-16T17:54:53.540Z",
    }
    create_workspace(data)


def create_workspace(data):
    try:
        if not frappe.db.exists("DocType", "CPRB Company Details"):
            return
        workspace = frappe.new_doc("Workspace")
        workspace.update(
            {
                "name": data["name"],
                "owner": data["owner"],
                "creation": data["creation"],
                "modified": data["modified"],
                "modified_by": data["modified_by"],
                "docstatus": data["docstatus"],
                "idx": data["idx"],
                "label": data["label"],
                "title": data["title"],
                "sequence_id": data["sequence_id"],
                "for_user": data["for_user"],
                "parent_page": data["parent_page"],
                "icon": data["icon"],
                "hide_custom": data["hide_custom"],
                "public": data["public"],
                "is_hidden": data["is_hidden"],
                "content": data["content"],
                "doctype": data["doctype"],
            }
        )

        for quick_list_data in data["quick_lists"]:
            quick_list = frappe.new_doc("Workspace Quick List")
            quick_list.update(
                {
                    "name": quick_list_data["name"],
                    "owner": quick_list_data["owner"],
                    "creation": quick_list_data["creation"],
                    "modified": quick_list_data["modified"],
                    "modified_by": quick_list_data["modified_by"],
                    "docstatus": quick_list_data["docstatus"],
                    "idx": quick_list_data["idx"],
                    "document_type": quick_list_data["document_type"],
                    "label": quick_list_data["label"],
                    "parent": workspace.name,
                    "parentfield": "quick_lists",
                    "parenttype": "Workspace Quick List",
                    "doctype": quick_list_data["doctype"],
                }
            )

            workspace.append("quick_lists", quick_list)

        workspace.insert(ignore_permissions=True)
    except Exception as e:
        frappe.logger("utils").exception(e)
        return
