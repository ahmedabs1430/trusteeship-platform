from frappe import _


def get_data():
    return [
        {
            "module_name": "Trusteeship Platform",
            "color": "grey",
            "icon": "octicon octicon-file-directory",
            "type": "module",
            "label": _("Trusteeship Platform"),
        }
    ]
