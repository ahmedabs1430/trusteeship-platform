/* global frappe */

frappe.listview_settings['Canopi Other Documents Post Execution'] = {
  onload: function (listView) {
    listView.can_create = false;
    listView.page.actions
      .find('[data-label="Delete"]')
      .parent()
      .parent()
      .remove();
  },
};
