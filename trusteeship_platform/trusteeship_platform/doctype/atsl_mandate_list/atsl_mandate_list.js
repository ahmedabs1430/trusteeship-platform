// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs, cur_frm, erpnext, in_list, open_url_post, locals */
/* eslint-env jquery */
frappe.ui.form.on('ATSL Mandate List', {
  refresh: function (frm) {
    if (!frm.is_new()) {
      frm.set_df_property('ops_servicing_rm', 'reqd', true);
      frm.set_df_property('tl_representative', 'reqd', true);
    }

    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    frm.set_intro('');
    hideOpenNotification();
    hideMenuItems();
    addCustomEmailIcon(frm);
    showActivities(frm);
    showNotes(frm);
    setQuotationFilter(frm);
    showHideConsentLetterTab(frm);

    const prevRoute = frappe.get_prev_route();
    if (prevRoute[1] === 'Quotation' || !frm.doc.__islocal) {
      if (frm.doc.product_name !== '') {
        frm.set_df_property('product_name', 'read_only', true);
      }
      if (frm.doc.opportunity !== '') {
        frm.set_df_property('opportunity', 'read_only', true);
      }
      if (frm.doc.quotation !== '') {
        frm.set_df_property('quotation', 'read_only', true);
      }
    }

    hideBlankValueInSecurities(frm);
    hideCheckboxDetailsConnections(frm);
    frm.show_submit_message();
    isQuotationAmended(frm);
    renderButtons(frm);
    headerButtons(frm);
  },

  before_load: frm => {
    if (frm.doc.__islocal === 1) {
      // empty trache history table if doc is duplicate
      frm.doc.tranche_history = [];
      frm.refresh_fields();

      if (frm.doc.opportunity) {
        setFields(frm);
      }
    }
  },

  onload: function (frm) {
    setRegisteredAddressFilter(frm);
    setCorporateAddressFilter(frm);
  },
  registered_address: frm => {
    setCorporateAddressFilter(frm);
  },

  upfront_security_creation: function (frm) {
    hideCheckboxDetailsConnections(frm);
  },

  opportunity: function (frm) {
    setQuotationFilter(frm);
    frm.set_value('quotation', '');
    frm.set_value('product_name', '');
    if (frm.doc.opportunity) {
      setFields(frm);
    }
  },
  address: function (frm) {
    setAddressDetails(frm);
  },
});

frappe.ui.form.on('Canopi Operations Security Type', {
  security_type_add: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    const currentRow =
      frm.fields_dict.security_type.grid.grid_rows_by_docname[row.name];

    currentRow.doc.justification = 'Newly added';
    currentRow.doc.change_in_security = ' ';
  },
});

function setAddressDetails(frm) {
  erpnext.utils.get_address_display(frm, 'address', 'address_details');
}

function downloadOfferLetter(frm) {
  if (frm.doc.quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.quotation,
        doc_type: 'Quotation',
      },
      freeze: true,
      callback: r => {
        if (r.message.cnp_s3_key) {
          open_url_post(frappe.request.url, {
            cmd: 'trusteeship_platform.custom_methods.download_s3_file',
            key: r.message.cnp_s3_key,
          });
        }
      },
    });
  }
}

function viewOfferLetter(frm) {
  if (frm.doc.quotation) {
    window.open(
      `/api/method/trusteeship_platform.custom_methods.download_signed_pdf?doctype=Quotation&docname=${frm.doc.quotation}`,
      '_blank',
    );
  }
}

function showHideConsentLetterTab(frm) {
  if (frm.doc.__islocal) {
    frm.fields_dict.letter_from.tab.df.hidden = true;
  } else {
    if (frm.doc.product_name.toUpperCase() === 'DTE') {
      frm.fields_dict.letter_from.tab.df.hidden = false;
      frm.toggle_reqd('letter_subject', true);
    }
  }
}

function addConsentLetterButton(frm) {
  if (!frm.doc.__islocal && frm.doc.product_name.toUpperCase() === 'DTE') {
    if (frm.doc.security_type) {
      for (let i = 0; i < frm.doc.security_type.length; i++) {
        if (
          frm.doc.security_type[i].creation !==
          frm.doc.security_type[i].modified
        ) {
          consentLetterButton(frm);
          break;
        }
      }
    }
  }
}

function consentLetterButton(frm) {
  frm.dirty();
  frm.save().then(() => {
    window.open(
      `/api/method/trusteeship_platform.custom_methods.download_consent_letter?doctype=ATSL Mandate List&docname=${frm.doc.name}`,
      '_blank',
    );
  });
}

function addInitiateTrancheFlowButton(frm) {
  if (!frm.doc.__islocal) {
    if (frm.doc.quotation) {
      initiateTrancheFlow(frm);
    } else {
      getQuotationDialog(frm);
    }
  }
}

function initiateTrancheFlow(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform.doctype.atsl_mandate_list.atsl_mandate_list.initiate_tranche_flow',
    args: { quotation: frm.doc.quotation },
    freeze: true,
    callback: r => {
      if (r.message?.length) {
        getQuotationDialog(frm, 'Please select another Quotation');
      } else {
        localStorage.setItem('initiate_tranche_flow', 1);
        frappe.set_route('quotation', frm.doc.quotation);
      }
    },
  });
}

function getQuotationDialog(frm, title = 'Select Quotation') {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Quotation',
        fieldname: 'quotation',
        fieldtype: 'Link',
        options: 'Quotation',
        reqd: 1,
      },
    ],
    primary_action_label: 'Initiate Trache Flow',
    primary_action: async values => {
      frm.set_value('quotation', values.quotation);
      await frm.save();
      d.hide();
      initiateTrancheFlow(frm);
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.fields_dict.quotation.get_query = function () {
    const filters = [
      ['Quotation', 'cnp_cl_code', '=', ''],
      ['workflow_state', '=', 'Accepted By Client'],
    ];
    if (frm.doc.opportunity) {
      filters.push(['Quotation', 'cnp_opportunity', '=', frm.doc.opportunity]);
    }
    return {
      filters,
    };
  };
  d.show();
}

function isQuotationAmended(frm) {
  if (frm.doc.quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: {
        doctype: 'Quotation',
        filters: [['amended_from', '=', frm.doc.quotation]],
      },
      freeze: true,
      callback: r => {
        if (r.message.length) {
          frm.set_intro(
            'The selected quotaion is already amended, please select another quotation',
          );
          frm.set_df_property('quotation', 'read_only', false);
        }
      },
    });
  }
}

function setQuotationFilter(frm) {
  const filters = [
    ['Quotation', 'cnp_cl_code', '=', ''],
    ['workflow_state', '=', 'Accepted By Client'],
  ];
  if (frm.doc.opportunity) {
    filters.push(['Quotation', 'cnp_opportunity', '=', frm.doc.opportunity]);
  }
  frm.set_query('quotation', filter => {
    return {
      filters,
    };
  });
}

function hideBlankValueInSecurities(frm) {
  if (!frm.doc.__islocal) {
    frm.fields_dict.security_type.grid.update_docfield_property(
      'change_in_security',
      'options',
      ['No', 'Yes'],
    );
  }
}

function showNotes(frm) {
  $(frm.fields_dict.notes_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;

  const crmNotes = new erpnext.utils.CRMNotes({
    frm,
    notes_wrapper: $(frm.fields_dict.notes_html.wrapper),
  });
  crmNotes.refresh();

  $('.new-note-btn').unbind();
  $('.new-note-btn').on('click', function () {
    addNote(frm);
  });

  $('.notes-section').find('.edit-note-btn').unbind();
  $('.notes-section')
    .find('.edit-note-btn')
    .on('click', function () {
      editNote(this, frm);
    });

  $('.notes-section').find('.delete-note-btn').unbind();
  $('.notes-section')
    .find('.delete-note-btn')
    .on('click', function () {
      deleteNote(this, frm);
    });
}

function addNote(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Add a Note',
    fields: [
      {
        label: 'Note',
        fieldname: 'note',
        fieldtype: 'Text Editor',
        reqd: 1,
        enable_mentions: true,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_note',
        args: {
          doctype: frm.doctype,
          docname: frm.doc.name,
          note: data.note,
        },
        freeze: true,
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.refresh_fields();
          }
        },
      });
    },
    primary_action_label: 'Add',
  });
  d.show();
}

function editNote(editBtn, frm) {
  const row = $(editBtn).closest('.comment-content');
  const rowId = row.attr('name');
  const rowContent = $(row).find('.content').html();
  if (rowContent) {
    const d = new frappe.ui.Dialog({
      title: 'Edit Note',
      fields: [
        {
          label: 'Note',
          fieldname: 'note',
          fieldtype: 'Text Editor',
          default: rowContent,
        },
      ],
      primary_action: function () {
        const data = d.get_values();
        frappe.call({
          method: 'trusteeship_platform.custom_methods.edit_note',
          args: {
            doctype: frm.doctype,
            docname: frm.doc.name,
            note: data.note,
            rowId,
          },
          freeze: true,
          callback: function (r) {
            if (!r.exc) {
              d.hide();
              frm.reload_doc();
            }
          },
        });
      },
      primary_action_label: 'Done',
    });
    d.show();
  }
}

function deleteNote(deleteBtn, frm) {
  const rowId = $(deleteBtn).closest('.comment-content').attr('name');
  frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_note',
    args: {
      doctype: frm.doctype,
      docname: frm.doc.name,
      rowId,
    },
    freeze: true,
    callback: function (r) {
      if (!r.exc) {
        frm.refresh_fields();
      }
    },
  });
}

function showActivities(frm) {
  $(frm.fields_dict.open_activities_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;
  const crmActivities = new erpnext.utils.CRMActivities({
    frm,
    open_activities_wrapper: $(frm.fields_dict.open_activities_html.wrapper),
    all_activities_wrapper: $(frm.fields_dict.all_activities_html.wrapper),
    form_wrapper: $(frm.wrapper),
  });
  crmActivities.refresh();

  // remove extra activities table
  setTimeout(() => {
    if ($('.open-activities').length > 1) {
      crmActivities.refresh();
    }
  }, 400);
}

function hideOpenNotification() {
  $('.open-notification').hide();
}

function setFields(frm) {
  const docs = [
    {
      doctype: 'Opportunity',
      docname: frm.doc.opportunity,
    },
    {
      doctype: 'Lead',
      parent_doctype: 'Opportunity',
      child_link_field: 'party_name',
    },
    {
      doctype: 'Customer',
      parent_doctype: 'Opportunity',
      child_link_field: 'party_name',
    },
  ];
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_docs_with_linked_childs',
    args: { docs },
    freeze: true,
    callback: r => {
      const customerName = r.message.opportunity.customer_name;
      const address = r.message.opportunity.customer_address;
      frm.doc.company_name = customerName;
      frm.doc.address = address;
      frm.refresh_field('company_name');
      frm.refresh_field('address');
      frm.refresh_field('address_details');

      frm.doc.product_name = r.message.opportunity.cnp_product;
      frm.refresh_field('product_name');
      frm.doc.mandate_name = r.message.opportunity.customer_name;
      frm.refresh_field('mandate_name');
      frm.doc.cnp_customer_code = r.message.opportunity.cnp_customer_code;
      frm.refresh_field('cnp_customer_code');
      frm.doc.person_name =
        r.message.opportunity.opportunity_from === 'Lead'
          ? r.message.lead.lead_name
          : r.message.customer.customer_name;
      frm.refresh_field('person_name');
      frm.set_df_property('product_name', 'read_only', true);
      frm.refresh_field('product_name');

      cur_frm.clear_table('security_type');
      frm.refresh_field('security_type');
      for (let i = 0; i < r.message.opportunity.cnp_security_type.length; i++) {
        const child = cur_frm.add_child('security_type');
        frappe.model.set_value(
          child.doctype,
          child.name,
          'existing_security_type',
          r.message.opportunity.cnp_security_type[i].type_of_security,
        );
        frappe.model.set_value(
          child.doctype,
          child.name,
          'change_in_security',
          'No',
        );
      }
      frm.refresh_field('security_type');
      setAddressDetails(frm);
      setConsentLetterBody(frm);
    },
  });
}

function setConsentLetterBody(frm) {
  if (frm.doc.product_name.toUpperCase() === 'DTE') {
    const item = {
      full_name: 'Debenture Trustee',
      full_name_plural: ['Debenture Trustees', 'Debentures Trustee'],
      first_name: 'Debenture',
      first_name_plural: 'Debentures',
    };

    let letterBody =
      `We, Axis Trustee Services Limited, hereby give our consent to act as the ${item.full_name} for the above mentioned issue of` +
      `${item.first_name_plural} having a tenure of more than one year and are agreeable to the inclusion of our name as` +
      `${item.full_name} in the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum and/or application to be` +
      `made to the Stock Exchange for the listing of the said ${item.first_name_plural}.\n\n`;
    letterBody +=
      `Axis Trustee Services Limited (ATSL) consenting to act as ${item.full_name_plural[0]} is purely its business decision and not ` +
      `an indication on the Issuer Company's standing or on the ${item.first_name} Issue. By consenting to act as ` +
      `${item.full_name_plural[0]}, ATSL does not make nor deems to have made any representation on the Issuer Company, its ` +
      `Operations, the details and projections about the Issuer Company or the ${item.first_name_plural} under Offer made in the ` +
      'Shelf Prospectus/ Private Placement offer letter/ Information Memorandum / Offer Document. Applicants / Investors are ' +
      'advised to read carefully the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum / Offer Document ' +
      'and make their own enquiry, carry out due diligence and analysis about the Issuer Company, its performance and ' +
      'profitability and details in the Shelf Prospectus/ Private Placement offer letter/ Information Memorandum / Offer Document ' +
      'before taking their investment decision. ATSL shall not be responsible for the investment decision and its consequence.\n\n';
    letterBody +=
      `We also confirm that we are not disqualified to be appointed as ${item.full_name_plural[1]} within the meaning of Rule 18(2)(c) ` +
      `of the Companies (Share Capital and ${item.first_name_plural}) Rules, 2014.`;
    frm.doc.letter_body = letterBody;
    frm.refresh_field('letter_body');
  }
}

function setRegisteredAddressFilter(frm) {
  frm.set_query('registered_address', function () {
    return {
      filters: [
        ['Address', 'is_your_company_address', '=', 1],
        ['Address', 'address_type', '=', 'Registered Office'],
      ],
    };
  });
}

function setCorporateAddressFilter(frm) {
  if (frm.doc.registered_address) {
    frm.toggle_reqd('corporate_office', true);
    frm.toggle_display('corporate_office', true);
    frm.set_df_property('corporate_office', 'label', 'Corporate Office');
    frm.set_query('corporate_office', function () {
      return {
        filters: [
          ['Address', 'is_your_company_address', '=', 1],
          ['Address', 'address_type', '=', 'Corporate Office'],
        ],
      };
    });
  } else {
    frm.toggle_display('corporate_office', false);
    frm.toggle_reqd('corporate_office', false);
    frm.doc.corporate_office = '';
  }
}

function hideMenuItems() {
  const elements = document.querySelectorAll('a.grey-link ');
  elements.forEach(element => {
    // hide Email option from menu dropdown
    if (element.firstElementChild.innerText === 'Email') {
      element.hidden = true;
    }
  });
}

function addCustomEmailIcon(frm) {
  $('button[data-original-title=Mail]').remove();
  frm.page.add_action_icon('mail', () => {
    emailDialog();
  });
}

const dialog = new frappe.ui.Dialog({
  title: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
  no_submit_on_enter: true,
  fields: getFields(),
  primary_action_label: 'Send',
  primary_action() {
    sendAction();
  },
  secondary_action_label: 'Discard',
  secondary_action() {
    dialog.hide();
  },
  size: 'large',
  minimizable: true,
});

function emailDialog() {
  $(dialog.$wrapper.find('.form-section').get(0)).addClass('to_section');
  prepare();
  dialog.show();
}

function prepare() {
  setupMultiselectQueries();
  setupAttach();
  setupEmail();
  setupEmailTemplate();
}

function setupMultiselectQueries() {
  ['recipients', 'cc', 'bcc'].forEach(field => {
    dialog.fields_dict[field].get_data = () => {
      const data = dialog.fields_dict[field].get_value();
      const txt = data.match(/[^,\s*]*$/)[0] || '';

      frappe.call({
        method: 'frappe.email.get_contact_list',
        args: { txt },
        callback: r => {
          dialog.fields_dict[field].set_data(r.message);
        },
      });
    };
  });
}

let attachments = [];

function setupAttach() {
  const fields = dialog.fields_dict;
  const attach = $(fields.select_attachments.wrapper);

  if (!attachments) {
    attachments = [];
  }

  let args = {
    folder: 'Home/Attachments',
    on_success: attachment => {
      attachments.push(attachment);
      renderAttachmentRows(attachment);
    },
  };

  if (cur_frm) {
    args = {
      doctype: cur_frm.doctype,
      docname: cur_frm.docname,
      folder: 'Home/Attachments',
      on_success: attachment => {
        cur_frm.attachments.attachment_uploaded(attachment);
        renderAttachmentRows(attachment);
      },
    };
  }

  $(`<label class="control-label">
  ${'Select Attachments'}
  </label>
  <div class='attach-list'></div>
  <p class='add-more-attachments'>
  <button class='btn btn-xs btn-default'>
  ${frappe.utils.icon('small-add', 'xs')}&nbsp;
  ${'Add Attachment'}
  </button>
  </p>
  `).appendTo(attach.empty());

  attach
    .find('.add-more-attachments button')
    .on('click', () => new frappe.ui.FileUploader(args));
  renderAttachmentRows();
}

function renderAttachmentRows(attachment = null) {
  const selectAttachments = dialog.fields_dict.select_attachments;
  const attachmentRows = $(selectAttachments.wrapper).find('.attach-list');
  if (attachment) {
    attachmentRows.append(getAttachmentRow(attachment, true));
  } else {
    let files = [];
    if (attachments && attachments.length) {
      files = files.concat(attachments);
    }
    if (cur_frm) {
      files = files.concat(cur_frm.get_files());
    }

    if (files.length) {
      $.each(files, (i, f) => {
        if (!f.file_name) return;
        if (!attachmentRows.find(`[data-file-name="${f.name}"]`).length) {
          f.file_url = frappe.urllib.get_full_url(f.file_url);
          attachmentRows.append(getAttachmentRow(f));
        }
      });
    }
  }
}

function getAttachmentRow(attachment, checked = null) {
  return $(`<p class="checkbox flex">
    <label class="ellipsis" title="${attachment.file_name}">
      <input
        type="checkbox"
        data-file-name="${attachment.name}"
        ${checked ? 'checked' : ''}>
      </input>
      <span class="ellipsis">${attachment.file_name}</span>
    </label>
    &nbsp;
    <a href="${attachment.file_url}" target="_blank" class="btn-linkF">
      ${frappe.utils.icon('link-url')}
    </a>
  </p>`);
}

function setupEmail() {
  // email
  const fields = dialog.fields_dict;

  $(fields.send_me_a_copy.input).on('click', () => {
    // update send me a copy (make it sticky)
    const val = fields.send_me_a_copy.get_value();
    frappe.db.set_value('User', frappe.session.user, 'send_me_a_copy', val);
    frappe.boot.user.send_me_a_copy = val;
  });
}

function setupEmailTemplate() {
  dialog.fields_dict.email_template.df.onchange = () => {
    const emailTemplate = dialog.fields_dict.email_template.get_value();
    if (!emailTemplate) return;

    frappe.call({
      method:
        'frappe.email.doctype.email_template.email_template.get_email_template',
      args: {
        template_name: emailTemplate,
        doc: cur_frm.doc,
        _lang: dialog.get_value('language_sel'),
      },
      callback(r) {
        prependReply(r.message, emailTemplate);
      },
    });
  };
}

let replyAdded = '';
function prependReply(reply, emailTemplate) {
  if (replyAdded === emailTemplate) return;
  const contentField = dialog.fields_dict.content;
  const subjectField = dialog.fields_dict.subject;

  let content = contentField.get_value() || '';
  content = content.split('<!-- salutation-ends -->')[1] || content;

  contentField.set_value(`${reply.message}<br>${content}`);
  subjectField.set_value(reply.subject);

  replyAdded = emailTemplate;
}

function sendAction() {
  const formValues = getValues();
  if (!formValues) return;

  const selectedAttachments = $.map(
    $(dialog.wrapper).find('[data-file-name]:checked'),
    function (element) {
      return $(element).attr('data-file-name');
    },
  );
  sendEmail(formValues, selectedAttachments);
}

function getValues() {
  const formValues = dialog.get_values();

  // cc
  for (let i = 0, l = dialog.fields.length; i < l; i++) {
    const df = dialog.fields[i];

    if (df.is_cc_checkbox) {
      // concat in cc
      if (formValues[df.fieldname]) {
        formValues.cc =
          (formValues.cc ? formValues.cc + ', ' : '') + df.fieldname;
        formValues.bcc =
          (formValues.bcc ? formValues.bcc + ', ' : '') + df.fieldname;
      }

      delete formValues[df.fieldname];
    }
  }

  return formValues;
}

function sendEmail(formValues, selectedAttachments) {
  dialog.hide();

  if (!formValues.recipients) {
    frappe.msgprint('Enter Email Recipient(s)');
    return;
  }

  if (cur_frm && !frappe.model.can_email(cur_frm.doc.doctype, cur_frm)) {
    frappe.msgprint(
      'You are not allowed to send emails related to this document',
    );
    return;
  }

  frappe.call({
    method: 'trusteeship_platform.custom_methods.send_mail_consentletter',
    args: {
      recipients: formValues.recipients,
      cc: formValues.cc,
      bcc: formValues.bcc,
      subject: formValues.subject,
      content: formValues.content,
      doctype: cur_frm.doc.doctype,
      name: cur_frm.doc.name,
      send_me_a_copy: formValues.send_me_a_copy,
      attachments: selectedAttachments,
      read_receipt: formValues.send_read_receipt,
      attach_consent_letter: formValues.attach_consent_letter,
      sender: formValues.sender,
    },
    freeze: true,
    freeze_message: 'Sending',

    callback: r => {
      if (!r.exc) {
        frappe.utils.play_sound('email');

        if (r.message?.emails_not_sent_to) {
          frappe.msgprint('Email not sent to {0} (unsubscribed / disabled)', [
            frappe.utils.escape_html(r.message.emails_not_sent_to),
          ]);
        }

        if (cur_frm) {
          cur_frm.reload_doc();
        }
      } else {
        frappe.msgprint(
          'There were errors while sending email. Please try again.',
        );
      }
    },
  });
}

function getFields() {
  const fields = [
    {
      label: 'To',
      fieldtype: 'MultiSelect',
      reqd: 0,
      fieldname: 'recipients',
    },
    {
      fieldtype: 'Button',
      label: frappe.utils.icon('down'),
      fieldname: 'option_toggle_button',
      click: () => {
        toggleMoreOptions(false);
      },
    },
    {
      fieldtype: 'Section Break',
      hidden: 1,
      fieldname: 'more_options',
    },
    {
      label: 'CC',
      fieldtype: 'MultiSelect',
      fieldname: 'cc',
    },
    {
      label: 'BCC',
      fieldtype: 'MultiSelect',
      fieldname: 'bcc',
    },
    {
      label: 'Email Template',
      fieldtype: 'Link',
      options: 'Email Template',
      fieldname: 'email_template',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Subject',
      fieldtype: 'Data',
      reqd: 1,
      fieldname: 'subject',
      default: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
      length: 524288,
    },
    {
      label: 'Message',
      fieldtype: 'Text Editor',
      fieldname: 'content',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Send me a copy',
      fieldtype: 'Check',
      fieldname: 'send_me_a_copy',
      default: frappe.boot.user.send_me_a_copy,
    },
    {
      label: 'Send Read Receipt',
      fieldtype: 'Check',
      fieldname: 'send_read_receipt',
    },
    {
      label: 'Attach Consent Letter',
      fieldtype: 'Check',
      fieldname: 'attach_consent_letter',
      default: 1,
      hidden: cur_frm.doc.product_name?.toUpperCase() !== 'DTE',
    },
    { fieldtype: 'Column Break' },
    {
      label: 'Select Attachments',
      fieldtype: 'HTML',
      fieldname: 'select_attachments',
    },
  ];

  // add from if user has access to multiple email accounts
  const emailAccounts = frappe.boot.email_accounts.filter(account => {
    return (
      !in_list(
        ['All Accounts', 'Sent', 'Spam', 'Trash'],
        account.email_account,
      ) && account.enable_outgoing
    );
  });

  if (emailAccounts.length) {
    fields.unshift({
      label: 'From',
      fieldtype: 'Select',
      reqd: 1,
      fieldname: 'sender',
      options: emailAccounts.map(function (e) {
        return e.email_id;
      }),
    });
  }

  return fields;
}

function toggleMoreOptions(showOptions) {
  showOptions = showOptions || dialog.fields_dict.more_options.df.hidden;
  dialog.set_df_property('more_options', 'hidden', !showOptions);

  const label = frappe.utils.icon(showOptions ? 'up-line' : 'down');
  dialog.get_field('option_toggle_button').set_label(label);
}

function hideCheckboxDetailsConnections(frm) {
  if (frm.doc.upfront_security_creation === 1) {
    frm.get_field('security_type').grid.cannot_add_rows = true;
  } else {
    frm.get_field('security_type').grid.cannot_add_rows = false;
  }

  if (frm.doc.waive_transaction_details === 1) {
    $("div[data-doctype='Transaction Details']").hide();
  } else {
    $("div[data-doctype='Transaction Details']").show();
  }
  frm.refresh_fields();
}

function renderButtons(frm) {
  if (frm.doc.quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Quotation',
        doc_name: frm.doc.quotation,
      },
      freeze: true,
      callback: r => {
        const filename = r.message.cnp_s3_key
          ? r.message.cnp_s3_key.split('/').pop()
          : '';
        let viewOfferLetteButton;

        if (filename) {
          viewOfferLetteButton =
            `<div class="form-group">
            <div class="clearfix">
                <label class="control-label" style="padding-right: 0px"
                  >Download Accepted Offer Letter</label
                  >
            </div>
              <div class="control-input-wrapper">
                <div class="control-input" style="display: none"></div>
                <div class="control-value like-disabled-input bold">
                  <a class="` +
            frm.doc.name +
            '-download-offer-letter">' +
            filename +
            `</a>
                </div>
            </div></div>`;
        } else {
          viewOfferLetteButton =
            '<a class="btn btn-xs btn-default ' +
            frm.doc.name +
            '-view-offer-letter">View Offer Letter</a>';
        }

        frm.set_df_property(
          'view_offer_letter',
          'options',
          frappe.render_template(viewOfferLetteButton, {
            frm,
          }),
        );
      },
    });
  }

  if (frm.doc.product_name?.toUpperCase() === 'DTE') {
    const generateConsentLetterButton =
      '<a class="btn btn-xs btn-default float-right ' +
      frm.doc.name +
      '-generate-consent-letter">Generate Consent Letter</a>';
    frm.set_df_property(
      'generate_consent_letter',
      'options',
      frappe.render_template(generateConsentLetterButton, {
        frm,
      }),
    );
  }

  if (!frm.doc.__islocal) {
    const initiateTrancheFlowButton =
      '<a class="btn btn-xs btn-default ' +
      frm.doc.name +
      '-initiate-tranche-button">Initiate Trache Flow</a>';
    frm.set_df_property(
      'initiate_tranche_flow',
      'options',
      frappe.render_template(initiateTrancheFlowButton, {
        frm,
      }),
    );
  }
  viewButtons(frm);
}

function viewButtons(frm) {
  const viewOfferLetterId = '.' + frm.doc.name + '-view-offer-letter';
  const generateConsentLetter = '.' + frm.doc.name + '-generate-consent-letter';
  const initiateTrancheFlow = '.' + frm.doc.name + '-initiate-tranche-button';
  const download = '.' + frm.doc.name + '-download-offer-letter';
  $(document)
    .off('click', download)
    .on('click', download, function () {
      downloadOfferLetter(frm);
    });

  $(document)
    .off('click', viewOfferLetterId)
    .on('click', viewOfferLetterId, function () {
      viewOfferLetter(frm);
    });

  $(generateConsentLetter).unbind();
  $(generateConsentLetter).on('click', function () {
    addConsentLetterButton(frm);
  });
  $(initiateTrancheFlow).unbind();
  $(initiateTrancheFlow).on('click', function () {
    addInitiateTrancheFlowButton(frm);
  });
}

function headerButtons(frm) {
  frm.add_custom_button('Main', () => {
    frm.scroll_to_field('cnp_customer_code');
  });
  if (!frm.doc.__islocal) {
    let transactionDetails;
    if (frm.doc.product_name.toUpperCase() === 'FA') {
      transactionDetails = 'Canopi Transaction Details-FA';
    } else {
      transactionDetails = 'Transaction Details';
    }
    frm.add_custom_button(
      transactionDetails,
      () => {
        const linkedDoctypes = {
          [transactionDetails]: {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: frm.doctype,
            name: frm.doc.name,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, transactionDetails, r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Pre-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Pre Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: frm.doctype,
            name: frm.doc.name,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Pre Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Documentation',
      () => {
        const linkedDoctypes = {
          'Canopi Documentation': {
            fieldname: ['operations'],
          },
        };

        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: frm.doctype,
            name: frm.doc.name,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Documentation', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Post-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Post Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: frm.doctype,
            name: frm.doc.name,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Post Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
  }
}

function redirectDoctype(frm, doctype, response) {
  if (response.message[doctype]) {
    const outputString = doctype
      .replace(/([a-z0-9])([A-Z])/g, '$1 $2') // Insert space before capital letters if not already separated
      .toLowerCase() // Convert the entire string to lowercase
      .replace(/\s+/g, '-'); // Replace spaces with hyphens
    frappe.set_route(
      `/app/${outputString}/${response.message[doctype][0].name}`,
    );
  } else {
    frappe.model.with_doctype(doctype, function () {
      const source = frm.doc;
      const targetDoc = frappe.model.get_new_doc(doctype);

      targetDoc.operations = source.name;
      targetDoc.person_name = source.person_name;
      targetDoc.cnp_customer_code = source.cnp_customer_code;
      targetDoc.mandate_name = source.mandate_name;
      targetDoc.opportunity = source.opportunity;
      targetDoc.product_name = source.product_name;
      if (
        doctype === 'Canopi Post Execution Checklist' ||
        doctype === 'Canopi Pre Execution Checklist' ||
        doctype === 'Canopi Documentation'
      ) {
        targetDoc.ops_servicing_rm = source.ops_servicing_rm;
        targetDoc.tl_representative = source.tl_representative;
      }
      frappe.ui.form.make_quick_entry(doctype, null, null, targetDoc);
    });
  }
}
