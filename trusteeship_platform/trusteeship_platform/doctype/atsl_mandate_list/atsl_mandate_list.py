# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import get_link_to_form

from trusteeship_platform.custom_methods import set_status


class ATSLMandateList(Document):
    def __setup__(self):
        self.flags.ignore_links = True

    def before_submit(self):
        self.db_set("status", set_status(self.docstatus))

    def after_insert(self):
        update_quotation(self)
        update_opportunity(self)

    def before_insert(self):
        self.status = set_status(self.docstatus)
        # empty trache history table if doc is duplicate
        self.tranche_history = []

    def on_cancel(self):
        self.db_set("status", set_status(self.docstatus))

    def validate(self):
        update_quotation(self)
        if not self.serial_no and self.letter_subject:
            fiscal_year = frappe.get_doc(
                "Fiscal Year", frappe.defaults.get_user_default("fiscal_year")
            )
            year = fiscal_year.year.split("-")
            y1 = year[0][-2:]
            y2 = year[1][-2:]
            year_start_date = fiscal_year.year_start_date.strftime("%Y-%m-%d")
            year_end_date = fiscal_year.year_end_date.strftime("%Y-%m-%d")

            max_seq_no = 0
            if self.letter_from == "ATSL":
                last_seq_no = frappe.db.sql(
                    """
                select
                max(sequence_atsl) as max_seq_no
                from
                `tabATSL Mandate List`
                where letter_from = 'ATSL' and creation >= %s
                and creation <= %s
                """,
                    (year_start_date, year_end_date),
                    as_dict=1,
                )
                if last_seq_no:
                    if last_seq_no[0].max_seq_no is not None:
                        max_seq_no = last_seq_no[0].max_seq_no
                self.sequence_atsl = max_seq_no + 1
                ss = str(self.sequence_atsl)

            if self.letter_from == "AXIS":
                last_seq_no = frappe.db.sql(
                    """
                select
                max(sequence_axis) as max_seq_no
                from
                `tabATSL Mandate List`
                where letter_from = 'AXIS' and creation >= %s
                and creation <= %s """,
                    (year_start_date, year_end_date),
                    as_dict=1,
                )
                if last_seq_no:
                    if last_seq_no[0].max_seq_no is not None:
                        max_seq_no = last_seq_no[0].max_seq_no
                self.sequence_axis = max_seq_no + 1
                ss = str(self.sequence_axis)

            ss = ss.rjust(5, "0")
            self.serial_no = (
                self.letter_from + "/CO/" + str(y1) + "-" + str(y2) + "/" + ss
            )


def update_quotation(self):
    if self.quotation:
        validate_quotation(self)
    else:
        get_quotation(self)

    if self.get("__islocal"):
        return

    if self.quotation:
        quotation = frappe.get_doc("Quotation", self.quotation)

        if quotation.cnp_cl_code == self.name:
            return

    frappe.db.set_value("Quotation", self.quotation, "cnp_cl_code", self.name)
    frappe.db.commit()


def update_opportunity(self):
    if self.opportunity:
        opportunity = frappe.get_doc("Opportunity", self.opportunity)

        if opportunity.cnp_cl_code == self.name:
            return

    frappe.db.set_value(
        "Opportunity", self.opportunity, "cnp_cl_code", self.name
    )  # noqa: 501
    frappe.db.commit()


def validate_quotation(self):
    if self.quotation:
        doc_list = frappe.get_all(
            "ATSL Mandate List",
            filters=[
                ["quotation", "=", self.quotation],
                ["name", "!=", self.name],
            ],  # noqa: 501
        )
        if len(doc_list):
            frappe.throw(
                (
                    "Operations Flow {} already created for selected quotation"
                ).format(  # noqa: 501
                    get_link_to_form("ATSL Mandate List", doc_list[0].name)  # noqa: 501
                )
            )


def get_quotation(self):
    quotation_list = frappe.get_all(
        "Quotation",
        filters=[
            ["cnp_opportunity", "=", self.opportunity],
            ["cnp_cl_code", "=", ""],
            ["workflow_state", "=", "Accepted By Client"],
        ],
    )
    if len(quotation_list) == 1:
        self.quotation = quotation_list[0].name


@frappe.whitelist()
def initiate_tranche_flow(quotation):
    doc_list = frappe.get_all("Quotation", [["amended_from", "=", quotation]])

    if len(doc_list) > 0:
        return doc_list

    frappe.db.set_value("Quotation", quotation, "cnp_is_tranche_initiated", 1)
