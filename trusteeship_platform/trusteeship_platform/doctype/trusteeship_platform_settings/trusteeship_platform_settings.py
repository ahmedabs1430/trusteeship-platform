# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class TrusteeshipPlatformSettings(Document):
    def validate(self):
        # Navbar logo
        doc = frappe.get_doc("Navbar Settings")
        if frappe.db.exists("DocType", "Navbar Settings") and self.app_logo:
            doc.app_logo = self.app_logo
        elif not self.app_logo:
            doc.app_logo = ""
        doc.save()
        frappe.db.commit()
        # website logo
        doc = frappe.get_doc("Website Settings")
        if frappe.db.exists("DocType", "Website Settings") and self.app_logo:
            doc.favicon = self.app_logo
            doc.app_logo = self.app_logo
            doc.splash_image = self.app_logo
        elif not self.app_logo:
            doc.favicon = ""
            doc.app_logo = ""
            doc.splash_image = ""
        doc.save()
        frappe.db.commit()
        frappe.clear_cache()


def get_site_name():
    return frappe.local.site_path.strip("./")
