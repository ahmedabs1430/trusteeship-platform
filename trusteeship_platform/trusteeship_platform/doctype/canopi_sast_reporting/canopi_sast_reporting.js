// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs, showActivities, showNotes */

frappe.ui.form.on('Canopi SAST Reporting', {
  refresh: function (frm) {
    changeBreadcrumbs(frm);
    setClCodeFilter(frm);
    setIntro(frm);
    showActivitiesAndnotes(frm);
  },
  form_w: function (frm) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.form_w,
        doc_type: 'Canopi Form W',
      },
      freeze: true,
      callback: r => {
        frm.doc.scrip_code = r.message.scrip_code;
        frm.refresh_field('scrip_code');
      },
    });
  },
});

function changeBreadcrumbs() {
  frappe.require('assets/trusteeship_platform/js/common.js', () => {
    updateBreadcrumbs('SAST');
  });
}

function setIntro(frm) {
  frm.set_intro('');
  if (!frm.doc.__islocal) {
    if (frm.doc.docstatus === 0) {
      frm.set_intro('Submit this document to confirm');
    }
  }
  if (frm.doc.initial_reporting_total > 5) {
    frm.set_intro('');
    frm.set_intro('Reporting to be done to SAST and NDU in 2 days');
  } else {
    if (frm.doc.additional_reporting_total > 2) {
      frm.set_intro('');
      frm.set_intro('Reporting to be done to SAST and NDU in 2 days');
    } else {
      if (frm.doc.additional_release_total > 2) {
        frm.set_intro('');
        frm.set_intro('Reporting to be done to SAST and NDU in 2 days');
      }
    }
  }
}

function setClCodeFilter(frm) {
  frm.fields_dict.initial_reporting_table.grid.get_field('cl_code').get_query =
    function (doc, cdt, cdn) {
      const d = locals[cdt][cdn];
      return {
        filters: [['cnp_customer_code', '=', d.customer]],
      };
    };

  frm.fields_dict.additional_reporting_table.grid.get_field(
    'cl_code',
  ).get_query = function (doc, cdt, cdn) {
    const d = locals[cdt][cdn];
    return {
      filters: [['cnp_customer_code', '=', d.customer]],
    };
  };

  frm.fields_dict.initial_reporting_table.grid.get_field('form_w').get_query =
    function (doc, cdt, cdn) {
      // eslint-disable-next-line no-unused-vars
      const d = locals[cdt][cdn];
      return {
        filters: [['scrip_code', '=', frm.doc.scrip_code]],
      };
    };

  frm.fields_dict.additional_reporting_table.grid.get_field(
    'form_w',
  ).get_query = function (doc, cdt, cdn) {
    // eslint-disable-next-line no-unused-vars
    const d = locals[cdt][cdn];
    return {
      filters: [['scrip_code', '=', frm.doc.scrip_code]],
    };
  };
}

frappe.ui.form.on('Canopi SAST Initial Reporting', {
  cl_code: function (frm, cdt, cdn) {
    getProductInitialReporting(frm, cdt, cdn);
  },
  percentage: function (frm, cdt, cdn) {
    setInitialTotalPercentage(frm, cdt, cdn);
  },
  pledge_release: function (frm, cdt, cdn) {
    setInitialTotalPercentage(frm, cdt, cdn);
  },
  initial_reporting_table_add: function (frm, cdt, cdn) {
    setInitialTotalPercentage(frm, cdt, cdn);
  },
  initial_reporting_table_remove: function (frm, cdt, cdn) {
    setInitialTotalPercentage(frm, cdt, cdn);
  },
});

function setInitialTotalPercentage(frm, cdt, cdn) {
  // eslint-disable-next-line no-unused-vars
  const d = locals[cdt][cdn];
  let initial_reporting_total = 0;
  frm.doc.initial_reporting_table.forEach(function (d) {
    if (d.percentage > 0) {
      if (d.pledge_release === 'Add Pledge')
        initial_reporting_total += d.percentage;
      else initial_reporting_total -= d.percentage;
    }
  });
  frm.set_value('initial_reporting_total', initial_reporting_total);
  refresh_field('initial_reporting_total');
  setIntro(frm);
}

frappe.ui.form.on('Canopi SAST Additional Reporting', {
  cl_code: function (frm, cdt, cdn) {
    getProductAdditionalReporting(frm, cdt, cdn);
  },
  percentage: function (frm, cdt, cdn) {
    setAdditionalTotalPercentage(frm, cdt, cdn);
  },
  pledge_release: function (frm, cdt, cdn) {
    setAdditionalTotalPercentage(frm, cdt, cdn);
  },
  initial_reporting_table_add: function (frm, cdt, cdn) {
    setAdditionalTotalPercentage(frm, cdt, cdn);
  },
  initial_reporting_table_remove: function (frm, cdt, cdn) {
    setAdditionalTotalPercentage(frm, cdt, cdn);
  },
});

function setAdditionalTotalPercentage(frm, cdt, cdn) {
  // eslint-disable-next-line no-unused-vars
  const d = locals[cdt][cdn];
  let additional_reporting_total = 0;
  let additional_release_total = 0;
  frm.doc.additional_reporting_table.forEach(function (d) {
    if (d.percentage > 0) {
      if (d.pledge_release === 'Add Pledge')
        additional_reporting_total += d.percentage;
      else {
        additional_reporting_total -= d.percentage;
        additional_release_total += d.percentage;
      }
    }
  });
  frm.set_value('additional_reporting_total', additional_reporting_total);
  refresh_field('additional_reporting_total');
  frm.set_value('additional_release_total', additional_release_total);
  refresh_field('additional_release_total');
  setIntro(frm);
}

function getProductInitialReporting(frm, cdt, cdn) {
  const d = locals[cdt][cdn];
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'ATSL Mandate List',
      doc_name: d.cl_code,
    },
    freeze: true,
    callback: r => {
      d.product = r.message.product_name;
      refresh_field('initial_reporting_table');
    },
  });
}

function getProductAdditionalReporting(frm, cdt, cdn) {
  const d = locals[cdt][cdn];
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'ATSL Mandate List',
      doc_name: d.cl_code,
    },
    freeze: true,
    callback: r => {
      d.product = r.message.product_name;
      refresh_field('additional_reporting_table');
    },
  });
}

function showActivitiesAndnotes(frm) {
  frappe.require('assets/trusteeship_platform/js/common.js', () => {
    showActivities(frm);
    showNotes(frm);
  });
}
