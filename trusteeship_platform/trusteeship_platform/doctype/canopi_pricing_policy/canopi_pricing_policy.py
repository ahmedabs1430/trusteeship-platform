# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class CanopiPricingPolicy(Document):
    def validate(self):
        filters = [
            ["cnp_opportunity", "=", self.opportunity],
            ["workflow_state", "in", "Submitted,Approved,Accepted by Client"],
        ]
        list = frappe.get_list("Quotation", fields=('["*"]'), filters=filters)
        if len(list) > 0:
            frappe.throw(
                "Cannot update because Quotation " + "has been submitted."
            )  # noqa: 501
        else:
            pass

    def before_insert(self):
        if not self.is_insertable:
            frappe.throw("Cannot create Canopi Pricing Policy")

    def on_trash(self):
        quotation = frappe.get_list(
            "Quotation", filters=[["opportunity", "=", self.opportunity]]
        )
        if quotation:
            frappe.throw(
                (
                    "Cannot delete {} {} because Quotation has been "
                    + "created for selected Opportunity {}."
                ).format(self.doctype, self.name, self.opportunity),
                frappe.LinkExistsError,
            )
