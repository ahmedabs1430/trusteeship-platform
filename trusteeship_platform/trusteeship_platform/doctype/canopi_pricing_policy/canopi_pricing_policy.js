// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe */
frappe.ui.form.on('Canopi Pricing Policy', {
  validate: function (frm) {
    setSaveByUser(frm);
  },

  refresh: frm => {
    disableForm(frm);
  },

  after_save: async function (frm) {
    await calInitialFinalCost(frm);
    if (frappe.get_prev_route().length) {
      frappe.set_route(frappe.get_prev_route());
    }
  },
});

function disableForm(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'Quotation',
      filters: [
        ['cnp_opportunity', '=', frm.doc.opportunity],
        ['workflow_state', 'in', 'Submitted,Approved,Accepted by Client'],
      ],
    },
    freeze: true,
    callback: r => {
      if (r.message.length > 0) {
        frm.disable_form();
      }
    },
  });
}

function setSaveByUser(frm) {
  frm.set_value('is_saved_by_user', true);
}

function calInitialFinalCost(frm) {
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.cal_opportunity_product_fee',
    args: {
      doc_name: frm.doc.opportunity,
      product: frm.doc.product_name,
    },
    freeze: true,
  });
}
