/* global frappe */

frappe.listview_settings['Canopi Pricing Policy'] = {
  onload: function (listView) {
    listView.can_create = false;
    listView.page.actions
      .find('[data-label="Delete"]')
      .parent()
      .parent()
      .remove();
  },
};
