from frappe import _


def get_data():
    return {
        "fieldname": "form_w",
        "transactions": [
            {
                "label": _(""),
                "items": [
                    "Canopi SAST Reporting",
                ],
            },
        ],
    }
