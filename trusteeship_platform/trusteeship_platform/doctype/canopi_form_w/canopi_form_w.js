// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs */
frappe.ui.form.on('Canopi Form W', {
  refresh: frm => {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Form W');
    });
  },

  demat_account: frm => {
    if (frm.doc.demat_account) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',

        args: {
          doc_name: frm.doc.demat_account,
          doc_type: 'Canopi Demat Account',
        },
        freeze: true,
        callback: r => {
          frm.set_value('client_name', r.message.client_name);
          frm.set_value('client_id', r.message.client_id);
          frm.set_value('dp_id', r.message.dp_id);
        },
      });
    }
  },
});
