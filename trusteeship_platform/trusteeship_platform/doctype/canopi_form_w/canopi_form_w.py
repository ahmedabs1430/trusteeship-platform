# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class CanopiFormW(Document):
    def on_submit(self):
        create_sast(self)


def create_sast(self):
    if self.if_listed == "Yes":
        sast_doc = frappe.new_doc("Canopi SAST Reporting")
        sast_doc.form_w = self.name
        sast_doc.scrip_code = self.scrip_code
        sast_doc.save()
