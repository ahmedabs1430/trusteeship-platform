// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
frappe.listview_settings['Canopi Post Execution Checklist'] = {
  refresh: function (listview) {
    listview.page.btn_primary.hide();
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
  },
};
