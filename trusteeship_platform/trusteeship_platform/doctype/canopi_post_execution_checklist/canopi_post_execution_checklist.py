# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import json
from datetime import datetime

import frappe
import pytz
from frappe.desk.form import assign_to
from frappe.model.document import Document
from frappe.utils import cstr

from trusteeship_platform.custom_methods import copy_s3_object  # noqa

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    asign_task,
    get_doc_list,
    update_tl_ops,
)


class CanopiPostExecutionChecklist(Document):
    def validate(self):
        validate_file_attachments(self)
        doc_type_list = [
            "Canopi Pre Execution Checklist",
            "Canopi Documentation",
        ]
        update_tl_ops(self, doc_type_list)

    def before_save(self):
        calculate_completion_percent(self)

    def after_insert(self):
        get_exception_tracking_details(self)
        create_all_documents(self)

    def on_submit(self):
        pre_execution = frappe.db.get_value(
            "Canopi Pre Execution Checklist",
            {"operations": self.operations},
            ["docstatus"],
        )
        if pre_execution == 0:
            frappe.throw("Pre Execution should be submitted")


def create_all_documents(self):
    operations_doc = frappe.get_doc("ATSL Mandate List", self.operations)  # noqa: 501

    item_doc = frappe.get_doc("Item", operations_doc.product_name)

    related_doctypes_map = [
        {
            "doctype": "Canopi SEBI Application",
            "item_checklist": item_doc.cnp_sebi_application_checklist,
        },
        {
            "doctype": "Canopi Stages of Offer Document",
            "item_checklist": item_doc.cnp_stages_of_offer_document_checklist,
        },
        {
            "doctype": "Canopi Document Based Compliances",
            "item_checklist": item_doc.cnp_document_based_compliances_checklist,  # noqa: 501
        },
        {
            "doctype": "Canopi Event Based Compliances",
            "item_checklist": item_doc.cnp_event_based_compliances_checklist,  # noqa: 501
        },
    ]

    for doc in related_doctypes_map:
        create_related_documents(self, doc["doctype"], doc["item_checklist"])


def create_related_documents(self, doctype, checklist):
    for item in checklist:
        newdoc = frappe.new_doc(doctype)
        newdoc.post_execution_name = self.name
        newdoc.document_code = item.document_code
        newdoc.document_name = item.description
        newdoc.insert()


def get_exception_tracking_details(self):
    pre_execution_doc = get_doc_list(
        doctype="Canopi Pre Execution Checklist",
        filters=[["operations", "=", self.operations]],
    )
    for pre in pre_execution_doc:
        doc = frappe.get_doc("Canopi Pre Execution Checklist", pre.name)
        exception_tracking_details = {}
        details = []
        if (doc.product_name.upper() == "DTE") or (
            doc.product_name.upper() == "STE"
        ):  # noqa: 501
            details = json.loads(doc.type_of_security_details)
        elif doc.pre_execution_details:
            details = json.loads(doc.pre_execution_details)
        if details:
            for asset in details:
                exception_tracking_details[asset] = []
                for index, asset_details in enumerate(details[asset]):
                    if asset_details["track_exception"] == "Yes":
                        if asset_details.get("s3_key"):
                            source_key = asset_details["s3_key"]
                            timezone = pytz.timezone(
                                frappe.defaults.get_defaults().get("time_zone")
                            )
                            destination_key = (
                                cstr(frappe.local.site)
                                + "/Post Execution Checklist/"
                                + self.name
                                + "/Exception Tracking/"
                                + asset.replace("_", " ").title()
                                + "/"
                                + asset_details["document_code"]
                                + "/"
                                + str(datetime.now(tz=timezone))
                                + "/"
                                + asset_details["file_name"]
                            )
                            copy_s3_object(source_key, destination_key)
                            asset_details["s3_key"] = destination_key
                            asset_details["upload_date"] = str(
                                datetime.now().date()
                            )  # noqa: 501
                        exception_tracking_details[asset].append(asset_details)
            self.exception_tracking_details = json.dumps(
                exception_tracking_details
            )  # noqa: 501
            self.save()


def validate_file_attachments(self):
    details = json.loads(self.type_of_security_details)
    for asset in details:
        for index, asset_details in enumerate(details[asset]):
            if asset_details.get("file"):
                frappe.throw(
                    "Cannot save document with some attached file. "
                    + "Either remove or upload them."
                )


def calculate_completion_percent(self):
    if (
        self.product_name.upper() == "DTE"
        or self.product_name.upper() == "STE"  # noqa: 501
    ):
        rows = json.loads(self.type_of_security_details)
        assign_completion_percent(self, rows)
    elif (
        self.product_name.upper() == "EA"
        or self.product_name.upper() == "FA"
        or self.product_name.upper() == "INVIT"
        or self.product_name.upper() == "REIT"
    ):
        rows = json.loads(self.post_execution_details)
        assign_completion_percent(self, rows)


def assign_completion_percent(self, rows):
    exception_tracking_details = json.loads(self.exception_tracking_details)
    rows = list(rows.values())
    exception_tracking_securities = list(exception_tracking_details.values())
    total_type_of_securities = [item for sublist in rows for item in sublist]
    total_type_exception_tracking_securities = [
        item for sublist in exception_tracking_securities for item in sublist
    ]
    total_type_of_securities = (
        total_type_of_securities + total_type_exception_tracking_securities
    )
    if len(total_type_of_securities) != 0:
        total_rows = len(total_type_of_securities)

        completed_type_of_securities = [
            item for item in total_type_of_securities if item.get("s3_key")
        ]

        completed_rows = len(completed_type_of_securities)

        percent = (completed_rows / total_rows) * 100
        self.completion_percent = round(percent, 2)


@frappe.whitelist()
def save_sebi_application(
    post_execution_name,
    values,
):
    field_values = json.loads(values)
    sub_name = field_values["name"] if "name" in field_values else ""
    if "document_code" in field_values:
        document_code = field_values["document_code"]
    else:
        document_code = ""
    if "document_name" in field_values:
        document_name = field_values["document_name"]
    else:
        document_name = ""
    if "upload_date" in field_values:
        upload_date = field_values["upload_date"]
    else:
        upload_date = ""
    comments = field_values["comments"] if "comments" in field_values else ""

    if sub_name:
        if (
            frappe.db.count(
                "Canopi SEBI Application",
                {
                    "post_execution_name": post_execution_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi SEBI Application", {"name": sub_name}
            )  # noqa: 501
            post_execution_name = doc.post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi SEBI Application",
                {
                    "post_execution_name": post_execution_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:

            doc = frappe.new_doc("Canopi SEBI Application")
            doc.post_execution_name = post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()


@frappe.whitelist()
def save_stages_of_offer_document(
    post_execution_name,
    values,
):
    field_values = json.loads(values)
    sub_name = field_values["name"] if "name" in field_values else ""
    if "document_code" in field_values:
        document_code = field_values["document_code"]
    else:
        document_code = ""
    if "document_name" in field_values:
        document_name = field_values["document_name"]
    else:
        document_name = ""
    if "upload_date" in field_values:
        upload_date = field_values["upload_date"]
    else:
        upload_date = ""

    comments = field_values["comments"] if "comments" in field_values else ""

    if sub_name:
        if (
            frappe.db.count(
                "Canopi Stages of Offer Document",
                {
                    "post_execution_name": post_execution_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Stages of Offer Document", {"name": sub_name}
            )  # noqa: 501
            post_execution_name = doc.post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Stages of Offer Document",
                {
                    "post_execution_name": post_execution_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:

            doc = frappe.new_doc("Canopi Stages of Offer Document")
            doc.post_execution_name = post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()


@frappe.whitelist()
def save_document_based_compliances(
    post_execution_name,
    values,
):
    field_values = json.loads(values)
    sub_name = field_values["name"] if "name" in field_values else ""
    if "document_code" in field_values:
        document_code = field_values["document_code"]
    else:
        document_code = ""
    if "document_name" in field_values:
        document_name = field_values["document_name"]
    else:
        document_name = ""
    if "upload_date" in field_values:
        upload_date = field_values["upload_date"]
    else:
        upload_date = ""
    comments = field_values["comments"] if "comments" in field_values else ""

    if sub_name:
        if (
            frappe.db.count(
                "Canopi Document Based Compliances",
                {
                    "post_execution_name": post_execution_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Document Based Compliances", {"name": sub_name}
            )  # noqa: 501
            post_execution_name = doc.post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Document Based Compliances",
                {
                    "post_execution_name": post_execution_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:

            doc = frappe.new_doc("Canopi Document Based Compliances")
            doc.post_execution_name = post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()


@frappe.whitelist()
def save_event_based_compliances(
    post_execution_name,
    values,
):
    field_values = json.loads(values)
    sub_name = field_values["name"] if "name" in field_values else ""
    if "document_code" in field_values:
        document_code = field_values["document_code"]
    else:
        document_code = ""
    if "document_name" in field_values:
        document_name = field_values["document_name"]
    else:
        document_name = ""
    if "upload_date" in field_values:
        upload_date = field_values["upload_date"]
    else:
        upload_date = ""
    comments = field_values["comments"] if "comments" in field_values else ""

    if sub_name:
        if (
            frappe.db.count(
                "Canopi Event Based Compliances",
                {
                    "post_execution_name": post_execution_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Event Based Compliances", {"name": sub_name}
            )  # noqa: 501
            post_execution_name = doc.post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Event Based Compliances",
                {
                    "post_execution_name": post_execution_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:

            doc = frappe.new_doc("Canopi Event Based Compliances")
            doc.post_execution_name = post_execution_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.save()


@frappe.whitelist()
def send_to_legal(docname, child_doctype, docs, vetting=0, noting=0):
    if vetting == "1":
        purpose = "Vetting"
    if noting == "1":
        purpose = "Noting"
    docsArr = json.loads(docs)
    for x in docsArr:
        child_docname = x["name"]
        doc = frappe.get_doc(child_doctype, child_docname)
        doc.send_to_legal = "Yes"
        doc.purpose_of_legal = purpose
        doc.save()

    users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Legal Maker"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for user in users:
        try:
            asign_task(
                {
                    "assign_to": [user.email],
                    "doctype": "Canopi Post Execution Checklist",
                    "name": docname,
                    "description": "",
                    "priority": "High",
                    "notify": 1,
                    "action": "",
                }
            )
        except assign_to.DuplicateToDoError:
            frappe.message_log.pop()
            pass


@frappe.whitelist()
def del_doc(doctype, docname):
    frappe.delete_doc(doctype, docname)
