/* eslint-disable indent */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-escape */
/*  eslint-disable max-len */
// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
let stages_of_offer_document = [];
let sebi_application = [];
let document_based_compliances = [];
let event_based_compliances = [];
const files_upload = {};
const files_upload_exception = {};
frappe.ui.form.on('Canopi Post Execution Checklist', {
  refresh: frm => {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    show_activities(frm);
    show_notes(frm);
    set_operations_filter(frm);
    frm.trigger('ops_servicing_rm');
    frm.trigger('tl_representative');
    if (frm.doc.__islocal) {
      frm.trigger('operations');
    } else {
      load_asset_tables(frm);
      getFileUpload(frm);
    }
    add_percent_pill(frm);
    headerButtons(frm);
    load_sebi_application(frm);
    load_stages_of_offer_document(frm);
    load_document_based_compliances(frm);
    load_event_based_compliances(frm);
  },

  after_save: frm => {
    empty_html_fields(frm);
  },

  operations: async frm => {
    if (!frm.doc.operations) {
      frm.fields_dict.immovable_asset_section.df.hidden = 1;
      frm.fields_dict.movable_asset_section.df.hidden = 1;
      frm.fields_dict.pledge_section.df.hidden = 1;
      frm.fields_dict.personal_guarantee_section.df.hidden = 1;
      frm.fields_dict.corporate_guarantee_section.df.hidden = 1;
      frm.fields_dict.ndu_section.df.hidden = 1;
      frm.fields_dict.intangible_asset_section.df.hidden = 1;
      frm.fields_dict.motor_vehicle_section.df.hidden = 1;
      frm.fields_dict.financial_asset_section.df.hidden = 1;
      frm.fields_dict.assignment_of_rights_section.df.hidden = 1;
      frm.fields_dict.current_asset_section.df.hidden = 1;
      frm.fields_dict.debt_service_reserve_account_section.df.hidden = 1;
      frm.fields_dict.others_section.df.hidden = 1;
      frm.fields_dict.exception_tracking_section.df.hidden = 1;

      frm.refresh_fields();
    }
    empty_html_fields(frm);
    frm.doc.type_of_security_details = JSON.stringify({});
    if (frm.doc.operations) {
      await populate_form(frm);
    }
  },
  ops_servicing_rm: frm => {
    validate_checker_and_maker(frm);
  },

  tl_representative: frm => {
    validate_checker_and_maker(frm);
  },
});

async function get_doc_list(frm, doctype, filters = [], order_by = '') {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype,
      filters,
      order_by,
    },
    freeze: true,
  });

  return response.message;
}

async function get_doc(doctype, docname) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: doctype,
      doc_name: docname,
    },
  });

  return response.message;
}

function headerButtons(frm) {
  if (frm.doc.operations) {
    frm.add_custom_button('Main', () => {
      window.open(`/app/atsl-mandate-list/${frm.doc.operations}`);
    });
  }
  if (!frm.doc.__islocal && frm.doc.operations) {
    let transactionDetails;
    if (frm.doc.product_name.toUpperCase() === 'FA') {
      transactionDetails = 'Canopi Transaction Details-FA';
    } else {
      transactionDetails = 'Transaction Details';
    }
    frm.add_custom_button(
      transactionDetails,
      () => {
        const linkedDoctypes = {
          [transactionDetails]: {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, transactionDetails, r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Pre-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Pre Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Pre Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Documentation',
      () => {
        const linkedDoctypes = {
          'Canopi Documentation': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Documentation', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Post-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Post Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        if (!frm.doc.__islocal) {
          frm.scroll_to_field('customer_code');
        }
      },
      'Operations',
    );
  }
}

function set_operations_filter(frm) {
  frm.set_query('operations', function () {
    return {
      filters: [['ATSL Mandate List', 'docstatus', '=', 1]],
    };
  });
}

async function populate_form(frm) {
  const response = await get_multiple_docs(frm);

  frm.doc.company = response.message.opportunity.customer_name;
  frm.doc.person_name = response.message.atsl_mandate_list.person_name;
  frm.doc.customer_code = response.message.atsl_mandate_list.cnp_customer_code;
  frm.doc.product_name = response.message.item.name;
  frm.doc.ops_servicing_rm =
    response.message.atsl_mandate_list.ops_servicing_rm;
  frm.doc.tl_representative =
    response.message.atsl_mandate_list.tl_representative;
  frm.refresh_field('customer_code');
  frm.refresh_field('person_name');
  frm.refresh_field('company');
  frm.refresh_field('ops_servicing_rm');
  frm.refresh_field('tl_representative');

  get_html_field_values(frm, response);
}

function get_multiple_docs(frm) {
  const docs = [
    {
      doctype: 'ATSL Mandate List',
      docname: frm.doc.operations,
    },
    {
      doctype: 'Item',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'product_name',
    },
    {
      doctype: 'Canopi Pricing Policy',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: ['opportunity', 'product_name'],
    },
    {
      doctype: 'Opportunity',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'opportunity',
    },
  ];
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.get_docs_with_linked_childs',
    args: { docs },
    freeze: true,
  });
}

function get_html_field_values(frm, response) {
  let security_types = [];

  if (
    frm.doc.product_name.toUpperCase() === 'STE' ||
    frm.doc.product_name.toUpperCase() === 'DTE'
  ) {
    security_types = response.message.atsl_mandate_list.security_type.map(
      types =>
        types.change_in_security === 'Yes'
          ? types.new_security_type
          : types.existing_security_type,
    );
  }

  const immovable_asset = [];
  const movable_asset = [];
  const pledge = [];
  const personal_guarantee = [];
  const corporate_guarantee = [];
  const ndu = [];
  const intangible_asset = [];
  const motor_vehicle = [];
  const financial_asset = [];
  const assignment_of_rights = [];
  const current_asset = [];
  const debt_service_reserve_account = [];
  const others = [];
  const post_execution_details = [];
  response.message.item.cnp_post_execution_checklist.forEach(
    post_execution_template => {
      if (
        check_conditions(
          get_type_of_security_condition(post_execution_template, frm),
          security_types,
          get_pricing_policy_condition(post_execution_template),
          response.message.canopi_pricing_policy,
        )
      ) {
        if (
          frm.doc.product_name.toUpperCase() === 'EA' ||
          frm.doc.product_name.toUpperCase() === 'FA' ||
          frm.doc.product_name.toUpperCase() === 'INVIT' ||
          frm.doc.product_name.toUpperCase() === 'REIT'
        ) {
          post_execution_details.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            document_code: post_execution_template.document_code,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(
            x =>
              x === 'Immovable Asset' &&
              post_execution_template.immovable === 1,
          )
        ) {
          immovable_asset.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            document_code: post_execution_template.document_code,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Movable Asset') &&
          post_execution_template.movable === 1
        ) {
          movable_asset.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Pledge') &&
          post_execution_template.pledge === 1
        ) {
          pledge.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            document_code: post_execution_template.document_code,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Personal Guarantee') &&
          post_execution_template.personal_guarantee === 1
        ) {
          personal_guarantee.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            document_code: post_execution_template.document_code,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Corporate Guarantee') &&
          post_execution_template.corporate_guarantee === 1
        ) {
          corporate_guarantee.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'NDU') &&
          post_execution_template.ndu === 1
        ) {
          ndu.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Intangible Asset') &&
          post_execution_template.intangible === 1
        ) {
          intangible_asset.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }
        if (
          security_types.some(x => x === 'Motor Vehicle') &&
          post_execution_template.motor === 1
        ) {
          motor_vehicle.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Financial Asset') &&
          post_execution_template.financial === 1
        ) {
          financial_asset.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Assignment of Rights') &&
          post_execution_template.assignment_of_rights === 1
        ) {
          assignment_of_rights.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Current Asset') &&
          post_execution_template.current === 1
        ) {
          current_asset.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(
            x => x === 'Debt Service Reserve Account (DSRA)',
          ) &&
          post_execution_template.dsra === 1
        ) {
          debt_service_reserve_account.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }

        if (
          security_types.some(x => x === 'Others') &&
          post_execution_template.others === 1
        ) {
          others.push({
            description: post_execution_template.description,
            comments: '',
            is_date_of_registration_visible:
              post_execution_template.date_of_registration,
            is_sro_name_visible: post_execution_template.sro_name,
            is_reg_number_visible: post_execution_template.reg_number,
            is_roc_filing_date_visible: post_execution_template.roc_filing_date,
            is_cersai_filing_date_visible:
              post_execution_template.cersai_filing_date,
            is_pledge_creation_date_visible:
              post_execution_template.pledge_creation_date,
            document_code: post_execution_template.document_code,
            date_of_registration: '',
            sro_name: '',
            reg_number: '',
            roc_filing_date: '',
            cersai_filing_date: '',
            pledge_creation_date: '',
          });
        }
      }
    },
  );
  const type_of_securities = {
    immovable_asset,
    movable_asset,
    pledge,
    personal_guarantee,
    corporate_guarantee,
    ndu,
    intangible_asset,
    motor_vehicle,
    financial_asset,
    assignment_of_rights,
    current_asset,
    debt_service_reserve_account,
    others,
  };

  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    const type_of_security_details = get_filtered_type_of_security(
      security_types,
      type_of_securities,
    );
    frm.doc.type_of_security_details = JSON.stringify(type_of_security_details);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    frm.doc.post_execution_details = JSON.stringify({
      post_execution_details,
    });
  }
  load_asset_tables(frm);
}

function get_filtered_type_of_security(source, target) {
  let type_of_securities = Object.entries(target);
  type_of_securities = type_of_securities.filter(([key, value]) =>
    source.some(x => title_to_snake_case(remove_parenthesis(x)) === key),
  );
  return Object.fromEntries(type_of_securities);
}

function title_to_snake_case(str) {
  if (!str) return;

  return str
    .split(' ')
    .map(word => word.toLowerCase())
    .join('_');
}

function remove_parenthesis(str) {
  if (!str) return;
  return str.replace(/ *\([^)]*\) */g, '');
}

function get_pricing_policy_condition(post_execution_checklist) {
  const conditions = [];
  const pricing_policy_list = {
    no_of_securities: 'no_of_securities',
    listed_yes_no: 'listed_yes_no',
    whether_public_issue: 'whether_public_issue',
  };

  if (post_execution_checklist.secured) {
    conditions.push(pricing_policy_list.no_of_securities);
  }
  if (post_execution_checklist.listed) {
    conditions.push(pricing_policy_list.listed_yes_no);
  }
  if (post_execution_checklist.whether_public_issue) {
    conditions.push(pricing_policy_list.whether_public_issue);
  }

  return conditions;
}

function get_type_of_security_condition(post_execution_checklist, frm) {
  const conditions = [];
  if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  )
    return conditions;
  const type_of_security_list = {
    immovable: 'Immovable Asset',
    movable: 'Movable Asset',
    pledge: 'Pledge',
    personal_guarantee: 'Personal Guarantee',
    corporate_guarantee: 'Corporate Guarantee',
    ndu: 'NDU',
    intangible: 'Intangible Asset',
    motor: 'Motor Vehicle',
    financial: 'Financial Asset',
    assignment_of_rights: 'Assignment of Rights',
    current: 'Current Asset',
    dsra: 'Debt Service Reserve Account (DSRA)',
    others: 'Others',
  };

  if (post_execution_checklist.immovable) {
    conditions.push(type_of_security_list.immovable);
  }
  if (post_execution_checklist.movable) {
    conditions.push(type_of_security_list.movable);
  }
  if (post_execution_checklist.pledge) {
    conditions.push(type_of_security_list.pledge);
  }
  if (post_execution_checklist.personal_guarantee) {
    conditions.push(type_of_security_list.personal_guarantee);
  }
  if (post_execution_checklist.corporate_guarantee) {
    conditions.push(type_of_security_list.corporate_guarantee);
  }
  if (post_execution_checklist.ndu) {
    conditions.push(type_of_security_list.ndu);
  }
  if (post_execution_checklist.intangible) {
    conditions.push(type_of_security_list.intangible);
  }
  if (post_execution_checklist.motor) {
    conditions.push(type_of_security_list.motor);
  }
  if (post_execution_checklist.financial) {
    conditions.push(type_of_security_list.financial);
  }
  if (post_execution_checklist.assignment_of_rights) {
    conditions.push(type_of_security_list.assignment_of_rights);
  }
  if (post_execution_checklist.current) {
    conditions.push(type_of_security_list.current);
  }
  if (post_execution_checklist.dsra) {
    conditions.push(type_of_security_list.dsra);
  }
  if (post_execution_checklist.others) {
    conditions.push(type_of_security_list.others);
  }
  return conditions;
}

function check_conditions(
  type_of_security_condition,
  type_of_security,
  pricing_policy_condition,
  pricing_policy,
) {
  let is_security_type_valid = type_of_security_condition.length === 0;
  let is_pricing_policy_valid = pricing_policy_condition.length === 0;

  if (type_of_security_condition.length !== 0) {
    is_security_type_valid = type_of_security_condition.some(element =>
      type_of_security.includes(element),
    );
  }

  pricing_policy_condition.forEach(element => {
    if (
      (element === 'no_of_securities' &&
        pricing_policy.no_of_securities !== 0) ||
      ((element === 'listed_yes_no' || element === 'whether_public_issue') &&
        pricing_policy[`${element}`] === 'Yes')
    ) {
      is_pricing_policy_valid = true;
    } else {
      is_pricing_policy_valid = false;
    }
  });

  // eslint-disable-next-line no-constant-condition
  return true
    ? is_pricing_policy_valid === true && is_security_type_valid === true
    : false;
}

function load_asset_tables(frm) {
  empty_html_fields(frm);
  hide_unhide_sections(frm);

  setTimeout(() => {
    render_template(frm);
    add_template_button_events(frm);
  }, 1500);
}

function hide_unhide_sections(frm) {
  frm.fields_dict.immovable_asset_section.df.hidden =
    get_asset_values(frm).immovable_asset !== undefined ? 0 : 1;
  frm.fields_dict.movable_asset_section.df.hidden =
    get_asset_values(frm).movable_asset !== undefined ? 0 : 1;
  frm.fields_dict.pledge_section.df.hidden =
    get_asset_values(frm).pledge !== undefined ? 0 : 1;
  frm.fields_dict.personal_guarantee_section.df.hidden =
    get_asset_values(frm).personal_guarantee !== undefined ? 0 : 1;
  frm.fields_dict.corporate_guarantee_section.df.hidden =
    get_asset_values(frm).corporate_guarantee !== undefined ? 0 : 1;
  frm.fields_dict.ndu_section.df.hidden =
    get_asset_values(frm).ndu !== undefined ? 0 : 1;
  frm.fields_dict.intangible_asset_section.df.hidden =
    get_asset_values(frm).intangible_asset !== undefined ? 0 : 1;
  frm.fields_dict.others_section.df.hidden =
    get_asset_values(frm).others !== undefined ? 0 : 1;

  frm.fields_dict.motor_vehicle_section.df.hidden =
    get_asset_values(frm).motor_vehicle !== undefined ? 0 : 1;
  frm.fields_dict.financial_asset_section.df.hidden =
    get_asset_values(frm).financial_asset !== undefined ? 0 : 1;
  frm.fields_dict.assignment_of_rights_section.df.hidden =
    get_asset_values(frm).assignment_of_rights !== undefined ? 0 : 1;
  frm.fields_dict.current_asset_section.df.hidden =
    get_asset_values(frm).current_asset !== undefined ? 0 : 1;
  frm.fields_dict.debt_service_reserve_account_section.df.hidden =
    get_asset_values(frm).debt_service_reserve_account !== undefined ? 0 : 1;

  frm.fields_dict.exception_tracking_section.df.hidden =
    get_exception_tracking_values(frm).is_empty;
  frm.refresh_fields();
}

function render_template(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    if (get_asset_values(frm).immovable_asset !== undefined) {
      frm.set_df_property(
        'immovable_asset_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).immovable_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-immovable',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).movable_asset !== undefined) {
      frm.set_df_property(
        'movable_asset_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).movable_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-movable',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
    if (get_asset_values(frm).pledge !== undefined) {
      frm.set_df_property(
        'pledge_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).pledge,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-pledge',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
    if (get_asset_values(frm).personal_guarantee !== undefined) {
      frm.set_df_property(
        'personal_guarantee_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).personal_guarantee,
          id:
            frm.doc.__islocal === 1 ? '' : frm.doc.name + '-personal-guarantee',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).corporate_guarantee !== undefined) {
      frm.set_df_property(
        'corporate_guarantee_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).corporate_guarantee,
          id:
            frm.doc.__islocal === 1
              ? ''
              : frm.doc.name + '-corporate-guarantee',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).ndu !== undefined) {
      frm.set_df_property(
        'ndu_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).ndu,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-ndu',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).intangible_asset !== undefined) {
      frm.set_df_property(
        'intangible_asset_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).intangible_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-intangible',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
    if (get_asset_values(frm).motor_vehicle !== undefined) {
      frm.set_df_property(
        'motor_vehicle_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).motor_vehicle,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-motor',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
    if (get_asset_values(frm).financial_asset !== undefined) {
      frm.set_df_property(
        'financial_asset_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).financial_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-financial',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
    if (get_asset_values(frm).assignment_of_rights !== undefined) {
      frm.set_df_property(
        'assignment_of_rights_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).assignment_of_rights,
          id:
            frm.doc.__islocal === 1
              ? ''
              : frm.doc.name + '-assignment-of-rights',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).current_asset !== undefined) {
      frm.set_df_property(
        'current_asset_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).current_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-current',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).debt_service_reserve_account !== undefined) {
      frm.set_df_property(
        'debt_service_reserve_account_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).debt_service_reserve_account,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-dsra',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (get_asset_values(frm).others !== undefined) {
      frm.set_df_property(
        'others_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).others,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-others',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    if (get_asset_values(frm).post_execution_details !== undefined) {
      frm.set_df_property(
        'post_execution_details_html',
        'options',
        frappe.render_template('post_execution_checklist_table', {
          doc: get_asset_values(frm).post_execution_details,
          id:
            frm.doc.__islocal === 1
              ? ''
              : frm.doc.name + '-post-execution-details',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
  }

  if (frm.doc.__islocal !== 1 && !get_exception_tracking_values(frm).is_empty) {
    frm.set_df_property(
      'exception_tracking_html',
      'options',
      frappe.render_template('post_execution_exception_tracking', {
        doc: get_exception_tracking_values(frm),
        id: frm.doc.__islocal === 1 ? '' : frm.doc.name,
        docstatus: frm.doc.docstatus,
      }),
    );
  }
}

// Canopi SEBI Application
async function load_sebi_application(frm) {
  $(frm.fields_dict.sebi_application_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() === 'REIT' ||
    frm.doc.product_name?.toUpperCase() === 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      sebi_application = await get_doc_list(
        frm,
        'Canopi SEBI Application',
        [['post_execution_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (sebi_application.length === 0) {
        sebi_application = [];
      }

      $(frm.fields_dict.sebi_application_html.wrapper).empty();
      frm.set_df_property(
        'sebi_application_html',
        'options',
        frappe.render_template('sebi_application_table', {
          doc: sebi_application,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('sebi_application_html');
      $('.' + frm.doc.name + '-sebi_application-edit-row').on(
        'click',
        function () {
          sebi_application_dialog(
            frm,
            sebi_application[this.id],
            'Edit Documents - Canopi SEBI Application',
          );
        },
      );

      $('.sebi_application_chkAll').click(function () {
        if (this.checked) {
          $('.sebi_application_chk').prop('checked', true);
        } else {
          $('.sebi_application_chk').prop('checked', false);
        }
      });
      sebi_application_add(frm);
      sebi_application_delete(frm);
      sebi_application_upload_events(frm);
      sebi_application_download_events(frm);
      sebi_application_email(frm);
      sebi_application_send_to_legal(frm);
    }
  }
}

function sebi_application_add(frm) {
  $('.' + frm.doc.name + '-sebi_application-add').on('click', function () {
    sebi_application_dialog(frm, {}, 'Add Documents - Canopi SEBI Application');
  });
}

function sebi_application_delete(frm) {
  $('.' + frm.doc.name + '-sebi_application-delete').on('click', function () {
    $('input[class="sebi_application_chk"]:checked').each(function () {
      sebi_application.forEach(x => {
        if (x.name === this.value && x.send_to_legal !== 'No')
          frappe.throw('Please select row of <b>No</b> Send To Legal.');
      });
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.del_doc',
        args: {
          doctype: 'Canopi SEBI Application',
          docname: this.value,
        },
        freeze: true,

        callback: async r => {
          delete sebi_application_files[this.value];
          await load_sebi_application(frm);
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('sebi_application_html');
            }, 100);
          });
        },
      });
    });
  });
}

function sebi_application_download_events(frm) {
  $('.' + frm.doc.name + '-sebi_application-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function sebi_application_upload_events(frm) {
  sebi_application_attach_file_event(frm);
  sebi_application_upload_all_event(frm);
}

let sebi_application_files = {};
function sebi_application_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-sebi_application-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-sebi_application-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-sebi_application-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        $('#sebi_application_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-sebi_application-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-sebi_application-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );
        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined') sebi_application_files[row_name] = {};
        sebi_application_files[row_name].file = file[0];
        sebi_application_files[row_name].file_name = file[0].name;
        sebi_application_files[row_name].row_name = row_name;
        add_sebi_application_preview_event(frm);
        add_sebi_application_remove_file_event(frm);
      }
    },
  );
}

function add_sebi_application_preview_event(frm) {
  $('.' + frm.doc.name + '-sebi_application-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        sebi_application_files[rowname].file,
      );

      frappe.dom.unfreeze();

      window.open(blobUrl, '_blank');
    },
  );
}

function add_sebi_application_remove_file_event(frm) {
  $('.' + frm.doc.name + '-sebi_application-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete sebi_application_files[rowname];
    },
  );
}

function sebi_application_upload_all_event(frm) {
  $('.' + frm.doc.name + '-sebi_application-upload-all').on('click', () => {
    if (Object.keys(sebi_application_files).length > 0) {
      preExecutionS3Upload(
        frm,
        sebi_application_files,
        'Canopi SEBI Application',
        'sebi_application_html',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function sebi_application_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          sebi_application_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Description',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.save_sebi_application',
        args: {
          documentation_name: '',
          post_execution_name: frm.doc.name,
          values,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_sebi_application(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('sebi_application_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

let s3_attachments = {};
let after_email_update_field = '';
let child_doctype = '';
function sebi_application_email(frm) {
  $('.' + frm.doc.name + '-sebi_application-email').on('click', function () {
    email_dialog('');
    s3_attachments = {};
    after_email_update_field = 'send_email';
    child_doctype = 'Canopi SEBI Application';
    $('input[class="sebi_application_chk"]:checked').each(function () {
      const attachment = [];
      attachment.name = this.value;
      attachment.file_name = $(
        '#' + this.value + '_sebi_application_file_name',
      ).val();
      attachment.file_url = $(
        '#' + this.value + '_sebi_application_file_path',
      ).val();
      s3_attachments[this.value] = {};
      s3_attachments[this.value].file_name = $(
        '#' + this.value + '_sebi_application_file_name',
      ).val();
      s3_attachments[this.value].file_url = $(
        '#' + this.value + '_sebi_application_file_path',
      ).val();
      render_attachment_rows(attachment);
    });
  });
}

function sebi_application_send_to_legal(frm) {
  $('.' + frm.doc.name + '-sebi_application-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="sebi_application_chk"]:checked').each(function () {
        if (
          $('#' + this.value + 'sebi_application_send_to_legal').text() ===
          'Yes'
        ) {
          frappe.throw(
            'Some of the selected documents have already been sent to legal',
          );
        }
        if ($('#' + this.value + '_sebi_application_file_name').val() === '') {
          frappe.throw('Please Upload File For Selected Rows');
        }
        selected_documents.push({
          name: this.value,
        });
      });
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi SEBI Application',
        'sebi_application_html',
      );
    },
  );
}

// Stages of Offer Document
async function load_stages_of_offer_document(frm) {
  $(frm.fields_dict.stages_of_offer_document_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() === 'REIT' ||
    frm.doc.product_name?.toUpperCase() === 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      stages_of_offer_document = await get_doc_list(
        frm,
        'Canopi Stages of Offer Document',
        [['post_execution_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (stages_of_offer_document.length === 0) {
        stages_of_offer_document = [];
      }

      $(frm.fields_dict.stages_of_offer_document_html.wrapper).empty();
      frm.set_df_property(
        'stages_of_offer_document_html',
        'options',
        frappe.render_template('stages_of_offer_document_table', {
          doc: stages_of_offer_document,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('stages_of_offer_document_html');
      $('.' + frm.doc.name + '-stages_of_offer_document-edit-row').on(
        'click',
        function () {
          stages_of_offer_document_dialog(
            frm,
            stages_of_offer_document[this.id],
            'Edit Documents - Stages of offer document',
          );
        },
      );

      $('.stages_of_offer_document_chkAll').click(function () {
        if (this.checked) {
          $('.stages_of_offer_document_chk').prop('checked', true);
        } else {
          $('.stages_of_offer_document_chk').prop('checked', false);
        }
      });
      stages_of_offer_document_add(frm);
      stages_of_offer_document_delete(frm);
      stages_of_offer_document_upload_events(frm);
      stages_of_offer_document_download_events(frm);
      stages_of_offer_document_email(frm);
      stages_of_offer_document_send_to_legal(frm);
    }
  }
}

function stages_of_offer_document_add(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-add').on(
    'click',
    function () {
      stages_of_offer_document_dialog(
        frm,
        {},
        'Add Documents - Stages of offer document',
      );
    },
  );
}

function stages_of_offer_document_delete(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-delete').on(
    'click',
    function () {
      $('input[class="stages_of_offer_document_chk"]:checked').each(
        function () {
          stages_of_offer_document.forEach(x => {
            if (x.name === this.value && x.send_to_legal !== 'No')
              frappe.throw('Please select row of <b>No</b> Send To Legal.');
          });
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.del_doc',
            args: {
              doctype: 'Canopi Stages of Offer Document',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete stages_of_offer_document_files[this.value];
              await load_stages_of_offer_document(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('stages_of_offer_document_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function stages_of_offer_document_download_events(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function stages_of_offer_document_upload_events(frm) {
  stages_of_offer_document_attach_file_event(frm);
  stages_of_offer_document_upload_all_event(frm);
}

let stages_of_offer_document_files = {};
function stages_of_offer_document_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-stages_of_offer_document-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-stages_of_offer_document-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-stages_of_offer_document-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        $('#stages_of_offer_document_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-stages_of_offer_document-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-stages_of_offer_document-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          stages_of_offer_document_files[row_name] = {};
        stages_of_offer_document_files[row_name].file = file[0];
        stages_of_offer_document_files[row_name].file_name = file[0].name;
        stages_of_offer_document_files[row_name].row_name = row_name;
        add_stages_of_offer_document_preview_event(frm);
        add_stages_of_offer_document_remove_file_event(frm);
      }
    },
  );
}

function add_stages_of_offer_document_preview_event(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        stages_of_offer_document_files[rowname].file,
      );

      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_stages_of_offer_document_remove_file_event(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete stages_of_offer_document_files[rowname];
    },
  );
}

function stages_of_offer_document_upload_all_event(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-upload-all').on(
    'click',
    () => {
      if (Object.keys(stages_of_offer_document_files).length > 0) {
        preExecutionS3Upload(
          frm,
          stages_of_offer_document_files,
          'Canopi Stages of Offer Document',
          'stages_of_offer_document_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function stages_of_offer_document_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          stages_of_offer_document_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Description',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.save_stages_of_offer_document',
        args: {
          documentation_name: '',
          post_execution_name: frm.doc.name,
          values,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_stages_of_offer_document(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('stages_of_offer_document_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

function stages_of_offer_document_email(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Stages of Offer Document';
      $('input[class="stages_of_offer_document_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_stages_of_offer_document_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_stages_of_offer_document_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_stages_of_offer_document_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_stages_of_offer_document_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function stages_of_offer_document_send_to_legal(frm) {
  $('.' + frm.doc.name + '-stages_of_offer_document-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="stages_of_offer_document_chk"]:checked').each(
        function () {
          if (
            $(
              '#' + this.value + 'stages_of_offer_document_send_to_legal',
            ).text() === 'Yes'
          ) {
            frappe.throw(
              'Some of the selected documents have already been sent to legal',
            );
          }
          if (
            $(
              '#' + this.value + '_stages_of_offer_document_file_name',
            ).val() === ''
          ) {
            frappe.throw('Please Upload File For Selected Rows');
          }
          selected_documents.push({
            name: this.value,
          });
        },
      );
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Stages of Offer Document',
        'stages_of_offer_document_html',
      );
    },
  );
}

// Canopi Document Based Compliances
async function load_document_based_compliances(frm) {
  $(frm.fields_dict.document_based_compliances_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() === 'REIT' ||
    frm.doc.product_name?.toUpperCase() === 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      document_based_compliances = await get_doc_list(
        frm,
        'Canopi Document Based Compliances',
        [['post_execution_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (document_based_compliances.length === 0) {
        document_based_compliances = [];
      }

      $(frm.fields_dict.document_based_compliances_html.wrapper).empty();
      frm.set_df_property(
        'document_based_compliances_html',
        'options',
        frappe.render_template('document_based_compliances_table', {
          doc: document_based_compliances,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('document_based_compliances_html');
      $('.' + frm.doc.name + '-document_based_compliances-edit-row').on(
        'click',
        function () {
          document_based_compliances_dialog(
            frm,
            document_based_compliances[this.id],
            'Edit Documents - Canopi Document Based Compliances',
          );
        },
      );

      $('.document_based_compliances_chkAll').click(function () {
        if (this.checked) {
          $('.document_based_compliances_chk').prop('checked', true);
        } else {
          $('.document_based_compliances_chk').prop('checked', false);
        }
      });
      document_based_compliances_add(frm);
      document_based_compliances_delete(frm);
      document_based_compliances_upload_events(frm);
      document_based_compliances_download_events(frm);
      document_based_compliances_email(frm);
      document_based_compliances_send_to_legal(frm);
    }
  }
}

function document_based_compliances_add(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-add').on(
    'click',
    function () {
      document_based_compliances_dialog(
        frm,
        {},
        'Add Documents - Canopi Document Based Compliances',
      );
    },
  );
}

function document_based_compliances_delete(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-delete').on(
    'click',
    function () {
      $('input[class="document_based_compliances_chk"]:checked').each(
        function () {
          document_based_compliances.forEach(x => {
            if (x.name === this.value && x.send_to_legal !== 'No')
              frappe.throw('Please select row of <b>No</b> Send To Legal.');
          });
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.del_doc',
            args: {
              doctype: 'Canopi Document Based Compliances',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete document_based_compliances[this.value];
              await load_document_based_compliances(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('document_based_compliances_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function document_based_compliances_download_events(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function document_based_compliances_upload_events(frm) {
  document_based_compliances_attach_file_event(frm);
  document_based_compliances_upload_all_event(frm);
}

const document_based_compliances_files = {};
function document_based_compliances_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-document_based_compliances-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-document_based_compliances-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-document_based_compliances-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#document_based_compliances_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-document_based_compliances-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-document_based_compliances-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          document_based_compliances_files[row_name] = {};
        document_based_compliances_files[row_name].file = file[0];
        document_based_compliances_files[row_name].file_name = file[0].name;
        document_based_compliances_files[row_name].row_name = row_name;
        add_document_based_compliances_preview_event(frm);
        add_document_based_compliances_remove_file_event(frm);
      }
    },
  );
}

function add_document_based_compliances_preview_event(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        document_based_compliances_files[rowname].file,
      );

      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_document_based_compliances_remove_file_event(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete document_based_compliances_files[rowname];
    },
  );
}

function document_based_compliances_upload_all_event(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-upload-all').on(
    'click',
    () => {
      if (Object.keys(document_based_compliances_files).length > 0) {
        preExecutionS3Upload(
          frm,
          document_based_compliances_files,
          'Canopi Document Based Compliances',
          'document_based_compliances_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function document_based_compliances_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          document_based_compliances_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Description',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.save_document_based_compliances',
        args: {
          documentation_name: '',
          post_execution_name: frm.doc.name,
          values,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_document_based_compliances(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('document_based_compliances_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

function document_based_compliances_email(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Document Based Compliances';
      $('input[class="document_based_compliances_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_document_based_compliances_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_document_based_compliances_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_document_based_compliances_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_document_based_compliances_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function document_based_compliances_send_to_legal(frm) {
  $('.' + frm.doc.name + '-document_based_compliances-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="document_based_compliances_chk"]:checked').each(
        function () {
          if (
            $(
              '#' + this.value + 'document_based_compliances_send_to_legal',
            ).text() === 'Yes'
          ) {
            frappe.throw(
              'Some of the selected documents have already been sent to legal',
            );
          }
          if (
            $(
              '#' + this.value + '_document_based_compliances_file_name',
            ).val() === ''
          ) {
            frappe.throw('Please Upload File For Selected Rows');
          }
          selected_documents.push({
            name: this.value,
          });
        },
      );
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Document Based Compliances',
        'document_based_compliances_html',
      );
    },
  );
}

// Canopi Event Based Compliances
async function load_event_based_compliances(frm) {
  $(frm.fields_dict.event_based_compliances_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() === 'REIT' ||
    frm.doc.product_name?.toUpperCase() === 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      event_based_compliances = await get_doc_list(
        frm,
        'Canopi Event Based Compliances',
        [['post_execution_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (event_based_compliances.length === 0) {
        event_based_compliances = [];
      }

      $(frm.fields_dict.event_based_compliances_html.wrapper).empty();
      frm.set_df_property(
        'event_based_compliances_html',
        'options',
        frappe.render_template('event_based_compliances_table', {
          doc: event_based_compliances,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('event_based_compliances_html');
      $('.' + frm.doc.name + '-event_based_compliances-edit-row').on(
        'click',
        function () {
          event_based_compliances_dialog(
            frm,
            event_based_compliances[this.id],
            'Edit Documents - Canopi Event Based Compliances',
          );
        },
      );

      $('.event_based_compliances_chkAll').click(function () {
        if (this.checked) {
          $('.event_based_compliances_chk').prop('checked', true);
        } else {
          $('.event_based_compliances_chk').prop('checked', false);
        }
      });
      event_based_compliances_add(frm);
      event_based_compliances_delete(frm);
      event_based_compliances_upload_events(frm);
      event_based_compliances_download_events(frm);
      event_based_compliances_email(frm);
      event_based_compliances_send_to_legal(frm);
    }
  }
}

function event_based_compliances_add(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-add').on(
    'click',
    function () {
      event_based_compliances_dialog(
        frm,
        {},
        'Add Documents - Canopi Event Based Compliances',
      );
    },
  );
}

function event_based_compliances_delete(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-delete').on(
    'click',
    function () {
      $('input[class="event_based_compliances_chk"]:checked').each(function () {
        event_based_compliances.forEach(x => {
          if (x.name === this.value && x.send_to_legal !== 'No')
            frappe.throw('Please select row of <b>No</b> Send To Legal.');
        });
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.del_doc',
          args: {
            doctype: 'Canopi Event Based Compliances',
            docname: this.value,
          },
          freeze: true,

          callback: async r => {
            delete event_based_compliances[this.value];
            await load_event_based_compliances(frm);
            frm.reload_doc().then(() => {
              setTimeout(() => {
                frm.scroll_to_field('event_based_compliances_html');
              }, 100);
            });
          },
        });
      });
    },
  );
}

function event_based_compliances_download_events(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function event_based_compliances_upload_events(frm) {
  event_based_compliances_attach_file_event(frm);
  event_based_compliances_upload_all_event(frm);
}

const event_based_compliances_files = {};
function event_based_compliances_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-event_based_compliances-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-event_based_compliances-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-event_based_compliances-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#event_based_compliances_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-event_based_compliances-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-event_based_compliances-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          event_based_compliances_files[row_name] = {};
        event_based_compliances_files[row_name].file = file[0];
        event_based_compliances_files[row_name].file_name = file[0].name;
        event_based_compliances_files[row_name].row_name = row_name;
        add_event_based_compliances_preview_event(frm);
        add_event_based_compliances_remove_file_event(frm);
      }
    },
  );
}

function add_event_based_compliances_preview_event(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        event_based_compliances_files[rowname].file,
      );

      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_event_based_compliances_remove_file_event(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete event_based_compliances_files[rowname];
    },
  );
}

function event_based_compliances_upload_all_event(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-upload-all').on(
    'click',
    () => {
      if (Object.keys(event_based_compliances_files).length > 0) {
        preExecutionS3Upload(
          frm,
          event_based_compliances_files,
          'Canopi Event Based Compliances',
          'event_based_compliances_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function event_based_compliances_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          event_based_compliances_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Description',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.save_event_based_compliances',
        args: {
          documentation_name: '',
          post_execution_name: frm.doc.name,
          values,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_event_based_compliances(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('event_based_compliances_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

function event_based_compliances_email(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Event Based Compliances';
      $('input[class="event_based_compliances_chk"]:checked').each(function () {
        const attachment = [];
        attachment.name = this.value;
        attachment.file_name = $(
          '#' + this.value + '_event_based_compliances_file_name',
        ).val();
        attachment.file_url = $(
          '#' + this.value + '_event_based_compliances_file_path',
        ).val();
        s3_attachments[this.value] = {};
        s3_attachments[this.value].file_name = $(
          '#' + this.value + '_event_based_compliances_file_name',
        ).val();
        s3_attachments[this.value].file_url = $(
          '#' + this.value + '_event_based_compliances_file_path',
        ).val();
        render_attachment_rows(attachment);
      });
    },
  );
}

function event_based_compliances_send_to_legal(frm) {
  $('.' + frm.doc.name + '-event_based_compliances-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="event_based_compliances_chk"]:checked').each(function () {
        if (
          $(
            '#' + this.value + 'event_based_compliances_send_to_legal',
          ).text() === 'Yes'
        ) {
          frappe.throw(
            'Some of the selected documents have already been sent to legal',
          );
        }
        if (
          $('#' + this.value + '_event_based_compliances_file_name').val() ===
          ''
        ) {
          frappe.throw('Please Upload File For Selected Rows');
        }
        selected_documents.push({
          name: this.value,
        });
      });
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Event Based Compliances',
        'event_based_compliances_html',
      );
    },
  );
}

function sendToLegalDialog(frm, docs, child_doctype, scroll_to_field) {
  const d = new frappe.ui.Dialog({
    title: 'Select purpose of Sending to Legal',
    fields: [
      {
        label: 'Vetting',
        fieldname: 'vetting',
        fieldtype: 'Check',
        reqd: 0,
        onchange: function (e) {
          const field = d.get_field('noting');
          if (this.value === 1) {
            field.df.read_only = true;
            field.refresh();
          } else {
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Noting',
        fieldname: 'noting',
        fieldtype: 'Check',
        reqd: 0,
        onchange: function (e) {
          const field = d.get_field('vetting');
          if (this.value === 1) {
            field.df.read_only = true;
            field.refresh();
          } else {
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      if (values.vetting === 0 && values.noting === 0) {
        frappe.throw('Please Select One Option');
      }
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_post_execution_checklist.canopi_post_execution_checklist.send_to_legal',
        args: {
          docname: frm.doc.name,
          child_doctype,
          docs,
          vetting: values.vetting,
          noting: values.noting,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field(scroll_to_field);
            }, 100);
          });
        },
      });
      d.hide();
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
}
function add_template_button_events(frm) {
  if (frm.doc.__islocal !== 1) {
    add_edit_events(frm);
    add_exception_tracking_edit_events(frm);
    add_new_row_events(frm);
    add_remove_row_event(frm);
    add_select_events(frm);
    add_upload_events(frm);
    add_exception_tracking_upload_events(frm);
    add_exception_tracking_download_events(frm);
    add_download_events(frm);
    download_excel(frm);
  }
}

function add_new_row_events(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    $('.' + frm.doc.name + '-immovable-add-row').unbind();
    $('.' + frm.doc.name + '-immovable-add-row').on('click', function () {
      const asset_name = 'immovable_asset';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-movable-add-row').unbind();
    $('.' + frm.doc.name + '-movable-add-row').on('click', function () {
      const asset_name = 'movable_asset';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-pledge-add-row').unbind();
    $('.' + frm.doc.name + '-pledge-add-row').on('click', function () {
      const asset_name = 'pledge';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-personal-guarantee-add-row').unbind();
    $('.' + frm.doc.name + '-personal-guarantee-add-row').on(
      'click',
      function () {
        const asset_name = 'personal_guarantee';
        type_of_security_row_dialog(frm, {}, asset_name);
      },
    );

    $('.' + frm.doc.name + '-corporate-guarantee-add-row').unbind();
    $('.' + frm.doc.name + '-corporate-guarantee-add-row').on(
      'click',
      function () {
        const asset_name = 'corporate_guarantee';
        type_of_security_row_dialog(frm, {}, asset_name);
      },
    );

    $('.' + frm.doc.name + '-ndu-add-row').unbind();
    $('.' + frm.doc.name + '-ndu-add-row').on('click', function () {
      const asset_name = 'ndu';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-intangible-add-row').unbind();
    $('.' + frm.doc.name + '-intangible-add-row').on('click', function () {
      const asset_name = 'intangible_asset';
      type_of_security_row_dialog(frm, {}, asset_name);
    });
    $('.' + frm.doc.name + '-motor-add-row').unbind();
    $('.' + frm.doc.name + '-motor-add-row').on('click', function () {
      const asset_name = 'motor_vehicle';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-financial-add-row').unbind();
    $('.' + frm.doc.name + '-financial-add-row').on('click', function () {
      const asset_name = 'financial_asset';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-assignment-of-rights-add-row').unbind();
    $('.' + frm.doc.name + '-assignment-of-rights-add-row').on(
      'click',
      function () {
        const asset_name = 'assignment_of_rights';
        type_of_security_row_dialog(frm, {}, asset_name);
      },
    );

    $('.' + frm.doc.name + '-current-add-row').unbind();
    $('.' + frm.doc.name + '-current-add-row').on('click', function () {
      const asset_name = 'current_asset';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-dsra-add-row').unbind();
    $('.' + frm.doc.name + '-dsra-add-row').on('click', function () {
      const asset_name = 'debt_service_reserve_account';
      type_of_security_row_dialog(frm, {}, asset_name);
    });

    $('.' + frm.doc.name + '-others-add-row').unbind();
    $('.' + frm.doc.name + '-others-add-row').on('click', function () {
      const asset_name = 'others';
      type_of_security_row_dialog(frm, {}, asset_name);
    });
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    $('.' + frm.doc.name + '-post-execution-details-add-row').unbind();
    $('.' + frm.doc.name + '-post-execution-details-add-row').on(
      'click',
      function () {
        const asset_name = 'post_execution_details';
        type_of_security_row_dialog(frm, {}, asset_name);
      },
    );
  }
}

function add_select_events(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    $('.' + frm.doc.name + '-immovable-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-immovable-select-row').each(
          (index, element) => {
            if ($(element).is(':hidden') === false) {
              $(element).prop('checked', true);
            }
          },
        );
      } else {
        $('.' + frm.doc.name + '-immovable-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-movable-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-movable-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-movable-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-pledge-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-pledge-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-pledge-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-personal-guarantee-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-personal-guarantee-select-row').each(
          (index, element) => {
            if ($(element).is(':hidden') === false) {
              $(element).prop('checked', true);
            }
          },
        );
      } else {
        $('.' + frm.doc.name + '-personal-guarantee-select-row').prop(
          'checked',
          false,
        );
      }
    });

    $('.' + frm.doc.name + '-corporate-guarantee-select-all').click(
      function () {
        if (this.checked) {
          $('.' + frm.doc.name + '-corporate-guarantee-select-row').each(
            (index, element) => {
              if ($(element).is(':hidden') === false) {
                $(element).prop('checked', true);
              }
            },
          );
        } else {
          $('.' + frm.doc.name + '-corporate-guarantee-select-row').prop(
            'checked',
            false,
          );
        }
      },
    );

    $('.' + frm.doc.name + '-ndu-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-ndu-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-ndu-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-intangible-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-intangible-select-row').each(
          (index, element) => {
            if ($(element).is(':hidden') === false) {
              $(element).prop('checked', true);
            }
          },
        );
      } else {
        $('.' + frm.doc.name + '-intangible-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-motor-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-motor-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-motor-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-financial-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-financial-select-row').each(
          (index, element) => {
            if ($(element).is(':hidden') === false) {
              $(element).prop('checked', true);
            }
          },
        );
      } else {
        $('.' + frm.doc.name + '-financial-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-assignment-of-rights-select-all').click(
      function () {
        if (this.checked) {
          $('.' + frm.doc.name + '-assignment-of-rights-select-row').each(
            (index, element) => {
              if ($(element).is(':hidden') === false) {
                $(element).prop('checked', true);
              }
            },
          );
        } else {
          $('.' + frm.doc.name + '-assignment-of-rights-select-row').prop(
            'checked',
            false,
          );
        }
      },
    );

    $('.' + frm.doc.name + '-current-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-current-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-current-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-dsra-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-dsra-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-dsra-select-row').prop('checked', false);
      }
    });

    $('.' + frm.doc.name + '-others-select-all').click(function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-others-select-row').each((index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        });
      } else {
        $('.' + frm.doc.name + '-others-select-row').prop('checked', false);
      }
    });
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    $('.' + frm.doc.name + '-post-execution-details-select-all').click(
      function () {
        if (this.checked) {
          $('.' + frm.doc.name + '-post-execution-details-select-row').each(
            (index, element) => {
              if ($(element).is(':hidden') === false) {
                $(element).prop('checked', true);
              }
            },
          );
        } else {
          $('.' + frm.doc.name + '-post-execution-details-select-row').prop(
            'checked',
            false,
          );
        }
      },
    );
  }
}

function delete_multiple_from_s3(keys) {
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_multiple_from_s3',
    args: { keys },
  });
}

function add_remove_row_event(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    add_immovable_remove_row_event(frm);
    add_movable_remove_row_event(frm);
    add_pledge_remove_row_event(frm);
    add_personal_guarantee_remove_row_event(frm);
    add_corporate_guarantee_remove_row_event(frm);
    add_ndu_remove_row_event(frm);
    add_intangible_remove_row_event(frm);
    add_motor_remove_row_event(frm);
    add_financial_remove_row_event(frm);
    add_assignment_of_rights_remove_row_event(frm);
    add_current_remove_row_event(frm);
    add_dsra_remove_row_event(frm);
    add_others_remove_row_event(frm);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    add_ea_table_remove_row_event(frm);
  }
}
function delete_keys(frm, details, remove_assets, asset_name) {
  const keys = [];
  remove_assets.forEach(asset => {
    if (asset_name === 'immovable_asset') {
      details.immovable_asset = details.immovable_asset.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'movable_asset') {
      details.movable_asset = details.movable_asset.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'pledge') {
      details.pledge = details.pledge.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'personal_guarantee') {
      details.personal_guarantee = details.personal_guarantee.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'corporate_guarantee') {
      details.corporate_guarantee = details.corporate_guarantee.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'ndu') {
      details.ndu = details.ndu.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'intangible_asset') {
      details.intangible_asset = details.intangible_asset.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'motor_vehicle') {
      details.motor_vehicle = details.motor_vehicle.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'financial_asset') {
      details.financial_asset = details.financial_asset.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'assignment_of_rights') {
      details.assignment_of_rights = details.assignment_of_rights.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'current_asset') {
      details.current_asset = details.current_asset.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'debt_service_reserve_account') {
      details.debt_service_reserve_account =
        details.debt_service_reserve_account.filter(
          el => el.document_code !== asset.document_code,
        );
    } else if (asset_name === 'others') {
      details.others = details.others.filter(
        el => el.document_code !== asset.document_code,
      );
    } else if (asset_name === 'post_execution_details') {
      details.post_execution_details = details.post_execution_details.filter(
        el => el.document_code !== asset.document_code,
      );
    }
    if (asset?.s3_key) {
      asset?.s3_key.forEach(key => {
        keys.push(key);
      });
    }
  });
  frm.dirty();
  if (keys.length > 0) {
    delete_multiple_from_s3(keys).then(r => {
      if (asset_name === 'post_execution_details') {
        frm.doc.post_execution_details = JSON.stringify(details);
      } else {
        frm.doc.type_of_security_details = JSON.stringify(details);
      }
      frm.save().then(() => {
        setTimeout(() => {
          scroll_to_field(frm, asset_name);
        }, 100);
      });
    });
  } else {
    if (asset_name === 'post_execution_details') {
      frm.doc.post_execution_details = JSON.stringify(details);
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
    }
    frm.save().then(() => {
      setTimeout(() => {
        scroll_to_field(frm, asset_name);
      }, 100);
    });
  }
}
function add_immovable_remove_row_event(frm) {
  $('.' + frm.doc.name + '-immovable-remove-row').unbind();
  $('.' + frm.doc.name + '-immovable-remove-row').on('click', function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-immovable-select-row"]:checked`).length <=
      0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-immovable-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.immovable_asset[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'immovable_asset');
  });
}

function add_movable_remove_row_event(frm) {
  $('.' + frm.doc.name + '-movable-remove-row').unbind();
  $('.' + frm.doc.name + '-movable-remove-row').on('click', async function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-movable-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-movable-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.movable_asset[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'movable_asset');
  });
}

function add_pledge_remove_row_event(frm) {
  $('.' + frm.doc.name + '-pledge-remove-row').unbind();
  $('.' + frm.doc.name + '-pledge-remove-row').on('click', function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-pledge-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-pledge-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.pledge[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'pledge');
  });
}

function add_personal_guarantee_remove_row_event(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-remove-row').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-remove-row').on(
    'click',
    function () {
      const remove_assets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-personal-guarantee-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-personal-guarantee-select-row`).each(function (i) {
        if (this.checked) {
          remove_assets.push(details.personal_guarantee[i]);
        }
      });
      delete_keys(frm, details, remove_assets, 'personal_guarantee');
    },
  );
}

function add_corporate_guarantee_remove_row_event(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-remove-row').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-remove-row').on(
    'click',
    function () {
      const remove_assets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-corporate-guarantee-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-corporate-guarantee-select-row`).each(function (i) {
        if (this.checked) {
          remove_assets.push(details.corporate_guarantee[i]);
        }
      });
      delete_keys(frm, details, remove_assets, 'corporate_guarantee');
    },
  );
}

function add_ndu_remove_row_event(frm) {
  $('.' + frm.doc.name + '-ndu-remove-row').unbind();
  $('.' + frm.doc.name + '-ndu-remove-row').on('click', async function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-ndu-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-ndu-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.ndu[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'ndu');
  });
}

function add_intangible_remove_row_event(frm) {
  $('.' + frm.doc.name + '-intangible-remove-row').unbind();
  $('.' + frm.doc.name + '-intangible-remove-row').on(
    'click',
    async function () {
      const remove_assets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(`input[class="${frm.doc.name}-intangible-select-row"]:checked`)
          .length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-intangible-select-row`).each(function (i) {
        if (this.checked) {
          remove_assets.push(details.intangible_asset[i]);
        }
      });
      delete_keys(frm, details, remove_assets, 'intangible_asset');
    },
  );
}

function add_motor_remove_row_event(frm) {
  $('.' + frm.doc.name + '-motor-remove-row').unbind();
  $('.' + frm.doc.name + '-motor-remove-row').on('click', function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-motor-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-motor-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.motor_vehicle[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'motor_vehicle');
  });
}

function add_financial_remove_row_event(frm) {
  $('.' + frm.doc.name + '-financial-remove-row').unbind();
  $('.' + frm.doc.name + '-financial-remove-row').on('click', function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-financial-select-row"]:checked`).length <=
      0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-financial-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.financial_asset[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'financial_asset');
  });
}

function add_assignment_of_rights_remove_row_event(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-remove-row').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-remove-row').on(
    'click',
    function () {
      const remove_assets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-assignment-of-rights-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-assignment-of-rights-select-row`).each(function (i) {
        if (this.checked) {
          remove_assets.push(details.assignment_of_rights[i]);
        }
      });
      delete_keys(frm, details, remove_assets, 'assignment_of_rights');
    },
  );
}

function add_current_remove_row_event(frm) {
  $('.' + frm.doc.name + '-current-remove-row').unbind();
  $('.' + frm.doc.name + '-current-remove-row').on('click', function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-current-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-current-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.current_asset[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'current_asset');
  });
}

function add_dsra_remove_row_event(frm) {
  $('.' + frm.doc.name + '-dsra-remove-row').unbind();
  $('.' + frm.doc.name + '-dsra-remove-row').on('click', function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-dsra-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-dsra-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.debt_service_reserve_account[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'debt_service_reserve_account');
  });
}

function add_others_remove_row_event(frm) {
  $('.' + frm.doc.name + '-others-remove-row').unbind();
  $('.' + frm.doc.name + '-others-remove-row').on('click', async function () {
    const remove_assets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-others-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-others-select-row`).each(function (i) {
      if (this.checked) {
        remove_assets.push(details.others[i]);
      }
    });
    delete_keys(frm, details, remove_assets, 'others');
  });
}

function add_ea_table_remove_row_event(frm) {
  $('.' + frm.doc.name + '-post-execution-details-remove-row').unbind();
  $('.' + frm.doc.name + '-post-execution-details-remove-row').on(
    'click',
    function () {
      const remove_assets = [];
      frm.doc.post_execution_details = frm.doc.post_execution_details
        ? frm.doc.post_execution_details
        : JSON.stringify({});
      const details =
        frm.doc.post_execution_details !== undefined
          ? JSON.parse(frm.doc.post_execution_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-post-execution-details-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-post-execution-details-select-row`).each(
        function (i) {
          if (this.checked) {
            remove_assets.push(details.post_execution_details[i]);
          }
        },
      );
      delete_keys(frm, details, remove_assets, 'post_execution_details');
    },
  );
}

function download_excel(frm) {
  $('#' + frm.doc.name + '-immovable-download').on('click', function () {
    load_csv(get_asset_values(frm).immovable_asset);
  });

  $('#' + frm.doc.name + '-movable-download').on('click', function () {
    load_csv(get_asset_values(frm).movable_asset);
  });

  $('#' + frm.doc.name + '-pledge-download').on('click', function () {
    load_csv(get_asset_values(frm).pledge);
  });

  $('#' + frm.doc.name + '-personal-guarantee-download').on(
    'click',
    function () {
      load_csv(get_asset_values(frm).personal_guarantee);
    },
  );

  $('#' + frm.doc.name + '-corporate-guarantee-download').on(
    'click',
    function () {
      load_csv(get_asset_values(frm).corporate_guarantee);
    },
  );

  $('#' + frm.doc.name + '-ndu-download').on('click', function () {
    load_csv(get_asset_values(frm).ndu);
  });

  $('#' + frm.doc.name + '-intangible-download').on('click', function () {
    load_csv(get_asset_values(frm).intangible_asset);
  });
  $('#' + frm.doc.name + '-motor-download').on('click', function () {
    load_csv(get_asset_values(frm).motor_vehicle);
  });
  $('#' + frm.doc.name + '-financial-download').on('click', function () {
    load_csv(get_asset_values(frm).financial_asset);
  });
  $('#' + frm.doc.name + '-assignment-of-rights-download').on(
    'click',
    function () {
      load_csv(get_asset_values(frm).assignment_of_rights);
    },
  );
  $('#' + frm.doc.name + '-current-download').on('click', function () {
    load_csv(get_asset_values(frm).current_asset);
  });
  $('#' + frm.doc.name + '-dsra-download').on('click', function () {
    load_csv(get_asset_values(frm).debt_service_reserve_account);
  });
  $('#' + frm.doc.name + '-others-download').on('click', function () {
    load_csv(get_asset_values(frm).others);
  });
  $('#' + frm.doc.name + '-post-execution-details-download').on(
    'click',
    function () {
      load_csv(get_asset_values(frm).post_execution_details);
    },
  );
}

function load_csv(data) {
  const items = [];
  data.forEach(function (s, i) {
    let description = s.description ? s.description : ' ';
    description = description.replace(/\s+/g, ' ');
    let comments = s.comments ? s.comments : ' ';
    comments = comments.replace(/\s+/g, ' ');
    const file_name = s.file_name ? s.file_name : ' ';
    const upload_date = s.upload_date ? s.upload_date : ' ';

    if (i === 0) {
      items.push({
        sr_no: 'Sr No',
        description: 'Description',
        file_name: 'File Name',
        upload_date: 'Upload Date',
        comments: 'Comments',
      });
    }
    items.push({
      sr_no: i + 1,
      description,
      file_name: file_name.toString(),
      upload_date,
      comments,
    });
  });

  let CSV = ConvertToCSV(items);
  CSV = 'data:application/csv,' + encodeURIComponent(CSV);
  const x = document.createElement('A');
  x.setAttribute('href', CSV);
  x.setAttribute('download', 'post_excel_checklist.csv');
  document.body.appendChild(x);
  x.click();
}

function ConvertToCSV(objArray) {
  const items = objArray; // your json data
  const replacer = (key, value) => (value === null ? 'unknownData' : value); // handle null data or actual value
  const header = Object.keys(items[0]); // get your headers  -> Sr No,Description,File Name
  const csv = [
    ...items.map(row =>
      header
        .map(fieldName => JSON.stringify(row[fieldName], replacer))
        .join(','),
    ),
  ].join('\r\n');

  return csv;
}

function add_edit_events(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    $('.' + frm.doc.name + '-immovable-edit-row').unbind();
    $('.' + frm.doc.name + '-immovable-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-immovable-edit-row').index(this);
      const asset_name = 'immovable_asset';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).immovable_asset[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-movable-edit-row').unbind();
    $('.' + frm.doc.name + '-movable-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-movable-edit-row').index(this);
      const asset_name = 'movable_asset';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).movable_asset[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-pledge-edit-row').unbind();
    $('.' + frm.doc.name + '-pledge-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-pledge-edit-row').index(this);
      const asset_name = 'pledge';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).pledge[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-personal-guarantee-edit-row').unbind();
    $('.' + frm.doc.name + '-personal-guarantee-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-personal-guarantee-edit-row',
        ).index(this);
        const asset_name = 'personal_guarantee';
        type_of_security_row_dialog(
          frm,
          get_asset_values(frm).personal_guarantee[index],
          asset_name,
          index,
        );
      },
    );

    $('.' + frm.doc.name + '-corporate-guarantee-edit-row').unbind();
    $('.' + frm.doc.name + '-corporate-guarantee-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-corporate-guarantee-edit-row',
        ).index(this);
        const asset_name = 'corporate_guarantee';
        type_of_security_row_dialog(
          frm,
          get_asset_values(frm).corporate_guarantee[index],
          asset_name,
          index,
        );
      },
    );

    $('.' + frm.doc.name + '-ndu-edit-row').unbind();
    $('.' + frm.doc.name + '-ndu-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-ndu-edit-row').index(this);
      const asset_name = 'ndu';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).ndu[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-intangible-edit-row').unbind();
    $('.' + frm.doc.name + '-intangible-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-intangible-edit-row').index(this);
      const asset_name = 'intangible_asset';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).intangible_asset[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-motor-edit-row').unbind();
    $('.' + frm.doc.name + '-motor-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-motor-edit-row').index(this);
      const asset_name = 'motor_vehicle';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).motor_vehicle[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-financial-edit-row').unbind();
    $('.' + frm.doc.name + '-financial-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-financial-edit-row').index(this);
      const asset_name = 'financial_asset';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).financial_asset[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-assignment-of-rights-edit-row').unbind();
    $('.' + frm.doc.name + '-assignment-of-rights-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-assignment-of-rights-edit-row',
        ).index(this);
        const asset_name = 'assignment_of_rights';
        type_of_security_row_dialog(
          frm,
          get_asset_values(frm).assignment_of_rights[index],
          asset_name,
          index,
        );
      },
    );

    $('.' + frm.doc.name + '-current-edit-row').unbind();
    $('.' + frm.doc.name + '-current-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-current-edit-row').index(this);
      const asset_name = 'current_asset';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).current_asset[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-dsra-edit-row').unbind();
    $('.' + frm.doc.name + '-dsra-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-dsra-edit-row').index(this);
      const asset_name = 'debt_service_reserve_account';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).debt_service_reserve_account[index],
        asset_name,
        index,
      );
    });

    $('.' + frm.doc.name + '-others-edit-row').unbind();
    $('.' + frm.doc.name + '-others-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-others-edit-row').index(this);
      const asset_name = 'others';
      type_of_security_row_dialog(
        frm,
        get_asset_values(frm).others[index],
        asset_name,
        index,
      );
    });
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    $('.' + frm.doc.name + '-post-execution-details-edit-row').unbind();
    $('.' + frm.doc.name + '-post-execution-details-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-post-execution-details-edit-row',
        ).index(this);
        const asset_name = 'post_execution_details';
        type_of_security_row_dialog(
          frm,
          get_asset_values(frm).post_execution_details[index],
          asset_name,
          index,
        );
      },
    );
  }
}

function add_exception_tracking_edit_events(frm) {
  $('.' + frm.doc.name + '-immovable-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-immovable-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-immovable-exception-edit-row',
      ).index(this);
      const asset_name = 'immovable_asset';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).immovable_asset[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-movable-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-movable-exception-edit-row').on(
    'click',
    function () {
      const index = $('.' + frm.doc.name + '-movable-exception-edit-row').index(
        this,
      );
      const asset_name = 'movable_asset';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).movable_asset[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-pledge-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-pledge-exception-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-pledge-exception-edit-row').index(
      this,
    );
    const asset_name = 'pledge';
    exception_tracking_row_dialog(
      frm,
      get_exception_tracking_values(frm).pledge[index],
      asset_name,
      index,
    );
  });

  $('.' + frm.doc.name + '-personal-guarantee-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-exception-edit-row',
      ).index(this);
      const asset_name = 'personal_guarantee';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).personal_guarantee[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-corporate-guarantee-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-exception-edit-row',
      ).index(this);
      const asset_name = 'corporate_guarantee';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).corporate_guarantee[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-ndu-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-ndu-exception-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-ndu-exception-edit-row').index(this);
    const asset_name = 'ndu';
    exception_tracking_row_dialog(
      frm,
      get_exception_tracking_values(frm).ndu[index],
      asset_name,
      index,
    );
  });

  $('.' + frm.doc.name + '-intangible-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-intangible-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-intangible-exception-edit-row',
      ).index(this);
      const asset_name = 'intangible_asset';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).intangible_asset[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-motor-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-motor-exception-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-motor-exception-edit-row').index(
      this,
    );
    const asset_name = 'motor_vehicle';
    exception_tracking_row_dialog(
      frm,
      get_exception_tracking_values(frm).motor_vehicle[index],
      asset_name,
      index,
    );
  });

  $('.' + frm.doc.name + '-financial-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-financial-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-financial-exception-edit-row',
      ).index(this);
      const asset_name = 'financial_asset';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).financial_asset[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-assignment-of-rights-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-exception-edit-row',
      ).index(this);
      const asset_name = 'assignment_of_rights';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).assignment_of_rights[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-current-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-current-exception-edit-row').on(
    'click',
    function () {
      const index = $('.' + frm.doc.name + '-current-exception-edit-row').index(
        this,
      );
      const asset_name = 'current_asset';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).current_asset[index],
        asset_name,
        index,
      );
    },
  );

  $('.' + frm.doc.name + '-dsra-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-dsra-exception-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-dsra-exception-edit-row').index(
      this,
    );
    const asset_name = 'debt_service_reserve_account';
    exception_tracking_row_dialog(
      frm,
      get_exception_tracking_values(frm).debt_service_reserve_account[index],
      asset_name,
      index,
    );
  });

  $('.' + frm.doc.name + '-others-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-others-exception-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-others-exception-edit-row').index(
      this,
    );
    const asset_name = 'others';
    exception_tracking_row_dialog(
      frm,
      get_exception_tracking_values(frm).others[index],
      asset_name,
      index,
    );
  });

  $('.' + frm.doc.name + '-pre-execution-details-exception-edit-row').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-exception-edit-row').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-exception-edit-row',
      ).index(this);
      const asset_name = 'pre_execution_details';
      exception_tracking_row_dialog(
        frm,
        get_exception_tracking_values(frm).pre_execution_details[index],
        asset_name,
        index,
      );
    },
  );
}

function add_upload_events(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    // immovable asset events
    if (get_asset_values(frm).immovable_asset) {
      add_immovable_delete_attachment_event(frm);
      add_immovable_remove_file_event(frm);
      add_immovable_attach_file_event(frm);
      add_immovable_upload_all_event(frm);
    }

    // movable asset events
    if (get_asset_values(frm).movable_asset) {
      add_movable_delete_attachment_event(frm);
      add_movable_remove_file_event(frm);
      add_movable_attach_file_event(frm);
      add_movable_upload_all_event(frm);
    }

    // pledge asset events
    if (get_asset_values(frm).pledge) {
      add_pledge_delete_attachment_event(frm);
      add_pledge_remove_file_event(frm);
      add_pledge_attach_file_event(frm);
      add_pledge_upload_all_event(frm);
    }

    // personal guarantee events
    if (get_asset_values(frm).personal_guarantee) {
      add_personal_guarantee_delete_attachment_event(frm);
      add_personal_guarantee_remove_file_event(frm);
      add_personal_guarantee_attach_file_event(frm);
      add_personal_guarantee_upload_all_event(frm);
    }

    // corporate guarantee events
    if (get_asset_values(frm).corporate_guarantee) {
      add_corporate_guarantee_delete_attachment_event(frm);
      add_corporate_guarantee_remove_file_event(frm);
      add_corporate_guarantee_attach_file_event(frm);
      add_corporate_guarantee_upload_all_event(frm);
    }

    // ndu events
    if (get_asset_values(frm).ndu) {
      add_ndu_delete_attachment_event(frm);
      add_ndu_remove_file_event(frm);
      add_ndu_attach_file_event(frm);
      add_ndu_upload_all_event(frm);
    }

    // intangible events
    if (get_asset_values(frm).intangible_asset) {
      add_intangible_delete_attachment_event(frm);
      add_intangible_remove_file_event(frm);
      add_intangible_attach_file_event(frm);
      add_intangible_upload_all_event(frm);
    }

    // motor events
    if (get_asset_values(frm).motor_vehicle) {
      add_motor_delete_attachment_event(frm);
      add_motor_remove_file_event(frm);
      add_motor_attach_file_event(frm);
      add_motor_upload_all_event(frm);
    }

    // financial events
    if (get_asset_values(frm).financial_asset) {
      add_financial_delete_attachment_event(frm);
      add_financial_remove_file_event(frm);
      add_financial_attach_file_event(frm);
      add_financial_upload_all_event(frm);
    }

    // assignment of rights events
    if (get_asset_values(frm).assignment_of_rights) {
      add_assignment_of_rights_delete_attachment_event(frm);
      add_assignment_of_rights_remove_file_event(frm);
      add_assignment_of_rights_attach_file_event(frm);
      add_assignment_of_rights_upload_all_event(frm);
    }

    // current events
    if (get_asset_values(frm).current_asset) {
      add_current_delete_attachment_event(frm);
      add_current_remove_file_event(frm);
      add_current_attach_file_event(frm);
      add_current_upload_all_event(frm);
    }

    // dsra events
    if (get_asset_values(frm).debt_service_reserve_account) {
      add_dsra_delete_attachment_event(frm);
      add_dsra_remove_file_event(frm);
      add_dsra_attach_file_event(frm);
      add_dsra_upload_all_event(frm);
    }

    // others events
    if (get_asset_values(frm).others) {
      add_others_delete_attachment_event(frm);
      add_others_remove_file_event(frm);
      add_others_attach_file_event(frm);
      add_others_upload_all_event(frm);
    }
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    // post execution details table events
    add_ea_delete_attachment_event(frm);
    add_ea_remove_file_event(frm);
    add_ea_attach_file_event(frm);
    add_ea_upload_all_event(frm);
  }
}
function delete_from_s3(frm, details, key, asset_name) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    frm.doc.type_of_security_details = JSON.stringify(details);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    frm.doc.post_execution_details = JSON.stringify(details);
  }
  frm.dirty();
  frm.save().then(() => {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.delete_from_s3',
      freeze: true,
      args: { key },
      callback: r => {
        frm.reload_doc().then(() => {
          load_asset_tables(frm);
          setTimeout(() => {
            scroll_to_field(frm, asset_name);
          }, 100);
        });
      },
    });
  });
}
function add_immovable_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).immovable_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-immovable-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-immovable-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-immovable-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.immovable_asset[index].s3_key[s3_index]
            ? details.immovable_asset[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.immovable_asset[index].file_name.splice(s3_index, 1);
          details.immovable_asset[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'immovable_asset');
        }, 100);
      },
    );
  }
}

function add_movable_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).movable_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-movable-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-movable-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-movable-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.movable_asset[index].s3_key[s3_index]
            ? details.movable_asset[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.movable_asset[index].file_name.splice(s3_index, 1);
          details.movable_asset[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'movable_asset');
        }, 100);
      },
    );
  }
}

function add_pledge_delete_attachment_event(frm) {
  for (let index = 0; index < get_asset_values(frm).pledge.length; index++) {
    $('.' + frm.doc.name + '-pledge-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-pledge-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-pledge-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.pledge[index].s3_key[s3_index]
            ? details.pledge[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.pledge[index].file_name.splice(s3_index, 1);
          details.pledge[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'pledge');
        }, 100);
      },
    );
  }
}

function add_personal_guarantee_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).personal_guarantee.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-personal-guarantee-' + index + 'delete-file',
    ).unbind();
    $('.' + frm.doc.name + '-personal-guarantee-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-personal-guarantee-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.personal_guarantee[index].s3_key[s3_index]
            ? details.personal_guarantee[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.personal_guarantee[index].file_name.splice(s3_index, 1);
          details.personal_guarantee[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'personal_guarantee');
        }, 100);
      },
    );
  }
}

function add_corporate_guarantee_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).corporate_guarantee.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-corporate-guarantee-' + index + 'delete-file',
    ).unbind();
    $('.' + frm.doc.name + '-corporate-guarantee-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-corporate-guarantee-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.corporate_guarantee[index].s3_key[s3_index]
            ? details.corporate_guarantee[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.corporate_guarantee[index].file_name.splice(s3_index, 1);
          details.corporate_guarantee[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'corporate_guarantee');
        }, 100);
      },
    );
  }
}

function add_ndu_delete_attachment_event(frm) {
  for (let index = 0; index < get_asset_values(frm).ndu.length; index++) {
    $('.' + frm.doc.name + '-ndu-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-ndu-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-ndu-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.ndu[index].s3_key[s3_index]
            ? details.ndu[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.ndu[index].file_name.splice(s3_index, 1);
          details.ndu[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'ndu');
        }, 100);
      },
    );
  }
}

function add_intangible_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).intangible_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-intangible-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-intangible-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-intangible-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.intangible_asset[index].s3_key[s3_index]
            ? details.intangible_asset[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.intangible_asset[index].file_name.splice(s3_index, 1);
          details.intangible_asset[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'intangible_asset');
        }, 100);
      },
    );
  }
}

function add_motor_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).motor_vehicle.length;
    index++
  ) {
    $('.' + frm.doc.name + '-motor-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-motor-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-motor-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.motor_vehicle[index].s3_key[s3_index]
            ? details.motor_vehicle[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.motor_vehicle[index].file_name.splice(s3_index, 1);
          details.motor_vehicle[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'motor_vehicle');
        }, 100);
      },
    );
  }
}

function add_financial_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).financial_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-financial-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-financial-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-financial-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.financial_asset[index].s3_key[s3_index]
            ? details.financial_asset[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.financial_asset[index].file_name.splice(s3_index, 1);
          details.financial_asset[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'financial_asset');
        }, 100);
      },
    );
  }
}

function add_assignment_of_rights_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).assignment_of_rights.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-assignment-of-rights-' + index + 'delete-file',
    ).unbind();
    $('.' + frm.doc.name + '-assignment-of-rights-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-assignment-of-rights-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.assignment_of_rights[index].s3_key[s3_index]
            ? details.assignment_of_rights[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.assignment_of_rights[index].file_name.splice(s3_index, 1);
          details.assignment_of_rights[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'assignment_of_rights');
        }, 100);
      },
    );
  }
}

function add_current_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).current_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-current-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-current-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-current-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.current_asset[index].s3_key[s3_index]
            ? details.current_asset[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.current_asset[index].file_name.splice(s3_index, 1);
          details.current_asset[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'current_asset');
        }, 100);
      },
    );
  }
}

function add_dsra_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).debt_service_reserve_account.length;
    index++
  ) {
    $('.' + frm.doc.name + '-dsra-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-dsra-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-dsra-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.debt_service_reserve_account[index].s3_key[
            s3_index
          ]
            ? details.debt_service_reserve_account[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.debt_service_reserve_account[index].file_name.splice(
            s3_index,
            1,
          );
          details.debt_service_reserve_account[index].s3_key.splice(
            s3_index,
            1,
          );
          delete_from_s3(frm, details, key, 'debt_service_reserve_account');
        }, 100);
      },
    );
  }
}

function add_others_delete_attachment_event(frm) {
  for (let index = 0; index < get_asset_values(frm).others.length; index++) {
    $('.' + frm.doc.name + '-others-' + index + 'delete-file').unbind();
    $('.' + frm.doc.name + '-others-' + index + 'delete-file').on(
      'click',
      function () {
        document.activeElement.blur();
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-others-' + index + 'delete-file',
        ).index(this);
        setTimeout(() => {
          const key = details.others[index].s3_key[s3_index]
            ? details.others[index].s3_key[s3_index]
            : frappe.throw('S3 key not found.');

          details.others[index].file_name.splice(s3_index, 1);
          details.others[index].s3_key.splice(s3_index, 1);
          delete_from_s3(frm, details, key, 'others');
        }, 100);
      },
    );
  }
}

function add_ea_delete_attachment_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).post_execution_details.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-post-execution-details-' + index + 'delete-file',
    ).unbind();
    $(
      '.' + frm.doc.name + '-post-execution-details-' + index + 'delete-file',
    ).on('click', function () {
      document.activeElement.blur();
      const details =
        frm.doc.post_execution_details !== undefined
          ? JSON.parse(frm.doc.post_execution_details)
          : {};
      const s3_index = $(
        '.' + frm.doc.name + '-post-execution-details-' + index + 'delete-file',
      ).index(this);
      setTimeout(() => {
        const key = details.post_execution_details[index].s3_key[s3_index]
          ? details.post_execution_details[index].s3_key[s3_index]
          : frappe.throw('S3 key not found.');

        details.post_execution_details[index].file_name.splice(s3_index, 1);
        details.post_execution_details[index].s3_key.splice(s3_index, 1);
        delete_from_s3(frm, details, key, 'post_execution_details');
      }, 100);
    });
  }
}

function add_immovable_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).immovable_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-immovable-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-immovable-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-immovable-' + index + 'remove-file',
        ).index(this);

        details.immovable_asset[index].file_name.splice(s3_index, 1);
        details.immovable_asset[index].file_url.splice(s3_index, 1);
        fileRemove('immovable_asset', index, s3_index);
        const s3_key = [];
        if (
          details.immovable_asset[index].file_name.length === 0 &&
          details.immovable_asset[index].s3_key
        ) {
          details.immovable_asset[index].file_name = '';
          details.immovable_asset[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.immovable_asset[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.immovable_asset[index].s3_key[
              s3_key_index
            ]
              ? details.immovable_asset[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.immovable_asset[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'immovable_asset');
      },
    );
  }
}

function add_immovable_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-immovable-attach-file').unbind();
  $('.' + frm.doc.name + '-immovable-attach-input').unbind();

  $('.' + frm.doc.name + '-immovable-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-immovable-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-immovable-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-immovable-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];

    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('immovable_asset', files, details, s3_button_index);
    details.immovable_asset[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.immovable_asset[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'immovable_asset');
  });
}

function add_immovable_upload_all_event(frm) {
  $('.' + frm.doc.name + '-immovable-upload-all').unbind();
  $('.' + frm.doc.name + '-immovable-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-immovable-upload-all').index(this);
    if (get_asset_values(frm).immovable_asset.some(x => x.file_url)) {
      const immovable_asset = JSON.stringify(
        get_asset_values(frm).immovable_asset,
      );
      post_execution_s3_upload(
        frm,
        immovable_asset,
        'immovable_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_movable_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).movable_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-movable-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-movable-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-movable-' + index + 'remove-file',
        ).index(this);

        details.movable_asset[index].file_name.splice([s3_index]);
        details.movable_asset[index].file_url.splice([s3_index]);
        fileRemove('movable_asset', index, s3_index);
        const s3_key = [];
        if (
          details.movable_asset[index].file_name.length === 0 &&
          details.movable_asset[index].s3_key
        ) {
          details.movable_asset[index].file_name = '';
          details.movable_asset[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.movable_asset[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.movable_asset[index].s3_key[s3_key_index]
              ? details.movable_asset[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.movable_asset[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'movable_asset');
      },
    );
  }
}

function add_movable_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-movable-attach-file').unbind();
  $('.' + frm.doc.name + '-movable-attach-input').unbind();

  $('.' + frm.doc.name + '-movable-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-movable-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-movable-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-movable-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];

    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('movable_asset', files, details, s3_button_index);
    details.movable_asset[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.movable_asset[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'movable_asset');
  });
}

function add_movable_upload_all_event(frm) {
  $('.' + frm.doc.name + '-movable-upload-all').unbind();
  $('.' + frm.doc.name + '-movable-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-movable-upload-all').index(this);
    if (get_asset_values(frm).movable_asset.some(x => x.file_url)) {
      const movable_asset = JSON.stringify(get_asset_values(frm).movable_asset);
      post_execution_s3_upload(
        frm,
        movable_asset,
        'movable_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_pledge_remove_file_event(frm) {
  for (let index = 0; index < get_asset_values(frm).pledge.length; index++) {
    $('.' + frm.doc.name + '-pledge-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-pledge-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-pledge-' + index + 'remove-file',
        ).index(this);

        details.pledge[index].file_name.splice([s3_index]);
        details.pledge[index].file_url.splice([s3_index]);
        fileRemove('pledge', index, s3_index);
        const s3_key = [];
        if (
          details.pledge[index].file_name.length === 0 &&
          details.pledge[index].s3_key
        ) {
          details.pledge[index].file_name = '';
          details.pledge[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.pledge[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.pledge[index].s3_key[s3_key_index]
              ? details.pledge[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.pledge[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'pledge');
      },
    );
  }
}

function add_pledge_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-pledge-attach-file').unbind();
  $('.' + frm.doc.name + '-pledge-attach-input').unbind();

  $('.' + frm.doc.name + '-pledge-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-pledge-attach-file').index(this);
    $('.' + frm.doc.name + '-pledge-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-pledge-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];

    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('pledge', files, details, s3_button_index);
    details.pledge[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.pledge[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'pledge');
  });
}

function add_pledge_upload_all_event(frm) {
  $('.' + frm.doc.name + '-pledge-upload-all').unbind();
  $('.' + frm.doc.name + '-pledge-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-pledge-upload-all').index(this);
    if (get_asset_values(frm).pledge.some(x => x.file_url)) {
      const pledge = JSON.stringify(get_asset_values(frm).pledge);
      post_execution_s3_upload(
        frm,
        pledge,
        'pledge',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_personal_guarantee_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).personal_guarantee.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-personal-guarantee-' + index + 'remove-file',
    ).unbind();
    $('.' + frm.doc.name + '-personal-guarantee-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-personal-guarantee-' + index + 'remove-file',
        ).index(this);

        details.personal_guarantee[index].file_name.splice([s3_index]);
        details.personal_guarantee[index].file_url.splice([s3_index]);
        fileRemove('personal_guarantee', index, s3_index);
        const s3_key = [];
        if (
          details.personal_guarantee[index].file_name.length === 0 &&
          details.personal_guarantee[index].s3_key
        ) {
          details.personal_guarantee[index].file_name = '';
          details.personal_guarantee[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.personal_guarantee[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.personal_guarantee[index].s3_key[
              s3_key_index
            ]
              ? details.personal_guarantee[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.personal_guarantee[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'personal_guarantee');
      },
    );
  }
}

function add_personal_guarantee_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-personal-guarantee-attach-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-attach-input').unbind();

  $('.' + frm.doc.name + '-personal-guarantee-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-personal-guarantee-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-personal-guarantee-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-personal-guarantee-attach-input').change(
    async event => {
      const file_name = [];
      const files = [...event.target.files];
      const blobUrl = [];

      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      files.forEach(element => {
        file_name.push(element.name);
      });
      addFiles('personal_guarantee', files, details, s3_button_index);
      details.personal_guarantee[s3_button_index].file_name = file_name;
      for (let i = 0; i < file_name.length; i++) {
        blobUrl.push(await fileToBlobAndUrl(files[i]));
      }
      details.personal_guarantee[s3_button_index].file_url = blobUrl;
      frm.doc.type_of_security_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'personal_guarantee');
    },
  );
}

function add_personal_guarantee_upload_all_event(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-upload-all').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-upload-all').on('click', () => {
    const index = $(
      '.' + frm.doc.name + '-personal-guarantee-upload-all',
    ).index(this);
    if (get_asset_values(frm).personal_guarantee.some(x => x.file_url)) {
      const personal_guarantee = JSON.stringify(
        get_asset_values(frm).personal_guarantee,
      );
      post_execution_s3_upload(
        frm,
        personal_guarantee,
        'personal_guarantee',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_corporate_guarantee_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).corporate_guarantee.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-corporate-guarantee-' + index + 'remove-file',
    ).unbind();
    $('.' + frm.doc.name + '-corporate-guarantee-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-corporate-guarantee-' + index + 'remove-file',
        ).index(this);

        details.corporate_guarantee[index].file_name.splice([s3_index]);
        details.corporate_guarantee[index].file_url.splice([s3_index]);
        fileRemove('corporate_guarantee', index, s3_index);
        const s3_key = [];
        if (
          details.corporate_guarantee[index].file_name.length === 0 &&
          details.corporate_guarantee[index].s3_key
        ) {
          details.corporate_guarantee[index].file_name = '';
          details.corporate_guarantee[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.corporate_guarantee[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.corporate_guarantee[index].s3_key[
              s3_key_index
            ]
              ? details.corporate_guarantee[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.corporate_guarantee[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'corporate_guarantee');
      },
    );
  }
}

function add_corporate_guarantee_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-corporate-guarantee-attach-file').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-attach-input').unbind();

  $('.' + frm.doc.name + '-corporate-guarantee-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-corporate-guarantee-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-corporate-guarantee-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-corporate-guarantee-attach-input').change(
    async event => {
      const file_name = [];
      const files = [...event.target.files];
      const blobUrl = [];

      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      details.corporate_guarantee[s3_button_index].file_name = file_name;
      files.forEach(element => {
        file_name.push(element.name);
      });
      addFiles('corporate_guarantee', files, details, s3_button_index);
      details.corporate_guarantee[s3_button_index].file_url = blobUrl;
      frm.doc.type_of_security_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'corporate_guarantee');
    },
  );
}

function add_corporate_guarantee_upload_all_event(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-upload-all').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-upload-all').on('click', () => {
    const index = $(
      '.' + frm.doc.name + '-corporate-guarantee-upload-all',
    ).index(this);
    if (get_asset_values(frm).corporate_guarantee.some(x => x.file_url)) {
      const corporate_guarantee = JSON.stringify(
        get_asset_values(frm).corporate_guarantee,
      );
      post_execution_s3_upload(
        frm,
        corporate_guarantee,
        'corporate_guarantee',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_ndu_remove_file_event(frm) {
  for (let index = 0; index < get_asset_values(frm).ndu.length; index++) {
    $('.' + frm.doc.name + '-ndu-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-ndu-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-ndu-' + index + 'remove-file',
        ).index(this);

        details.ndu[index].file_name.splice([s3_index]);
        details.ndu[index].file_url.splice([s3_index]);
        fileRemove('ndu', index, s3_index);
        const s3_key = [];
        if (
          details.ndu[index].file_name.length === 0 &&
          details.ndu[index].s3_key
        ) {
          details.ndu[index].file_name = '';
          details.ndu[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.ndu[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.ndu[index].s3_key[s3_key_index]
              ? details.ndu[index].s3_key[s3_key_index].replace(/^.*[\\\/]/, '')
              : '';
            s3_key.push(file_name);
          }
          details.ndu[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'ndu');
      },
    );
  }
}

function add_ndu_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-ndu-attach-file').unbind();
  $('.' + frm.doc.name + '-ndu-attach-input').unbind();

  $('.' + frm.doc.name + '-ndu-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-ndu-attach-file').index(this);
    $('.' + frm.doc.name + '-ndu-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-ndu-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('ndu', files, details, s3_button_index);
    details.ndu[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.ndu[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'ndu');
  });
}

function add_ndu_upload_all_event(frm) {
  $('.' + frm.doc.name + '-ndu-upload-all').unbind();
  $('.' + frm.doc.name + '-ndu-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-ndu-upload-all').index(this);
    if (get_asset_values(frm).ndu.some(x => x.file_url)) {
      const ndu = JSON.stringify(get_asset_values(frm).ndu);
      post_execution_s3_upload(frm, ndu, 'ndu', 'type_of_security_details');
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_intangible_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).intangible_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-intangible-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-intangible-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-intangible-' + index + 'remove-file',
        ).index(this);

        details.intangible_asset[index].file_name.splice([s3_index]);
        details.intangible_asset[index].file_url.splice([s3_index]);
        fileRemove('intangible_asset', index, s3_index);
        const s3_key = [];
        if (
          details.intangible_asset[index].file_name.length === 0 &&
          details.intangible_asset[index].s3_key
        ) {
          details.intangible_asset[index].file_name = '';
          details.intangible_asset[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.intangible_asset[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.intangible_asset[index].s3_key[
              s3_key_index
            ]
              ? details.intangible_asset[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.intangible_asset[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'intangible_asset');
      },
    );
  }
}

function add_intangible_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-intangible-attach-file').unbind();
  $('.' + frm.doc.name + '-intangible-attach-input').unbind();

  $('.' + frm.doc.name + '-intangible-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-intangible-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-intangible-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-intangible-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('intangible_asset', files, details, s3_button_index);
    details.intangible_asset[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.intangible_asset[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'intangible_asset');
  });
}

function add_intangible_upload_all_event(frm) {
  $('.' + frm.doc.name + '-intangible-upload-all').unbind();
  $('.' + frm.doc.name + '-intangible-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-intangible-upload-all').index(this);
    if (get_asset_values(frm).intangible_asset.some(x => x.file_url)) {
      const intangible_asset = JSON.stringify(
        get_asset_values(frm).intangible_asset,
      );
      post_execution_s3_upload(
        frm,
        intangible_asset,
        'intangible_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_motor_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).motor_vehicle.length;
    index++
  ) {
    $('.' + frm.doc.name + '-motor-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-motor-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-motor-' + index + 'remove-file',
        ).index(this);

        details.motor_vehicle[index].file_name.splice([s3_index]);
        details.motor_vehicle[index].file_url.splice([s3_index]);
        fileRemove('motor_vehicle', index, s3_index);
        const s3_key = [];
        if (
          details.motor_vehicle[index].file_name.length === 0 &&
          details.motor_vehicle[index].s3_key
        ) {
          details.motor_vehicle[index].file_name = '';
          details.motor_vehicle[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.motor_vehicle[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.motor_vehicle[index].s3_key[s3_key_index]
              ? details.motor_vehicle[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.motor_vehicle[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'motor_vehicle');
      },
    );
  }
}

function add_motor_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-motor-attach-file').unbind();
  $('.' + frm.doc.name + '-motor-attach-input').unbind();

  $('.' + frm.doc.name + '-motor-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-motor-attach-file').index(this);
    $('.' + frm.doc.name + '-motor-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-motor-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('motor_vehicle', files, details, s3_button_index);
    details.motor_vehicle[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.motor_vehicle[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'motor_vehicle');
  });
}

function add_motor_upload_all_event(frm) {
  $('.' + frm.doc.name + '-motor-upload-all').unbind();
  $('.' + frm.doc.name + '-motor-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-motor-upload-all').index(this);
    if (get_asset_values(frm).motor_vehicle.some(x => x.file_url)) {
      const motor_vehicle = JSON.stringify(get_asset_values(frm).motor_vehicle);
      post_execution_s3_upload(
        frm,
        motor_vehicle,
        'motor_vehicle',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_financial_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).financial_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-financial-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-financial-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-financial-' + index + 'remove-file',
        ).index(this);

        details.financial_asset[index].file_name.splice([s3_index]);
        details.financial_asset[index].file_url.splice([s3_index]);
        fileRemove('financial_asset', index, s3_index);
        const s3_key = [];
        if (
          details.financial_asset[index].file_name.length === 0 &&
          details.financial_asset[index].s3_key
        ) {
          details.financial_asset[index].file_name = '';
          details.financial_asset[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.financial_asset[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.financial_asset[index].s3_key[
              s3_key_index
            ]
              ? details.financial_asset[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.financial_asset[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'financial_asset');
      },
    );
  }
}

function add_financial_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-financial-attach-file').unbind();
  $('.' + frm.doc.name + '-financial-attach-input').unbind();

  $('.' + frm.doc.name + '-financial-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-financial-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-financial-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-financial-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('financial_asset', files, details, s3_button_index);
    details.financial_asset[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.financial_asset[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'financial_asset');
  });
}

function add_financial_upload_all_event(frm) {
  $('.' + frm.doc.name + '-financial-upload-all').unbind();
  $('.' + frm.doc.name + '-financial-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-financial-upload-all').index(this);
    if (get_asset_values(frm).financial_asset.some(x => x.file_url)) {
      const financial_asset = JSON.stringify(
        get_asset_values(frm).financial_asset,
      );
      post_execution_s3_upload(
        frm,
        financial_asset,
        'financial_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_assignment_of_rights_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).assignment_of_rights.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-assignment-of-rights-' + index + 'remove-file',
    ).unbind();
    $('.' + frm.doc.name + '-assignment-of-rights-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-assignment-of-rights-' + index + 'remove-file',
        ).index(this);

        details.assignment_of_rights[index].file_name.splice([s3_index]);
        details.assignment_of_rights[index].file_url.splice([s3_index]);
        fileRemove('assignment_of_rights', index, s3_index);
        const s3_key = [];
        if (
          details.assignment_of_rights[index].file_name.length === 0 &&
          details.assignment_of_rights[index].s3_key
        ) {
          details.assignment_of_rights[index].file_name = '';
          details.assignment_of_rights[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.assignment_of_rights[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.assignment_of_rights[index].s3_key[
              s3_key_index
            ]
              ? details.assignment_of_rights[index].s3_key[
                  s3_key_index
                ].replace(/^.*[\\\/]/, '')
              : '';
            s3_key.push(file_name);
          }
          details.assignment_of_rights[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'assignment_of_rights');
      },
    );
  }
}

function add_assignment_of_rights_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-assignment-of-rights-attach-file').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-attach-input').unbind();

  $('.' + frm.doc.name + '-assignment-of-rights-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-assignment-of-rights-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-assignment-of-rights-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-assignment-of-rights-attach-input').change(
    async event => {
      const file_name = [];
      const files = [...event.target.files];
      const blobUrl = [];

      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      files.forEach(element => {
        file_name.push(element.name);
      });
      addFiles('assignment_of_rights', files, details, s3_button_index);
      details.assignment_of_rights[s3_button_index].file_name = file_name;
      for (let i = 0; i < file_name.length; i++) {
        blobUrl.push(await fileToBlobAndUrl(files[i]));
      }
      details.assignment_of_rights[s3_button_index].file_url = blobUrl;
      frm.doc.type_of_security_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'assignment_of_rights');
    },
  );
}

function add_assignment_of_rights_upload_all_event(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-upload-all').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-upload-all').on('click', () => {
    const index = $(
      '.' + frm.doc.name + '-assignment-of-rights-upload-all',
    ).index(this);
    if (get_asset_values(frm).assignment_of_rights.some(x => x.file_url)) {
      const assignment_of_rights = JSON.stringify(
        get_asset_values(frm).assignment_of_rights,
      );
      post_execution_s3_upload(
        frm,
        assignment_of_rights,
        'assignment_of_rights',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_current_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).current_asset.length;
    index++
  ) {
    $('.' + frm.doc.name + '-current-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-current-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-current-' + index + 'remove-file',
        ).index(this);

        details.current_asset[index].file_name.splice([s3_index]);
        details.current_asset[index].file_url.splice([s3_index]);
        fileRemove('current_asset', index, s3_index);
        const s3_key = [];
        if (
          details.current_asset[index].file_name.length === 0 &&
          details.current_asset[index].s3_key
        ) {
          details.current_asset[index].file_name = '';
          details.current_asset[index].file_url = '';
          for (
            let s3_key_index = 0;
            s3_key_index < details.current_asset[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.current_asset[index].s3_key[s3_key_index]
              ? details.current_asset[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.current_asset[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'current_asset');
      },
    );
  }
}

function add_current_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-current-attach-file').unbind();
  $('.' + frm.doc.name + '-current-attach-input').unbind();

  $('.' + frm.doc.name + '-current-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-current-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-current-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-current-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('current_asset', files, details, s3_button_index);
    details.current_asset[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.current_asset[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'current_asset');
  });
}

function add_current_upload_all_event(frm) {
  $('.' + frm.doc.name + '-current-upload-all').unbind();
  $('.' + frm.doc.name + '-current-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-current-upload-all').index(this);
    if (get_asset_values(frm).current_asset.some(x => x.file_url)) {
      const current_asset = JSON.stringify(get_asset_values(frm).current_asset);
      post_execution_s3_upload(
        frm,
        current_asset,
        'current_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_dsra_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).debt_service_reserve_account.length;
    index++
  ) {
    $('.' + frm.doc.name + '-dsra-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-dsra-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-dsra-' + index + 'remove-file',
        ).index(this);

        details.debt_service_reserve_account[index].file_name.splice([
          s3_index,
        ]);
        details.debt_service_reserve_account[index].file_url.splice([s3_index]);
        const s3_key = [];
        if (
          details.debt_service_reserve_account[index].file_name.length === 0 &&
          details.debt_service_reserve_account[index].s3_key
        ) {
          details.debt_service_reserve_account[index].file_name = '';
          details.debt_service_reserve_account[index].file_url = '';
          fileRemove('debt_service_reserve_account', index, s3_index);
          for (
            let s3_key_index = 0;
            s3_key_index <
            details.debt_service_reserve_account[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.debt_service_reserve_account[index]
              .s3_key[s3_key_index]
              ? details.debt_service_reserve_account[index].s3_key[
                  s3_key_index
                ].replace(/^.*[\\\/]/, '')
              : '';
            s3_key.push(file_name);
          }
          details.debt_service_reserve_account[index].file_name =
            s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'debt_service_reserve_account');
      },
    );
  }
}

function add_dsra_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-dsra-attach-file').unbind();
  $('.' + frm.doc.name + '-dsra-attach-input').unbind();

  $('.' + frm.doc.name + '-dsra-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-dsra-attach-file').index(this);
    $('.' + frm.doc.name + '-dsra-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-dsra-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('debt_service_reserve_account', files, details, s3_button_index);
    details.debt_service_reserve_account[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.debt_service_reserve_account[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'debt_service_reserve_account');
  });
}

function add_dsra_upload_all_event(frm) {
  $('.' + frm.doc.name + '-dsra-upload-all').unbind();
  $('.' + frm.doc.name + '-dsra-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-dsra-upload-all').index(this);
    if (
      get_asset_values(frm).debt_service_reserve_account.some(x => x.file_url)
    ) {
      const debt_service_reserve_account = JSON.stringify(
        get_asset_values(frm).debt_service_reserve_account,
      );
      post_execution_s3_upload(
        frm,
        debt_service_reserve_account,
        'debt_service_reserve_account',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_others_remove_file_event(frm) {
  for (let index = 0; index < get_asset_values(frm).others.length; index++) {
    $('.' + frm.doc.name + '-others-' + index + 'remove-file').unbind();
    $('.' + frm.doc.name + '-others-' + index + 'remove-file').on(
      'click',
      function () {
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        const s3_index = $(
          '.' + frm.doc.name + '-others-' + index + 'remove-file',
        ).index(this);

        details.others[index].file_name.splice([s3_index]);
        details.others[index].file_url.splice([s3_index]);
        const s3_key = [];
        if (
          details.others[index].file_name.length === 0 &&
          details.others[index].s3_key
        ) {
          details.others[index].file_name = '';
          details.others[index].file_url = '';
          fileRemove('others', index, s3_index);
          for (
            let s3_key_index = 0;
            s3_key_index < details.others[index].s3_key.length;
            s3_key_index++
          ) {
            const file_name = details.others[index].s3_key[s3_key_index]
              ? details.others[index].s3_key[s3_key_index].replace(
                  /^.*[\\\/]/,
                  '',
                )
              : '';
            s3_key.push(file_name);
          }
          details.others[index].file_name = s3_key.reverse();
        }
        frm.doc.type_of_security_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'others');
      },
    );
  }
}

function add_others_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-others-attach-file').unbind();
  $('.' + frm.doc.name + '-others-attach-input').unbind();

  $('.' + frm.doc.name + '-others-attach-file').on('click', function () {
    s3_button_index = $('.' + frm.doc.name + '-others-attach-file').index(this);
    $('.' + frm.doc.name + '-others-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-others-attach-input').change(async event => {
    const file_name = [];
    const files = [...event.target.files];
    const blobUrl = [];

    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    files.forEach(element => {
      file_name.push(element.name);
    });
    addFiles('others', files, details, s3_button_index);
    details.others[s3_button_index].file_name = file_name;
    for (let i = 0; i < file_name.length; i++) {
      blobUrl.push(await fileToBlobAndUrl(files[i]));
    }
    details.others[s3_button_index].file_url = blobUrl;
    frm.doc.type_of_security_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'others');
  });
}

function add_others_upload_all_event(frm) {
  $('.' + frm.doc.name + '-others-upload-all').unbind();
  $('.' + frm.doc.name + '-others-upload-all').on('click', () => {
    const index = $('.' + frm.doc.name + '-others-upload-all').index(this);
    if (get_asset_values(frm).others.some(x => x.file_url)) {
      const others = JSON.stringify(get_asset_values(frm).others);
      post_execution_s3_upload(
        frm,
        others,
        'others',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_ea_remove_file_event(frm) {
  for (
    let index = 0;
    index < get_asset_values(frm).post_execution_details.length;
    index++
  ) {
    $(
      '.' + frm.doc.name + '-post-execution-details-' + index + 'remove-file',
    ).unbind();
    $(
      '.' + frm.doc.name + '-post-execution-details-' + index + 'remove-file',
    ).on('click', function () {
      const details =
        frm.doc.post_execution_details !== undefined
          ? JSON.parse(frm.doc.post_execution_details)
          : {};
      const s3_index = $(
        '.' + frm.doc.name + '-post-execution-details-' + index + 'remove-file',
      ).index(this);

      details.post_execution_details[index].file_name.splice([s3_index]);
      details.post_execution_details[index].file_url.splice([s3_index]);
      fileRemove('post_execution_details', index, s3_index);
      const s3_key = [];
      if (
        details.post_execution_details[index].file_name.length === 0 &&
        details.post_execution_details[index].s3_key
      ) {
        details.post_execution_details[index].file_name = '';
        details.post_execution_details[index].file_url = '';
        for (
          let s3_key_index = 0;
          s3_key_index < details.post_execution_details[index].s3_key.length;
          s3_key_index++
        ) {
          const file_name = details.post_execution_details[index].s3_key[
            s3_key_index
          ]
            ? details.post_execution_details[index].s3_key[
                s3_key_index
              ].replace(/^.*[\\\/]/, '')
            : '';
          s3_key.push(file_name);
        }
        details.post_execution_details[index].file_name = s3_key.reverse();
      }
      frm.doc.post_execution_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'post_execution_details');
    });
  }
}

function add_ea_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-post-execution-details-attach-file').unbind();
  $('.' + frm.doc.name + '-post-execution-details-attach-input').unbind();

  $('.' + frm.doc.name + '-post-execution-details-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-post-execution-details-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-post-execution-details-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-post-execution-details-attach-input').change(
    async event => {
      const file_name = [];
      const files = [...event.target.files];
      const blobUrl = [];

      const details =
        frm.doc.post_execution_details !== undefined
          ? JSON.parse(frm.doc.post_execution_details)
          : {};
      files.forEach(element => {
        file_name.push(element.name);
      });
      addFiles('post_execution_details', files, details, s3_button_index);
      details.post_execution_details[s3_button_index].file_name = file_name;
      for (let i = 0; i < file_name.length; i++) {
        blobUrl.push(await fileToBlobAndUrl(files[i]));
      }
      details.post_execution_details[s3_button_index].file_url = blobUrl;
      frm.doc.post_execution_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'post_execution_details');
    },
  );
}

function add_ea_upload_all_event(frm) {
  $('.' + frm.doc.name + '-post-execution-details-upload-all').unbind();
  $('.' + frm.doc.name + '-post-execution-details-upload-all').on(
    'click',
    () => {
      const index = $(
        '.' + frm.doc.name + '-post-execution-details-upload-all',
      ).index(this);
      if (get_asset_values(frm).post_execution_details.some(x => x.file_url)) {
        const post_execution_details = JSON.stringify(
          get_asset_values(frm).post_execution_details,
        );
        post_execution_s3_upload(
          frm,
          post_execution_details,
          'post_execution_details',
          'post_execution_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function add_exception_tracking_upload_events(frm) {
  // immovable asset events
  add_immovable_exception_preview_event(frm);
  add_immovable_exception_remove_file_event(frm);
  add_immovable_exception_attach_file_event(frm);
  add_immovable_exception_upload_all_event(frm);

  // movable asset events
  add_movable_exception_preview_event(frm);
  add_movable_exception_remove_file_event(frm);
  add_movable_exception_attach_file_event(frm);
  add_movable_exception_upload_all_event(frm);

  // pledge asset events
  add_pledge_exception_preview_event(frm);
  add_pledge_exception_remove_file_event(frm);
  add_pledge_exception_attach_file_event(frm);
  add_pledge_exception_upload_all_event(frm);

  // personal guarantee events
  add_personal_guarantee_exception_preview_event(frm);
  add_personal_guarantee_exception_remove_file_event(frm);
  add_personal_guarantee_exception_attach_file_event(frm);
  add_personal_guarantee_exception_upload_all_event(frm);

  // corporate guarantee events
  add_corporate_guarantee_exception_preview_event(frm);
  add_corporate_guarantee_exception_remove_file_event(frm);
  add_corporate_guarantee_exception_attach_file_event(frm);
  add_corporate_guarantee_exception_upload_all_event(frm);

  // ndu events
  add_ndu_exception_preview_event(frm);
  add_ndu_exception_remove_file_event(frm);
  add_ndu_exception_attach_file_event(frm);
  add_ndu_exception_upload_all_event(frm);

  // intangible events
  add_intangible_exception_preview_event(frm);
  add_intangible_exception_remove_file_event(frm);
  add_intangible_exception_attach_file_event(frm);
  add_intangible_exception_upload_all_event(frm);

  // motor events
  add_motor_exception_preview_event(frm);
  add_motor_exception_remove_file_event(frm);
  add_motor_exception_attach_file_event(frm);
  add_motor_exception_upload_all_event(frm);

  // financial events
  add_financial_exception_preview_event(frm);
  add_financial_exception_remove_file_event(frm);
  add_financial_exception_attach_file_event(frm);
  add_financial_exception_upload_all_event(frm);

  // assignment of rights events
  add_assignment_of_rights_exception_preview_event(frm);
  add_assignment_of_rights_exception_remove_file_event(frm);
  add_assignment_of_rights_exception_attach_file_event(frm);
  add_assignment_of_rights_exception_upload_all_event(frm);

  // current events
  add_current_exception_preview_event(frm);
  add_current_exception_remove_file_event(frm);
  add_current_exception_attach_file_event(frm);
  add_current_exception_upload_all_event(frm);

  // dsra events
  add_dsra_exception_preview_event(frm);
  add_dsra_exception_remove_file_event(frm);
  add_dsra_exception_attach_file_event(frm);
  add_dsra_exception_upload_all_event(frm);
  // others events
  add_others_exception_preview_event(frm);
  add_others_exception_remove_file_event(frm);
  add_others_exception_attach_file_event(frm);
  add_others_exception_upload_all_event(frm);

  // pre_execution_details events
  add_pre_execution_details_exception_preview_event(frm);
  add_pre_execution_details_exception_remove_file_event(frm);
  add_pre_execution_details_exception_attach_file_event(frm);
  add_pre_execution_details_exception_upload_all_event(frm);
}

function add_immovable_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-immovable-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-immovable-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-immovable-exception-preview-file',
      ).index(this);
      const blobUrl = details.immovable_asset[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_immovable_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-immovable-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-immovable-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-immovable-exception-remove-file',
      ).index(this);
      details.immovable_asset[index].file = '';
      details.immovable_asset[index].file_name = details.immovable_asset[index]
        .s3_key
        ? details.immovable_asset[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('immovable_asset', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_immovable_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-immovable-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-immovable-exception-attach-input').unbind();
  $('.' + frm.doc.name + '-immovable-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-immovable-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-immovable-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-immovable-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('immovable_asset', file[0], details, s3_button_index);
        details.immovable_asset[s3_button_index].file_name = file[0].name;
        details.immovable_asset[s3_button_index].file = await fileToBlobAndUrl(
          file[0],
        );

        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_immovable_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + 'immovable-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-immovable-exception-upload-all').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-immovable-exception-upload-all',
      ).index(this);
      if (
        get_exception_tracking_values(frm).immovable_asset.some(x => x.file)
      ) {
        const immovable_asset = JSON.stringify(
          get_exception_tracking_values(frm).immovable_asset,
        );
        post_execution_s3_upload(
          frm,
          immovable_asset,
          'immovable_asset',
          'exception_tracking_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function add_movable_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-movable-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-movable-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-movable-exception-preview-file',
      ).index(this);
      const blobUrl = details.movable_asset[index].file;
      window.open(blobUrl, '_blank');
    },
  );
}

function add_movable_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-movable-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-movable-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-movable-exception-remove-file',
      ).index(this);
      details.movable_asset[index].file = '';
      details.movable_asset[index].file_name = details.movable_asset[index]
        .s3_key
        ? details.movable_asset[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('movable_asset', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_movable_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-movable-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-movable-exception-attach-input').unbind();
  $('.' + frm.doc.name + '-movable-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-movable-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-movable-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-movable-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('movable_asset', file[0], details, s3_button_index);
        details.movable_asset[s3_button_index].file_name = file[0].name;
        details.movable_asset[s3_button_index].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_movable_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-movable-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-movable-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).movable_asset.some(x => x.file)) {
      const movable_asset = JSON.stringify(
        get_exception_tracking_values(frm).movable_asset,
      );

      post_execution_s3_upload(
        frm,
        movable_asset,
        'movable_asset',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_pledge_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-pledge-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-pledge-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-pledge-exception-preview-file',
      ).index(this);
      const blobUrl = details.pledge[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_pledge_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-pledge-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-pledge-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-pledge-exception-remove-file',
      ).index(this);
      details.pledge[index].file = '';
      details.pledge[index].file_name = details.pledge[index].s3_key
        ? details.pledge[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('pledge', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_pledge_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-pledge-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-pledge-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-pledge-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-pledge-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-pledge-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-pledge-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('pledge', file[0], details, s3_button_index);
        details.pledge[s3_button_index].file_name = file[0].name;
        details.pledge[s3_button_index].file = await fileToBlobAndUrl(file[0]);
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_pledge_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-pledge-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-pledge-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).pledge.some(x => x.file)) {
      const pledge = JSON.stringify(get_exception_tracking_values(frm).pledge);
      post_execution_s3_upload(
        frm,
        pledge,
        'pledge',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_personal_guarantee_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-exception-preview-file',
      ).index(this);
      const blobUrl = details.personal_guarantee[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_personal_guarantee_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-exception-remove-file',
      ).index(this);
      details.personal_guarantee[index].file = '';
      details.personal_guarantee[index].file_name = details.personal_guarantee[
        index
      ].s3_key
        ? details.personal_guarantee[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('personal_guarantee', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_personal_guarantee_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-personal-guarantee-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-personal-guarantee-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-personal-guarantee-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-personal-guarantee-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-personal-guarantee-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException(
          'personal_guarantee',
          file[0],
          details,
          s3_button_index,
        );
        details.personal_guarantee[s3_button_index].file_name = file[0].name;
        details.personal_guarantee[s3_button_index].file =
          await fileToBlobAndUrl(file[0]);
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_personal_guarantee_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-exception-upload-all').on(
    'click',
    () => {
      if (
        get_exception_tracking_values(frm).personal_guarantee.some(x => x.file)
      ) {
        const personal_guarantee = JSON.stringify(
          get_exception_tracking_values(frm).personal_guarantee,
        );
        post_execution_s3_upload(
          frm,
          personal_guarantee,
          'personal_guarantee',
          'exception_tracking_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function add_corporate_guarantee_exception_preview_event(frm) {
  $(
    '.' + frm.doc.name + '-corporate-guarantee-exception-preview-file',
  ).unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-exception-preview-file',
      ).index(this);
      const blobUrl = details.corporate_guarantee[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_corporate_guarantee_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-exception-remove-file',
      ).index(this);
      details.corporate_guarantee[index].file = '';
      details.corporate_guarantee[index].file_name = details
        .corporate_guarantee[index].s3_key
        ? details.corporate_guarantee[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('corporate_guarantee', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_corporate_guarantee_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-corporate-guarantee-exception-attach-file').unbind();
  $(
    '.' + frm.doc.name + '-corporate-guarantee-exception-attach-input',
  ).unbind();

  $('.' + frm.doc.name + '-corporate-guarantee-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-corporate-guarantee-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-corporate-guarantee-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-corporate-guarantee-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException(
          'corporate_guarantee',
          file[0],
          details,
          s3_button_index,
        );
        details.corporate_guarantee[s3_button_index].file_name = file[0].name;
        details.corporate_guarantee[s3_button_index].file =
          await fileToBlobAndUrl(file[0]);
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_corporate_guarantee_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-exception-upload-all').on(
    'click',
    () => {
      if (
        get_exception_tracking_values(frm).corporate_guarantee.some(x => x.file)
      ) {
        const corporate_guarantee = JSON.stringify(
          get_exception_tracking_values(frm).corporate_guarantee,
        );
        post_execution_s3_upload(
          frm,
          corporate_guarantee,
          'corporate_guarantee',
          'exception_tracking_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function add_ndu_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-ndu-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-ndu-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $('.' + frm.doc.name + '-ndu-exception-preview-file').index(
        this,
      );
      const blobUrl = details.ndu[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_ndu_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-ndu-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-ndu-exception-remove-file').on('click', function () {
    const details =
      frm.doc.exception_tracking_details !== undefined
        ? JSON.parse(frm.doc.exception_tracking_details)
        : {};
    const index = $('.' + frm.doc.name + '-ndu-exception-remove-file').index(
      this,
    );
    details.ndu[index].file = '';
    details.ndu[index].file_name = details.ndu[index].s3_key
      ? details.ndu[index].s3_key.replace(/^.*[\\\/]/, '')
      : '';
    fileRemoveException('ndu', index);
    frm.doc.exception_tracking_details = JSON.stringify(details);
    load_asset_tables(frm);
    scroll_to_field(frm, 'exception_tracking');
  });
}

function add_ndu_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-ndu-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-ndu-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-ndu-exception-attach-file').on('click', function () {
    s3_button_index = $(
      '.' + frm.doc.name + '-ndu-exception-attach-file',
    ).index(this);
    $('.' + frm.doc.name + '-ndu-exception-attach-input')[
      s3_button_index
    ].click();
  });

  $('.' + frm.doc.name + '-ndu-exception-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      addFilesException('ndu', file[0], details, s3_button_index);
      details.ndu[s3_button_index].file_name = file[0].name;
      details.ndu[s3_button_index].file = await fileToBlobAndUrl(file[0]);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    }
  });
}

function add_ndu_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-ndu-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-ndu-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).ndu.some(x => x.file)) {
      const ndu = JSON.stringify(get_exception_tracking_values(frm).ndu);
      post_execution_s3_upload(frm, ndu, 'ndu', 'exception_tracking_details');
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_intangible_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-intangible-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-intangible-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-intangible-exception-preview-file',
      ).index(this);
      const blobUrl = details.intangible_asset[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_intangible_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-intangible-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-intangible-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-intangible-exception-remove-file',
      ).index(this);
      details.intangible_asset[index].file = '';
      details.intangible_asset[index].file_name = details.intangible_asset[
        index
      ].s3_key
        ? details.intangible_asset[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('intangible_asset', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_intangible_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-intangible-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-intangible-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-intangible-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-intangible-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-intangible-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-intangible-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException(
          'intangible_asset',
          file[0],
          details,
          s3_button_index,
        );
        details.intangible_asset[s3_button_index].file_name = file[0].name;
        details.intangible_asset[s3_button_index].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_intangible_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-intangible-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-intangible-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).intangible_asset.some(x => x.file)) {
      const intangible_asset = JSON.stringify(
        get_exception_tracking_values(frm).intangible_asset,
      );
      post_execution_s3_upload(
        frm,
        intangible_asset,
        'intangible_asset',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_motor_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-motor-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-motor-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-motor-exception-preview-file',
      ).index(this);
      const blobUrl = details.motor_vehicle[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_motor_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-motor-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-motor-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-motor-exception-remove-file',
      ).index(this);
      details.motor_vehicle[index].file = '';
      details.motor_vehicle[index].file_name = details.motor_vehicle[index]
        .s3_key
        ? details.motor_vehicle[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('motor_vehicle', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_motor_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-motor-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-motor-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-motor-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-motor-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-motor-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-motor-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('motor_vehicle', file[0], details, s3_button_index);
        details.motor_vehicle[s3_button_index].file_name = file[0].name;
        details.motor_vehicle[s3_button_index].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_motor_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-motor-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-motor-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).motor_vehicle.some(x => x.file)) {
      const motor_vehicle = JSON.stringify(
        get_exception_tracking_values(frm).motor_vehicle,
      );
      post_execution_s3_upload(
        frm,
        motor_vehicle,
        'motor_vehicle',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_financial_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-financial-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-financial-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-financial-exception-preview-file',
      ).index(this);
      const blobUrl = details.financial_asset[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_financial_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-financial-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-financial-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-financial-exception-remove-file',
      ).index(this);
      details.financial_asset[index].file = '';
      details.financial_asset[index].file_name = details.financial_asset[index]
        .s3_key
        ? details.financial_asset[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('financial_asset', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_financial_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-financial-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-financial-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-financial-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-financial-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-financial-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-financial-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('financial_asset', file[0], details, s3_button_index);
        details.financial_asset[s3_button_index].file_name = file[0].name;
        details.financial_asset[s3_button_index].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_financial_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-financial-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-financial-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).financial_asset.some(x => x.file)) {
      const financial_asset = JSON.stringify(
        get_exception_tracking_values(frm).financial_asset,
      );
      post_execution_s3_upload(
        frm,
        financial_asset,
        'financial_asset',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_assignment_of_rights_exception_preview_event(frm) {
  $(
    '.' + frm.doc.name + '-assignment-of-rights-exception-preview-file',
  ).unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-exception-preview-file',
      ).index(this);
      const blobUrl = details.assignment_of_rights[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_assignment_of_rights_exception_remove_file_event(frm) {
  $(
    '.' + frm.doc.name + '-assignment-of-rights-exception-remove-file',
  ).unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-exception-remove-file',
      ).index(this);
      details.assignment_of_rights[index].file = '';
      details.assignment_of_rights[index].file_name = details
        .assignment_of_rights[index].s3_key
        ? details.assignment_of_rights[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('assignment_of_rights', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_assignment_of_rights_exception_attach_file_event(frm) {
  let s3_button_index;

  $(
    '.' + frm.doc.name + '-assignment-of-rights-exception-attach-file',
  ).unbind();
  $(
    '.' + frm.doc.name + '-assignment-of-rights-exception-attach-input',
  ).unbind();

  $('.' + frm.doc.name + '-assignment-of-rights-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-assignment-of-rights-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-assignment-of-rights-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-assignment-of-rights-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException(
          'assignment_of_rights',
          file[0],
          details,
          s3_button_index,
        );
        details.assignment_of_rights[s3_button_index].file_name = file[0].name;
        details.assignment_of_rights[s3_button_index].file =
          await fileToBlobAndUrl(file[0]);
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_assignment_of_rights_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-exception-upload-all').on(
    'click',
    () => {
      if (
        get_exception_tracking_values(frm).assignment_of_rights.some(
          x => x.file,
        )
      ) {
        const assignment_of_rights = JSON.stringify(
          get_exception_tracking_values(frm).assignment_of_rights,
        );
        post_execution_s3_upload(
          frm,
          assignment_of_rights,
          'assignment_of_rights',
          'exception_tracking_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function add_current_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-current-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-current-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-current-exception-preview-file',
      ).index(this);
      const blobUrl = details.current_asset[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_current_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-current-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-current-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-current-exception-remove-file',
      ).index(this);
      details.current_asset[index].file = '';
      details.current_asset[index].file_name = details.current_asset[index]
        .s3_key
        ? details.current_asset[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('current_asset', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_current_exception_attach_file_event(frm) {
  let s3_button_index;
  // 2987b1c8d4-current-exception-attach-input
  // 2987b1c8d4-assignment-of-rights-exception-attach-input
  $('.' + frm.doc.name + '-current-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-current-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-current-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-current-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-current-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-current-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('current_asset', file[0], details, s3_button_index);
        details.current_asset[s3_button_index].file_name = file[0].name;
        details.current_asset[s3_button_index].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_current_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-current-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-current-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).current_asset.some(x => x.file)) {
      const current_asset = JSON.stringify(
        get_exception_tracking_values(frm).current_asset,
      );
      post_execution_s3_upload(
        frm,
        current_asset,
        'current_asset',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_dsra_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-dsra-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-dsra-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-dsra-exception-preview-file',
      ).index(this);
      const blobUrl = details.debt_service_reserve_account[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_dsra_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-dsra-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-dsra-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $('.' + frm.doc.name + '-dsra-exception-remove-file').index(
        this,
      );
      details.debt_service_reserve_account[index].file = '';
      details.debt_service_reserve_account[index].file_name = details
        .debt_service_reserve_account[index].s3_key
        ? details.debt_service_reserve_account[index].s3_key.replace(
            /^.*[\\\/]/,
            '',
          )
        : '';
      fileRemoveException('debt_service_reserve_account', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_dsra_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-dsra-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-dsra-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-dsra-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-dsra-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-dsra-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-dsra-exception-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      addFilesException(
        'debt_service_reserve_account',
        file[0],
        details,
        s3_button_index,
      );
      details.debt_service_reserve_account[s3_button_index].file_name =
        file[0].name;
      details.debt_service_reserve_account[s3_button_index].file =
        await fileToBlobAndUrl(file[0]);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    }
  });
}

function add_dsra_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-dsra-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-dsra-exception-upload-all').on('click', () => {
    if (
      get_exception_tracking_values(frm).debt_service_reserve_account.some(
        x => x.file,
      )
    ) {
      const debt_service_reserve_account = JSON.stringify(
        get_exception_tracking_values(frm).debt_service_reserve_account,
      );
      post_execution_s3_upload(
        frm,
        debt_service_reserve_account,
        'debt_service_reserve_account',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_others_exception_preview_event(frm) {
  $('.' + frm.doc.name + '-others-exception-preview-file').unbind();
  $('.' + frm.doc.name + '-others-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-others-exception-preview-file',
      ).index(this);
      const blobUrl = details.others[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_others_exception_remove_file_event(frm) {
  $('.' + frm.doc.name + '-others-exception-remove-file').unbind();
  $('.' + frm.doc.name + '-others-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-others-exception-remove-file',
      ).index(this);
      details.others[index].file = '';
      details.others[index].file_name = details.others[index].s3_key
        ? details.others[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('others', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_others_exception_attach_file_event(frm) {
  let s3_button_index;

  $('.' + frm.doc.name + '-others-exception-attach-file').unbind();
  $('.' + frm.doc.name + '-others-exception-attach-input').unbind();

  $('.' + frm.doc.name + '-others-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-others-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-others-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-others-exception-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.exception_tracking_details !== undefined
            ? JSON.parse(frm.doc.exception_tracking_details)
            : {};
        addFilesException('others', file[0], details, s3_button_index);
        details.others[s3_button_index].file_name = file[0].name;
        details.others[s3_button_index].file = await fileToBlobAndUrl(file[0]);
        frm.doc.exception_tracking_details = JSON.stringify(details);
        load_asset_tables(frm);
        scroll_to_field(frm, 'exception_tracking');
      }
    },
  );
}

function add_others_exception_upload_all_event(frm) {
  $('.' + frm.doc.name + '-others-exception-upload-all').unbind();
  $('.' + frm.doc.name + '-others-exception-upload-all').on('click', () => {
    if (get_exception_tracking_values(frm).others.some(x => x.file)) {
      const others = JSON.stringify(get_exception_tracking_values(frm).others);
      post_execution_s3_upload(
        frm,
        others,
        'others',
        'exception_tracking_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function add_pre_execution_details_exception_preview_event(frm) {
  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-preview-file',
  ).unbind();
  $('.' + frm.doc.name + '-pre-execution-details-exception-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-exception-preview-file',
      ).index(this);
      const blobUrl = details.pre_execution_details[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function add_pre_execution_details_exception_remove_file_event(frm) {
  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-remove-file',
  ).unbind();
  $('.' + frm.doc.name + '-pre-execution-details-exception-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-exception-remove-file',
      ).index(this);
      details.pre_execution_details[index].file = '';
      details.pre_execution_details[index].file_name = details
        .pre_execution_details[index].s3_key
        ? details.pre_execution_details[index].s3_key.replace(/^.*[\\\/]/, '')
        : '';
      fileRemoveException('pre_execution_details', index);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    },
  );
}

function add_pre_execution_details_exception_attach_file_event(frm) {
  let s3_button_index;

  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-attach-file',
  ).unbind();
  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-attach-input',
  ).unbind();

  $('.' + frm.doc.name + '-pre-execution-details-exception-attach-file').on(
    'click',
    function () {
      s3_button_index = $(
        '.' + frm.doc.name + '-pre-execution-details-exception-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-pre-execution-details-exception-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-attach-input',
  ).change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.exception_tracking_details !== undefined
          ? JSON.parse(frm.doc.exception_tracking_details)
          : {};
      addFilesException(
        'pre_execution_details',
        file[0],
        details,
        s3_button_index,
      );
      details.pre_execution_details[s3_button_index].file_name = file[0].name;
      details.pre_execution_details[s3_button_index].file =
        await fileToBlobAndUrl(file[0]);
      frm.doc.exception_tracking_details = JSON.stringify(details);
      load_asset_tables(frm);
      scroll_to_field(frm, 'exception_tracking');
    }
  });
}

function add_pre_execution_details_exception_upload_all_event(frm) {
  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-upload-all',
  ).unbind();
  $('.' + frm.doc.name + '-pre-execution-details-exception-upload-all').on(
    'click',
    () => {
      if (
        get_exception_tracking_values(frm).pre_execution_details.some(
          x => x.file,
        )
      ) {
        const pre_execution_details = JSON.stringify(
          get_exception_tracking_values(frm).pre_execution_details,
        );
        post_execution_s3_upload(
          frm,
          pre_execution_details,
          'pre_execution_details',
          'exception_tracking_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function add_download_events(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    if (get_asset_values(frm).immovable_asset) {
      for (
        let index = 0;
        index < get_asset_values(frm).immovable_asset.length;
        index++
      ) {
        $('.' + frm.doc.name + '-immovable-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-immovable-' + index + 's3-download',
            ).index(this);
            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).immovable_asset[index].s3_key[
                s3_index
              ],
            });
          },
        );
      }
    }
    if (get_asset_values(frm).movable_asset) {
      for (
        let index = 0;
        index < get_asset_values(frm).movable_asset.length;
        index++
      ) {
        $('.' + frm.doc.name + '-movable-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-movable-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).movable_asset[index].s3_key[s3_index],
            });
          },
        );
      }
    }

    if (get_asset_values(frm).pledge) {
      for (
        let index = 0;
        index < get_asset_values(frm).pledge.length;
        index++
      ) {
        $('.' + frm.doc.name + '-pledge-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-pledge-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).pledge[index].s3_key[s3_index],
            });
          },
        );
      }
    }

    if (get_asset_values(frm).personal_guarantee) {
      for (
        let index = 0;
        index < get_asset_values(frm).personal_guarantee.length;
        index++
      ) {
        $(
          '.' + frm.doc.name + '-personal-guarantee-' + index + 's3-download',
        ).on('click', function () {
          const s3_index = $(
            '.' + frm.doc.name + '-personal-guarantee-' + index + 's3-download',
          ).index(this);

          open_url_post(frappe.request.url, {
            cmd: 'trusteeship_platform.custom_methods.download_s3_file',
            key: get_asset_values(frm).personal_guarantee[index].s3_key[
              s3_index
            ],
          });
        });
      }
    }

    if (get_asset_values(frm).corporate_guarantee) {
      for (
        let index = 0;
        index < get_asset_values(frm).corporate_guarantee.length;
        index++
      ) {
        $(
          '.' + frm.doc.name + '-corporate-guarantee-' + index + 's3-download',
        ).on('click', function () {
          const s3_index = $(
            '.' +
              frm.doc.name +
              '-corporate-guarantee-' +
              index +
              's3-download',
          ).index(this);

          open_url_post(frappe.request.url, {
            cmd: 'trusteeship_platform.custom_methods.download_s3_file',
            key: get_asset_values(frm).corporate_guarantee[index].s3_key[
              s3_index
            ],
          });
        });
      }
    }
    if (get_asset_values(frm).ndu) {
      for (let index = 0; index < get_asset_values(frm).ndu.length; index++) {
        $('.' + frm.doc.name + '-ndu-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-ndu-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).ndu[index].s3_key[s3_index],
            });
          },
        );
      }
    }

    if (get_asset_values(frm).intangible_asset) {
      for (
        let index = 0;
        index < get_asset_values(frm).intangible_asset.length;
        index++
      ) {
        $('.' + frm.doc.name + '-intangible-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-intangible-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).intangible_asset[index].s3_key[
                s3_index
              ],
            });
          },
        );
      }
    }

    if (get_asset_values(frm).motor_vehicle) {
      for (
        let index = 0;
        index < get_asset_values(frm).motor_vehicle.length;
        index++
      ) {
        $('.' + frm.doc.name + '-motor-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-motor-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).motor_vehicle[index].s3_key[s3_index],
            });
          },
        );
      }
    }
    if (get_asset_values(frm).financial_asset) {
      for (
        let index = 0;
        index < get_asset_values(frm).financial_asset.length;
        index++
      ) {
        $('.' + frm.doc.name + '-financial-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-financial-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).financial_asset[index].s3_key[
                s3_index
              ],
            });
          },
        );
      }
    }

    if (get_asset_values(frm).assignment_of_rights) {
      for (
        let index = 0;
        index < get_asset_values(frm).assignment_of_rights.length;
        index++
      ) {
        $(
          '.' + frm.doc.name + '-assignment-of-rights-' + index + 's3-download',
        ).on('click', function () {
          const s3_index = $(
            '.' +
              frm.doc.name +
              '-assignment-of-rights-' +
              index +
              's3-download',
          ).index(this);

          open_url_post(frappe.request.url, {
            cmd: 'trusteeship_platform.custom_methods.download_s3_file',
            key: get_asset_values(frm).assignment_of_rights[index].s3_key[
              s3_index
            ],
          });
        });
      }
    }
    if (get_asset_values(frm).current_asset) {
      for (
        let index = 0;
        index < get_asset_values(frm).current_asset.length;
        index++
      ) {
        $('.' + frm.doc.name + '-current-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-current-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).current_asset[index].s3_key[s3_index],
            });
          },
        );
      }
    }
    if (get_asset_values(frm).debt_service_reserve_account) {
      for (
        let index = 0;
        index < get_asset_values(frm).debt_service_reserve_account.length;
        index++
      ) {
        $('.' + frm.doc.name + '-dsra-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-dsra-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).debt_service_reserve_account[index]
                .s3_key[s3_index],
            });
          },
        );
      }
    }
    if (get_asset_values(frm).others) {
      for (
        let index = 0;
        index < get_asset_values(frm).others.length;
        index++
      ) {
        $('.' + frm.doc.name + '-others-' + index + 's3-download').on(
          'click',
          function () {
            const s3_index = $(
              '.' + frm.doc.name + '-others-' + index + 's3-download',
            ).index(this);

            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: get_asset_values(frm).others[index].s3_key[s3_index],
            });
          },
        );
      }
    }
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    if (get_asset_values(frm).post_execution_details) {
      for (
        let index = 0;
        index < get_asset_values(frm).post_execution_details.length;
        index++
      ) {
        $(
          '.' +
            frm.doc.name +
            '-post-execution-details-' +
            index +
            's3-download',
        ).on('click', function () {
          const s3_index = $(
            '.' +
              frm.doc.name +
              '-post-execution-details-' +
              index +
              's3-download',
          ).index(this);

          open_url_post(frappe.request.url, {
            cmd: 'trusteeship_platform.custom_methods.download_s3_file',
            key: get_asset_values(frm).post_execution_details[index].s3_key[
              s3_index
            ],
          });
        });
      }
    }
  }
}

function add_exception_tracking_download_events(frm) {
  $(
    '.' + frm.doc.name + '-immovable-exception-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-immovable-exception-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-immovable-exception-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).immovable_asset[index]
          .standard_template_s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-immovable-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-immovable-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-immovable-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).immovable_asset[index].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-movable-exception-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-movable-exception-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-movable-exception-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).movable_asset[index]
          .standard_template_s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-movable-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-movable-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-movable-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).movable_asset[index].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-pledge-exception-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-pledge-exception-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-pledge-exception-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).pledge[index]
          .standard_template_s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-pledge-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-pledge-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-pledge-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).pledge[index].s3_key,
      });
    },
  );

  $(
    '.' +
      frm.doc.name +
      '-personal-guarantee-exception-standard-template-download',
  ).unbind();
  $(
    '.' +
      frm.doc.name +
      '-personal-guarantee-exception-standard-template-download',
  ).on('click', function () {
    const index = $(
      '.' +
        frm.doc.name +
        '-personal-guarantee-exception-standard-template-download',
    ).index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: get_exception_tracking_values(frm).personal_guarantee[index]
        .standard_template_s3_key,
    });
  });

  $('.' + frm.doc.name + '-personal-guarantee-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).personal_guarantee[index]
          .s3_key,
      });
    },
  );

  $(
    '.' +
      frm.doc.name +
      '-corporate-guarantee-exception-standard-template-download',
  ).unbind();
  $(
    '.' +
      frm.doc.name +
      '-corporate-guarantee-exception-standard-template-download',
  ).on('click', function () {
    const index = $(
      '.' +
        frm.doc.name +
        '-corporate-guarantee-exception-standard-template-download',
    ).index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: get_exception_tracking_values(frm).corporate_guarantee[index]
        .standard_template_s3_key,
    });
  });

  $('.' + frm.doc.name + '-corporate-guarantee-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).corporate_guarantee[index]
          .s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-ndu-exception-standard-template-download').unbind();
  $('.' + frm.doc.name + '-ndu-exception-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-ndu-exception-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).ndu[index]
          .standard_template_s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-ndu-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-ndu-exception-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-ndu-exception-s3-download').index(
      this,
    );
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: get_exception_tracking_values(frm).ndu[index].s3_key,
    });
  });

  $(
    '.' + frm.doc.name + '-intangible-exception-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-intangible-exception-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-intangible-exception-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).intangible_asset[index]
          .standard_template_s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-intangible-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-intangible-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-intangible-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).intangible_asset[index].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-others-exception-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-others-exception-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-others-exception-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).others[index]
          .standard_template_s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-others-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-others-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-others-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).others[index].s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-motor-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-motor-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-motor-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).motor_vehicle[index].s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-financial-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-financial-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-financial-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).financial_asset[index].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-assignment-of-rights-exception-s3-download',
  ).unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).assignment_of_rights[index]
          .s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-current-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-current-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-current-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).current_asset[index].s3_key,
      });
    },
  );

  $('.' + frm.doc.name + '-dsra-exception-s3-download').unbind();
  $('.' + frm.doc.name + '-dsra-exception-s3-download').on(
    'click',
    function () {
      const index = $('.' + frm.doc.name + '-dsra-exception-s3-download').index(
        this,
      );
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).debt_service_reserve_account[
          index
        ].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-motor-exception-standard-template-s3-download',
  ).unbind();
  $('.' + frm.doc.name + '-motor-exception-standard-template-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-motor-exception-standard-template-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).motor_vehicle[index].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-financial-exception-standard-template-s3-download',
  ).unbind();
  $(
    '.' + frm.doc.name + '-financial-exception-standard-template-s3-download',
  ).on('click', function () {
    const index = $(
      '.' + frm.doc.name + '-financial-exception-standard-template-s3-download',
    ).index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: get_exception_tracking_values(frm).financial_asset[index].s3_key,
    });
  });

  $(
    '.' +
      frm.doc.name +
      '-assignment-of-rights-exception-standard-template-s3-download',
  ).unbind();
  $(
    '.' +
      frm.doc.name +
      '-assignment-of-rights-exception-standard-template-s3-download',
  ).on('click', function () {
    const index = $(
      '.' +
        frm.doc.name +
        '-assignment-of-rights-exception-standard-template-s3-download',
    ).index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: get_exception_tracking_values(frm).assignment_of_rights[index]
        .s3_key,
    });
  });

  $(
    '.' + frm.doc.name + '-current-exception-standard-template-s3-download',
  ).unbind();
  $('.' + frm.doc.name + '-current-exception-standard-template-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-current-exception-standard-template-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).current_asset[index].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-dsra-exception-standard-template-s3-download',
  ).unbind();
  $('.' + frm.doc.name + '-dsra-exception-standard-template-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-dsra-exception-standard-template-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).debt_service_reserve_account[
          index
        ].s3_key,
      });
    },
  );

  $(
    '.' + frm.doc.name + '-pre-execution-details-exception-s3-download',
  ).unbind();
  $('.' + frm.doc.name + '-pre-execution-details-exception-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-exception-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: get_exception_tracking_values(frm).pre_execution_details[index]
          .s3_key,
      });
    },
  );
}

async function post_execution_s3_upload(
  frm,
  assetDetails,
  asset_name,
  detailsKey,
) {
  frappe.dom.freeze();
  const parsedassetDetails = JSON.parse(assetDetails);
  let row;
  let s3Index;
  if (detailsKey === 'type_of_security_details') {
    const promises = [];
    for (let i = 0; i < files_upload[asset_name].length; i++) {
      s3Index = files_upload[asset_name][i].s3ButtonIndex;
      row = parsedassetDetails[s3Index];
      const files = files_upload[asset_name][i].files;
      const assetFiles = files[i].files;

      delete row.file_url;

      for (let j = 0; j < assetFiles.length; j++) {
        promises.push(
          uploadAssetFile(
            row,
            frm,
            assetFiles[j],
            asset_name,
            detailsKey,
            s3Index,
          ),
        );
      }
      await Promise.all(promises);
      files_upload[asset_name] = [];
    }
  } else {
    for (let i = 0; i < files_upload_exception[asset_name].length; i++) {
      s3Index = files_upload_exception[asset_name][i].s3ButtonIndex;
      row = parsedassetDetails[s3Index];
      const file = files_upload_exception[asset_name][i].file;
      delete row.file;
      await uploadAssetFile(row, frm, file, asset_name, detailsKey);
    }
    files_upload_exception[asset_name] = [];
  }
  asset_name =
    detailsKey !== 'exception_tracking_details'
      ? asset_name
      : 'exception_tracking';
  frappe.dom.freeze();
  frm.reload_doc().then(() => {
    setTimeout(() => {
      frappe.dom.unfreeze();
      scroll_to_field(frm, asset_name);
    }, 2200);
  });
  frappe.show_alert(
    {
      message: 'Uploaded',
      indicator: 'green',
    },
    5,
  );
}

function type_of_security_row_dialog(
  frm,
  default_values = {},
  asset_name,
  index = null,
) {
  const d = new frappe.ui.Dialog({
    title: index !== null ? 'Edit' : 'Add',
    fields: get_type_of_security_fields(
      frm,
      default_values,
      asset_name,
      index === null,
    ),
    primary_action_label: 'Confirm',
    primary_action: values => {
      if (index === null) {
        validate_document_code(frm, values, asset_name);
      } else {
        add_values(values, default_values);
      }
      if (
        frm.doc.product_name.toUpperCase() === 'STE' ||
        frm.doc.product_name.toUpperCase() === 'DTE'
      ) {
        const immovable_asset = get_asset_values(frm).immovable_asset;
        const movable_asset = get_asset_values(frm).movable_asset;
        const pledge = get_asset_values(frm).pledge;
        const personal_guarantee = get_asset_values(frm).personal_guarantee;
        const corporate_guarantee = get_asset_values(frm).corporate_guarantee;
        const ndu = get_asset_values(frm).ndu;
        const intangible_asset = get_asset_values(frm).intangible_asset;
        const motor_vehicle = get_asset_values(frm).motor_vehicle;
        const financial_asset = get_asset_values(frm).financial_asset;
        const assignment_of_rights = get_asset_values(frm).assignment_of_rights;
        const current_asset = get_asset_values(frm).current_asset;
        const debt_service_reserve_account =
          get_asset_values(frm).debt_service_reserve_account;
        const others = get_asset_values(frm).others;
        if (asset_name === 'immovable_asset') {
          index !== null
            ? (immovable_asset[index] = values)
            : immovable_asset.push(values);
        } else if (asset_name === 'movable_asset') {
          index !== null
            ? (movable_asset[index] = values)
            : movable_asset.push(values);
        } else if (asset_name === 'pledge') {
          index !== null ? (pledge[index] = values) : pledge.push(values);
        } else if (asset_name === 'personal_guarantee') {
          index !== null
            ? (personal_guarantee[index] = values)
            : personal_guarantee.push(values);
        } else if (asset_name === 'corporate_guarantee') {
          index !== null
            ? (corporate_guarantee[index] = values)
            : corporate_guarantee.push(values);
        } else if (asset_name === 'ndu') {
          index !== null ? (ndu[index] = values) : ndu.push(values);
        } else if (asset_name === 'intangible_asset') {
          index !== null
            ? (intangible_asset[index] = values)
            : intangible_asset.push(values);
        } else if (asset_name === 'motor_vehicle') {
          index !== null
            ? (motor_vehicle[index] = values)
            : motor_vehicle.push(values);
        } else if (asset_name === 'financial_asset') {
          index !== null
            ? (financial_asset[index] = values)
            : financial_asset.push(values);
        } else if (asset_name === 'assignment_of_rights') {
          index !== null
            ? (assignment_of_rights[index] = values)
            : assignment_of_rights.push(values);
        } else if (asset_name === 'current_asset') {
          index !== null
            ? (current_asset[index] = values)
            : current_asset.push(values);
        } else if (asset_name === 'debt_service_reserve_account') {
          index !== null
            ? (debt_service_reserve_account[index] = values)
            : debt_service_reserve_account.push(values);
        } else if (asset_name === 'others') {
          index !== null ? (others[index] = values) : others.push(values);
        }
        frm.doc.type_of_security_details = JSON.stringify({
          immovable_asset,
          movable_asset,
          pledge,
          personal_guarantee,
          corporate_guarantee,
          ndu,
          intangible_asset,
          motor_vehicle,
          financial_asset,
          assignment_of_rights,
          current_asset,
          debt_service_reserve_account,
          others,
        });
      } else if (
        frm.doc.product_name.toUpperCase() === 'EA' ||
        frm.doc.product_name.toUpperCase() === 'FA' ||
        frm.doc.product_name.toUpperCase() === 'INVIT' ||
        frm.doc.product_name.toUpperCase() === 'REIT'
      ) {
        const post_execution_details = get_asset_values(frm)
          .post_execution_details
          ? get_asset_values(frm).post_execution_details
          : [];
        index !== null
          ? (post_execution_details[index] = values)
          : post_execution_details.push(values);
        frm.doc.post_execution_details = JSON.stringify({
          post_execution_details,
        });
      }
      d.hide();
      frm.dirty();
      frm.save().then(() => {
        setTimeout(() => {
          scroll_to_field(frm, asset_name);
        }, 100);
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  d.fields_dict.document_code.get_query = function () {
    return {
      filters: [
        ['Canopi Document Code', 'post_execution_checklist', '=', 1],
        ['Canopi Document Code', 'product', '=', frm.doc.product_name],
      ],
    };
  };
  filter_security(frm, d, asset_name);
}

function validate_document_code(frm, dialog_values, asset_name) {
  const asset_values = get_asset_values(frm)[asset_name]
    ? get_asset_values(frm)[asset_name]
    : [];

  if (asset_values.some(x => x.document_code === dialog_values.document_code)) {
    frappe.throw('<b>Document Code</b> must be unique.');
  }
}

function add_values(values, default_values) {
  values.is_date_of_registration_visible =
    default_values.is_date_of_registration_visible;
  values.is_sro_name_visible = default_values.is_sro_name_visible;
  values.is_reg_number_visible = default_values.is_reg_number_visible;
  values.is_roc_filing_date_visible = default_values.is_roc_filing_date_visible;
  values.is_cersai_filing_date_visible =
    default_values.is_cersai_filing_date_visible;
  values.is_pledge_creation_date_visible =
    default_values.is_pledge_creation_date_visible;
}
function exception_tracking_row_dialog(frm, default_values, asset_name, index) {
  const d = new frappe.ui.Dialog({
    title: 'Edit',
    fields: get_exception_tracking_fields(default_values),
    primary_action_label: 'Confirm',
    primary_action: values => {
      const immovable_asset =
        get_exception_tracking_values(frm).immovable_asset;
      const movable_asset = get_exception_tracking_values(frm).movable_asset;
      const pledge = get_exception_tracking_values(frm).pledge;
      const personal_guarantee =
        get_exception_tracking_values(frm).personal_guarantee;
      const corporate_guarantee =
        get_exception_tracking_values(frm).corporate_guarantee;
      const ndu = get_exception_tracking_values(frm).ndu;
      const intangible_asset =
        get_exception_tracking_values(frm).intangible_asset;
      const motor_vehicle = get_exception_tracking_values(frm).motor_vehicle;
      const financial_asset =
        get_exception_tracking_values(frm).financial_asset;
      const assignment_of_rights =
        get_exception_tracking_values(frm).assignment_of_rights;
      const current_asset = get_exception_tracking_values(frm).current_asset;
      const debt_service_reserve_account =
        get_exception_tracking_values(frm).debt_service_reserve_account;
      const others = get_exception_tracking_values(frm).others;
      const pre_execution_details =
        get_exception_tracking_values(frm).pre_execution_details;

      if (asset_name === 'immovable_asset') {
        immovable_asset[index] = values;
      } else if (asset_name === 'movable_asset') {
        movable_asset[index] = values;
      } else if (asset_name === 'pledge') {
        pledge[index] = values;
      } else if (asset_name === 'personal_guarantee') {
        personal_guarantee[index] = values;
      } else if (asset_name === 'corporate_guarantee') {
        corporate_guarantee[index] = values;
      } else if (asset_name === 'ndu') {
        ndu[index] = values;
      } else if (asset_name === 'intangible_asset') {
        intangible_asset[index] = values;
      } else if (asset_name === 'motor_vehicle') {
        motor_vehicle[index] = values;
      } else if (asset_name === 'financial_asset') {
        financial_asset[index] = values;
      } else if (asset_name === 'assignment_of_rights') {
        assignment_of_rights[index] = values;
      } else if (asset_name === 'current_asset') {
        current_asset[index] = values;
      } else if (asset_name === 'debt_service_reserve_account') {
        debt_service_reserve_account[index] = values;
      } else if (asset_name === 'others') {
        others[index] = values;
      } else if (asset_name === 'pre_execution_details') {
        pre_execution_details[index] = values;
      }

      frm.doc.exception_tracking_details = JSON.stringify({
        immovable_asset,
        movable_asset,
        pledge,
        personal_guarantee,
        corporate_guarantee,
        ndu,
        intangible_asset,
        motor_vehicle,
        financial_asset,
        assignment_of_rights,
        current_asset,
        debt_service_reserve_account,
        others,
        pre_execution_details,
      });
      d.hide();
      frm.dirty();
      frm.refresh();
      load_asset_tables(frm);

      setTimeout(() => {
        scroll_to_field(frm, 'exception_tracking');
      }, 100);
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
}

function get_type_of_security_fields(frm, doc, asset_name, is_new = false) {
  const fields = [
    {
      label: 'Document Code',
      fieldname: 'document_code',
      fieldtype: 'Link',
      options: 'Canopi Document Code',
      default: doc.document_code,
      read_only: is_new ? 0 : 1,
      reqd: 1,
      onchange: () => {
        if (cur_dialog.get_field('document_code').value) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: {
              doc_name: cur_dialog.get_field('document_code').value,
              doc_type: 'Canopi Document Code',
            },
            freeze: true,
            callback: r => {
              cur_dialog.get_field('description').value = r.message.description;
              cur_dialog.refresh();
            },
          });
          filter_security(frm, cur_dialog, asset_name);
        }
      },
    },
    {
      label: 'Description',
      fieldname: 'description',
      fieldtype: 'Data',
      default: doc.description ? doc.description : ' ',
      read_only: is_new ? 0 : 1,
      reqd: is_new ? 1 : 0,
    },
    {
      label: 'Security Documents - Post-Execution Stage',
      fieldname: 'security_doc_post',
      fieldtype: 'Link',
      options: 'Canopi Security Documents Post Execution',
      depends_on: 'eval: doc.document_code',
      default: doc.security_doc_post ? doc.security_doc_post : '',
    },
    {
      label: 'Upload Date',
      fieldname: 'upload_date',
      fieldtype: 'Date',
      read_only: 1,
      default: doc.upload_date ? doc.upload_date : null,
    },
    {
      label: 'Meta Data Fields',
      fieldname: 'meta_section_break',
      fieldtype: 'Section Break',
      depends_on: is_new
        ? 'eval: doc.description === `Registration with SRO`|| doc.description === `ROC Filing` || doc.description === `CERSAI` ||doc.description === `Pledge Creation with DP (Form W)`'
        : 'eval: (doc.description === `Registration with SRO` && (doc.is_date_of_registration_visible === 1 || doc.is_sro_name_visible ===1 || doc.is_reg_number_visible ===1)) || (doc.description === `ROC Filing` && doc.is_roc_filing_date_visible ===1) ||  (doc.description === `CERSAI` && doc.is_cersai_filing_date_visible ===1) || (doc.description === `Pledge Creation with DP (Form W)` && doc.is_pledge_creation_date_visible ===1)',
    },
    {
      label: 'Date of Registration',
      fieldname: 'is_date_of_registration_visible',
      fieldtype: 'Check',
      hidden: is_new ? 0 : 1,
      depends_on: "eval: doc.description === 'Registration with SRO';",
      default: doc.is_date_of_registration_visible
        ? doc.is_date_of_registration_visible
        : null,
    },
    {
      label: 'SRO Name',
      fieldname: 'is_sro_name_visible',
      fieldtype: 'Check',
      hidden: is_new ? 0 : 1,
      depends_on: "eval: doc.description === 'Registration with SRO';",
      default: doc.is_sro_name_visible ? doc.is_sro_name_visible : null,
    },
    {
      label: 'Reg Number',
      fieldname: 'is_reg_number_visible',
      fieldtype: 'Check',
      hidden: is_new ? 0 : 1,
      depends_on: "eval: doc.description === 'Registration with SRO';",
      default: doc.is_reg_number_visible ? doc.is_reg_number_visible : null,
    },
    {
      label: 'ROC Filing Date',
      fieldname: 'is_roc_filing_date_visible',
      fieldtype: 'Check',
      hidden: is_new ? 0 : 1,
      depends_on: "eval: doc.description === 'ROC Filing';",
      default: doc.is_roc_filing_date_visible
        ? doc.is_roc_filing_date_visible
        : null,
    },
    {
      label: 'CERSAI Filing Date',
      fieldname: 'is_cersai_filing_date_visible',
      fieldtype: 'Check',
      depends_on: "eval: doc.description === 'CERSAI';",
      hidden: is_new ? 0 : 1,
      default: doc.is_cersai_filing_date_visible
        ? doc.is_cersai_filing_date_visible
        : null,
    },
    {
      label: 'Pledge Creation Date',
      fieldname: 'is_pledge_creation_date_visible',
      fieldtype: 'Check',
      depends_on:
        "eval: doc.description === 'Pledge Creation with DP (Form W)';",
      hidden: is_new ? 0 : 1,
      default: doc.is_pledge_creation_date_visible
        ? doc.is_pledge_creation_date_visible
        : null,
    },
    {
      label: 'Date of Registration',
      fieldname: 'date_of_registration',
      fieldtype: 'Date',
      hidden: !is_new
        ? !(
            doc.description === 'Registration with SRO' &&
            doc.is_date_of_registration_visible === 1
          )
        : 0,
      default: doc.date_of_registration ? doc.date_of_registration : null,
      depends_on: is_new
        ? "eval: doc.description === 'Registration with SRO' && doc.is_date_of_registration_visible === 1;"
        : 0,
    },
    {
      label: 'SRO Name',
      fieldname: 'sro_name',
      fieldtype: 'Data',
      hidden: !is_new
        ? !(
            doc.description === 'Registration with SRO' &&
            doc.is_sro_name_visible === 1
          )
        : 0,
      default: doc.sro_name ? doc.sro_name : ' ',
      depends_on: is_new
        ? "eval: doc.description === 'Registration with SRO' && doc.is_sro_name_visible === 1;"
        : 0,
    },
    {
      label: 'Reg Number',
      fieldname: 'reg_number',
      fieldtype: 'Data',
      hidden: !is_new
        ? !(
            doc.description === 'Registration with SRO' &&
            doc.is_reg_number_visible === 1
          )
        : 0,
      default: doc.reg_number ? doc.reg_number : ' ',
      depends_on: is_new
        ? "eval: doc.description === 'Registration with SRO' && doc.is_reg_number_visible === 1;"
        : 0,
    },
    {
      label: 'ROC Filing Date',
      fieldname: 'roc_filing_date',
      fieldtype: 'Date',
      hidden: !is_new
        ? !(
            doc.description === 'ROC Filing' &&
            doc.is_roc_filing_date_visible === 1
          )
        : 0,
      default: doc.roc_filing_date ? doc.roc_filing_date : null,
      depends_on: is_new
        ? "eval: doc.description === 'ROC Filing' && doc.is_roc_filing_date_visible === 1;"
        : 0,
    },
    {
      label: 'CERSAI Filing Date',
      fieldname: 'cersai_filing_date',
      fieldtype: 'Date',
      hidden: !is_new
        ? !(
            doc.description === 'CERSAI' &&
            doc.is_cersai_filing_date_visible === 1
          )
        : 0,
      default: doc.cersai_filing_date ? doc.cersai_filing_date : null,
      depends_on: is_new
        ? "eval: doc.description === 'CERSAI' && doc.is_cersai_filing_date_visible === 1;"
        : 0,
    },
    {
      label: 'Pledge Creation Date',
      fieldname: 'pledge_creation_date',
      fieldtype: 'Date',
      hidden: !is_new
        ? !(
            doc.description === 'Pledge Creation with DP (Form W)' &&
            doc.is_pledge_creation_date_visible === 1
          )
        : 0,
      default: doc.pledge_creation_date ? doc.pledge_creation_date : null,
      depends_on: is_new
        ? "eval: doc.description === 'Pledge Creation with DP (Form W)' && doc.is_pledge_creation_date_visible === 1;"
        : 0,
    },
    {
      label: 'Comments',
      fieldname: 'comments',
      fieldtype: 'Small Text',
      default: doc.comments ? doc.comments : ' ',
    },
    {
      label: 'S3 Key',
      fieldname: 's3_key',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.s3_key ? doc.s3_key : '',
    },
    {
      label: 'File Name',
      fieldname: 'file_name',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.file_name ? doc.file_name : '',
    },
    {
      label: 'File Name',
      fieldname: 'file',
      fieldtype: 'Small Text',
      hidden: 1,
      default: doc.file ? doc.file : '',
    },
  ];
  return fields;
}

function filter_security(frm, cur_dialog, asset_name) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_single_doc',
    args: { doc_type: 'Canopi Notification Interval' },
    freeze: true,
    callback: r => {
      const canopi_notification_interval =
        r.message.canopi_notification_interval.find(
          element =>
            element.document_name ===
            cur_dialog.get_field('document_code').value,
        );

      if (canopi_notification_interval) {
        let filter = add_filter(canopi_notification_interval);
        filter = filter.filter(p => p !== 0);
        if (filter.length !== 0) {
          cur_dialog.fields_dict.security_doc_post.get_query = function () {
            return {
              filters: [
                ['operations', '=', frm.doc.operations],
                ['security_type', 'in', filter],
                ['security_type', '=', capitalizeWords(asset_name)],
                ['status', '=', 'Approved By Ops Checker'],
              ],
            };
          };
        } else {
          cur_dialog.fields_dict.security_doc_post.get_query = function () {
            return {
              filters: [
                ['operations', '=', frm.doc.operations],
                ['security_type', '=', capitalizeWords(asset_name)],
                ['status', '=', 'Approved By Ops Checker'],
              ],
            };
          };
        }
      } else {
        cur_dialog.fields_dict.security_doc_post.get_query = function () {
          return {
            filters: [
              ['operations', '=', frm.doc.operations],
              ['security_type', '=', capitalizeWords(asset_name)],
              ['status', '=', 'Approved By Ops Checker'],
            ],
          };
        };
      }
    },
  });
}
function add_filter(canopi_notification_interval) {
  const filter = [];
  canopi_notification_interval.immovable === 1
    ? filter.push('Immovable Asset')
    : filter.push(0);
  canopi_notification_interval.movable === 1
    ? filter.push('Movable Asset')
    : filter.push(0);
  canopi_notification_interval.pledge === 1
    ? filter.push('Pledge')
    : filter.push(0);
  canopi_notification_interval.personal_guarantee === 1
    ? filter.push('Personal Guarantee')
    : filter.push(0);
  canopi_notification_interval.corporate_guarantee === 1
    ? filter.push('Corporate Guarantee')
    : filter.push(0);
  canopi_notification_interval.ndu === 1 ? filter.push('NDU') : filter.push(0);
  canopi_notification_interval.intangible === 1
    ? filter.push('security_type', '=', 'Intangible Asset')
    : filter.push(0);
  canopi_notification_interval.motor_vehicle === 1
    ? filter.push('security_type', '=', 'Motor Vehicle')
    : filter.push(0);
  canopi_notification_interval.financial_asset === 1
    ? filter.push('Financial Asset')
    : filter.push(0);
  canopi_notification_interval.assignment_of_rights === 1
    ? filter.push('Assignment of Rights')
    : filter.push(0);
  canopi_notification_interval.current_asset === 1
    ? filter.push('Current Asset')
    : filter.push(0);
  canopi_notification_interval.debt_service_reserve_account_dsra === 1
    ? filter.push('Debt Service Reserve Account (DSRA)')
    : filter.push(0);
  canopi_notification_interval.others === 1
    ? filter.push('Others')
    : filter.push(0);
  return filter;
}

function get_exception_tracking_fields(doc) {
  const fields = [
    {
      label: 'Description',
      fieldname: 'description',
      fieldtype: 'Data',
      default: doc.description ? doc.description : ' ',
      read_only: 1,
    },
    {
      label: 'Document Code',
      fieldname: 'document_code',
      fieldtype: 'Data',
      default: doc.document_code ? doc.document_code : ' ',
      read_only: 1,
    },
    {
      label: 'Status',
      fieldname: 'status',
      fieldtype: 'Data',
      default: doc.status ? doc.status : '',
    },
    {
      label: 'Upload Date',
      fieldname: 'upload_date',
      fieldtype: 'Date',
      default: doc.upload_date ? doc.upload_date : null,
      read_only: 1,
    },
    {
      label: 'Comments / Justification',
      fieldname: 'comments',
      fieldtype: 'Small Text',
      default: doc.comments ? doc.comments : '',
    },
    {
      label: 'Due Date',
      fieldname: 'due_date',
      fieldtype: 'Date',
      default: doc.due_date ? doc.due_date : null,
    },
    {
      label: 'S3 Key',
      fieldname: 's3_key',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.s3_key ? doc.s3_key : '',
      read_only: 1,
    },
    {
      label: 'File Name',
      fieldname: 'file_name',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.file_name ? doc.file_name : '',
    },
    {
      label: 'File Name',
      fieldname: 'file',
      fieldtype: 'Small Text',
      hidden: 1,
      default: doc.file ? doc.file : '',
    },
    {
      label: 'Standard Template Name',
      fieldname: 'standard_template_name',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.standard_template_name ? doc.standard_template_name : '',
    },
    {
      label: 'Standard Template S3 Key',
      fieldname: 'standard_template_s3_key',
      fieldtype: 'Small Text',
      hidden: 1,
      default: doc.standard_template_s3_key ? doc.standard_template_s3_key : '',
    },
  ];
  return fields;
}

function scroll_to_field(frm, field_name) {
  if (field_name === 'immovable_asset') {
    frm.fields_dict.immovable_asset_section.collapse(1);
    frm.scroll_to_field('immovable_asset_html');
  } else if (field_name === 'movable_asset') {
    frm.fields_dict.movable_asset_section.collapse(1);
    frm.scroll_to_field('movable_asset_html');
  } else if (field_name === 'pledge') {
    frm.fields_dict.pledge_section.collapse(1);
    frm.scroll_to_field('pledge_html');
  } else if (field_name === 'personal_guarantee') {
    frm.fields_dict.personal_guarantee_section.collapse(1);
    frm.scroll_to_field('personal_guarantee_html');
  } else if (field_name === 'corporate_guarantee') {
    frm.fields_dict.corporate_guarantee_section.collapse(1);
    frm.scroll_to_field('corporate_guarantee_html');
  } else if (field_name === 'ndu') {
    frm.fields_dict.ndu_section.collapse(1);
    frm.scroll_to_field('ndu_html');
  } else if (field_name === 'intangible_asset') {
    frm.fields_dict.intangible_asset_section.collapse(1);
    frm.scroll_to_field('intangible_asset_html');
  } else if (field_name === 'motor_vehicle') {
    frm.fields_dict.motor_vehicle_section.collapse(1);
    frm.scroll_to_field('motor_vehicle_html');
  } else if (field_name === 'financial_asset') {
    frm.fields_dict.financial_asset_section.collapse(1);
    frm.scroll_to_field('financial_asset_html');
  } else if (field_name === 'assignment_of_rights') {
    frm.fields_dict.assignment_of_rights_section.collapse(1);
    frm.scroll_to_field('assignment_of_rights_html');
  } else if (field_name === 'current_asset') {
    frm.fields_dict.current_asset_section.collapse(1);
    frm.scroll_to_field('current_asset_html');
  } else if (field_name === 'debt_service_reserve_account') {
    frm.fields_dict.debt_service_reserve_account_section.collapse(1);
    frm.scroll_to_field('debt_service_reserve_account_html');
  } else if (field_name === 'others') {
    frm.fields_dict.others_section.collapse(1);
    frm.scroll_to_field('others_html');
  } else if (field_name === 'exception_tracking') {
    frm.fields_dict.exception_tracking_section.collapse(1);
    frm.scroll_to_field('exception_tracking_html');
  } else if (field_name === 'post_execution_details') {
    frm.fields_dict.post_execution_details_section.collapse(1);
    frm.scroll_to_field('post_execution_details_html');
  }
}

function empty_html_fields(frm) {
  $(frm.fields_dict.immovable_asset_html.wrapper).empty();
  $(frm.fields_dict.movable_asset_html.wrapper).empty();
  $(frm.fields_dict.pledge_html.wrapper).empty();
  $(frm.fields_dict.personal_guarantee_html.wrapper).empty();
  $(frm.fields_dict.corporate_guarantee_html.wrapper).empty();
  $(frm.fields_dict.ndu_html.wrapper).empty();
  $(frm.fields_dict.intangible_asset_html.wrapper).empty();
  $(frm.fields_dict.others_html.wrapper).empty();
  $(frm.fields_dict.motor_vehicle_html.wrapper).empty();
  $(frm.fields_dict.financial_asset_html.wrapper).empty();
  $(frm.fields_dict.assignment_of_rights_html.wrapper).empty();
  $(frm.fields_dict.current_asset_html.wrapper).empty();
  $(frm.fields_dict.debt_service_reserve_account_html.wrapper).empty();
  $(frm.fields_dict.exception_tracking_html.wrapper).empty();
}

function get_asset_values(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const immovable_asset = details.immovable_asset;
    const movable_asset = details.movable_asset;
    const pledge = details.pledge;
    const personal_guarantee = details.personal_guarantee;
    const corporate_guarantee = details.corporate_guarantee;
    const ndu = details.ndu;
    const intangible_asset = details.intangible_asset;
    const motor_vehicle = details.motor_vehicle;
    const financial_asset = details.financial_asset;
    const assignment_of_rights = details.assignment_of_rights;
    const current_asset = details.current_asset;
    const debt_service_reserve_account = details.debt_service_reserve_account;

    const others = details.others;

    return {
      immovable_asset,
      movable_asset,
      pledge,
      personal_guarantee,
      corporate_guarantee,
      ndu,
      intangible_asset,
      motor_vehicle,
      financial_asset,
      assignment_of_rights,
      current_asset,
      debt_service_reserve_account,
      others,
    };
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    return frm.doc.post_execution_details
      ? JSON.parse(frm.doc.post_execution_details)
      : {};
  } else {
    return {};
  }
}

function get_exception_tracking_values(frm) {
  frm.doc.exception_tracking_details = frm.doc.exception_tracking_details
    ? frm.doc.exception_tracking_details
    : JSON.stringify({});
  const details =
    frm.doc.exception_tracking_details !== undefined
      ? JSON.parse(frm.doc.exception_tracking_details)
      : {};
  const immovable_asset = details.immovable_asset
    ? details.immovable_asset
    : [];
  const movable_asset = details.movable_asset ? details.movable_asset : [];
  const pledge = details.pledge ? details.pledge : [];
  const personal_guarantee = details.personal_guarantee
    ? details.personal_guarantee
    : [];
  const corporate_guarantee = details.corporate_guarantee
    ? details.corporate_guarantee
    : [];
  const ndu = details.ndu ? details.ndu : [];
  const intangible_asset = details.intangible_asset
    ? details.intangible_asset
    : [];
  const others = details.others ? details.others : [];
  const motor_vehicle = details.motor_vehicle ? details.motor_vehicle : [];
  const financial_asset = details.financial_asset
    ? details.financial_asset
    : [];
  const assignment_of_rights = details.assignment_of_rights
    ? details.assignment_of_rights
    : [];
  const current_asset = details.current_asset ? details.current_asset : [];
  const debt_service_reserve_account = details.debt_service_reserve_account
    ? details.debt_service_reserve_account
    : [];
  const pre_execution_details = details.pre_execution_details
    ? details.pre_execution_details
    : [];
  const is_empty = Object.keys(details).every(key => details[key].length === 0);

  return {
    immovable_asset,
    movable_asset,
    pledge,
    personal_guarantee,
    corporate_guarantee,
    ndu,
    intangible_asset,
    motor_vehicle,
    financial_asset,
    assignment_of_rights,
    current_asset,
    debt_service_reserve_account,
    others,
    pre_execution_details,
    is_empty,
  };
}

function add_percent_pill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let title_element = $(`h3[title="${frm.doc.name}"]`).parent();
    title_element = title_element[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.completion_percent + '%';
    pillspan.appendChild(textspan);

    title_element.appendChild(pillspan);
  }
}

function show_activities(frm) {
  $(frm.fields_dict.open_activities_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;
  const crm_activities = new erpnext.utils.CRMActivities({
    frm,
    open_activities_wrapper: $(frm.fields_dict.open_activities_html.wrapper),
    all_activities_wrapper: $(frm.fields_dict.all_activities_html.wrapper),
    form_wrapper: $(frm.wrapper),
  });
  crm_activities.refresh();

  // remove extra activities table
  setTimeout(() => {
    if ($('.open-activities').length > 1) {
      crm_activities.refresh();
    }
  }, 400);
}

function show_notes(frm) {
  $(frm.fields_dict.notes_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;

  const crm_notes = new erpnext.utils.CRMNotes({
    frm,
    notes_wrapper: $(frm.fields_dict.notes_html.wrapper),
  });
  crm_notes.refresh();

  $('.new-note-btn').unbind();
  $('.new-note-btn').on('click', function () {
    add_note(frm);
  });

  $('.notes-section').find('.edit-note-btn').unbind();
  $('.notes-section')
    .find('.edit-note-btn')
    .on('click', function () {
      edit_note(this, frm);
    });

  $('.notes-section').find('.delete-note-btn').unbind();
  $('.notes-section')
    .find('.delete-note-btn')
    .on('click', function () {
      delete_note(this, frm);
    });
}

function add_note(frm) {
  const d = new frappe.ui.Dialog({
    title: __('Add a Note'),
    fields: [
      {
        label: 'Note',
        fieldname: 'note',
        fieldtype: 'Text Editor',
        reqd: 1,
        enable_mentions: true,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_note',
        args: {
          doctype: frm.doctype,
          docname: frm.doc.name,
          note: data.note,
        },
        freeze: true,
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.refresh_fields();
          }
        },
      });
    },
    primary_action_label: __('Add'),
  });
  d.show();
}

function edit_note(edit_btn, frm) {
  const row = $(edit_btn).closest('.comment-content');
  const row_id = row.attr('name');
  const row_content = $(row).find('.content').html();
  if (row_content) {
    const d = new frappe.ui.Dialog({
      title: __('Edit Note'),
      fields: [
        {
          label: 'Note',
          fieldname: 'note',
          fieldtype: 'Text Editor',
          default: row_content,
        },
      ],
      primary_action: function () {
        const data = d.get_values();
        frappe.call({
          method: 'trusteeship_platform.custom_methods.edit_note',
          args: {
            doctype: frm.doctype,
            docname: frm.doc.name,
            note: data.note,
            row_id,
          },
          freeze: true,
          callback: function (r) {
            if (!r.exc) {
              d.hide();
              frm.reload_doc();
            }
          },
        });
      },
      primary_action_label: __('Done'),
    });
    d.show();
  }
}

function delete_note(delete_btn, frm) {
  const row_id = $(delete_btn).closest('.comment-content').attr('name');
  frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_note',
    args: {
      doctype: frm.doctype,
      docname: frm.doc.name,
      row_id,
    },
    freeze: true,
    callback: function (r) {
      if (!r.exc) {
        frm.refresh_fields();
      }
    },
  });
}

function validate_checker_and_maker(frm) {
  if (
    frm.doc.tl_representative &&
    frm.doc.ops_servicing_rm &&
    frm.doc.tl_representative === frm.doc.ops_servicing_rm
  ) {
    frm.set_value('ops_servicing_rm', '');
    frm.set_value('tl_representative', '');
    frappe.throw(
      '<b>Ops & Servicing RM</b> and <b>TL Representative</b> cannot be same.',
    );
  }
}

function redirectDoctype(frm, doctype, response) {
  if (response.message[doctype]) {
    const outputString = doctype
      .replace(/([a-z0-9])([A-Z])/g, '$1 $2') // Insert space before capital letters if not already separated
      .toLowerCase() // Convert the entire string to lowercase
      .replace(/\s+/g, '-'); // Replace spaces with hyphens
    window.open(`/app/${outputString}/${response.message[doctype][0].name}`);
  } else {
    frappe.model.with_doctype(doctype, function () {
      const source = frm.doc;
      const targetDoc = frappe.model.get_new_doc(doctype);

      targetDoc.operations = source.operations;
      targetDoc.person_name = source.person_name;
      targetDoc.cnp_customer_code = source.cnp_customer_code;
      targetDoc.mandate_name = source.mandate_name;
      targetDoc.opportunity = source.opportunity;
      targetDoc.product_name = source.product_name;
      if (
        doctype === 'Canopi Post Execution Checklist' ||
        doctype === 'Canopi Pre Execution Checklist' ||
        doctype === 'Canopi Documentation'
      ) {
        targetDoc.ops_servicing_rm = source.ops_servicing_rm;
        targetDoc.tl_representative = source.tl_representative;
      }
      frappe.ui.form.make_quick_entry(doctype, null, null, targetDoc);
    });
  }
}

function capitalizeWords(str) {
  // Split the string into words using underscores as separators
  const words = str.split('_');
  // Capitalize the first letter of each word and join them with spaces
  let capitalizedWords = words
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
  capitalizedWords =
    capitalizedWords === 'Debt Service Reserve Account'
      ? capitalizedWords + ' (DSRA)'
      : capitalizedWords;
  return capitalizedWords;
}

// email dialig
const dialog = new frappe.ui.Dialog({
  title: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
  no_submit_on_enter: true,
  fields: get_fields(),
  primary_action_label: __('Send'),
  primary_action() {
    send_action();
  },
  secondary_action_label: __('Discard'),
  secondary_action() {
    dialog.hide();
  },
  size: 'large',
  minimizable: true,
});

function email_dialog() {
  $(dialog.$wrapper.find('.form-section').get(0)).addClass('to_section');
  prepare();
  dialog.show();
}

function prepare() {
  setup_multiselect_queries();
  setup_attach();
  setup_email();
  setup_email_template();
}

function setup_multiselect_queries() {
  ['recipients', 'cc', 'bcc'].forEach(field => {
    dialog.fields_dict[field].get_data = () => {
      const data = dialog.fields_dict[field].get_value();
      const txt = data.match(/[^,\s*]*$/)[0] || '';

      frappe.call({
        method: 'frappe.email.get_contact_list',
        args: { txt },
        callback: r => {
          dialog.fields_dict[field].set_data(r.message);
        },
      });
    };
  });
}

let attachments = [];

function setup_attach() {
  const fields = dialog.fields_dict;
  const attach = $(fields.select_attachments.wrapper);

  if (!attachments) {
    attachments = [];
  }

  let args = {
    folder: 'Home/Attachments',
    on_success: attachment => {
      attachments.push(attachment);
      render_attachment_rows(attachment);
    },
  };

  if (cur_frm) {
    args = {
      doctype: cur_frm.doctype,
      docname: cur_frm.docname,
      folder: 'Home/Attachments',
      on_success: attachment => {
        cur_frm.attachments.attachment_uploaded(attachment);
        render_attachment_rows(attachment);
      },
    };
  }

  $(`
    <label class="control-label">
    ${__('Select Attachments')}
    </label>
    <div class='attach-list'></div>
    <p class='add-more-attachments'>
    <button class='btn btn-xs btn-default'>
      ${frappe.utils.icon('small-add', 'xs')}&nbsp;
      ${__('Add Attachment')}
    </button>
    </p>
  `).appendTo(attach.empty());

  attach
    .find('.add-more-attachments button')
    .on('click', () => new frappe.ui.FileUploader(args));
  render_attachment_rows();
}

function render_attachment_rows(attachment = null) {
  const select_attachments = dialog.fields_dict.select_attachments;
  const attachment_rows = $(select_attachments.wrapper).find('.attach-list');
  if (attachment) {
    attachment_rows.append(get_attachment_row(attachment, true));
  } else {
    let files = [];
    if (attachments && attachments.length) {
      files = files.concat(attachments);
    }
    if (cur_frm) {
      files = files.concat(cur_frm.get_files());
    }

    if (files.length) {
      $.each(files, (i, f) => {
        if (!f.file_name) return;
        if (!attachment_rows.find(`[data-file-name="${f.name}"]`).length) {
          f.file_url = frappe.urllib.get_full_url(f.file_url);
          attachment_rows.append(get_attachment_row(f));
        }
      });
    }
  }
}

function get_attachment_row(attachment, checked = null) {
  return $(`<p class="checkbox flex">
    <label class="ellipsis" title="${attachment.file_name}">
    <input
      type="checkbox"
      data-file-name="${attachment.name}"
      ${checked ? 'checked' : ''}>
    </input>
    <span class="ellipsis">${attachment.file_name}</span>
    </label>
    &nbsp;
    <a href="${attachment.file_url}" target="_blank" class="btn-linkF">
    ${frappe.utils.icon('link-url')}
    </a>
  </p>`);
}

function setup_email() {
  // email
  const fields = dialog.fields_dict;

  $(fields.send_me_a_copy.input).on('click', () => {
    // update send me a copy (make it sticky)
    const val = fields.send_me_a_copy.get_value();
    frappe.db.set_value('User', frappe.session.user, 'send_me_a_copy', val);
    frappe.boot.user.send_me_a_copy = val;
  });
}

function setup_email_template() {
  dialog.fields_dict.email_template.df.onchange = () => {
    const email_template = dialog.fields_dict.email_template.get_value();
    if (!email_template) return;

    frappe.call({
      method:
        'frappe.email.doctype.email_template.email_template.get_email_template',
      args: {
        template_name: email_template,
        doc: cur_frm.doc,
        _lang: dialog.get_value('language_sel'),
      },
      callback(r) {
        prepend_reply(r.message, email_template);
      },
    });
  };
}

let reply_added = '';
function prepend_reply(reply, email_template) {
  if (reply_added === email_template) return;
  const content_field = dialog.fields_dict.content;
  const subject_field = dialog.fields_dict.subject;

  let content = content_field.get_value() || '';
  content = content.split('<!-- salutation-ends -->')[1] || content;

  content_field.set_value(`${reply.message}<br>${content}`);
  subject_field.set_value(reply.subject);

  reply_added = email_template;
}

function send_action() {
  const form_values = get_values();
  if (!form_values) return;

  const selected_attachments = $.map(
    $(dialog.wrapper).find('[data-file-name]:checked'),
    function (element) {
      return $(element).attr('data-file-name');
    },
  );
  send_email(form_values, selected_attachments);
}

function get_values() {
  const form_values = dialog.get_values();

  // cc
  for (let i = 0, l = dialog.fields.length; i < l; i++) {
    const df = dialog.fields[i];

    if (df.is_cc_checkbox) {
      // concat in cc
      if (form_values[df.fieldname]) {
        form_values.cc =
          (form_values.cc ? form_values.cc + ', ' : '') + df.fieldname;
        form_values.bcc =
          (form_values.bcc ? form_values.bcc + ', ' : '') + df.fieldname;
      }

      delete form_values[df.fieldname];
    }
  }

  return form_values;
}

function send_email(form_values, selected_attachments) {
  dialog.hide();

  if (!form_values.recipients) {
    frappe.msgprint(__('Enter Email Recipient(s)'));
    return;
  }

  if (cur_frm && !frappe.model.can_email(cur_frm.doc.doctype, cur_frm)) {
    frappe.msgprint(
      __('You are not allowed to send emails related to this document'),
    );
    return;
  }

  frappe.call({
    method: 'trusteeship_platform.custom_methods.documentation_send_mail',
    args: {
      recipients: form_values.recipients,
      cc: form_values.cc,
      bcc: form_values.bcc,
      subject: form_values.subject,
      content: form_values.content,
      send_me_a_copy: form_values.send_me_a_copy,
      attachments: selected_attachments,
      s3_attachments: JSON.stringify(s3_attachments),
      read_receipt: form_values.send_read_receipt,
      sender: form_values.sender,
      child_doctype,
      after_email_update_field,
    },
    freeze: true,
    freeze_message: __('Sending'),

    callback: r => {
      if (!r.exc) {
        frappe.utils.play_sound('email');

        if (r.message?.emails_not_sent_to) {
          frappe.msgprint(
            __('Email not sent to {0} (unsubscribed / disabled)', [
              frappe.utils.escape_html(r.message.emails_not_sent_to),
            ]),
          );
        }

        if (cur_frm) {
          cur_frm.reload_doc().then(() => {
            setTimeout(() => {
              if (child_doctype === 'Canopi Transaction Documents') {
                cur_frm.scroll_to_field('transaction_documents_html');
              }
              if (
                child_doctype === 'Canopi Transaction Documents Post Execution'
              ) {
                cur_frm.scroll_to_field(
                  'transaction_documents_post_execution_html',
                );
              }
              if (child_doctype === 'Canopi Security Documents') {
                cur_frm.scroll_to_field(
                  'security_documents_pre_execution_html',
                );
              }
            }, 100);
          });
        }
      } else {
        frappe.msgprint(
          __('There were errors while sending email. Please try again.'),
        );
      }
    },
  });
}

function get_fields() {
  const fields = [
    {
      label: __('To'),
      fieldtype: 'MultiSelect',
      reqd: 0,
      fieldname: 'recipients',
    },
    {
      fieldtype: 'Button',
      label: frappe.utils.icon('down'),
      fieldname: 'option_toggle_button',
      click: () => {
        toggle_more_options(false);
      },
    },
    {
      fieldtype: 'Section Break',
      hidden: 1,
      fieldname: 'more_options',
    },
    {
      label: __('CC'),
      fieldtype: 'MultiSelect',
      fieldname: 'cc',
    },
    {
      label: __('BCC'),
      fieldtype: 'MultiSelect',
      fieldname: 'bcc',
    },
    {
      label: __('Email Template'),
      fieldtype: 'Link',
      options: 'Email Template',
      fieldname: 'email_template',
    },
    { fieldtype: 'Section Break' },
    {
      label: __('Subject'),
      fieldtype: 'Data',
      reqd: 1,
      fieldname: 'subject',
      default: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
      length: 524288,
    },
    {
      label: __('Message'),
      fieldtype: 'Text Editor',
      fieldname: 'content',
    },
    { fieldtype: 'Section Break' },
    {
      label: __('Send me a copy'),
      fieldtype: 'Check',
      fieldname: 'send_me_a_copy',
      default: frappe.boot.user.send_me_a_copy,
    },
    {
      label: __('Send Read Receipt'),
      fieldtype: 'Check',
      fieldname: 'send_read_receipt',
    },

    { fieldtype: 'Column Break' },
    {
      label: __('Select Attachments'),
      fieldtype: 'HTML',
      fieldname: 'select_attachments',
    },
  ];

  // add from if user has access to multiple email accounts
  const email_accounts = frappe.boot.email_accounts.filter(account => {
    return (
      !in_list(
        ['All Accounts', 'Sent', 'Spam', 'Trash'],
        account.email_account,
      ) && account.enable_outgoing
    );
  });

  if (email_accounts.length) {
    fields.unshift({
      label: __('From'),
      fieldtype: 'Select',
      reqd: 1,
      fieldname: 'sender',
      options: email_accounts.map(function (e) {
        return e.email_id;
      }),
    });
  }

  return fields;
}

function toggle_more_options(show_options) {
  show_options = show_options || dialog.fields_dict.more_options.df.hidden;
  dialog.set_df_property('more_options', 'hidden', !show_options);

  const label = frappe.utils.icon(show_options ? 'up-line' : 'down');
  dialog.get_field('option_toggle_button').set_label(label);
}

function fileToBlobAndUrl(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      const blob = new Blob([reader.result], { type: file.type });
      const blobUrl = URL.createObjectURL(blob);
      resolve(blobUrl);
    };

    reader.onerror = error => {
      reject(error);
    };

    reader.readAsArrayBuffer(file);
  });
}

function getFileUpload(frm) {
  if (frm.doc.type_of_security_details) {
    const keys = Object.keys(JSON.parse(frm.doc.type_of_security_details));
    keys.forEach(key => {
      files_upload[key] = [];
    });
  }

  if (frm.doc.exception_tracking_details) {
    const keys = Object.keys(JSON.parse(frm.doc.exception_tracking_details));
    keys.forEach(key => {
      files_upload_exception[key] = [];
    });
  }
  if (frm.doc.post_execution_details) {
    const keys = Object.keys(JSON.parse(frm.doc.post_execution_details));
    keys.forEach(key => {
      files_upload_exception[key] = [];
    });
  }
}

function addFiles(assetName, files, details, s3ButtonIndex) {
  const file_name = [];
  if (!details[assetName][s3ButtonIndex].file_url) {
    files.forEach(element => {
      file_name.push(element.name);
    });
    files_upload[assetName].push({
      files,
      file_name,
      assetName,
      s3ButtonIndex,
    });
  } else {
    files_upload[assetName] = [];
    files.forEach(element => {
      file_name.push(element.name);
    });
    files_upload[assetName].push({
      files,
      file_name,
      assetName,
      s3ButtonIndex,
    });
  }
}

function addFilesException(assetName, file, details, s3ButtonIndex) {
  if (!details[assetName][s3ButtonIndex].file) {
    files_upload_exception[assetName].push({
      file,
      file_name: file.name,
      assetName,
      s3ButtonIndex,
    });
  } else {
    files_upload_exception[assetName] = [];
    files_upload_exception[assetName].push({
      file,
      file_name: file.name,
      assetName,
      s3ButtonIndex,
    });
  }
}

function fileRemove(assetName, index, s3_index) {
  const foundIndex = files_upload[assetName].findIndex(
    asset => asset.s3ButtonIndex === index,
  );
  if (foundIndex !== -1) {
    files_upload[assetName][foundIndex].file_name.splice(s3_index, 1);
    files_upload[assetName][foundIndex].files.splice(s3_index, 1);
    const filteredAssets = files_upload[assetName].filter(
      asset => asset.files.length > 0,
    );
    files_upload[assetName] = filteredAssets;
  }
}
function fileRemoveException(assetName, index) {
  const foundIndex = files_upload_exception[assetName].findIndex(
    element => element.s3ButtonIndex === index,
  );
  if (foundIndex !== -1) {
    files_upload_exception[assetName].splice(foundIndex, 1);
  }
}

function uploadAssetFile(row, frm, file, assetName, detailsKey, index) {
  detailsKey !== 'exception_tracking_details' ? (row.file_name = []) : '';
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    const api =
      detailsKey !== 'exception_tracking_details'
        ? '/api/method/trusteeship_platform.custom_methods.post_execution_s3_upload'
        : '/api/method/trusteeship_platform.custom_methods.pre_execution_and_exception_tracking_s3_upload';

    const formData = new FormData();
    formData.append('file', file);
    formData.append('file_name', file.name);
    formData.append('row', JSON.stringify(row));
    formData.append('docname', frm.doc.name);
    formData.append('doctype', frm.doc.doctype);
    formData.append('assetName', assetName);
    formData.append('detailsKey', detailsKey);
    formData.append('index', index);
    xhr.open('POST', api, true);
    xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          frappe.dom.unfreeze();
          resolve(1);
        } else {
          reject(xhr.statusText);
        }
      } else {
        frappe.dom.unfreeze();
        try {
          const response = JSON.parse(xhr.responseText);
          const errorMessage = JSON.parse(response?._server_messages);
          if (errorMessage) {
            frappe.msgprint(errorMessage, 'Error');
          }
        } catch (error) {
          if (xhr.status === 413) {
            displayHtmlDialog();
          }
        }
      }
    };
    xhr.send(formData);
  });
}

function displayHtmlDialog() {
  if (frappe.boot.max_file_size) {
    frappe.msgprint({
      title: __('File too big'),
      indicator: 'red',
      message: __(
        'File size exceeded the maximum allowed size of ' +
          bytesToMB(frappe.boot.max_file_size),
      ),
    });
  }
}

function bytesToMB(bytes) {
  if (bytes === 0) return '0 MB';

  const mb = bytes / (1024 * 1024);
  return `${mb.toFixed(0)} MB`;
}

function uploadAssetFileOthers(row_name, frm, file, doctype) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    const api =
      '/api/method/trusteeship_platform.custom_methods.invit_post_execution_s3_upload';
    const formData = new FormData();
    formData.append('file', file);
    formData.append('row_name', row_name);
    formData.append('docname', frm.doc.name);
    formData.append('doctype', doctype);
    formData.append('file_name', file.name);
    xhr.open('POST', api, true);
    xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          frappe.dom.unfreeze();
          resolve(1);
        } else {
          frappe.dom.unfreeze();
          reject(xhr.statusText);
        }
      } else {
        frappe.dom.unfreeze();
        try {
          const response = JSON.parse(xhr.responseText);
          const errorMessage = JSON.parse(response?._server_messages);
          if (errorMessage) {
            frappe.msgprint(errorMessage, 'Error');
          }
        } catch (error) {
          if (xhr.status === 413) {
            displayHtmlDialog();
          }
        }
      }
    };
    xhr.send(formData);
  });
}

async function preExecutionS3Upload(frm, assetDetails, doctype, field) {
  frappe.dom.freeze();
  const docnames = Object.keys(assetDetails);
  for (let i = 0; i < docnames.length; i++) {
    const element = docnames[i];
    const file = assetDetails[element].file;
    await uploadAssetFileOthers(
      assetDetails[element].row_name,
      frm,
      file,
      doctype,
    );
  }
  frappe.dom.freeze();
  sebi_application_files = {};
  stages_of_offer_document_files = {};
  document_based_compliances = {};
  event_based_compliances = {};
  frm.refresh_fields();
  frm.reload_doc().then(() => {
    frappe.dom.unfreeze();
    setTimeout(() => {
      frm.scroll_to_field(field);
    }, 100);
  });

  frappe.show_alert(
    {
      message: __('Uploaded'),
      indicator: 'green',
    },
    5,
  );
}
