from frappe import _


def get_data():
    return {
        "fieldname": "isin_name",
        "transactions": [
            {
                "label": _(""),
                "items": ["Canopi ISIN Monitoring"],
            },
        ],
    }
