# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import re

import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname


class InitialInvestorISIN(Document):
    def before_insert(self):
        match = re.search("[0-9]+", make_autoname("ISIN.#"))
        self.sr_no = match.group(0)

    def after_insert(self):
        make_address(self)


def make_address(args):
    address_doc = frappe.new_doc("Address")
    if args.get("address_title"):
        address_doc.address_type = args.get("address_type")
        address_doc.address_title = args.get("address_title")
        address_doc.address_line1 = args.get("address_line_1")
        address_doc.address_line2 = args.get("address_line_2")
        address_doc.city = args.get("citytown")
        address_doc.state = args.get("stateprovince")
        address_doc.country = args.get("country")
        address_doc.pincode = args.get("postal_code")
        address_doc.phone = args.get("phone_number")
        address_doc.email_id = args.get("email_id")
        address_doc.append(
            "links",
            {
                "link_doctype": "Initial Investor ISIN",
                "link_name": args.get("name"),
            },  # noqa: 501
        )
        address_doc.insert()
