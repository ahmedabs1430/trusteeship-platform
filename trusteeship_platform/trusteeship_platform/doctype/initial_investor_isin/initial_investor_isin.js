// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs */
frappe.ui.form.on('Initial Investor ISIN', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    frm.set_query('documentation_name', filter => {
      return {
        filters: [['Canopi Documentation', 'product_name', '=', 'DTE']],
      };
    });
    if (!frm.doc.__islocal) {
      $("button[data-doctype='Canopi ISIN Monitoring']").removeClass('hidden');
    }
    hideConnectionAddBtn(frm);
  },
  documentation_name: function (frm) {
    set_fields(frm);
  },
});

function hideConnectionAddBtn(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype: 'Canopi ISIN Monitoring',
      filters: [['isin_name', '=', frm.doc.name]],
    },
    freeze: true,
    callback: r => {
      if (r.message.length > 0) {
        $('.open-notification').hide();
        $("button[data-doctype='Canopi ISIN Monitoring']").addClass('hidden');
      }
    },
  });
}

function set_fields(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_name: frm.doc.documentation_name,
      doc_type: 'Canopi Documentation',
    },
    freeze: true,
    callback: r => {
      const cnp_customer_code = r.message.cnp_customer_code;
      const client_name = r.message.client_name;
      const operations = r.message.operations;

      frm.doc.cnp_customer_code = cnp_customer_code;
      frm.doc.client_name = client_name;
      frm.doc.operations = operations;
      frm.refresh_field('cnp_customer_code');
      frm.refresh_field('client_name');
      frm.refresh_field('operations');
    },
  });
}
