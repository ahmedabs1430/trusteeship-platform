// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs, showActivities, showNotes */
/* eslint-env jquery */

frappe.ui.form.on('Canopi FA Details', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
      showActivities(frm);
      showNotes(frm);
    });

    if (!frm.doc.__islocal) {
      let titleElement = $(
        "[class='document-link-badge'][data-doctype='Customer']",
      );
      titleElement = titleElement[0];
      titleElement.getElementsByTagName('a')[0].innerText = 'Borrower';
    }

    getFacilityAgent(frm);
  },

  from_date: frm => {
    calculateNoOfDays(frm);
  },

  upto_date: frm => {
    calculateNoOfDays(frm);
  },
});

function calculateNoOfDays(frm) {
  if (frm.doc.from_date && frm.doc.upto_date) {
    const diffDate = new Date(
      new Date(frm.doc.upto_date) - new Date(frm.doc.from_date),
    );
    const days = diffDate / (1000 * 3600 * 24);
    frm.doc.no_of_days = days;
    frm.refresh_field('no_of_days');
  } else {
    frm.doc.no_of_days = '';
    frm.refresh_field('no_of_days');
  }
}

function getFacilityAgent(frm) {
  const prevRoute = frappe.get_prev_route();
  if (
    prevRoute.includes('Canopi Transaction Details-FA') &&
    frm.doc.__islocal &&
    !frm.doc.facility_agent
  ) {
    frm.set_value('facility_agent', prevRoute[2]);
  }
}
