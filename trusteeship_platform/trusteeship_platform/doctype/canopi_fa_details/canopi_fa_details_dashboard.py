from frappe import _


def get_data():
    return {
        "heatmap": True,
        "fieldname": "cnp_fa_details",
        "transactions": [
            {
                "label": _("Loans"),
                "items": ["Loan"],
            },
            {
                "label": _("Lender Details"),
                "items": ["Canopi Lender"],
            },
            {
                "label": _("Borrower Details"),
                "items": ["Customer"],
            },
        ],
    }
