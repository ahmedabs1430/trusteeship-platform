// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe */
frappe.ui.form.on('Canopi Document Code', {
  refresh: function (frm) {
    setFieldProperty(frm);
  },
  is_dta: frm => {
    makeTransactionDocumentsReadonly(frm);
  },
  common_documents_checklist: frm => {
    setFieldProperty(frm);
  },
});

function setFieldProperty(frm) {
  if (frm.doc.common_documents_checklist) {
    frm.toggle_reqd('product', false);
  } else {
    frm.toggle_reqd('product', true);
  }
}

function makeTransactionDocumentsReadonly(frm) {
  frm.set_df_property(
    'transaction_documents_checklist',
    'read_only',
    frm.doc.is_dta,
  );
  if (frm.doc.is_dta) {
    frm.set_value('transaction_documents_checklist', 1);
  }
}
