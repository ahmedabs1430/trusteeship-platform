// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs, showActivities, showNotes, user */
let transaction_documents = [];
let transaction_documents_post_execution = [];
let security_documents_pre_execution = [];
let security_documents_post_execution = [];
let condition_precedent_part_a = [];
let condition_precedent_part_b = [];
let condition_subsequent = [];
let other_documents_pre_execution = [];
let other_documents_post_execution = [];
let pre_execution_documents = [];
let post_execution_documents = [];
let initial_investors_isin = [];
let is_send_for_approval_visible = false;
frappe.ui.form.on('Canopi Documentation', {
  refresh: async function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    if (frm.doc.__islocal === 1) {
      frm.set_value('ops_servicing_rm', user);
    }

    show_activities_and_notes(frm);
    configure_annexure_b_checklist_table(frm);

    set_facility_amt_fix_height(frm);
    hideAddApprovalButton();
    await Promise.all([
      fileUpload(frm),
      load_transaction_documents(frm),
      load_transaction_documents_post_execution(frm),
      load_security_documents_pre_execution(frm),
      load_security_documents_post_execution(frm),
      load_condition_precedent_part_a(frm),
      load_condition_precedent_part_b(frm),
      load_condition_subsequent(frm),
      load_other_documents_pre_execution(frm),
      load_other_documents_post_execution(frm),
      load_pre_execution_documents(frm),
      load_post_execution_documents(frm),
      load_initial_investors_isin(frm),
    ]);
    set_intro(frm);
    add_actions_button(frm);
    load_loa_table(frm);
    set_serial_no_filter(frm);

    frm.trigger('ops_servicing_rm');
    frm.trigger('tl_representative');
    if (frm.doc.__islocal === 1) {
      frm.trigger('operations');
    }

    set_read_only_fields(frm);
    add_percent_pill(frm);
    headerButtons(frm);

    // Annexure B
    if (frm.doc.product_name?.toUpperCase() === 'DTE') {
      configureAnnexureBApprovalBtn(frm);
      configureViewAttachAnnexureBBtn(frm);
      configureAttachAnnexureBBtn(frm);
      renderDownloadAnnexureBBtn(frm);
      collapseAnnextureB(frm);
    }
    if (frm.doc.annexure_b_status === 'Approved') {
      setReadonlyIsinFields(frm);
    }
  },
  attach_annexure_b: frm => {
    uploadAnnexureBToS3(frm);
  },

  annexure_b_authorised_signatory: frm => {
    setFullName(frm);
  },

  validate: frm => {
    validate_checker_and_maker(frm);
  },

  onload: frm => {
    set_registered_address_filter(frm);
    set_corporate_address_filter(frm);
  },

  cnp_registered_address: frm => {
    set_corporate_address_filter(frm);
  },

  view_annexure_b: frm => {
    getAnnexureB(frm);
  },

  annexure_b_approval: frm => {
    if (
      frm.doc.annexure_b_status === 'Draft' ||
      frm.doc.annexure_b_status === 'Rejected'
    ) {
      sendAnnexureBApproval(frm);
    }
  },

  operations: async function (frm) {
    if (frm.doc.operations) {
      await populate_form(frm);
    }
  },

  face_value: frm => {
    frm.trigger('letter_sub');
  },

  nominal_value: frm => {
    frm.trigger('letter_sub');
  },

  letter_from: frm => {
    frm.trigger('letter_sub');
    append_letter_from_in_serial_no(frm);
  },

  letter_sub: async frm => {
    const response = await get_multiple_docs(frm);
    set_letter_sub(
      frm,
      response.message.item,
      response.message.canopi_pricing_policy,
      response.message.opportunity,
    );
  },

  loa_body_date: async frm => {
    frm.trigger('loa_body');
  },

  loa_email_date: async frm => {
    frm.trigger('loa_body');
  },

  loa_attorney_date: async frm => {
    frm.trigger('loa_body');
  },

  loa_body: async frm => {
    const response = await get_doc('Item', frm.doc.product_name);
    set_loa_letter_body(frm, response);
  },

  ops_servicing_rm: frm => {
    validate_checker_and_maker(frm);
  },

  tl_representative: frm => {
    validate_checker_and_maker(frm);
  },

  isin_button: frm => {
    window.open(
      window.location.origin +
        '/app/data-import/new-data-import-1?reference_doctype=Initial Investor ISIN',
    );
  },
  issue_size_in_units: function (frm) {
    calculateTotalIssueSize(frm);
  },
  green_shoe_value: function (frm) {
    calculateTotalIssueSize(frm);
  },
});

function setReadonlyIsinFields(frm) {
  frm.toggle_display('isin_button', false);
  frm.set_df_property('date_of_improspectus', 'read_only', true);
  frm.set_df_property('issue_size_in_units', 'read_only', true);
  frm.set_df_property('green_shoe_option', 'read_only', true);
  frm.set_df_property('green_shoe_value', 'read_only', true);
  frm.set_df_property(
    'total_issue_size_in_cr_including_green_shoe',
    'read_only',
    true,
  );
  frm.set_df_property('strpps', 'read_only', true);
  frm.set_df_property('number_of_strpps', 'read_only', true);
  frm.set_df_property('purpose_of_issue', 'read_only', true);
  frm.set_df_property('nature_of_instrument', 'read_only', true);
  frm.set_df_property('name_of_depository', 'read_only', true);
  frm.set_df_property('name_of_registrar', 'read_only', true);
  frm.set_df_property('min_security_cover_required', 'read_only', true);
  frm.set_df_property('issue_opening_date', 'read_only', true);
  frm.set_df_property('issue_close_date', 'read_only', true);
  frm.set_df_property('deemed_date_of_allotment', 'read_only', true);
  frm.set_df_property('reminder_mail_to_be_sent', 'read_only', true);
  frm.set_df_property('followup_mail_to_be_sent', 'read_only', true);
  frm.set_df_property('rating_agency_email_id', 'read_only', true);
  frm.set_df_property('fundtrf_dateescrow_to_issuer', 'read_only', true);
  frm.fields_dict.isin_list_update_section.collapse(false);
}

function setFullName(frm) {
  if (frm.doc.annexure_b_authorised_signatory) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.annexure_b_authorised_signatory,
        doc_type: 'User',
      },
      freeze: true,
      callback: r => {
        frm.set_value('annexure_b_signatory_full_name', r.message.full_name);
      },
    });
  }
}

function hideAddApprovalButton() {
  $("button[data-doctype='Canopi Approval']").addClass('hidden');
}

function configure_annexure_b_checklist_table(frm) {
  frm.get_field('annexure_b_checklist').grid.cannot_add_rows = true;
  frm.refresh_fields();
}

function show_activities_and_notes(frm) {
  frappe.require('assets/trusteeship_platform/js/common.js', () => {
    showActivities(frm);
    showNotes(frm);
  });
}

function set_facility_amt_fix_height(frm) {
  const field = frm.get_field('cnp_facility_amt');
  if (field.input) {
    field.input.style.height = 'calc(1.5em + 0.75rem + 2px)';
    field.input.style.resize = 'none';
  }
}

function approval_dialog(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.create_approval',
    args: {
      docname: frm.doc.name,
      is_post_execution: 1,
    },
    freeze: true,
    callback: r => {
      frappe.set_route('Form', 'Canopi Approval', r.message);
    },
  });
}

function check_is_send_for_approval_visible() {
  const post_execution_data = [
    ...transaction_documents_post_execution,
    ...security_documents_post_execution,
    ...other_documents_post_execution,
    ...post_execution_documents,
  ];
  is_send_for_approval_visible = post_execution_data.some(
    x => x.status === 'Submit to Checker',
  );
}

function set_intro(frm) {
  check_is_send_for_approval_visible();
  frm.set_intro('');
  if (frm.doc.docstatus === 0) {
    frm.show_submit_message();
  }
  if (is_send_for_approval_visible) {
    frm.set_intro(
      'Few Documents are Submitted, please send them for approval to the checker',
    );
  }
}

function add_actions_button(frm) {
  if (is_send_for_approval_visible) {
    frm.add_custom_button(
      'Send For Approval',
      () => {
        approval_dialog(frm);
      },
      'Actions',
    );
  } else if (frm.custom_buttons['Send For Approval']) {
    frm.custom_buttons['Send For Approval'].hide();
  }
}

function set_read_only_fields(frm) {
  if (!frm.doc.__islocal) {
    frm.set_df_property('client_name', 'read_only', true);
    frm.set_df_property('operations', 'read_only', true);
  }
}

async function getAnnexureB(frm) {
  if (!frm.doc.__islocal && frm.doc.product_name?.toUpperCase() === 'DTE') {
    if (frm.is_dirty()) {
      await frm.save();
    }
    await frappe.call({
      method:
        'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.update_annexure_b_draft_status',
      args: { docname: frm.doc.name },
      freeze: true,
    });
    frm.reload_doc();
    window.open(
      `/api/method/trusteeship_platform.custom_methods.download_documentation_annexure_b?docname=${frm.doc.name}`,
      '_blank',
    );
  }
}

function add_loa_btn(frm) {
  if (!frm.doc.__islocal) {
    if (frm.is_dirty())
      frappe.throw('Please save the document befor generating LOA');

    const selected_documents = [];
    $(`.${frm.doc.name}_loa_select_row`).each(function (i) {
      if (this.checked) {
        selected_documents.push({
          name: $(`.${frm.doc.name}_loa_select_row`)[i].value,
          doctype: $(`.${frm.doc.name}_loa_select_row_doctype`)[i].value,
          document_name: $(`.${frm.doc.name}_loa_select_row_document_name`)[i]
            .value,
        });
      }
    });

    if (selected_documents.length === 0)
      frappe.throw('Please Select atleast one Document');

    let msg = 'Are you sure you want to proceed with these document names?';
    let preview_files = '';
    // adding selected document list in message
    selected_documents.forEach((x, i) => {
      if (i === 0) msg = msg + '<ul>';
      msg = msg + `<li>${x.document_name}</li>`;
      preview_files = preview_files + `<li>${x.document_name}</li>`;
      if (i === selected_documents.length - 1) msg = msg + '</ul>';
    });
    showLOAPreview(frm, preview_files, selected_documents);
  }
}

const GenerationLOA = (frm, selected_doc) => {
  const doclist = JSON.stringify(selected_doc);
  frappe.call({
    method:
      'trusteeship_platform.custom_methods.update_documentation_loa_details',
    args: { docname: frm.doc.name, doclist },
    freeze: true,
    callback: () => frm.reload_doc(),
  });
};
const getPreviewDialog = (frm, action, selected_doc) => {
  const dialog = new frappe.ui.Dialog({
    title: __('Send for Approval'),
    size: 'large',
    fields: [
      {
        label: 'Preview',
        fieldname: 'preview_html',
        fieldtype: 'HTML',
      },
    ],
    primary_action: () => action(frm, selected_doc) || dialog.hide(),
    primary_action_label: __('Confirm'),
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      dialog.hide();
    },
  });
  return dialog;
};
const showLOAPreview = (frm, preview_files, selected_doc) => {
  const previewDialog = getPreviewDialog(frm, GenerationLOA, selected_doc);

  // initialize preview wrapper
  const $previewWrapper = previewDialog.get_field('preview_html').$wrapper;
  $previewWrapper.html(
    `<div>
    <div class="print-preview">
    <div class="print-format"></div>
    </div>
    <div class="page-break-message text-muted text-center text-medium margin-top"></div>
    </div>`,
  );
  frm.doc.preview_files = preview_files;
  frappe.call({
    method: 'frappe.www.printview.get_html_and_style',
    args: {
      doc: frm.doc,
      print_format: 'Letter Of Authority',
      no_letterhead: 1,
    },
    callback: function (r) {
      if (!r.exc) {
        $previewWrapper.find('.print-format').html(r.message.html);
        const style = `
        .print-format { box-shadow: 0px 0px 5px rgba(0,0,0,0.2); padding: 0.30in; min-height: 80vh; }
        .print-preview { min-height: 0px; }
        .modal-dialog { width: 720px; }`;

        frappe.dom.set_style(style, 'custom-print-style');
        previewDialog.show();
      }
    },
  });
};

function set_letter_sub(frm, item, pricing_policy, opportunity) {
  if (item.item_code?.toUpperCase() === 'DTE') {
    frm.set_value(
      'letter_subject',
      'Issue of Debenture Trustee ' +
        (pricing_policy.no_of_securities === 1 ? 'secured' : 'unsecured') +
        `, ${
          opportunity.cnp_type_of_security
        } Debentures, of the face value of Rs. ${
          frm.doc.face_value ? frm.doc.face_value : 0
        } each, of the aggregating to Rs. ${
          frm.doc.nominal_value ? frm.doc.nominal_value : 0
        } by ${opportunity.customer_name} Limited`,
    );
  }
}

function set_default_letter_body(frm, item_name) {
  /* eslint-disable */

  if (item_name === 'Debenture Trustee')
    frm.doc.letter_body = `We, the debenture trustee to the above mentioned issue state as follows:
  <ol>
  <br>
    <li>We have examined documents pertaining to the creation of charge over assets of Issuer.</li> <br>

    <li> On the basis of such examination and of the discussions with the Issuer, its directors and other officers, other agencies and of independent verification of the various relevant documents, WE CONFIRM that: </li></ol>

  We confirm that:
  <ol type="a">
  <br>
  <li>The Issuer has created charge over its assets in favour of debenture trustee as per terms of offer document or private placement memorandum/ information memorandum and debenture trustee agreement.</li>
  <li>Issuer has executed the debenture trust deed as per terms of offer document or private placement memorandum/ information memorandum and debenture trustee agreement. </li>
  <li>The Issuer has given an undertaking that charge shall be registered with Sub-registrar, Registrar of Companies, Central Registry of Securitization Asset Reconstruction and Security Interest (CERSAI), Depository etc., as applicable, within 30 days of creation of charge. </li>
  </ol>

  We have satisfied ourselves about the ability of the Issuer to service the debt securities.`;
  frm.refresh_field('letter_body');
  /* eslint-enable */
}

function set_loa_letter_body(frm, item) {
  if (item) {
    frm.doc.loa_body = `We are appointed as ${
      item.item_name
    } for the captioned company, pertaining to which transaction documents are proposed to be executed at ${moment(
      frm.doc.loa_body_date,
    ).format('DD MMM YYYY')}.
    <br>
    <br>
    <b>Particulars of Documents:</b> As per email dated ${moment(
      frm.doc.loa_email_date,
    ).format('DD MMM YYYY')}.
    <br>
    <br>
    In this connection and in furtherance to the power of attorney dated ${moment(
      frm.doc.loa_attorney_date,
    ).format(
      'DD MMM YYYY',
    )}, we hereby authorise you to sign, execute/accept and do necessary acts in relation to the execution / registration of the aforesaid documents.
    <br>
    Kindly update the ATSL officer concerned on close of execution of the documents.`;

    frm.refresh_field('loa_body');
  }
}

function set_registered_address_filter(frm) {
  frm.set_query('cnp_registered_address', function () {
    return {
      filters: [
        ['Address', 'is_your_company_address', '=', 1],
        ['Address', 'address_type', '=', 'Registered Office'],
      ],
    };
  });
}

function set_corporate_address_filter(frm) {
  if (frm.doc.cnp_registered_address) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: frm.doc.cnp_registered_address, doc_type: 'Address' },
      freeze: true,
      callback: r => {
        frm.toggle_reqd('cnp_corporate_address', true);
        frm.toggle_display('cnp_corporate_address', true);
        frm.set_df_property(
          'cnp_corporate_address',
          'label',
          'Corporate Office',
        );
        frm.set_query('cnp_corporate_address', function () {
          return {
            filters: [
              ['Address', 'is_your_company_address', '=', 1],
              ['Address', 'address_type', '=', 'Corporate Office'],
            ],
          };
        });
      },
    });
  } else {
    frm.toggle_display('cnp_corporate_address', false);
    frm.toggle_reqd('cnp_corporate_address', false);
    frm.doc.cnp_corporate_address = '';
  }
}

async function populate_form(frm) {
  const response = await get_multiple_docs(frm);
  set_letter_sub(
    frm,
    response.message.item,
    response.message.canopi_pricing_policy,
    response.message.opportunity,
  );
  set_default_letter_body(frm, response.message.item.item_name);
  set_loa_letter_body(frm, response.message.item);

  // set opportunity values
  frm.set_value('opportunity', response.message.opportunity.name);
  frm.set_value('customer_name', response.message.opportunity.customer_name);
  frm.set_value('company', response.message.opportunity.customer_name);
  frm.set_value(
    'cnp_facility_amt',
    response.message.opportunity.cnp_facility_amt,
  );
  if (response.message.opportunity.cnp_product === 'DTE') {
    frm.set_value(
      'nature_of_instrument',
      response.message.opportunity.cnp_type_of_security,
    );
  }

  frm.set_value(
    'cnp_facility_amt_in_words',
    response.message.opportunity.cnp_facility_amt_in_words,
  );
  frm.set_value(
    'cnp_facility_amt_in_words_millions',
    response.message.opportunity.cnp_facility_amt_in_words_millions,
  );

  // set atsl mandate list values
  frm.set_value('client_name', response.message.atsl_mandate_list.mandate_name);
  frm.set_value(
    'product_name',
    response.message.atsl_mandate_list.product_name,
  );
  frm.set_value(
    'cnp_customer_code',
    response.message.atsl_mandate_list.cnp_customer_code,
  );

  // set quotation values
  if (response.message.quotation) {
    frm.set_value('quotation', response.message.quotation.name);
    frm.set_value(
      'cnp_registered_address',
      response.message.quotation.cnp_registered_address,
    );
  }
  frm.set_value(
    'ops_servicing_rm',
    response.message.atsl_mandate_list.ops_servicing_rm,
  );
  frm.set_value(
    'tl_representative',
    response.message.atsl_mandate_list.tl_representative,
  );

  // set item values
  if (frm.doc.product_name?.toUpperCase() === 'DTE') {
    frm.doc.annexure_b_checklist = [];
    response.message.item.cnp_annexure_b_checklist.forEach(x => {
      frm.add_child('annexure_b_checklist', {
        particulars: x.particulars,
      });
    });
    frm.refresh_fields();
  }

  frappe.db
    .exists('Canopi Serial Number', 'Canopi Documentation-Annexure B')
    .then(exists => {
      if (exists) {
        frm.set_value('serial_no_link', 'Canopi Documentation-Annexure B');
      }
    });
  frappe.db
    .exists('Canopi Serial Number', 'Canopi Documentation-Letter Of Authority')
    .then(exists => {
      if (exists) {
        frm.set_value(
          'loa_serial_link',
          'Canopi Documentation-Letter Of Authority',
        );
      }
    });
}

function get_multiple_docs(frm) {
  const docs = [
    {
      doctype: 'ATSL Mandate List',
      docname: frm.doc.operations,
    },
    {
      doctype: 'Item',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'product_name',
    },
    {
      doctype: 'Canopi Pricing Policy',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: ['opportunity', 'product_name'],
    },
    {
      doctype: 'Quotation',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'quotation',
    },
    {
      doctype: 'Opportunity',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'opportunity',
    },
    {
      doctype: 'Customer',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'cnp_customer_code',
    },
  ];
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.get_docs_with_linked_childs',
    args: { docs },
    freeze: true,
  });
}

async function get_doc_list(frm, doctype, filters = [], order_by = '') {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype,
      filters,
      order_by,
    },
    freeze: true,
  });

  return response.message;
}

async function get_doc(doctype, docname) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: doctype,
      doc_name: docname,
    },
  });

  return response.message;
}

async function load_transaction_documents(frm) {
  $(frm.fields_dict.transaction_documents_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() !== 'REIT' &&
    frm.doc.product_name?.toUpperCase() !== 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      transaction_documents = await get_doc_list(
        frm,
        'Canopi Transaction Documents',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );
      if (transaction_documents.length === 0) {
        transaction_documents = [];
      }

      $(frm.fields_dict.transaction_documents_html.wrapper).empty();
      frm.set_df_property(
        'transaction_documents_html',
        'options',
        frappe.render_template('documentation_transaction_documents', {
          doc: transaction_documents,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('transaction_documents_html');
      $('.' + frm.doc.name + '-transaction-documents-edit-row').on(
        'click',
        function () {
          transaction_documents_dialog(
            frm,
            transaction_documents[this.id],
            'Edit Transaction Documents - Pre-Execution',
          );
        },
      );
      $('.transaction_documents_chkAll').click(function () {
        if (this.checked) {
          $('.transaction_documents_chk').prop('checked', true);
        } else {
          $('.transaction_documents_chk').prop('checked', false);
        }
      });
      add_transaction_documents(frm);
      delete_transaction_documents(frm);
      add_upload_events(frm);
      add_download_events(frm);
      initiate_post_execution(frm);
      transaction_documents_email(frm);
      transaction_documents_send_to_legal(frm);
    }
  }
}

function add_transaction_documents(frm) {
  $('.' + frm.doc.name + '-transaction_documents_add').on('click', function () {
    transaction_documents_dialog(
      frm,
      {},
      'Add Transaction Documents - Pre-Execution',
    );
  });
}

function delete_transaction_documents(frm) {
  $('.' + frm.doc.name + '-transaction_documents_delete').on(
    'click',
    function () {
      $('input[class="transaction_documents_chk"]:checked').each(function () {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
          args: {
            doctype: 'Canopi Transaction Documents',
            docname: this.value,
          },
          freeze: true,

          callback: async r => {
            delete transaction_documents_pre_execution_files[this.value];
            await load_transaction_documents(frm);
            load_loa_table(frm);
            frm.reload_doc().then(() => {
              setTimeout(() => {
                frm.scroll_to_field('transaction_documents_html');
              }, 100);
            });
          },
        });
      });
    },
  );
}

function initiate_post_execution(frm) {
  $('.' + frm.doc.name + '-initiate-post-execution').on('click', function () {
    const doc_name_arr = {};
    // eslint-disable-next-line no-unused-vars
    let i = 0;
    let is_error = false;
    $('input[class="transaction_documents_chk"]:checked').each(function () {
      doc_name_arr[this.value] = this.value;
      const retain_waive = $('#' + this.value + '_retain_waive').text();
      const status = $('#' + this.value + '_status').text();
      const send_to_legal = $('#' + this.value + '_send_to_legal').text();
      const purpose_of_legal = $('#' + this.value + '_purpose_of_legal').text();
      const remarks_by_legal = $('#' + this.value + '_remarks_by_legal').text();
      const approval_if_required = $(
        '#' + this.value + '_approval_if_required',
      ).text();
      if (status !== 'LOA Generated') {
        frappe.msgprint({
          title: __('Error'),
          indicator: 'orange',
          message: __(
            'Can not Initiate post execution, Status must be LOA Generated.',
          ),
        });
        is_error = true;
      }
      if (send_to_legal !== 'Yes') {
        frappe.msgprint({
          title: __('Error'),
          indicator: 'orange',
          message: __(
            'Can not Initiate post execution, Send To Legal must be Yes',
          ),
        });
        is_error = true;
      }
      if (purpose_of_legal === 'Vetting' && remarks_by_legal === '') {
        frappe.msgprint({
          title: __('Error'),
          indicator: 'orange',
          message: __(
            'Can not Initiate post execution, Remarks by Legal Require',
          ),
        });
        is_error = true;
      }
      if (retain_waive === 'Waive') {
        frappe.msgprint({
          title: __('Error'),
          indicator: 'orange',
          message: __(
            'Can not Initiate post execution for documents that are Waived.',
          ),
        });
        is_error = true;
      } else if (approval_if_required === 'Yes') {
        frappe.msgprint({
          title: __('Error'),
          indicator: 'orange',
          message: __('Can not Initiate post execution, approval required'),
        });
        is_error = true;
      }
      i++;
    });

    if (!is_error && Object.keys(doc_name_arr).length > 0) {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.initiate_post_execution',
        args: {
          doctype: 'Canopi Transaction Documents',
          newdoctype: 'Canopi Transaction Documents Post Execution',
          docname: JSON.stringify(doc_name_arr),
          documentation_name: frm.doc.name,
        },
        freeze: true,

        callback: async r => {
          await load_transaction_documents(frm);
          await load_transaction_documents_post_execution(frm);
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('transaction_documents_post_execution_html');
            }, 100);
          });
        },
      });
    }
  });
}

let s3_attachments = {};
let after_email_update_field = '';
let child_doctype = '';
function transaction_documents_email(frm) {
  $('.' + frm.doc.name + '-email').on('click', function () {
    email_dialog('');
    s3_attachments = {};
    after_email_update_field = 'send_email';
    child_doctype = 'Canopi Transaction Documents';
    $('input[class="transaction_documents_chk"]:checked').each(function () {
      const attachment = [];
      attachment.name = this.value;
      attachment.file_name = $('#' + this.value + '_file_name').val();
      attachment.file_url = $('#' + this.value + '_file_path').val();
      s3_attachments[this.value] = {};
      s3_attachments[this.value].file_name = $(
        '#' + this.value + '_file_name',
      ).val();
      s3_attachments[this.value].file_url = $(
        '#' + this.value + '_file_path',
      ).val();
      render_attachment_rows(attachment);
    });
  });
}

function transaction_documents_send_to_legal(frm) {
  $('.' + frm.doc.name + '-send-to-legal').on('click', function () {
    const selected_documents = [];
    $('input[class="transaction_documents_chk"]:checked').each(function () {
      if ($('#' + this.value + '_send_to_legal').text() === 'Yes') {
        frappe.throw(
          'Some of the selected documents have already been sent to legal',
        );
      }
      if ($('#' + this.value + '_file_name').val() === '') {
        frappe.throw('Please Upload File For Selected Rows');
      }
      selected_documents.push({
        name: this.value,
      });
    });
    if (selected_documents.length === 0)
      frappe.throw('Please Select atleast one Document');

    sendToLegalDialog(
      frm,
      selected_documents,
      'Canopi Transaction Documents',
      'transaction_documents_html',
    );
  });
}

function sendToLegalDialog(frm, docs, child_doctype, scroll_to_field) {
  const d = new frappe.ui.Dialog({
    title: 'Select purpose of Sending to Legal',
    fields: [
      {
        label: 'Vetting',
        fieldname: 'vetting',
        fieldtype: 'Check',
        reqd: 0,
        onchange: function (e) {
          const field = d.get_field('noting');
          if (this.value === 1) {
            field.df.read_only = true;
            field.refresh();
          } else {
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Noting',
        fieldname: 'noting',
        fieldtype: 'Check',
        reqd: 0,
        onchange: function (e) {
          const field = d.get_field('vetting');
          if (this.value === 1) {
            field.df.read_only = true;
            field.refresh();
          } else {
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      if (values.vetting === 0 && values.noting === 0) {
        frappe.throw('Please Select One Option');
      }
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.send_to_legal',
        args: {
          docname: frm.doc.name,
          child_doctype,
          docs,
          vetting: values.vetting,
          noting: values.noting,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field(scroll_to_field);
            }, 100);
          });
        },
      });
      d.hide();
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
}

function add_download_events(frm) {
  $('.' + frm.doc.name + '-s3-download').on('click', function () {
    const s3_key = this.id;
    if (s3_key) {
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: s3_key,
      });
    }
  });
}

function add_upload_events(frm) {
  add_transaction_documents_attach_file_event(frm);
  add_transaction_documents_upload_all_event(frm);
}

function transaction_documents_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          transaction_documents_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const documentName = d.get_field('document_name');
              documentName.value = r.message?.description;
              documentName.refresh();

              const isDta = d.get_field('is_dta');
              isDta.value = r.message.is_dta;
              isDta.refresh();

              const is_for_document_execution = d.get_field(
                'is_for_document_execution',
              );
              is_for_document_execution.value =
                r.message.is_for_document_execution;
              is_for_document_execution.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Is DTA',
        reqd: 0,
        read_only: 1,
        fieldname: 'is_dta',
        fieldtype: 'Check',
        default: rowData.is_dta,
      },
      {
        label: 'Is for Document Execution in Transaction Documents',
        reqd: 0,
        read_only: 1,
        fieldname: 'is_for_document_execution',
        fieldtype: 'Check',
        default: rowData.is_for_document_execution,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();

              field = d.get_field('for_post_execution');
              field.df.read_only = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'For Post Execution',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'for_post_execution',
        fieldtype: 'Select',
        default: rowData.for_post_execution,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_transaction_documents',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          remarks_by_legal: values.remarks_by_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          for_post_execution: values.for_post_execution,
          is_dta: values.is_dta,
          is_for_document_execution: values.is_for_document_execution,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_transaction_documents(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('transaction_documents_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.refresh();
    field = d.get_field('for_post_execution');
    field.value = 'No';
    field.df.read_only = true;
    field.refresh();
  } else {
    let field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = true;
      field.refresh();
    } else {
      field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = false;
      field.refresh();
    }
  }
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

function add_transaction_documents_preview_event(frm) {
  $('.' + frm.doc.name + '-preview-file').on('click', async function () {
    frappe.dom.freeze();
    const rowname = $(this).closest('tr').attr('id');
    const blobUrl = await fileToBlobAndUrl(
      transaction_documents_pre_execution_files[rowname].file,
    );
    frappe.dom.unfreeze();
    window.open(blobUrl, '_blank');
  });
}

function add_transaction_documents_remove_file_event(frm) {
  $('.' + frm.doc.name + '-remove-file').on('click', function () {
    const rowname = $(this).closest('tr').attr('id');
    $(this).parent().html('');
    delete transaction_documents_pre_execution_files[rowname];
  });
}

let transaction_documents_pre_execution_files = {};
function add_transaction_documents_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-attach-file').on('click', function () {
    s3_button_index = this.id;
    row_name = $(this).closest('tr').attr('id');
    $('.' + frm.doc.name + '-attach-input')[s3_button_index].click();
  });

  $('.' + frm.doc.name + '-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      $('#f_' + row_name).html(
        '<a><span class="' +
          frm.doc.name +
          '-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
          file[0].name +
          '</a>&nbsp;<a class="' +
          frm.doc.name +
          '-remove-file"><span>' +
          frappe.utils.icon('close', 'sm') +
          '</span></a>',
      );

      // details[s3_button_index]['a45'] = file[0].name
      if (row_name !== 'undefined')
        transaction_documents_pre_execution_files[row_name] = {};
      transaction_documents_pre_execution_files[row_name].file = file[0];
      transaction_documents_pre_execution_files[row_name].file_name =
        file[0].name;
      transaction_documents_pre_execution_files[row_name].row_name = row_name;
      add_transaction_documents_preview_event(frm);
      add_transaction_documents_remove_file_event(frm);
    }
  });
}

function add_transaction_documents_upload_all_event(frm) {
  $('.' + frm.doc.name + '-upload-all').on('click', () => {
    if (Object.keys(transaction_documents_pre_execution_files).length > 0) {
      detailsS3Upload(
        frm,
        transaction_documents_pre_execution_files,
        'Canopi Transaction Documents',
        'transaction_documents_html',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

async function load_transaction_documents_post_execution(frm) {
  $(frm.fields_dict.transaction_documents_post_execution_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() !== 'REIT' &&
    frm.doc.product_name?.toUpperCase() !== 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      transaction_documents_post_execution = await get_doc_list(
        frm,
        'Canopi Transaction Documents Post Execution',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (transaction_documents_post_execution.length === 0) {
        transaction_documents_post_execution = [];
      }

      $(
        frm.fields_dict.transaction_documents_post_execution_html.wrapper,
      ).empty();
      frm.set_df_property(
        'transaction_documents_post_execution_html',
        'options',
        frappe.render_template(
          'documentation_transaction_documents_post_execution',
          {
            doc: transaction_documents_post_execution,
            id: frm.doc.name,
            docstatus: frm.doc.docstatus,
          },
        ),
      );

      frm.refresh_field('transaction_documents_post_execution_html');
      $(
        '.' + frm.doc.name + '-transaction_documents_post_execution-edit-row',
      ).on('click', function () {
        transaction_documents_post_execution_dialog(
          frm,
          transaction_documents_post_execution[this.id],
          'Edit Transaction Documents - Post-Execution',
        );
      });

      $('.transaction_documents_post_execution_chkAll').click(function () {
        if (this.checked) {
          $('.transaction_documents_post_execution_chk').prop('checked', true);
        } else {
          $('.transaction_documents_post_execution_chk').prop('checked', false);
        }
      });
      transaction_documents_post_execution_add(frm);
      transaction_documents_post_execution_delete(frm);
      transaction_documents_post_execution_upload_events(frm);
      transaction_documents_post_execution_download_events(frm);
      transaction_documents_post_execution_email(frm);
      transaction_documents_post_execution_submit_to_checker(frm);
      transaction_documents_post_execution_send_to_legal(frm);
    }
  }
}

function transaction_documents_post_execution_add(frm) {
  $('.' + frm.doc.name + '-transaction_documents_post_execution-add').on(
    'click',
    function () {
      transaction_documents_post_execution_dialog(
        frm,
        {},
        'Add Transaction Documents - Post-Execution',
      );
    },
  );
}

function transaction_documents_post_execution_delete(frm) {
  $('.' + frm.doc.name + '-transaction_documents_post_execution-delete').on(
    'click',
    function () {
      $('input[class="transaction_documents_post_execution_chk"]:checked').each(
        function () {
          transaction_documents_post_execution.forEach(x => {
            if (x.name === this.value && x.status !== 'Open')
              frappe.throw('Please select row of <b>Open</b> status.');
          });
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Transaction Documents Post Execution',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete transaction_documents_post_execution_files[this.value];
              await load_transaction_documents_post_execution(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field(
                    'transaction_documents_post_execution_html',
                  );
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function transaction_documents_post_execution_download_events(frm) {
  $(
    '.' + frm.doc.name + '-transaction_documents_post_execution-s3-download',
  ).on('click', function () {
    const s3_key = this.id;
    if (s3_key) {
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: s3_key,
      });
    }
  });
}

function transaction_documents_post_execution_upload_events(frm) {
  transaction_documents_post_execution_attach_file_event(frm);
  transaction_documents_post_execution_upload_all_event(frm);
}

let transaction_documents_post_execution_files = {};
function transaction_documents_post_execution_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $(
    '.' + frm.doc.name + '-transaction_documents_post_execution-attach-file',
  ).on('click', function () {
    s3_button_index = this.id;
    row_name = $(this).closest('tr').attr('id');
    $(
      '.' + frm.doc.name + '-transaction_documents_post_execution-attach-input',
    )[s3_button_index].click();
  });

  $(
    '.' + frm.doc.name + '-transaction_documents_post_execution-attach-input',
  ).change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';

      $('#transaction_documents_post_execution_f_' + row_name).html(
        '<a><span class="' +
          frm.doc.name +
          // eslint-disable-next-line max-len
          '-transaction_documents_post_execution-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
          file[0].name +
          '</a>&nbsp;<a class="' +
          frm.doc.name +
          '-transaction_documents_post_execution-remove-file"><span>' +
          frappe.utils.icon('close', 'sm') +
          '</span></a>',
      );

      // details[s3_button_index]['a45'] = file[0].name
      if (row_name !== 'undefined')
        transaction_documents_post_execution_files[row_name] = {};
      transaction_documents_post_execution_files[row_name].file = file[0];
      transaction_documents_post_execution_files[row_name].file_name =
        file[0].name;
      transaction_documents_post_execution_files[row_name].row_name = row_name;
      add_transaction_documents_post_execution_preview_event(frm);
      add_transaction_documents_post_execution_remove_file_event(frm);
    }
  });
}

function add_transaction_documents_post_execution_preview_event(frm) {
  $(
    '.' + frm.doc.name + '-transaction_documents_post_execution-preview-file',
  ).on('click', async function () {
    frappe.dom.freeze();
    const rowname = $(this).closest('tr').attr('id');
    const blobUrl = await fileToBlobAndUrl(
      transaction_documents_post_execution_files[rowname].file,
    );
    frappe.dom.unfreeze();
    window.open(blobUrl, '_blank');
  });
}

function add_transaction_documents_post_execution_remove_file_event(frm) {
  $(
    '.' + frm.doc.name + '-transaction_documents_post_execution-remove-file',
  ).on('click', function () {
    const rowname = $(this).closest('tr').attr('id');
    $(this).parent().html('');
    delete transaction_documents_post_execution_files[rowname];
  });
}

function transaction_documents_post_execution_upload_all_event(frm) {
  $('.' + frm.doc.name + '-transaction_documents_post_execution-upload-all').on(
    'click',
    () => {
      if (Object.keys(transaction_documents_post_execution_files).length > 0) {
        detailsS3Upload(
          frm,
          transaction_documents_post_execution_files,
          'Canopi Transaction Documents Post Execution',
          'transaction_documents_post_execution_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function transaction_documents_post_execution_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          transaction_documents_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const documentName = d.get_field('document_name');
              documentName.value = r.message?.description;
              documentName.refresh();

              const isDta = d.get_field('is_dta');
              isDta.value = r.message.is_dta;
              isDta.refresh();

              const is_for_document_execution = d.get_field(
                'is_for_document_execution',
              );
              is_for_document_execution.value =
                r.message.is_for_document_execution;
              is_for_document_execution.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Is DTA',
        reqd: 0,
        read_only: 1,
        fieldname: 'is_dta',
        fieldtype: 'Check',
        default: rowData.is_dta,
      },
      {
        label: 'Is for Document Execution in Transaction Documents',
        reqd: 0,
        read_only: 1,
        fieldname: 'is_for_document_execution',
        fieldtype: 'Check',
        default: rowData.is_for_document_execution,
      },
      {
        label: 'Execution Version',
        reqd: 1,
        fieldname: 'execution_version',
        fieldtype: 'Data',
        default: rowData.execution_version,
      },
      {
        label: 'Single Signing Required',
        reqd: 0,
        fieldname: 'single_signing',
        fieldtype: 'Check',
        default: rowData.single_signing,
        onchange: function (e) {
          if (this.value === 1) {
            let field = d.get_field('signing_party');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = true;
            field.df.hidden = false;
            field.refresh();
          } else {
            let field = d.get_field('signing_party');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = false;
            field.df.hidden = true;
            field.refresh();
          }
        },
      },
      {
        label: 'Signing Party - ATSL',
        reqd: 1,
        options: ['ATSL', 'POA'],
        onchange: function (e) {
          if (this.value === 'ATSL') {
            let field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else {
            let field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            d.fields_dict.signatory_poa.get_query = function () {
              return {
                query:
                  'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.get_canopi_poa',
              };
            };
          }
        },
        fieldname: 'signing_party',
        fieldtype: 'Select',
        default: rowData.signing_party,
      },
      {
        label: 'Signatory-ATSL',
        fieldtype: 'Link',
        options: 'User',
        fieldname: 'signatory',
        default: rowData.signatory,
      },
      {
        label: 'Signatory-POA',
        fieldtype: 'Link',
        hidden: 1,
        options: 'Canopi POA',
        fieldname: 'signatory_poa',
        default: rowData.signatory_poa,
      },
      {
        label: 'Signatory Name - Party',
        hidden: 1,
        fieldtype: 'Data',
        fieldname: 'signatory_text',
        default: rowData.signatory_text,
      },
      {
        label: 'Custody Location',
        reqd: 1,
        fieldtype: 'Link',
        options: 'Location',
        fieldname: 'location',
        default: rowData.location,
      },
      {
        label: 'Execution Date',
        reqd: 0,
        fieldname: 'execution_date',
        fieldtype: 'Date',
        default: rowData.execution_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Status',
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Data',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
      {
        label: 'Document Custody with',
        options: ['ATSL', 'Axis Bank', 'Client', 'Third Party'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Axis Bank') {
            let field = d.get_field('axis_bank_branch_name');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('third_party_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else if (this.value === 'Third Party') {
            let field = d.get_field('third_party_name');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('axis_bank_branch_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else {
            let field = d.get_field('third_party_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('axis_bank_branch_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          }
        },
        fieldname: 'document_custody_with',
        fieldtype: 'Select',
        default: rowData.document_custody_with,
      },
      {
        label: 'Axis Bank Branch Name',
        fieldtype: 'Data',
        fieldname: 'axis_bank_branch_name',
        depends_on: 'eval:doc.document_custody_with==="Axis Bank"',
        default: rowData.axis_bank_branch_name,
      },
      {
        label: 'Third Party Name',
        fieldtype: 'Data',
        fieldname: 'third_party_name',
        depends_on: 'eval:doc.document_custody_with==="Third Party"',
        default: rowData.third_party_name,
      },
      {
        label: 'Document Type',
        options: ['Original', 'CTC', 'Photocopy'],
        fieldname: 'document_type',
        fieldtype: 'Select',
        default: rowData.document_type,
      },
      {
        label: 'Executed Location',
        fieldtype: 'Text',
        fieldname: 'executed_location',
        default: rowData.executed_location,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_transaction_documents_post_execution',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          execution_version: values.execution_version,
          single_signing: values.single_signing,
          signing_party: values.signing_party,
          signatory: values.signatory,
          signatory_poa: values.signatory_poa,
          signatory_text: values.signatory_text,
          location: values.location,
          execution_date: values.execution_date,
          comments: values.comments,
          status: values.status,
          send_email: values.send_email,
          approval_if_required: values.approval_if_required,
          send_to_legal: values.send_to_legal,
          remarks_by_legal: values.remarks_by_legal,
          document_custody_with: values.document_custody_with,
          axis_bank_branch_name: values.axis_bank_branch_name,
          third_party_name: values.third_party_name,
          document_type: values.document_type,
          executed_location: values.executed_location,
          is_dta: values.is_dta,
          is_for_document_execution: values.is_for_document_execution,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_transaction_documents_post_execution(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('transaction_documents_post_execution_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  if (rowData.document_custody_with === 'Axis Bank') {
    let field = d.get_field('axis_bank_branch_name');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('third_party_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else if (rowData.document_custody_with === 'Third Party') {
    let field = d.get_field('third_party_name');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('axis_bank_branch_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else {
    let field = d.get_field('third_party_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('axis_bank_branch_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  }
  if (rowData.single_signing === 1) {
    let field = d.get_field('signing_party');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_poa');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.reqd = true;
    field.df.hidden = false;
    field.refresh();
  } else {
    let field = d.get_field('signing_party');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.reqd = false;
    field.df.hidden = true;
    field.refresh();

    if (rowData.signing_party) {
      if (rowData.signing_party === 'ATSL') {
        let field = d.get_field('signatory');
        field.df.hidden = false;
        field.df.reqd = true;
        field.refresh();
        field = d.get_field('signatory_poa');
        field.df.hidden = true;
        field.df.reqd = false;
        field.refresh();
      } else {
        let field = d.get_field('signatory');
        field.df.hidden = true;
        field.df.reqd = false;
        field.refresh();
        field = d.get_field('signatory_poa');
        field.df.hidden = false;
        field.df.reqd = true;
        field.refresh();
      }
    }
  }
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
  d.show();
}

function transaction_documents_post_execution_email(frm) {
  $('.' + frm.doc.name + '-transaction_documents_post_execution-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Transaction Documents Post Execution';
      $('input[class="transaction_documents_post_execution_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' +
              this.value +
              '_transaction_documents_post_execution_file_name',
          ).val();
          attachment.file_url = $(
            '#' +
              this.value +
              '_transaction_documents_post_execution_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' +
              this.value +
              '_transaction_documents_post_execution_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' +
              this.value +
              '_transaction_documents_post_execution_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function transaction_documents_post_execution_submit_to_checker(frm) {
  $(
    '.' +
      frm.doc.name +
      '-transaction_documents_post_execution-submit-to-checker',
  ).unbind();
  $(
    '.' +
      frm.doc.name +
      '-transaction_documents_post_execution-submit-to-checker',
  ).on('click', function () {
    const docnames = $(
      'input[class="transaction_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Transaction Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    docnames.forEach(doc => {
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            post.status !== 'Open' &&
            post.status.indexOf('Rejected') === -1,
        )
      ) {
        frappe.throw(
          'Some of the selected documents have already been sent for approval',
        );
      }
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            (post.file_name === null || post.file_name === ''),
        )
      ) {
        frappe.throw('Please Upload File for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post => post.name === doc.docname && post.execution_version === '',
        )
      ) {
        frappe.throw('Execution Version mandatory for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            post.signing_party === 'ATSL' &&
            post.signatory === '',
        )
      ) {
        frappe.throw('Signatory mandatory for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            post.signing_party === 'POA' &&
            post.signatory_poa === '',
        )
      ) {
        frappe.throw('Signatory mandatory for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            post.single_signing === '1' &&
            post.signatory_text === '',
        )
      ) {
        frappe.throw('Signatory Party mandatory for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post => post.name === doc.docname && post.location === '',
        )
      ) {
        frappe.throw('Location mandatory for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            (post.execution_date === null || post.execution_date === ''),
        )
      ) {
        frappe.throw('Execution Date mandatory for selected documents');
      }
      if (
        transaction_documents_post_execution.some(
          post =>
            post.name === doc.docname &&
            post.purpose_of_legal === 'Vetting' &&
            post.remarks_by_legal === '',
        )
      ) {
        frappe.throw('Remarks by legal mandatory for selected documents');
      }
    });
    update_documents_status(docnames, 'Submit to Checker').then(async () => {
      approval_dialog(frm);
    });
  });
}

async function update_documents_status(docnames, status = 'Open') {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.update_documents_status',
    args: {
      docs: docnames,
      status,
      is_post_execution: 1,
    },
    freeze: true,
  });

  return response;
}
function transaction_documents_post_execution_send_to_legal(frm) {
  $(
    '.' + frm.doc.name + '-transaction_documents_post_execution-send-to-legal',
  ).on('click', function () {
    const selected_documents = [];
    $('input[class="transaction_documents_post_execution_chk"]:checked').each(
      function () {
        const status = $(
          '#' + this.value + '_transaction_documents_post_execution_status',
        ).text();
        const send_to_legal = $(
          '#' +
            this.value +
            '_transaction_documents_post_execution_send_to_legal',
        ).text();
        if (status === 'Submit to Checker') {
          frappe.throw(
            'Some of the selected documents have been submit to checker',
          );
        }
        if (send_to_legal === 'Yes') {
          frappe.throw(
            'Some of the selected documents have already been sent to legal',
          );
        }
        if (
          $(
            '#' +
              this.value +
              '_transaction_documents_post_execution_file_name',
          ).val() === ''
        ) {
          frappe.throw('Please Upload File For Selected Rows');
        }
        selected_documents.push({
          name: this.value,
        });
      },
    );
    if (selected_documents.length === 0)
      frappe.throw('Please Select atleast one Document');

    sendToLegalDialog(
      frm,
      selected_documents,
      'Canopi Transaction Documents Post Execution',
      'transaction_documents_post_execution_html',
    );
  });
}

// pre execution documents
async function load_pre_execution_documents(frm) {
  $(frm.fields_dict.pre_execution_documents_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() === 'REIT' ||
    frm.doc.product_name?.toUpperCase() === 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      pre_execution_documents = await get_doc_list(
        frm,
        'Canopi Pre Execution Documents',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );
      if (pre_execution_documents.length === 0) {
        pre_execution_documents = [];
      }

      $(frm.fields_dict.pre_execution_documents_html.wrapper).empty();
      frm.set_df_property(
        'pre_execution_documents_html',
        'options',
        frappe.render_template('documentation_pre_execution_documents', {
          doc: pre_execution_documents,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('pre_execution_documents_html');
      $('.' + frm.doc.name + '-pre-execution-documents-edit-row').on(
        'click',
        function () {
          pre_execution_documents_dialog(
            frm,
            pre_execution_documents[this.id],
            'Edit Transaction Documents - Pre-Execution',
          );
        },
      );
      $('.pre_execution_documents_chkAll').click(function () {
        if (this.checked) {
          $('.pre_execution_documents_chk').prop('checked', true);
        } else {
          $('.pre_execution_documents_chk').prop('checked', false);
        }
      });
      add_pre_execution_documents(frm);
      delete_pre_execution_documents(frm);
      add_pre_execution_documents_upload_events(frm);
      add_pre_execution_documents_download_events(frm);
      pre_execution_documents_initiate_post_execution(frm);
      pre_execution_documents_email(frm);
      pre_execution_documents_send_to_legal(frm);
    }
  }
}

function add_pre_execution_documents(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents_add').on(
    'click',
    function () {
      pre_execution_documents_dialog(
        frm,
        {},
        'Add Transaction Documents - Pre-Execution',
      );
    },
  );
}

function delete_pre_execution_documents(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents_delete').on(
    'click',
    function () {
      $('input[class="pre_execution_documents_chk"]:checked').each(function () {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
          args: {
            doctype: 'Canopi Pre Execution Documents',
            docname: this.value,
          },
          freeze: true,

          callback: async r => {
            delete pre_execution_documents_files[this.value];
            await load_pre_execution_documents(frm);
            load_loa_table(frm);
            frm.reload_doc().then(() => {
              setTimeout(() => {
                frm.scroll_to_field('pre_execution_documents_html');
              }, 100);
            });
          },
        });
      });
    },
  );
}

function pre_execution_documents_initiate_post_execution(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents-initiate-post-execution').on(
    'click',
    function () {
      const doc_name_arr = {};
      // eslint-disable-next-line no-unused-vars
      let i = 0;
      let is_error = false;
      $('input[class="pre_execution_documents_chk"]:checked').each(function () {
        doc_name_arr[this.value] = this.value;
        const retain_waive = $('#' + this.value + '_retain_waive').text();
        const approval_if_required = $(
          '#' + this.value + '_approval_if_required',
        ).text();
        if (retain_waive === 'Waive') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution for documents that are Waived.',
            ),
          });
          is_error = true;
        } else if (approval_if_required === 'Yes') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __('Can not Initiate post execution, approval required'),
          });
          is_error = true;
        }
        i++;
      });

      if (!is_error && Object.keys(doc_name_arr).length > 0) {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.initiate_post_execution',
          args: {
            doctype: 'Canopi Pre Execution Documents',
            newdoctype: 'Canopi Post Execution Documents',
            docname: JSON.stringify(doc_name_arr),
            documentation_name: frm.doc.name,
          },
          freeze: true,

          callback: async r => {
            await load_pre_execution_documents(frm);
            await load_post_execution_documents(frm);
            frm.reload_doc().then(() => {
              setTimeout(() => {
                frm.scroll_to_field('post_execution_documents_html');
              }, 100);
            });
          },
        });
      }
    },
  );
}

function pre_execution_documents_email(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Pre Execution Documents';
      $('input[class="pre_execution_documents_chk"]:checked').each(function () {
        const attachment = [];
        attachment.name = this.value;
        attachment.file_name = $(
          '#' + this.value + '_pre_execution_documents_file_name',
        ).val();
        attachment.file_url = $(
          '#' + this.value + '_pre_execution_documents_file_path',
        ).val();
        s3_attachments[this.value] = {};
        s3_attachments[this.value].file_name = $(
          '#' + this.value + '_pre_execution_documents_file_name',
        ).val();
        s3_attachments[this.value].file_url = $(
          '#' + this.value + '_pre_execution_documents_file_path',
        ).val();
        render_attachment_rows(attachment);
      });
    },
  );
}

function pre_execution_documents_send_to_legal(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents-send-to-legal').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_to_legal';
      child_doctype = 'Canopi Pre Execution Documents';
      $('input[class="pre_execution_documents_chk"]:checked').each(function () {
        const attachment = [];
        attachment.name = this.value;
        attachment.file_name = $(
          '#' + this.value + '_pre_execution_documents_file_name',
        ).val();
        attachment.file_url = $(
          '#' + this.value + '_pre_execution_documents_file_path',
        ).val();
        s3_attachments[this.value] = {};
        s3_attachments[this.value].file_name = $(
          '#' + this.value + '_pre_execution_documents_file_name',
        ).val();
        s3_attachments[this.value].file_url = $(
          '#' + this.value + '_pre_execution_documents_file_path',
        ).val();
        render_attachment_rows(attachment);
      });
    },
  );
}

function add_pre_execution_documents_download_events(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function add_pre_execution_documents_upload_events(frm) {
  add_pre_execution_documents_attach_file_event(frm);
  add_pre_execution_documents_upload_all_event(frm);
}

function pre_execution_documents_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          document_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();

              field = d.get_field('for_post_execution');
              field.df.read_only = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'For Post Execution',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'for_post_execution',
        fieldtype: 'Select',
        default: rowData.for_post_execution,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_pre_execution_documents',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          for_post_execution: values.for_post_execution,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_pre_execution_documents(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('pre_execution_documents_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.refresh();
    field = d.get_field('for_post_execution');
    field.value = 'No';
    field.df.read_only = true;
    field.refresh();
  } else {
    const field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      let field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = true;
      field.refresh();
    } else {
      let field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = false;
      field.refresh();
    }
  }
}

function add_pre_execution_documents_preview_event(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        pre_execution_documents_files[rowname].file,
      );
      frappe.dom.unfreeze();

      window.open(blobUrl, '_blank');
    },
  );
}

function add_pre_execution_documents_remove_file_event(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete pre_execution_documents_files[rowname];
    },
  );
}

// eslint-disable-next-line no-empty-pattern
let pre_execution_documents_files = ({} = {});
function add_pre_execution_documents_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-pre_execution_documents-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-pre_execution_documents-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-pre_execution_documents-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#pre_execution_documents_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-pre_execution_documents-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-pre_execution_documents-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // pre_execution_documents_files[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          pre_execution_documents_files[row_name] = {};
        pre_execution_documents_files[row_name].file = file[0];
        pre_execution_documents_files[row_name].file_name = file[0].name;
        pre_execution_documents_files[row_name].row_name = row_name;
        add_pre_execution_documents_preview_event(frm);
        add_pre_execution_documents_remove_file_event(frm);
      }
    },
  );
}

function add_pre_execution_documents_upload_all_event(frm) {
  $('.' + frm.doc.name + '-pre_execution_documents_upload-all').on(
    'click',
    () => {
      if (Object.keys(pre_execution_documents_files).length > 0) {
        detailsS3Upload(
          frm,
          pre_execution_documents_files,
          'Canopi Pre Execution Documents',
          'pre_execution_documents_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}
// post excecution documents
async function load_post_execution_documents(frm) {
  $(frm.fields_dict.post_execution_documents_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() === 'REIT' ||
    frm.doc.product_name?.toUpperCase() === 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      post_execution_documents = await get_doc_list(
        frm,
        'Canopi Post Execution Documents',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (post_execution_documents.length === 0) {
        post_execution_documents = [];
      }

      $(frm.fields_dict.post_execution_documents_html.wrapper).empty();
      frm.set_df_property(
        'post_execution_documents_html',
        'options',
        frappe.render_template('documentation_post_execution_documents', {
          doc: post_execution_documents,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('post_execution_documents_html');
      $('.' + frm.doc.name + '-post_execution_documents-edit-row').on(
        'click',
        function () {
          post_execution_documents_dialog(
            frm,
            post_execution_documents[this.id],
            'Edit Transaction Documents - Post-Execution',
          );
        },
      );

      $('.post_execution_documents_chkAll').click(function () {
        if (this.checked) {
          $('.post_execution_documents_chk').prop('checked', true);
        } else {
          $('.post_execution_documents_chk').prop('checked', false);
        }
      });
      post_execution_documents_add(frm);
      post_execution_documents_delete(frm);
      post_execution_documents_upload_events(frm);
      post_execution_documents_download_events(frm);
      post_execution_documents_email(frm);
      post_execution_documents_submit_to_checker(frm);
      post_execution_documents_send_to_legal(frm);
    }
  }
}

function post_execution_documents_add(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-add').on(
    'click',
    function () {
      post_execution_documents_dialog(
        frm,
        {},
        'Add Transaction Documents - Post-Execution',
      );
    },
  );
}

function post_execution_documents_delete(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-delete').on(
    'click',
    function () {
      $('input[class="post_execution_documents_chk"]:checked').each(
        function () {
          post_execution_documents.forEach(x => {
            if (x.name === this.value && x.status !== 'Open')
              frappe.throw('Please select row of <b>Open</b> status.');
          });
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Post Execution Documents',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete post_execution_documents_files[this.value];
              await load_post_execution_documents(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('post_execution_documents_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function post_execution_documents_download_events(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function post_execution_documents_upload_events(frm) {
  post_execution_documents_attach_file_event(frm);
  post_execution_documents_upload_all_event(frm);
}

let post_execution_documents_files = {};
function post_execution_documents_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-post_execution_documents-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-post_execution_documents-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-post_execution_documents-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#post_execution_documents_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-post_execution_documents-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-post_execution_documents-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          post_execution_documents_files[row_name] = {};
        post_execution_documents_files[row_name].file = file[0];
        post_execution_documents_files[row_name].file_name = file[0].name;
        post_execution_documents_files[row_name].row_name = row_name;
        add_post_execution_documents_preview_event(frm);
        add_post_execution_documents_remove_file_event(frm);
      }
    },
  );
}

function add_post_execution_documents_preview_event(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        post_execution_documents_files[rowname].file,
      );
      frappe.dom.unfreeze();

      window.open(blobUrl, '_blank');
    },
  );
}

function add_post_execution_documents_remove_file_event(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete post_execution_documents_files[rowname];
    },
  );
}

function post_execution_documents_upload_all_event(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-upload-all').on(
    'click',
    () => {
      if (Object.keys(post_execution_documents_files).length > 0) {
        detailsS3Upload(
          frm,
          post_execution_documents_files,
          'Canopi Post Execution Documents',
          'post_execution_documents_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function post_execution_documents_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          document_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Execution Version',
        reqd: 0,
        fieldname: 'execution_version',
        fieldtype: 'Data',
        default: rowData.execution_version,
      },
      {
        label: 'Signing Party',
        reqd: 0,
        options: ['ATSL', 'Party'],
        onchange: function (e) {
          if (this.value === 'ATSL') {
            let field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = false;
            field.df.hidden = true;
            field.refresh();
          } else {
            let field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
          }
        },
        fieldname: 'signing_party',
        fieldtype: 'Select',
        default: rowData.signing_party,
      },
      {
        label: 'Signatory',
        fieldtype: 'Link',
        options: 'User',
        fieldname: 'signatory',
        default: rowData.signatory,
      },
      {
        label: 'Signatory',
        hidden: 1,
        fieldtype: 'Data',
        fieldname: 'signatory_text',
        default: rowData.signatory_text,
      },
      {
        label: 'Custody Location',
        fieldtype: 'Link',
        options: 'Location',
        fieldname: 'location',
        default: rowData.location,
      },
      {
        label: 'Execution Date',
        reqd: 0,
        fieldname: 'execution_date',
        fieldtype: 'Date',
        default: rowData.execution_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Status',
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Data',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_post_execution_documents',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          execution_version: values.execution_version,
          signing_party: values.signing_party,
          signatory: values.signatory,
          signatory_text: values.signatory_text,
          location: values.location,
          execution_date: values.execution_date,
          comments: values.comments,
          status: values.status,
          send_email: values.send_email,
          approval_if_required: values.approval_if_required,
          send_to_legal: values.send_to_legal,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_post_execution_documents(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('post_execution_documents_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (rowData.signing_party === 'ATSL') {
    let field = d.get_field('signatory');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else {
    let field = d.get_field('signatory');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
  }
}

function post_execution_documents_email(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Post Execution Documents';
      $('input[class="post_execution_documents_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_post_execution_documents_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_post_execution_documents_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_post_execution_documents_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_post_execution_documents_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function post_execution_documents_submit_to_checker(frm) {
  $(
    '.' + frm.doc.name + '-post_execution_documents-submit-to-checker',
  ).unbind();
  $('.' + frm.doc.name + '-post_execution_documents-submit-to-checker').on(
    'click',
    function () {
      const docnames = $('input[class="post_execution_documents_chk"]:checked')
        .map(function () {
          return {
            doctype: 'Canopi Post Execution Documents',
            docname: $(this).val(),
          };
        })
        .get();
      if (docnames.length === 0) frappe.throw('Select atleast one row');
      docnames.forEach(doc => {
        if (
          post_execution_documents.some(
            post => post.name === doc.docname && post.status !== 'Open',
          )
        ) {
          frappe.throw(
            'Some of the selected documents have already been sent for approval',
          );
        }
      });
      update_documents_status(docnames, 'Submit to Checker').then(async () => {
        await load_post_execution_documents(frm);
        set_intro(frm);
        add_actions_button(frm);
      });
    },
  );
}

function post_execution_documents_send_to_legal(frm) {
  $('.' + frm.doc.name + '-post_execution_documents-send-to-legal').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_to_legal';
      child_doctype = 'Canopi Post Execution Documents';
      $('input[class="post_execution_documents_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_post_execution_documents_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_post_execution_documents_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_post_execution_documents_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_post_execution_documents_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}
// Document Based Compliances
async function load_initial_investors_isin(frm) {
  $(frm.fields_dict.initial_investors_isin_html.wrapper).empty();
  if (frm.doc.product_name?.toUpperCase() === 'DTE') {
    if (!frm.doc.__islocal) {
      initial_investors_isin = await get_doc_list(
        frm,
        'Initial Investor ISIN',
        [['operations', '=', frm.doc.operations]],
        'creation asc',
      );

      if (initial_investors_isin.length === 0) {
        initial_investors_isin = [];
      }

      $(frm.fields_dict.initial_investors_isin_html.wrapper).empty();
      frm.set_df_property(
        'initial_investors_isin_html',
        'options',
        frappe.render_template('documentation_initial_investors_isin', {
          doc: initial_investors_isin,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('initial_investors_isin_html');
    }
  }
}

// Condition Precedent
async function load_condition_precedent_part_a(frm) {
  $(frm.fields_dict.condition_precedent_part_a_html.wrapper).empty();
  if (frm.doc.product_name?.toUpperCase() === 'FA') {
    if (!frm.doc.__islocal) {
      const condition_precedent_part_a_new = [];
      const st = [];
      condition_precedent_part_a = await get_doc_list(
        frm,
        'Canopi Condition Precedent Part A',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (condition_precedent_part_a.length === 0) {
        condition_precedent_part_a = [];
      } else {
        for (let i = 0; i < condition_precedent_part_a.length; i++) {
          let category = condition_precedent_part_a[i].category;
          if (category === '') category = 'Others';
          if (condition_precedent_part_a_new[category] === undefined) {
            condition_precedent_part_a_new[category] = [];
          }
          if (st.indexOf(category) === -1) {
            st.push(category);
          }
          condition_precedent_part_a_new[category].push(
            condition_precedent_part_a[i],
          );
        }
      }
      $(frm.fields_dict.condition_precedent_part_a_html.wrapper).empty();
      frm.set_df_property(
        'condition_precedent_part_a_html',
        'options',
        frappe.render_template('documentation_condition_precedent_part_a', {
          st,
          doc_data: condition_precedent_part_a_new,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('condition_precedent_part_a_html');
      $('.' + frm.doc.name + '-condition_precedent_part_a-edit-row').on(
        'click',
        function () {
          condition_precedent_part_a_dialog(
            frm,
            condition_precedent_part_a[this.id],
            'Edit Condition Precedent - PART A',
          );
        },
      );

      $('.condition_precedent_part_a_chkAll').click(function () {
        if (this.checked) {
          $('.condition_precedent_part_a_chk').prop('checked', true);
        } else {
          $('.condition_precedent_part_a_chk').prop('checked', false);
        }
      });
      condition_precedent_part_a_add(frm);
      condition_precedent_part_a_delete(frm);
      condition_precedent_part_a_upload_events(frm);
      condition_precedent_part_a_download_events(frm);
      condition_precedent_part_a_email(frm);
      condition_precedent_part_a_send_to_legal(frm);
    }
  }
}

function condition_precedent_part_a_add(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-add').on(
    'click',
    function () {
      condition_precedent_part_a_dialog(
        frm,
        {},
        'Add Condition Precedent - Part A',
      );
    },
  );
}

function condition_precedent_part_a_delete(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-delete').on(
    'click',
    function () {
      $('input[class="condition_precedent_part_a_chk"]:checked').each(
        function () {
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Condition Precedent Part A',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete condition_precedent_part_a_files[this.value];
              await load_condition_precedent_part_a(frm);
              load_loa_table(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('condition_precedent_part_a_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function condition_precedent_part_a_download_events(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function condition_precedent_part_a_upload_events(frm) {
  condition_precedent_part_a_attach_file_event(frm);
  condition_precedent_part_a_upload_all_event(frm);
}

let condition_precedent_part_a_files = {};
function condition_precedent_part_a_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-condition_precedent_part_a-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-condition_precedent_part_a-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-condition_precedent_part_a-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#condition_precedent_part_a_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-condition_precedent_part_a-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-condition_precedent_part_a-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          condition_precedent_part_a_files[row_name] = {};
        condition_precedent_part_a_files[row_name].file = file[0];
        condition_precedent_part_a_files[row_name].file_name = file[0].name;
        condition_precedent_part_a_files[row_name].row_name = row_name;
        add_condition_precedent_part_a_preview_event(frm);
        add_condition_precedent_part_a_remove_file_event(frm);
      }
    },
  );
}

function add_condition_precedent_part_a_preview_event(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        condition_precedent_part_a_files[rowname].file,
      );
      frappe.dom.unfreeze();

      window.open(blobUrl, '_blank');
    },
  );
}

function add_condition_precedent_part_a_remove_file_event(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete condition_precedent_part_a_files[rowname];
    },
  );
}

function condition_precedent_part_a_upload_all_event(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-upload-all').on(
    'click',
    () => {
      if (Object.keys(condition_precedent_part_a_files).length > 0) {
        detailsS3Upload(
          frm,
          condition_precedent_part_a_files,
          'Canopi Condition Precedent Part A',
          'condition_precedent_part_a_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function condition_precedent_part_a_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },

      {
        label: 'Category',
        options: [
          'Borrower',
          'Guarantor and Sponsor',
          'Project Documents',
          'Finance Documents',
          'Transaction Security',
          'Legal opinions',
          'Advisors’ Reports',
          'Insurance',
          'Environment',
          'Land and Authorisations',
          'Equity',
          'Other documents and evidence',
        ],
        reqd: 1,
        fieldname: 'category',
        fieldtype: 'Select',
        default: rowData.category,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          condition_precedent_part_a_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Small Text',
        default: rowData.document_name,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_condition_precedent_part_a',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          category: values.category,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_condition_precedent_part_a(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('condition_precedent_part_a_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();

  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.value = 'Yes';
    field.refresh();
  } else {
    let field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();
    } else {
      field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();
    }
  }
}

function condition_precedent_part_a_email(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Condition Precedent Part A';
      $('input[class="condition_precedent_part_a_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_condition_precedent_part_a_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_condition_precedent_part_a_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_condition_precedent_part_a_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_condition_precedent_part_a_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function condition_precedent_part_a_send_to_legal(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_a-send-to-legal').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_to_legal';
      child_doctype = 'Canopi Condition Precedent Part A';
      $('input[class="condition_precedent_part_a_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_condition_precedent_part_a_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_condition_precedent_part_a_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_condition_precedent_part_a_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_condition_precedent_part_a_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

async function load_condition_precedent_part_b(frm) {
  $(frm.fields_dict.condition_precedent_part_b_html.wrapper).empty();
  if (frm.doc.product_name?.toUpperCase() === 'FA') {
    if (!frm.doc.__islocal) {
      const condition_precedent_part_b_new = [];
      const st = [];
      condition_precedent_part_b = await get_doc_list(
        frm,
        'Canopi Condition Precedent Part B',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (condition_precedent_part_b.length === 0) {
        condition_precedent_part_b = [];
      } else {
        for (let i = 0; i < condition_precedent_part_b.length; i++) {
          let category = condition_precedent_part_b[i].category;
          if (category === '') category = 'Others';
          if (condition_precedent_part_b_new[category] === undefined) {
            condition_precedent_part_b_new[category] = [];
          }
          if (st.indexOf(category) === -1) {
            st.push(category);
          }
          condition_precedent_part_b_new[category].push(
            condition_precedent_part_b[i],
          );
        }
      }
      $(frm.fields_dict.condition_precedent_part_b_html.wrapper).empty();
      frm.set_df_property(
        'condition_precedent_part_b_html',
        'options',
        frappe.render_template('documentation_condition_precedent_part_b', {
          st,
          doc_data: condition_precedent_part_b_new,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('condition_precedent_part_b_html');
      $('.' + frm.doc.name + '-condition_precedent_part_b-edit-row').on(
        'click',
        function () {
          condition_precedent_part_b_dialog(
            frm,
            condition_precedent_part_b[this.id],
            'Edit Condition Precedent - PART B',
          );
        },
      );

      $('.condition_precedent_part_b_chkAll').click(function () {
        if (this.checked) {
          $('.condition_precedent_part_b_chk').prop('checked', true);
        } else {
          $('.condition_precedent_part_b_chk').prop('checked', false);
        }
      });
      condition_precedent_part_b_add(frm);
      condition_precedent_part_b_delete(frm);
      condition_precedent_part_b_upload_events(frm);
      condition_precedent_part_b_download_events(frm);
      condition_precedent_part_b_email(frm);
      condition_precedent_part_b_send_to_legal(frm);
    }
  }
}

function condition_precedent_part_b_add(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-add').on(
    'click',
    function () {
      condition_precedent_part_b_dialog(
        frm,
        {},
        'Add Condition Precedent - Part A',
      );
    },
  );
}

function condition_precedent_part_b_delete(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-delete').on(
    'click',
    function () {
      $('input[class="condition_precedent_part_b_chk"]:checked').each(
        function () {
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Condition Precedent Part B',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete condition_precedent_part_b_files[this.value];
              await load_condition_precedent_part_b(frm);
              load_loa_table(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('condition_precedent_part_b_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function condition_precedent_part_b_download_events(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function condition_precedent_part_b_upload_events(frm) {
  condition_precedent_part_b_attach_file_event(frm);
  condition_precedent_part_b_upload_all_event(frm);
}

let condition_precedent_part_b_files = {};
function condition_precedent_part_b_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-condition_precedent_part_b-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-condition_precedent_part_b-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-condition_precedent_part_b-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#condition_precedent_part_b_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-condition_precedent_part_b-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-condition_precedent_part_b-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          condition_precedent_part_b_files[row_name] = {};
        condition_precedent_part_b_files[row_name].file = file[0];
        condition_precedent_part_b_files[row_name].file_name = file[0].name;
        condition_precedent_part_b_files[row_name].row_name = row_name;
        add_condition_precedent_part_b_preview_event(frm);
        add_condition_precedent_part_b_remove_file_event(frm);
      }
    },
  );
}

function add_condition_precedent_part_b_preview_event(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        condition_precedent_part_b_files[rowname].file,
      );
      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_condition_precedent_part_b_remove_file_event(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete condition_precedent_part_b_files[rowname];
    },
  );
}

function condition_precedent_part_b_upload_all_event(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-upload-all').on(
    'click',
    () => {
      if (Object.keys(condition_precedent_part_b_files).length > 0) {
        detailsS3Upload(
          frm,
          condition_precedent_part_b_files,
          'Canopi Condition Precedent Part B',
          'condition_precedent_part_b_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function condition_precedent_part_b_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },

      {
        label: 'Category',
        options: ['Borrower', 'Insurance'],
        reqd: 1,
        fieldname: 'category',
        fieldtype: 'Select',
        default: rowData.category,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          condition_precedent_part_b_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Small Text',
        default: rowData.document_name,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_condition_precedent_part_b',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          category: values.category,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_condition_precedent_part_b(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('condition_precedent_part_b_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();

  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.value = 'Yes';
    field.refresh();
  } else {
    let field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();
    } else {
      field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();
    }
  }
}

function condition_precedent_part_b_email(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Condition Precedent Part B';
      $('input[class="condition_precedent_part_b_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_condition_precedent_part_b_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_condition_precedent_part_b_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_condition_precedent_part_b_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_condition_precedent_part_b_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function condition_precedent_part_b_send_to_legal(frm) {
  $('.' + frm.doc.name + '-condition_precedent_part_b-send-to-legal').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_to_legal';
      child_doctype = 'Canopi Condition Precedent Part B';
      $('input[class="condition_precedent_part_b_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_condition_precedent_part_b_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_condition_precedent_part_b_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_condition_precedent_part_b_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_condition_precedent_part_b_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

// Security Documents Pre Execution
async function load_security_documents_pre_execution(frm) {
  $(frm.fields_dict.security_documents_pre_execution_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() !== 'EA' &&
    frm.doc.product_name?.toUpperCase() !== 'FA' &&
    frm.doc.product_name?.toUpperCase() !== 'REIT' &&
    frm.doc.product_name?.toUpperCase() !== 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      const security_documents_pre_execution_new = [];
      const st = [];
      security_documents_pre_execution = await get_doc_list(
        frm,
        'Canopi Security Documents',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (security_documents_pre_execution.length === 0) {
        security_documents_pre_execution = [];
      } else {
        for (let i = 0; i < security_documents_pre_execution.length; i++) {
          let security_type = security_documents_pre_execution[i].security_type;
          if (security_type === '') security_type = 'Others';
          if (
            security_documents_pre_execution_new[security_type] === undefined
          ) {
            security_documents_pre_execution_new[security_type] = [];
          }
          if (st.indexOf(security_type) === -1) {
            st.push(security_type);
          }
          security_documents_pre_execution_new[security_type].push(
            security_documents_pre_execution[i],
          );
        }
      }
      $(frm.fields_dict.security_documents_pre_execution_html.wrapper).empty();
      frm.set_df_property(
        'security_documents_pre_execution_html',
        'options',
        frappe.render_template(
          'documentation_security_documents_pre_execution',
          {
            st,
            doc_data: security_documents_pre_execution_new,
            id: frm.doc.name,
            docstatus: frm.doc.docstatus,
          },
        ),
      );

      frm.refresh_field('security_documents_pre_execution_html');
      $('.' + frm.doc.name + '-security_documents_pre_execution-edit-row').on(
        'click',
        function () {
          security_documents_pre_execution_dialog(
            frm,
            security_documents_pre_execution[this.id],
            'Edit Security Documents - Pre-Execution',
          );
        },
      );

      $('.security_documents_pre_execution_chkAll').click(function () {
        if (this.checked) {
          $('.security_documents_pre_execution_chk').prop('checked', true);
        } else {
          $('.security_documents_pre_execution_chk').prop('checked', false);
        }
      });
      security_documents_pre_execution_add(frm);
      security_documents_pre_execution_delete(frm);
      security_documents_pre_execution_upload_events(frm);
      security_documents_pre_execution_download_events(frm);
      security_documents_pre_execution_initiate_post_execution(frm);
      security_documents_pre_execution_email(frm);
      security_documents_pre_execution_send_to_legal(frm);
    }
  }
}

function security_documents_pre_execution_add(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-add').on(
    'click',
    function () {
      security_documents_pre_execution_dialog(
        frm,
        {},
        'Add Security Documents - Pre-Execution',
      );
    },
  );
}

function security_documents_pre_execution_delete(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-delete').on(
    'click',
    function () {
      $('input[class="security_documents_pre_execution_chk"]:checked').each(
        function () {
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Security Documents',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete security_documents_pre_execution_files[this.value];
              await load_security_documents_pre_execution(frm);
              load_loa_table(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('security_documents_pre_execution_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function security_documents_pre_execution_download_events(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function security_documents_pre_execution_upload_events(frm) {
  security_documents_pre_execution_attach_file_event(frm);
  security_documents_pre_execution_upload_all_event(frm);
}

let security_documents_pre_execution_files = {};
function security_documents_pre_execution_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-security_documents_pre_execution-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-security_documents_pre_execution-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $(
    '.' + frm.doc.name + '-security_documents_pre_execution-attach-input',
  ).change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';

      $('#security_documents_pre_execution_f_' + row_name).html(
        '<a><span class="' +
          frm.doc.name +
          // eslint-disable-next-line max-len
          '-security_documents_pre_execution-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
          file[0].name +
          '</a>&nbsp;<a class="' +
          frm.doc.name +
          '-security_documents_pre_execution-remove-file"><span>' +
          frappe.utils.icon('close', 'sm') +
          '</span></a>',
      );

      // details[s3_button_index]['a45'] = file[0].name
      if (row_name !== 'undefined')
        security_documents_pre_execution_files[row_name] = {};
      security_documents_pre_execution_files[row_name].file = file[0];
      security_documents_pre_execution_files[row_name].file_name = file[0].name;
      security_documents_pre_execution_files[row_name].row_name = row_name;
      add_security_documents_pre_execution_preview_event(frm);
      add_security_documents_pre_execution_remove_file_event(frm);
    }
  });
}

function add_security_documents_pre_execution_preview_event(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        security_documents_pre_execution_files[rowname].file,
      );
      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_security_documents_pre_execution_remove_file_event(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete security_documents_pre_execution_files[rowname];
    },
  );
}

function security_documents_pre_execution_upload_all_event(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-upload-all').on(
    'click',
    () => {
      if (Object.keys(security_documents_pre_execution_files).length > 0) {
        detailsS3Upload(
          frm,
          security_documents_pre_execution_files,
          'Canopi Security Documents',
          'security_documents_pre_execution_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function security_documents_pre_execution_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },

      {
        label: 'Type Of Security',
        options: 'Canopi Type of Security',
        reqd: 1,
        fieldname: 'security_type',
        fieldtype: 'Link',
        default: rowData.security_type,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          security_documents_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();

              field = d.get_field('for_post_execution');
              field.df.read_only = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'For Post Execution',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'for_post_execution',
        fieldtype: 'Select',
        default: rowData.for_post_execution,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_security_documents_pre_execution',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          remarks_by_legal: values.remarks_by_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          for_post_execution: values.for_post_execution,
          security_type: values.security_type,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_security_documents_pre_execution(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('security_documents_pre_execution_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();

  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.value = 'Yes';
    field.refresh();
    field = d.get_field('for_post_execution');
    field.value = 'No';
    field.df.read_only = true;
    field.refresh();
  } else {
    let field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = true;
      field.refresh();
    } else {
      field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = false;
      field.refresh();
    }
  }
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

function security_documents_pre_execution_initiate_post_execution(frm) {
  $(
    '.' +
      frm.doc.name +
      '-security_documents_pre_execution-initiate-post-execution',
  ).on('click', function () {
    const doc_name_arr = {};
    // eslint-disable-next-line no-unused-vars
    let i = 0;
    let is_error = false;
    $('input[class="security_documents_pre_execution_chk"]:checked').each(
      function () {
        doc_name_arr[this.value] = this.value;
        const retain_waive = $(
          '#' + this.value + '_security_documents_pre_execution_retain_waive',
        ).text();
        const status = $(
          '#' + this.value + '_security_documents_pre_execution_status',
        ).text();
        const send_to_legal = $(
          '#' + this.value + '_security_documents_pre_execution_send_to_legal',
        ).text();
        const purpose_of_legal = $(
          '#' +
            this.value +
            '_security_documents_pre_execution_purpose_of_legal',
        ).text();
        const remarks_by_legal = $(
          '#' +
            this.value +
            '_security_documents_pre_execution_remarks_by_legal',
        ).text();
        const approval_if_required = $(
          '#' +
            this.value +
            '_security_documents_pre_execution_approval_if_required',
        ).text();
        if (status !== 'LOA Generated') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution, Status must be LOA Generated.',
            ),
          });
          is_error = true;
        }
        if (send_to_legal !== 'Yes') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution, Send To Legal must be Yes',
            ),
          });
          is_error = true;
        }
        if (purpose_of_legal === 'Vetting' && remarks_by_legal === '') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution, Remarks by Legal Require',
            ),
          });
          is_error = true;
        }
        if (retain_waive === 'Waive') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution for documents that are Waived.',
            ),
          });
          is_error = true;
        } else if (approval_if_required === 'Yes') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __('Can not Initiate post execution, approval required.'),
          });
          is_error = true;
        }
        i++;
      },
    );

    if (!is_error && Object.keys(doc_name_arr).length > 0) {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.initiate_post_execution',
        args: {
          doctype: 'Canopi Security Documents',
          newdoctype: 'Canopi Security Documents Post Execution',
          docname: JSON.stringify(doc_name_arr),
          documentation_name: frm.doc.name,
        },
        freeze: true,

        callback: async r => {
          await load_security_documents_pre_execution(frm);
          await load_security_documents_post_execution(frm);
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('security_documents_post_execution_html');
            }, 100);
          });
        },
      });
    }
  });
}

function security_documents_pre_execution_email(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Security Documents';
      $('input[class="security_documents_pre_execution_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_security_documents_pre_execution_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_security_documents_pre_execution_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_security_documents_pre_execution_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_security_documents_pre_execution_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}
function security_documents_pre_execution_send_to_legal(frm) {
  $('.' + frm.doc.name + '-security_documents_pre_execution-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="security_documents_pre_execution_chk"]:checked').each(
        function () {
          if ($('#' + this.value + '_send_to_legal').text() === 'Yes') {
            frappe.throw(
              'Some of the selected documents have already been sent to legal',
            );
          }
          if (
            $(
              '#' + this.value + '_security_documents_pre_execution_file_name',
            ).val() === ''
          ) {
            frappe.throw('Please Upload File For Selected Rows');
          }
          selected_documents.push({
            name: this.value,
          });
        },
      );
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Security Documents',
        'security_documents_pre_execution_html',
      );
    },
  );
}

// security documents post execution
async function load_security_documents_post_execution(frm) {
  $(frm.fields_dict.security_documents_post_execution_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() !== 'EA' &&
    frm.doc.product_name?.toUpperCase() !== 'FA' &&
    frm.doc.product_name?.toUpperCase() !== 'REIT' &&
    frm.doc.product_name?.toUpperCase() !== 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      security_documents_post_execution = await get_doc_list(
        frm,
        'Canopi Security Documents Post Execution',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'security_type asc, creation asc',
      );

      const security_documents_post_execution_new = [];
      const st = [];
      if (security_documents_post_execution.length === 0) {
        security_documents_post_execution = [];
      } else {
        for (let i = 0; i < security_documents_post_execution.length; i++) {
          let security_type =
            security_documents_post_execution[i].security_type;
          if (security_type === '') security_type = 'Others';
          if (
            security_documents_post_execution_new[security_type] === undefined
          ) {
            security_documents_post_execution_new[security_type] = [];
          }
          if (st.indexOf(security_type) === -1) {
            st.push(security_type);
          }
          security_documents_post_execution_new[security_type].push(
            security_documents_post_execution[i],
          );
        }
      }
      $(frm.fields_dict.security_documents_post_execution_html.wrapper).empty();
      frm.set_df_property(
        'security_documents_post_execution_html',
        'options',
        frappe.render_template(
          'documentation_security_documents_post_execution',
          {
            st,
            doc_data: security_documents_post_execution_new,
            id: frm.doc.name,
            docstatus: frm.doc.docstatus,
          },
        ),
      );

      frm.refresh_field('security_documents_post_execution_html');
      $('.' + frm.doc.name + '-security_documents_post_execution-edit-row').on(
        'click',
        function () {
          security_documents_post_execution_dialog(
            frm,
            security_documents_post_execution[this.id],
            'Edit Security Documents - Post-Execution',
          );
        },
      );

      $('.security_documents_post_execution_chkAll').click(function () {
        if (this.checked) {
          $('.security_documents_post_execution_chk').prop('checked', true);
        } else {
          $('.security_documents_post_execution_chk').prop('checked', false);
        }
      });
      security_documents_post_execution_add(frm);
      security_documents_post_execution_delete(frm);
      security_documents_post_execution_upload_events(frm);
      security_documents_post_execution_download_events(frm);
      security_documents_post_execution_email(frm);
      security_documents_post_execution_submit_to_checker(frm);
      security_documents_post_execution_send_to_legal(frm);
    }
  }
}

function security_documents_post_execution_add(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-add').on(
    'click',
    function () {
      security_documents_post_execution_dialog(
        frm,
        {},
        'Add Security Documents - Post-Execution',
      );
    },
  );
}

function security_documents_post_execution_delete(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-delete').on(
    'click',
    function () {
      $('input[class="security_documents_post_execution_chk"]:checked').each(
        function () {
          security_documents_post_execution.forEach(x => {
            if (x.name === this.value && x.status !== 'Open')
              frappe.throw('Please select row of <b>Open</b> status.');
          });
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Security Documents Post Execution',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete security_documents_post_execution_files[this.value];
              await load_security_documents_post_execution(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('security_documents_post_execution_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function security_documents_post_execution_download_events(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function security_documents_post_execution_upload_events(frm) {
  security_documents_post_execution_attach_file_event(frm);
  security_documents_post_execution_upload_all_event(frm);
}

let security_documents_post_execution_files = {};
function security_documents_post_execution_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-security_documents_post_execution-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-security_documents_post_execution-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $(
    '.' + frm.doc.name + '-security_documents_post_execution-attach-input',
  ).change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';

      $('#security_documents_post_execution_f_' + row_name).html(
        '<a><span class="' +
          frm.doc.name +
          // eslint-disable-next-line max-len
          '-security_documents_post_execution-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
          file[0].name +
          '</a>&nbsp;<a class="' +
          frm.doc.name +
          '-security_documents_post_execution-remove-file"><span>' +
          frappe.utils.icon('close', 'sm') +
          '</span></a>',
      );

      // details[s3_button_index]['a45'] = file[0].name
      if (row_name !== 'undefined')
        security_documents_post_execution_files[row_name] = {};
      security_documents_post_execution_files[row_name].file = file[0];
      security_documents_post_execution_files[row_name].file_name =
        file[0].name;
      security_documents_post_execution_files[row_name].row_name = row_name;
      add_security_documents_post_execution_preview_event(frm);
      add_security_documents_post_execution_remove_file_event(frm);
    }
  });
}

function add_security_documents_post_execution_preview_event(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        security_documents_post_execution_files[rowname].file,
      );
      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_security_documents_post_execution_remove_file_event(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete security_documents_post_execution_files[rowname];
    },
  );
}

function security_documents_post_execution_upload_all_event(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-upload-all').on(
    'click',
    () => {
      if (Object.keys(security_documents_post_execution_files).length > 0) {
        detailsS3Upload(
          frm,
          security_documents_post_execution_files,
          'Canopi Security Documents Post Execution',
          'security_documents_post_execution_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

async function security_documents_post_execution_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Type Of Security',
        options: 'Canopi Type of Security',
        reqd: 1,
        fieldname: 'security_type',
        fieldtype: 'Link',
        default: rowData.security_type,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          security_documents_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Execution Version',
        reqd: 0,
        fieldname: 'execution_version',
        fieldtype: 'Data',
        default: rowData.execution_version,
      },
      {
        label: 'Single Signing Required',
        reqd: 0,
        fieldname: 'single_signing',
        fieldtype: 'Check',
        default: rowData.single_signing,
        onchange: function (e) {
          if (this.value === 1) {
            let field = d.get_field('signing_party');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = true;
            field.df.hidden = false;
            field.refresh();
          } else {
            let field = d.get_field('signing_party');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = false;
            field.df.hidden = true;
            field.refresh();
          }
        },
      },
      {
        label: 'Signing Party - ATSL',
        reqd: 1,
        options: ['ATSL', 'POA'],
        onchange: function (e) {
          if (this.value === 'ATSL') {
            let field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else {
            let field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
          }
        },
        fieldname: 'signing_party',
        fieldtype: 'Select',
        default: rowData.signing_party,
      },
      {
        label: 'Signatory-ATSL',
        fieldtype: 'Link',
        options: 'User',
        fieldname: 'signatory',
        default: rowData.signatory,
      },
      {
        label: 'Signatory-POA',
        fieldtype: 'Link',
        hidden: 1,
        options: 'Canopi POA',
        fieldname: 'signatory_poa',
        default: rowData.signatory_poa,
      },
      {
        label: 'Signatory Name - Party',
        hidden: 1,
        fieldtype: 'Data',
        fieldname: 'signatory_text',
        default: rowData.signatory_text,
      },
      {
        label: 'Custody Location',
        fieldtype: 'Link',
        options: 'Location',
        fieldname: 'location',
        default: rowData.location,
      },
      {
        label: 'Execution Date',
        reqd: await checkreq(frm, rowData),
        fieldname: 'execution_date',
        fieldtype: 'Date',
        default: rowData.execution_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Status',
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Data',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
      {
        label: 'Document Custody with',
        options: ['ATSL', 'Axis Bank', 'Client', 'Third Party'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Axis Bank') {
            let field = d.get_field('axis_bank_branch_name');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('third_party_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else if (this.value === 'Third Party') {
            let field = d.get_field('third_party_name');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('axis_bank_branch_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else {
            let field = d.get_field('third_party_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('axis_bank_branch_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          }
        },
        fieldname: 'document_custody_with',
        fieldtype: 'Select',
        default: rowData.document_custody_with,
      },
      {
        label: 'Axis Bank Branch Name',
        reqd: 1,
        fieldtype: 'Data',
        fieldname: 'axis_bank_branch_name',
        depends_on: 'eval:doc.document_custody_with==="Axis Bank"',
        default: rowData.axis_bank_branch_name,
      },
      {
        label: 'Third Party Name',
        reqd: 1,
        fieldtype: 'Data',
        fieldname: 'third_party_name',
        depends_on: 'eval:doc.document_custody_with==="Third Party"',
        default: rowData.third_party_name,
      },
      {
        label: 'Document Type',
        options: ['Original', 'CTC', 'Photocopy'],
        fieldname: 'document_type',
        fieldtype: 'Select',
        default: rowData.document_type,
      },
      {
        label: 'Executed Location',
        fieldtype: 'Text',
        fieldname: 'executed_location',
        default: rowData.executed_location,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_security_documents_post_execution',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          execution_version: values.execution_version,
          single_signing: values.single_signing,
          signing_party: values.signing_party,
          signatory: values.signatory,
          signatory_poa: values.signatory_poa,
          signatory_text: values.signatory_text,
          location: values.location,
          execution_date: values.execution_date,
          comments: values.comments,
          status: values.status,
          send_email: values.send_email,
          approval_if_required: values.approval_if_required,
          send_to_legal: values.send_to_legal,
          remarks_by_legal: values.remarks_by_legal,
          document_custody_with: values.document_custody_with,
          axis_bank_branch_name: values.axis_bank_branch_name,
          third_party_name: values.third_party_name,
          document_type: values.document_type,
          executed_location: values.executed_location,
          security_type: values.security_type,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_security_documents_post_execution(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('security_documents_post_execution_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  if (rowData.document_custody_with === 'Axis Bank') {
    let field = d.get_field('axis_bank_branch_name');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('third_party_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else if (rowData.document_custody_with === 'Third Party') {
    let field = d.get_field('third_party_name');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('axis_bank_branch_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else {
    let field = d.get_field('third_party_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('axis_bank_branch_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  }
  if (rowData.single_signing === 1) {
    let field = d.get_field('signing_party');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_poa');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.reqd = true;
    field.df.hidden = false;
    field.refresh();
  } else {
    let field = d.get_field('signing_party');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.reqd = false;
    field.df.hidden = true;
    field.refresh();

    if (rowData.signing_party) {
      if (rowData.signing_party === 'ATSL') {
        let field = d.get_field('signatory');
        field.df.hidden = false;
        field.df.reqd = true;
        field.refresh();
        field = d.get_field('signatory_poa');
        field.df.hidden = true;
        field.df.reqd = false;
        field.refresh();
      } else {
        let field = d.get_field('signatory');
        field.df.hidden = true;
        field.df.reqd = false;
        field.refresh();
        field = d.get_field('signatory_poa');
        field.df.hidden = false;
        field.df.reqd = true;
        field.refresh();
      }
    }
  }
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
  d.show();
}

async function checkreq(frm, rowData) {
  if (Object.keys(rowData).length === 0) {
    return 0;
  } else {
    const res = await chk_req_for_documentation(rowData, frm.doc.operations);
    return res.message;
  }
}

function chk_req_for_documentation(data, operations) {
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.chk_req_for_documentation',
    args: {
      data,
      operations,
    },
  });
}

function security_documents_post_execution_email(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Security Documents Post Execution';
      $('input[class="security_documents_post_execution_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_security_documents_post_execution_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_security_documents_post_execution_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_security_documents_post_execution_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_security_documents_post_execution_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function security_documents_post_execution_submit_to_checker(frm) {
  $(
    '.' + frm.doc.name + '-security_documents_post_execution-submit-to-checker',
  ).unbind();
  $(
    '.' + frm.doc.name + '-security_documents_post_execution-submit-to-checker',
  ).on('click', function () {
    const docnames = $(
      'input[class="security_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Security Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    docnames.forEach(doc => {
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            security_docs.status !== 'Open' &&
            security_docs.status.indexOf('Rejected') === -1,
        )
      ) {
        frappe.throw(
          'Some of the selected documents have already been sent for approval',
        );
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            (security_docs.file_name === null ||
              security_docs.file_name === ''),
        )
      ) {
        frappe.throw('Please Upload File for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            security_docs.execution_version === '',
        )
      ) {
        frappe.throw('Execution Version mandatory for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            security_docs.signing_party === 'ATSL' &&
            security_docs.signatory === '',
        )
      ) {
        frappe.throw('Signatory mandatory for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            security_docs.signing_party === 'POA' &&
            security_docs.signatory_poa === '',
        )
      ) {
        frappe.throw('Signatory mandatory for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            security_docs.single_signing === '1' &&
            security_docs.signatory_text === '',
        )
      ) {
        frappe.throw('Signatory Party mandatory for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname && security_docs.location === '',
        )
      ) {
        frappe.throw('Location mandatory for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            (security_docs.execution_date === null ||
              security_docs.execution_date === ''),
        )
      ) {
        frappe.throw('Execution Date mandatory for selected documents');
      }
      if (
        security_documents_post_execution.some(
          security_docs =>
            security_docs.name === doc.docname &&
            security_docs.purpose_of_legal === 'Vetting' &&
            security_docs.remarks_by_legal === '',
        )
      ) {
        frappe.throw('Remarks by legal mandatory for selected documents');
      }
    });
    update_documents_status(docnames, 'Submit to Checker').then(async () => {
      approval_dialog(frm);
    });
  });
}
function security_documents_post_execution_send_to_legal(frm) {
  $('.' + frm.doc.name + '-security_documents_post_execution-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="security_documents_post_execution_chk"]:checked').each(
        function () {
          const status = $(
            '#' + this.value + '_security_documents_post_execution_status',
          ).text();

          if (status === 'Submit to Checker') {
            frappe.throw(
              'Some of the selected documents have been submit to checker',
            );
          }
          if (
            $(
              '#' +
                this.value +
                '_security_documents_post_execution_send_to_legal',
            ).text() === 'Yes'
          ) {
            frappe.throw(
              'Some of the selected documents have already been sent to legal',
            );
          }
          if (
            $(
              '#' + this.value + '_security_documents_post_execution_file_name',
            ).val() === ''
          ) {
            frappe.throw('Please Upload File For Selected Rows');
          }
          selected_documents.push({
            name: this.value,
          });
        },
      );
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Security Documents Post Execution',
        'security_documents_post_execution_html',
      );
    },
  );
}

async function load_condition_subsequent(frm) {
  $(frm.fields_dict.condition_subsequent_html.wrapper).empty();
  if (frm.doc.product_name?.toUpperCase() === 'FA') {
    if (!frm.doc.__islocal) {
      const condition_subsequent_new = [];
      const st = [];
      condition_subsequent = await get_doc_list(
        frm,
        'Canopi Condition Subsequent',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (condition_subsequent.length === 0) {
        condition_subsequent = [];
      } else {
        for (let i = 0; i < condition_subsequent.length; i++) {
          let category = condition_subsequent[i].category;
          if (category === '') category = 'Others';
          if (condition_subsequent_new[category] === undefined) {
            condition_subsequent_new[category] = [];
          }
          if (st.indexOf(category) === -1) {
            st.push(category);
          }
          condition_subsequent_new[category].push(condition_subsequent[i]);
        }
      }
      $(frm.fields_dict.condition_subsequent_html.wrapper).empty();
      frm.set_df_property(
        'condition_subsequent_html',
        'options',
        frappe.render_template('documentation_condition_subsequent', {
          st,
          doc_data: condition_subsequent_new,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('condition_subsequent_html');
      $('.' + frm.doc.name + '-condition_subsequent-edit-row').on(
        'click',
        function () {
          condition_subsequent_dialog(
            frm,
            condition_subsequent[this.id],
            'Edit Condition Precedent - PART B',
          );
        },
      );

      $('.condition_subsequent_chkAll').click(function () {
        if (this.checked) {
          $('.condition_subsequent_chk').prop('checked', true);
        } else {
          $('.condition_subsequent_chk').prop('checked', false);
        }
      });
      condition_subsequent_add(frm);
      condition_subsequent_delete(frm);
      condition_subsequent_upload_events(frm);
      condition_subsequent_download_events(frm);
      condition_subsequent_email(frm);
      condition_subsequent_send_to_legal(frm);
    }
  }
}

function condition_subsequent_add(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-add').on('click', function () {
    condition_subsequent_dialog(frm, {}, 'Add Condition Precedent - Part A');
  });
}

function condition_subsequent_delete(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-delete').on(
    'click',
    function () {
      $('input[class="condition_subsequent_chk"]:checked').each(function () {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
          args: {
            doctype: 'Canopi Condition Subsequent',
            docname: this.value,
          },
          freeze: true,

          callback: async r => {
            delete condition_subsequent_files[this.value];
            await load_condition_subsequent(frm);
            load_loa_table(frm);
            frm.reload_doc().then(() => {
              setTimeout(() => {
                frm.scroll_to_field('condition_subsequent_html');
              }, 100);
            });
          },
        });
      });
    },
  );
}

function condition_subsequent_download_events(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function condition_subsequent_upload_events(frm) {
  condition_subsequent_attach_file_event(frm);
  condition_subsequent_upload_all_event(frm);
}

let condition_subsequent_files = {};
function condition_subsequent_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-condition_subsequent-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-condition_subsequent-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-condition_subsequent-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#condition_subsequent_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-condition_subsequent-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-condition_subsequent-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined') condition_subsequent_files[row_name] = {};
        condition_subsequent_files[row_name].file = file[0];
        condition_subsequent_files[row_name].file_name = file[0].name;
        condition_subsequent_files[row_name].row_name = row_name;
        add_condition_subsequent_preview_event(frm);
        add_condition_subsequent_remove_file_event(frm);
      }
    },
  );
}

function add_condition_subsequent_preview_event(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        condition_subsequent_files[rowname].file,
      );
      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_condition_subsequent_remove_file_event(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete condition_subsequent_files[rowname];
    },
  );
}

function condition_subsequent_upload_all_event(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-upload-all').on('click', () => {
    if (Object.keys(condition_subsequent_files).length > 0) {
      detailsS3Upload(
        frm,
        condition_subsequent_files,
        'Canopi Condition Subsequent',
        'condition_subsequent_html',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function condition_subsequent_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },

      {
        label: 'Category',
        options: [
          'Borrower',
          'Finance Documents',
          'Transaction Security Documents',
          'Transaction Security',
          'Other documents and evidence',
          'O&M',
        ],
        reqd: 1,
        fieldname: 'category',
        fieldtype: 'Select',
        default: rowData.category,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          condition_subsequent_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Small Text',
        default: rowData.document_name,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_condition_subsequent',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          category: values.category,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_condition_subsequent(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('condition_subsequent_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();

  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.value = 'Yes';
    field.refresh();
  } else {
    let field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();
    } else {
      field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();
    }
  }
}

function condition_subsequent_email(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Condition Subsequent';
      $('input[class="condition_subsequent_chk"]:checked').each(function () {
        const attachment = [];
        attachment.name = this.value;
        attachment.file_name = $(
          '#' + this.value + '_condition_subsequent_file_name',
        ).val();
        attachment.file_url = $(
          '#' + this.value + '_condition_subsequent_file_path',
        ).val();
        s3_attachments[this.value] = {};
        s3_attachments[this.value].file_name = $(
          '#' + this.value + '_condition_subsequent_file_name',
        ).val();
        s3_attachments[this.value].file_url = $(
          '#' + this.value + '_condition_subsequent_file_path',
        ).val();
        render_attachment_rows(attachment);
      });
    },
  );
}

function condition_subsequent_send_to_legal(frm) {
  $('.' + frm.doc.name + '-condition_subsequent-send-to-legal').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_to_legal';
      child_doctype = 'Canopi Condition Subsequent';
      $('input[class="condition_subsequent_chk"]:checked').each(function () {
        const attachment = [];
        attachment.name = this.value;
        attachment.file_name = $(
          '#' + this.value + '_condition_subsequent_file_name',
        ).val();
        attachment.file_url = $(
          '#' + this.value + '_condition_subsequent_file_path',
        ).val();
        s3_attachments[this.value] = {};
        s3_attachments[this.value].file_name = $(
          '#' + this.value + '_condition_subsequent_file_name',
        ).val();
        s3_attachments[this.value].file_url = $(
          '#' + this.value + '_condition_subsequent_file_path',
        ).val();
        render_attachment_rows(attachment);
      });
    },
  );
}

// Other Documents Pre Execution
async function load_other_documents_pre_execution(frm) {
  $(frm.fields_dict.other_documents_pre_execution_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() !== 'REIT' &&
    frm.doc.product_name?.toUpperCase() !== 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      other_documents_pre_execution = await get_doc_list(
        frm,
        'Canopi Other Documents',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (other_documents_pre_execution.length === 0) {
        other_documents_pre_execution = [];
      }

      $(frm.fields_dict.other_documents_pre_execution_html.wrapper).empty();
      frm.set_df_property(
        'other_documents_pre_execution_html',
        'options',
        frappe.render_template('documentation_other_documents_pre_execution', {
          doc: other_documents_pre_execution,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('other_documents_pre_execution_html');
      $('.' + frm.doc.name + '-other_documents_pre_execution-edit-row').on(
        'click',
        function () {
          other_documents_pre_execution_dialog(
            frm,
            other_documents_pre_execution[this.id],
            'Edit Other Documents - Pre-Execution',
          );
        },
      );

      $('.other_documents_pre_execution_chkAll').click(function () {
        if (this.checked) {
          $('.other_documents_pre_execution_chk').prop('checked', true);
        } else {
          $('.other_documents_pre_execution_chk').prop('checked', false);
        }
      });
      other_documents_pre_execution_add(frm);
      other_documents_pre_execution_delete(frm);
      other_documents_pre_execution_upload_events(frm);
      other_documents_pre_execution_download_events(frm);
      other_documents_pre_execution_initiate_post_execution(frm);
      other_documents_pre_execution_email(frm);
      other_documents_pre_execution_send_to_legal(frm);
    }
  }
}

function other_documents_pre_execution_add(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-add').on(
    'click',
    function () {
      other_documents_pre_execution_dialog(
        frm,
        {},
        'Add Other Documents - Pre-Execution',
      );
    },
  );
}

function other_documents_pre_execution_delete(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-delete').on(
    'click',
    function () {
      $('input[class="other_documents_pre_execution_chk"]:checked').each(
        function () {
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Other Documents',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete other_documents_pre_execution_files[this.value];
              await load_other_documents_pre_execution(frm);
              load_loa_table(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('other_documents_pre_execution_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function other_documents_pre_execution_download_events(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function other_documents_pre_execution_upload_events(frm) {
  other_documents_pre_execution_attach_file_event(frm);
  other_documents_pre_execution_upload_all_event(frm);
}

let other_documents_pre_execution_files = {};
function other_documents_pre_execution_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-other_documents_pre_execution-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-other_documents_pre_execution-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-other_documents_pre_execution-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#other_documents_pre_execution_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-other_documents_pre_execution-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-other_documents_pre_execution-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          other_documents_pre_execution_files[row_name] = {};
        other_documents_pre_execution_files[row_name].file = file[0];
        other_documents_pre_execution_files[row_name].file_name = file[0].name;
        other_documents_pre_execution_files[row_name].row_name = row_name;
        add_other_documents_pre_execution_preview_event(frm);
        add_other_documents_pre_execution_remove_file_event(frm);
      }
    },
  );
}

function add_other_documents_pre_execution_preview_event(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-preview-file').on(
    'click',
    async function () {
      frappe.dom.freeze();
      const rowname = $(this).closest('tr').attr('id');
      const blobUrl = await fileToBlobAndUrl(
        other_documents_pre_execution_files[rowname].file,
      );
      frappe.dom.unfreeze();

      window.open(blobUrl, '_blank');
    },
  );
}

function add_other_documents_pre_execution_remove_file_event(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete other_documents_pre_execution_files[rowname];
    },
  );
}

function other_documents_pre_execution_upload_all_event(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-upload-all').on(
    'click',
    () => {
      if (Object.keys(other_documents_pre_execution_files).length > 0) {
        detailsS3Upload(
          frm,
          other_documents_pre_execution_files,
          'Canopi Other Documents',
          'other_documents_pre_execution_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function other_documents_pre_execution_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        options: '',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          other_documents_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        options: '',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Retain/ Waive',
        options: ['Retain', 'Waive'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Waive') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('approval_if_required');
            field.df.read_only = true;
            field.value = 'Yes';
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('approval_if_required');
            field.df.read_only = false;
            field.refresh();
            if (field.value === 'No') {
              field = d.get_field('comments');
              field.df.reqd = false;
              field.refresh();

              field = d.get_field('for_post_execution');
              field.df.read_only = false;
              field.refresh();
            }
          }
        },
        fieldname: 'retain_waive',
        fieldtype: 'Select',
        default: rowData.retain_waive,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
      {
        label: 'Status',
        options: ['Open', 'LOA Generated'],
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Select',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
        onchange: function (e) {
          if (this.value === 'Yes') {
            let field = d.get_field('comments');
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.value = 'No';
            field.df.read_only = true;
            field.refresh();
          } else {
            let field = d.get_field('comments');
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('for_post_execution');
            field.df.read_only = false;
            field.refresh();
          }
        },
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'For Post Execution',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'for_post_execution',
        fieldtype: 'Select',
        default: rowData.for_post_execution,
      },
      {
        label: 'Upload Date',
        reqd: 0,
        fieldname: 'upload_date',
        fieldtype: 'Date',
        default: rowData.upload_date,
        read_only: 1,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_other_documents_pre_execution',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          upload_date: values.upload_date,
          comments: values.comments,
          retain_waive: values.retain_waive,
          send_to_legal: values.send_to_legal,
          remarks_by_legal: values.remarks_by_legal,
          status: values.status,
          approval_if_required: values.approval_if_required,
          send_email: values.send_email,
          for_post_execution: values.for_post_execution,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_other_documents_pre_execution(frm);
            load_loa_table(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('other_documents_pre_execution_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (rowData.retain_waive === 'Waive') {
    let field = d.get_field('comments');
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('approval_if_required');
    field.df.read_only = true;
    field.value = 'Yes';
    field.refresh();
    field = d.get_field('for_post_execution');
    field.value = 'No';
    field.df.read_only = true;
    field.refresh();
  } else {
    let field = d.get_field('approval_if_required');
    field.df.read_only = false;
    field.refresh();
    if (field.value === 'Yes') {
      field = d.get_field('comments');
      field.df.reqd = true;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = true;
      field.refresh();
    } else {
      field = d.get_field('comments');
      field.df.reqd = false;
      field.refresh();

      field = d.get_field('for_post_execution');
      field.df.read_only = false;
      field.refresh();
    }
  }
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
}

function other_documents_pre_execution_initiate_post_execution(frm) {
  $(
    '.' +
      frm.doc.name +
      '-other_documents_pre_execution-initiate-post-execution',
  ).on('click', function () {
    const doc_name_arr = {};
    // eslint-disable-next-line no-unused-vars
    let i = 0;
    let is_error = false;
    $('input[class="other_documents_pre_execution_chk"]:checked').each(
      function () {
        doc_name_arr[this.value] = this.value;
        const retain_waive = $(
          '#' + this.value + '_other_documents_pre_execution_retain_waive',
        ).text();
        const status = $(
          '#' + this.value + '_other_documents_pre_execution_status',
        ).text();
        const send_to_legal = $(
          '#' + this.value + '_other_documents_pre_execution_send_to_legal',
        ).text();
        const purpose_of_legal = $(
          '#' + this.value + '_other_documents_pre_execution_purpose_of_legal',
        ).text();
        const remarks_by_legal = $(
          '#' + this.value + '_other_documents_pre_execution_remarks_by_legal',
        ).text();
        const approval_if_required = $(
          '#' +
            this.value +
            '_other_documents_pre_execution_approval_if_required',
        ).text();
        if (status !== 'LOA Generated') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution, Status must be LOA Generated.',
            ),
          });
          is_error = true;
        }
        if (send_to_legal !== 'Yes') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution, Send To Legal must be Yes',
            ),
          });
          is_error = true;
        }
        if (purpose_of_legal === 'Vetting' && remarks_by_legal === '') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution, Remarks by Legal Require',
            ),
          });
          is_error = true;
        }
        if (retain_waive === 'Waive') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __(
              'Can not Initiate post execution for documents that are Waived.',
            ),
          });
          is_error = true;
        } else if (approval_if_required === 'Yes') {
          frappe.msgprint({
            title: __('Error'),
            indicator: 'orange',
            message: __('Can not Initiate post execution, approval required'),
          });
          is_error = true;
        }
        i++;
      },
    );

    if (!is_error && Object.keys(doc_name_arr).length > 0) {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.initiate_post_execution',
        args: {
          doctype: 'Canopi Other Documents',
          newdoctype: 'Canopi Other Documents Post Execution',
          docname: JSON.stringify(doc_name_arr),
          documentation_name: frm.doc.name,
        },
        freeze: true,

        callback: async r => {
          await load_other_documents_pre_execution(frm);
          await load_other_documents_post_execution(frm);
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('other_documents_post_execution_html');
            }, 100);
          });
        },
      });
    }
  });
}

function other_documents_pre_execution_email(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Other Documents Post Execution';
      $('input[class="other_documents_pre_execution_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_other_documents_pre_execution_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_other_documents_pre_execution_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_other_documents_pre_execution_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_other_documents_pre_execution_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function other_documents_pre_execution_send_to_legal(frm) {
  $('.' + frm.doc.name + '-other_documents_pre_execution-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="other_documents_pre_execution_chk"]:checked').each(
        function () {
          if (
            $(
              '#' + this.value + '_other_documents_pre_execution_send_to_legal',
            ).text() === 'Yes'
          ) {
            frappe.throw(
              'Some of the selected documents have already been sent to legal',
            );
          }
          if (
            $(
              '#' + this.value + '_other_documents_pre_execution_file_name',
            ).val() === ''
          ) {
            frappe.throw('Please Upload File For Selected Rows');
          }
          selected_documents.push({
            name: this.value,
          });
        },
      );
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Other Documents',
        'other_documents_pre_execution_html',
      );
    },
  );
}

// Other Documents post execution
async function load_other_documents_post_execution(frm) {
  $(frm.fields_dict.other_documents_post_execution_html.wrapper).empty();
  if (
    frm.doc.product_name?.toUpperCase() !== 'REIT' &&
    frm.doc.product_name?.toUpperCase() !== 'INVIT'
  ) {
    if (!frm.doc.__islocal) {
      other_documents_post_execution = await get_doc_list(
        frm,
        'Canopi Other Documents Post Execution',
        [['canopi_documentation_name', '=', frm.doc.name]],
        'creation asc',
      );

      if (other_documents_post_execution.length === 0) {
        other_documents_post_execution = [];
      }

      $(frm.fields_dict.other_documents_post_execution_html.wrapper).empty();
      frm.set_df_property(
        'other_documents_post_execution_html',
        'options',
        frappe.render_template('documentation_other_documents_post_execution', {
          doc: other_documents_post_execution,
          id: frm.doc.name,
          docstatus: frm.doc.docstatus,
        }),
      );

      frm.refresh_field('other_documents_post_execution_html');
      $('.' + frm.doc.name + '-other_documents_post_execution-edit-row').on(
        'click',
        function () {
          other_documents_post_execution_dialog(
            frm,
            other_documents_post_execution[this.id],
            'Edit Other Documents - Post-Execution',
          );
        },
      );

      $('.other_documents_post_execution_chkAll').click(function () {
        if (this.checked) {
          $('.other_documents_post_execution_chk').prop('checked', true);
        } else {
          $('.other_documents_post_execution_chk').prop('checked', false);
        }
      });
      other_documents_post_execution_add(frm);
      other_documents_post_execution_delete(frm);
      other_documents_post_execution_upload_events(frm);
      other_documents_post_execution_download_events(frm);
      other_documents_post_execution_email(frm);
      other_documents_post_execution_submit_to_checker(frm);
      other_documents_post_execution_send_to_legal(frm);
    }
  }
}

function other_documents_post_execution_add(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-add').on(
    'click',
    function () {
      other_documents_post_execution_dialog(
        frm,
        {},
        'Add Other Documents - Post-Execution',
      );
    },
  );
}

function other_documents_post_execution_delete(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-delete').on(
    'click',
    function () {
      $('input[class="other_documents_post_execution_chk"]:checked').each(
        function () {
          other_documents_post_execution.forEach(x => {
            if (x.name === this.value && x.status !== 'Open')
              frappe.throw('Please select row of <b>Open</b> status.');
          });
          frappe.call({
            method:
              'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.del_doc',
            args: {
              doctype: 'Canopi Other Documents Post Execution',
              docname: this.value,
            },
            freeze: true,

            callback: async r => {
              delete other_documents_post_execution_files[this.value];
              await load_other_documents_post_execution(frm);
              frm.reload_doc().then(() => {
                setTimeout(() => {
                  frm.scroll_to_field('other_documents_post_execution_html');
                }, 100);
              });
            },
          });
        },
      );
    },
  );
}

function other_documents_post_execution_download_events(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-s3-download').on(
    'click',
    function () {
      const s3_key = this.id;
      if (s3_key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3_key,
        });
      }
    },
  );
}

function other_documents_post_execution_upload_events(frm) {
  other_documents_post_execution_attach_file_event(frm);
  other_documents_post_execution_upload_all_event(frm);
}

let other_documents_post_execution_files = {};
function other_documents_post_execution_attach_file_event(frm) {
  let s3_button_index;
  let row_name;
  $('.' + frm.doc.name + '-other_documents_post_execution-attach-file').on(
    'click',
    function () {
      s3_button_index = this.id;
      row_name = $(this).closest('tr').attr('id');
      $('.' + frm.doc.name + '-other_documents_post_execution-attach-input')[
        s3_button_index
      ].click();
    },
  );

  $('.' + frm.doc.name + '-other_documents_post_execution-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';

        $('#other_documents_post_execution_f_' + row_name).html(
          '<a><span class="' +
            frm.doc.name +
            // eslint-disable-next-line max-len
            '-other_documents_post_execution-preview-file" style="display: inline-block; width: 8rem; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;">' +
            file[0].name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-other_documents_post_execution-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</span></a>',
        );

        // details[s3_button_index]['a45'] = file[0].name
        if (row_name !== 'undefined')
          other_documents_post_execution_files[row_name] = {};
        other_documents_post_execution_files[row_name].file = file[0];
        other_documents_post_execution_files[row_name].file_name = file[0].name;
        other_documents_post_execution_files[row_name].row_name = row_name;
        add_other_documents_post_execution_preview_event(frm);
        add_other_documents_post_execution_remove_file_event(frm);
      }
    },
  );
}

function add_other_documents_post_execution_preview_event(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-preview-file').on(
    'click',
    async function () {
      const rowname = $(this).closest('tr').attr('id');
      frappe.dom.freeze();
      const blobUrl = await fileToBlobAndUrl(
        other_documents_post_execution_files[rowname].file,
      );

      frappe.dom.unfreeze();
      window.open(blobUrl, '_blank');
    },
  );
}

function add_other_documents_post_execution_remove_file_event(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-remove-file').on(
    'click',
    function () {
      const rowname = $(this).closest('tr').attr('id');
      $(this).parent().html('');
      delete other_documents_post_execution_files[rowname];
    },
  );
}

function other_documents_post_execution_upload_all_event(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-upload-all').on(
    'click',
    () => {
      if (Object.keys(other_documents_post_execution_files).length > 0) {
        detailsS3Upload(
          frm,
          other_documents_post_execution_files,
          'Canopi Other Documents Post Execution',
          'other_documents_post_execution_html',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function other_documents_post_execution_dialog(frm, rowData, title) {
  const d = new frappe.ui.Dialog({
    title,
    fields: [
      {
        label: 'Name',
        reqd: 0,
        hidden: 1,
        fieldname: 'name',
        fieldtype: 'Data',
        default: rowData.name,
      },
      {
        label: 'Document Code',
        options: 'Canopi Document Code',
        filters: {
          other_documents_checklist: 1,
          product: frm.doc.product_name,
        },
        onchange: function (e) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: { doc_type: 'Canopi Document Code', doc_name: this.value },
            freeze: true,
            callback: r => {
              const field = d.get_field('document_name');
              field.value = r.message.description;
              field.refresh();
            },
          });
        },
        reqd: 1,
        fieldname: 'document_code',
        fieldtype: 'Link',
        default: rowData.document_code,
      },
      {
        label: 'Document Name',
        reqd: 0,
        fieldname: 'document_name',
        fieldtype: 'Data',
        default: rowData.document_name,
      },
      {
        label: 'Execution Version',
        reqd: 0,
        fieldname: 'execution_version',
        fieldtype: 'Data',
        default: rowData.execution_version,
      },
      {
        label: 'Single Signing Required',
        reqd: 0,
        fieldname: 'single_signing',
        fieldtype: 'Check',
        default: rowData.single_signing,
        onchange: function (e) {
          if (this.value === 1) {
            let field = d.get_field('signing_party');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = true;
            field.df.hidden = false;
            field.refresh();
          } else {
            let field = d.get_field('signing_party');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_text');
            field.df.reqd = false;
            field.df.hidden = true;
            field.refresh();
          }
        },
      },
      {
        label: 'Signing Party - ATSL',
        reqd: 1,
        options: ['ATSL', 'POA'],
        onchange: function (e) {
          if (this.value === 'ATSL') {
            let field = d.get_field('signatory');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else {
            let field = d.get_field('signatory');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('signatory_poa');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
          }
        },
        fieldname: 'signing_party',
        fieldtype: 'Select',
        default: rowData.signing_party,
      },
      {
        label: 'Signatory-ATSL',
        fieldtype: 'Link',
        options: 'User',
        fieldname: 'signatory',
        default: rowData.signatory,
      },
      {
        label: 'Signatory-POA',
        fieldtype: 'Link',
        hidden: 1,
        options: 'Canopi POA',
        fieldname: 'signatory_poa',
        default: rowData.signatory_poa,
      },
      {
        label: 'Signatory Name - Party',
        hidden: 1,
        fieldtype: 'Data',
        fieldname: 'signatory_text',
        default: rowData.signatory_text,
      },
      {
        label: 'Custody Location',
        fieldtype: 'Link',
        options: 'Location',
        fieldname: 'location',
        default: rowData.location,
      },
      {
        label: 'Execution Date',
        reqd: 0,
        fieldname: 'execution_date',
        fieldtype: 'Date',
        default: rowData.execution_date,
      },
      {
        label: 'Comments',
        reqd: 0,
        fieldname: 'comments',
        fieldtype: 'Small Text',
        default: rowData.comments,
      },
      {
        label: 'Status',
        reqd: 0,
        fieldname: 'status',
        fieldtype: 'Data',
        default: rowData.status ? rowData.status : 'Open',
        read_only: 1,
      },
      {
        label: 'Send Email',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'send_email',
        fieldtype: 'Select',
        default: rowData.send_email,
      },
      {
        label: 'Approval If Required',
        options: ['No', 'Yes'],
        reqd: 0,
        fieldname: 'approval_if_required',
        fieldtype: 'Select',
        default: rowData.approval_if_required,
      },
      {
        label: 'Send to Legal',
        options: ['No', 'Yes'],
        reqd: 0,
        read_only: 1,
        fieldname: 'send_to_legal',
        fieldtype: 'Select',
        default: rowData.send_to_legal,
      },
      {
        label: 'Remarks by Legal',
        options: '',
        reqd: 0,
        read_only: 1,
        fieldname: 'remarks_by_legal',
        fieldtype: 'Data',
        default: rowData.remarks_by_legal,
      },
      {
        label: 'Document Custody with',
        options: ['ATSL', 'Axis Bank', 'Client', 'Third Party'],
        reqd: 0,
        onchange: function (e) {
          if (this.value === 'Axis Bank') {
            let field = d.get_field('axis_bank_branch_name');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('third_party_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else if (this.value === 'Third Party') {
            let field = d.get_field('third_party_name');
            field.df.hidden = false;
            field.df.reqd = true;
            field.refresh();
            field = d.get_field('axis_bank_branch_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          } else {
            let field = d.get_field('third_party_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
            field = d.get_field('axis_bank_branch_name');
            field.df.hidden = true;
            field.df.reqd = false;
            field.refresh();
          }
        },
        fieldname: 'document_custody_with',
        fieldtype: 'Select',
        default: rowData.document_custody_with,
      },
      {
        label: 'Axis Bank Branch Name',
        reqd: 1,
        fieldtype: 'Data',
        fieldname: 'axis_bank_branch_name',
        depends_on: 'eval:doc.document_custody_with==="Axis Bank"',
        default: rowData.axis_bank_branch_name,
      },
      {
        label: 'Third Party Name',
        reqd: 1,
        fieldtype: 'Data',
        fieldname: 'third_party_name',
        depends_on: 'eval:doc.document_custody_with==="Third Party"',
        default: rowData.third_party_name,
      },
      {
        label: 'Document Type',
        options: ['Original', 'CTC', 'Photocopy'],
        fieldname: 'document_type',
        fieldtype: 'Select',
        default: rowData.document_type,
      },
      {
        label: 'Executed Location',
        fieldtype: 'Text',
        fieldname: 'executed_location',
        default: rowData.executed_location,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.save_other_documents_post_execution',
        args: {
          documentation_name: frm.doc.name,
          sub_name: values.name,
          document_name: values.document_name,
          document_code: values.document_code,
          execution_version: values.execution_version,
          single_signing: values.single_signing,
          signing_party: values.signing_party,
          signatory: values.signatory,
          signatory_poa: values.signatory_poa,
          signatory_text: values.signatory_text,
          location: values.location,
          execution_date: values.execution_date,
          comments: values.comments,
          status: values.status,
          send_email: values.send_email,
          approval_if_required: values.approval_if_required,
          send_to_legal: values.send_to_legal,
          remarks_by_legal: values.remarks_by_legal,
          document_custody_with: values.document_custody_with,
          axis_bank_branch_name: values.axis_bank_branch_name,
          third_party_name: values.third_party_name,
          document_type: values.document_type,
          executed_location: values.executed_location,
        },
        freeze: true,
        callback: async r => {
          if (r._server_messages) {
            frappe.throw(r._server_messages);
          } else {
            await load_other_documents_post_execution(frm);
            d.hide();
          }
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.scroll_to_field('other_documents_post_execution_html');
            }, 100);
          });
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  if (rowData.document_custody_with === 'Axis Bank') {
    let field = d.get_field('axis_bank_branch_name');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('third_party_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else if (rowData.document_custody_with === 'Third Party') {
    let field = d.get_field('third_party_name');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('axis_bank_branch_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  } else {
    let field = d.get_field('third_party_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('axis_bank_branch_name');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
  }
  if (rowData.single_signing === 1) {
    let field = d.get_field('signing_party');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_poa');
    field.df.hidden = true;
    field.df.reqd = false;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.reqd = true;
    field.df.hidden = false;
    field.refresh();
  } else {
    let field = d.get_field('signing_party');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory');
    field.df.hidden = false;
    field.df.reqd = true;
    field.refresh();
    field = d.get_field('signatory_text');
    field.df.reqd = false;
    field.df.hidden = true;
    field.refresh();

    if (rowData.signing_party) {
      if (rowData.signing_party === 'ATSL') {
        let field = d.get_field('signatory');
        field.df.hidden = false;
        field.df.reqd = true;
        field.refresh();
        field = d.get_field('signatory_poa');
        field.df.hidden = true;
        field.df.reqd = false;
        field.refresh();
      } else {
        let field = d.get_field('signatory');
        field.df.hidden = true;
        field.df.reqd = false;
        field.refresh();
        field = d.get_field('signatory_poa');
        field.df.hidden = false;
        field.df.reqd = true;
        field.refresh();
      }
    }
  }
  if (frappe.user.has_role('Legal Maker')) {
    const field = d.get_field('remarks_by_legal');
    field.df.read_only = false;
    field.refresh();
  }
  d.show();
}

function other_documents_post_execution_email(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-email').on(
    'click',
    function () {
      email_dialog('');
      s3_attachments = {};
      after_email_update_field = 'send_email';
      child_doctype = 'Canopi Other Documents Post Execution';
      $('input[class="other_documents_post_execution_chk"]:checked').each(
        function () {
          const attachment = [];
          attachment.name = this.value;
          attachment.file_name = $(
            '#' + this.value + '_other_documents_post_execution_file_name',
          ).val();
          attachment.file_url = $(
            '#' + this.value + '_other_documents_post_execution_file_path',
          ).val();
          s3_attachments[this.value] = {};
          s3_attachments[this.value].file_name = $(
            '#' + this.value + '_other_documents_post_execution_file_name',
          ).val();
          s3_attachments[this.value].file_url = $(
            '#' + this.value + '_other_documents_post_execution_file_path',
          ).val();
          render_attachment_rows(attachment);
        },
      );
    },
  );
}

function other_documents_post_execution_submit_to_checker(frm) {
  $(
    '.' + frm.doc.name + '-other_documents_post_execution-submit-to-checker',
  ).unbind();
  $(
    '.' + frm.doc.name + '-other_documents_post_execution-submit-to-checker',
  ).on('click', function () {
    const docnames = $(
      'input[class="other_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Other Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    docnames.forEach(doc => {
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            other_docs.status !== 'Open' &&
            other_docs.status.indexOf('Rejected') === -1,
        )
      ) {
        frappe.throw(
          'Some of the selected documents have already been sent for approval',
        );
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            (other_docs.file_name === null || other_docs.file_name === ''),
        )
      ) {
        frappe.throw('Please Upload File for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            other_docs.execution_version === '',
        )
      ) {
        frappe.throw('Execution Version mandatory for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            other_docs.signing_party === 'ATSL' &&
            other_docs.signatory === '',
        )
      ) {
        frappe.throw('Signatory mandatory for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            other_docs.signing_party === 'POA' &&
            other_docs.signatory_poa === '',
        )
      ) {
        frappe.throw('Signatory mandatory for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            other_docs.single_signing === '1' &&
            other_docs.signatory_text === '',
        )
      ) {
        frappe.throw('Signatory Party mandatory for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname && other_docs.location === '',
        )
      ) {
        frappe.throw('Location mandatory for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            (other_docs.execution_date === null ||
              other_docs.execution_date === ''),
        )
      ) {
        frappe.throw('Execution Date mandatory for selected documents');
      }
      if (
        other_documents_post_execution.some(
          other_docs =>
            other_docs.name === doc.docname &&
            other_docs.purpose_of_legal === 'Vetting' &&
            other_docs.remarks_by_legal === '',
        )
      ) {
        frappe.throw('Remarks by legal mandatory for selected documents');
      }
    });
    update_documents_status(docnames, 'Submit to Checker').then(async () => {
      approval_dialog(frm);
    });
  });
}

function other_documents_post_execution_send_to_legal(frm) {
  $('.' + frm.doc.name + '-other_documents_post_execution-send-to-legal').on(
    'click',
    function () {
      const selected_documents = [];
      $('input[class="other_documents_post_execution_chk"]:checked').each(
        function () {
          const status = $(
            '#' + this.value + '_other_documents_post_execution_status',
          ).text();

          if (status === 'Submit to Checker') {
            frappe.throw(
              'Some of the selected documents have been submit to checker',
            );
          }
          if (
            $(
              '#' +
                this.value +
                '_other_documents_post_execution_send_to_legal',
            ).text() === 'Yes'
          ) {
            frappe.throw(
              'Some of the selected documents have already been sent to legal',
            );
          }
          if (
            $(
              '#' + this.value + '_other_documents_post_execution_file_name',
            ).val() === ''
          ) {
            frappe.throw('Please Upload File For Selected Rows');
          }
          selected_documents.push({
            name: this.value,
          });
        },
      );
      if (selected_documents.length === 0)
        frappe.throw('Please Select atleast one Document');

      sendToLegalDialog(
        frm,
        selected_documents,
        'Canopi Other Documents Post Execution',
        'other_documents_post_execution_html',
      );
    },
  );
}

function load_loa_table(frm) {
  $(frm.fields_dict.loa_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    /* eslint-disable indent */
    const mapped_transaction_documents = transaction_documents
      ? transaction_documents.map(x => ({
          ...x,
          doctype: 'Canopi Transaction Documents',
        }))
      : [];
    const mapped_security_documents_pre_execution =
      security_documents_pre_execution
        ? security_documents_pre_execution.map(x => ({
            ...x,
            doctype: 'Canopi Security Documents',
          }))
        : [];
    const mapped_other_documents_pre_execution = other_documents_pre_execution
      ? other_documents_pre_execution.map(x => ({
          ...x,
          doctype: 'Canopi Other Documents',
        }))
      : [];
    const doc = [
      ...mapped_transaction_documents,
      ...mapped_security_documents_pre_execution,
      ...mapped_other_documents_pre_execution,
    ];
    /* eslint-enable indent */

    const loa_history = frm.doc.generated_loa_history
      ? JSON.parse(frm.doc.generated_loa_history)
      : [];
    frm.set_df_property(
      'loa_html',
      'options',
      frappe.render_template('loa_table', {
        doc,
        loa_history,
        id: frm.doc.name,
        docstatus: frm.doc.docstatus,
        islocal: frm.doc.__islocal,
      }),
    );

    frm.refresh_field('loa_html');
    setTimeout(() => {
      $('.' + frm.doc.name + '_loa_select_all').unbind();
      $('.' + frm.doc.name + '_loa_select_all').click(function () {
        $('.' + frm.doc.name + '_loa_select_row').prop('checked', this.checked);
      });

      $('.' + frm.doc.name + '_loa_download').unbind();
      $('.' + frm.doc.name + '_loa_download').click(function () {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: this.id,
          download_type: 'download',
        });
      });

      $('.' + frm.doc.name + '_generated_loa_select_all').unbind();
      $('.' + frm.doc.name + '_generated_loa_select_all').click(function () {
        $('.' + frm.doc.name + '_generated_loa_select_row').prop(
          'checked',
          this.checked,
        );
      });

      $('.' + frm.doc.name + '_cancel_loa').unbind();
      $('.' + frm.doc.name + '_cancel_loa').on('click', function () {
        cancel_loa(frm);
      });

      $('.' + frm.doc.name + '-generate-loa-button').unbind();
      $('.' + frm.doc.name + '-generate-loa-button').on('click', function () {
        add_loa_btn(frm);
      });
    }, 100);
  }
}

function cancel_loa(frm) {
  if (frm.is_dirty())
    frappe.throw('Please save the document befor Cancelling LOA');

  if (
    $(`input[class="${frm.doc.name}_generated_loa_select_row"]:checked`)
      .length <= 0
  ) {
    frappe.throw('Atleast one row must be selected to perform this action');
  }

  const loa_file_list = [];
  $(`.${frm.doc.name}_generated_loa_select_row`).each(function (i) {
    if (this.checked) {
      loa_file_list.push(this.value);
    }
  });

  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.cancel_loa_documents',
    args: {
      loa_file_list,
      docname: frm.doc.name,
    },
    freeze: true,
    callback: r => {
      frm.reload_doc();
    },
  });
}

// email dialig
const dialog = new frappe.ui.Dialog({
  title: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
  no_submit_on_enter: true,
  fields: get_fields(),
  primary_action_label: __('Send'),
  primary_action() {
    send_action();
  },
  secondary_action_label: __('Discard'),
  secondary_action() {
    dialog.hide();
  },
  size: 'large',
  minimizable: true,
});

function email_dialog() {
  $(dialog.$wrapper.find('.form-section').get(0)).addClass('to_section');
  prepare();
  dialog.show();
}

function prepare() {
  setup_multiselect_queries();
  setup_attach();
  setup_email();
  setup_email_template();
}

function setup_multiselect_queries() {
  ['recipients', 'cc', 'bcc'].forEach(field => {
    dialog.fields_dict[field].get_data = () => {
      const data = dialog.fields_dict[field].get_value();
      const txt = data.match(/[^,\s*]*$/)[0] || '';

      frappe.call({
        method: 'frappe.email.get_contact_list',
        args: { txt },
        callback: r => {
          dialog.fields_dict[field].set_data(r.message);
        },
      });
    };
  });
}

let attachments = [];

function setup_attach() {
  const fields = dialog.fields_dict;
  const attach = $(fields.select_attachments.wrapper);

  if (!attachments) {
    attachments = [];
  }

  let args = {
    folder: 'Home/Attachments',
    on_success: attachment => {
      attachments.push(attachment);
      render_attachment_rows(attachment);
    },
  };

  if (cur_frm) {
    args = {
      doctype: cur_frm.doctype,
      docname: cur_frm.docname,
      folder: 'Home/Attachments',
      on_success: attachment => {
        cur_frm.attachments.attachment_uploaded(attachment);
        render_attachment_rows(attachment);
      },
    };
  }

  $(`
    <label class="control-label">
    ${__('Select Attachments')}
    </label>
    <div class='attach-list'></div>
    <p class='add-more-attachments'>
    <button class='btn btn-xs btn-default'>
      ${frappe.utils.icon('small-add', 'xs')}&nbsp;
      ${__('Add Attachment')}
    </button>
    </p>
  `).appendTo(attach.empty());

  attach
    .find('.add-more-attachments button')
    .on('click', () => new frappe.ui.FileUploader(args));
  render_attachment_rows();
}

function render_attachment_rows(attachment = null) {
  const select_attachments = dialog.fields_dict.select_attachments;
  const attachment_rows = $(select_attachments.wrapper).find('.attach-list');
  if (attachment) {
    attachment_rows.append(get_attachment_row(attachment, true));
  } else {
    let files = [];
    if (attachments && attachments.length) {
      files = files.concat(attachments);
    }
    if (cur_frm) {
      files = files.concat(cur_frm.get_files());
    }

    if (files.length) {
      $.each(files, (i, f) => {
        if (!f.file_name) return;
        if (!attachment_rows.find(`[data-file-name="${f.name}"]`).length) {
          f.file_url = frappe.urllib.get_full_url(f.file_url);
          attachment_rows.append(get_attachment_row(f));
        }
      });
    }
  }
}

function get_attachment_row(attachment, checked = null) {
  return $(`<p class="checkbox flex">
    <label class="ellipsis" title="${attachment.file_name}">
    <input
      type="checkbox"
      data-file-name="${attachment.name}"
      ${checked ? 'checked' : ''}>
    </input>
    <span class="ellipsis">${attachment.file_name}</span>
    </label>
    &nbsp;
    <a href="${attachment.file_url}" target="_blank" class="btn-linkF">
    ${frappe.utils.icon('link-url')}
    </a>
  </p>`);
}

function setup_email() {
  // email
  const fields = dialog.fields_dict;

  $(fields.send_me_a_copy.input).on('click', () => {
    // update send me a copy (make it sticky)
    const val = fields.send_me_a_copy.get_value();
    frappe.db.set_value('User', frappe.session.user, 'send_me_a_copy', val);
    frappe.boot.user.send_me_a_copy = val;
  });
}

function setup_email_template() {
  dialog.fields_dict.email_template.df.onchange = () => {
    const email_template = dialog.fields_dict.email_template.get_value();
    if (!email_template) return;

    frappe.call({
      method:
        'frappe.email.doctype.email_template.email_template.get_email_template',
      args: {
        template_name: email_template,
        doc: cur_frm.doc,
        _lang: dialog.get_value('language_sel'),
      },
      callback(r) {
        prepend_reply(r.message, email_template);
      },
    });
  };
}

let reply_added = '';
function prepend_reply(reply, email_template) {
  if (reply_added === email_template) return;
  const content_field = dialog.fields_dict.content;
  const subject_field = dialog.fields_dict.subject;

  let content = content_field.get_value() || '';
  content = content.split('<!-- salutation-ends -->')[1] || content;

  content_field.set_value(`${reply.message}<br>${content}`);
  subject_field.set_value(reply.subject);

  reply_added = email_template;
}

function send_action() {
  const form_values = get_values();
  if (!form_values) return;

  const selected_attachments = $.map(
    $(dialog.wrapper).find('[data-file-name]:checked'),
    function (element) {
      return $(element).attr('data-file-name');
    },
  );
  send_email(form_values, selected_attachments);
}

function get_values() {
  const form_values = dialog.get_values();

  // cc
  for (let i = 0, l = dialog.fields.length; i < l; i++) {
    const df = dialog.fields[i];

    if (df.is_cc_checkbox) {
      // concat in cc
      if (form_values[df.fieldname]) {
        form_values.cc =
          (form_values.cc ? form_values.cc + ', ' : '') + df.fieldname;
        form_values.bcc =
          (form_values.bcc ? form_values.bcc + ', ' : '') + df.fieldname;
      }

      delete form_values[df.fieldname];
    }
  }

  return form_values;
}

function send_email(form_values, selected_attachments) {
  dialog.hide();

  if (!form_values.recipients) {
    frappe.msgprint(__('Enter Email Recipient(s)'));
    return;
  }

  if (cur_frm && !frappe.model.can_email(cur_frm.doc.doctype, cur_frm)) {
    frappe.msgprint(
      __('You are not allowed to send emails related to this document'),
    );
    return;
  }

  frappe.call({
    method: 'trusteeship_platform.custom_methods.documentation_send_mail',
    args: {
      recipients: form_values.recipients,
      cc: form_values.cc,
      bcc: form_values.bcc,
      subject: form_values.subject,
      content: form_values.content,
      send_me_a_copy: form_values.send_me_a_copy,
      attachments: selected_attachments,
      s3_attachments: JSON.stringify(s3_attachments),
      read_receipt: form_values.send_read_receipt,
      sender: form_values.sender,
      child_doctype,
      after_email_update_field,
    },
    freeze: true,
    freeze_message: __('Sending'),

    callback: r => {
      if (!r.exc) {
        frappe.utils.play_sound('email');

        if (r.message?.emails_not_sent_to) {
          frappe.msgprint(
            __('Email not sent to {0} (unsubscribed / disabled)', [
              frappe.utils.escape_html(r.message.emails_not_sent_to),
            ]),
          );
        }

        if (cur_frm) {
          cur_frm.reload_doc().then(() => {
            setTimeout(() => {
              if (child_doctype === 'Canopi Transaction Documents') {
                cur_frm.scroll_to_field('transaction_documents_html');
              }
              if (
                child_doctype === 'Canopi Transaction Documents Post Execution'
              ) {
                cur_frm.scroll_to_field(
                  'transaction_documents_post_execution_html',
                );
              }
              if (child_doctype === 'Canopi Security Documents') {
                cur_frm.scroll_to_field(
                  'security_documents_pre_execution_html',
                );
              }
            }, 100);
          });
        }
      } else {
        frappe.msgprint(
          __('There were errors while sending email. Please try again.'),
        );
      }
    },
  });
}

function get_fields() {
  const fields = [
    {
      label: __('To'),
      fieldtype: 'MultiSelect',
      reqd: 0,
      fieldname: 'recipients',
    },
    {
      fieldtype: 'Button',
      label: frappe.utils.icon('down'),
      fieldname: 'option_toggle_button',
      click: () => {
        toggle_more_options(false);
      },
    },
    {
      fieldtype: 'Section Break',
      hidden: 1,
      fieldname: 'more_options',
    },
    {
      label: __('CC'),
      fieldtype: 'MultiSelect',
      fieldname: 'cc',
    },
    {
      label: __('BCC'),
      fieldtype: 'MultiSelect',
      fieldname: 'bcc',
    },
    {
      label: __('Email Template'),
      fieldtype: 'Link',
      options: 'Email Template',
      fieldname: 'email_template',
    },
    { fieldtype: 'Section Break' },
    {
      label: __('Subject'),
      fieldtype: 'Data',
      reqd: 1,
      fieldname: 'subject',
      default: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
      length: 524288,
    },
    {
      label: __('Message'),
      fieldtype: 'Text Editor',
      fieldname: 'content',
    },
    { fieldtype: 'Section Break' },
    {
      label: __('Send me a copy'),
      fieldtype: 'Check',
      fieldname: 'send_me_a_copy',
      default: frappe.boot.user.send_me_a_copy,
    },
    {
      label: __('Send Read Receipt'),
      fieldtype: 'Check',
      fieldname: 'send_read_receipt',
    },

    { fieldtype: 'Column Break' },
    {
      label: __('Select Attachments'),
      fieldtype: 'HTML',
      fieldname: 'select_attachments',
    },
  ];

  // add from if user has access to multiple email accounts
  const email_accounts = frappe.boot.email_accounts.filter(account => {
    return (
      !in_list(
        ['All Accounts', 'Sent', 'Spam', 'Trash'],
        account.email_account,
      ) && account.enable_outgoing
    );
  });

  if (email_accounts.length) {
    fields.unshift({
      label: __('From'),
      fieldtype: 'Select',
      reqd: 1,
      fieldname: 'sender',
      options: email_accounts.map(function (e) {
        return e.email_id;
      }),
    });
  }

  return fields;
}

function toggle_more_options(show_options) {
  show_options = show_options || dialog.fields_dict.more_options.df.hidden;
  dialog.set_df_property('more_options', 'hidden', !show_options);

  const label = frappe.utils.icon(show_options ? 'up-line' : 'down');
  dialog.get_field('option_toggle_button').set_label(label);
}

function append_letter_from_in_serial_no(frm) {
  if (
    frm.doc.__islocal !== 1 &&
    frm.doc.letter_from !== frm.doc.serial_no.split('/')[0]
  ) {
    const serial_no = frm.doc.serial_no.split('/');
    serial_no[0] = frm.doc.letter_from;
    frm.set_value('serial_no', serial_no.join('/'));
  }
}

function set_serial_no_filter(frm) {
  // annexure b
  frm.set_query('serial_no_link', function () {
    return {
      filters: [
        ['Canopi Serial Number', 'doc_type', '=', 'Canopi Documentation'],
        ['Canopi Serial Number', 'letter_type', '=', 'Annexure B'],
      ],
    };
  });

  // LOA
  frm.set_query('loa_serial_link', function () {
    return {
      filters: [
        ['Canopi Serial Number', 'doc_type', '=', 'Canopi Documentation'],
        ['Canopi Serial Number', 'letter_type', '=', 'Letter Of Authority'],
      ],
    };
  });
}

function validate_checker_and_maker(frm) {
  if (
    frm.doc.tl_representative &&
    frm.doc.ops_servicing_rm &&
    frm.doc.tl_representative === frm.doc.ops_servicing_rm
  ) {
    frm.set_value('ops_servicing_rm', '');
    frm.set_value('tl_representative', '');
    frappe.throw(
      '<b>Ops & Servicing RM</b> and <b>TL Representative</b> cannot be same.',
    );
  }
}

function add_percent_pill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let title_element = $(`h3[title="${frm.doc.name}"]`).parent();
    title_element = title_element[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.completion_percent + '%';
    pillspan.appendChild(textspan);

    title_element.appendChild(pillspan);
  }
}

async function sendAnnexureBApproval(frm) {
  await frappe
    .call({
      method:
        // eslint-disable-next-line max-len
        'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.validate_annexure_b',
      args: {
        docname: frm.doc.name,
      },
      freeze: true,
    })
    .then(r => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.create_approval',
        args: {
          docname: frm.doc.name,
          is_annexure_b: 1,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            frappe.set_route('Form', 'Canopi Approval', r.message);
          });
        },
      });
    });
}

function configureAnnexureBApprovalBtn(frm) {
  if (
    frm.doc.annexure_b_status !== 'Draft' &&
    !frm.doc.annexure_b_status?.includes('Rejected')
  ) {
    frm.set_df_property('annexure_b_approval', 'hidden', 1);
  } else {
    frm.set_df_property('annexure_b_approval', 'hidden', 0);
  }
}

function configureViewAttachAnnexureBBtn(frm) {
  frappe.call({
    method:
      // eslint-disable-next-line max-len
      'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.is_annexure_b_approval_completed',
    args: { docname: frm.doc.name },
    freeze: true,
    callback: r => {
      if (frm.doc.annexure_b_status === 'Approved' && r.message === true) {
        frm.set_df_property('view_annexure_b', 'label', 'Download Annexure B');
        frm.set_df_property('attach_annexure_b', 'hidden', 0);
      } else {
        frm.set_df_property('attach_annexure_b', 'hidden', 1);
      }
    },
  });
}

function configureAttachAnnexureBBtn(frm) {
  if (frm.doc.annexure_b_s3_key) {
    $('button[data-fieldname="attach_annexure_b"]').addClass('btn-primary');
    frm.set_df_property(
      'attach_annexure_b',
      'label',
      'Re Attach Signed Annexure B',
    );

    const deleteIcon = document.createElement('button');
    deleteIcon.classList.add(
      'btn',
      'btn-danger',
      'btn-xs',
      'btn-remove-perm',
      'delete_annexure_b',
    );
    deleteIcon.innerHTML = frappe.utils.icon('delete', 'sm');
    deleteIcon.style.margin = '5px';

    const attach_btn = $("button[data-fieldname='attach_annexure_b']");

    deleteIcon.addEventListener('click', function () {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.delete_annexure_b_from_s3',
        args: {
          docname: frm.doc.name,
          key: frm.doc.annexure_b_s3_key,
        },
        callback: r => {
          deleteIcon.remove();
          frm.reload_doc();
        },
      });
    });

    if ($('.delete_annexure_b').length === 0) {
      attach_btn.parent().append(deleteIcon);
    }
  } else {
    $('button[data-fieldname="attach_annexure_b"]').removeClass('btn-primary');
    frm.set_df_property(
      'attach_annexure_b',
      'label',
      'Attach Signed Annexure B',
    );
  }
}

function uploadAnnexureBToS3(frm) {
  const fileInput = document.createElement('input');
  fileInput.type = 'file';
  fileInput.click();

  fileInput.addEventListener('change', async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      frappe.dom.freeze();
      const xhr = new XMLHttpRequest();
      const api =
        // eslint-disable-next-line max-len
        '/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.upload_annexure_b_to_s3';
      const formData = new FormData();
      formData.append('file', file[0]);
      formData.append(
        'key',
        `${frm.doc.doctype}/${frm.doc.name}/Annexure B/${file[0].name}`,
      );
      formData.append('docname', frm.doc.name);
      xhr.open('POST', api, true);
      xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
      xhr.onreadystatechange = function (e) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            frappe.dom.unfreeze();
          } else {
            frappe.dom.unfreeze();
          }
        } else {
          frappe.dom.unfreeze();
          try {
            const response = JSON.parse(xhr.responseText);
            const errorMessage = JSON.parse(response?._server_messages);
            if (errorMessage) {
              frappe.msgprint(errorMessage, 'Error');
            }
          } catch (error) {
            if (xhr.status === 413) {
              displayHtmlDialog();
            }
          }
        }
      };
      xhr.send(formData);
    }
  });
}

function renderDownloadAnnexureBBtn(frm) {
  $(frm.fields_dict.download_signed_annexure_b_html.wrapper).empty();
  if (frm.doc.annexure_b_s3_key) {
    $(frm.fields_dict.download_signed_annexure_b_html.wrapper)
      .html(
        frappe.render_template('read_only_link_btn', {
          title: 'Download Attached Annexure B',
          value: frm.doc.annexure_b_s3_key.substring(
            frm.doc.annexure_b_s3_key.lastIndexOf('/') + 1,
          ),
        }),
      )
      .find('a')
      .on('click', function () {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: frm.doc.annexure_b_s3_key,
        });
      });
  }
}

function headerButtons(frm) {
  if (frm.doc.operations) {
    frm.add_custom_button('Main', () => {
      window.open(`/app/atsl-mandate-list/${frm.doc.operations}`);
    });
  }
  if (!frm.doc.__islocal && frm.doc.operations) {
    let transactionDetails;
    if (frm.doc.product_name?.toUpperCase() === 'FA') {
      transactionDetails = 'Canopi Transaction Details-FA';
    } else {
      transactionDetails = 'Transaction Details';
    }
    frm.add_custom_button(
      transactionDetails,
      () => {
        const linkedDoctypes = {
          [transactionDetails]: {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, transactionDetails, r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Pre-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Pre Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Pre Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Documentation',
      () => {
        // eslint-disable-next-line no-unused-vars
        const linkedDoctypes = {
          'Canopi Documentation': {
            fieldname: ['operations'],
          },
        };
        if (!frm.doc.__islocal) {
          frm.scroll_to_field('client_name');
        }
      },
      'Operations',
    );
    frm.add_custom_button(
      'Post-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Post Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Post Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
  }
}

function collapseAnnextureB(frm) {
  frm.fields_dict.annexure_b_checklist_section.collapse(false);
}

function redirectDoctype(frm, doctype, response) {
  if (response.message[doctype]) {
    const outputString = doctype
      .replace(/([a-z0-9])([A-Z])/g, '$1 $2') // Insert space before capital letters if not already separated
      .toLowerCase() // Convert the entire string to lowercase
      .replace(/\s+/g, '-'); // Replace spaces with hyphens
    window.open(`/app/${outputString}/${response.message[doctype][0].name}`);
  } else {
    frappe.model.with_doctype(doctype, function () {
      const source = frm.doc;
      const targetDoc = frappe.model.get_new_doc(doctype);

      targetDoc.operations = source.operations;
      targetDoc.person_name = source.person_name;
      targetDoc.cnp_customer_code = source.cnp_customer_code;
      targetDoc.mandate_name = source.mandate_name;
      targetDoc.opportunity = source.opportunity;
      targetDoc.product_name = source.product_name;

      if (
        doctype === 'Canopi Post Execution Checklist' ||
        doctype === 'Canopi Pre Execution Checklist' ||
        doctype === 'Canopi Documentation'
      ) {
        targetDoc.ops_servicing_rm = source.ops_servicing_rm;
        targetDoc.tl_representative = source.tl_representative;
      }
      frappe.ui.form.make_quick_entry(doctype, null, null, targetDoc);
    });
  }
}

async function fileUpload(frm) {
  $(frm.fields_dict.date_of_im_upload_html.wrapper).empty();
  frm.refresh_field('date_of_im_upload_html');
  setTimeout(() => {
    const file = ' ';
    renderAttachmentTemplate(frm, file);
  }, 500);
}
function renderAttachmentTemplate(frm, file) {
  frm.set_df_property(
    'date_of_im_upload_html',
    'options',
    frappe.render_template('date_of_im_upload', {
      doc: frm.doc,
      id: frm.doc.name,
      docstatus: frm.doc.docstatus,
    }),
  );
  frm.refresh_field('date_of_im_upload_html');
  addUploadEvents(frm);
}
let details = {};
let fileIndex = 0;
function addUploadEvents(frm) {
  addDateOfIMUploadAttachFileEvent(frm);
  addDateOfIMUploadUploadAllEvent(frm);
}

function addDateOfIMUploadAttachFileEvent(frm) {
  fileIndex = 0;
  details = {};
  let s3ButtonIndex;
  $('.' + frm.doc.name + '-im-attach-file').on('click', function () {
    s3ButtonIndex = 0;
    $('.' + frm.doc.name + '-im-attach-input')[s3ButtonIndex].click();
  });
  if (frm.doc.file_name) {
    const fileNames = frm.doc.file_name.split(',');
    for (let i = 0; i < fileNames.length; i++) {
      if (fileNames[i] !== '') {
        const rowName = 'file_' + fileIndex;
        let html =
          '<tr id="' +
          rowName +
          '"><td><a id="' +
          fileNames[i] +
          '" class="' +
          frm.doc.name +
          '-im-s3-download" style="display: inline-block; ">' +
          fileNames[i] +
          '</a>&nbsp;<a alt="' +
          fileNames[i] +
          '"';
        if (frm.doc.docstatus === 0) {
          html +=
            ' class="' +
            frm.doc.name +
            '-im-delete-file"><span>' +
            frappe.utils.icon('delete', 'sm');
        }
        html += '</a></td></tr>';

        $('#date_of_im_upload_file_list').append(html);
      }
    }
    addDateOfIMUploadDownloadEvents(frm);
    addDateOfIMUploadDeleteFileEvent(frm);
  }
  $('.' + frm.doc.name + '-im-attach-input').change(async event => {
    const files = [...event.target.files];
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      if (file) {
        event.target.value = '';

        const rowName = 'file_' + fileIndex;

        $('#date_of_im_upload_file_list').append(
          '<tr id="' +
            rowName +
            '"><td><a class="' +
            frm.doc.name +
            '-im-preview-file" style="display: inline-block; ">' +
            file.name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-im-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</a></td></tr>',
        );

        details[rowName] = {};
        details[rowName].file = file;
        details[rowName].file_name = file.name;
        details[rowName].row_name = rowName;
        addDateOfIMUploadPreviewEvent(frm);
        addDateOfIMUploadRemoveFileEvent(frm);
        fileIndex++;
      }
    }
  });
}

function addDateOfIMUploadDownloadEvents(frm) {
  $('.' + frm.doc.name + '-im-s3-download').on('click', function () {
    let s3Key = this.id;
    if (s3Key) {
      s3Key =
        window.location.hostname +
        '/Canopi Documentation/Date of IM Upload/' +
        frm.doc.name +
        '/' +
        s3Key;
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: s3Key,
      });
    }
  });
}
function addDateOfIMUploadPreviewEvent(frm) {
  $('.' + frm.doc.name + '-im-preview-file').on('click', async function () {
    frappe.dom.freeze();
    const rowname = $(this).closest('tr').attr('id');
    const blobUrl = await fileToBlobAndUrl(details[rowname].file);
    frappe.dom.unfreeze();

    window.open(blobUrl, '_blank');
  });
}
function addDateOfIMUploadRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-im-remove-file').on('click', function () {
    const rowName = $(this).closest('tr').attr('id');
    $(this).parent().remove();
    delete details[rowName];
  });
}
function addDateOfIMUploadDeleteFileEvent(frm) {
  $('.' + frm.doc.name + '-im-delete-file').on('click', function () {
    const rowName = $(this).closest('tr').attr('id');

    const fileName = $(this).attr('alt');
    $(this).parent().remove();
    details[rowName] = {};

    frappe.call({
      method:
        'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.delete_file',
      freeze: true,
      args: {
        docname: frm.doc.name,
        file_name: fileName,
        doctype: 'Canopi Documentation',
      },
      callback: r => {
        frm.reload_doc().then(() => {
          setTimeout(() => {
            frm.fields_dict.date_of_im_upload_section.collapse(false);
          }, 500);
        });
        frappe.show_alert(
          {
            message: 'Deleted',
            indicator: 'Red',
          },
          5,
        );
      },
    });
  });
}
function addDateOfIMUploadUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-im-upload-all').on('click', () => {
    if (Object.keys(details).length > 0) {
      dateOfIMS3Upload(
        frm,
        details,
        'Canopi Documentation',
        'date_of_im_upload_section',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

async function dateOfIMS3Upload(frm, assetDetails, doctype, field) {
  frappe.dom.freeze();
  const docnames = Object.keys(assetDetails);
  for (let i = 0; i < docnames.length; i++) {
    const element = docnames[i];
    const file = assetDetails[element].file;
    await uploadAssetFileIM(assetDetails[element].row_name, frm, file, doctype);
  }
  frappe.dom.freeze();

  frm.refresh_fields();
  frm.reload_doc().then(() => {
    frappe.dom.unfreeze();
    setTimeout(() => {
      frm.fields_dict.date_of_im_upload_section.collapse(false);
    }, 500);
  });

  frappe.show_alert(
    {
      message: __('Uploaded'),
      indicator: 'green',
    },
    5,
  );
}

function uploadAssetFileIM(row_name, frm, file, doctype) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    const api =
      '/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.s3_upload';
    const formData = new FormData();
    formData.append('file', file);
    formData.append('docname', frm.doc.name);
    formData.append('doctype', doctype);
    formData.append('file_name', file.name);
    xhr.open('POST', api, true);
    xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          frappe.dom.unfreeze();
          resolve(1);
        } else {
          frappe.dom.unfreeze();
          reject(xhr.statusText);
        }
      } else {
        frappe.dom.unfreeze();
        try {
          const response = JSON.parse(xhr.responseText);
          const errorMessage = JSON.parse(response?._server_messages);
          if (errorMessage) {
            frappe.msgprint(errorMessage, 'Error');
          }
        } catch (error) {
          if (xhr.status === 413) {
            displayHtmlDialog();
          }
        }
      }
    };
    xhr.send(formData);
  });
}

function calculateTotalIssueSize(frm) {
  const issueSize = frm.doc.issue_size_in_units || 0;
  const greenShoeValue = frm.doc.green_shoe_value || 0;
  const totalIssueSize = issueSize + greenShoeValue;
  frm.set_value('total_issue_size_in_cr_including_green_shoe', totalIssueSize);
}

function fileToBlobAndUrl(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      const blob = new Blob([reader.result], { type: file.type });
      const blobUrl = URL.createObjectURL(blob);
      resolve(blobUrl);
    };

    reader.onerror = error => {
      reject(error);
    };

    reader.readAsArrayBuffer(file);
  });
}

async function detailsS3Upload(frm, assetDetails, doctype, field) {
  frappe.dom.freeze();
  const docnames = Object.keys(assetDetails);
  for (let i = 0; i < docnames.length; i++) {
    const element = docnames[i];
    const file = assetDetails[element].file;
    await uploadAssetFileOthers(
      assetDetails[element].row_name,
      frm,
      file,
      doctype,
    );
  }
  frappe.dom.freeze();
  transaction_documents_pre_execution_files = {};
  transaction_documents_post_execution_files = {};
  security_documents_pre_execution_files = {};
  security_documents_post_execution_files = {};
  condition_subsequent_files = {};
  condition_precedent_part_a_files = {};
  condition_precedent_part_b_files = {};
  other_documents_pre_execution_files = {};
  other_documents_post_execution_files = {};
  pre_execution_documents_files = {};
  post_execution_documents_files = {};
  frm.reload_doc().then(() => {
    frappe.dom.unfreeze();
    setTimeout(() => {
      frm.scroll_to_field(field);
    }, 100);
  });

  frappe.show_alert(
    {
      message: __('Uploaded'),
      indicator: 'green',
    },
    5,
  );
}

function uploadAssetFileOthers(row_name, frm, file, doctype) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    const api =
      '/api/method/trusteeship_platform.custom_methods.documentation_s3_upload';
    const formData = new FormData();
    formData.append('file', file);
    formData.append('row_name', row_name);
    formData.append('docname', frm.doc.name);
    formData.append('doctype', doctype);
    formData.append('file_name', file.name);
    xhr.open('POST', api, true);
    xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          frappe.dom.unfreeze();
          resolve(1);
        } else {
          frappe.dom.unfreeze();
          reject(xhr.statusText);
        }
      } else {
        frappe.dom.unfreeze();
        try {
          const response = JSON.parse(xhr.responseText);
          const errorMessage = JSON.parse(response?._server_messages);
          if (errorMessage) {
            frappe.msgprint(errorMessage, 'Error');
          }
        } catch (error) {
          if (xhr.status === 413) {
            displayHtmlDialog();
          }
        }
      }
    };
    xhr.send(formData);
  });
}

function displayHtmlDialog() {
  if (frappe.boot.max_file_size) {
    frappe.msgprint({
      title: __('File too big'),
      indicator: 'red',
      message: __(
        'File size exceeded the maximum allowed size of ' +
          bytesToMB(frappe.boot.max_file_size),
      ),
    });
  }
}

function bytesToMB(bytes) {
  if (bytes === 0) return '0 MB';

  const mb = bytes / (1024 * 1024);
  return `${mb.toFixed(0)} MB`;
}
