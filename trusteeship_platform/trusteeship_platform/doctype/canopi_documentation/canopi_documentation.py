# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import json
from datetime import datetime

import frappe
from frappe.desk.form import assign_to
from frappe.desk.reportview import get_filters_cond
from frappe.model.document import Document
from frappe.utils import cstr

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    calculate_completion_percent,
    delete_from_s3,
    generate_qrcode,
    update_tl_ops,
    validate_s3_settings,
    upload_file_to_s3,
    asign_task,
)

from trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval import (  # noqa: 501 isort:skip
    get_approval_count_cond,
)


class CanopiDocumentation(Document):
    def validate(self):
        get_qrcode(self)
        doc_type_list = [
            "Canopi Pre Execution Checklist",
            "Canopi Post Execution Checklist",
        ]
        update_tl_ops(self, doc_type_list)

    def on_submit(self):
        validate_submit(self)

    def after_insert(self):
        generate_serial_number(self)
        self.save()
        create_all_documents(self)


def create_all_documents(self):
    operations_doc = frappe.get_doc("ATSL Mandate List", self.operations)  # noqa: 501

    item_doc = frappe.get_doc("Item", operations_doc.product_name)

    create_security_documents(
        self, operations_doc, item_doc.cnp_security_documents_checklist
    )

    related_doctypes_map = [
        {
            "doctype": "Canopi Transaction Documents",
            "item_checklist": item_doc.cnp_transaction_documents_checklist,
        },
        {
            "doctype": "Canopi Other Documents",
            "item_checklist": item_doc.cnp_other_documents_checklist,
        },
        {
            "doctype": "Canopi Condition Precedent Part A",
            "item_checklist": item_doc.cnp_condition_precedent_part_a_checklist,  # noqa: 501
        },
        {
            "doctype": "Canopi Condition Precedent Part B",
            "item_checklist": item_doc.cnp_condition_precedent_part_b_checklist,  # noqa: 501
        },
        {
            "doctype": "Canopi Condition Subsequent",
            "item_checklist": item_doc.cnp_condition_subsequent_checklist,
        },
        {
            "doctype": "Canopi Pre Execution Documents",
            "item_checklist": item_doc.cnp_documents_checklist,
        },
        {
            "doctype": "Canopi Post Execution Documents",
            "item_checklist": item_doc.cnp_sebi_application_checklist,
        },
    ]

    for doc in related_doctypes_map:
        create_related_documents(self, doc["doctype"], doc["item_checklist"])


def create_related_documents(self, doctype, checklist):
    for item in checklist:
        newdoc = frappe.new_doc(doctype)
        newdoc.canopi_documentation_name = self.name
        newdoc.document_code = item.document_code
        newdoc.document_name = item.description
        if hasattr(newdoc, "is_dta"):
            newdoc.is_dta = item.is_dta

        if hasattr(newdoc, "category"):
            newdoc.category = item.category

        newdoc.insert()


def create_security_documents(self, operations_doc, checklist):
    canopi_documentation_name = self.name

    product_name = operations_doc.product_name
    opportunity = operations_doc.opportunity

    pricing_policy_doc = frappe.get_doc(
        "Canopi Pricing Policy", opportunity + "-" + product_name
    )
    for element in checklist:
        dict = {
            "Movable Asset": element.movable,
            "Immovable Asset": element.immovable,
            "Pledge": element.pledge,
            "Personal Guarantee": element.personal_guarantee,
            "Corporate Guarantee": element.corporate_guarantee,
            "NDU": element.ndu,
            "Intangible": element.intangible,
            "Motor Vehicle": element.motor,
            "Financial Asset": element.financial,
            "Assignment of Rights": element.assignment_of_rights,
            "Current Asset": element.current,
            "Debt Service Reserve Account (DSRA)": element.dsra,
            "Others": element.others,
        }

        if (
            (element.secured == 1 and pricing_policy_doc.no_of_securities > 0)
            or (
                element.whether_public_issue == 1
                and pricing_policy_doc.whether_public_issue_check > 0
            )
            or (
                element.listed == 1
                and pricing_policy_doc.listed_yes_no == "Yes"  # noqa: 501
            )
        ):
            for security_type in operations_doc.security_type:
                existing_security_type = (
                    security_type.new_security_type
                    if security_type.change_in_security == "Yes"
                    else security_type.existing_security_type  # noqa: 501
                )

                for key, value in dict.items():
                    if key == existing_security_type and value == 1:
                        newdoc = frappe.new_doc(
                            "Canopi Security Documents"
                        )  # noqa: 501
                        newdoc.canopi_documentation_name = (
                            canopi_documentation_name  # noqa: 501
                        )
                        newdoc.security_type = existing_security_type
                        newdoc.document_name = element.description
                        newdoc.document_code = element.document_code
                        newdoc.insert()


def validate_submit(self):
    if self.product_name.upper() == "DTE" and (not self.annexure_b_s3_key):
        frappe.throw(
            "Please attach a signed copy of Annexure B before submitting this document."  # noqa: 501
        )


def get_qrcode(self):
    if self.ops_servicing_rm and self.tl_representative:
        string_to_qrcode = f"CL Code: {self.operations} \nDocument ID: {self.name} \nMaker ID: {self.ops_servicing_rm} \nChecker ID: {self.tl_representative}"  # noqa: 501
        self.qr_code = generate_qrcode(string_to_qrcode)


def generate_serial_number(self):
    if self.product_name.upper() == "DTE":
        if not self.serial_no_link:
            frappe.throw("Serial Number is mandatory field.")
        else:
            doc = frappe.get_doc("Canopi Serial Number", self.serial_no_link)

            current_fiscal_end_date = frappe.defaults.get_defaults().get(
                "year_end_date"
            )
            current_fiscal_end_date = datetime.strptime(
                current_fiscal_end_date, "%Y-%m-%d"
            ).date()

            current_fiscal_start_date = frappe.defaults.get_defaults().get(
                "year_start_date"
            )
            current_fiscal_start_date = datetime.strptime(
                current_fiscal_start_date, "%Y-%m-%d"
            ).date()

            # reset serial number if fiscal year changes
            if current_fiscal_end_date > doc.fiscal_end_date:
                doc.serial_no = "000000"
                doc.fiscal_end_date = current_fiscal_end_date

            serial_no = int(doc.serial_no) + 1
            doc.serial_no = str(serial_no).rjust(6, "0")
            self.serial_no = f'{self.letter_from}/CO/{self.operations}/{str(current_fiscal_start_date.year)[2:]}-{str(current_fiscal_end_date.year)[2:]}/{str(serial_no).rjust(6,"0")}'  # noqa: 501
            doc.save()


@frappe.whitelist()
def add_doc(doctype, docname):
    doc = frappe.new_doc(doctype)
    doc.canopi_documentation_name = docname
    doc.insert()


@frappe.whitelist()
def del_doc(doctype, docname):
    canopi_documentation = frappe.get_doc(doctype, docname)
    frappe.delete_doc(doctype, docname)
    calculate_completion_percent(
        canopi_documentation.canopi_documentation_name
    )  # noqa: 501


@frappe.whitelist()
def initiate_post_execution(doctype, newdoctype, docname, documentation_name):
    dname = json.loads(docname)
    for x in dname:
        doc = frappe.get_doc(doctype, x)
        if doc.for_post_execution != "Yes":
            if (
                doctype == "Canopi Transaction Documents"
                or doctype == "Canopi Security Documents"
                or doctype == "Canopi Other Documents"
            ):
                if doc.status != "LOA Generated":
                    frappe.throw("Status must be LOA Generated.")
                if doc.send_to_legal != "Yes":
                    frappe.throw("Send To Legal must be Yes")
                if doc.purpose_of_legal == "Vetting":
                    if doc.remarks_by_legal == "":
                        frappe.throw("Remarks by Legal Require")
            documentation_name = doc.canopi_documentation_name
            document_name = doc.document_name
            document_code = doc.document_code
            if doctype == "Canopi Security Documents":
                security_type = doc.security_type
            doc.for_post_execution = "Yes"
            doc.save()
            newdoc = frappe.new_doc(newdoctype)
            newdoc.canopi_documentation_name = documentation_name
            newdoc.document_name = document_name
            newdoc.document_code = document_code
            if doctype == "Canopi Transaction Documents":
                newdoc.is_dta = doc.is_dta
                newdoc.is_for_document_execution = (
                    doc.is_for_document_execution
                )  # noqa: 501
            if doctype == "Canopi Security Documents":
                newdoc.security_type = security_type
            newdoc.insert()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_transaction_documents(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    remarks_by_legal="",
    status="",
    approval_if_required="",
    send_email="",
    for_post_execution="",
    is_dta=False,
    is_for_document_execution=False,
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Transaction Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Transaction Documents", {"name": sub_name}
            )  # noqa: 501
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.is_dta = is_dta
            doc.is_for_document_execution = is_for_document_execution
            doc.save()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc(
                    "Canopi Transaction Documents Post Execution"
                )  # noqa: 501
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.insert()
    else:
        if (
            frappe.db.count(
                "Canopi Transaction Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Transaction Documents")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.is_dta = is_dta
            doc.is_for_document_execution = is_for_document_execution
            doc.insert()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc(
                    "Canopi Transaction Documents Post Execution"
                )  # noqa: 501
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.insert()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_transaction_documents_post_execution(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    execution_version="",
    single_signing="",
    signing_party="",
    signatory="",
    signatory_poa="",
    signatory_text="",
    location="",
    execution_date="",
    comments="",
    status="",
    send_email="",
    approval_if_required="",
    send_to_legal="",
    remarks_by_legal="",
    document_custody_with="",
    axis_bank_branch_name="",
    third_party_name="",
    executed_location="",
    document_type="",
    is_dta=False,
    is_for_document_execution=False,
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Transaction Documents Post Execution",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if single_signing == "1":
                signatory = ""
                signatory_poa = ""
                signing_party = ""
            else:
                signatory_text = ""

            if signing_party == "ATSL":
                signatory_poa = ""
            else:
                signatory = ""

            doc = frappe.get_doc(
                "Canopi Transaction Documents Post Execution",
                {"name": sub_name},  # noqa: 501
            )
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.single_signing = single_signing
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_poa = signatory_poa
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.document_custody_with = document_custody_with
            doc.axis_bank_branch_name = axis_bank_branch_name
            doc.third_party_name = third_party_name
            doc.document_type = document_type
            doc.executed_location = executed_location

            doc.is_dta = is_dta
            doc.is_for_document_execution = is_for_document_execution
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Transaction Documents Post Execution",
                {
                    "canopi_documentation_name": documentation_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if single_signing == 1:
                signatory = ""
                signatory_poa = ""
                signing_party = ""
            else:
                signatory_text = ""

            if signing_party == "ATSL":
                signatory_poa = ""
            else:
                signatory = ""

            doc = frappe.new_doc(
                "Canopi Transaction Documents Post Execution"
            )  # noqa: 501
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.single_signing = single_signing
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_poa = signatory_poa
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.document_custody_with = document_custody_with
            doc.axis_bank_branch_name = axis_bank_branch_name
            doc.third_party_name = third_party_name
            doc.document_type = document_type
            doc.executed_location = executed_location
            doc.is_dta = is_dta
            doc.is_for_document_execution = is_for_document_execution
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_security_documents_pre_execution(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    remarks_by_legal="",
    status="",
    approval_if_required="",
    send_email="",
    for_post_execution="",
    security_type="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Security Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "security_type": security_type,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Security Documents", {"name": sub_name}
            )  # noqa: 501
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.security_type = security_type
            doc.save()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc(
                    "Canopi Security Documents Post Execution"
                )  # noqa: 501
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.security_type = security_type
                newdoc.insert()
    else:
        if (
            frappe.db.count(
                "Canopi Security Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "security_type": security_type,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Security Documents")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.security_type = security_type
            doc.save()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc(
                    "Canopi Security Documents Post Execution"
                )  # noqa: 501
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.security_type = security_type
                newdoc.insert()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_security_documents_post_execution(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    execution_version="",
    single_signing="",
    signing_party="",
    signatory="",
    signatory_poa="",
    signatory_text="",
    location="",
    execution_date="",
    comments="",
    status="",
    send_email="",
    approval_if_required="",
    send_to_legal="",
    remarks_by_legal="",
    document_custody_with="",
    axis_bank_branch_name="",
    third_party_name="",
    executed_location="",
    document_type="",
    security_type="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Security Documents Post Execution",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "security_type": security_type,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if single_signing == "1":
                signatory = ""
                signatory_poa = ""
                signing_party = ""
            else:
                signatory_text = ""

            if signing_party == "ATSL":
                signatory_poa = ""
            else:
                signatory = ""

            doc = frappe.get_doc(
                "Canopi Security Documents Post Execution", {"name": sub_name}
            )
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.single_signing = single_signing
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_poa = signatory_poa
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.security_type = security_type
            doc.document_custody_with = document_custody_with
            doc.axis_bank_branch_name = axis_bank_branch_name
            doc.third_party_name = third_party_name
            doc.document_type = document_type
            doc.executed_location = executed_location
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Security Documents Post Execution",
                {
                    "canopi_documentation_name": documentation_name,
                    "security_type": security_type,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if single_signing == "1":
                signatory = ""
                signatory_poa = ""
                signing_party = ""
            else:
                signatory_text = ""

            if signing_party == "ATSL":
                signatory_poa = ""
            else:
                signatory = ""

            doc = frappe.new_doc("Canopi Security Documents Post Execution")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.single_signing = single_signing
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_poa = signatory_poa
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.security_type = security_type
            doc.document_custody_with = document_custody_with
            doc.axis_bank_branch_name = axis_bank_branch_name
            doc.third_party_name = third_party_name
            doc.document_type = document_type
            doc.executed_location = executed_location
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_other_documents_pre_execution(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    remarks_by_legal="",
    status="",
    approval_if_required="",
    send_email="",
    for_post_execution="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Other Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc("Canopi Other Documents", {"name": sub_name})
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.save()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc(
                    "Canopi Other Documents Post Execution"
                )  # noqa: 501
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.insert()
    else:
        if (
            frappe.db.count(
                "Canopi Other Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Other Documents")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.save()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc(
                    "Canopi Other Documents Post Execution"
                )  # noqa: 501
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.insert()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_other_documents_post_execution(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    execution_version="",
    single_signing="",
    signing_party="",
    signatory="",
    signatory_poa="",
    signatory_text="",
    location="",
    execution_date="",
    comments="",
    status="",
    send_email="",
    approval_if_required="",
    send_to_legal="",
    remarks_by_legal="",
    document_custody_with="",
    axis_bank_branch_name="",
    third_party_name="",
    executed_location="",
    document_type="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Other Documents Post Execution",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if single_signing == "1":
                signatory = ""
                signatory_poa = ""
                signing_party = ""
            else:
                signatory_text = ""

            if signing_party == "ATSL":
                signatory_poa = ""
            else:
                signatory = ""

            doc = frappe.get_doc(
                "Canopi Other Documents Post Execution", {"name": sub_name}
            )
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.single_signing = single_signing
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_poa = signatory_poa
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.document_custody_with = document_custody_with
            doc.axis_bank_branch_name = axis_bank_branch_name
            doc.third_party_name = third_party_name
            doc.document_type = document_type
            doc.executed_location = executed_location
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Other Documents Post Execution",
                {
                    "canopi_documentation_name": documentation_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if single_signing == "1":
                signatory = ""
                signatory_poa = ""
                signing_party = ""
            else:
                signatory_text = ""

            if signing_party == "ATSL":
                signatory_poa = ""
            else:
                signatory = ""

            doc = frappe.new_doc("Canopi Other Documents Post Execution")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.single_signing = single_signing
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_poa = signatory_poa
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.remarks_by_legal = remarks_by_legal
            doc.document_custody_with = document_custody_with
            doc.axis_bank_branch_name = axis_bank_branch_name
            doc.third_party_name = third_party_name
            doc.document_type = document_type
            doc.executed_location = executed_location
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def cancel_loa_documents(loa_file_list, docname):
    loa_file_list = json.loads(loa_file_list)
    documentation = frappe.get_doc("Canopi Documentation", docname)
    generated_loa_list = json.loads(documentation.generated_loa_history)

    for file in loa_file_list:
        for loa in generated_loa_list:
            if loa["file_name"] == file:
                loa["status"] = "Cancelled"
                delete_from_s3(loa["s3_key"])
                for document in loa["documents"]:
                    doc = frappe.get_doc(
                        document["doctype"], document["name"]
                    )  # noqa: 501
                    doc.status = "Open"
                    doc.save()

    documentation.generated_loa_history = json.dumps(generated_loa_list)
    documentation.save()


@frappe.whitelist()
def save_condition_precedent_part_a(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    status="",
    approval_if_required="",
    send_email="",
    category="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Condition Precedent Part A",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "category": category,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Condition Precedent Part A", {"name": sub_name}
            )
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.category = category
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Condition Precedent Part A",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "category": category,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Condition Precedent Part A")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.category = category
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_condition_precedent_part_b(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    status="",
    approval_if_required="",
    send_email="",
    category="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Condition Precedent Part B",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "category": category,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Condition Precedent Part B", {"name": sub_name}
            )
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.category = category
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Condition Precedent Part B",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "category": category,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Condition Precedent Part B")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.category = category
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_condition_subsequent(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    status="",
    approval_if_required="",
    send_email="",
    category="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Condition Subsequent",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "category": category,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Condition Subsequent", {"name": sub_name}
            )  # noqa: 501
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.category = category
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Condition Subsequent",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "category": category,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Condition Subsequent")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.category = category
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_pre_execution_documents(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    upload_date="",
    comments="",
    retain_waive="",
    send_to_legal="",
    status="",
    approval_if_required="",
    send_email="",
    for_post_execution="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Pre Execution Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.get_doc(
                "Canopi Pre Execution Documents", {"name": sub_name}
            )  # noqa: 501
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.save()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc("Canopi Post Execution Documents")
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.insert()
    else:
        if (
            frappe.db.count(
                "Canopi Pre Execution Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            doc = frappe.new_doc("Canopi Pre Execution Documents")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.upload_date = upload_date
            doc.comments = comments
            doc.retain_waive = retain_waive
            doc.send_to_legal = send_to_legal
            doc.status = status
            doc.approval_if_required = approval_if_required
            doc.send_email = send_email
            doc.for_post_execution = for_post_execution
            doc.insert()
            if doc.for_post_execution == "Yes":
                newdoc = frappe.new_doc("Canopi Post Execution Documents")
                newdoc.canopi_documentation_name = documentation_name
                newdoc.document_name = document_name
                newdoc.document_code = document_code
                newdoc.insert()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def save_post_execution_documents(
    documentation_name,
    sub_name="",
    document_name="",
    document_code="",
    execution_version="",
    signing_party="",
    signatory="",
    signatory_text="",
    location="",
    execution_date="",
    comments="",
    status="",
    send_email="",
    approval_if_required="",
    send_to_legal="",
):
    if sub_name:
        if (
            frappe.db.count(
                "Canopi Post Execution Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "name": ("!=", sub_name),
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if signing_party == "ATSL":
                signatory_text = ""
            else:
                signatory = ""
            doc = frappe.get_doc(
                "Canopi Post Execution Documents", {"name": sub_name}
            )  # noqa: 501
            documentation_name = doc.canopi_documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.save()
    else:
        if (
            frappe.db.count(
                "Canopi Post Execution Documents",
                {
                    "canopi_documentation_name": documentation_name,
                    "document_code": document_code,
                },
            )
            > 0
        ):
            frappe.msgprint("<b>Document Code</b> must be unique.")
        else:
            if signing_party == "ATSL":
                signatory_text = ""
            else:
                signatory = ""
            doc = frappe.new_doc("Canopi Post Execution Documents")
            doc.canopi_documentation_name = documentation_name
            doc.document_name = document_name
            doc.document_code = document_code
            doc.execution_version = execution_version
            doc.signing_party = signing_party
            doc.signatory = signatory
            doc.signatory_text = signatory_text
            doc.location = location
            doc.execution_date = execution_date
            doc.comments = comments
            doc.status = status
            doc.send_email = send_email
            doc.approval_if_required = approval_if_required
            doc.send_to_legal = send_to_legal
            doc.save()
    calculate_completion_percent(documentation_name)


@frappe.whitelist()
def send_to_legal(docname, child_doctype, docs, vetting=0, noting=0):
    if vetting == "1":
        purpose = "Vetting"
    if noting == "1":
        purpose = "Noting"
    docsArr = json.loads(docs)
    for x in docsArr:
        child_docname = x["name"]
        doc = frappe.get_doc(child_doctype, child_docname)
        doc.send_to_legal = "Yes"
        doc.purpose_of_legal = purpose
        doc.save()

    users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Legal Maker"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for user in users:
        try:
            asign_task(
                {
                    "assign_to": [user.email],
                    "doctype": "Canopi Documentation",
                    "name": docname,
                    "description": "",
                    "priority": "High",
                    "notify": 1,
                    "action": "",
                }
            )
        except assign_to.DuplicateToDoError:
            frappe.message_log.pop()
            pass


@frappe.whitelist()
def update_annexure_b_draft_status(docname):
    documentation = frappe.get_doc("Canopi Documentation", docname)

    if documentation.docstatus == 0:
        documentation.is_annexure_draft = 1

        if is_annexure_b_approval_completed(docname):
            documentation.is_annexure_draft = 0
        documentation.save()


@frappe.whitelist()
def is_annexure_b_approval_completed(docname):
    approval_list = frappe.get_all(
        "Canopi Approval",
        filters=[
            ["is_annexure_b", "=", 1],
            ["documentation", "=", docname],
        ],  # noqa: 501
    )
    approval = next((x.name for x in approval_list if x.name), None)
    if approval:
        approval_doc = frappe.get_doc("Canopi Approval", approval)
        return approval_doc.approval_count == get_approval_count_cond(
            approval_doc
        )  # noqa: 501


@frappe.whitelist()
def validate_annexure_b(docname):
    documentation = frappe.get_doc("Canopi Documentation", docname)

    quotation_error = ""
    sales_invoice_error = ""
    post_exec_error = ""
    annexure_b_checklist_table_error = ""

    # validate quotation
    if not documentation.quotation:
        quotation_error += """
        <li>
        Must be linked to the Operations Master.
        </li>
        """

        quotation_error += """
        <li>
        Should be accepted by the client.
        </li>
        """
    elif (
        frappe.db.get_value(
            "Quotation", documentation.quotation, "workflow_state"
        )  # noqa: 501
        != "Accepted By Client"
    ):
        quotation_error += """
        <li>
        Should be accepted by the client.
        </li>
        """

    # validate Sales Invoice
    sales_invoice_list = frappe.get_all(
        "Sales Invoice",
        filters=[
            ["cnp_from_quotation", "=", documentation.quotation],
            ["docstatus", "!=", 2],
        ],
    )
    if not sales_invoice_list:
        sales_invoice_error += """
        <li>
        Must be Linked to Quotation and must not be in a cancelled state.
        </li>
        """

    # validate post execution checklist
    post_execution_list = frappe.get_all(
        "Canopi Post Execution Checklist",
        filters=[["operations", "=", documentation.operations]],
        fields=["name", "type_of_security_details"],
    )
    if not post_execution_list:
        post_exec_error += """
        <li>
        Must be linked to the Operations Master.
        </li>
        """
        post_exec_error += """
        <li>
        Document Code POS0013 must have a file uploaded and Upload Date updated
        </li>
        """
    else:
        # get first element
        post_exec = next(iter(post_execution_list))
        details = json.loads(post_exec.type_of_security_details)
        notification_interval = frappe.get_single(
            "Canopi Notification Interval"
        )  # noqa: 501
        annexure_settings = notification_interval.canopi_annexture_settings

        # check annexure settings document codes present and
        # uploaded in post execution's asset table
        # returns True if every document from annexure settings
        # is present and uploaded in post execution, else False
        is_document_present_and_uploaded = not any(
            any(
                asset.get("document_code") == settings.document_code
                and ("file_name" not in asset or not asset["file_name"])
                for asset in details[key]
            )
            for key in details
            for settings in annexure_settings
        )

        if not is_document_present_and_uploaded:
            post_exec_error += """<li>Document Code POS0013 must have a file uploaded and Upload Date updated</li>"""  # noqa: 501

    # validate Annexure B Checklist table
    for index, checklist in enumerate(documentation.annexure_b_checklist):
        if not checklist.particulars_status:
            annexure_b_checklist_table_error += f"""
            <li>
            Yes/No/Not Applicable/Not Received is mandatory at Row {index+1}
            </li>
            """

        if (
            checklist.particulars_status
            in ["No", "Not Applicable", "Not Received"]  # noqa: 501
        ) and (not checklist.remarks):
            annexure_b_checklist_table_error += f"""
            <li>
            Remarks is mandatory at Row {index+1}
            </li>
            """

    errors = ""
    if quotation_error:
        errors += f"""
        <li>
        Quotation:
        <ul style='list-style-type:disc;'>
        {quotation_error}
        </ul>
        </li>
        """
    if sales_invoice_error:
        errors += f"""
        <li>
        Sales Invoice:
        <ul style='list-style-type:disc;'>
        {sales_invoice_error}
        </ul>
        </li>
        """
    if post_exec_error:
        errors += f"""
        <li>
        Canopi Post Execution Checcklist:
        <ul style='list-style-type:disc;'>
        {post_exec_error}
        </ul>
        </li>
        """
    if annexure_b_checklist_table_error:
        errors += f"""
        <li>
        Annexure B Checklist:
        <ul style='list-style-type:disc;'>
        {annexure_b_checklist_table_error}
        </ul>
        </li>
        """

    if errors:
        frappe.throw(
            (
                "<p>To proceed further, the following requirements must be met:</p><ol>"  # noqa: 501
                + errors
                + "</ol>"
            ).format(),
            title="Insufficient Requisites",
        )


@frappe.whitelist()
def upload_annexure_b_to_s3():
    validate_s3_settings()
    file = frappe.request.files
    docname = frappe.request.form.get("docname")
    key = frappe.request.form.get("key")
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    key = f"""{cstr(frappe.local.site)}/{key}"""

    doc = frappe.get_doc("Canopi Documentation", docname)
    if doc.annexure_b_s3_key:
        delete_from_s3(key)
        doc.annexure_b_s3_key = ""

    file_data = file["file"].read()
    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        doc.annexure_b_s3_key = key
        doc.save()


@frappe.whitelist()
def delete_annexure_b_from_s3(docname, key):
    delete_from_s3(key)
    doc = frappe.get_doc("Canopi Documentation", docname)
    doc.annexure_b_s3_key = ""
    doc.save()


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_canopi_poa(doctype, txt, searchfield, start, page_len, filters):
    conditions = []
    txt = f"%{txt}%"

    poa_list_query = """
        SELECT name,
        DATE_FORMAT(poa_expiry_date, '%%d-%%m-%%Y') AS formatted_expiry_date
        FROM `tabCanopi POA`
        WHERE poa_expiry_date > CURDATE()
        AND (name LIKE %(txt)s OR poa_expiry_date LIKE %(txt)s)
        {fcond}
        ORDER BY name ASC
        LIMIT %(page_len)s OFFSET %(start)s
    """.format(
        fcond=get_filters_cond(doctype, filters, conditions)
    )

    return frappe.db.sql(
        poa_list_query, dict(start=start, page_len=page_len, txt=txt)
    )  # noqa: 501


@frappe.whitelist()
def s3_upload():
    validate_s3_settings()
    file = frappe.request.files
    doctype = frappe.request.form.get("doctype")
    docname = frappe.request.form.get("docname")
    file_name = frappe.request.form.get("file_name")
    file_list = []
    settings_doc = frappe.get_single("Trusteeship Platform Settings")

    doc = frappe.get_doc(doctype, docname)
    old_file = doc.file_name
    if old_file:
        file_list = old_file.split(",")
    if file_name and file:
        key = (
            cstr(frappe.local.site)
            + "/"
            + doctype
            + "/Date of IM Upload/"
            + docname
            + "/"
            + file_name
        )
        file_data = file["file"].read()
        response = upload_file_to_s3(settings_doc, key, file_data)
        if response["status_code"] == 200:
            if file_name not in file_list:
                file_list.append(file_name)

    files = ",".join(file_list)
    doc.file_name = files
    doc.save()


@frappe.whitelist()
def delete_file(docname, file_name, doctype):
    key = (
        cstr(frappe.local.site)
        + "/"
        + doctype
        + "/Date of IM Upload/"
        + docname
        + "/"
        + file_name  # noqa: 501 isort:skip
    )  # noqa: 501 isort:skip
    delete_from_s3(key)

    doc = frappe.get_doc(doctype, docname)
    files = doc.file_name
    fArr = files.split(",")
    fArr.remove(file_name)
    file_names = ",".join(fArr)
    doc.file_name = file_names
    doc.save()
