/* eslint-disable indent */
/* global frappe, DataTable */
/* eslint-env jquery */

frappe.ui.form.on('Canopi Reports', {
  refresh: function (frm) {
    makeFullPage(frm);
    template(frm);
  },

  fiscal_year: function (frm) {
    checkColumns(frm);
  },
});
function template(frm) {
  const template =
    '<div class="data-table-tree-container demo-target-3"></div>';
  const id = frm.doc.name;
  let elementsToAdd;
  let columns;
  let quarterMonths;
  let periodMonths;
  let yearMonth;
  if (
    frm.doc.title === 'Profit and Loss' ||
    frm.doc.title === 'Balance Sheet'
  ) {
    quarterMonths = lastDayOfEachMonth(frm, frm.doc.fiscal_year, 'quarter');
    periodMonths = lastDayOfEachMonth(frm, frm.doc.fiscal_year, 'period');
    yearMonth = lastDayOfEachMonth(frm, frm.doc.fiscal_year, 'year');
    quarterMonths = getMonths(quarterMonths);
    periodMonths = getMonths(periodMonths);
    yearMonth = getMonths(yearMonth);
    elementsToAdd = [
      {
        name: 'Particulars',
        width: 270,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      },
    ];
    quarterMonths.splice(0, 0, ...elementsToAdd);
    columns = [...quarterMonths, ...periodMonths, ...yearMonth];
  } else if (frm.doc.title === 'Cash Flow') {
    quarterMonths = lastDayOfEachMonth(
      frm,
      frm.doc.fiscal_year,
      'quarter_cash_flow',
    );
    yearMonth = lastDayOfEachMonth(frm, frm.doc.fiscal_year, 'year_cash_flow');
    elementsToAdd = [
      {
        name: 'Cash flow from operating activities',
        width: 480,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      },
    ];
    quarterMonths.splice(0, 0, ...elementsToAdd);
    columns = [...quarterMonths, ...yearMonth];
  }

  if (frm.doc.fiscal_year) {
    datatable(frm, columns, template, id);
  }
}
function datatable(frm, columns, template, id) {
  const data = getFieldValues(frm).year;

  frm.set_df_property(
    'report_html',
    'options',
    frappe.render_template(template, {
      doc: frm.doc,
      id: frm.doc.__islocal === 1 ? '' : id,
      data,
      columns,
    }),
  );
  const element = $('.demo-target-3')[0];
  // eslint-disable-next-line no-new
  new DataTable(element, {
    columns,
    data,
    treeView: true,
    column_toggle: true,
    getEditor(colIndex, rowIndex, value, parent, column, row, data) {
      if (setReadonly(frm, colIndex, rowIndex)) {
        return null;
      } else {
        const dialog = new frappe.ui.Dialog({
          title: 'Edit Row',
          fields: [
            {
              label: 'Particulars',
              fieldname: 'Particulars',
              fieldtype: 'Data',
              read_only: true,
              default: data.Particulars,
            },
            {
              label: 'Amount',
              fieldname: getData(frm, colIndex),
              fieldtype: 'Currency',
              reqd: 1,
              default: data[getData(frm, colIndex)],
            },
          ],
          primary_action_label: 'Update',
          primary_action: function () {
            const values = dialog.get_values();
            const details = getFieldValues(frm).year;
            const year = frm.doc.fiscal_year
              ? frm.doc.fiscal_year.replace(/-/g, '_')
              : frappe.defaults
                  .get_user_default('fiscal_year')
                  .replace(/-/g, '_');
            for (let i = 0; i < details.length; i++) {
              if (details[i].id === data.id) {
                details[i][getData(frm, colIndex)] =
                  values[getData(frm, colIndex)];
              }
            }
            const reportDetails =
              frm.doc.report_details !== undefined
                ? JSON.parse(frm.doc.report_details)
                : {};
            reportDetails[year] = details;
            frm.doc.report_details = JSON.stringify(reportDetails);
            dialog.hide();
            frm.dirty();
            frm.save();
            getFieldValues(frm);
          },
          secondary_action_label: 'Cancel',
          secondary_action: () => {
            dialog.hide();
          },
        });
        dialog.show();
      }
    },
  });
}

function getMonths(months) {
  months.sort(function (a, b) {
    const dateA = new Date(a.name);
    const dateB = new Date(b.name);

    if (dateA > dateB) {
      return -1;
    }
    if (dateA < dateB) {
      return 1;
    }
    return 0;
  });
  return months;
}

function setReadonly(frm, colIndex, rowIndex) {
  if (frm.doc.title === 'Profit and Loss') {
    if (
      (colIndex === 2 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 2 ||
      rowIndex === 3 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 9 ||
      rowIndex === 10 ||
      rowIndex === 11 ||
      rowIndex === 12 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      (colIndex === 3 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 2 ||
      rowIndex === 3 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 9 ||
      rowIndex === 10 ||
      rowIndex === 11 ||
      rowIndex === 12 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      (colIndex === 4 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 2 ||
      rowIndex === 3 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 9 ||
      rowIndex === 10 ||
      rowIndex === 11 ||
      rowIndex === 12 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      (colIndex === 5 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 2 ||
      rowIndex === 3 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 9 ||
      rowIndex === 10 ||
      rowIndex === 11 ||
      rowIndex === 12 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      (colIndex === 6 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 2 ||
      rowIndex === 3 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 9 ||
      rowIndex === 10 ||
      rowIndex === 11 ||
      rowIndex === 12 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19 ||
      (colIndex === 7 && rowIndex === 0) ||
      rowIndex === 1 ||
      rowIndex === 2 ||
      rowIndex === 3 ||
      rowIndex === 5 ||
      rowIndex === 6 ||
      rowIndex === 7 ||
      rowIndex === 9 ||
      rowIndex === 10 ||
      rowIndex === 11 ||
      rowIndex === 12 ||
      rowIndex === 15 ||
      rowIndex === 16 ||
      rowIndex === 17 ||
      rowIndex === 18 ||
      rowIndex === 19
    ) {
      return true;
    } else {
      return false;
    }
  }
  if (frm.doc.title === 'Balance Sheet') {
    if (
      (colIndex === 2 &&
        (rowIndex === 0 ||
          rowIndex === 1 ||
          rowIndex === 2 ||
          rowIndex === 3 ||
          rowIndex === 4 ||
          rowIndex === 5 ||
          rowIndex === 6 ||
          rowIndex === 7 ||
          rowIndex === 8 ||
          rowIndex === 9 ||
          rowIndex === 10 ||
          rowIndex === 11 ||
          rowIndex === 12 ||
          rowIndex === 13 ||
          rowIndex === 15 ||
          rowIndex === 16 ||
          rowIndex === 17 ||
          rowIndex === 18 ||
          rowIndex === 19 ||
          rowIndex === 20 ||
          rowIndex === 21 ||
          rowIndex === 23 ||
          rowIndex === 25 ||
          rowIndex === 27 ||
          rowIndex === 28 ||
          rowIndex === 29 ||
          rowIndex === 30 ||
          rowIndex === 31 ||
          rowIndex === 32 ||
          rowIndex === 33 ||
          rowIndex === 34 ||
          rowIndex === 35)) ||
      (colIndex === 3 &&
        (rowIndex === 0 ||
          rowIndex === 1 ||
          rowIndex === 2 ||
          rowIndex === 3 ||
          rowIndex === 4 ||
          rowIndex === 5 ||
          rowIndex === 6 ||
          rowIndex === 7 ||
          rowIndex === 8 ||
          rowIndex === 9 ||
          rowIndex === 10 ||
          rowIndex === 11 ||
          rowIndex === 12 ||
          rowIndex === 13 ||
          rowIndex === 15 ||
          rowIndex === 16 ||
          rowIndex === 17 ||
          rowIndex === 18 ||
          rowIndex === 19 ||
          rowIndex === 23 ||
          rowIndex === 25 ||
          rowIndex === 27 ||
          rowIndex === 28 ||
          rowIndex === 29 ||
          rowIndex === 30 ||
          rowIndex === 31 ||
          rowIndex === 32 ||
          rowIndex === 33 ||
          rowIndex === 34 ||
          rowIndex === 35)) ||
      (colIndex === 4 &&
        (rowIndex === 0 ||
          rowIndex === 1 ||
          rowIndex === 2 ||
          rowIndex === 3 ||
          rowIndex === 4 ||
          rowIndex === 5 ||
          rowIndex === 6 ||
          rowIndex === 7 ||
          rowIndex === 8 ||
          rowIndex === 9 ||
          rowIndex === 10 ||
          rowIndex === 11 ||
          rowIndex === 12 ||
          rowIndex === 13 ||
          rowIndex === 15 ||
          rowIndex === 16 ||
          rowIndex === 17 ||
          rowIndex === 18 ||
          rowIndex === 19 ||
          rowIndex === 23 ||
          rowIndex === 25 ||
          rowIndex === 27 ||
          rowIndex === 28 ||
          rowIndex === 29 ||
          rowIndex === 30 ||
          rowIndex === 31 ||
          rowIndex === 32 ||
          rowIndex === 33 ||
          rowIndex === 34 ||
          rowIndex === 35)) ||
      (colIndex === 5 &&
        (rowIndex === 0 ||
          rowIndex === 1 ||
          rowIndex === 2 ||
          rowIndex === 3 ||
          rowIndex === 4 ||
          rowIndex === 5 ||
          rowIndex === 6 ||
          rowIndex === 7 ||
          rowIndex === 8 ||
          rowIndex === 9 ||
          rowIndex === 10 ||
          rowIndex === 11 ||
          rowIndex === 12 ||
          rowIndex === 13 ||
          rowIndex === 15 ||
          rowIndex === 16 ||
          rowIndex === 17 ||
          rowIndex === 18 ||
          rowIndex === 19 ||
          rowIndex === 20 ||
          rowIndex === 21 ||
          rowIndex === 23 ||
          rowIndex === 25 ||
          rowIndex === 27 ||
          rowIndex === 28 ||
          rowIndex === 29 ||
          rowIndex === 30 ||
          rowIndex === 31 ||
          rowIndex === 32 ||
          rowIndex === 33 ||
          rowIndex === 34 ||
          rowIndex === 35)) ||
      (colIndex === 6 &&
        (rowIndex === 0 ||
          rowIndex === 1 ||
          rowIndex === 2 ||
          rowIndex === 3 ||
          rowIndex === 4 ||
          rowIndex === 5 ||
          rowIndex === 6 ||
          rowIndex === 7 ||
          rowIndex === 8 ||
          rowIndex === 9 ||
          rowIndex === 10 ||
          rowIndex === 11 ||
          rowIndex === 12 ||
          rowIndex === 13 ||
          rowIndex === 15 ||
          rowIndex === 16 ||
          rowIndex === 17 ||
          rowIndex === 18 ||
          rowIndex === 19 ||
          rowIndex === 23 ||
          rowIndex === 25 ||
          rowIndex === 27 ||
          rowIndex === 28 ||
          rowIndex === 29 ||
          rowIndex === 30 ||
          rowIndex === 31 ||
          rowIndex === 32 ||
          rowIndex === 33 ||
          rowIndex === 34 ||
          rowIndex === 35)) ||
      (colIndex === 7 &&
        (rowIndex === 0 ||
          rowIndex === 1 ||
          rowIndex === 2 ||
          rowIndex === 3 ||
          rowIndex === 4 ||
          rowIndex === 5 ||
          rowIndex === 6 ||
          rowIndex === 7 ||
          rowIndex === 8 ||
          rowIndex === 9 ||
          rowIndex === 10 ||
          rowIndex === 11 ||
          rowIndex === 12 ||
          rowIndex === 13 ||
          rowIndex === 15 ||
          rowIndex === 16 ||
          rowIndex === 17 ||
          rowIndex === 18 ||
          rowIndex === 19 ||
          rowIndex === 23 ||
          rowIndex === 25 ||
          rowIndex === 27 ||
          rowIndex === 28 ||
          rowIndex === 29 ||
          rowIndex === 30 ||
          rowIndex === 31 ||
          rowIndex === 32 ||
          rowIndex === 33 ||
          rowIndex === 34 ||
          rowIndex === 35))
    ) {
      return true;
    } else {
      return false;
    }
  }
}

function getData(frm, colIndex) {
  const fiscalYearStart = frm.doc.fiscal_year.split('-')[0];
  const fiscalYearEnd = frm.doc.fiscal_year.split('-')[1];
  const data =
    colIndex === 2
      ? 'Sep 30,  ' + fiscalYearEnd
      : colIndex === 3
      ? 'June 30,  ' + fiscalYearEnd
      : colIndex === 4
      ? 'Sep 30,  ' + fiscalYearStart
      : colIndex === 5
      ? 'Sep 30, ' + fiscalYearEnd
      : colIndex === 6
      ? 'Sep 30, ' + fiscalYearStart
      : colIndex === 7
      ? 'Mar 31, ' + fiscalYearEnd
      : 'amount';
  return data;
}

function formatFileName(cell, row, frm) {
  let indent = '';
  for (let index = 0; index < row.length; index++) {
    const element = row[index];
    const formattedNumber = cell.toLocaleString('en-US', {
      style: 'currency',
      currency: 'INR',
    });
    if (element.colIndex === 1) {
      element.column.editable = false;
    }
    if (element.indent === 0) {
      indent =
        '<a row="' +
        element.colIndex +
        '-' +
        element.rowIndex +
        '"><b><span style="margin-left:' +
        row.indent * 20 +
        'px;">' +
        formattedNumber +
        '</span></b></a>';
    } else {
      indent = '<a row="' + element + '">' + formattedNumber + '</span></a>';
    }
  }

  return indent;
}

function getFieldValues(frm) {
  frm.doc.report_details = frm.doc.report_details
    ? frm.doc.report_details
    : JSON.stringify({});
  const details =
    frm.doc.report_details !== undefined
      ? JSON.parse(frm.doc.report_details)
      : {};
  let year = frm.doc.fiscal_year
    ? frm.doc.fiscal_year.replace(/-/g, '_')
    : frappe.defaults.get_user_default('fiscal_year').replace(/-/g, '_');
  year = details[year];

  return {
    year,
  };
}

function makeFullPage(frm) {
  frm.page
    .set_primary_action('Save', function () {
      frm.save();
    })
    .addClass('btn-primary');
  frm.page.clear_user_actions();
  frm.page.set_title(frm.doc.title);
  frm.page.sidebar.hide();
  frm.page.wrapper.find('.navbar').toggle(false);
  frm.page.wrapper.find('.page-footer').toggle(false);
  frm.page.full_page = 1;
  frm.page.wrapper.find('.comment-box').css({ display: 'none' });
}
function lastDayOfEachMonth(frm, fiscalYear, columnType) {
  const fiscalYearStart = new Date(`${fiscalYear.split('-')[0]}-01-01`);
  const fiscalYearEnd = new Date(`${fiscalYear.split('-')[1]}-12-31`);

  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  const lastDays = [];
  for (
    let year = fiscalYearStart.getFullYear();
    year <= fiscalYearEnd.getFullYear();
    year++
  ) {
    for (let month = 0; month < 12; month++) {
      const lastDay = new Date(year, month + 1, 0);
      if (lastDay >= fiscalYearStart && lastDay <= fiscalYearEnd) {
        getVisible(
          frm,
          monthNames[lastDay.getMonth()],
          lastDay.getDate(),
          lastDay.getFullYear(),
          lastDays,
          columnType,
          fiscalYearStart.getFullYear(),
          fiscalYearEnd.getFullYear(),
        );
      }
    }
  }
  return lastDays;
}

function getVisible(
  frm,
  month,
  date,
  year,
  lastDays,
  columnType,
  fiscalYearEnd,
  fiscalYearStart,
) {
  if (columnType === 'quarter') {
    if (
      month + ' ' + date + ',  ' + year === 'Sep 30, ' + ' ' + fiscalYearEnd ||
      month + ' ' + date + ',  ' + year ===
        'June 30, ' + ' ' + fiscalYearStart ||
      month + ' ' + date + ',  ' + year === 'Sep 30, ' + ' ' + fiscalYearStart
    ) {
      lastDays.push({
        name: month + ' ' + date + ',  ' + year,
        fieldtype: 'Currency',
        width: 150,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  } else if (columnType === 'period') {
    if (
      month + ' ' + date + ', ' + year === 'Sep 30, ' + fiscalYearEnd ||
      month + ' ' + date + ', ' + year === 'Sep 30, ' + fiscalYearStart
    ) {
      lastDays.push({
        name: month + ' ' + date + ', ' + year,
        fieldtype: 'Currency',
        width: 150,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  } else if (columnType === 'year') {
    if (month + ' ' + date + ', ' + year === 'Mar 31, ' + fiscalYearStart) {
      lastDays.push({
        name: month + ' ' + date + ', ' + year,
        fieldtype: 'Currency',
        width: 150,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  } else if (columnType === 'quarter_cash_flow') {
    if (month + ' ' + date + ', ' + year === 'Sep 30, ' + fiscalYearStart) {
      lastDays.push({
        name: 'For the Quarter Ended ' + month + ', ' + year,
        fieldtype: 'Currency',
        width: 350,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  } else if (columnType === 'year_cash_flow') {
    if (month + ' ' + date + ', ' + year === 'Mar 31, ' + fiscalYearStart) {
      lastDays.push({
        name: 'For the Year Ended ' + month + ' 31, ' + year,
        fieldtype: 'Currency',
        width: 350,
        align: 'right',
        default: 0,
        format: function (value, row, column, data, defaultFormatter) {
          return formatFileName(value, row, frm);
        },
      });
    }
  }
}

function checkColumns(frm) {
  const fiscalYearStart = frm.doc.fiscal_year.split('-')[0];
  const fiscalYearEnd = frm.doc.fiscal_year.split('-')[1];
  frm.doc.report_details = frm.doc.report_details
    ? frm.doc.report_details
    : JSON.stringify({});
  const details =
    frm.doc.report_details === undefined
      ? {}
      : JSON.parse(frm.doc.report_details);
  const year = frm.doc.fiscal_year
    ? frm.doc.fiscal_year.replace(/-/g, '_')
    : frappe.defaults.get_user_default('fiscal_year').replace(/-/g, '_');

  if (details[year] === undefined && frm.doc.fiscal_year) {
    if (frm.doc.name === 'Profit and Loss') {
      details[year] = [
        {
          Particulars: 'Income',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'Revenue from operations',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 2,
        },
        {
          Particulars: 'Other income',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 3,
        },
        {
          Particulars: 'Total revenue (I)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 4,
        },
        {
          Particulars: '',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: '',
          [`Sep 30,  ${fiscalYearStart}`]: '',
          [`Sep 30, ${fiscalYearEnd}`]: '',
          [`Sep 30, ${fiscalYearStart}`]: '',
          [`Mar 31, ${fiscalYearEnd}`]: '',
          indent: 0,
          id: 5,
        },
        {
          Particulars: 'Expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 6,
        },
        {
          Particulars: 'Employee benefit expense',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 7,
        },
        {
          Particulars: 'Other expenses',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 8,
        },
        {
          Particulars: 'CSR Contribution',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 9,
        },
        {
          Particulars: 'Depreciation and amortisation',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 10,
        },
        {
          Particulars: 'Total expenses (II)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 11,
        },
        {
          Particulars: 'Profit before  tax  (I-II)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 12,
        },
        {
          Particulars: 'Tax for earlier Years',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 13,
        },
        {
          Particulars: 'Current Tax',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 14,
        },
        {
          Particulars: 'Deferred Tax',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 15,
        },
        {
          Particulars: 'Tax expense',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 16,
        },
        {
          Particulars: 'Profit/(Loss) after tax for the Year',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 17,
        },
        {
          Particulars: 'Earning Per Share',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 18,
        },
        {
          Particulars: 'Basic',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 19,
        },
        {
          Particulars: 'Diluted',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 20,
        },
      ];
    } else if (frm.doc.name === 'Balance Sheet') {
      details[year] = [
        {
          Particulars: 'Equity and liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 1,
        },
        {
          Particulars: 'Shareholders funds',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 2,
        },
        {
          Particulars: 'Share capital',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 3,
        },
        {
          Particulars: 'Reserves & surplus',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 4,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 5,
        },
        {
          Particulars: '',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 6,
        },
        {
          Particulars: 'Non-current liabilites',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 7,
        },
        {
          Particulars: 'Other long term liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 8,
        },
        {
          Particulars: 'Long term provisions',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 9,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 10,
        },
        {
          Particulars: '',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 11,
        },
        {
          Particulars: 'Current liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 12,
        },
        {
          Particulars: 'Trade payables',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 13,
        },
        {
          Particulars: 'Other current liabilities',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 14,
        },
        {
          Particulars: 'Short term provisions',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 15,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 16,
        },
        {
          Particulars: 'Total',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 17,
        },
        {
          Particulars: 'Assets',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 18,
        },
        {
          Particulars: 'Non-current assets',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 19,
        },
        {
          Particulars: 'Fixed assets',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 1,
          id: 20,
        },
        {
          Particulars: 'Tangible assets',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 21,
        },
        {
          Particulars: 'Intangible assets',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 22,
        },
        {
          Particulars: 'Intangible asset under development',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 2,
          id: 23,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 24,
        },
        {
          Particulars: 'Deferred Tax Asset (net)',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 25,
        },
        {
          Particulars: 'Other non-current assets',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 26,
        },
        {
          Particulars: 'Loans &  Advances',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 27,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 28,
        },
        {
          Particulars: 'Current assets',
          [`Sep 30,  ${fiscalYearEnd}`]: ' ',
          [`June 30,  ${fiscalYearEnd}`]: ' ',
          [`Sep 30,  ${fiscalYearStart}`]: ' ',
          [`Sep 30, ${fiscalYearEnd}`]: ' ',
          [`Sep 30, ${fiscalYearStart}`]: ' ',
          [`Mar 31, ${fiscalYearEnd}`]: ' ',
          indent: 0,
          id: 29,
        },
        {
          Particulars: 'Current Investments',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 30,
        },
        {
          Particulars: 'Loans &  Adavances',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 31,
        },
        {
          Particulars: 'Trade receivables',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 32,
        },
        {
          Particulars: 'Cash and Bank balances',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 33,
        },
        {
          Particulars: 'Other current assets',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 34,
        },
        {
          Particulars: ' ',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 1,
          id: 35,
        },
        {
          Particulars: 'Total',
          [`Sep 30,  ${fiscalYearEnd}`]: 0,
          [`June 30,  ${fiscalYearEnd}`]: 0,
          [`Sep 30,  ${fiscalYearStart}`]: 0,
          [`Sep 30, ${fiscalYearEnd}`]: 0,
          [`Sep 30, ${fiscalYearStart}`]: 0,
          [`Mar 31, ${fiscalYearEnd}`]: 0,
          indent: 0,
          id: 36,
        },
      ];
    }
    frm.doc.report_details = JSON.stringify(details);

    frm.refresh_field('report_details');
    frm.refresh_field('report_html');
    frm.save();
  }
}
