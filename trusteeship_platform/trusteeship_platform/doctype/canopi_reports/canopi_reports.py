# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe.model.document import Document


class CanopiReports(Document):
    def before_save(self):
        reports_data(self)


def reports_data(self):
    notes = all_reports_details(self)
    report_details = json.loads(self.report_details)

    report_calc_details = report_calc(self.type)
    calc_details = json.loads(self.calc_details)
    if self.type == "Balance Sheet":
        report_data = balance_sheet(
            self,
            notes.get("note1to5"),
            notes.get("note6"),
            notes.get("note7"),
            notes.get("note8to10"),
            report_details,
            report_calc_details,
            calc_details,
        )
    if self.type == "Profit Loss":
        report_data = profit_loss(
            self,
            notes.get("note11to12"),
            notes.get("note13to15"),
            report_details,
            report_calc_details,
            calc_details,
        )
    if self.type == "Cash Flow":
        report_data = cash_flow(
            self, report_details, report_calc_details, calc_details, notes
        )

    self.report_details = json.dumps(report_data.get("report_details"))
    self.calc_details = json.dumps(report_data.get("calc_details"))


def all_reports_details(self):
    formatted_string = format_company_name(self.fiscal_year, self.company)
    if self.type == "Balance Sheet":
        if frappe.db.exists("Note", "note_note1to5_" + formatted_string):
            note1to5 = frappe.get_doc(
                "Note", "note_note1to5_" + formatted_string
            )  # noqa: 501
        else:
            note1to5 = insert_note(
                self, "Note 1 to 5", "note_note1to5_" + formatted_string
            )

        if frappe.db.exists("Note", "note_note6_" + formatted_string):
            note6 = frappe.get_doc("Note", "note_note6_" + formatted_string)
        else:
            note6 = insert_note(
                self, "Note 6", "note_note6_" + formatted_string
            )  # noqa: 501

        if frappe.db.exists("Note", "note_note7_" + formatted_string):
            note7 = frappe.get_doc("Note", "note_note7_" + formatted_string)
        else:
            note7 = insert_note(
                self, "Note 7", "note_note7_" + formatted_string
            )  # noqa: 501
        if frappe.db.exists("Note", "note_note8to10_" + formatted_string):
            note8to10 = frappe.get_doc(
                "Note", "note_note8to10_" + formatted_string
            )  # noqa: 501
        else:
            note8to10 = insert_note(
                self, "Note 8 to 10", "note_note8to10_" + formatted_string
            )
        return {
            "note1to5": note1to5,
            "note6": note6,
            "note7": note7,
            "note8to10": note8to10,
        }

    if self.type == "Profit Loss":
        if frappe.db.exists("Note", "note_note11to12_" + formatted_string):
            note11to12 = frappe.get_doc(
                "Note", "note_note11to12_" + formatted_string
            )  # noqa: 501
        else:
            note11to12 = insert_note(
                self, "Note 11 to 12", "note_note11to12_" + formatted_string
            )
        if frappe.db.exists("Note", "note_note13to15_" + formatted_string):
            note13to15 = frappe.get_doc(
                "Note", "note_note13to15_" + formatted_string
            )  # noqa: 501
        else:
            note13to15 = insert_note(
                self, "Note 13 to 15", "note_note13to15_" + formatted_string
            )
        return {"note11to12": note11to12, "note13to15": note13to15}

    if self.type == "Cash Flow":
        if frappe.db.exists(
            "Canopi Reports", "report_profit_loss_" + formatted_string
        ):  # noqa: 501
            profit_loss = frappe.get_doc(
                "Canopi Reports", "report_profit_loss_" + formatted_string
            )
        else:
            profit_loss = insert_report(
                self, "Profit Loss", "report_profit_loss_" + formatted_string
            )

        if frappe.db.exists(
            "Canopi Reports", "report_balance_sheet_" + formatted_string
        ):
            balance_sheet = frappe.get_doc(
                "Canopi Reports", "report_balance_sheet_" + formatted_string
            )
        else:
            balance_sheet = insert_report(
                self,
                "Balance Sheet",
                "report_balance_sheet_" + formatted_string,  # noqa: 501
            )
        if frappe.db.exists("Note", "note_note13to15_" + formatted_string):
            note13to15 = frappe.get_doc(
                "Note", "note_note13to15_" + formatted_string
            )  # noqa: 501
        else:
            note13to15 = insert_note(
                self, "Note 13 to 15", "note_note13to15_" + formatted_string
            )
        if frappe.db.exists("Note", "note_note1to5_" + formatted_string):
            note1to5 = frappe.get_doc(
                "Note", "note_note1to5_" + formatted_string
            )  # noqa: 501
        else:
            note1to5 = insert_note(
                self, "Note 1 to 5", "note_note1to5_" + formatted_string
            )
        if frappe.db.exists("Note", "note_note11to12_" + formatted_string):
            note11to12 = frappe.get_doc(
                "Note", "note_note11to12_" + formatted_string
            )  # noqa: 501
        else:
            note11to12 = insert_note(
                self, "Note 11 to 12", "note_note11to12_" + formatted_string
            )
        if frappe.db.exists("Note", "note_note8to10_" + formatted_string):
            note8to10 = frappe.get_doc(
                "Note", "note_note8to10_" + formatted_string
            )  # noqa: 501
        else:
            note8to10 = insert_note(
                self, "Note 8 to 10", "note_note8to10_" + formatted_string
            )
        return {
            "profit_loss": profit_loss,
            "note1to5": note1to5,
            "note13to15": note13to15,
            "note11to12": note11to12,
            "note8to10": note8to10,
            "balance_sheet": balance_sheet,
        }


def balance_sheet(
    self,
    note1to5,
    note6,
    note7,
    note8to10,
    report_details,
    report_calc_details,
    calc_details,
):
    note1to5_calc_details = json.loads(note1to5.cnp_calc_details)
    note6_calc_details = json.loads(note6.cnp_calc_details)
    note7_calc_details = json.loads(note7.cnp_calc_details)
    note8to10_calc_details = json.loads(note8to10.cnp_calc_details)
    filter_year = frappe.get_doc("Fiscal Year", self.fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    for pl in report_details:
        if pl.get("id") == 3:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note1_year"
            )
        if pl.get("id") == 4:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note2_year"
            )

        if pl.get("id") == 5:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_quarter1"
            ) + note1to5_calc_details.get(
                "note2_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_quarter2"
            ) + note1to5_calc_details.get(
                "note2_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_quarter3"
            ) + note1to5_calc_details.get(
                "note2_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_period1"
            ) + note1to5_calc_details.get(
                "note2_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note1_period2"
            ) + note1to5_calc_details.get(
                "note2_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note1_year"
            ) + note1to5_calc_details.get("note2_year")
        if pl.get("id") == 8:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note3_year"
            )
        if pl.get("id") == 9:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_quarter1_long_term"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_quarter2_long_term"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_quarter3_long_term"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_period1_long_term"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_period2_long_term"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note4_year_long_term"
            )
        if pl.get("id") == 10:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_quarter1"
            ) + note1to5_calc_details.get(
                "note4_quarter1_long_term"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_quarter2"
            ) + note1to5_calc_details.get(
                "note4_quarter2_long_term"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_quarter3"
            ) + note1to5_calc_details.get(
                "note4_quarter3_long_term"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_period1"
            ) + note1to5_calc_details.get(
                "note4_period1_long_term"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_period2"
            ) + note1to5_calc_details.get(
                "note4_period2_long_term"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note3_year"
            ) + note1to5_calc_details.get("note4_year_long_term")
        if pl.get("id") == 13:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_quarter1_trade_payables"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_quarter2_trade_payables"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_quarter3_trade_payables"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_period1_trade_payables"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_period2_trade_payables"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note5_year_trade_payables"
            )
        if pl.get("id") == 14:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_quarter1_other_liabilities"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_quarter2_other_liabilities"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_quarter3_other_liabilities"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_period1_other_liabilities"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_period2_other_liabilities"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note5_year_other_liabilities"
            )
        if pl.get("id") == 15:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_quarter1_short_term"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_quarter2_short_term"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_quarter3_short_term"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_period1_short_term"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_period2_short_term"
            )
            pl["year_ended_mar_31,_" + end_year] = note1to5_calc_details.get(
                "note4_year_short_term"
            )
        if pl.get("id") == 16:
            pl["quarter_ended_sep_30,_" + end_year] = (
                note1to5_calc_details.get("note5_quarter1_trade_payables")
                + note1to5_calc_details.get(
                    "note5_quarter1_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_quarter1_short_term")
            )
            pl["quarter_ended_june_30,_" + end_year] = (
                note1to5_calc_details.get("note5_quarter2_trade_payables")
                + note1to5_calc_details.get(
                    "note5_quarter2_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_quarter2_short_term")
            )
            pl["quarter_ended_sep_30,_" + start_year] = (
                note1to5_calc_details.get("note5_quarter3_trade_payables")
                + note1to5_calc_details.get(
                    "note5_quarter3_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_quarter3_short_term")
            )
            pl["period_ended_sep_30,_" + end_year] = (
                note1to5_calc_details.get("note5_period1_trade_payables")
                + note1to5_calc_details.get(
                    "note5_period1_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_period1_short_term")
            )
            pl["period_ended_sep_30,_" + start_year] = (
                note1to5_calc_details.get("note5_period1_other_liabilities")
                + note1to5_calc_details.get(
                    "note5_period2_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_period2_short_term")
            )
            pl["year_ended_mar_31,_" + end_year] = (
                note1to5_calc_details.get("note5_year_trade_payables")
                + note1to5_calc_details.get("note5_year_other_liabilities")  # noqa: 501
                + note1to5_calc_details.get("note4_year_short_term")
            )
        if pl.get("id") == 17:
            pl["quarter_ended_sep_30,_" + end_year] = (
                note1to5_calc_details.get("note1_quarter1")
                + note1to5_calc_details.get("note2_quarter1")
                + note1to5_calc_details.get("note3_quarter1")
                + note1to5_calc_details.get("note4_quarter1_long_term")
                + note1to5_calc_details.get("note5_quarter1_trade_payables")
                + note1to5_calc_details.get(
                    "note5_quarter1_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_quarter1_short_term")
            )
            pl["quarter_ended_june_30,_" + end_year] = (
                note1to5_calc_details.get("note1_quarter2")
                + note1to5_calc_details.get("note2_quarter2")
                + note1to5_calc_details.get("note3_quarter2")
                + note1to5_calc_details.get("note4_quarter2_long_term")
                + note1to5_calc_details.get("note5_quarter2_trade_payables")
                + note1to5_calc_details.get(
                    "note5_quarter2_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_quarter2_short_term")
            )
            pl["quarter_ended_sep_30,_" + start_year] = (
                note1to5_calc_details.get("note1_quarter3")
                + note1to5_calc_details.get("note2_quarter3")
                + note1to5_calc_details.get("note3_quarter3")
                + note1to5_calc_details.get("note4_quarter3_long_term")
                + note1to5_calc_details.get("note5_quarter3_trade_payables")
                + note1to5_calc_details.get(
                    "note5_quarter3_other_liabilities"
                )  # noqa: 501
                + note1to5_calc_details.get("note4_quarter3_short_term")
            )
            pl["period_ended_sep_30,_" + end_year] = (
                note1to5_calc_details.get("note1_period1")
                + note1to5_calc_details.get("note2_period1")
                + note1to5_calc_details.get("note3_period1")
                + note1to5_calc_details.get("note4_period1_long_term")
                + note1to5_calc_details.get("note5_period1_trade_payables")
                + note1to5_calc_details.get("note5_period1_other_liabilities")
                + note1to5_calc_details.get("note4_period1_short_term")
            )
            pl["period_ended_sep_30,_" + start_year] = (
                note1to5_calc_details.get("note1_period2")
                + note1to5_calc_details.get("note2_period2")
                + note1to5_calc_details.get("note3_period2")
                + note1to5_calc_details.get("note4_period2_long_term")
                + note1to5_calc_details.get("note5_period1_other_liabilities")
                + note1to5_calc_details.get("note5_period2_other_liabilities")
                + note1to5_calc_details.get("note4_period2_short_term")
            )
            pl["year_ended_mar_31,_" + end_year] = (
                note1to5_calc_details.get("note1_year")
                + note1to5_calc_details.get("note2_year")
                + note1to5_calc_details.get("note3_year")
                + note1to5_calc_details.get("note4_year_long_term")
                + note1to5_calc_details.get("note5_year_trade_payables")
                + note1to5_calc_details.get("note5_year_other_liabilities")
                + note1to5_calc_details.get("note4_year_short_term")
            )
        if pl.get("id") == 21:
            pl["quarter_ended_sep_30,_" + end_year] = note6_calc_details.get(
                "note6_net_block_total"
            )
            pl["period_ended_sep_30,_" + end_year] = note6_calc_details.get(
                "note6_net_block_total"
            )
        if pl.get("id") == 22:
            pl["quarter_ended_sep_30,_" + end_year] = note7_calc_details.get(
                "note7_net_block_computer_software"
            )
            pl["period_ended_sep_30,_" + end_year] = note7_calc_details.get(
                "note7_net_block_computer_software"
            )
        if pl.get("id") == 26:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_non_current_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_non_current_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_non_current_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note8to10_calc_details.get(
                "note9_2_non_current_year"
            )
        for i in range(21, 23):
            if pl.get("id") == i:
                if isinstance(pl["quarter_ended_sep_30,_" + end_year], int):
                    report_calc_details["bs_fixed_assets_period1"] += pl[
                        "period_ended_sep_30,_" + end_year
                    ]
                    report_calc_details["bs_fixed_assets_year"] += pl[
                        "year_ended_mar_31,_" + end_year
                    ]

        for i in range(21, 28):
            if pl.get("id") == i:
                if isinstance(pl["quarter_ended_sep_30,_" + end_year], int):
                    report_calc_details[
                        "bs_non_current_assets_quarter1"
                    ] += pl[  # noqa: 501
                        "quarter_ended_sep_30,_" + end_year
                    ]
                    report_calc_details[
                        "bs_non_current_assets_quarter2"
                    ] += pl[  # noqa: 501
                        "quarter_ended_june_30,_" + end_year
                    ]
                    report_calc_details[
                        "bs_non_current_assets_quarter3"
                    ] += pl[  # noqa: 501
                        "quarter_ended_sep_30,_" + start_year
                    ]
                    report_calc_details[
                        "bs_non_current_assets_period1"
                    ] += pl[  # noqa: 501
                        "period_ended_sep_30,_" + end_year
                    ]
                    report_calc_details[
                        "bs_non_current_assets_period2"
                    ] += pl[  # noqa: 501
                        "period_ended_sep_30,_" + start_year
                    ]
                    report_calc_details["bs_non_current_assets_year"] += pl[
                        "year_ended_mar_31,_" + end_year
                    ]
        if pl.get("id") == 28:
            pl["quarter_ended_sep_30,_" + end_year] = report_calc_details.get(
                "bs_non_current_assets_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "bs_non_current_assets_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "bs_non_current_assets_quarter3"
            )
            pl["period_ended_sep_30,_" + end_year] = report_calc_details.get(
                "bs_non_current_assets_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "bs_non_current_assets_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = report_calc_details.get(
                "bs_non_current_assets_year"
            )
        if pl.get("id") == 30:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note8to10_calc_details.get(
                "note8_year"
            )
        if pl.get("id") == 31:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_1_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_1_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_1_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_1_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note8_1_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note8to10_calc_details.get(
                "note8_1_year"
            )
        if pl.get("id") == 32:
            pl["quarter_ended_sep_30,_" + end_year] = (
                note8to10_calc_details.get("note9_1_quarter1")
                - note8to10_calc_details["sub1_note9_quarter1"]
            ) + (
                note8to10_calc_details.get("note9_2_quarter1")
                - note8to10_calc_details.get("sub2_note9_quarter1")
            )
            pl["quarter_ended_june_30,_" + end_year] = (
                note8to10_calc_details.get("note9_1_quarter2")
                - note8to10_calc_details["sub1_note9_quarter2"]
            ) + (
                note8to10_calc_details.get("note9_2_quarter2")
                - note8to10_calc_details.get("sub2_note9_quarter2")
            )
            pl["quarter_ended_sep_30,_" + start_year] = (
                note8to10_calc_details.get("note9_1_quarter3")
                - note8to10_calc_details["sub1_note9_quarter3"]
            ) + (
                note8to10_calc_details.get("note9_2_quarter3")
                - note8to10_calc_details.get("sub2_note9_quarter3")
            )
            pl["period_ended_sep_30,_" + end_year] = (
                note8to10_calc_details.get("note9_1_period1")
                - note8to10_calc_details["sub1_note9_period1"]
            ) + (
                note8to10_calc_details.get("note9_2_period1")
                - note8to10_calc_details.get("sub2_note9_period1")
            )
            pl["period_ended_sep_30,_" + start_year] = (
                note8to10_calc_details.get("note9_1_period2")
                - note8to10_calc_details["sub1_note9_period2"]
            ) + (
                note8to10_calc_details.get("note9_2_period2")
                - note8to10_calc_details.get("sub2_note9_period2")
            )
            pl["year_ended_mar_31,_" + end_year] = (
                note8to10_calc_details.get("note9_1_year")
                - note8to10_calc_details["sub1_note9_year"]
            ) + (
                note8to10_calc_details.get("note9_2_year")
                - note8to10_calc_details.get("sub2_note9_year")
            )
        if pl.get("id") == 33:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_quarter1"
            ) + note8to10_calc_details.get(
                "note10_2_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_quarter2"
            ) + note8to10_calc_details.get(
                "note10_2_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_quarter3"
            ) + note8to10_calc_details.get(
                "note10_2_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_period1"
            ) + note8to10_calc_details.get(
                "note10_2_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_period2"
            ) + note8to10_calc_details.get(
                "note10_2_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note8to10_calc_details.get(
                "note10_1_year"
            ) + note8to10_calc_details.get("note10_2_year")
        if pl.get("id") == 34:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_current_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_current_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_current_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_current_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_current_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = note8to10_calc_details.get(
                "note9_2_current_year"
            )
        if pl.get("id") == 35:
            pl["quarter_ended_sep_30,_" + end_year] = (
                note8to10_calc_details.get("note8_quarter1")
                + note8to10_calc_details.get("note8_1_quarter1")
                + note8to10_calc_details.get("sub1_note9_quarter1")
                + note8to10_calc_details["sub2_note9_quarter1"]
                + note8to10_calc_details.get("note10_1_quarter1")
                + note8to10_calc_details.get("note10_2_quarter1")
                + note8to10_calc_details.get("note9_2_current_quarter1")
            )
            pl["quarter_ended_june_30,_" + end_year] = (
                note8to10_calc_details.get("note8_quarter2")
                + note8to10_calc_details.get("note8_1_quarter2")
                + note8to10_calc_details.get("sub1_note9_quarter2")
                + note8to10_calc_details["sub2_note9_quarter2"]
                + note8to10_calc_details.get("note10_1_quarter2")
                + note8to10_calc_details.get("note10_2_quarter2")
                + note8to10_calc_details.get("note9_2_current_quarter2")
            )
            pl["quarter_ended_sep_30,_" + start_year] = (
                note8to10_calc_details.get("note8_quarter3")
                + note8to10_calc_details.get("note8_1_quarter3")
                + note8to10_calc_details.get("sub1_note9_quarter3")
                + note8to10_calc_details["sub2_note9_quarter3"]
                + note8to10_calc_details.get("note10_1_quarter3")
                + note8to10_calc_details.get("note10_2_quarter3")
                + note8to10_calc_details.get("note9_2_current_quarter3")
            )
            pl["period_ended_sep_30,_" + end_year] = (
                note8to10_calc_details.get("note8_period1")
                + note8to10_calc_details.get("note8_1_period1")
                + note8to10_calc_details.get("sub1_note9_period1")
                + note8to10_calc_details["sub2_note9_period1"]
                + note8to10_calc_details.get("note10_1_period1")
                + note8to10_calc_details.get("note10_2_period1")
                + note8to10_calc_details.get("note9_2_current_period1")
            )
            pl["period_ended_sep_30,_" + start_year] = (
                note8to10_calc_details.get("note8_period2")
                + note8to10_calc_details.get("note8_1_period2")
                + note8to10_calc_details.get("sub1_note9_period2")
                + note8to10_calc_details["sub2_note9_period2"]
                + note8to10_calc_details.get("note10_1_period2")
                + note8to10_calc_details.get("note10_2_period2")
                + note8to10_calc_details.get("note9_2_current_period2")
            )
            pl["year_ended_mar_31,_" + end_year] = (
                note8to10_calc_details.get("note8_year")
                + note8to10_calc_details.get("note8_1_year")
                + note8to10_calc_details.get("sub1_note9_year")
                + note8to10_calc_details["sub2_note9_year"]
                + note8to10_calc_details.get("note10_1_year")
                + note8to10_calc_details.get("note10_2_year")
                + note8to10_calc_details.get("note9_2_current_year")
            )
            report_calc_details["bs_current_assets_quarter1"] = pl[
                "quarter_ended_sep_30,_" + end_year
            ]
            report_calc_details["bs_current_assets_quarter2"] = pl[
                "quarter_ended_june_30,_" + end_year
            ]
            report_calc_details["bs_current_assets_quarter3"] = pl[
                "quarter_ended_sep_30,_" + start_year
            ]
            report_calc_details["bs_current_assets_period1"] = pl[
                "period_ended_sep_30,_" + end_year
            ]
            report_calc_details["bs_current_assets_period2"] = pl[
                "period_ended_sep_30,_" + start_year
            ]
            report_calc_details["bs_current_assets_year"] = pl[
                "year_ended_mar_31,_" + end_year
            ]
        if pl.get("id") == 36:
            pl["quarter_ended_sep_30,_" + end_year] = report_calc_details.get(
                "bs_non_current_assets_quarter1"
            ) + report_calc_details.get("bs_current_assets_quarter1")
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "bs_non_current_assets_quarter2"
            ) + report_calc_details.get(
                "bs_current_assets_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "bs_non_current_assets_quarter3"
            ) + report_calc_details.get(
                "bs_current_assets_quarter3"
            )
            pl["period_ended_sep_30,_" + end_year] = report_calc_details.get(
                "bs_non_current_assets_period1"
            ) + report_calc_details.get("bs_current_assets_period1")
            pl[
                "period_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "bs_non_current_assets_period2"
            ) + report_calc_details.get(
                "bs_current_assets_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = report_calc_details.get(
                "bs_non_current_assets_year"
            ) + report_calc_details.get("bs_current_assets_year")
    calc_details = {
        "bs_non_current_assets_quarter1": report_calc_details.get(
            "bs_non_current_assets_quarter1"
        ),
        "bs_non_current_assets_quarter2": report_calc_details.get(
            "bs_non_current_assets_quarter2"
        ),
        "bs_non_current_assets_quarter3": report_calc_details.get(
            "bs_non_current_assets_quarter3"
        ),
        "bs_non_current_assets_period1": report_calc_details.get(
            "bs_non_current_assets_period1"
        ),
        "bs_non_current_assets_period2": report_calc_details.get(
            "bs_non_current_assets_period2"
        ),
        "bs_non_current_assets_year": report_calc_details.get(
            "bs_non_current_assets_year"
        ),
        "bs_current_assets_quarter1": report_calc_details.get(
            "bs_current_assets_quarter1"
        ),
        "bs_current_assets_quarter2": report_calc_details.get(
            "bs_current_assets_quarter2"
        ),
        "bs_current_assets_quarter3": report_calc_details.get(
            "bs_current_assets_quarter3"
        ),
        "bs_current_assets_period1": report_calc_details.get(
            "bs_current_assets_period1"
        ),
        "bs_current_assets_period2": report_calc_details.get(
            "bs_current_assets_period2"
        ),
        "bs_current_assets_year": report_calc_details.get(
            "bs_current_assets_year"
        ),  # noqa: 501
        "bs_fixed_assets_period1": report_calc_details.get(
            "bs_current_assets_year"
        ),  # noqa: 501
        "bs_fixed_assets_year": report_calc_details.get(
            "bs_fixed_assets_year"
        ),  # noqa: 501
    }
    report_calc_details["bs_non_current_assets_quarter1"] = 0
    report_calc_details["bs_non_current_assets_quarter2"] = 0
    report_calc_details["bs_non_current_assets_quarter3"] = 0
    report_calc_details["bs_non_current_assets_period1"] = 0
    report_calc_details["bs_non_current_assets_period2"] = 0
    report_calc_details["bs_non_current_assets_year"] = 0
    report_calc_details["bs_current_assets_quarter1"] = 0
    report_calc_details["bs_current_assets_quarter2"] = 0
    report_calc_details["bs_current_assets_quarter3"] = 0
    report_calc_details["bs_current_assets_period1"] = 0
    report_calc_details["bs_current_assets_period2"] = 0
    report_calc_details["bs_current_assets_year"] = 0
    report_calc_details["bs_fixed_assets_period1"] = 0
    report_calc_details["bs_fixed_assets_year"] = 0
    return {"report_details": report_details, "calc_details": calc_details}


def profit_loss(
    self,
    note11to12,
    note13to15,
    report_details,
    report_calc_details,
    calc_details,  # noqa: 501
):
    note13to15_calc_details = json.loads(note13to15.cnp_calc_details)
    note11to12_calc_details = json.loads(note11to12.cnp_calc_details)
    filter_year = frappe.get_doc("Fiscal Year", self.fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    for pl in report_details:
        if pl.get("id") == 2:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note11_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note11_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note11_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note11_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note11_period2"
            )
            pl[
                "year_ended_mar_31,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note11_year"
            )
        if pl.get("id") == 3:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period2"
            )
            pl[
                "year_ended_mar_31,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_year"
            )

        if pl.get("id") == 4:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_quarter1"
            ) + note11to12_calc_details.get(
                "note11_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_quarter2"
            ) + note11to12_calc_details.get(
                "note11_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_quarter3"
            ) + note11to12_calc_details.get(
                "note11_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period1"
            ) + note11to12_calc_details.get(
                "note11_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period2"
            ) + note11to12_calc_details.get(
                "note11_period2"
            )
            pl[
                "year_ended_mar_31,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_year"
            ) + note11to12_calc_details.get(
                "note11_year"
            )
            total_revenue1_quarter1 = pl["quarter_ended_sep_30,_" + end_year]
            total_revenue1_quarter2 = pl["quarter_ended_june_30,_" + end_year]
            total_revenue1_quarter3 = pl[
                "quarter_ended_sep_30,_" + start_year
            ]  # noqa: 501
            total_revenue1_period1 = pl["period_ended_sep_30,_" + end_year]
            total_revenue1_period2 = pl["period_ended_sep_30,_" + start_year]
            total_revenue1_year = pl["year_ended_mar_31,_" + end_year]

        for i in range(7, 11):
            if pl.get("id") == 7:
                pl[
                    "quarter_ended_sep_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note13_quarter1"
                )
                pl[
                    "quarter_ended_june_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note13_quarter2"
                )
                pl[
                    "quarter_ended_sep_30,_" + start_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note13_quarter3"
                )
                pl[
                    "period_ended_sep_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note13_period1"
                )
                pl[
                    "period_ended_sep_30,_" + start_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note13_period2"
                )
                pl[
                    "year_ended_mar_31,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note13_year"
                )
            if pl.get("id") == 8:
                pl[
                    "quarter_ended_sep_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note14_quarter1"
                )
                pl[
                    "quarter_ended_june_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note14_quarter2"
                )
                pl[
                    "quarter_ended_sep_30,_" + start_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note14_quarter3"
                )
                pl[
                    "period_ended_sep_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note14_period1"
                )
                pl[
                    "period_ended_sep_30,_" + start_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note14_period2"
                )
                pl[
                    "year_ended_mar_31,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note14_year"
                )
            if pl.get("id") == 10:
                pl[
                    "quarter_ended_sep_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note15_quarter1"
                )
                pl[
                    "quarter_ended_june_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note15_quarter2"
                )
                pl[
                    "quarter_ended_sep_30,_" + start_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note15_quarter3"
                )
                pl[
                    "period_ended_sep_30,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note15_period1"
                )
                pl[
                    "period_ended_sep_30,_" + start_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note15_period2"
                )
                pl[
                    "year_ended_mar_31,_" + end_year
                ] = note13to15_calc_details.get(  # noqa: 501
                    "note15_year"
                )
            if pl.get("id") == i:
                report_calc_details["total_revenue2_quarter1"] += pl[
                    "quarter_ended_sep_30,_" + end_year
                ]
                report_calc_details["total_revenue2_quarter2"] += pl[
                    "quarter_ended_june_30,_" + end_year
                ]
                report_calc_details["total_revenue2_quarter3"] += pl[
                    "quarter_ended_sep_30,_" + start_year
                ]
                report_calc_details["total_revenue2_period1"] += pl[
                    "period_ended_sep_30,_" + end_year
                ]
                report_calc_details["total_revenue2_period2"] += pl[
                    "period_ended_sep_30,_" + start_year
                ]
                report_calc_details["total_revenue2_year"] += pl[
                    "year_ended_mar_31,_" + end_year
                ]
        if pl.get("id") == 11:
            pl["quarter_ended_sep_30,_" + end_year] = report_calc_details.get(
                "total_revenue2_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "total_revenue2_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "total_revenue2_quarter3"
            )
            pl["period_ended_sep_30,_" + end_year] = report_calc_details.get(
                "total_revenue2_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "total_revenue2_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = report_calc_details.get(
                "total_revenue2_year"
            )
        if pl.get("id") == 12:
            pl[
                "quarter_ended_sep_30,_" + end_year
            ] = total_revenue1_quarter1 - report_calc_details.get(
                "total_revenue2_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = total_revenue1_quarter2 - report_calc_details.get(
                "total_revenue2_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = total_revenue1_quarter3 - report_calc_details.get(
                "total_revenue2_quarter3"
            )
            pl[
                "period_ended_sep_30,_" + end_year
            ] = total_revenue1_period1 - report_calc_details.get(
                "total_revenue2_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = total_revenue1_period2 - report_calc_details.get(
                "total_revenue2_period2"
            )
            pl[
                "year_ended_mar_31,_" + end_year
            ] = total_revenue1_year - report_calc_details.get(
                "total_revenue2_year"
            )  # noqa: 501
            report_calc_details["profit_before_tax_quarter1"] = pl[
                "quarter_ended_sep_30,_" + end_year
            ]
            report_calc_details["profit_before_tax_quarter2"] = pl[
                "quarter_ended_june_30,_" + end_year
            ]
            report_calc_details["profit_before_tax_quarter3"] = pl[
                "quarter_ended_sep_30,_" + start_year
            ]
            report_calc_details["profit_before_tax_period1"] = pl[
                "period_ended_sep_30,_" + end_year
            ]
            report_calc_details["profit_before_tax_period2"] = pl[
                "period_ended_sep_30,_" + start_year
            ]
            report_calc_details["profit_before_tax_year"] = pl[
                "year_ended_mar_31,_" + end_year
            ]

        for i in range(14, 16):
            if pl.get("id") == 14:
                pl["quarter_ended_sep_30,_" + end_year] = (
                    pl["period_ended_sep_30,_" + end_year]
                    - pl["quarter_ended_june_30,_" + end_year]
                )
                report_calc_details["current_tax_period1"] = pl[
                    "period_ended_sep_30,_" + end_year
                ]
            if pl.get("id") == 15:
                pl["quarter_ended_sep_30,_" + end_year] = (
                    pl["period_ended_sep_30,_" + end_year] + 2353332.61532512
                )
            if pl.get("id") == i:
                report_calc_details["tax_revenue_quarter1"] += pl[
                    "quarter_ended_sep_30,_" + end_year
                ]
                report_calc_details["tax_revenue_quarter2"] += pl[
                    "quarter_ended_june_30,_" + end_year
                ]
                report_calc_details["tax_revenue_quarter3"] += pl[
                    "quarter_ended_sep_30,_" + start_year
                ]
                report_calc_details["tax_revenue_period1"] += pl[
                    "period_ended_sep_30,_" + end_year
                ]
                report_calc_details["tax_revenue_period2"] += pl[
                    "period_ended_sep_30,_" + start_year
                ]
                report_calc_details["tax_revenue_year"] += pl[
                    "year_ended_mar_31,_" + end_year
                ]
        if pl.get("id") == 16:
            pl["quarter_ended_sep_30,_" + end_year] = report_calc_details.get(
                "tax_revenue_quarter1"
            )
            pl[
                "quarter_ended_june_30,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "tax_revenue_quarter2"
            )
            pl[
                "quarter_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "tax_revenue_quarter3"
            )
            pl["period_ended_sep_30,_" + end_year] = report_calc_details.get(
                "tax_revenue_period1"
            )
            pl[
                "period_ended_sep_30,_" + start_year
            ] = report_calc_details.get(  # noqa: 501
                "tax_revenue_period2"
            )
            pl["year_ended_mar_31,_" + end_year] = report_calc_details.get(
                "tax_revenue_year"
            )
        if pl.get("id") == 17:
            pl["quarter_ended_sep_30,_" + end_year] = (
                total_revenue1_quarter1
                - report_calc_details.get("total_revenue2_quarter1")
                - report_calc_details.get("tax_revenue_quarter1")
            )
            pl["quarter_ended_june_30,_" + end_year] = (
                total_revenue1_quarter2
                - report_calc_details.get("total_revenue2_quarter2")
                - report_calc_details.get("tax_revenue_quarter2")
            )
            pl["quarter_ended_sep_30,_" + start_year] = (
                total_revenue1_quarter3
                - report_calc_details.get("total_revenue2_quarter3")
                - report_calc_details.get("tax_revenue_quarter3")
            )
            pl["period_ended_sep_30,_" + end_year] = (
                total_revenue1_period1
                - report_calc_details.get("total_revenue2_period1")
                - report_calc_details.get("tax_revenue_period1")
            )
            pl["period_ended_sep_30,_" + start_year] = (
                total_revenue1_period2
                - report_calc_details.get("total_revenue2_period2")
                - report_calc_details.get("tax_revenue_period2")
            )
            pl["year_ended_mar_31,_" + end_year] = (
                total_revenue1_year
                - report_calc_details.get("total_revenue2_year")
                - report_calc_details.get("tax_revenue_year")
            )
        if pl.get("id") == 19:
            pl["quarter_ended_sep_30,_" + end_year] = (
                total_revenue1_quarter1
                - report_calc_details.get("total_revenue2_quarter1")
                - report_calc_details.get("tax_revenue_quarter1")
            ) / 1500000
            pl["quarter_ended_june_30,_" + end_year] = (
                total_revenue1_quarter2
                - report_calc_details.get("total_revenue2_quarter2")
                - report_calc_details.get("tax_revenue_quarter2")
            ) / 1500000
            pl["quarter_ended_sep_30,_" + start_year] = (
                total_revenue1_quarter3
                - report_calc_details.get("total_revenue2_quarter3")
                - report_calc_details.get("tax_revenue_quarter3")
            ) / 1500000
            pl["period_ended_sep_30,_" + end_year] = (
                total_revenue1_period1
                - report_calc_details.get("total_revenue2_period1")
                - report_calc_details.get("tax_revenue_period1")
            ) / 1500000
            pl["period_ended_sep_30,_" + start_year] = (
                total_revenue1_period2
                - report_calc_details.get("total_revenue2_period2")
                - report_calc_details.get("tax_revenue_period2")
            ) / 1500000
            pl["year_ended_mar_31,_" + end_year] = (
                total_revenue1_year
                - report_calc_details.get("total_revenue2_year")
                - report_calc_details.get("tax_revenue_year")
            ) / 1500000
        if pl.get("id") == 20:
            pl["quarter_ended_sep_30,_" + end_year] = (
                total_revenue1_quarter1
                - report_calc_details.get("total_revenue2_quarter1")
                - report_calc_details.get("tax_revenue_quarter1")
            ) / 1500000
            pl["quarter_ended_june_30,_" + end_year] = (
                total_revenue1_quarter2
                - report_calc_details.get("total_revenue2_quarter2")
                - report_calc_details.get("tax_revenue_quarter2")
            ) / 1500000
            pl["quarter_ended_sep_30,_" + start_year] = (
                total_revenue1_quarter3
                - report_calc_details.get("total_revenue2_quarter3")
                - report_calc_details.get("tax_revenue_quarter3")
            ) / 1500000
            pl["period_ended_sep_30,_" + end_year] = (
                total_revenue1_period1
                - report_calc_details.get("total_revenue2_period1")
                - report_calc_details.get("tax_revenue_period1")
            ) / 1500000
            pl["period_ended_sep_30,_" + start_year] = (
                total_revenue1_period2
                - report_calc_details.get("total_revenue2_period2")
                - report_calc_details.get("tax_revenue_period2")
            ) / 1500000
            pl["year_ended_mar_31,_" + end_year] = (
                total_revenue1_year
                - report_calc_details.get("total_revenue2_year")
                - report_calc_details.get("tax_revenue_year")
            ) / 1500000
    calc_details = {
        "profit_before_tax_quarter1": report_calc_details.get(
            "profit_before_tax_quarter1"
        ),
        "profit_before_tax_quarter2": report_calc_details.get(
            "profit_before_tax_quarter2"
        ),
        "profit_before_tax_quarter3": report_calc_details.get(
            "profit_before_tax_quarter3"
        ),
        "profit_before_tax_period1": report_calc_details.get(
            "profit_before_tax_period1"
        ),
        "profit_before_tax_period2": report_calc_details.get(
            "profit_before_tax_period2"
        ),
        "profit_before_tax_year": report_calc_details.get(
            "profit_before_tax_year"
        ),  # noqa: 501
        "total_revenue2_quarter1": report_calc_details.get(
            "total_revenue2_quarter1"
        ),  # noqa: 501
        "total_revenue2_quarter2": report_calc_details.get(
            "total_revenue2_quarter2"
        ),  # noqa: 501
        "total_revenue2_quarter3": report_calc_details.get(
            "total_revenue2_quarter3"
        ),  # noqa: 501
        "total_revenue2_period1": report_calc_details.get(
            "total_revenue2_period1"
        ),  # noqa: 501
        "total_revenue2_period2": report_calc_details.get(
            "total_revenue2_period2"
        ),  # noqa: 501
        "total_revenue2_year": report_calc_details.get(
            "total_revenue2_year"
        ),  # noqa: 501
        "tax_revenue_quarter1": report_calc_details.get(
            "tax_revenue_quarter1"
        ),  # noqa: 501
        "tax_revenue_quarter2": report_calc_details.get(
            "tax_revenue_quarter2"
        ),  # noqa: 501
        "tax_revenue_quarter3": report_calc_details.get(
            "tax_revenue_quarter3"
        ),  # noqa: 501
        "tax_revenue_period1": report_calc_details.get("tax_revenue_period1"),
        "tax_revenue_period2": report_calc_details.get("tax_revenue_period2"),
        "tax_revenue_year": report_calc_details.get("tax_revenue_year"),
        "current_tax_period1": report_calc_details.get("current_tax_period1"),
    }
    report_calc_details["profit_before_tax_quarter1"] = 0
    report_calc_details["profit_before_tax_quarter2"] = 0
    report_calc_details["profit_before_tax_quarter3"] = 0
    report_calc_details["profit_before_tax_period1"] = 0
    report_calc_details["profit_before_tax_period2"] = 0
    report_calc_details["profit_before_tax_year"] = 0
    report_calc_details["total_revenue2_quarter1"] = 0
    report_calc_details["total_revenue2_quarter2"] = 0
    report_calc_details["total_revenue2_quarter3"] = 0
    report_calc_details["total_revenue2_period1"] = 0
    report_calc_details["total_revenue2_period2"] = 0
    report_calc_details["total_revenue2_year"] = 0
    report_calc_details["tax_revenue_quarter1"] = 0
    report_calc_details["tax_revenue_quarter2"] = 0
    report_calc_details["tax_revenue_quarter3"] = 0
    report_calc_details["tax_revenue_period1"] = 0
    report_calc_details["tax_revenue_period2"] = 0
    report_calc_details["tax_revenue_year"] = 0
    report_calc_details["current_tax_period1"] = 0
    return {"report_details": report_details, "calc_details": calc_details}


def report_calc(type):
    if type == "Profit Loss":
        return {
            "profit_before_tax_quarter1": 0,
            "profit_before_tax_quarter2": 0,
            "profit_before_tax_quarter3": 0,
            "profit_before_tax_period1": 0,
            "profit_before_tax_period2": 0,
            "profit_before_tax_year": 0,
            "total_revenue2_quarter1": 0,
            "total_revenue2_quarter2": 0,
            "total_revenue2_quarter3": 0,
            "total_revenue2_period1": 0,
            "total_revenue2_period2": 0,
            "total_revenue2_year": 0,
            "tax_revenue_quarter1": 0,
            "tax_revenue_quarter2": 0,
            "tax_revenue_quarter3": 0,
            "tax_revenue_period1": 0,
            "tax_revenue_period2": 0,
            "tax_revenue_year": 0,
            "current_tax_period1": 0,
        }
    if type == "Balance Sheet":
        return {
            "bs_non_current_assets_quarter1": 0,
            "bs_non_current_assets_quarter2": 0,
            "bs_non_current_assets_quarter3": 0,
            "bs_non_current_assets_period1": 0,
            "bs_non_current_assets_period2": 0,
            "bs_non_current_assets_year": 0,
            "bs_current_assets_quarter1": 0,
            "bs_current_assets_quarter2": 0,
            "bs_current_assets_quarter3": 0,
            "bs_current_assets_period1": 0,
            "bs_current_assets_period2": 0,
            "bs_current_assets_year": 0,
            "bs_fixed_assets_period1": 0,
            "bs_fixed_assets_year": 0,
        }
    if type == "Cash Flow":
        return {
            "cash_flow_operating_profit_quarter": 0,
            "cash_flow_operating_profit_year": 0,
            "cash_generated_quarter": 0,
            "cash_generated_year": 0,
            "net_cash_flow_quarter": 0,
            "net_cash_flow_year": 0,
            "maturity_bank_deposits_quarter": 0,
            "cash_flow_investing_profit_quarter": 0,
            "cash_flow_investing_profit_year": 0,
            "cash_flow_financing_profit_quarter": 0,
            "cash_flow_financing_profit_year": 0,
            "cash_beginning_quarter": 0,
            "cash_beginning_year": 0,
        }


def cash_flow(
    self, report_details, report_calc_details, calc_details, all_reports
):  # noqa: 501
    filter_year = frappe.get_doc("Fiscal Year", self.fiscal_year)
    date_format = "%Y-%m-%d"
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    end_year = str(end_year_date_object.year)
    profit_loss_calc_details = json.loads(
        all_reports.get("profit_loss").calc_details
    )  # noqa: 501
    balance_sheet_calc_details = json.loads(
        all_reports.get("balance_sheet").calc_details
    )
    note1to5_calc_details = json.loads(
        all_reports.get("note1to5").cnp_calc_details
    )  # noqa: 501
    note13to15_calc_details = json.loads(
        all_reports.get("note13to15").cnp_calc_details
    )  # noqa: 501
    note11to12_calc_details = json.loads(
        all_reports.get("note11to12").cnp_calc_details
    )  # noqa: 501
    note8to10_calc_details = json.loads(
        all_reports.get("note8to10").cnp_calc_details
    )  # noqa: 501
    for pl in report_details:
        if pl.get("id") == 1:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = profit_loss_calc_details.get(  # noqa: 501
                "profit_before_tax_period1"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = profit_loss_calc_details.get(  # noqa: 501
                "profit_before_tax_year"
            )
        if pl.get("id") == 3:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note13to15_calc_details.get(  # noqa: 501
                "note15_period1"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note13to15_calc_details.get(  # noqa: 501
                "note15_year"
            )
        if pl.get("id") == 4:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note13to15_calc_details.get(  # noqa: 501
                "note14_period1_provisions_doubtful_debts"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note13to15_calc_details.get(  # noqa: 501
                "note14_year_provisions_doubtful_debts"
            )
        if pl.get("id") == 5:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note13to15_calc_details.get(  # noqa: 501
                "note13_quarter3"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note13to15_calc_details.get(  # noqa: 501
                "note13_year"
            )
        if pl.get("id") == 7:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period1_cash_flow_net_gain"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_year_cash_flow_net_gain"
            )
        if pl.get("id") == 8:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period1_cash_flow_interest"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_year_cash_flow_interest"
            )
        for i in range(1, 9):
            if pl.get("id") == i:
                if isinstance(
                    pl["for_the_quarter_ended_sep,_" + end_year], int
                ):  # noqa: 501
                    report_calc_details[
                        "cash_flow_operating_profit_quarter"
                    ] += pl[  # noqa: 501
                        "for_the_quarter_ended_sep,_" + end_year
                    ]
                    report_calc_details[
                        "cash_flow_operating_profit_year"
                    ] += pl[  # noqa: 501
                        "for_the_year_ended_mar_31,_" + end_year
                    ]
        if pl.get("id") == 9:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "cash_flow_operating_profit_quarter"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "cash_flow_operating_profit_year"
            )
        if pl.get("id") == 11:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note5_period1_trade_payables"
            ) - note1to5_calc_details.get(
                "note5_year_trade_payables"
            )

        if pl.get("id") == 12:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_period1_long_term"
            ) - note1to5_calc_details.get(
                "note4_year_long_term"
            )

        if pl.get("id") == 13:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note4_period1_short_term"
            ) - note1to5_calc_details.get(
                "note4_year_short_term"
            )
        if pl.get("id") == 14:
            pl["for_the_quarter_ended_sep,_" + end_year] = (
                note1to5_calc_details.get("note5_period1_other_liabilities")
                - note1to5_calc_details.get("note5_year_other_liabilities")
                - (
                    note1to5_calc_details.get(
                        "note5_period1_income_tax_liabilty"
                    )  # noqa: 501
                    - note1to5_calc_details.get(
                        "note5_year_income_tax_liabilty"
                    )  # noqa: 501
                )
            )
        if pl.get("id") == 15:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note3_period1"
            ) - note1to5_calc_details.get(
                "note3_year"
            )

        if pl.get("id") == 16:
            pl["for_the_quarter_ended_sep,_" + end_year] = (
                note8to10_calc_details.get("note9_1_period1")
                - note8to10_calc_details["sub1_note9_period1"]
            ) + (
                note8to10_calc_details.get("note9_2_period1")
                - note8to10_calc_details["sub2_note9_period1"]
            )
        if pl.get("id") == 17:
            pl["for_the_quarter_ended_sep,_" + end_year] = (
                (
                    note8to10_calc_details.get("note8_1_period1")
                    + note8to10_calc_details.get("note8_2_period1")
                )
                - (
                    note8to10_calc_details.get("note8_1_year")
                    + note8to10_calc_details.get("note8_2_year")
                )
            ) + (
                note8to10_calc_details.get("note8_2_year")
                - note8to10_calc_details.get("note8_period1")
            )
        if pl.get("id") == 18:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_current_period1"
            ) - note8to10_calc_details.get(
                "note9_2_current_year"
            )
        for i in range(9, 19):
            if pl.get("id") == i:
                if isinstance(
                    pl["for_the_quarter_ended_sep,_" + end_year], int
                ):  # noqa: 501
                    report_calc_details["cash_generated_quarter"] += pl[
                        "for_the_quarter_ended_sep,_" + end_year
                    ]
                    report_calc_details["cash_generated_year"] += pl[
                        "for_the_year_ended_mar_31,_" + end_year
                    ]
        if pl.get("id") == 19:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "cash_generated_quarter"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "cash_generated_year"
            )
        if pl.get("id") == 20:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = profit_loss_calc_details.get(  # noqa: 501
                "current_tax_period1"
            ) + (
                note1to5_calc_details.get("note5_period1_income_tax_liabilty")
                - note1to5_calc_details.get("note5_year_income_tax_liabilty")
            )
        for i in range(19, 21):
            if pl.get("id") == i:
                if isinstance(
                    pl["for_the_quarter_ended_sep,_" + end_year], int
                ):  # noqa: 501
                    report_calc_details["net_cash_flow_quarter"] += pl[
                        "for_the_quarter_ended_sep,_" + end_year
                    ]
                    report_calc_details["net_cash_flow_year"] += pl[
                        "for_the_year_ended_mar_31,_" + end_year
                    ]
        if pl.get("id") == 21:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "net_cash_flow_quarter"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "net_cash_flow_year"
            )
        if pl.get("id") == 24:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = balance_sheet_calc_details.get(
                "bs_fixed_assets_year"
            ) - balance_sheet_calc_details.get(
                "bs_fixed_assets_period1"
            )
        if pl.get("id") == 26:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_2_period1"
            ) - note8to10_calc_details.get(
                "note10_2_year"
            )
        if pl.get("id") == 27:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note9_2_non_current_year"
            ) - note8to10_calc_details.get(
                "note9_2_non_current_period1"
            )
        if pl.get("id") == 28:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "maturity_bank_deposits_quarter"
            )
        if pl.get("id") == 29:
            pl["for_the_quarter_ended_sep,_" + end_year] = (
                note8to10_calc_details.get("note8_year")
                - note8to10_calc_details.get("note8_period1")
                - note11to12_calc_details.get(
                    "note12_period1_cash_flow_net_gain"
                )  # noqa: 501
                - report_calc_details.get("maturity_bank_deposits_quarter")
            )
        if pl.get("id") == 30:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note11to12_calc_details.get(  # noqa: 501
                "note12_period1_cash_flow_interest"
            ) + (
                note8to10_calc_details.get("note9_2_interest_fd_year")
                - note8to10_calc_details.get("note9_2_interest_fd_period1")
            )
        for i in range(24, 31):
            if pl.get("id") == i:
                if isinstance(
                    pl["for_the_quarter_ended_sep,_" + end_year], int
                ):  # noqa: 501
                    report_calc_details[
                        "cash_flow_investing_profit_quarter"
                    ] += pl[  # noqa: 501
                        "for_the_quarter_ended_sep,_" + end_year
                    ]
                    report_calc_details[
                        "cash_flow_investing_profit_year"
                    ] += pl[  # noqa: 501
                        "for_the_year_ended_mar_31,_" + end_year
                    ]
        if pl.get("id") == 31:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = report_calc_details[  # noqa: 501
                "cash_flow_investing_profit_quarter"
            ]
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = report_calc_details[  # noqa: 501
                "cash_flow_investing_profit_year"
            ]
        if pl.get("id") == 34:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_proposed_dividend_period1"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note1to5_calc_details.get(  # noqa: 501
                "note2_proposed_dividend_year"
            )
        for i in range(34, 36):
            if pl.get("id") == i:
                if isinstance(
                    pl["for_the_quarter_ended_sep,_" + end_year], int
                ):  # noqa: 501
                    report_calc_details[
                        "cash_flow_financing_profit_quarter"
                    ] += pl[  # noqa: 501
                        "for_the_quarter_ended_sep,_" + end_year
                    ]
                    report_calc_details[
                        "cash_flow_financing_profit_year"
                    ] += pl[  # noqa: 501
                        "for_the_year_ended_mar_31,_" + end_year
                    ]
        if pl.get("id") == 36:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "cash_flow_financing_profit_quarter"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = report_calc_details.get(  # noqa: 501
                "cash_flow_financing_profit_year"
            )
        if pl.get("id") == 38:
            pl["for_the_quarter_ended_sep,_" + end_year] = (
                report_calc_details.get("net_cash_flow_quarter")
                + report_calc_details["cash_flow_investing_profit_quarter"]
                + report_calc_details.get(
                    "cash_flow_financing_profit_quarter"
                )  # noqa: 501
            )
            pl["for_the_year_ended_mar_31,_" + end_year] = (
                report_calc_details.get("net_cash_flow_year")
                + report_calc_details["cash_flow_investing_profit_year"]
                + report_calc_details.get("cash_flow_financing_profit_year")
            )
            report_calc_details["net_total_quarter"] = pl[
                "for_the_quarter_ended_sep,_" + end_year
            ]
            report_calc_details["net_total_year"] = pl[
                "for_the_year_ended_mar_31,_" + end_year
            ]
        if pl.get("id") == 39:
            report_calc_details["cash_beginning_quarter"] = pl[
                "for_the_quarter_ended_sep,_" + end_year
            ]
            report_calc_details["cash_beginning_year"] = pl[
                "for_the_year_ended_mar_31,_" + end_year
            ]
        if pl.get("id") == 40:
            pl["for_the_quarter_ended_sep,_" + end_year] = (
                report_calc_details["cash_beginning_quarter"]
                + report_calc_details["net_total_quarter"]
            )
            pl["for_the_year_ended_mar_31,_" + end_year] = (
                report_calc_details["cash_beginning_year"]
                + report_calc_details["net_total_year"]
            )
        if pl.get("id") == 43:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_period1"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_year"
            )
        if pl.get("id") == 44:
            pl[
                "for_the_quarter_ended_sep,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_period1"
            )
            pl[
                "for_the_year_ended_mar_31,_" + end_year
            ] = note8to10_calc_details.get(  # noqa: 501
                "note10_1_year"
            )
    calc_details = {
        "cash_generated_quarter": report_calc_details.get(
            "cash_generated_quarter"
        ),  # noqa: 501
        "cash_generated_year": report_calc_details.get("cash_generated_year"),
        "cash_flow_operating_profit_quarter": report_calc_details.get(
            "cash_flow_operating_profit_quarter"
        ),
        "cash_flow_operating_profit_year": report_calc_details.get(
            "cash_flow_operating_profit_year"
        ),
        "net_cash_flow_quarter": report_calc_details.get(
            "net_cash_flow_quarter"
        ),  # noqa: 501
        "net_cash_flow_year": report_calc_details.get("net_cash_flow_year"),
        "maturity_bank_deposits_quarter": report_calc_details.get(
            "maturity_bank_deposits_quarter"
        ),
        "cash_flow_investing_profit_quarter": report_calc_details.get(
            "cash_flow_investing_profit_quarter"
        ),
        "cash_flow_investing_profit_year": report_calc_details.get(
            "cash_flow_investing_profit_year"
        ),
        "cash_flow_financing_profit_quarter": report_calc_details.get(
            "cash_flow_financing_profit_quarter"
        ),
        "cash_flow_financing_profit_year": report_calc_details.get(
            "cash_flow_financing_profit_year"
        ),
        "net_total_quarter": report_calc_details.get("net_total_quarter"),
        "net_total_year": report_calc_details.get("net_total_year"),
        "cash_beginning_quarter": report_calc_details.get(
            "cash_beginning_quarter"
        ),  # noqa: 501
        "cash_beginning_year": report_calc_details.get("cash_beginning_year"),
    }
    report_calc_details["net_cash_flow_quarter"] = 0
    report_calc_details["net_cash_flow_year"] = 0
    report_calc_details["cash_flow_operating_profit_quarter"] = 0
    report_calc_details["cash_flow_operating_profit_year"] = 0
    report_calc_details["cash_generated_quarter"] = 0
    report_calc_details["cash_generated_year"] = 0
    report_calc_details["maturity_bank_deposits_quarter"] = 0
    report_calc_details["cash_flow_investing_profit_quarter"] = 0
    report_calc_details["cash_flow_investing_profit_year"] = 0
    report_calc_details["cash_flow_financing_profit_quarter"] = 0
    report_calc_details["cash_flow_financing_profit_year"] = 0
    report_calc_details["net_total_quarter"] = 0
    report_calc_details["net_total_year"] = 0
    report_calc_details["cash_beginning_quarter"] = 0
    report_calc_details["cash_beginning_year"] = 0
    return {"report_details": report_details, "calc_details": calc_details}


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    # Replace the '-' in fiscal_year with '_'
    # and concatenate with formattedCompany
    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_calc(type):
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    if type == "Note 1 to 5":
        calc_details = {
            "note1_quarter1": 0,
            "note1_quarter2": 0,
            "note1_quarter3": 0,
            "note1_period1": 0,
            "note1_period2": 0,
            "note1_year": 0,
            "note2_quarter1": 0,
            "note2_quarter2": 0,
            "note2_quarter3": 0,
            "note2_period1": 0,
            "note2_period2": 0,
            "note2_year": 0,
            "note2_quarter1_a": 0,
            "note2_quarter2_a": 0,
            "note2_quarter3_a": 0,
            "note2_period1_a": 0,
            "note2_period2_a": 0,
            "note2_year_a": 0,
            "note2_quarter1_b": 0,
            "note2_quarter2_b": 0,
            "note2_quarter3_b": 0,
            "note2_period1_b": 0,
            "note2_period2_b": 0,
            "note2_year_b": 0,
            "note3_quarter1": 0,
            "note3_quarter2": 0,
            "note3_quarter3": 0,
            "note3_period1": 0,
            "note3_period2": 0,
            "note3_year": 0,
            "note4_quarter1_long_term": 0,
            "note4_quarter2_long_term": 0,
            "note4_quarter3_long_term": 0,
            "note4_period1_long_term": 0,
            "note4_period2_long_term": 0,
            "note4_year_long_term": 0,
            "note4_quarter1_short_term": 0,
            "note4_quarter2_short_term": 0,
            "note4_quarter3_short_term": 0,
            "note4_period1_short_term": 0,
            "note4_period2_short_term": 0,
            "note4_year_short_term": 0,
            "note5_quarter1_trade_payables": 0,
            "note5_quarter2_trade_payables": 0,
            "note5_quarter3_trade_payables": 0,
            "note5_period1_trade_payables": 0,
            "note5_period2_trade_payables": 0,
            "note5_year_trade_payables": 0,
            "note5_quarter1_other_liabilities": 0,
            "note5_quarter2_other_liabilities": 0,
            "note5_quarter3_other_liabilities": 0,
            "note5_period1_other_liabilities": 0,
            "note5_period2_other_liabilities": 0,
            "note5_year_other_liabilities": 0,
            "note5_period1_income_tax_liabilty": 0,
            "note5_year_income_tax_liabilty": 0,
            "note2_proposed_dividend_period1": 0,
            "note2_proposed_dividend_year": 0,
        }
    if type == "Note 6":
        calc_details = {
            "note6_gross_block_computers": 0,
            "note6_gross_block_computer_server": 0,
            "note6_gross_block_mobile": 0,
            "note6_gross_block_furniture_and_fixture": 0,
            "note6_gross_block_office_equipment": 0,
            "note6_gross_block_total": 0,
            "note6_depreciation_computers": 0,
            "note6_depreciation_computer_server": 0,
            "note6_depreciation_mobile": 0,
            "note6_depreciation_furniture_and_fixture": 0,
            "note6_depreciation_office_equipment": 0,
            "note6_depreciation_total": 0,
            "note6_net_block_computers": 0,
            "note6_net_block_computer_server": 0,
            "note6_net_block_mobile": 0,
            "note6_net_block_furniture_and_fixture": 0,
            "note6_net_block_office_equipment": 0,
            "note6_net_block_total": 0,
        }
    if type == "Note 7":
        calc_details = {
            "note7_gross_block_computer_software": 0,
            "note7_depreciation_computer_software": 0,
            "note7_net_block_computer_software": 0,
        }
    if type == "Note 8 to 10":
        calc_details = {
            "note8_quarter1": 0,
            "note8_quarter2": 0,
            "note8_quarter3": 0,
            "note8_period1": 0,
            "note8_period2": 0,
            "note8_year": 0,
            "note8_1_quarter1": 0,
            "note8_1_quarter2": 0,
            "note8_1_quarter3": 0,
            "note8_1_period1": 0,
            "note8_1_period2": 0,
            "note8_1_year": 0,
            "note8_2_quarter1": 0,
            "note8_2_quarter2": 0,
            "note8_2_quarter3": 0,
            "note8_2_period1": 0,
            "note8_2_period2": 0,
            "note8_2_year": 0,
            "note9_1_quarter1": 0,
            "note9_1_quarter2": 0,
            "note9_1_quarter3": 0,
            "note9_1_period1": 0,
            "note9_1_period2": 0,
            "note9_1_year": 0,
            "note9_2_quarter1": 0,
            "note9_2_quarter2": 0,
            "note9_2_quarter3": 0,
            "note9_2_period1": 0,
            "note9_2_period2": 0,
            "note9_2_year": 0,
            "note9_2_non_current_quarter1": 0,
            "note9_2_non_current_quarter2": 0,
            "note9_2_non_current_quarter3": 0,
            "note9_2_non_current_period1": 0,
            "note9_2_non_current_period2": 0,
            "note9_2_non_current_year": 0,
            "note9_2_current_quarter1": 0,
            "note9_2_current_quarter2": 0,
            "note9_2_current_quarter3": 0,
            "note9_2_current_period1": 0,
            "note9_2_current_period2": 0,
            "note9_2_current_year": 0,
            "sub1_note9_quarter1": 0,
            "sub1_note9_quarter2": 0,
            "sub1_note9_quarter3": 0,
            "sub1_note9_period1": 0,
            "sub1_note9_period2": 0,
            "sub1_note9_year": 0,
            "sub2_note9_quarter1": 0,
            "sub2_note9_quarter2": 0,
            "sub2_note9_quarter3": 0,
            "sub2_note9_period1": 0,
            "sub2_note9_period2": 0,
            "sub2_note9_year": 0,
            "note10_1_quarter1": 0,
            "note10_1_quarter2": 0,
            "note10_1_quarter3": 0,
            "note10_1_period1": 0,
            "note10_1_period2": 0,
            "note10_1_year": 0,
            "note10_2_quarter1": 0,
            "note10_2_quarter2": 0,
            "note10_2_quarter3": 0,
            "note10_2_period1": 0,
            "note10_2_period2": 0,
            "note10_2_year": 0,
            "note9_2_interest_fd_period1": 0,
            "note9_2_interest_fd_year": 0,
        }
    if type == "Note 11 to 12":
        calc_details = {
            "note12_period1_cash_flow_interest": 0,
            "note12_year_cash_flow_interest": 0,
            "note12_period1_cash_flow_net_gain": 0,
            "note12_year_cash_flow_net_gain": 0,
            "note11_quarter1": 0,
            "note11_quarter2": 0,
            "note11_quarter3": 0,
            "note11_period1": 0,
            "note11_period2": 0,
            "note11_year": 0,
            "note12_quarter1": 0,
            "note12_quarter2": 0,
            "note12_quarter3": 0,
            "note12_period1": 0,
            "note12_period2": 0,
            "note12_year": 0,
        }
    if type == "Note 13 to 15":
        calc_details = {
            "note14_period1_provisions_doubtful_debts": 0,
            "note14_year_provisions_doubtful_debts": 0,
            "note13_quarter1": 0,
            "note13_quarter2": 0,
            "note13_quarter3": 0,
            "note13_period1": 0,
            "note13_period2": 0,
            "note13_year": 0,
            "note14_quarter1": 0,
            "note14_quarter2": 0,
            "note14_quarter3": 0,
            "note14_period1": 0,
            "note14_period2": 0,
            "note14_year": 0,
            "note15_quarter1": 0,
            "note15_quarter2": 0,
            "note15_quarter3": 0,
            "note15_period1": 0,
            "note15_period2": 0,
            "note15_year": 0,
        }
    calc_details = json.dumps(calc_details)
    return calc_details


def note_json(self, type):
    filter_year = frappe.get_doc("Fiscal Year", self.fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    if type == "Note 1 to 5":
        note_details = [
            {
                "particulars": "Note 1: Share Capital",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Authorised",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "50,00,000 Equity Shares of Rs.10/- each",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 3,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 4,
            },
            {
                "particulars": "Subscribed and Fully Paid-up",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 5,
            },
            {
                "particulars": "1,500,000 Equity Shares of Rs. 10/- each",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 6,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 7,
            },
            {
                "particulars": "The above Equity Shares are entirely "
                + "held by Holding Company and its nominees.",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 8,
            },
            {
                "particulars": "Note 2: Reserves And Surplus",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 9,
            },
            {
                "particulars": "General Reserve",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "As per last Account",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 11,
            },
            {
                "particulars": "Add: Transfer from Profit & Loss Account",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 12,
            },
            {
                "particulars": "a",
                "Sep 30,  2023": 3,
                "June 30,  2023": 0,
                "Sep 30,  2022": 0,
                "Sep 30, 2023": 0,
                "Sep 30, 2022": 0,
                "Mar 31, 2023": 0,
                "indent": 1,
                "id": 13,
            },
            {
                "particulars": "Surplus",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 14,
            },
            {
                "particulars": "As per last Account",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 15,
            },
            {
                "particulars": "Profit/(Loss) during the period",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 16,
            },
            {
                "particulars": "Proposed Dividend",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 17,
            },
            {
                "particulars": "Transfer to General Reserve",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 18,
            },
            {
                "particulars": "b",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 19,
            },
            {
                "particulars": "(a+b)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 20,
            },
            {
                "particulars": "Note 3: Other Long Term Liabilities",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 21,
            },
            {
                "particulars": "Unearned Revenue",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 22,
            },
            {
                "particulars": "Lease Equalisation Account",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 23,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 24,
            },
            {
                "particulars": "Note 4: Provisions",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 25,
            },
            {
                "particulars": "Long Term",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 26,
            },
            {
                "particulars": "Provision for gratuity",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 27,
            },
            {
                "particulars": "Provision for Leave Benefits",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 28,
            },
            {
                "particulars": "Retention Pay",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 29,
            },
            {
                "particulars": "Provision for Employee Benefits "
                + "(Variable Pay)",  # noqa: 501
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 30,
            },
            {
                "particulars": "a",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 31,
            },
            {
                "particulars": "Short Term",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 32,
            },
            {
                "particulars": "Provision for Employee Benefits "
                + "(Variable Pay)Incl.Deputed",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 33,
            },
            {
                "particulars": "Provision for gratuity",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 34,
            },
            {
                "particulars": "Provision for Leave Benefits",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 35,
            },
            {
                "particulars": "Provision For Expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 36,
            },
            {
                "particulars": "b",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 37,
            },
            {
                "particulars": "(a + b)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 38,
            },
            {
                "particulars": "Note 5: Other Current Liabilites",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 39,
            },
            {
                "particulars": "Trades Payable",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 40,
            },
            {
                "particulars": "a",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 41,
            },
            {
                "particulars": "Other Liabilities",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 42,
            },
            {
                "particulars": "Unearned Revenue",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 43,
            },
            {
                "particulars": "Other Advances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 44,
            },
            {
                "particulars": "Employees Contribution to PF/ESIC",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 45,
            },
            {
                "particulars": "Professional Tax",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 46,
            },
            {
                "particulars": "Due to the Holding Company - "
                + "Axis Bank Limited",  # noqa: 501
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 47,
            },
            {
                "particulars": "TDS Payable",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 48,
            },
            {
                "particulars": "Service/GST Tax Payable",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 49,
            },
            {
                "particulars": "Income Tax Liability",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 50,
            },
            {
                "particulars": "b",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 51,
            },
            {
                "particulars": "(a + b)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 52,
            },
        ]
    if type == "Note 6":
        note_details = [
            {
                "particulars": "Gross block",
                "computers": " ",
                "computer_server": " ",
                "mobile": " ",
                "furniture_and_fixtures": " ",
                "office_equipment": " ",
                "total": " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "At April 1, " + end_year,
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Additions",
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 2,
                "id": 3,
            },
            {
                "particulars": "Disposals",
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 2,
                "id": 4,
            },
            {
                "particulars": "At Sep 30, " + end_year,
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 0,
                "id": 5,
            },
            {
                "particulars": " ",
                "computers": " ",
                "computer_server": " ",
                "mobile": " ",
                "furniture_and_fixtures": " ",
                "office_equipment": " ",
                "total": " ",
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": "Depreciation",
                "computers": " ",
                "computer_server": " ",
                "mobile": " ",
                "furniture_and_fixtures": " ",
                "office_equipment": " ",
                "total": " ",
                "indent": 0,
                "id": 7,
            },
            {
                "particulars": "At April 1, " + end_year,
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "Charge for the period",
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 2,
                "id": 9,
            },
            {
                "particulars": "Disposals",
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 2,
                "id": 10,
            },
            {
                "particulars": "At Sep 30, " + end_year,
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 0,
                "id": 11,
            },
            {
                "particulars": " ",
                "computers": " ",
                "computer_server": " ",
                "mobile": " ",
                "furniture_and_fixtures": " ",
                "office_equipment": " ",
                "total": " ",
                "indent": 0,
                "id": 12,
            },
            {
                "particulars": "Net Block",
                "computers": " ",
                "computer_server": " ",
                "mobile": " ",
                "furniture_and_fixtures": " ",
                "office_equipment": " ",
                "total": " ",
                "indent": 0,
                "id": 13,
            },
            {
                "particulars": "At Sep 30, " + end_year,
                "computers": 0,
                "computer_server": 0,
                "mobile": 0,
                "furniture_and_fixtures": 0,
                "office_equipment": 0,
                "total": 0,
                "indent": 1,
                "id": 14,
            },
        ]
    if type == "Note 7":
        note_details = [
            {
                "particulars": "Gross block",
                "computer_software": " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "At April 1, " + end_year,
                "computer_software": 0,
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Additions",
                "computer_software": 0,
                "indent": 2,
                "id": 3,
            },  # noqa: 501
            {
                "particulars": "Disposals",
                "computer_software": 0,
                "indent": 2,
                "id": 4,
            },  # noqa: 501
            {
                "particulars": "At Sep 30, " + end_year,
                "computer_software": 0,
                "indent": 0,
                "id": 5,
            },
            {
                "particulars": " ",
                "computer_software": " ",
                "indent": 0,
                "id": 6,
            },  # noqa: 501
            {
                "particulars": "Depreciation",
                "computer_software": " ",
                "indent": 0,
                "id": 7,
            },
            {
                "particulars": "At April 1, " + end_year,
                "computer_software": 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "Charge for the period",
                "computer_software": 0,
                "indent": 2,
                "id": 9,
            },
            {
                "particulars": "Disposals",
                "computer_software": 0,
                "indent": 2,
                "id": 10,
            },  # noqa: 501
            {
                "particulars": "At Sep 30, " + end_year,
                "computer_software": 0,
                "indent": 0,
                "id": 11,
            },
            {
                "particulars": " ",
                "computer_software": " ",
                "indent": 0,
                "id": 12,
            },  # noqa: 501
            {
                "particulars": "Net Block",
                "computer_software": " ",
                "indent": 0,
                "id": 13,
            },
            {
                "particulars": "At Sep 30, " + end_year,
                "computer_software": 0,
                "indent": 1,
                "id": 14,
            },
        ]
    if type == "Note 8 to 10":
        note_details = [
            {
                "particulars": "8. Investments",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Current Non- Trade Investments",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Investments in Liquid Funds",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 3,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 4,
            },
            {
                "particulars": "8.1  Loans and Advances",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 5,
            },
            {
                "particulars": "Non-Current",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 1,
                "id": 6,
            },
            {
                "particulars": "Advance Payament of Taxes",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 7,
            },
            {
                "particulars": "Less Shown Separtaely "
                + "in other current liabilities",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 8,
            },
            {
                "particulars": "a",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 9,
            },
            {
                "particulars": "Current",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "Prepaid Expenses",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 11,
            },
            {
                "particulars": "Deposit with Central Registry",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 12,
            },
            {
                "particulars": "Advance with Corporate Credit Card",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 13,
            },
            {
                "particulars": "Other Advances",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 14,
            },
            {
                "particulars": "Service Tax/GST Receivable",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 15,
            },
            {
                "particulars": "Advance to Reliance Commercial Finance",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 16,
            },
            {
                "particulars": "b",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 17,
            },
            {
                "particulars": "(a+b)",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 0,
                "id": 18,
            },
            {
                "particulars": "Note 9:  Trade Receivables",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 19,
            },
            {
                "particulars": "Current",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 1,
                "id": 20,
            },
            {
                "particulars": "Unsecured, considered "
                + "good unless stated otherwise",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 21,
            },
            {
                "particulars": "Outstanding for a period "
                + "exceeding six months from ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 22,
            },
            {
                "particulars": "the date they are due for payment",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 23,
            },
            {
                "particulars": "Secured, Considered good",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 24,
            },
            {
                "particulars": "Unsecured, Considered good",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 25,
            },
            {
                "particulars": "Doubtful",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 26,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 27,
            },
            {
                "particulars": "Provision for doubtful receivables",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 28,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 29,
            },
            {
                "particulars": "Other receivables( less than 180 days)",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 30,
            },
            {
                "particulars": "Secured, Considered good",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 31,
            },
            {
                "particulars": "Unsecured, Considered good",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 32,
            },
            {
                "particulars": "Doubtful",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 33,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 34,
            },
            {
                "particulars": "Provision for doubtful receivables",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 35,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 36,
            },
            {
                "particulars": "Total",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 0,
                "id": 37,
            },
            {
                "particulars": "9.2 Other Assets",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 38,
            },
            {
                "particulars": "Non-Current",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 1,
                "id": 39,
            },
            {
                "particulars": "Unsecured, considered good "
                + "unless stated otherwise",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 40,
            },
            {
                "particulars": "Non-current Bank Balances "
                + "(FD maturing after 12 months)",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 41,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 42,
            },
            {
                "particulars": "Current",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 1,
                "id": 43,
            },
            {
                "particulars": "Due from the Holding Company - "
                + "Axis Bank Limited",  # noqa: 501
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 44,
            },
            {
                "particulars": "Due from other subsidiaries",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 45,
            },
            {
                "particulars": "Other Deposits",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 46,
            },
            {
                "particulars": "Interest Accrued on Fixed Deposits",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 47,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 2,
                "id": 48,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 49,
            },
            {
                "particulars": "Note 10: Cash & Bank Balances",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 50,
            },
            {
                "particulars": "Current",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 51,
            },
            {
                "particulars": "Cash and Cash Equivalents",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 52,
            },
            {
                "particulars": "Balances with banks:",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 53,
            },
            {
                "particulars": "On current Account",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 54,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 55,
            },
            {
                "particulars": "Other Bank Balances :",
                "quarter_ended_sep_30,_+end_year": " ",
                "quarter_ended_june_30,_+end_year": " ",
                "quarter_ended_sep_30,_+start_year": " ",
                "period_ended_sep_30,_+end_year": " ",
                "period_ended_sep_30,_+start_year": " ",
                "year_ended_mar_31,_+end_year": " ",
                "indent": 0,
                "id": 56,
            },
            {
                "particulars": "Deposit with remaining maturity "
                + "for less than 12 months",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 57,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 1,
                "id": 58,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_+end_year": 0,
                "quarter_ended_june_30,_+end_year": 0,
                "quarter_ended_sep_30,_+start_year": 0,
                "period_ended_sep_30,_+end_year": 0,
                "period_ended_sep_30,_+start_year": 0,
                "year_ended_mar_31,_+end_year": 0,
                "indent": 0,
                "id": 59,
            },
        ]
    if type == "Note 11 to 12":
        note_details = [
            {
                "particulars": "Note 11: Revenue from Operations",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Trusteeship Fees",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Initial Acceptance Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 3,
            },
            {
                "particulars": "Annual Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 4,
            },
            {
                "particulars": "Servicing Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 5,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": "Note 12 : Other Income",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 7,
            },
            {
                "particulars": "Interest on Fixed Deposits",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "Capital Gain on Mutual Fund",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 9,
            },
            {
                "particulars": "Profit of FA",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "FC Exchange Gain",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 11,
            },
            {
                "particulars": "Insurance claim received",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 12,
            },
            {
                "particulars": "Recovery of Bad Debts",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 13,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 14,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 15,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 16,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 17,
            },
            {
                "particulars": "Initial Acceptance Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 18,
            },
            {
                "particulars": "Annual Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 19,
            },
            {
                "particulars": "Servicing Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 20,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 21,
            },
            {
                "particulars": "Adjusted Operating Income",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 22,
            },
            {
                "particulars": "Add GST @ 12%",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 23,
            },
            {
                "particulars": "Total",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 24,
            },
        ]
    if type == "Note 13 to 15":
        note_details = [
            {
                "particulars": "Note 13 : Employee Benefit Expenses",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Salaries & Wages",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Contribution to Provident, "
                + "Gratuity and Other Funds",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "particulars": "Staff Welfare Expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 4,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 5,
            },
            {
                "particulars": "Note 14 : Other Expenses",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 7,
            },
            {
                "particulars": "Rent",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "Rates and Taxes",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 9,
            },
            {
                "particulars": "Printing and Stationery",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "Travelling & Conveyance",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 11,
            },
            {
                "particulars": "Conference Expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 12,
            },
            {
                "particulars": "Communication Expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 13,
            },
            {
                "particulars": "Legal & Professional Charges",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "particulars": "Auditors Remuneration:",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "particulars": "As Auditors",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 16,
            },
            {
                "particulars": "In other capacity for "
                + "Certificates and other Services",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 17,
            },
            {
                "particulars": "Directors Sitting Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 18,
            },
            {
                "particulars": "Electricity Charges",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 19,
            },
            {
                "particulars": "Bank Charges",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 20,
            },
            {
                "particulars": "Doubtful Debts Written Off",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 21,
            },
            {
                "particulars": "Provision for Doubtful Debts",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 22,
            },
            {
                "particulars": "DP Charges",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 23,
            },
            {
                "particulars": "Office Expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 24,
            },
            {
                "particulars": "Advertisement & Business Promotion Expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 25,
            },
            {
                "particulars": "Annual Maintainence Charges of Software",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 26,
            },
            {
                "particulars": "Registeration Fee",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 27,
            },
            {
                "particulars": "Contribution for CSR activities",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 28,
            },
            {
                "particulars": "Royalty Charges",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 29,
            },
            {
                "particulars": "Referral Fees",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 30,
            },
            {
                "particulars": "Other Payments",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 31,
            },
            {
                "particulars": "Exchange Loss",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 32,
            },
            {
                "particulars": "Microsoft Licence Fees / website Development",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 33,
            },
            {
                "particulars": "Bad debts recognised",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 34,
            },
            {
                "particulars": "Less : write Off",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 35,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 36,
            },
            {
                "particulars": "Note 15: Depreciation & Amortisation",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 37,
            },
            {
                "particulars": "On Tangible Fixed Assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 38,
            },
            {
                "particulars": "On Intangible Assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 39,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 40,
            },
        ]
    note_details = json.dumps(note_details)
    return note_details


def insert_note(self, type, formatted_string):
    note = frappe.new_doc("Note")
    note.title = formatted_string
    note.cnp_note_details = note_json(self, type)
    note.cnp_calc_details = note_calc(type)
    note.cnp_type = type
    note.cnp_fiscal_year = self.fiscal_year
    note.cnp_company = self.company
    note.public = 1
    note.insert()
    frappe.db.commit()
    return note


def insert_report(self, type, formatted_string):
    canopi_reports = frappe.new_doc("Canopi Reports")
    canopi_reports.title = formatted_string
    canopi_reports.report_details = report_json(self, type)
    canopi_reports.type = type
    canopi_reports.calc_details = json.dumps(report_calc(type))
    canopi_reports.fiscal_year = self.fiscal_year
    canopi_reports.company = self.company
    canopi_reports.insert()
    frappe.db.commit()
    return canopi_reports


def report_json(self, type):
    filter_year = frappe.get_doc("Fiscal Year", self.fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    end_year = str(end_year_date_object.year)
    start_year = str(start_year_date_object.year)
    if type == "Balance Sheet":
        report_details = [
            {
                "particulars": "Equity and liabilities",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Shareholders funds",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 2,
            },
            {
                "particulars": "Share capital",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "particulars": "Reserves & surplus",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 4,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 5,
            },
            {
                "particulars": "",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": "Non-current liabilites",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 7,
            },
            {
                "particulars": "Other long term liabilities",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "Long term provisions",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 9,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 11,
            },
            {
                "particulars": "Current liabilities",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 12,
            },
            {
                "particulars": "Trade payables",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 13,
            },
            {
                "particulars": "Other current liabilities",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "particulars": "Short term provisions",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 16,
            },
            {
                "particulars": "Total",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 17,
            },
            {
                "particulars": "Assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 18,
            },
            {
                "particulars": "Non-current assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 19,
            },
            {
                "particulars": "Fixed assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 20,
            },
            {
                "particulars": "Tangible assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 21,
            },
            {
                "particulars": "Intangible assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 22,
            },
            {
                "particulars": "Intangible asset under development",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 2,
                "id": 23,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 24,
            },
            {
                "particulars": "Deferred Tax Asset (net)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 25,
            },
            {
                "particulars": "Other non-current assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 26,
            },
            {
                "particulars": "Loans &  Advances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 27,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 28,
            },
            {
                "particulars": "Current assets",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 29,
            },
            {
                "particulars": "Current Investments",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 30,
            },
            {
                "particulars": "Loans &  Adavances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 31,
            },
            {
                "particulars": "Trade receivables",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 32,
            },
            {
                "particulars": "Cash and Bank balances",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 33,
            },
            {
                "particulars": "Other current assets",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 34,
            },
            {
                "particulars": " ",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 35,
            },
            {
                "particulars": "Total",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 36,
            },
        ]
    if type == "Profit Loss":
        report_details = [
            {
                "particulars": "Income",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 1,
            },
            {
                "particulars": "Revenue from operations",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 2,
            },
            {
                "particulars": "Other income",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "particulars": "Total revenue (I)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 4,
            },
            {
                "particulars": "",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: "",
                "quarter_ended_sep_30,_" + start_year: "",
                "period_ended_sep_30,_" + end_year: "",
                "period_ended_sep_30,_" + start_year: "",
                "year_ended_mar_31,_" + end_year: "",
                "indent": 0,
                "id": 5,
            },
            {
                "particulars": "Expenses",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 6,
            },
            {
                "particulars": "Employee benefit expense",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 7,
            },
            {
                "particulars": "Other expenses",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "particulars": "CSR Contribution",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 9,
            },
            {
                "particulars": "Depreciation and amortisation",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 10,
            },
            {
                "particulars": "Total expenses (II)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 11,
            },
            {
                "particulars": "Profit before  tax  (I-II)",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 12,
            },
            {
                "particulars": "Tax for earlier Years",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 13,
            },
            {
                "particulars": "Current Tax",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "particulars": "Deferred Tax",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "particulars": "Tax expense",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 16,
            },
            {
                "particulars": "Profit/(Loss) after tax for the Year",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 17,
            },
            {
                "particulars": "Earning Per Share",
                "quarter_ended_sep_30,_" + end_year: " ",
                "quarter_ended_june_30,_" + end_year: " ",
                "quarter_ended_sep_30,_" + start_year: " ",
                "period_ended_sep_30,_" + end_year: " ",
                "period_ended_sep_30,_" + start_year: " ",
                "year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 18,
            },
            {
                "particulars": "Basic",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 19,
            },
            {
                "particulars": "Diluted",
                "quarter_ended_sep_30,_" + end_year: 0,
                "quarter_ended_june_30,_" + end_year: 0,
                "quarter_ended_sep_30,_" + start_year: 0,
                "period_ended_sep_30,_" + end_year: 0,
                "period_ended_sep_30,_" + start_year: 0,
                "year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 20,
            },
        ]
    if type == "Cash Flow":
        report_details = [
            {
                "cash_flow_from_operating_activities": "Profit before tax "
                + "from continuing operations",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 1,
            },
            {
                "cash_flow_from_operating_activities": "Adjustment to "
                + "reconcile profit before tax to net cash flows",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 2,
            },
            {
                "cash_flow_from_operating_activities": "Depreciation/ "
                + "amortization on continuing operation",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 3,
            },
            {
                "cash_flow_from_operating_activities": "Provision for "
                + "doubtful debts (net)",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 4,
            },
            {
                "cash_flow_from_operating_activities": "Loss/(Profit) "
                + "on disposal/ write off on property,plant & equipment / "
                + "intangible assets pertaining to   continuing operations ",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 5,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 1,
                "id": 6,
            },
            {
                "cash_flow_from_operating_activities": "Net Gain on sale "
                + "of current investments",
                "for_the_quarter_ended_sep,_" + end_year: 2445,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 7,
            },
            {
                "cash_flow_from_operating_activities": "Interest  Income",
                "for_the_quarter_ended_sep,_" + end_year: 3334,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 8,
            },
            {
                "cash_flow_from_operating_activities": "Operating profit "
                + "before working capital changes",
                "for_the_quarter_ended_sep,_" + end_year: 60558,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 9,
            },
            {
                "cash_flow_from_operating_activities": "Movements in working capital :",  # noqa: 501
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 10,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ "
                + "(decrease) in trade payables",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 11,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ "
                + "(decrease) in long-term provisions",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 12,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ "
                + "(decrease) in short-term provisions",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 13,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ "
                + "(decrease) in other current liabilities",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 14,
            },
            {
                "cash_flow_from_operating_activities": "Increase/ "
                + "(decrease) in other long-term liabilities",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 15,
            },
            {
                "cash_flow_from_operating_activities": "Decrease / "
                + "(increase) in trade receivables",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 16,
            },
            {
                "cash_flow_from_operating_activities": "Decrease / "
                + "(increase) in Loans & Advances",
                "for_the_quarter_ended_sep,_" + end_year: -44442,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 17,
            },
            {
                "cash_flow_from_operating_activities": "Decrease / "
                + "(increase) in other current assets",
                "for_the_quarter_ended_sep,_" + end_year: -218891,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 18,
            },
            {
                "cash_flow_from_operating_activities": "Cash generated "
                + "from operations",
                "for_the_quarter_ended_sep,_" + end_year: -202775,
                "for_the_year_ended_mar_31,_" + end_year: 2475467040,
                "indent": 0,
                "id": 19,
            },
            {
                "cash_flow_from_operating_activities": "Direct taxes "
                + "paid (net of refunds)",
                "for_the_quarter_ended_sep,_" + end_year: 1387920,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 20,
            },
            {
                "cash_flow_from_operating_activities": "Net "
                + "cash_flow_from_operating_activities (A)",
                "for_the_quarter_ended_sep,_" + end_year: 1185145,
                "for_the_year_ended_mar_31,_" + end_year: 2784900420,
                "indent": 0,
                "id": 21,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 22,
            },
            {
                "cash_flow_from_operating_activities": "Cash flows from "
                + "investing activities",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 23,
            },
            {
                "cash_flow_from_operating_activities": "Purchase of "
                + "fixed assets, including CWIP and capital advances",
                "for_the_quarter_ended_sep,_" + end_year: -230322,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 24,
            },
            {
                "cash_flow_from_operating_activities": "Proceeds from "
                + "Sale of Fixed Assets",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 25,
            },
            {
                "cash_flow_from_operating_activities": "Investments in "
                + "bank deposits (having original maturity of "
                + "more than twelve months)",
                "for_the_quarter_ended_sep,_" + end_year: -977,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 26,
            },
            {
                "cash_flow_from_operating_activities": "Redemption/ "
                + "maturity of bank deposits (having original maturity of "
                + "more than twelve months)",
                "for_the_quarter_ended_sep,_" + end_year: -345552,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 27,
            },
            {
                "cash_flow_from_operating_activities": "Purchase of "
                + "current investments",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 28,
            },
            {
                "cash_flow_from_operating_activities": "Proceeds from "
                + "sale/maturity of current investments",
                "for_the_quarter_ended_sep,_" + end_year: -45553,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 29,
            },
            {
                "cash_flow_from_operating_activities": "Interest  income",
                "for_the_quarter_ended_sep,_" + end_year: 222225,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 30,
            },
            {
                "cash_flow_from_operating_activities": "Net cash flow "
                + "from/ (used in) investing activities (B)",
                "for_the_quarter_ended_sep,_" + end_year: -400179,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 31,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 32,
            },
            {
                "cash_flow_from_operating_activities": "Cash flows from "
                + "financing activities",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 33,
            },
            {
                "cash_flow_from_operating_activities": "Dividend paid "
                + "on equity shares",
                "for_the_quarter_ended_sep,_" + end_year: 223,
                "for_the_year_ended_mar_31,_" + end_year: 45452,
                "indent": 1,
                "id": 34,
            },
            {
                "cash_flow_from_operating_activities": "Tax on equity "
                + "dividend paid",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 35,
            },
            {
                "cash_flow_from_operating_activities": "Net cash flow "
                + "from/ (used in) in financing activities (C)",
                "for_the_quarter_ended_sep,_" + end_year: 223,
                "for_the_year_ended_mar_31,_" + end_year: 45452,
                "indent": 0,
                "id": 36,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 37,
            },
            {
                "cash_flow_from_operating_activities": "Net increase/"
                + "(decrease) in cash and cash equivalents (A + B + C)",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 38,
            },
            {
                "cash_flow_from_operating_activities": "Cash and cash "
                + "equivalents at the beginning of the year",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 39,
            },
            {
                "cash_flow_from_operating_activities": "Cash and cash "
                + "equivalents at the end of the year",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 40,
            },
            {
                "cash_flow_from_operating_activities": " ",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 41,
            },
            {
                "cash_flow_from_operating_activities": "Components of cash "
                + "and cash equivalents",
                "for_the_quarter_ended_sep,_" + end_year: " ",
                "for_the_year_ended_mar_31,_" + end_year: " ",
                "indent": 0,
                "id": 42,
            },
            {
                "cash_flow_from_operating_activities": "With banks- on "
                + "current account incl. Cash in Hand",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 1,
                "id": 43,
            },
            {
                "cash_flow_from_operating_activities": "Total cash and "
                + "cash equivalents (note 15)",
                "for_the_quarter_ended_sep,_" + end_year: 0,
                "for_the_year_ended_mar_31,_" + end_year: 0,
                "indent": 0,
                "id": 44,
            },
        ]
    report_details = json.dumps(report_details)
    return report_details


@frappe.whitelist()
def reports_events(
    row_data, fiscal_year, company, report, canopi_report_name
):  # noqa: 501
    filter_year = frappe.get_doc("Fiscal Year", fiscal_year)
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    row_data = json.loads(row_data)
    canopi_report = frappe.get_doc("Canopi Reports", canopi_report_name)
    report_details = json.loads(canopi_report.report_details)
    if report == "Canopi Balance Sheet" or report == "Canopi Profit and Loss":
        for report in report_details:
            if report.get("id") == row_data.get("id"):
                report["quarter_ended_sep_30,_" + end_year] = row_data[
                    "quarter_ended_sep_30,_" + end_year
                ]
                report["quarter_ended_june_30,_" + end_year] = row_data[
                    "quarter_ended_june_30,_" + end_year
                ]
                report["quarter_ended_sep_30,_" + start_year] = row_data[
                    "quarter_ended_sep_30,_" + start_year
                ]
                report["period_ended_sep_30,_" + end_year] = row_data[
                    "period_ended_sep_30,_" + end_year
                ]
                report["period_ended_sep_30,_" + start_year] = row_data[
                    "period_ended_sep_30,_" + start_year
                ]
                report["year_ended_mar_31,_" + end_year] = row_data[
                    "year_ended_mar_31,_" + end_year
                ]
                break
    if report == "Canopi Cash Flow":
        for report in report_details:
            if report.get("id") == row_data.get("id"):
                report["for_the_quarter_ended_sep,_" + end_year] = row_data[
                    "for_the_quarter_ended_sep,_" + end_year
                ]
                report["for_the_year_ended_mar_31,_" + end_year] = row_data[
                    "for_the_year_ended_mar_31,_" + end_year
                ]
                break

    canopi_report.report_details = json.dumps(report_details)
    canopi_report.save()
    return canopi_report
