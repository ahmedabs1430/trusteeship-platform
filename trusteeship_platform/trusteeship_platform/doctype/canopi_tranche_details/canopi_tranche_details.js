// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe */
/* eslint-env jquery */

frappe.ui.form.on('Canopi Tranche Details', {
  refresh: function (frm) {
    customResetGrid(frm);
    addTrancheSlOptions(frm);
    setFilters(frm);
  },

  no_of_tranches: function (frm) {
    addTrancheSlOptions(frm);
  },

  transaction_details_fa: function (frm) {
    setFilters(frm);
  },
});

frappe.ui.form.on('Canopi Tranche SL Details', {
  tranche_details_add: function (frm, cdt, cdn) {
    addTrancheSlOptions(frm);
  },
});

function addTrancheSlOptions(frm) {
  const options = [];
  const isCollapsed = frm.fields_dict.tranche_details_section.is_collapsed();
  if (frm.doc.no_of_tranches) {
    for (let i = 1; i <= frm.doc.no_of_tranches; i++) {
      options.push(i < 10 ? `TRAN-0${i}` : `TRAN-${i}`);
    }
  }

  // add options into edit row dialog form
  frm.fields_dict.tranche_details.grid.update_docfield_property(
    'tranche_sl',
    'options',
    options,
  );
  frm.refresh_fields();
  frm.fields_dict.tranche_details_section.collapse(isCollapsed);

  // add options into table ui
  if (frm.doc?.tranche_details?.length > 0) {
    const selectElem = getChildTableSelectElement();
    selectElem.empty();
    for (let i = 0; i < selectElem.length; i++) {
      options.forEach(num => {
        const optionElem = document.createElement('option');
        optionElem.value = num;
        optionElem.innerText = num;
        selectElem[i].append(optionElem);
      });
    }
  }
}

function getChildTableSelectElement() {
  $("div[data-fieldname='tranche_sl'][data-fieldtype='Select']").click();
  return $("select[data-fieldname='tranche_sl'][data-fieldtype='Select']");
}

async function setFilters(frm) {
  const transactionDetailsFa = await getDoc(
    'Canopi Transaction Details-FA',
    frm.doc.transaction_details_fa ? frm.doc.transaction_details_fa : '',
  );

  const transactionFaFacility = transactionDetailsFa?.facility_details?.map(
    x => x.facility_id,
  );

  const transactionFaCustomers = transactionDetailsFa?.borrower_details?.map(
    x => x.borrower_code,
  );

  const transactionFaLenders = transactionDetailsFa?.lender_details?.map(
    x => x.lender_code,
  );

  // form field filters
  frm.set_query('facility_id', function () {
    return {
      filters: [['Canopi FA Details', 'name', 'in', transactionFaFacility]],
    };
  });

  // child table filters
  frm.fields_dict.tranche_details.grid.get_field('borrower_name').get_query =
    function (doc, cdt, cdn) {
      return {
        filters: [['Customer', 'name', 'in', transactionFaCustomers || []]],
      };
    };

  frm.fields_dict.tranche_details.grid.get_field('lender_name').get_query =
    function (doc, cdt, cdn) {
      return {
        filters: [['Canopi Lender', 'name', 'in', transactionFaLenders || []]],
      };
    };

  // apply filters on table ui
  frm.fields_dict.tranche_details.grid.reset_grid();
}

function customResetGrid(frm) {
  frm.fields_dict.tranche_details.grid.reset_grid = () => {
    frm.fields_dict.tranche_details.grid.visible_columns = [];
    frm.fields_dict.tranche_details.grid.grid_rows = [];
    $(frm.fields_dict.tranche_details.grid.parent)
      .find('.grid-body .grid-row')
      .remove();
    frm.fields_dict.tranche_details.grid.refresh();

    // add options whenever grid resets
    addTrancheSlOptions(frm);
  };
}

async function getDoc(doctype, docname) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: { doc_type: doctype, doc_name: docname },
    freeze: true,
  });

  return response.message;
}
