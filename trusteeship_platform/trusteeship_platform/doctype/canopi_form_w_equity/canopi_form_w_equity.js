// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs */
frappe.ui.form.on('Canopi Form W Equity', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Form W');
    });
  },
});
