// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe */
frappe.ui.form.on('Canopi Type of Security', {
  // refresh: function(frm) {

  // }

  security_type: frm => {
    if (frm.doc.security_type) {
      frm.doc.security_type_name = frm.doc.security_type;
    }
  },
});
