/* global frappe, updateBreadcrumbs */
frappe.listview_settings['Canopi ISIN Request'] = {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Form W');
    });
  },
};
