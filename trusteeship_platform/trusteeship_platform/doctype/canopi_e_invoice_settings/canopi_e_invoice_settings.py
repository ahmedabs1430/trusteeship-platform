# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document
from india_compliance.gst_india.constants.custom_fields import E_INVOICE_FIELDS
from india_compliance.gst_india.utils.custom_fields import toggle_custom_fields


class CanopiEInvoiceSettings(Document):
    def on_update(self):
        self.update_custom_fields()

    def update_custom_fields(self):
        if self.has_value_changed("enable"):
            toggle_custom_fields(E_INVOICE_FIELDS, self.enable)
