// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs */
frappe.ui.form.on('Transaction Details Immovable Asset', {
  refresh: frm => {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
    getAssetSubType(frm);
    setFilters(frm);
  },
  onload: frm => {
    const prevRoute = frappe.get_prev_route();
    if (frm.doc.__islocal && prevRoute[1] === 'Transaction Details') {
      setClCode(frm, prevRoute[2]);
    }
  },
  asset_type: frm => {
    getAssetSubType(frm);
  },
  cnp_customer_code: frm => {
    populate_form(frm);
  },
});

function setClCode(frm, tr_id) {
  if (tr_id) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Transaction Details',
        doc_name: tr_id,
      },
      callback: r => {
        frm.set_value('cnp_customer_code', r.message.cnp_customer_code);
        frm.set_value('operations', r.message.operations);
      },
    });
  }
}

function setFilters(frm) {
  frm.set_query('operations', filter => {
    return {
      filters: [
        [
          'ATSL Mandate List',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });
}

function getAssetSubType(frm) {
  if (frm.doc.asset_type) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.asset_type,
        doc_type: 'Canopi Asset Type',
      },
      freeze: true,
      callback: r => {
        const asset_sub_type = r.message.asset_sub_type;
        const options = asset_sub_type.map(x => x.asset_sub_type);
        frm.set_df_property('asset_sub_type', 'options', options);
      },
    });
  }
}

function populate_form(frm) {
  if (frm.doc.cnp_customer_code) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Customer',
        doc_name: frm.doc.cnp_customer_code,
      },
      callback: r => {
        frm.set_value('cnp_customer_name', r.message.customer_name);
      },
    });
  }
}
