# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document


class CanopiNotificationInterval(Document):
    def validate(self):
        for i in range(len(self.canopi_annexture_settings)):
            for aal in range(i + 1, len(self.canopi_annexture_settings)):
                if (
                    self.canopi_annexture_settings[i].document_code
                    == self.canopi_annexture_settings[aal].document_code
                ):
                    frappe.throw(
                        "Document already exists in Row " + str(i + 1)
                    )  # noqa: 501
