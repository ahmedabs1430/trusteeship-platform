// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, locals */
frappe.ui.form.on('Canopi Notification Interval', {
  refresh: function (frm) {
    frm.fields_dict.canopi_annexture_settings.grid.update_docfield_property(
      'document_code',
      'unique',
      false,
    );
  },
});
frappe.ui.form.on('Canopi Post Execution Checklist Description', {
  form_render: function (frm, cdt, cdn) {
    frm.fields_dict.canopi_annexture_settings.grid.update_docfield_property(
      'pricing_policy_section',
      'hidden',
      true,
    );
    frm.fields_dict.canopi_annexture_settings.grid.update_docfield_property(
      'type_of_security_section',
      'hidden',
      true,
    );
    frm.fields_dict.canopi_annexture_settings.grid.update_docfield_property(
      'section_break_15',
      'hidden',
      true,
    );
    frm.fields_dict.canopi_annexture_settings.grid.update_docfield_property(
      'meta_data_fields_section',
      'hidden',
      true,
    );
  },
  document_code: (frm, cdt, cdn) => {
    const row = locals[cdt][cdn];
    if (row.document_code) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: { doc_name: row.document_code, doc_type: 'Canopi Document Code' },
        freeze: true,
        callback: r => {
          row.description = r.message.description;
          frm.refresh_field('cnp_post_execution_checklist');
        },
      });
    }
  },
});
