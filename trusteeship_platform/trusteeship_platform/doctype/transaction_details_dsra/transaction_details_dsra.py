# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import cint


class TransactionDetailsDSRA(Document):
    def autoname(self):
        set_name(self)


def set_name(self):
    series_number = frappe.db.sql(
        """SELECT MAX(series_number) FROM `tabTransaction Details DSRA`
                    WHERE operations = %s""",
        self.operations,
    )
    series_number = cint(series_number[0][0])
    series_number = series_number + 1
    self.series_number = series_number
    self.name = (
        self.operations + "DSRA" + str(series_number).rjust(2, "0")
    )  # noqa: E501
