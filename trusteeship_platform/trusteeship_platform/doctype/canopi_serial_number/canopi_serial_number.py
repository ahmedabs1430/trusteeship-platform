# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

# import frappe
import frappe
from frappe.model.document import Document


class CanopiSerialNumber(Document):
    def before_insert(self):
        self.fiscal_end_date = frappe.defaults.get_defaults().get(
            "year_end_date"
        )  # noqa: 501
