// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe */
frappe.ui.form.on('Canopi Security Documents Post Execution', {
  refresh: function (frm) {
    frm.disable_form();
    frm.disable_save();
  },
});
