# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import json
import os
from datetime import datetime

import frappe
from frappe.desk.form.linked_with import get_linked_docs
from frappe.model.document import Document
from frappe.utils import cstr
from frappe.utils.data import get_url

from trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval import (  # noqa: 501 isort:skip
    get_approval_count_cond,
)

from trusteeship_platform.custom_methods import (  # isort:skip
    generate_qrcode,
    copy_s3_object,
    delete_from_s3,
    update_tl_ops,
    upload_file_to_s3,
    validate_s3_settings,
)


class CanopiPreExecutionChecklist(Document):
    def on_submit(self):
        validate_submit(self)

    def after_insert(self):
        copy_standard_template(self)
        generate_serial_number(self)
        self.save()

    def validate(self):
        validate_file_attachments(self)
        get_qrcode(self)
        calculate_completion_percent(self)
        doc_type_list = [
            "Canopi Post Execution Checklist",
            "Canopi Documentation",
        ]
        update_tl_ops(self, doc_type_list)


def validate_submit(self):
    if self.product_name.upper() == "DTE" and (not self.annexure_a_s3_key):
        frappe.throw(
            "Please attach a signed copy of Annexure A before submitting this document."  # noqa: 501
        )


def validate_file_attachments(self):
    details = json.loads(self.type_of_security_details)
    for asset in details:
        for index, asset_details in enumerate(details[asset]):
            if asset_details.get("file"):
                frappe.throw(
                    "Cannot save document with some attached file. "
                    + "Either remove or upload them."
                )


def copy_standard_template(self):
    details = json.loads(self.type_of_security_details)
    for asset in details:
        for index, asset_details in enumerate(details[asset]):
            if asset_details.get("standard_template_s3_key"):
                source_key = asset_details["standard_template_s3_key"]
                destination_key = (
                    cstr(frappe.local.site)
                    + "/Pre Execution Checklist/"
                    + self.name
                    + "/"
                    + asset.replace("_", " ").title()
                    + "/"
                    + asset_details["document_code"]
                    + "/Standard Template/ "
                    + os.path.basename(source_key)
                )
                copy_s3_object(source_key, destination_key)
                asset_details["standard_template_s3_key"] = destination_key
    self.type_of_security_details = json.dumps(details)


def get_qrcode(self):
    if self.ops_servicing_rm and self.tl_representative:
        string_to_qrcode = f"CL Code: {self.operations} \nDocument ID: {self.name} \nMaker ID: {self.ops_servicing_rm} \nChecker ID: {self.tl_representative}"  # noqa: 501
        self.qr_code = generate_qrcode(string_to_qrcode)


def generate_serial_number(self):
    if self.product_name.upper() == "DTE":
        if not self.serial_no_link:
            frappe.throw("Serial Number is mandatory field.")
        else:
            doc = frappe.get_doc("Canopi Serial Number", self.serial_no_link)

            current_fiscal_end_date = frappe.defaults.get_defaults().get(
                "year_end_date"
            )
            current_fiscal_end_date = datetime.strptime(
                current_fiscal_end_date, "%Y-%m-%d"
            ).date()

            current_fiscal_start_date = frappe.defaults.get_defaults().get(
                "year_start_date"
            )
            current_fiscal_start_date = datetime.strptime(
                current_fiscal_start_date, "%Y-%m-%d"
            ).date()

            # reset serial number if fiscal year changes
            if current_fiscal_end_date > doc.fiscal_end_date:
                doc.serial_no = "000000"
                doc.fiscal_end_date = current_fiscal_end_date

            serial_no = int(doc.serial_no) + 1
            doc.serial_no = str(serial_no).rjust(6, "0")
            self.serial_no = f'{self.letter_from}/CO/{self.operations}/{str(current_fiscal_start_date.year)[2:]}-{str(current_fiscal_end_date.year)[2:]}/{str(serial_no).rjust(6,"0")}'  # noqa: 501
            doc.save()


def calculate_completion_percent(self):
    rows = {}
    if (
        self.product_name.upper() == "DTE"
        or self.product_name.upper() == "STE"  # noqa: 501
    ):
        rows = json.loads(self.type_of_security_details)
    elif (
        self.product_name.upper() == "EA"
        or self.product_name.upper() == "FA"
        or self.product_name.upper() == "INVIT"
        or self.product_name.upper() == "REIT"
    ):
        rows = json.loads(self.pre_execution_details)

    rows = list(rows.values())
    total_rows = [
        item
        for sublist in rows
        for item in sublist
        if item["track_exception"] == "No"
        if item["retain_waive"] == "Retain"
    ]
    if len(total_rows) != 0:
        completed_rows = [item for item in total_rows if item.get("s3_key")]

        percent = (len(completed_rows) / len(total_rows)) * 100
        self.completion_percent = round(percent, 2)


@frappe.whitelist()
def validate_annexure_a_approval(docname):
    doc = frappe.get_doc("Canopi Pre Execution Checklist", docname)
    quotation_error = ""
    documentation_error = ""
    pre_exec_error = ""
    sales_invoice_error = ""

    # validate quotation
    if not doc.quotation:
        quotation_error += """
        <li>
        Must be linked to the Operations Master.
        </li>
        """

        quotation_error += """
        <li>
        Should be accepted by the client.
        </li>
        """

    elif (
        frappe.db.get_value("Quotation", doc.quotation, "workflow_state")
        != "Accepted By Client"
    ):
        quotation_error += """
        <li>
        Should be accepted by the client.
        </li>
        """

    # validate Sales Invoice
    sales_invoice_list = frappe.get_all(
        "Sales Invoice",
        filters=[
            ["cnp_from_quotation", "=", doc.quotation],
            ["docstatus", "!=", 2],
        ],
    )
    if not sales_invoice_list:
        sales_invoice_error += """
        <li>
        Must be Linked to Quotation and must not be in a cancelled state.
        </li>
        """

    # validate documentation
    linkinfo = {
        "Canopi Documentation": {
            "fieldname": ["operations"],
        },
    }

    operations_master_linked_docs = get_linked_docs(
        "ATSL Mandate List", doc.operations, linkinfo
    )

    if not operations_master_linked_docs.get("Canopi Documentation"):
        documentation_error += """
        <li>
        Must be linked to the Operations Master.
        </li>
        <li>
        DTA is mandatory in Transaction Documents
        </li>
        <li>
        DTA in Transaction Documents must be uploaded by the Ops Maker
        </li>
        """
    else:
        documentation = next(
            iter(operations_master_linked_docs["Canopi Documentation"])
        )["name"]

        filters = [
            ["canopi_documentation_name", "=", documentation],
            ["is_dta", "=", 1],
        ]

        transaction_doc_post_exec = frappe.get_all(
            "Canopi Transaction Documents Post Execution",
            filters=filters,
            fields=["name", "file_path", "status"],
        )

        if not len(transaction_doc_post_exec) > 0:
            documentation_error += """
            <li>
            DTA is mandatory in Transaction Documents
            </li>
            """

            documentation_error += """<li>DTA in Transaction Documents must be uploaded by the Ops Maker and Approved</li>"""  # noqa: 501
        elif (next(iter(transaction_doc_post_exec))["file_path"] is None) or (
            "Approved" not in next(iter(transaction_doc_post_exec))["status"]
        ):
            documentation_error += """
            <li>DTA in Transaction Documents must be uploaded by the Ops Maker and Approved</li>"""  # noqa: 501

    # validate pre execution uploads
    if doc.completion_percent != 100:
        pre_exec_error += """
        <li>
        All required files must be uploaded.
        </li>
        """

    mandatory_error = ""
    if quotation_error:
        mandatory_error += f"""
        <li>
        Quotation:
        <ul style='list-style-type:disc;'>
        {quotation_error}
        </ul>
        </li>
        """

    if sales_invoice_error:
        mandatory_error += f"""
        <li>
        Sales Invoice:
        <ul style='list-style-type:disc;'>
        {sales_invoice_error}
        </ul>
        </li>
        """

    if documentation_error:
        mandatory_error += f"""
        <li>
        Documentation:
        <ul style='list-style-type:disc;'>
        {documentation_error}
        </ul>
        </li>
        """

    if pre_exec_error:
        mandatory_error += f"""
        <li>
        Pre-execution Checklist (Due-Diligence):
        <ul style='list-style-type:disc;'>
        {pre_exec_error}
        </ul>
        </li>"""

    if mandatory_error:
        frappe.throw(
            (
                "<p>To proceed further, the following requirements must be met:</p><ol>"  # noqa: 501
                + mandatory_error
                + "</ol>"
            ).format(),
            title="Insufficient Requisites",
        )


@frappe.whitelist(allow_guest=True)
def email_action(docname, action, redirect=True):
    if (
        frappe.db.get_value(
            "Canopi Pre Execution Checklist", docname, "annexure_a_status"
        )
        != "Pending For Approval"
    ):
        frappe.throw(
            "The requested document has already been approved/rejected."
        )  # noqa: 501
    frappe.db.set_value(
        "Canopi Pre Execution Checklist",
        docname,
        "annexure_a_status",
        action,  # noqa: 501
    )
    frappe.db.commit()
    if redirect is True:
        url = get_url() + "/app/canopi-pre-execution-checklist/" + docname
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = url


@frappe.whitelist()
def update_annexure_a_draft_status(docname):
    pre_execution = frappe.get_doc("Canopi Pre Execution Checklist", docname)

    if (
        pre_execution.product_name.upper() == "DTE"
        and pre_execution.docstatus == 0  # noqa: 501
    ):
        pre_execution.is_annexure_draft = 1

        if is_annexure_a_approval_completed(docname):
            pre_execution.is_annexure_draft = 0
        pre_execution.save()


@frappe.whitelist()
def is_annexure_a_approval_completed(docname):
    approval_list = frappe.get_all(
        "Canopi Approval",
        filters=[
            ["is_annexure_a", "=", 1],
            ["pre_execution", "=", docname],
        ],  # noqa: 501
    )
    approval = next((x.name for x in approval_list if x.name), None)
    if approval:
        approval_doc = frappe.get_doc("Canopi Approval", approval)
        return approval_doc.approval_count == get_approval_count_cond(
            approval_doc
        )  # noqa: 501


@frappe.whitelist()
def upload_annexure_a_to_s3():
    validate_s3_settings()
    file = frappe.request.files
    docname = frappe.request.form.get("docname")
    key = frappe.request.form.get("key")
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    key = f"""{cstr(frappe.local.site)}/{key}"""

    doc = frappe.get_doc("Canopi Pre Execution Checklist", docname)
    if doc.annexure_a_s3_key:
        delete_from_s3(key)
        doc.annexure_a_s3_key = ""

    file_data = file["file"].read()
    response = upload_file_to_s3(settings_doc, key, file_data)
    if response["status_code"] == 200:
        doc.annexure_a_s3_key = key
        doc.save()


@frappe.whitelist()
def delete_annexure_a_from_s3(docname, key):
    delete_from_s3(key)
    doc = frappe.get_doc("Canopi Pre Execution Checklist", docname)
    doc.annexure_a_s3_key = ""
    doc.save()
