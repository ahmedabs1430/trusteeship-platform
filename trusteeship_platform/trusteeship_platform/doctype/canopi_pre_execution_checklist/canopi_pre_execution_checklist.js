// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs, showActivities, showNotes, open_url_post, cur_frm, cur_dialog, in_list, user */
/* eslint-env jquery */
const files_upload = {};
frappe.ui.form.on('Canopi Pre Execution Checklist', {
  refresh: frm => {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and servicing');
    });

    if (frm.doc.__islocal === 1) {
      frm.set_value('ops_servicing_rm', user);
    } else {
      getFileUpload(frm);
    }
    showActivitiesAndnotes(frm);
    setOperationsFilter(frm);
    setTlRepresentativeFilter(frm);
    setSerialNoFilter(frm);
    frm.trigger('ops_servicing_rm');
    frm.trigger('tl_representative');
    if (frm.doc.__islocal === 1) {
      frm.trigger('operations');
    } else {
      loadAssetTables(frm);
    }
    addPercentPill(frm);
    headerButtons(frm);

    // Annexure A
    if (frm.doc.product_name.toUpperCase() === 'DTE') {
      configure_annexure_a_approval_btn(frm);
      configure_view_attach_annexure_a_btn(frm);
      configure_attach_annexure_a_btn(frm);
      approval_button_in_connections(frm);
      renderDownloadAnnexureABtn(frm);
    }
  },

  authorised_signatory: frm => {
    set_full_name(frm);
  },

  view_annexure_a: frm => {
    get_annexure_a(frm);
  },

  annexure_a_approval: frm => {
    if (
      frm.doc.product_name.toUpperCase() === 'DTE' &&
      (frm.doc.annexure_a_status === 'Draft' ||
        frm.doc.annexure_a_status === 'Rejected')
    ) {
      send_annexure_a_approval(frm);
    }
  },

  attach_annexure_a: frm => {
    upload_annexure_a_to_s3(frm);
  },

  after_save: frm => {
    emptyHtmlFields(frm);
  },

  validate: frm => {
    validateCheckerAndMaker(frm);
  },

  onload: frm => {
    setMandateNumberReadOnly(frm);
    setRegisteredAddressFilter(frm);
    setCorporateAddressFilter(frm);
  },

  cnp_registered_address: frm => {
    setCorporateAddressFilter(frm);
  },

  operations: async frm => {
    if (!frm.doc.operations) {
      frm.fields_dict.immovable_asset_section.df.hidden = 1;
      frm.fields_dict.movable_asset_section.df.hidden = 1;
      frm.fields_dict.pledge_section.df.hidden = 1;
      frm.fields_dict.personal_guarantee_section.df.hidden = 1;
      frm.fields_dict.corporate_guarantee_section.df.hidden = 1;
      frm.fields_dict.ndu_section.df.hidden = 1;
      frm.fields_dict.intangible_asset_section.df.hidden = 1;
      frm.fields_dict.motor_vehicle_section.df.hidden = 1;
      frm.fields_dict.financial_asset_section.df.hidden = 1;
      frm.fields_dict.assignment_of_rights_section.df.hidden = 1;
      frm.fields_dict.current_asset_section.df.hidden = 1;
      frm.fields_dict.debt_service_reserve_account_section.df.hidden = 1;
      frm.fields_dict.others_section.df.hidden = 1;
      $('#canopi-pre-execution-checklist-annexure_a_tab-tab').hide();

      frm.refresh_fields();
    }
    emptyHtmlFields(frm);

    frm.doc.type_of_security_details = JSON.stringify({});

    if (frm.doc.operations) {
      await populateForm(frm);
    }
  },

  face_value: frm => {
    frm.trigger('letter_sub');
  },

  nominal_value: frm => {
    frm.trigger('letter_sub');
  },

  letter_from: frm => {
    frm.trigger('letter_sub');
    appendLetterFromInSerialNo(frm);
  },

  letter_sub: async frm => {
    const response = await getMultipleDocs(frm);
    setLetterSub(
      frm,
      response.message.item,
      response.message.canopi_pricing_policy,
      response.message.opportunity,
    );
  },

  ops_servicing_rm: frm => {
    validateCheckerAndMaker(frm);
  },

  tl_representative: frm => {
    validateCheckerAndMaker(frm);
  },
});

function set_full_name(frm) {
  if (frm.doc.authorised_signatory) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.authorised_signatory,
        doc_type: 'User',
      },
      freeze: true,
      callback: r => {
        frm.set_value('signatory_full_name', r.message.full_name);
      },
    });
  }
}

async function get_annexure_a(frm) {
  if (!frm.doc.__islocal && frm.doc.product_name.toUpperCase() === 'DTE') {
    if (frm.is_dirty()) {
      await frm.save();
    }
    await frappe.call({
      method:
        // eslint-disable-next-line max-len
        'trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.update_annexure_a_draft_status',
      args: { docname: frm.doc.name },
      freeze: true,
    });
    frm.reload_doc();
    window.open(
      `/api/method/trusteeship_platform.custom_methods.download_pre_execution_annexure_a?docname=${frm.doc.name}`,
      '_blank',
    );
  }
}

function headerButtons(frm) {
  if (frm.doc.operations) {
    frm.add_custom_button('Main', () => {
      window.open(`/app/atsl-mandate-list/${frm.doc.operations}`);
    });
  }
  if (!frm.doc.__islocal && frm.doc.operations) {
    let transactionDetails;
    if (frm.doc.product_name.toUpperCase() === 'FA') {
      transactionDetails = 'Canopi Transaction Details-FA';
    } else {
      transactionDetails = 'Transaction Details';
    }
    frm.add_custom_button(
      transactionDetails,
      () => {
        const linkedDoctypes = {
          [transactionDetails]: {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, transactionDetails, r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Pre-Execution Checklist',
      () => {
        if (!frm.doc.__islocal) {
          frm.scroll_to_field('customer_code');
        }
      },
      'Operations',
    );
    frm.add_custom_button(
      'Documentation',
      () => {
        const linkedDoctypes = {
          'Canopi Documentation': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Documentation', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Post-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Post Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Post Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
  }
}

function showActivitiesAndnotes(frm) {
  frappe.require('assets/trusteeship_platform/js/common.js', () => {
    showActivities(frm);
    showNotes(frm);
  });
}

function addPercentPill(frm) {
  if (document.getElementById('completion-progress')) {
    document.getElementById('completion-progress').remove();
  }

  if (!frm.doc.__islocal) {
    let titleElement = $(`h3[title="${frm.doc.name}"]`).parent();
    titleElement = titleElement[0];
    const pillspan = document.createElement('span');
    pillspan.setAttribute('class', 'indicator-pill whitespace-nowrap');
    pillspan.classList.add(
      frm.doc.completion_percent === 100 ? 'green' : 'red',
    );
    pillspan.setAttribute('id', 'completion-progress');
    pillspan.style.marginLeft = 'var(--margin-sm)';
    const textspan = document.createElement('span');
    textspan.innerHTML = frm.doc.completion_percent + '%';
    pillspan.appendChild(textspan);

    titleElement.appendChild(pillspan);
  }
}

function appendLetterFromInSerialNo(frm) {
  if (
    frm.doc.__islocal !== 1 &&
    frm.doc.letter_from !== frm.doc.serial_no.split('/')[0]
  ) {
    const serialNo = frm.doc.serial_no.split('/');
    serialNo[0] = frm.doc.letter_from;
    frm.set_value('serial_no', serialNo.join('/'));
  }
}

function validateCheckerAndMaker(frm) {
  if (
    frm.doc.tl_representative &&
    frm.doc.ops_servicing_rm &&
    frm.doc.tl_representative === frm.doc.ops_servicing_rm
  ) {
    frm.set_value('ops_servicing_rm', '');
    frm.set_value('tl_representative', '');
    frappe.throw(
      '<b>Ops & Servicing RM</b> and <b>TL Representative</b> cannot be same.',
    );
  }
}

function setMandateNumberReadOnly(frm) {
  if (
    frm.doc.__islocal === 1 &&
    frappe.get_prev_route().some(x => x === 'ATSL Mandate List')
  ) {
    frm.set_df_property('operations', 'read_only', true);
  }
}

function setLetterSub(frm, item, pricingPolicy, opportunity) {
  setDebenturesSub(frm, item, pricingPolicy, opportunity);
  frm.refresh_field('letter_subject');
}

function setDebenturesSub(frm, item, pricingPolicy, opportunity) {
  if (item.item_code.toUpperCase() === 'DTE') {
    frm.doc.letter_subject = `Issue of Debenture Trustee ${
      pricingPolicy.no_of_securities === 1 ? 'secured' : 'unsecured'
    }, ${
      opportunity.cnp_type_of_security
    } debentures, each having a face value of ₹ ${
      frm.doc.face_value ? frm.doc.face_value : 0
    } of the aggregate nominal value of up to ₹ ${
      frm.doc.nominal_value ? frm.doc.nominal_value : 0
    } by ${
      frm.doc.letter_from === 'ATSL'
        ? 'Axis Trustee Services Limited'
        : 'AXIS Bank'
    } in one or more tranches.`;
  }
}

function setDefaultLetterBody(frm, itemName) {
  if (itemName === 'Debenture Trustee')
    /* eslint-disable max-len */
    frm.doc.letter_body = `We, the debenture trustee(s) to the above-mentioned forthcoming issue state as follows:
    <ol>
    <br>
        <li>We have examined documents pertaining to the said issue and other such relevant documents, reports and certifications.</li> <br>

        <li> On the basis of such examination and of the discussions with the Issuer, its directors and other officers, other agencies and on independent verification of the various relevant documents, reports and certifications:</li></ol>

    We confirm that:
    <ol type="a">
    <br>
    <li>The Issuer has made adequate provisions for and/or has taken steps to provide for adequate security for the debt securities to be issued.</li>
    <li>The Issuer has obtained the permissions / consents necessary for creating security on the said property(ies). </li>
    <li>The Issuer has made all the relevant disclosures about the security and its continued obligations towards the holders of debt securities. </li>
    <li>Issuer has adequately disclosed all consents/ permissions required for creation of further charge on assets in offer document or private placement memorandum/ information memorandum and all disclosures made in the offer document or private placement memorandum/ information memorandum with respect to creation of security are in confirmation with the clauses of debenture trustee agreement. </li>
    <li>Issuer has given an undertaking that charge shall be created in favour of debenture trustee as per terms of issue before filing of listing application. </li>
    <li>Issuer has disclosed all covenants proposed to be included in debenture trust deed (including any side letter, accelerated payment clause etc.), offer document or private placement memorandum/ information memorandum and given an undertaking that debenture trust deed would be executed before filing of listing application. </li>
    <li>All disclosures made in the draft offer document or private placement memorandum/ information memorandum with respect to the debt securities are true, fair and adequate to enable the investors to make a well-informed decision as to the investment in the proposed issue.</li>
    </ol>

    We have satisfied ourselves about the ability of the Issuer to service the debt securities.`;
  /* eslint-enable max-len */

  frm.refresh_field('letter_body');
}

function setRegisteredAddressFilter(frm) {
  frm.set_query('cnp_registered_address', function () {
    return {
      filters: [
        ['Address', 'is_your_company_address', '=', 1],
        ['Address', 'address_type', '=', 'Registered Office'],
      ],
    };
  });
}

function setCorporateAddressFilter(frm) {
  if (frm.doc.cnp_registered_address) {
    frm.toggle_reqd('cnp_corporate_address', true);
    frm.toggle_display('cnp_corporate_address', true);
    frm.set_df_property('cnp_corporate_address', 'label', 'Corporate Office');
    frm.set_query('cnp_corporate_address', function () {
      return {
        filters: [
          ['Address', 'is_your_company_address', '=', 1],
          ['Address', 'address_type', '=', 'Corporate Office'],
        ],
      };
    });
  } else {
    frm.toggle_display('cnp_corporate_address', false);
    frm.toggle_reqd('cnp_corporate_address', false);
    frm.doc.cnp_corporate_address = '';
  }
}

function setOperationsFilter(frm) {
  frm.set_query('operations', function () {
    return {
      filters: [['ATSL Mandate List', 'docstatus', '=', 1]],
    };
  });
}

function setTlRepresentativeFilter(frm) {
  frm.set_query('tl_representative', function () {
    return {
      query:
        'trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.user_query',
      filters: { role: 'Ops Checker' },
    };
  });
}

function setSerialNoFilter(frm) {
  frm.set_query('serial_no_link', function () {
    return {
      filters: [
        [
          'Canopi Serial Number',
          'doc_type',
          '=',
          'Canopi Pre Execution Checklist',
        ],
        ['Canopi Serial Number', 'letter_type', '=', 'Annexure A'],
      ],
    };
  });
}

function getMultipleDocs(frm) {
  const docs = [
    {
      doctype: 'ATSL Mandate List',
      docname: frm.doc.operations,
    },
    {
      doctype: 'Item',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'product_name',
    },
    {
      doctype: 'Canopi Pricing Policy',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: ['opportunity', 'product_name'],
    },
    {
      doctype: 'Opportunity',
      parent_doctype: 'ATSL Mandate List',
      child_link_field: 'opportunity',
    },
  ];
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.get_docs_with_linked_childs',
    args: { docs },
    freeze: true,
  });
}

async function populateForm(frm) {
  const response = await getMultipleDocs(frm);

  setLetterSub(
    frm,
    response.message.item,
    response.message.canopi_pricing_policy,
    response.message.opportunity,
  );
  setDefaultLetterBody(frm, response.message.item.item_name);

  frm.set_value('company', response.message.opportunity.customer_name);
  frm.set_value(
    'mandate_name',
    response.message.atsl_mandate_list.mandate_name,
  );
  frm.set_value(
    'customer_code',
    response.message.atsl_mandate_list.cnp_customer_code,
  );
  frm.set_value('product_name', response.message.item.name);
  frm.set_value('quotation', response.message.atsl_mandate_list.quotation);
  frm.set_value('opportunity', response.message.atsl_mandate_list.opportunity);
  frm.set_value(
    'ops_servicing_rm',
    response.message.atsl_mandate_list.ops_servicing_rm,
  );
  frm.set_value(
    'tl_representative',
    response.message.atsl_mandate_list.tl_representative,
  );
  if (frm.doc.__islocal && frm.doc.product_name.toUpperCase() === 'DTE') {
    frappe.db
      .exists(
        'Canopi Serial Number',
        'Canopi Pre Execution Checklist-Annexure A',
      )
      .then(exists => {
        if (exists) {
          frm.set_value(
            'serial_no_link',
            'Canopi Pre Execution Checklist-Annexure A',
          );
        }
      });
    setRegisteredAddress(frm);
  }
  getHtmlFieldValues(frm, response);
}

function setRegisteredAddress(frm) {
  if (frm.doc.quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: frm.doc.quotation, doc_type: 'Quotation' },
      freeze: true,
      callback: r => {
        frm.set_value(
          'cnp_registered_address',
          r.message.cnp_registered_address,
        );
      },
    });
  }
}

function getHtmlFieldValues(frm, response) {
  let securityTypes = [];

  if (
    frm.doc.product_name.toUpperCase() === 'STE' ||
    frm.doc.product_name.toUpperCase() === 'DTE'
  ) {
    securityTypes = response.message.atsl_mandate_list.security_type.map(
      types =>
        types.change_in_security === 'Yes'
          ? types.new_security_type
          : types.existing_security_type,
    );
  }

  const immovableAsset = [];
  const movableAsset = [];
  const pledge = [];
  const personalGuarantee = [];
  const corporateGuarantee = [];
  const ndu = [];
  const intangibleAsset = [];
  const motorVehicle = [];
  const financialAsset = [];
  const assignmentOfRights = [];
  const currentAsset = [];
  const debtServiceReserveAccount = [];
  const commonDocuments = [];

  const others = [];

  const preExecutionDetails = [];

  response.message.item.cnp_common_documents_checklist_table.forEach(
    preExecutionTemplate => {
      commonDocuments.push({
        description: preExecutionTemplate.description,
        standard_template_s3_key: preExecutionTemplate.standard_template_s3_key,
        standard_template_name: preExecutionTemplate.standard_template_s3_key
          ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
          : '',
        document_code: preExecutionTemplate.document_code,
        client_upload_yes_no: 'No',
        atsl_upload_yes_no: 'No',
        public: '',
        comments: '',
        track_exception: 'No',
        retain_waive: 'Retain',
        approval_required: 'No',
        send_email: 'No',
      });
    },
  );
  response.message.item.cnp_pre_execution_checklist.forEach(
    preExecutionTemplate => {
      if (
        checkConditions(
          getTypeOfSecurityCondition(preExecutionTemplate, frm),
          securityTypes,
          getPricingPolicyCondition(preExecutionTemplate),
          response.message.canopi_pricing_policy,
        )
      ) {
        if (
          frm.doc.product_name.toUpperCase() === 'EA' ||
          frm.doc.product_name.toUpperCase() === 'FA' ||
          frm.doc.product_name.toUpperCase() === 'INVIT' ||
          frm.doc.product_name.toUpperCase() === 'REIT'
        ) {
          preExecutionDetails.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Immovable Asset') &&
          preExecutionTemplate.immovable === 1
        ) {
          immovableAsset.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Movable Asset') &&
          preExecutionTemplate.movable === 1
        ) {
          movableAsset.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Pledge') &&
          preExecutionTemplate.pledge === 1
        ) {
          pledge.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Personal Guarantee') &&
          preExecutionTemplate.personal_guarantee === 1
        ) {
          personalGuarantee.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Corporate Guarantee') &&
          preExecutionTemplate.corporate_guarantee === 1
        ) {
          corporateGuarantee.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'NDU') &&
          preExecutionTemplate.ndu === 1
        ) {
          ndu.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Intangible Asset') &&
          preExecutionTemplate.intangible === 1
        ) {
          intangibleAsset.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Motor Vehicle') &&
          preExecutionTemplate.motor === 1
        ) {
          motorVehicle.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Financial Asset') &&
          preExecutionTemplate.financial === 1
        ) {
          financialAsset.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Assignment of Rights') &&
          preExecutionTemplate.assignment_of_rights === 1
        ) {
          assignmentOfRights.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Current Asset') &&
          preExecutionTemplate.current === 1
        ) {
          currentAsset.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(
            x => x === 'Debt Service Reserve Account (DSRA)',
          ) &&
          preExecutionTemplate.dsra === 1
        ) {
          debtServiceReserveAccount.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }

        if (
          securityTypes.some(x => x === 'Others') &&
          preExecutionTemplate.others === 1
        ) {
          others.push({
            description: preExecutionTemplate.description,
            standard_template_s3_key:
              preExecutionTemplate.standard_template_s3_key,
            standard_template_name:
              preExecutionTemplate.standard_template_s3_key
                ? preExecutionTemplate.standard_template_s3_key.split('/').pop()
                : '',
            document_code: preExecutionTemplate.document_code,
            client_upload_yes_no: 'No',
            atsl_upload_yes_no: 'No',
            public: '',
            comments: '',
            track_exception: 'No',
            retain_waive: 'Retain',
            approval_required: 'No',
            send_email: 'No',
          });
        }
      }
    },
  );

  const typeOfSecurities = {
    immovable_asset: immovableAsset,
    movable_asset: movableAsset,
    pledge,
    personal_guarantee: personalGuarantee,
    corporate_guarantee: corporateGuarantee,
    ndu,
    intangible_asset: intangibleAsset,
    motor_vehicle: motorVehicle,
    financial_asset: financialAsset,
    assignment_of_rights: assignmentOfRights,
    current_asset: currentAsset,
    debt_service_reserve_account: debtServiceReserveAccount,
    others,
  };
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    const typeOfSecurityDetails = getFilteredTypeOfSecurity(
      securityTypes,
      typeOfSecurities,
    );
    typeOfSecurityDetails.common_documents = commonDocuments;
    frm.doc.type_of_security_details = JSON.stringify(typeOfSecurityDetails);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    frm.doc.pre_execution_details = JSON.stringify({
      pre_execution_details: preExecutionDetails,
    });
  }

  loadAssetTables(frm);
}

function getFilteredTypeOfSecurity(source, target) {
  let typeOfSecurities = Object.entries(target);
  typeOfSecurities = typeOfSecurities.filter(([key, value]) =>
    source.some(x => titleToSnakeCase(removeParenthesis(x)) === key),
  );
  return Object.fromEntries(typeOfSecurities);
}

function getPricingPolicyCondition(preExecutionChecklist) {
  const conditions = [];
  const pricingPolicyList = {
    no_of_securities: 'no_of_securities',
    listed_yes_no: 'listed_yes_no',
    whether_public_issue: 'whether_public_issue',
  };

  if (preExecutionChecklist.secured) {
    conditions.push(pricingPolicyList.no_of_securities);
  }
  if (preExecutionChecklist.listed) {
    conditions.push(pricingPolicyList.listed_yes_no);
  }
  if (preExecutionChecklist.whether_public_issue) {
    conditions.push(pricingPolicyList.whether_public_issue);
  }

  return conditions;
}

function getTypeOfSecurityCondition(preExecutionChecklist, frm) {
  const conditions = [];

  if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  )
    return conditions;

  const typeOfSecurityList = {
    immovable: 'Immovable Asset',
    movable: 'Movable Asset',
    pledge: 'Pledge',
    personal_guarantee: 'Personal Guarantee',
    corporate_guarantee: 'Corporate Guarantee',
    ndu: 'NDU',
    intangible: 'Intangible Asset',
    motor: 'Motor Vehicle',
    financial: 'Financial Asset',
    assignment_of_rights: 'Assignment of Rights',
    current: 'Current Asset',
    dsra: 'Debt Service Reserve Account (DSRA)',
    others: 'Others',
    common_documents: 'common_documents',
  };

  if (preExecutionChecklist.immovable) {
    conditions.push(typeOfSecurityList.immovable);
  }
  if (preExecutionChecklist.movable) {
    conditions.push(typeOfSecurityList.movable);
  }
  if (preExecutionChecklist.pledge) {
    conditions.push(typeOfSecurityList.pledge);
  }
  if (preExecutionChecklist.personal_guarantee) {
    conditions.push(typeOfSecurityList.personal_guarantee);
  }
  if (preExecutionChecklist.corporate_guarantee) {
    conditions.push(typeOfSecurityList.corporate_guarantee);
  }
  if (preExecutionChecklist.ndu) {
    conditions.push(typeOfSecurityList.ndu);
  }
  if (preExecutionChecklist.intangible) {
    conditions.push(typeOfSecurityList.intangible);
  }
  if (preExecutionChecklist.motor) {
    conditions.push(typeOfSecurityList.motor);
  }
  if (preExecutionChecklist.financial) {
    conditions.push(typeOfSecurityList.financial);
  }
  if (preExecutionChecklist.assignment_of_rights) {
    conditions.push(typeOfSecurityList.assignment_of_rights);
  }
  if (preExecutionChecklist.current) {
    conditions.push(typeOfSecurityList.current);
  }
  if (preExecutionChecklist.dsra) {
    conditions.push(typeOfSecurityList.dsra);
  }
  if (preExecutionChecklist.others) {
    conditions.push(typeOfSecurityList.others);
  }
  if (preExecutionChecklist.common_documents) {
    conditions.push(typeOfSecurityList.common_documents);
  }
  return conditions;
}

function checkConditions(
  typeOfSecurityCondition,
  typeOfSecurity,
  pricingPolicyCondition,
  pricingPolicy,
) {
  let isSecurityTypeValid = typeOfSecurityCondition.length === 0;
  let isPricingPolicyValid = pricingPolicyCondition.length === 0;

  if (typeOfSecurityCondition.length !== 0) {
    isSecurityTypeValid = typeOfSecurityCondition.some(element =>
      typeOfSecurity.includes(element),
    );
  }

  pricingPolicyCondition.forEach(element => {
    if (
      (element === 'no_of_securities' &&
        pricingPolicy.no_of_securities !== 0) ||
      ((element === 'listed_yes_no' || element === 'whether_public_issue') &&
        pricingPolicy[`${element}`] === 'Yes')
    ) {
      isPricingPolicyValid = true;
    } else {
      isPricingPolicyValid = false;
    }
  });

  return isPricingPolicyValid && isSecurityTypeValid;
}

function loadAssetTables(frm, fieldName = undefined) {
  emptyHtmlFields(frm);
  hideUnHideSections(frm);

  setTimeout(() => {
    renderTemplate(frm, fieldName);
    addTemplateButtonEvents(frm);
  }, 100);
}

function hideUnHideSections(frm) {
  frm.fields_dict.immovable_asset_section.df.hidden =
    getAssetValues(frm).immovable_asset !== undefined ? 0 : 1;
  frm.fields_dict.movable_asset_section.df.hidden =
    getAssetValues(frm).movable_asset !== undefined ? 0 : 1;
  frm.fields_dict.pledge_section.df.hidden =
    getAssetValues(frm).pledge !== undefined ? 0 : 1;
  frm.fields_dict.personal_guarantee_section.df.hidden =
    getAssetValues(frm).personal_guarantee !== undefined ? 0 : 1;
  frm.fields_dict.corporate_guarantee_section.df.hidden =
    getAssetValues(frm).corporate_guarantee !== undefined ? 0 : 1;
  frm.fields_dict.ndu_section.df.hidden =
    getAssetValues(frm).ndu !== undefined ? 0 : 1;
  frm.fields_dict.intangible_asset_section.df.hidden =
    getAssetValues(frm).intangible_asset !== undefined ? 0 : 1;

  frm.fields_dict.motor_vehicle_section.df.hidden =
    getAssetValues(frm).motor_vehicle !== undefined ? 0 : 1;
  frm.fields_dict.financial_asset_section.df.hidden =
    getAssetValues(frm).financial_asset !== undefined ? 0 : 1;
  frm.fields_dict.assignment_of_rights_section.df.hidden =
    getAssetValues(frm).assignment_of_rights !== undefined ? 0 : 1;
  frm.fields_dict.current_asset_section.df.hidden =
    getAssetValues(frm).current_asset !== undefined ? 0 : 1;
  frm.fields_dict.debt_service_reserve_account_section.df.hidden =
    getAssetValues(frm).debt_service_reserve_account !== undefined ? 0 : 1;

  frm.fields_dict.others_section.df.hidden =
    getAssetValues(frm).others !== undefined ? 0 : 1;
  if (frm.doc.operations) {
    $('#canopi-pre-execution-checklist-annexure_a_tab-tab').show();
  } else {
    $('#canopi-pre-execution-checklist-annexure_a_tab-tab').hide();
  }
  frm.refresh_fields();
}

function renderTemplate(frm, fieldName = undefined) {
  frm.set_df_property(
    'common_documents_html',
    'options',
    frappe.render_template('pre_execution_checklist_table', {
      doc:
        getAssetValues(frm).common_documents !== undefined
          ? getAssetValues(frm).common_documents
          : [],
      id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-common_documents',
      docstatus: frm.doc.docstatus,
    }),
  );
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    if (getAssetValues(frm).immovable_asset !== undefined) {
      frm.set_df_property(
        'immovable_asset_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).immovable_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-immovable',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).movable_asset !== undefined) {
      frm.set_df_property(
        'movable_asset_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).movable_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-movable',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).pledge !== undefined) {
      frm.set_df_property(
        'pledge_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).pledge,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-pledge',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).personal_guarantee !== undefined) {
      frm.set_df_property(
        'personal_guarantee_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).personal_guarantee,
          id:
            frm.doc.__islocal === 1 ? '' : frm.doc.name + '-personal-guarantee',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).corporate_guarantee !== undefined) {
      frm.set_df_property(
        'corporate_guarantee_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).corporate_guarantee,
          id:
            frm.doc.__islocal === 1
              ? ''
              : frm.doc.name + '-corporate-guarantee',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).ndu !== undefined) {
      frm.set_df_property(
        'ndu_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).ndu,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-ndu',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).intangible_asset !== undefined) {
      frm.set_df_property(
        'intangible_asset_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).intangible_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-intangible',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).motor_vehicle !== undefined) {
      frm.set_df_property(
        'motor_vehicle_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).motor_vehicle,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-motor',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).financial_asset !== undefined) {
      frm.set_df_property(
        'financial_asset_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).financial_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-financial',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).assignment_of_rights !== undefined) {
      frm.set_df_property(
        'assignment_of_rights_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).assignment_of_rights,
          id:
            frm.doc.__islocal === 1
              ? ''
              : frm.doc.name + '-assignment-of-rights',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).current_asset !== undefined) {
      frm.set_df_property(
        'current_asset_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).current_asset,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-current',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).debt_service_reserve_account !== undefined) {
      frm.set_df_property(
        'debt_service_reserve_account_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).debt_service_reserve_account,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-dsra',
          docstatus: frm.doc.docstatus,
        }),
      );
    }

    if (getAssetValues(frm).others !== undefined) {
      frm.set_df_property(
        'others_html',
        'options',
        frappe.render_template('pre_execution_checklist_table', {
          doc: getAssetValues(frm).others,
          id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-others',
          docstatus: frm.doc.docstatus,
        }),
      );
    }
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    frm.set_df_property(
      'pre_execution_details_html',
      'options',
      frappe.render_template('pre_execution_checklist_table', {
        doc: getAssetValues(frm).pre_execution_details
          ? getAssetValues(frm).pre_execution_details
          : [],
        id:
          frm.doc.__islocal === 1
            ? ''
            : frm.doc.name + '-pre-execution-details',
        docstatus: frm.doc.docstatus,
      }),
    );
  }

  frm.refresh_fields();

  if (fieldName) {
    setTimeout(() => {
      scrollToField(frm, fieldName);
    }, 100);
  }
}

function addTemplateButtonEvents(frm) {
  if (frm.doc.__islocal !== 1) {
    addEditEvents(frm);
    addNewRowEvents(frm);
    addRemoveRowEvent(frm);
    addUploadEvents(frm);
    addDownloadEvents(frm);
    addSelectEvents(frm);
    addEmailEvent(frm);
  }
}

function addNewRowEvents(frm) {
  $('.' + frm.doc.name + '-common_documents-add-row').unbind();
  $('.' + frm.doc.name + '-common_documents-add-row').on('click', function () {
    const assetName = 'common_documents';
    rowDialog(frm, {}, assetName);
  });
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    $('.' + frm.doc.name + '-immovable-add-row').unbind();
    $('.' + frm.doc.name + '-immovable-add-row').on('click', function () {
      const assetName = 'immovable_asset';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-movable-add-row').unbind();
    $('.' + frm.doc.name + '-movable-add-row').on('click', function () {
      const assetName = 'movable_asset';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-pledge-add-row').unbind();
    $('.' + frm.doc.name + '-pledge-add-row').on('click', function () {
      const assetName = 'pledge';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-personal-guarantee-add-row').unbind();
    $('.' + frm.doc.name + '-personal-guarantee-add-row').on(
      'click',
      function () {
        const assetName = 'personal_guarantee';
        rowDialog(frm, {}, assetName);
      },
    );

    $('.' + frm.doc.name + '-corporate-guarantee-add-row').unbind();
    $('.' + frm.doc.name + '-corporate-guarantee-add-row').on(
      'click',
      function () {
        const assetName = 'corporate_guarantee';
        rowDialog(frm, {}, assetName);
      },
    );

    $('.' + frm.doc.name + '-ndu-add-row').unbind();
    $('.' + frm.doc.name + '-ndu-add-row').on('click', function () {
      const assetName = 'ndu';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-intangible-add-row').unbind();
    $('.' + frm.doc.name + '-intangible-add-row').on('click', function () {
      const assetName = 'intangible_asset';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-motor-add-row').unbind();
    $('.' + frm.doc.name + '-motor-add-row').on('click', function () {
      const assetName = 'motor_vehicle';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-financial-add-row').unbind();
    $('.' + frm.doc.name + '-financial-add-row').on('click', function () {
      const assetName = 'financial_asset';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-assignment-of-rights-add-row').unbind();
    $('.' + frm.doc.name + '-assignment-of-rights-add-row').on(
      'click',
      function () {
        const assetName = 'assignment_of_rights';
        rowDialog(frm, {}, assetName);
      },
    );

    $('.' + frm.doc.name + '-current-add-row').unbind();
    $('.' + frm.doc.name + '-current-add-row').on('click', function () {
      const assetName = 'current_asset';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-dsra-add-row').unbind();
    $('.' + frm.doc.name + '-dsra-add-row').on('click', function () {
      const assetName = 'debt_service_reserve_account';
      rowDialog(frm, {}, assetName);
    });

    $('.' + frm.doc.name + '-others-add-row').unbind();
    $('.' + frm.doc.name + '-others-add-row').on('click', function () {
      const assetName = 'others';
      rowDialog(frm, {}, assetName);
    });
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    $('.' + frm.doc.name + '-pre-execution-details-add-row').unbind();
    $('.' + frm.doc.name + '-pre-execution-details-add-row').on(
      'click',
      function () {
        const assetName = 'pre_execution_details';
        rowDialog(frm, {}, assetName);
      },
    );
  }
}

function addRemoveRowEvent(frm) {
  addCommonDocumentsRemoveRowEvent(frm);
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    addImmovableRemoveRowEvent(frm);
    addMovableRemoveRowEvent(frm);
    addPledgeRemoveRowEvent(frm);
    addPersonalGuaranteeRemoveRowEvent(frm);
    addCorporateGuaranteeRemoveRowEvent(frm);
    addNduRemoveRowEvent(frm);
    addIntangibleRemoveRowEvent(frm);
    addMotorRemoveRowEvent(frm);
    addFinancialRemoveRowEvent(frm);
    addAssignmentOfRightsRemoveRowEvent(frm);
    addCurrentRemoveRowEvent(frm);
    addDsraRemoveRowEvent(frm);
    addOthersRemoveRowEvent(frm);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    addPreExecutionDetailsTableRemoveRowEvent(frm);
  }
}

function addCommonDocumentsRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-common_documents-remove-row').unbind();
  $('.' + frm.doc.name + '-common_documents-remove-row').on(
    'click',
    function () {
      const removeAssets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(`input[class="${frm.doc.name}-common_documents-select-row"]:checked`)
          .length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-common_documents-select-row`).each(function (i) {
        if (this.checked) {
          removeAssets.push(details.common_documents[i]);
        }
      });

      const keys = [];
      removeAssets.forEach(asset => {
        details.common_documents = details.common_documents.filter(
          el => el.document_code !== asset.document_code,
        );
        if (asset?.s3_key) {
          keys.push(asset.s3_key);
        }
        if (asset?.standard_template_s3_key) {
          keys.push(asset.standard_template_s3_key);
        }
      });

      frm.dirty();
      if (keys.length > 0) {
        deleteMultipleFromS3(keys).then(r => {
          frm.doc.type_of_security_details = JSON.stringify(details);
          frm.save().then(() => {
            loadAssetTables(frm, 'common_documents');
          });
        });
      } else {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'common_documents');
        });
      }
    },
  );
}

function addImmovableRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-immovable-remove-row').unbind();
  $('.' + frm.doc.name + '-immovable-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-immovable-select-row"]:checked`).length <=
      0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-immovable-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.immovable_asset[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.immovable_asset = details.immovable_asset.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'immovable_asset');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'immovable_asset');
      });
    }
  });
}

function addMovableRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-movable-remove-row').unbind();
  $('.' + frm.doc.name + '-movable-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-movable-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-movable-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.movable_asset[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.movable_asset = details.movable_asset.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'movable_asset');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'movable_asset');
      });
    }
  });
}

function addPledgeRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-pledge-remove-row').unbind();
  $('.' + frm.doc.name + '-pledge-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-pledge-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-pledge-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.pledge[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.pledge = details.pledge.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'pledge');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'pledge');
      });
    }
  });
}

function addPersonalGuaranteeRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-remove-row').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-remove-row').on(
    'click',
    function () {
      const removeAssets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-personal-guarantee-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-personal-guarantee-select-row`).each(function (i) {
        if (this.checked) {
          removeAssets.push(details.personal_guarantee[i]);
        }
      });

      const keys = [];
      removeAssets.forEach(asset => {
        details.personal_guarantee = details.personal_guarantee.filter(
          el => el.document_code !== asset.document_code,
        );
        if (asset?.s3_key) {
          keys.push(asset.s3_key);
        }
        if (asset?.standard_template_s3_key) {
          keys.push(asset.standard_template_s3_key);
        }
      });

      frm.dirty();
      if (keys.length > 0) {
        deleteMultipleFromS3(keys).then(r => {
          frm.doc.type_of_security_details = JSON.stringify(details);
          frm.save().then(() => {
            loadAssetTables(frm, 'personal_guarantee');
          });
        });
      } else {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'personal_guarantee');
        });
      }
    },
  );
}

function addCorporateGuaranteeRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-remove-row').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-remove-row').on(
    'click',
    function () {
      const removeAssets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-corporate-guarantee-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-corporate-guarantee-select-row`).each(function (i) {
        if (this.checked) {
          removeAssets.push(details.corporate_guarantee[i]);
        }
      });

      const keys = [];
      removeAssets.forEach(asset => {
        details.corporate_guarantee = details.corporate_guarantee.filter(
          el => el.document_code !== asset.document_code,
        );
        if (asset?.s3_key) {
          keys.push(asset.s3_key);
        }
        if (asset?.standard_template_s3_key) {
          keys.push(asset.standard_template_s3_key);
        }
      });

      frm.dirty();
      if (keys.length > 0) {
        deleteMultipleFromS3(keys).then(r => {
          frm.doc.type_of_security_details = JSON.stringify(details);
          frm.save().then(() => {
            loadAssetTables(frm, 'corporate_guarantee');
          });
        });
      } else {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'corporate_guarantee');
        });
      }
    },
  );
}

function addNduRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-ndu-remove-row').unbind();
  $('.' + frm.doc.name + '-ndu-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-ndu-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-ndu-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.ndu[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.ndu = details.ndu.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'ndu');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'ndu');
      });
    }
  });
}

function addIntangibleRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-intangible-remove-row').unbind();
  $('.' + frm.doc.name + '-intangible-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-intangible-select-row"]:checked`)
        .length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-intangible-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.intangible_asset[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.intangible_asset = details.intangible_asset.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'intangible_asset');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'intangible_asset');
      });
    }
  });
}

function addMotorRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-motor-remove-row').unbind();
  $('.' + frm.doc.name + '-motor-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-motor-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-motor-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.motor_vehicle[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.motor_vehicle = details.motor_vehicle.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'motor_vehicle');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'motor_vehicle');
      });
    }
  });
}

function addFinancialRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-financial-remove-row').unbind();
  $('.' + frm.doc.name + '-financial-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-financial-select-row"]:checked`).length <=
      0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-financial-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.financial_asset[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.financial_asset = details.financial_asset.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'financial_asset');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'financial_asset');
      });
    }
  });
}

function addAssignmentOfRightsRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-remove-row').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-remove-row').on(
    'click',
    function () {
      const removeAssets = [];
      frm.doc.type_of_security_details = frm.doc.type_of_security_details
        ? frm.doc.type_of_security_details
        : JSON.stringify({});
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-assignment-of-rights-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-assignment-of-rights-select-row`).each(function (i) {
        if (this.checked) {
          removeAssets.push(details.assignment_of_rights[i]);
        }
      });

      const keys = [];
      removeAssets.forEach(asset => {
        details.assignment_of_rights = details.assignment_of_rights.filter(
          el => el.document_code !== asset.document_code,
        );
        if (asset?.s3_key) {
          keys.push(asset.s3_key);
        }
        if (asset?.standard_template_s3_key) {
          keys.push(asset.standard_template_s3_key);
        }
      });

      frm.dirty();
      if (keys.length > 0) {
        deleteMultipleFromS3(keys).then(r => {
          frm.doc.type_of_security_details = JSON.stringify(details);
          frm.save().then(() => {
            loadAssetTables(frm, 'assignment_of_rights');
          });
        });
      } else {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'assignment_of_rights');
        });
      }
    },
  );
}

function addCurrentRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-current-remove-row').unbind();
  $('.' + frm.doc.name + '-current-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-current-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-current-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.current_asset[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.current_asset = details.current_asset.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'current_asset');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'current_asset');
      });
    }
  });
}

function addDsraRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-dsra-remove-row').unbind();
  $('.' + frm.doc.name + '-dsra-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-dsra-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-dsra-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.debt_service_reserve_account[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.debt_service_reserve_account =
        details.debt_service_reserve_account.filter(
          el => el.document_code !== asset.document_code,
        );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'debt_service_reserve_account');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'debt_service_reserve_account');
      });
    }
  });
}

function addOthersRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-others-remove-row').unbind();
  $('.' + frm.doc.name + '-others-remove-row').on('click', function () {
    const removeAssets = [];
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};

    if (
      $(`input[class="${frm.doc.name}-others-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    $(`.${frm.doc.name}-others-select-row`).each(function (i) {
      if (this.checked) {
        removeAssets.push(details.others[i]);
      }
    });

    const keys = [];
    removeAssets.forEach(asset => {
      details.others = details.others.filter(
        el => el.document_code !== asset.document_code,
      );
      if (asset?.s3_key) {
        keys.push(asset.s3_key);
      }
      if (asset?.standard_template_s3_key) {
        keys.push(asset.standard_template_s3_key);
      }
    });

    frm.dirty();
    if (keys.length > 0) {
      deleteMultipleFromS3(keys).then(r => {
        frm.doc.type_of_security_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'others');
        });
      });
    } else {
      frm.doc.type_of_security_details = JSON.stringify(details);
      frm.save().then(() => {
        loadAssetTables(frm, 'others');
      });
    }
  });
}

function addPreExecutionDetailsTableRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-pre-execution-details-remove-row').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-remove-row').on(
    'click',
    function () {
      const removeAssets = [];
      frm.doc.pre_execution_details = frm.doc.pre_execution_details
        ? frm.doc.pre_execution_details
        : JSON.stringify({});
      const details =
        frm.doc.pre_execution_details !== undefined
          ? JSON.parse(frm.doc.pre_execution_details)
          : {};

      if (
        $(
          `input[class="${frm.doc.name}-pre-execution-details-select-row"]:checked`,
        ).length <= 0
      ) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $(`.${frm.doc.name}-pre-execution-details-select-row`).each(function (i) {
        if (this.checked) {
          removeAssets.push(details.pre_execution_details[i]);
        }
      });

      const keys = [];
      removeAssets.forEach(asset => {
        details.pre_execution_details = details.pre_execution_details.filter(
          el => el.document_code !== asset.document_code,
        );
        if (asset?.s3_key) {
          keys.push(asset.s3_key);
        }
        if (asset?.standard_template_s3_key) {
          keys.push(asset.standard_template_s3_key);
        }
      });

      frm.dirty();
      if (keys.length > 0) {
        deleteMultipleFromS3(keys).then(r => {
          frm.doc.pre_execution_details = JSON.stringify(details);
          frm.save().then(() => {
            loadAssetTables(frm, 'pre_execution_details');
          });
        });
      } else {
        frm.doc.pre_execution_details = JSON.stringify(details);
        frm.save().then(() => {
          loadAssetTables(frm, 'pre_execution_details');
        });
      }
    },
  );
}

function addEditEvents(frm) {
  $('.' + frm.doc.name + '-common_documents-edit-row').unbind();
  $('.' + frm.doc.name + '-common_documents-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-common_documents-edit-row').index(
      this,
    );
    const assetName = 'common_documents';
    rowDialog(
      frm,
      getAssetValues(frm).common_documents[index],
      assetName,
      index,
    );
  });
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    $('.' + frm.doc.name + '-immovable-edit-row').unbind();
    $('.' + frm.doc.name + '-immovable-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-immovable-edit-row').index(this);
      const assetName = 'immovable_asset';
      rowDialog(
        frm,
        getAssetValues(frm).immovable_asset[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-movable-edit-row').unbind();
    $('.' + frm.doc.name + '-movable-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-movable-edit-row').index(this);
      const assetName = 'movable_asset';
      rowDialog(
        frm,
        getAssetValues(frm).movable_asset[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-pledge-edit-row').unbind();
    $('.' + frm.doc.name + '-pledge-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-pledge-edit-row').index(this);
      const assetName = 'pledge';
      rowDialog(frm, getAssetValues(frm).pledge[index], assetName, index);
    });

    $('.' + frm.doc.name + '-personal-guarantee-edit-row').unbind();
    $('.' + frm.doc.name + '-personal-guarantee-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-personal-guarantee-edit-row',
        ).index(this);
        const assetName = 'personal_guarantee';
        rowDialog(
          frm,
          getAssetValues(frm).personal_guarantee[index],
          assetName,
          index,
        );
      },
    );

    $('.' + frm.doc.name + '-corporate-guarantee-edit-row').unbind();
    $('.' + frm.doc.name + '-corporate-guarantee-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-corporate-guarantee-edit-row',
        ).index(this);
        const assetName = 'corporate_guarantee';
        rowDialog(
          frm,
          getAssetValues(frm).corporate_guarantee[index],
          assetName,
          index,
        );
      },
    );

    $('.' + frm.doc.name + '-ndu-edit-row').unbind();
    $('.' + frm.doc.name + '-ndu-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-ndu-edit-row').index(this);
      const assetName = 'ndu';
      rowDialog(frm, getAssetValues(frm).ndu[index], assetName, index);
    });

    $('.' + frm.doc.name + '-intangible-edit-row').unbind();
    $('.' + frm.doc.name + '-intangible-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-intangible-edit-row').index(this);
      const assetName = 'intangible_asset';
      rowDialog(
        frm,
        getAssetValues(frm).intangible_asset[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-motor-edit-row').unbind();
    $('.' + frm.doc.name + '-motor-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-motor-edit-row').index(this);
      const assetName = 'motor_vehicle';
      rowDialog(
        frm,
        getAssetValues(frm).motor_vehicle[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-financial-edit-row').unbind();
    $('.' + frm.doc.name + '-financial-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-financial-edit-row').index(this);
      const assetName = 'financial_asset';
      rowDialog(
        frm,
        getAssetValues(frm).financial_asset[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-assignment-of-rights-edit-row').unbind();
    $('.' + frm.doc.name + '-assignment-of-rights-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-assignment-of-rights-edit-row',
        ).index(this);
        const assetName = 'assignment_of_rights';
        rowDialog(
          frm,
          getAssetValues(frm).assignment_of_rights[index],
          assetName,
          index,
        );
      },
    );

    $('.' + frm.doc.name + '-current-edit-row').unbind();
    $('.' + frm.doc.name + '-current-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-current-edit-row').index(this);
      const assetName = 'current_asset';
      rowDialog(
        frm,
        getAssetValues(frm).current_asset[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-dsra-edit-row').unbind();
    $('.' + frm.doc.name + '-dsra-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-dsra-edit-row').index(this);
      const assetName = 'debt_service_reserve_account';
      rowDialog(
        frm,
        getAssetValues(frm).debt_service_reserve_account[index],
        assetName,
        index,
      );
    });

    $('.' + frm.doc.name + '-others-edit-row').unbind();
    $('.' + frm.doc.name + '-others-edit-row').on('click', function () {
      const index = $('.' + frm.doc.name + '-others-edit-row').index(this);
      const assetName = 'others';
      rowDialog(frm, getAssetValues(frm).others[index], assetName, index);
    });
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    $('.' + frm.doc.name + '-pre-execution-details-edit-row').unbind();
    $('.' + frm.doc.name + '-pre-execution-details-edit-row').on(
      'click',
      function () {
        const index = $(
          '.' + frm.doc.name + '-pre-execution-details-edit-row',
        ).index(this);
        const assetName = 'pre_execution_details';
        rowDialog(
          frm,
          getAssetValues(frm).pre_execution_details[index],
          assetName,
          index,
        );
      },
    );
  }
}

function addUploadEvents(frm) {
  addCommonDocumentsPreviewEvent(frm);
  addCommonDocumentsRemoveFileEvent(frm);
  addCommonDocumentsAttachFileEvent(frm);
  addCommonDocumentsUploadAllEvent(frm);
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    // immovable asset events
    addImmovablePreviewEvent(frm);
    addImmovableRemoveFileEvent(frm);
    addImmovableAttachFileEvent(frm);
    addImmovableUploadAllEvent(frm);

    // movable asset events
    addMovablePreviewEvent(frm);
    addMovableRemoveFileEvent(frm);
    addMovableAttachFileEvent(frm);
    addMovableUploadAllEvent(frm);

    // pledge asset events
    addPledgePreviewEvent(frm);
    addPledgeEemoveFileEvent(frm);
    addPledgeAttachFileEvent(frm);
    addPledgeUploadAllEvent(frm);

    // personal guarantee events
    addPersonalGuaranteePreviewEvent(frm);
    addPersonalGuaranteeRemoveFileEvent(frm);
    addPersonalGuaranteeAttachFileEvent(frm);
    addPersonalGuaranteeUploadAllEvent(frm);

    // corporate guarantee events
    addCorporateGuaranteePreviewEvent(frm);
    addCorporateGuaranteeRemoveFileEvent(frm);
    addCorporateGuaranteeAttachFileEvent(frm);
    addCorporateGuaranteeUploadAllEvent(frm);

    // ndu events
    addNduPreviewEvent(frm);
    addNduRemoveFileEvent(frm);
    addNduAttachFileEvent(frm);
    addNduUploadAllEvent(frm);

    // intangible events
    addIntangiblePreviewEvent(frm);
    addIntangibleRemoveFileEvent(frm);
    addIntangibleAttachFileEvent(frm);
    addIntangibleUploadAllEvent(frm);

    // motor events
    addMotorPreviewEvent(frm);
    addMotorRemoveFileEvent(frm);
    addMotorAttachFileEvent(frm);
    addMotorUploadAllEvent(frm);

    // financial events
    addFinancialPreviewEvent(frm);
    addFinancialRemoveFileEvent(frm);
    addFinancialAttachFileEvent(frm);
    addFinancialUploadAllEvent(frm);

    // assignment of rights events
    addAssignmentOfRightsPreviewEvent(frm);
    addAssignmentOfRightsRemoveFileEvent(frm);
    addAssignmentOfRightsAttachFileEvent(frm);
    addAssignmentOfRightsUploadAllEvent(frm);

    // current events
    addCurrentPreviewEvent(frm);
    addCurrentRemoveFileEvent(frm);
    addCurrentAttachFileEvent(frm);
    addCurrentUploadAllEvent(frm);

    // dsra events
    addDsraPreviewEvent(frm);
    addDsraRemoveFileEvent(frm);
    addDsraAttachFileEvent(frm);
    addDsraUploadAllEvent(frm);

    // others events
    addOthersPreviewEvent(frm);
    addOthersRemoveFileEvent(frm);
    addOthersAttachFileEvent(frm);
    addOthersUploadAllEvent(frm);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    // pre execution details table events
    addPreExecutionDetailsPreviewEvent(frm);
    addPreExecutionDetailsRemoveFileEvent(frm);
    addPreExecutionDetailsAttachFileEvent(frm);
    addPreExecutionDetailsUploadAllEvent(frm);
  }
}

function addCommonDocumentsPreviewEvent(frm) {
  $('.' + frm.doc.name + '-common_documents-preview-file').unbind();
  $('.' + frm.doc.name + '-common_documents-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-common_documents-preview-file',
      ).index(this);
      const blobUrl = details.common_documents[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function addCommonDocumentsRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-common_documents-remove-file').unbind();
  $('.' + frm.doc.name + '-common_documents-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-common_documents-remove-file',
      ).index(this);
      details.common_documents[index].file = '';
      details.common_documents[index].file_name = details.common_documents[
        index
      ].s3_key
        ? details.common_documents[index].s3_key.replace(/^.*\//, '')
        : '';
      fileRemove('common_documents', index);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'common_documents');
    },
  );
}

function addCommonDocumentsAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-common_documents-attach-file').unbind();
  $('.' + frm.doc.name + '-common_documents-attach-input').unbind();
  $('.' + frm.doc.name + '-common_documents-attach-file').on(
    'click',
    function () {
      s3ButtonIndex = $(
        '.' + frm.doc.name + '-common_documents-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-common_documents-attach-input')[
        s3ButtonIndex
      ].click();
    },
  );

  $('.' + frm.doc.name + '-common_documents-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        addFiles('common_documents', file[0], details, s3ButtonIndex);
        details.common_documents[s3ButtonIndex].file_name = file[0].name;
        details.common_documents[s3ButtonIndex].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.type_of_security_details = JSON.stringify(details);
        loadAssetTables(frm, 'common_documents');
      }
    },
  );
}

function addCommonDocumentsUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-common_documents-upload-all').unbind();
  $('.' + frm.doc.name + '-common_documents-upload-all').on('click', () => {
    if (getAssetValues(frm).common_documents.some(x => x.file)) {
      const commonDocuments = JSON.stringify(
        getAssetValues(frm).common_documents,
      );
      preExecutionS3Upload(
        frm,
        commonDocuments,
        'common_documents',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addImmovablePreviewEvent(frm) {
  $('.' + frm.doc.name + '-immovable-preview-file').unbind();
  $('.' + frm.doc.name + '-immovable-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-immovable-preview-file').index(this);
    const blobUrl = details.immovable_asset[index].file;

    window.open(blobUrl, '_blank');
  });
}

function addImmovableRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-immovable-remove-file').unbind();
  $('.' + frm.doc.name + '-immovable-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-immovable-remove-file').index(this);
    details.immovable_asset[index].file = '';
    details.immovable_asset[index].file_name = details.immovable_asset[index]
      .s3_key
      ? details.immovable_asset[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('immovable_asset', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'immovable_asset');
  });
}

function addImmovableAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-immovable-attach-file').unbind();
  $('.' + frm.doc.name + '-immovable-attach-input').unbind();
  $('.' + frm.doc.name + '-immovable-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-immovable-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-immovable-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-immovable-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('immovable_asset', file[0], details, s3ButtonIndex);
      details.immovable_asset[s3ButtonIndex].file_name = file[0].name;
      details.immovable_asset[s3ButtonIndex].file = await fileToBlobAndUrl(
        file[0],
      );
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'immovable_asset');
    }
  });
}

function addImmovableUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-immovable-upload-all').unbind();
  $('.' + frm.doc.name + '-immovable-upload-all').on('click', () => {
    if (getAssetValues(frm).immovable_asset.some(x => x.file)) {
      const immovableAsset = JSON.stringify(
        getAssetValues(frm).immovable_asset,
      );
      preExecutionS3Upload(
        frm,
        immovableAsset,
        'immovable_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addMovablePreviewEvent(frm) {
  $('.' + frm.doc.name + '-movable-preview-file').unbind();
  $('.' + frm.doc.name + '-movable-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-movable-preview-file').index(this);
    const blobUrl = details.movable_asset[index].file;

    window.open(blobUrl, '_blank');
  });
}

function addMovableRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-movable-remove-file').unbind();
  $('.' + frm.doc.name + '-movable-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-movable-remove-file').index(this);
    details.movable_asset[index].file = '';
    details.movable_asset[index].file_name = details.movable_asset[index].s3_key
      ? details.movable_asset[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('movable_asset', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'movable_asset');
  });
}

function addMovableAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-movable-attach-file').unbind();
  $('.' + frm.doc.name + '-movable-attach-input').unbind();

  $('.' + frm.doc.name + '-movable-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-movable-attach-file').index(this);
    $('.' + frm.doc.name + '-movable-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-movable-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('movable_asset', file[0], details, s3ButtonIndex);
      details.movable_asset[s3ButtonIndex].file_name = file[0].name;
      details.movable_asset[s3ButtonIndex].file = await fileToBlobAndUrl(
        file[0],
      );
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'movable_asset');
    }
  });
}

function addMovableUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-movable-upload-all').unbind();
  $('.' + frm.doc.name + '-movable-upload-all').on('click', () => {
    if (getAssetValues(frm).movable_asset.some(x => x.file)) {
      const movableAsset = JSON.stringify(getAssetValues(frm).movable_asset);
      preExecutionS3Upload(
        frm,
        movableAsset,
        'movable_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addPledgePreviewEvent(frm) {
  $('.' + frm.doc.name + '-pledge-preview-file').unbind();
  $('.' + frm.doc.name + '-pledge-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-pledge-preview-file').index(this);
    const blobUrl = details.pledge[index].file;

    window.open(blobUrl, '_blank');
  });
}

function addPledgeEemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-pledge-remove-file').unbind();
  $('.' + frm.doc.name + '-pledge-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-pledge-remove-file').index(this);
    details.pledge[index].file = '';
    details.pledge[index].file_name = details.pledge[index].s3_key
      ? details.pledge[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('pledge', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'pledge');
  });
}

function addPledgeAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-pledge-attach-file').unbind();
  $('.' + frm.doc.name + '-pledge-attach-input').unbind();

  $('.' + frm.doc.name + '-pledge-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-pledge-attach-file').index(this);
    $('.' + frm.doc.name + '-pledge-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-pledge-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('pledge', file[0], details, s3ButtonIndex);
      details.pledge[s3ButtonIndex].file_name = file[0].name;
      details.pledge[s3ButtonIndex].file = await fileToBlobAndUrl(file[0]);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'pledge');
    }
  });
}

function addPledgeUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-pledge-upload-all').unbind();
  $('.' + frm.doc.name + '-pledge-upload-all').on('click', () => {
    if (getAssetValues(frm).pledge.some(x => x.file)) {
      const pledge = JSON.stringify(getAssetValues(frm).pledge);
      preExecutionS3Upload(frm, pledge, 'pledge', 'type_of_security_details');
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addPersonalGuaranteePreviewEvent(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-preview-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-preview-file',
      ).index(this);
      const blobUrl = details.personal_guarantee[index].file;
      window.open(blobUrl, '_blank');
    },
  );
}

function addPersonalGuaranteeRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-remove-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-remove-file',
      ).index(this);
      details.personal_guarantee[index].file = '';
      details.personal_guarantee[index].file_name = details.personal_guarantee[
        index
      ].s3_key
        ? details.personal_guarantee[index].s3_key.replace(/^.*\//, '')
        : '';
      fileRemove('immovable_asset', index);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'personal_guarantee');
    },
  );
}

function addPersonalGuaranteeAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-personal-guarantee-attach-file').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-attach-input').unbind();

  $('.' + frm.doc.name + '-personal-guarantee-attach-file').on(
    'click',
    function () {
      s3ButtonIndex = $(
        '.' + frm.doc.name + '-personal-guarantee-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-personal-guarantee-attach-input')[
        s3ButtonIndex
      ].click();
    },
  );

  $('.' + frm.doc.name + '-personal-guarantee-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        addFiles('personal_guarantee', file[0], details, s3ButtonIndex);
        details.personal_guarantee[s3ButtonIndex].file_name = file[0].name;
        details.personal_guarantee[s3ButtonIndex].file = await fileToBlobAndUrl(
          file[0],
        );
        frm.doc.type_of_security_details = JSON.stringify(details);
        loadAssetTables(frm, 'personal_guarantee');
      }
    },
  );
}

function addPersonalGuaranteeUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-upload-all').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-upload-all').on('click', () => {
    if (getAssetValues(frm).personal_guarantee.some(x => x.file)) {
      const personalGuarantee = JSON.stringify(
        getAssetValues(frm).personal_guarantee,
      );
      preExecutionS3Upload(
        frm,
        personalGuarantee,
        'personal_guarantee',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addCorporateGuaranteePreviewEvent(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-preview-file').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-preview-file',
      ).index(this);
      const blobUrl = details.corporate_guarantee[index].file;
      window.open(blobUrl, '_blank');
    },
  );
}

function addCorporateGuaranteeRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-remove-file').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-remove-file',
      ).index(this);
      details.corporate_guarantee[index].file = '';
      details.corporate_guarantee[index].file_name = details
        .corporate_guarantee[index].s3_key
        ? details.corporate_guarantee[index].s3_key.replace(/^.*\//, '')
        : '';
      fileRemove('corporate_guarantee', index);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'corporate_guarantee');
    },
  );
}

function addCorporateGuaranteeAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-corporate-guarantee-attach-file').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-attach-input').unbind();

  $('.' + frm.doc.name + '-corporate-guarantee-attach-file').on(
    'click',
    function () {
      s3ButtonIndex = $(
        '.' + frm.doc.name + '-corporate-guarantee-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-corporate-guarantee-attach-input')[
        s3ButtonIndex
      ].click();
    },
  );

  $('.' + frm.doc.name + '-corporate-guarantee-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        addFiles('corporate_guarantee', file[0], details, s3ButtonIndex);
        details.corporate_guarantee[s3ButtonIndex].file_name = file[0].name;
        details.corporate_guarantee[s3ButtonIndex].file =
          await fileToBlobAndUrl(file[0]);
        frm.doc.type_of_security_details = JSON.stringify(details);
        loadAssetTables(frm, 'corporate_guarantee');
      }
    },
  );
}

function addCorporateGuaranteeUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-upload-all').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-upload-all').on('click', () => {
    if (getAssetValues(frm).corporate_guarantee.some(x => x.file)) {
      const corporateGuarantee = JSON.stringify(
        getAssetValues(frm).corporate_guarantee,
      );
      preExecutionS3Upload(
        frm,
        corporateGuarantee,
        'corporate_guarantee',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addNduPreviewEvent(frm) {
  $('.' + frm.doc.name + '-ndu-preview-file').unbind();
  $('.' + frm.doc.name + '-ndu-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-ndu-preview-file').index(this);
    const blobUrl = details.ndu[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addNduRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-ndu-remove-file').unbind();
  $('.' + frm.doc.name + '-ndu-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-ndu-remove-file').index(this);
    details.ndu[index].file = '';
    details.ndu[index].file_name = details.ndu[index].s3_key
      ? details.ndu[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('ndu', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'ndu');
  });
}

function addNduAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-ndu-attach-file').unbind();
  $('.' + frm.doc.name + '-ndu-attach-input').unbind();

  $('.' + frm.doc.name + '-ndu-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-ndu-attach-file').index(this);
    $('.' + frm.doc.name + '-ndu-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-ndu-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('ndu', file[0], details, s3ButtonIndex);
      details.ndu[s3ButtonIndex].file_name = file[0].name;
      details.ndu[s3ButtonIndex].file = await fileToBlobAndUrl(file[0]);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'ndu');
    }
  });
}

function addNduUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-ndu-upload-all').unbind();
  $('.' + frm.doc.name + '-ndu-upload-all').on('click', () => {
    if (getAssetValues(frm).ndu.some(x => x.file)) {
      const ndu = JSON.stringify(getAssetValues(frm).ndu);
      preExecutionS3Upload(frm, ndu, 'ndu', 'type_of_security_details');
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addIntangiblePreviewEvent(frm) {
  $('.' + frm.doc.name + '-intangible-preview-file').unbind();
  $('.' + frm.doc.name + '-intangible-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-intangible-preview-file').index(
      this,
    );
    const blobUrl = details.intangible_asset[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addIntangibleRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-intangible-remove-file').unbind();
  $('.' + frm.doc.name + '-intangible-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-intangible-remove-file').index(this);
    details.intangible_asset[index].file = '';
    details.intangible_asset[index].file_name = details.intangible_asset[index]
      .s3_key
      ? details.intangible_asset[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('intangible_asset', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'intangible_asset');
  });
}

function addIntangibleAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-intangible-attach-file').unbind();
  $('.' + frm.doc.name + '-intangible-attach-input').unbind();

  $('.' + frm.doc.name + '-intangible-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-intangible-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-intangible-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-intangible-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('intangible_asset', file[0], details, s3ButtonIndex);
      details.intangible_asset[s3ButtonIndex].file_name = file[0].name;
      details.intangible_asset[s3ButtonIndex].file = await fileToBlobAndUrl(
        file[0],
      );
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'intangible_asset');
    }
  });
}

function addIntangibleUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-intangible-upload-all').unbind();
  $('.' + frm.doc.name + '-intangible-upload-all').on('click', () => {
    if (getAssetValues(frm).intangible_asset.some(x => x.file)) {
      const intangibleAsset = JSON.stringify(
        getAssetValues(frm).intangible_asset,
      );
      preExecutionS3Upload(
        frm,
        intangibleAsset,
        'intangible_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addMotorPreviewEvent(frm) {
  $('.' + frm.doc.name + '-motor-preview-file').unbind();
  $('.' + frm.doc.name + '-motor-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-motor-preview-file').index(this);
    const blobUrl = details.motor_vehicle[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addMotorRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-motor-remove-file').unbind();
  $('.' + frm.doc.name + '-motor-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-motor-remove-file').index(this);
    details.motor_vehicle[index].file = '';
    details.motor_vehicle[index].file_name = details.motor_vehicle[index].s3_key
      ? details.motor_vehicle[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('motor_vehicle', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'motor_vehicle');
  });
}

function addMotorAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-motor-attach-file').unbind();
  $('.' + frm.doc.name + '-motor-attach-input').unbind();

  $('.' + frm.doc.name + '-motor-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-motor-attach-file').index(this);
    $('.' + frm.doc.name + '-motor-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-motor-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('motor_vehicle', file[0], details, s3ButtonIndex);
      details.motor_vehicle[s3ButtonIndex].file_name = file[0].name;
      details.motor_vehicle[s3ButtonIndex].file = await fileToBlobAndUrl(
        file[0],
      );
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'motor_vehicle');
    }
  });
}

function addMotorUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-motor-upload-all').unbind();
  $('.' + frm.doc.name + '-motor-upload-all').on('click', () => {
    if (getAssetValues(frm).motor_vehicle.some(x => x.file)) {
      const motorVehicle = JSON.stringify(getAssetValues(frm).motor_vehicle);
      preExecutionS3Upload(
        frm,
        motorVehicle,
        'motor_vehicle',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addFinancialPreviewEvent(frm) {
  $('.' + frm.doc.name + '-financial-preview-file').unbind();
  $('.' + frm.doc.name + '-financial-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-financial-preview-file').index(this);
    const blobUrl = details.financial_asset[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addFinancialRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-financial-remove-file').unbind();
  $('.' + frm.doc.name + '-financial-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-financial-remove-file').index(this);
    details.financial_asset[index].file = '';
    details.financial_asset[index].file_name = details.financial_asset[index]
      .s3_key
      ? details.financial_asset[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('financial_asset', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'financial_asset');
  });
}

function addFinancialAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-financial-attach-file').unbind();
  $('.' + frm.doc.name + '-financial-attach-input').unbind();

  $('.' + frm.doc.name + '-financial-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-financial-attach-file').index(
      this,
    );
    $('.' + frm.doc.name + '-financial-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-financial-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('financial_asset', file[0], details, s3ButtonIndex);
      details.financial_asset[s3ButtonIndex].file_name = file[0].name;
      details.financial_asset[s3ButtonIndex].file = await fileToBlobAndUrl(
        file[0],
      );
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'financial_asset');
    }
  });
}

function addFinancialUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-financial-upload-all').unbind();
  $('.' + frm.doc.name + '-financial-upload-all').on('click', () => {
    if (getAssetValues(frm).financial_asset.some(x => x.file)) {
      const financialAsset = JSON.stringify(
        getAssetValues(frm).financial_asset,
      );
      preExecutionS3Upload(
        frm,
        financialAsset,
        'financial_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addAssignmentOfRightsPreviewEvent(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-preview-file').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-preview-file',
      ).index(this);
      const blobUrl = details.assignment_of_rights[index].file;
      window.open(blobUrl, '_blank');
    },
  );
}

function addAssignmentOfRightsRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-remove-file').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-remove-file',
      ).index(this);
      details.assignment_of_rights[index].file = '';
      details.assignment_of_rights[index].file_name = details
        .assignment_of_rights[index].s3_key
        ? details.assignment_of_rights[index].s3_key.replace(/^.*\//, '')
        : '';
      fileRemove('assignment_of_rights', index);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'assignment_of_rights');
    },
  );
}

function addAssignmentOfRightsAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-assignment-of-rights-attach-file').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-attach-input').unbind();

  $('.' + frm.doc.name + '-assignment-of-rights-attach-file').on(
    'click',
    function () {
      s3ButtonIndex = $(
        '.' + frm.doc.name + '-assignment-of-rights-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-assignment-of-rights-attach-input')[
        s3ButtonIndex
      ].click();
    },
  );

  $('.' + frm.doc.name + '-assignment-of-rights-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.type_of_security_details !== undefined
            ? JSON.parse(frm.doc.type_of_security_details)
            : {};
        addFiles('assignment_of_rights', file[0], details, s3ButtonIndex);
        details.assignment_of_rights[s3ButtonIndex].file_name = file[0].name;
        details.assignment_of_rights[s3ButtonIndex].file =
          await fileToBlobAndUrl(file[0]);
        frm.doc.type_of_security_details = JSON.stringify(details);
        loadAssetTables(frm, 'assignment_of_rights');
      }
    },
  );
}

function addAssignmentOfRightsUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-upload-all').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-upload-all').on('click', () => {
    if (getAssetValues(frm).assignment_of_rights.some(x => x.file)) {
      const assignmentOfRights = JSON.stringify(
        getAssetValues(frm).assignment_of_rights,
      );
      preExecutionS3Upload(
        frm,
        assignmentOfRights,
        'assignment_of_rights',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addCurrentPreviewEvent(frm) {
  $('.' + frm.doc.name + '-current-preview-file').unbind();
  $('.' + frm.doc.name + '-current-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-current-preview-file').index(this);
    const blobUrl = details.current_asset[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addCurrentRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-current-remove-file').unbind();
  $('.' + frm.doc.name + '-current-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-current-remove-file').index(this);
    details.current_asset[index].file = '';
    details.current_asset[index].file_name = details.current_asset[index].s3_key
      ? details.current_asset[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('current_asset', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'current_asset');
  });
}

function addCurrentAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-current-attach-file').unbind();
  $('.' + frm.doc.name + '-current-attach-input').unbind();

  $('.' + frm.doc.name + '-current-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-current-attach-file').index(this);
    $('.' + frm.doc.name + '-current-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-current-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('current_asset', file[0], details, s3ButtonIndex);
      details.current_asset[s3ButtonIndex].file_name = file[0].name;
      details.current_asset[s3ButtonIndex].file = await fileToBlobAndUrl(
        file[0],
      );
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'current_asset');
    }
  });
}

function addCurrentUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-current-upload-all').unbind();
  $('.' + frm.doc.name + '-current-upload-all').on('click', () => {
    if (getAssetValues(frm).current_asset.some(x => x.file)) {
      const others = JSON.stringify(getAssetValues(frm).current_asset);
      preExecutionS3Upload(
        frm,
        others,
        'current_asset',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addDsraPreviewEvent(frm) {
  $('.' + frm.doc.name + '-dsra-preview-file').unbind();
  $('.' + frm.doc.name + '-dsra-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-dsra-preview-file').index(this);
    const blobUrl = details.debt_service_reserve_account[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addDsraRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-dsra-remove-file').unbind();
  $('.' + frm.doc.name + '-dsra-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-dsra-remove-file').index(this);
    details.debt_service_reserve_account[index].file = '';
    details.debt_service_reserve_account[index].file_name = details
      .debt_service_reserve_account[index].s3_key
      ? details.debt_service_reserve_account[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('debt_service_reserve_account', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'debt_service_reserve_account');
  });
}

function addDsraAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-dsra-attach-file').unbind();
  $('.' + frm.doc.name + '-dsra-attach-input').unbind();

  $('.' + frm.doc.name + '-dsra-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-dsra-attach-file').index(this);
    $('.' + frm.doc.name + '-dsra-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-dsra-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('debt_service_reserve_account', file[0], details, s3ButtonIndex);
      details.debt_service_reserve_account[s3ButtonIndex].file_name =
        file[0].name;
      details.debt_service_reserve_account[s3ButtonIndex].file =
        await fileToBlobAndUrl(file[0]);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'debt_service_reserve_account');
    }
  });
}

function addDsraUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-dsra-upload-all').unbind();
  $('.' + frm.doc.name + '-dsra-upload-all').on('click', () => {
    if (getAssetValues(frm).debt_service_reserve_account.some(x => x.file)) {
      const others = JSON.stringify(
        getAssetValues(frm).debt_service_reserve_account,
      );
      preExecutionS3Upload(
        frm,
        others,
        'debt_service_reserve_account',
        'type_of_security_details',
      );
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addOthersPreviewEvent(frm) {
  $('.' + frm.doc.name + '-others-preview-file').unbind();
  $('.' + frm.doc.name + '-others-preview-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-others-preview-file').index(this);
    const blobUrl = details.others[index].file;
    window.open(blobUrl, '_blank');
  });
}

function addOthersRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-others-remove-file').unbind();
  $('.' + frm.doc.name + '-others-remove-file').on('click', function () {
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const index = $('.' + frm.doc.name + '-others-remove-file').index(this);
    details.others[index].file = '';
    details.others[index].file_name = details.others[index].s3_key
      ? details.others[index].s3_key.replace(/^.*\//, '')
      : '';
    fileRemove('others', index);
    frm.doc.type_of_security_details = JSON.stringify(details);
    loadAssetTables(frm, 'others');
  });
}

function addOthersAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-others-attach-file').unbind();
  $('.' + frm.doc.name + '-others-attach-input').unbind();

  $('.' + frm.doc.name + '-others-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-others-attach-file').index(this);
    $('.' + frm.doc.name + '-others-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-others-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const details =
        frm.doc.type_of_security_details !== undefined
          ? JSON.parse(frm.doc.type_of_security_details)
          : {};
      addFiles('others', file[0], details, s3ButtonIndex);
      details.others[s3ButtonIndex].file_name = file[0].name;
      details.others[s3ButtonIndex].file = await fileToBlobAndUrl(file[0]);
      frm.doc.type_of_security_details = JSON.stringify(details);
      loadAssetTables(frm, 'others');
    }
  });
}

function addOthersUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-others-upload-all').unbind();
  $('.' + frm.doc.name + '-others-upload-all').on('click', () => {
    if (getAssetValues(frm).others.some(x => x.file)) {
      const others = JSON.stringify(getAssetValues(frm).others);
      preExecutionS3Upload(frm, others, 'others', 'type_of_security_details');
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addPreExecutionDetailsPreviewEvent(frm) {
  $('.' + frm.doc.name + '-pre-execution-details-preview-file').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-preview-file').on(
    'click',
    function () {
      const details =
        frm.doc.pre_execution_details !== undefined
          ? JSON.parse(frm.doc.pre_execution_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-preview-file',
      ).index(this);
      const blobUrl = details.pre_execution_details[index].file;

      window.open(blobUrl, '_blank');
    },
  );
}

function addPreExecutionDetailsRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-pre-execution-details-remove-file').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-remove-file').on(
    'click',
    function () {
      const details =
        frm.doc.pre_execution_details !== undefined
          ? JSON.parse(frm.doc.pre_execution_details)
          : {};
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-remove-file',
      ).index(this);
      details.pre_execution_details[index].file = '';
      details.pre_execution_details[index].file_name = details
        .pre_execution_details[index].s3_key
        ? details.pre_execution_details[index].s3_key.replace(/^.*\//, '')
        : '';
      fileRemove('pre_execution_details', index);
      frm.doc.pre_execution_details = JSON.stringify(details);
      loadAssetTables(frm, 'pre_execution_details');
    },
  );
}

function addPreExecutionDetailsAttachFileEvent(frm) {
  let s3ButtonIndex;

  $('.' + frm.doc.name + '-pre-execution-details-attach-file').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-attach-input').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-attach-file').on(
    'click',
    function () {
      s3ButtonIndex = $(
        '.' + frm.doc.name + '-pre-execution-details-attach-file',
      ).index(this);
      $('.' + frm.doc.name + '-pre-execution-details-attach-input')[
        s3ButtonIndex
      ].click();
    },
  );

  $('.' + frm.doc.name + '-pre-execution-details-attach-input').change(
    async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        event.target.value = '';
        const details =
          frm.doc.pre_execution_details !== undefined
            ? JSON.parse(frm.doc.pre_execution_details)
            : {};
        addFiles('pre_execution_details', file[0], details, s3ButtonIndex);
        details.pre_execution_details[s3ButtonIndex].file_name = file[0].name;
        details.pre_execution_details[s3ButtonIndex].file =
          await fileToBlobAndUrl(file[0]);
        frm.doc.pre_execution_details = JSON.stringify(details);
        loadAssetTables(frm, 'pre_execution_details');
      }
    },
  );
}

function addPreExecutionDetailsUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-pre-execution-details-upload-all').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-upload-all').on(
    'click',
    () => {
      if (getAssetValues(frm).pre_execution_details.some(x => x.file)) {
        const preExecutionDetails = JSON.stringify(
          getAssetValues(frm).pre_execution_details
            ? getAssetValues(frm).pre_execution_details
            : [],
        );
        preExecutionS3Upload(
          frm,
          preExecutionDetails,
          'pre_execution_details',
          'pre_execution_details',
        );
      } else {
        frappe.throw({
          message: 'No Attachments found.',
          title: 'Missing Attachments',
        });
      }
    },
  );
}

function addDownloadEvents(frm) {
  addCommonDocumentsDownloadEvents(frm);
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    addImmovableDownloadEvents(frm);
    addMovableDownloadEvents(frm);
    addPledgeDownloadEvents(frm);
    addPersonalGuaranteeDownloadEvents(frm);
    addCorporateGuaranteeDownloadEvents(frm);
    addNduDownloadEvents(frm);
    addIntangibleDownloadEvents(frm);
    addMotorDownloadEvents(frm);
    addFinancialDownloadEvents(frm);
    addAssignmentOfRightsDownloadEvents(frm);
    addCurrentDownloadEvents(frm);
    addDsraDownloadEvents(frm);
    addOthersDownloadEvents(frm);
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    addPreExecutionDetailsDownloadEvents(frm);
  }
}

function addImmovableDownloadEvents(frm) {
  $('.' + frm.doc.name + '-immovable-s3-download').unbind();
  $('.' + frm.doc.name + '-immovable-standard-template-download').unbind();
  $('.' + frm.doc.name + '-immovable-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-immovable-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).immovable_asset[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-immovable-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-immovable-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).immovable_asset[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addCommonDocumentsDownloadEvents(frm) {
  $('.' + frm.doc.name + '-common_documents-s3-download').unbind();
  $(
    '.' + frm.doc.name + '-common_documents-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-common_documents-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-common_documents-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).common_documents[index].s3_key,
      });
    },
  );
  $('.' + frm.doc.name + '-common_documents-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-common_documents-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).common_documents[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addMovableDownloadEvents(frm) {
  $('.' + frm.doc.name + '-movable-s3-download').unbind();
  $('.' + frm.doc.name + '-movable-standard-template-download').unbind();
  $('.' + frm.doc.name + '-movable-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-movable-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).movable_asset[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-movable-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-movable-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).movable_asset[index].standard_template_s3_key,
      });
    },
  );
}

function addPledgeDownloadEvents(frm) {
  $('.' + frm.doc.name + '-pledge-s3-download').unbind();
  $('.' + frm.doc.name + '-pledge-standard-template-download').unbind();
  $('.' + frm.doc.name + '-pledge-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-pledge-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).pledge[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-pledge-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-pledge-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).pledge[index].standard_template_s3_key,
      });
    },
  );
}

function addPersonalGuaranteeDownloadEvents(frm) {
  $('.' + frm.doc.name + '-personal-guarantee-s3-download').unbind();
  $(
    '.' + frm.doc.name + '-personal-guarantee-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-personal-guarantee-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).personal_guarantee[index].s3_key,
      });
    },
  );
  $('.' + frm.doc.name + '-personal-guarantee-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-personal-guarantee-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).personal_guarantee[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addCorporateGuaranteeDownloadEvents(frm) {
  $('.' + frm.doc.name + '-corporate-guarantee-s3-download').unbind();
  $(
    '.' + frm.doc.name + '-corporate-guarantee-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).corporate_guarantee[index].s3_key,
      });
    },
  );
  $('.' + frm.doc.name + '-corporate-guarantee-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-corporate-guarantee-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).corporate_guarantee[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addNduDownloadEvents(frm) {
  $('.' + frm.doc.name + '-ndu-s3-download').unbind();
  $('.' + frm.doc.name + '-ndu-standard-template-download').unbind();
  $('.' + frm.doc.name + '-ndu-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-ndu-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).ndu[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-ndu-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-ndu-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).ndu[index].standard_template_s3_key,
      });
    },
  );
}

function addIntangibleDownloadEvents(frm) {
  $('.' + frm.doc.name + '-intangible-s3-download').unbind();
  $('.' + frm.doc.name + '-intangible-standard-template-download').unbind();
  $('.' + frm.doc.name + '-intangible-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-intangible-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).intangible_asset[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-intangible-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-intangible-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).intangible_asset[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addMotorDownloadEvents(frm) {
  $('.' + frm.doc.name + '-motor-s3-download').unbind();
  $('.' + frm.doc.name + '-motor-standard-template-download').unbind();
  $('.' + frm.doc.name + '-motor-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-motor-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).motor_vehicle[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-motor-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-motor-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).motor_vehicle[index].standard_template_s3_key,
      });
    },
  );
}

function addFinancialDownloadEvents(frm) {
  $('.' + frm.doc.name + '-financial-s3-download').unbind();
  $('.' + frm.doc.name + '-financial-standard-template-download').unbind();
  $('.' + frm.doc.name + '-financial-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-financial-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).financial_asset[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-financial-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-financial-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).financial_asset[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addAssignmentOfRightsDownloadEvents(frm) {
  $('.' + frm.doc.name + '-assignment-of-rights-s3-download').unbind();
  $(
    '.' + frm.doc.name + '-assignment-of-rights-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).assignment_of_rights[index].s3_key,
      });
    },
  );
  $('.' + frm.doc.name + '-assignment-of-rights-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-assignment-of-rights-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).assignment_of_rights[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addCurrentDownloadEvents(frm) {
  $('.' + frm.doc.name + '-current-s3-download').unbind();
  $('.' + frm.doc.name + '-current-standard-template-download').unbind();
  $('.' + frm.doc.name + '-current-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-current-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).current_asset[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-current-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-current-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).current_asset[index].standard_template_s3_key,
      });
    },
  );
}

function addDsraDownloadEvents(frm) {
  $('.' + frm.doc.name + '-dsra-s3-download').unbind();
  $('.' + frm.doc.name + '-dsra-standard-template-download').unbind();
  $('.' + frm.doc.name + '-dsra-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-dsra-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).debt_service_reserve_account[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-dsra-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-dsra-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).debt_service_reserve_account[index]
          .standard_template_s3_key,
      });
    },
  );
}

function addOthersDownloadEvents(frm) {
  $('.' + frm.doc.name + '-others-s3-download').unbind();
  $('.' + frm.doc.name + '-others-standard-template-download').unbind();
  $('.' + frm.doc.name + '-others-s3-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-others-s3-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).others[index].s3_key,
    });
  });
  $('.' + frm.doc.name + '-others-standard-template-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-others-standard-template-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).others[index].standard_template_s3_key,
      });
    },
  );
}

function addPreExecutionDetailsDownloadEvents(frm) {
  $('.' + frm.doc.name + '-pre-execution-details-s3-download').unbind();
  $(
    '.' + frm.doc.name + '-pre-execution-details-standard-template-download',
  ).unbind();
  $('.' + frm.doc.name + '-pre-execution-details-s3-download').on(
    'click',
    function () {
      const index = $(
        '.' + frm.doc.name + '-pre-execution-details-s3-download',
      ).index(this);
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: getAssetValues(frm).pre_execution_details[index].s3_key,
      });
    },
  );
  $(
    '.' + frm.doc.name + '-pre-execution-details-standard-template-download',
  ).on('click', function () {
    const index = $(
      '.' + frm.doc.name + '-pre-execution-details-standard-template-download',
    ).index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getAssetValues(frm).pre_execution_details[index]
        .standard_template_s3_key,
    });
  });
}

function addSelectEvents(frm) {
  $('.' + frm.doc.name + '-common_documents-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-common_documents-select-row').each(
        (index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        },
      );
    } else {
      $('.' + frm.doc.name + '-common_documents-select-row').prop(
        'checked',
        false,
      );
    }
  });

  $('.' + frm.doc.name + '-immovable-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-immovable-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-immovable-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-movable-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-movable-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-movable-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-pledge-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-pledge-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-pledge-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-personal-guarantee-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-personal-guarantee-select-row').each(
        (index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        },
      );
    } else {
      $('.' + frm.doc.name + '-personal-guarantee-select-row').prop(
        'checked',
        false,
      );
    }
  });

  $('.' + frm.doc.name + '-corporate-guarantee-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-corporate-guarantee-select-row').each(
        (index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        },
      );
    } else {
      $('.' + frm.doc.name + '-corporate-guarantee-select-row').prop(
        'checked',
        false,
      );
    }
  });

  $('.' + frm.doc.name + '-ndu-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-ndu-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-ndu-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-intangible-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-intangible-select-row').each(
        (index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        },
      );
    } else {
      $('.' + frm.doc.name + '-intangible-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-motor-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-motor-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-motor-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-financial-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-financial-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-financial-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-assignment-of-rights-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-assignment-of-rights-select-row').each(
        (index, element) => {
          if ($(element).is(':hidden') === false) {
            $(element).prop('checked', true);
          }
        },
      );
    } else {
      $('.' + frm.doc.name + '-assignment-of-rights-select-row').prop(
        'checked',
        false,
      );
    }
  });

  $('.' + frm.doc.name + '-current-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-current-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-current-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-dsra-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-dsra-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-dsra-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-others-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-others-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-others-select-row').prop('checked', false);
    }
  });

  $('.' + frm.doc.name + '-pre-execution-details-select-all').click(
    function () {
      if (this.checked) {
        $('.' + frm.doc.name + '-pre-execution-details-select-row').each(
          (index, element) => {
            if ($(element).is(':hidden') === false) {
              $(element).prop('checked', true);
            }
          },
        );
      } else {
        $('.' + frm.doc.name + '-pre-execution-details-select-row').prop(
          'checked',
          false,
        );
      }
    },
  );
}

function addEmailEvent(frm) {
  $('.' + frm.doc.name + '-common_documents-email').unbind();
  $('.' + frm.doc.name + '-common_documents-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).common_documents;
    const documentCodes = [];
    $(`.${frm.doc.name}-common_documents-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('common_documents', documentCodes);
  });

  $('.' + frm.doc.name + '-immovable-email').unbind();
  $('.' + frm.doc.name + '-immovable-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).immovable_asset;
    const documentCodes = [];
    $(`.${frm.doc.name}-immovable-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('immovable_asset', documentCodes);
  });

  $('.' + frm.doc.name + '-movable-email').unbind();
  $('.' + frm.doc.name + '-movable-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).movable_asset;
    const documentCodes = [];
    $(`.${frm.doc.name}-movable-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('movable_asset', documentCodes);
  });

  $('.' + frm.doc.name + '-pledge-email').unbind();
  $('.' + frm.doc.name + '-pledge-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).pledge;
    const documentCodes = [];
    $(`.${frm.doc.name}-pledge-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('pledge', documentCodes);
  });

  $('.' + frm.doc.name + '-personal-guarantee-email').unbind();
  $('.' + frm.doc.name + '-personal-guarantee-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).personal_guarantee;
    const documentCodes = [];
    $(`.${frm.doc.name}-personal-guarantee-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('personal_guarantee', documentCodes);
  });

  $('.' + frm.doc.name + '-corporate-guarantee-email').unbind();
  $('.' + frm.doc.name + '-corporate-guarantee-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).corporate_guarantee;
    const documentCodes = [];
    $(`.${frm.doc.name}-corporate-guarantee-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('corporate_guarantee', documentCodes);
  });

  $('.' + frm.doc.name + '-ndu-email').unbind();
  $('.' + frm.doc.name + '-ndu-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).ndu;
    const documentCodes = [];
    $(`.${frm.doc.name}-ndu-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('ndu', documentCodes);
  });

  $('.' + frm.doc.name + '-intangible-email').unbind();
  $('.' + frm.doc.name + '-intangible-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).intangible_asset;
    const documentCodes = [];
    $(`.${frm.doc.name}-intangible-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('intangible_asset', documentCodes);
  });

  $('.' + frm.doc.name + '-motor-email').unbind();
  $('.' + frm.doc.name + '-motor-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).motor_vehicle;
    const documentCodes = [];
    $(`.${frm.doc.name}-motor-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('motor_vehicle', documentCodes);
  });

  $('.' + frm.doc.name + '-financial-email').unbind();
  $('.' + frm.doc.name + '-financial-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).financial_asset;
    const documentCodes = [];
    $(`.${frm.doc.name}-financial-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('financial_asset', documentCodes);
  });

  $('.' + frm.doc.name + '-assignment-of-rights-email').unbind();
  $('.' + frm.doc.name + '-assignment-of-rights-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).assignment_of_rights;
    const documentCodes = [];
    $(`.${frm.doc.name}-assignment-of-rights-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('assignment_of_rights', documentCodes);
  });

  $('.' + frm.doc.name + '-current-email').unbind();
  $('.' + frm.doc.name + '-current-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).current_asset;
    const documentCodes = [];
    $(`.${frm.doc.name}-current-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('current_asset', documentCodes);
  });

  $('.' + frm.doc.name + '-dsra-email').unbind();
  $('.' + frm.doc.name + '-dsra-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).debt_service_reserve_account;
    const documentCodes = [];
    $(`.${frm.doc.name}-dsra-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('debt_service_reserve_account', documentCodes);
  });

  $('.' + frm.doc.name + '-others-email').unbind();
  $('.' + frm.doc.name + '-others-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).others;
    const documentCodes = [];
    $(`.${frm.doc.name}-others-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('others', documentCodes);
  });

  $('.' + frm.doc.name + '-pre-execution-details-email').unbind();
  $('.' + frm.doc.name + '-pre-execution-details-email').click(function () {
    attachments = [];
    const asset = getAssetValues(frm).pre_execution_details;
    const documentCodes = [];
    $(`.${frm.doc.name}-pre-execution-details-select-row`).each(function (i) {
      if (this.checked) {
        attachments.push({
          s3_key: asset[i].s3_key,
          file_name: asset[i].file_name,
          name: asset[i].file_name,
        });
        documentCodes.push(asset[i].document_code);
      }
    });
    emailDialog('pre_execution_details', documentCodes);
  });
}

async function preExecutionS3Upload(frm, assetDetails, assetName, detailsKey) {
  frappe.dom.freeze();
  const parsedassetDetails = JSON.parse(assetDetails);
  let row;
  let index;
  for (let i = 0; i < files_upload[assetName].length; i++) {
    index = files_upload[assetName][i].s3ButtonIndex;
    row = parsedassetDetails[index];
    const file = files_upload[assetName][i].file;
    delete row.file;
    await uploadAssetFile(row, frm, file, assetName, detailsKey);
  }
  files_upload[assetName] = [];
  frm.reload_doc().then(() => {
    loadAssetTables(frm, assetName);
  });

  frappe.show_alert(
    {
      message: 'Uploaded',
      indicator: 'green',
    },
    5,
  );
}

function uploadAssetFile(row, frm, file, assetName, detailsKey) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    const api =
      '/api/method/trusteeship_platform.custom_methods.pre_execution_and_exception_tracking_s3_upload';
    const formData = new FormData();
    formData.append('file', file);
    formData.append('file_name', file.name);
    formData.append('row', JSON.stringify(row));
    formData.append('docname', frm.doc.name);
    formData.append('doctype', frm.doc.doctype);
    formData.append('assetName', assetName);
    formData.append('detailsKey', detailsKey);
    xhr.open('POST', api, true);
    xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
    xhr.onreadystatechange = function (e) {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          frappe.dom.unfreeze();
          resolve(1);
        } else {
          frappe.dom.unfreeze();
          reject(xhr.statusText);
        }
      } else {
        frappe.dom.unfreeze();
        try {
          const response = JSON.parse(xhr.responseText);
          const errorMessage = JSON.parse(response?._server_messages);
          if (errorMessage) {
            frappe.msgprint(errorMessage, 'Error');
          }
        } catch (error) {
          if (xhr.status === 413) {
            displayHtmlDialog();
          }
        }
      }
    };
    xhr.send(formData);
  });
}

function displayHtmlDialog() {
  if (frappe.boot.max_file_size) {
    frappe.msgprint({
      title: __('File too big'),
      indicator: 'red',
      message: __(
        'File size exceeded the maximum allowed size of ' +
          bytesToMB(frappe.boot.max_file_size),
      ),
    });
  }
}

function bytesToMB(bytes) {
  if (bytes === 0) return '0 MB';

  const mb = bytes / (1024 * 1024);
  return `${mb.toFixed(0)} MB`;
}

function rowDialog(frm, defaultValues = {}, assetName, index = null) {
  const d = new frappe.ui.Dialog({
    title: index !== null ? 'Edit' : 'Add',
    fields: getAssetFields(defaultValues, index === null),
    primary_action_label: 'Confirm',
    primary_action: values => {
      if (index === null) {
        validateDocumentCode(frm, values, assetName);
      }
      if (
        frm.doc.product_name.toUpperCase() === 'STE' ||
        frm.doc.product_name.toUpperCase() === 'DTE'
      ) {
        const immovableAsset = getAssetValues(frm).immovable_asset;
        const movableAsset = getAssetValues(frm).movable_asset;
        const pledge = getAssetValues(frm).pledge;
        const personalGuarantee = getAssetValues(frm).personal_guarantee;
        const corporateGuarantee = getAssetValues(frm).corporate_guarantee;
        const ndu = getAssetValues(frm).ndu;
        const intangibleAsset = getAssetValues(frm).intangible_asset;
        const motorVehicle = getAssetValues(frm).motor_vehicle;
        const financialAsset = getAssetValues(frm).financial_asset;
        const assignmentOfRights = getAssetValues(frm).assignment_of_rights;
        const currentAsset = getAssetValues(frm).current_asset;
        const debtServiceReserveAccount =
          getAssetValues(frm).debt_service_reserve_account;
        const others = getAssetValues(frm).others;
        const commonDocuments = getAssetValues(frm).common_documents;

        if (assetName === 'immovable_asset') {
          index !== null
            ? (immovableAsset[index] = values)
            : immovableAsset.push(values);
        } else if (assetName === 'movable_asset') {
          index !== null
            ? (movableAsset[index] = values)
            : movableAsset.push(values);
        } else if (assetName === 'pledge') {
          index !== null ? (pledge[index] = values) : pledge.push(values);
        } else if (assetName === 'personal_guarantee') {
          index !== null
            ? (personalGuarantee[index] = values)
            : personalGuarantee.push(values);
        } else if (assetName === 'corporate_guarantee') {
          index !== null
            ? (corporateGuarantee[index] = values)
            : corporateGuarantee.push(values);
        } else if (assetName === 'ndu') {
          index !== null ? (ndu[index] = values) : ndu.push(values);
        } else if (assetName === 'intangible_asset') {
          index !== null
            ? (intangibleAsset[index] = values)
            : intangibleAsset.push(values);
        } else if (assetName === 'motor_vehicle') {
          index !== null
            ? (motorVehicle[index] = values)
            : motorVehicle.push(values);
        } else if (assetName === 'financial_asset') {
          index !== null
            ? (financialAsset[index] = values)
            : financialAsset.push(values);
        } else if (assetName === 'assignment_of_rights') {
          index !== null
            ? (assignmentOfRights[index] = values)
            : assignmentOfRights.push(values);
        } else if (assetName === 'current_asset') {
          index !== null
            ? (currentAsset[index] = values)
            : currentAsset.push(values);
        } else if (assetName === 'debt_service_reserve_account') {
          index !== null
            ? (debtServiceReserveAccount[index] = values)
            : debtServiceReserveAccount.push(values);
        } else if (assetName === 'others') {
          index !== null ? (others[index] = values) : others.push(values);
        } else if (assetName === 'common_documents') {
          index !== null
            ? (commonDocuments[index] = values)
            : commonDocuments.push(values);
        }

        frm.doc.type_of_security_details = JSON.stringify({
          immovable_asset: immovableAsset,
          movable_asset: movableAsset,
          pledge,
          personal_guarantee: personalGuarantee,
          corporate_guarantee: corporateGuarantee,
          ndu,
          intangible_asset: intangibleAsset,
          motor_vehicle: motorVehicle,
          financial_asset: financialAsset,
          assignment_of_rights: assignmentOfRights,
          current_asset: currentAsset,
          debt_service_reserve_account: debtServiceReserveAccount,
          others,
          common_documents: commonDocuments,
        });
      } else if (
        frm.doc.product_name.toUpperCase() === 'EA' ||
        frm.doc.product_name.toUpperCase() === 'FA' ||
        frm.doc.product_name.toUpperCase() === 'INVIT' ||
        frm.doc.product_name.toUpperCase() === 'REIT'
      ) {
        const preExecutionDetails = getAssetValues(frm).pre_execution_details
          ? getAssetValues(frm).pre_execution_details
          : [];
        index !== null
          ? (preExecutionDetails[index] = values)
          : preExecutionDetails.push(values);
        frm.doc.pre_execution_details = JSON.stringify({
          pre_execution_details: preExecutionDetails,
        });
      }

      d.hide();
      frm.dirty();
      frm.save().then(() => {
        moveToTrackException(frm, values, assetName);
        loadAssetTables(frm, assetName);
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  if (assetName === 'common_documents') {
    d.fields_dict.document_code.get_query = function () {
      return {
        filters: [
          ['Canopi Document Code', 'common_documents_checklist', '=', 1],
        ],
      };
    };
  } else {
    d.fields_dict.document_code.get_query = function () {
      return {
        filters: [
          ['Canopi Document Code', 'pre_execution_checklist', '=', 1],
          ['Canopi Document Code', 'product', '=', frm.doc.product_name],
        ],
      };
    };
  }
}

function validateDocumentCode(frm, dialogValues, assetName) {
  const assetValues = getAssetValues(frm)[assetName]
    ? getAssetValues(frm)[assetName]
    : [];

  if (assetValues.some(x => x.document_code === dialogValues.document_code)) {
    frappe.throw('<b>Document Code</b> must be unique.');
  }
}

function moveToTrackException(frm, row, assetName) {
  if (row.track_exception === 'Yes' && frm.doc.docstatus === 0) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.move_to_track_exception',
      args: { docname: frm.doc.name, row, append_to: assetName },
      freeze: true,
      callback: r => {},
    });
  }
}

function getAssetFields(doc, isNew = false) {
  const fields = [
    {
      label: 'Document Code',
      fieldname: 'document_code',
      fieldtype: 'Link',
      options: 'Canopi Document Code',
      default: doc.document_code,
      read_only: isNew ? 0 : 1,
      reqd: 1,
      onchange: () => {
        if (cur_dialog?.get_field('document_code')?.value) {
          frappe.call({
            method: 'trusteeship_platform.custom_methods.get_doc',
            args: {
              doc_name: cur_dialog.get_field('document_code').value,
              doc_type: 'Canopi Document Code',
            },
            freeze: true,
            callback: r => {
              cur_dialog.get_field('description').value = r.message.description;
              cur_dialog.refresh();
            },
          });
        }
      },
    },

    {
      label: 'Description',
      fieldname: 'description',
      fieldtype: 'Data',
      default: doc.description ? doc.description : ' ',
      read_only: isNew ? 0 : 1,
      reqd: isNew ? 1 : 0,
    },

    {
      label: 'Client Upload',
      fieldname: 'client_upload_yes_no',
      fieldtype: 'Select',
      options: 'Yes\nNo',
      default: doc.client_upload_yes_no ? doc.client_upload_yes_no : 'No',
    },

    {
      label: 'ATSL Upload',
      fieldname: 'atsl_upload_yes_no',
      fieldtype: 'Select',
      options: 'Yes\nNo',
      default: doc.atsl_upload_yes_no ? doc.atsl_upload_yes_no : 'No',
    },
    {
      label: 'Public',
      fieldname: 'public',
      fieldtype: 'Select',
      options: ' \nYes\nNo',
      default: doc.public ? doc.public : ' ',
    },
    {
      label: 'Upload Date',
      fieldname: 'upload_date',
      fieldtype: 'Date',
      read_only: 1,
      default: doc.upload_date ? doc.upload_date : null,
    },
    {
      label: 'Comments / Justification',
      fieldname: 'comments',
      fieldtype: 'Small Text',
      mandatory_depends_on:
        "eval:doc.retain_waive=='Waive' || doc.track_exception == 'Yes';",
      default: doc.comments ? doc.comments : ' ',
    },
    {
      label: 'Track Exception',
      fieldname: 'track_exception',
      fieldtype: 'Select',
      options: 'Yes\nNo',
      default: doc.track_exception ? doc.track_exception : 'No',
      onchange: e => {
        if (
          e.target.value === 'Yes' &&
          cur_dialog.get_field('retain_waive').value !== 'Retain'
        ) {
          cur_dialog.get_field('track_exception').value = 'No';
          cur_dialog.refresh();
          frappe.throw(
            'The document cannot be tracked for exception if it is waived.',
          );
        }
      },
    },
    {
      label: 'Due Date',
      fieldname: 'due_date',
      depends_on: "eval: doc.track_exception == 'Yes';",
      fieldtype: 'Date',
      default: doc.due_date ? doc.due_date : null,
      mandatory_depends_on: "eval:doc.track_exception == 'Yes';",
    },
    {
      label: 'Retain/Waive',
      fieldname: 'retain_waive',
      fieldtype: 'Select',
      options: 'Retain\nWaive',
      default: doc.retain_waive ? doc.retain_waive : 'Retain',
      onchange: e => {
        if (e.target.value === 'Waive') {
          if (cur_dialog.get_field('track_exception').value === 'Yes') {
            cur_dialog.get_field('retain_waive').value = 'Retain';
            cur_dialog.refresh();
            frappe.throw(
              'The document cannot be tracked for exception if it is waived.',
            );
          } else {
            cur_dialog.get_field('approval_required').value = 'Yes';
            cur_dialog.refresh();
          }
        }
      },
    },
    {
      label: 'Approval Required',
      fieldname: 'approval_required',
      fieldtype: 'Select',
      options: 'Yes\nNo',
      read_only_depends_on: "eval: doc.retain_waive == 'Waive'",
      default: doc.approval_required ? doc.approval_required : 'No',
    },
    {
      label: 'Send Email',
      fieldname: 'send_email',
      fieldtype: 'Select',
      options: 'Yes\nNo',
      default: doc.send_email ? doc.send_email : 'No',
      read_only: 1,
    },
    {
      label: 'S3 Key',
      fieldname: 's3_key',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.s3_key ? doc.s3_key : '',
    },
    {
      label: 'File Name',
      fieldname: 'file_name',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.file_name ? doc.file_name : '',
    },
    {
      label: 'File',
      fieldname: 'file',
      fieldtype: 'Small Text',
      hidden: 1,
      default: doc.file ? doc.file : '',
    },
    {
      label: 'Standard Template Name',
      fieldname: 'standard_template_name',
      fieldtype: 'Data',
      hidden: 1,
      default: doc.standard_template_name ? doc.standard_template_name : '',
    },
    {
      label: 'Standard Template S3 Key',
      fieldname: 'standard_template_s3_key',
      fieldtype: 'Small Text',
      hidden: 1,
      default: doc.standard_template_s3_key ? doc.standard_template_s3_key : '',
    },
  ];

  return fields;
}

function scrollToField(frm, fieldName) {
  if (fieldName === 'immovable_asset') {
    frm.fields_dict.immovable_asset_section.collapse(false);
    frm.scroll_to_field('immovable_asset_html');
  } else if (fieldName === 'movable_asset') {
    frm.fields_dict.movable_asset_section.collapse(false);
    frm.scroll_to_field('movable_asset_html');
  } else if (fieldName === 'pledge') {
    frm.fields_dict.pledge_section.collapse(false);
    frm.scroll_to_field('pledge_html');
  } else if (fieldName === 'personal_guarantee') {
    frm.fields_dict.personal_guarantee_section.collapse(false);
    frm.scroll_to_field('personal_guarantee_html');
  } else if (fieldName === 'corporate_guarantee') {
    frm.fields_dict.corporate_guarantee_section.collapse(false);
    frm.scroll_to_field('corporate_guarantee_html');
  } else if (fieldName === 'ndu') {
    frm.fields_dict.ndu_section.collapse(false);
    frm.scroll_to_field('ndu_html');
  } else if (fieldName === 'intangible_asset') {
    frm.fields_dict.intangible_asset_section.collapse(false);
    frm.scroll_to_field('intangible_asset_html');
  } else if (fieldName === 'motor_vehicle') {
    frm.fields_dict.motor_vehicle_section.collapse(false);
    frm.scroll_to_field('motor_vehicle_html');
  } else if (fieldName === 'financial_asset') {
    frm.fields_dict.financial_asset_section.collapse(false);
    frm.scroll_to_field('financial_asset_html');
  } else if (fieldName === 'assignment_of_rights') {
    frm.fields_dict.assignment_of_rights_section.collapse(false);
    frm.scroll_to_field('assignment_of_rights_html');
  } else if (fieldName === 'current_asset') {
    frm.fields_dict.current_asset_section.collapse(false);
    frm.scroll_to_field('current_asset_html');
  } else if (fieldName === 'debt_service_reserve_account') {
    frm.fields_dict.debt_service_reserve_account_section.collapse(false);
    frm.scroll_to_field('debt_service_reserve_account_html');
  } else if (fieldName === 'others') {
    frm.fields_dict.others_section.collapse(false);
    frm.scroll_to_field('others_html');
  } else if (fieldName === 'pre_execution_details') {
    frm.fields_dict.pre_execution_details_section.collapse(false);
    frm.scroll_to_field('pre_execution_details_html');
  }
}

function emptyHtmlFields(frm) {
  $(frm.fields_dict.immovable_asset_html.wrapper).empty();
  $(frm.fields_dict.movable_asset_html.wrapper).empty();
  $(frm.fields_dict.pledge_html.wrapper).empty();
  $(frm.fields_dict.personal_guarantee_html.wrapper).empty();
  $(frm.fields_dict.corporate_guarantee_html.wrapper).empty();
  $(frm.fields_dict.ndu_html.wrapper).empty();
  $(frm.fields_dict.intangible_asset_html.wrapper).empty();
  $(frm.fields_dict.motor_vehicle_html.wrapper).empty();
  $(frm.fields_dict.financial_asset_html.wrapper).empty();
  $(frm.fields_dict.assignment_of_rights_html.wrapper).empty();
  $(frm.fields_dict.current_asset_html.wrapper).empty();
  $(frm.fields_dict.debt_service_reserve_account_html.wrapper).empty();
  $(frm.fields_dict.others_html.wrapper).empty();

  $(frm.fields_dict.pre_execution_details_html.wrapper).empty();
}

function getAssetValues(frm) {
  if (
    frm.doc.product_name.toUpperCase() === 'DTE' ||
    frm.doc.product_name.toUpperCase() === 'STE'
  ) {
    frm.doc.type_of_security_details = frm.doc.type_of_security_details
      ? frm.doc.type_of_security_details
      : JSON.stringify({});
    const details =
      frm.doc.type_of_security_details !== undefined
        ? JSON.parse(frm.doc.type_of_security_details)
        : {};
    const immovableAsset = details.immovable_asset;
    const movableAsset = details.movable_asset;
    const pledge = details.pledge;
    const personalGuarantee = details.personal_guarantee;
    const corporateGuarantee = details.corporate_guarantee;
    const ndu = details.ndu;
    const intangibleAsset = details.intangible_asset;
    const motorVehicle = details.motor_vehicle;
    const financialAsset = details.financial_asset;
    const assignmentOfRights = details.assignment_of_rights;
    const currentAsset = details.current_asset;
    const debtServiceReserveAccount = details.debt_service_reserve_account;

    const others = details.others;
    const commonDocuments = details.common_documents;

    return {
      immovable_asset: immovableAsset,
      movable_asset: movableAsset,
      pledge,
      personal_guarantee: personalGuarantee,
      corporate_guarantee: corporateGuarantee,
      ndu,
      intangible_asset: intangibleAsset,
      motor_vehicle: motorVehicle,
      financial_asset: financialAsset,
      assignment_of_rights: assignmentOfRights,
      current_asset: currentAsset,
      debt_service_reserve_account: debtServiceReserveAccount,
      others,
      common_documents: commonDocuments,
    };
  } else if (
    frm.doc.product_name.toUpperCase() === 'EA' ||
    frm.doc.product_name.toUpperCase() === 'FA' ||
    frm.doc.product_name.toUpperCase() === 'INVIT' ||
    frm.doc.product_name.toUpperCase() === 'REIT'
  ) {
    return frm.doc.pre_execution_details
      ? JSON.parse(frm.doc.pre_execution_details)
      : {};
  } else {
    return {};
  }
}

function deleteMultipleFromS3(keys) {
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_multiple_from_s3',
    args: { keys },
  });
}

// email dialig
const dialog = new frappe.ui.Dialog({
  title: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
  no_submit_on_enter: true,
  fields: getEmailFields(),
  secondary_action_label: 'Discard',
  secondary_action() {
    dialog.hide();
  },
  size: 'large',
  minimizable: true,
});
function emailDialog(assetName, documentCodes) {
  const primaryButton = dialog.get_primary_btn();
  primaryButton.remove();
  const newPrimaryButton = document.createElement('BUTTON');
  newPrimaryButton.classList.add(
    'btn',
    'btn-primary',
    'btn-sm',
    'btn-modal-primary',
  );
  dialog.standard_actions.append(newPrimaryButton);
  dialog.set_primary_action('Send', () => {
    sendAction(assetName, documentCodes);
  });

  $(dialog.$wrapper.find('.form-section').get(0)).addClass('to_section');
  prepare();
  dialog.show();
}

function prepare() {
  setupMultiselectQueries();
  setupAttach();
  setupEmail();
  setupEmailTemplate();
}

function setupMultiselectQueries() {
  ['recipients', 'cc', 'bcc'].forEach(field => {
    dialog.fields_dict[field].get_data = () => {
      const data = dialog.fields_dict[field].get_value();
      const txt = data.match(/[^,\s*]*$/)[0] || '';

      frappe.call({
        method: 'frappe.email.get_contact_list',
        args: { txt },
        callback: r => {
          dialog.fields_dict[field].set_data(r.message);
        },
      });
    };
  });
}

let attachments = [];

function setupAttach() {
  const fields = dialog.fields_dict;
  const attach = $(fields.select_attachments.wrapper);

  if (!attachments) {
    attachments = [];
  }

  let args = {
    folder: 'Home/Attachments',
    on_success: attachment => {
      attachments.push(attachment);
      renderAttachmentRows(attachment);
    },
  };

  if (cur_frm) {
    args = {
      doctype: cur_frm.doctype,
      docname: cur_frm.docname,
      folder: 'Home/Attachments',
      on_success: attachment => {
        cur_frm.attachments.attachment_uploaded(attachment);
        renderAttachmentRows(attachment);
      },
    };
  }

  $(`
  <label class="control-label">
    ${'Attachments'}
  </label>
  <div class='attach-list'></div>
  <p class='add-more-attachments'>
    <button class='btn btn-xs btn-default'>
      ${frappe.utils.icon('small-add', 'xs')}&nbsp;
      ${'Add Attachment'}
    </button>
  </p>
`).appendTo(attach.empty());

  attach
    .find('.add-more-attachments button')
    .on('click', () => new frappe.ui.FileUploader(args));
  renderAttachmentRows();
}

function renderAttachmentRows(attachment = null) {
  const selectAttachments = dialog.fields_dict.select_attachments;
  const attachmentRows = $(selectAttachments.wrapper).find('.attach-list');
  if (attachment) {
    attachmentRows.append(getAttachmentRow(attachment, true));
  } else {
    let files = [];
    if (attachments && attachments.length) {
      files = files.concat(attachments);
    }
    if (cur_frm) {
      files = files.concat(cur_frm.get_files());
    }

    if (files.length) {
      $.each(files, (i, f) => {
        if (!f.file_name) return;
        if (!attachmentRows.find(`[data-file-name="${f.name}"]`).length) {
          if (f.file_url) {
            f.file_url = frappe.urllib.get_full_url(f.file_url);
          }
          attachmentRows.append(getAttachmentRow(f, true));
        }
      });
    }
  }
  addEmailAttachDownloadEvent();
}

function addEmailAttachDownloadEvent() {
  setTimeout(() => {
    $('.email-attachment-download').unbind();
    $('.email-attachment-download').on('click', function () {
      const index = $('.email-attachment-download').index(this);
      const element = $(dialog.wrapper).find('[data-file-key]')[index];
      const s3Key = $(element).attr('data-file-key');
      if (s3Key) {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: s3Key,
        });
      }
    });
  }, 200);
}

function getAttachmentRow(attachment, checked = null) {
  const html =
    `
  <p class="checkbox flex">
    <label class="ellipsis" title="${attachment.file_name}">
      <input
        type="checkbox"` +
    (attachment.s3_key !== undefined
      ? `data-file-key="${attachment.s3_key}" disabled`
      : `data-file-name="${attachment.name}"`) +
    `
        ${checked ? 'checked' : ''}>
      </input>
      <span class="ellipsis">${attachment.file_name}</span>
    </label>
    &nbsp;` +
    (attachment.s3_key !== undefined
      ? `<a class="btn-linkF email-attachment-download">
          ${frappe.utils.icon('link-url')}
        </a>
        <a hidden href="${
          attachment.file_url
        }" target="_blank" class="btn-linkF">
          ${frappe.utils.icon('link-url')}
        </a>`
      : `<a href="${attachment.file_url}" target="_blank" class="btn-linkF">
          ${frappe.utils.icon('link-url')}
        </a>
        <a hidden class="btn-linkF email-attachment-download">
          ${frappe.utils.icon('link-url')}
        </a>`) +
    '</p>';
  return $(html);
}

function setupEmail() {
  // email
  const fields = dialog.fields_dict;

  $(fields.send_me_a_copy.input).on('click', () => {
    // update send me a copy (make it sticky)
    const val = fields.send_me_a_copy.get_value();
    frappe.db.set_value('User', frappe.session.user, 'send_me_a_copy', val);
    frappe.boot.user.send_me_a_copy = val;
  });
}

function setupEmailTemplate() {
  dialog.fields_dict.email_template.df.onchange = () => {
    const emailTemplate = dialog.fields_dict.email_template.get_value();
    if (!emailTemplate) return;

    frappe.call({
      method:
        'frappe.email.doctype.email_template.email_template.get_email_template',
      args: {
        template_name: emailTemplate,
        doc: cur_frm.doc,
        _lang: dialog.get_value('language_sel'),
      },
      callback(r) {
        prependReply(r.message, emailTemplate);
      },
    });
  };
}

let replyAdded = '';
function prependReply(reply, emailTemplate) {
  if (replyAdded === emailTemplate) return;
  const contentField = dialog.fields_dict.content;
  const subjectField = dialog.fields_dict.subject;

  let content = contentField.get_value() || '';
  content = content.split('<!-- salutation-ends -->')[1] || content;

  contentField.set_value(`${reply.message}<br>${content}`);
  subjectField.set_value(reply.subject);

  replyAdded = emailTemplate;
}

function sendAction(assetName, documentCodes) {
  const formValues = getEmailFormValues();
  if (!formValues) return;

  const selectedAttachments = { s3_key: [], file_url: [] };

  $.map($(dialog.wrapper).find('[data-file-key]:checked'), function (element) {
    selectedAttachments.s3_key.push($(element).attr('data-file-key'));
  });

  $.map($(dialog.wrapper).find('[data-file-name]:checked'), function (element) {
    selectedAttachments.file_url.push($(element).attr('data-file-name'));
  });
  sendEmail(formValues, selectedAttachments, assetName, documentCodes);
}

function getEmailFormValues() {
  const formValues = dialog.get_values();

  // cc
  for (let i = 0, l = dialog.fields.length; i < l; i++) {
    const df = dialog.fields[i];

    if (df.is_cc_checkbox) {
      // concat in cc
      if (formValues[df.fieldname]) {
        formValues.cc =
          (formValues.cc ? formValues.cc + ', ' : '') + df.fieldname;
        formValues.bcc =
          (formValues.bcc ? formValues.bcc + ', ' : '') + df.fieldname;
      }

      delete formValues[df.fieldname];
    }
  }

  return formValues;
}

function sendEmail(formValues, selectedAttachments, assetName, documentCodes) {
  dialog.hide();
  if (!formValues.recipients) {
    frappe.msgprint('Enter Email Recipient(s)');
    return;
  }

  if (cur_frm && !frappe.model.can_email(cur_frm.doc.doctype, cur_frm)) {
    frappe.msgprint(
      'You are not allowed to send emails related to this document',
    );
    return;
  }
  frappe.call({
    method:
      'trusteeship_platform.custom_methods.pre_execution_checklist_send_mail',
    args: {
      recipients: formValues.recipients,
      cc: formValues.cc,
      bcc: formValues.bcc,
      subject: formValues.subject,
      content: formValues.content,
      send_me_a_copy: formValues.send_me_a_copy,
      attachments: selectedAttachments,
      read_receipt: formValues.send_read_receipt,
      sender: formValues.sender,
    },
    freeze: true,
    freeze_message: 'Sending',

    callback: r => {
      if (!r.exc) {
        frappe.utils.play_sound('email');

        if (r.message?.emails_not_sent_to) {
          frappe.msgprint(
            ('Email not sent to {0} (unsubscribed / disabled)',
            [frappe.utils.escape_html(r.message.emails_not_sent_to)]),
          );
        }

        if (cur_frm) {
          if (
            cur_frm.doc.product_name.toUpperCase() === 'DTE' ||
            cur_frm.doc.product_name.toUpperCase() === 'STE'
          ) {
            cur_frm.doc.type_of_security_details = cur_frm.doc
              .type_of_security_details
              ? cur_frm.doc.type_of_security_details
              : JSON.stringify({});

            const details =
              cur_frm.doc.type_of_security_details !== undefined
                ? JSON.parse(cur_frm.doc.type_of_security_details)
                : {};

            documentCodes.forEach(element => {
              details[assetName][
                details[assetName].findIndex(el => el.document_code === element)
              ].send_email = 'Yes';
            });

            cur_frm.doc.type_of_security_details = JSON.stringify(details);
          } else if (
            cur_frm.doc.product_name.toUpperCase() === 'EA' ||
            cur_frm.doc.product_name.toUpperCase() === 'FA' ||
            cur_frm.doc.product_name.toUpperCase() === 'INVIT' ||
            cur_frm.doc.product_name.toUpperCase() === 'REIT'
          ) {
            cur_frm.doc.pre_execution_details = cur_frm.doc
              .pre_execution_details
              ? cur_frm.doc.pre_execution_details
              : JSON.stringify({});

            const details =
              cur_frm.doc.pre_execution_details !== undefined
                ? JSON.parse(cur_frm.doc.pre_execution_details)
                : {};
            documentCodes.forEach(element => {
              details[assetName][
                details[assetName].findIndex(el => el.document_code === element)
              ].send_email = 'Yes';
            });

            cur_frm.doc.pre_execution_details = JSON.stringify(details);
          }

          cur_frm.dirty();
          cur_frm.save().then(() => {
            loadAssetTables(cur_frm, assetName);
          });
        }
      } else {
        frappe.msgprint(
          'There were errors while sending email. Please try again.',
        );
      }
    },
  });
}

function getEmailFields() {
  const fields = [
    {
      label: 'To',
      fieldtype: 'MultiSelect',
      reqd: 0,
      fieldname: 'recipients',
    },
    {
      fieldtype: 'Button',
      label: frappe.utils.icon('down'),
      fieldname: 'option_toggle_button',
      click: () => {
        toggleMoreOptions(false);
      },
    },
    {
      fieldtype: 'Section Break',
      hidden: 1,
      fieldname: 'more_options',
    },
    {
      label: 'CC',
      fieldtype: 'MultiSelect',
      fieldname: 'cc',
    },
    {
      label: 'BCC',
      fieldtype: 'MultiSelect',
      fieldname: 'bcc',
    },
    {
      label: 'Email Template',
      fieldtype: 'Link',
      options: 'Email Template',
      fieldname: 'email_template',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Subject',
      fieldtype: 'Data',
      reqd: 1,
      fieldname: 'subject',
      default: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
      length: 524288,
    },
    {
      label: 'Message',
      fieldtype: 'Text Editor',
      fieldname: 'content',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Send me a copy',
      fieldtype: 'Check',
      fieldname: 'send_me_a_copy',
      default: frappe.boot.user.send_me_a_copy,
    },
    {
      label: 'Send Read Receipt',
      fieldtype: 'Check',
      fieldname: 'send_read_receipt',
    },

    { fieldtype: 'Column Break' },
    {
      label: 'Select Attachments',
      fieldtype: 'HTML',
      fieldname: 'select_attachments',
    },
  ];

  // add from if user has access to multiple email accounts
  const emailAccounts = frappe.boot.email_accounts.filter(account => {
    return (
      !in_list(
        ['All Accounts', 'Sent', 'Spam', 'Trash'],
        account.email_account,
      ) && account.enable_outgoing
    );
  });

  if (emailAccounts.length) {
    fields.unshift({
      label: 'From',
      fieldtype: 'Select',
      reqd: 1,
      fieldname: 'sender',
      options: emailAccounts.map(function (e) {
        return e.email_id;
      }),
    });
  }

  return fields;
}
function toggleMoreOptions(showOptions) {
  showOptions = showOptions || dialog.fields_dict.more_options.df.hidden;
  dialog.set_df_property('more_options', 'hidden', !showOptions);

  const label = frappe.utils.icon(showOptions ? 'up-line' : 'down');
  dialog.get_field('option_toggle_button').set_label(label);
}

function titleToSnakeCase(str) {
  if (!str) return;

  return str
    .split(' ')
    .map(word => word.toLowerCase())
    .join('_');
}

function removeParenthesis(str) {
  if (!str) return;
  return str.replace(/ *\([^)]*\) */g, '');
}

function redirectDoctype(frm, doctype, response) {
  if (response.message[doctype]) {
    const outputString = doctype
      .replace(/([a-z0-9])([A-Z])/g, '$1 $2') // Insert space before capital letters if not already separated
      .toLowerCase() // Convert the entire string to lowercase
      .replace(/\s+/g, '-'); // Replace spaces with hyphens
    window.open(
      `/app/${outputString}/${response.message[doctype].find(x => x).name}`,
    );
  } else {
    frappe.model.with_doctype(doctype, function () {
      const source = frm.doc;
      const targetDoc = frappe.model.get_new_doc(doctype);

      targetDoc.operations = source.operations;
      targetDoc.person_name = source.person_name;
      targetDoc.cnp_customer_code = source.cnp_customer_code;
      targetDoc.mandate_name = source.mandate_name;
      targetDoc.opportunity = source.opportunity;
      targetDoc.product_name = source.product_name;
      if (
        doctype === 'Canopi Post Execution Checklist' ||
        doctype === 'Canopi Pre Execution Checklist' ||
        doctype === 'Canopi Documentation'
      ) {
        targetDoc.ops_servicing_rm = source.ops_servicing_rm;
        targetDoc.tl_representative = source.tl_representative;
      }
      frappe.ui.form.make_quick_entry(doctype, null, null, targetDoc);
    });
  }
}

async function send_annexure_a_approval(frm) {
  await frappe
    .call({
      method:
        // eslint-disable-next-line max-len
        'trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.validate_annexure_a_approval',
      args: {
        docname: frm.doc.name,
      },
      freeze: true,
    })
    .then(r => {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.create_approval',
        args: {
          docname: frm.doc.name,
          is_annexure_a: 1,
        },
        freeze: true,
        callback: r => {
          frm.reload_doc().then(() => {
            frappe.set_route('Form', 'Canopi Approval', r.message);
          });
        },
      });
    });
}

function configure_annexure_a_approval_btn(frm) {
  if (
    frm.doc.annexure_a_status !== 'Draft' &&
    !frm.doc.annexure_a_status?.includes('Rejected')
  ) {
    frm.set_df_property('annexure_a_approval', 'hidden', 1);
  } else {
    frm.set_df_property('annexure_a_approval', 'hidden', 0);
  }
}

function configure_attach_annexure_a_btn(frm) {
  if (frm.doc.annexure_a_s3_key) {
    $('button[data-fieldname="attach_annexure_a"]').addClass('btn-primary');
    frm.set_df_property(
      'attach_annexure_a',
      'label',
      'Re Attach Signed Annexure A',
    );

    const deleteIcon = document.createElement('button');
    deleteIcon.classList.add(
      'btn',
      'btn-danger',
      'btn-xs',
      'btn-remove-perm',
      'delete_annexure_a',
    );
    deleteIcon.innerHTML = frappe.utils.icon('delete', 'sm');
    deleteIcon.style.margin = '5px';

    const attach_btn = $("button[data-fieldname='attach_annexure_a']");

    deleteIcon.addEventListener('click', function () {
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.delete_annexure_a_from_s3',
        args: {
          docname: frm.doc.name,
          key: frm.doc.annexure_a_s3_key,
        },
        callback: r => {
          deleteIcon.remove();
          frm.reload_doc();
        },
      });
    });

    if ($('.delete_annexure_a').length === 0) {
      attach_btn.parent().append(deleteIcon);
    }
  } else {
    $('button[data-fieldname="attach_annexure_a"]').removeClass('btn-primary');
    frm.set_df_property(
      'attach_annexure_a',
      'label',
      'Attach Signed Annexure A',
    );
  }
}

function configure_view_attach_annexure_a_btn(frm) {
  frappe.call({
    method:
      // eslint-disable-next-line max-len
      'trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.is_annexure_a_approval_completed',
    args: { docname: frm.doc.name },
    freeze: true,
    callback: r => {
      if (frm.doc.annexure_a_status === 'Approved' && r.message === true) {
        frm.set_df_property('view_annexure_a', 'label', 'Download Annexure A');
        frm.set_df_property('attach_annexure_a', 'hidden', 0);
      } else {
        frm.set_df_property('attach_annexure_a', 'hidden', 1);
      }
    },
  });
}

function upload_annexure_a_to_s3(frm) {
  if (frm.doc.product_name.toUpperCase() === 'DTE') {
    const fileInput = document.createElement('input');
    fileInput.type = 'file';
    fileInput.click();

    fileInput.addEventListener('change', async event => {
      const file = { ...event.target.files };
      const reader = new FileReader();
      reader.readAsDataURL(file[0]);
      if (file) {
        frappe.dom.freeze();
        event.target.value = '';
        const xhr = new XMLHttpRequest();
        const api =
          // eslint-disable-next-line max-len
          '/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.upload_annexure_a_to_s3';
        const formData = new FormData();
        formData.append('file', file[0]);
        formData.append(
          'key',
          `${frm.doc.doctype.replace('Canopi ', '')}/${
            frm.doc.name
          }/Annexure A/${file[0].name}`,
        );
        formData.append('docname', frm.doc.name);
        xhr.open('POST', api, true);
        xhr.setRequestHeader('X-Frappe-CSRF-Token', frappe.csrf_token);
        xhr.onreadystatechange = function (e) {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              frappe.dom.unfreeze();
            } else {
              frappe.dom.unfreeze();
            }
          } else {
            frappe.dom.unfreeze();
            try {
              const response = JSON.parse(xhr.responseText);
              const errorMessage = JSON.parse(response?._server_messages);
              if (errorMessage) {
                frappe.msgprint(errorMessage, 'Error');
              }
            } catch (error) {
              if (xhr.status === 413) {
                displayHtmlDialog();
              }
            }
          }
        };
        xhr.send(formData);
      }
    });
  }
}

function renderDownloadAnnexureABtn(frm) {
  $(frm.fields_dict.download_signed_annexure_a_html.wrapper).empty();
  if (frm.doc.annexure_a_s3_key) {
    $(frm.fields_dict.download_signed_annexure_a_html.wrapper)
      .html(
        frappe.render_template('read_only_link_btn', {
          title: 'Download Attached Annexure A',
          value: frm.doc.annexure_a_s3_key.substring(
            frm.doc.annexure_a_s3_key.lastIndexOf('/') + 1,
          ),
        }),
      )
      .find('a')
      .on('click', function () {
        open_url_post(frappe.request.url, {
          cmd: 'trusteeship_platform.custom_methods.download_s3_file',
          key: frm.doc.annexure_a_s3_key,
        });
      });
  }
}

function approval_button_in_connections(frm) {
  $("button[data-doctype='Canopi Approval']").unbind();
  $("button[data-doctype='Canopi Approval']").addClass('hidden');
  if (frm.doc.annexure_a_status === 'Draft') {
    $("button[data-doctype='Canopi Approval']").removeClass('hidden');
    $("button[data-doctype='Canopi Approval']").on('click', function () {
      send_annexure_a_approval(frm);
    });
  }
}

function fileToBlobAndUrl(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = () => {
      const blob = new Blob([reader.result], { type: file.type });
      const blobUrl = URL.createObjectURL(blob);
      resolve(blobUrl);
    };

    reader.onerror = error => {
      reject(error);
    };

    reader.readAsArrayBuffer(file);
  });
}

function getFileUpload(frm) {
  if (frm.doc.type_of_security_details) {
    const keys = Object.keys(JSON.parse(frm.doc.type_of_security_details));
    keys.forEach(key => {
      files_upload[key] = [];
    });
  }
  if (frm.doc.pre_execution_details) {
    const keys = Object.keys(JSON.parse(frm.doc.pre_execution_details));
    keys.forEach(key => {
      files_upload[key] = [];
    });
  }
}

function addFiles(assetName, file, details, s3ButtonIndex) {
  if (!details[assetName][s3ButtonIndex].file) {
    files_upload[assetName].push({
      file,
      file_name: file.name,
      assetName,
      s3ButtonIndex,
    });
  } else {
    files_upload[assetName] = [];
    files_upload[assetName].push({
      file,
      file_name: file.name,
      assetName,
      s3ButtonIndex,
    });
  }
}

function fileRemove(assetName, index) {
  const foundIndex = files_upload[assetName].findIndex(
    element => element.s3ButtonIndex === index,
  );
  if (foundIndex !== -1) {
    files_upload[assetName].splice(foundIndex, 1);
  }
}
