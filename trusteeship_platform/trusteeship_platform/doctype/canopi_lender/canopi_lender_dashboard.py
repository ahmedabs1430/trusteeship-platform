from frappe import _


def get_data():
    return {
        "heatmap": True,
        "fieldname": "cnp_lender",
        "transactions": [
            {
                "label": _("Loans"),
                "items": ["Loan"],
            },
            {
                "label": _("Bank Details"),
                "items": ["Bank", "Bank Account"],
            },
        ],
    }
