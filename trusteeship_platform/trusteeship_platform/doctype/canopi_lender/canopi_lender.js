// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, erpnext, updateBreadcrumbs, open_url_post */
/* eslint-env jquery */
frappe.ui.form.on('Canopi Lender', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    scrollToTaxDocs(frm);
    renderAddressAndContacts(frm);
    renderTaxDocuments(frm);
    setDefaultBankAccountFilter(frm);
    setDefaultBankFilter(frm);
    showActivities(frm);
    showNotes(frm);
  },

  default_bank: function (frm) {
    setDefaultBankAccountFilter(frm);
  },

  default_bank_account: function (frm) {
    getDefaultBank(frm);
  },
});

function scrollToTaxDocs(frm) {
  setTimeout(() => {
    if (localStorage.getItem('scroll_to_tax_docs') === '1') {
      localStorage.setItem('scroll_to_tax_docs', '0');
      frm.scroll_to_field('tax_documents_html');
    } else {
      const tabsElem = $('.nav-item.show');
      tabsElem[0].lastElementChild.click();
    }
  }, 200);
}

function showNotes(frm) {
  $(frm.fields_dict.notes_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;

  const crmNotes = new erpnext.utils.CRMNotes({
    frm,
    notes_wrapper: $(frm.fields_dict.notes_html.wrapper),
  });
  crmNotes.refresh();

  $('.new-note-btn').unbind();
  $('.new-note-btn').on('click', function () {
    addNote(frm);
  });

  $('.notes-section').find('.edit-note-btn').unbind();
  $('.notes-section')
    .find('.edit-note-btn')
    .on('click', function () {
      editNote(this, frm);
    });

  $('.notes-section').find('.delete-note-btn').unbind();
  $('.notes-section')
    .find('.delete-note-btn')
    .on('click', function () {
      deleteNote(this, frm);
    });
}

function addNote(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Add a Note',
    fields: [
      {
        label: 'Note',
        fieldname: 'note',
        fieldtype: 'Text Editor',
        reqd: 1,
        enable_mentions: true,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_note',
        args: {
          doctype: frm.doctype,
          docname: frm.doc.name,
          note: data.note,
        },
        freeze: true,
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.refresh_fields();
          }
        },
      });
    },
    primary_action_label: 'Add',
  });
  d.show();
}

function editNote(editBtn, frm) {
  const row = $(editBtn).closest('.comment-content');
  const rowId = row.attr('name');
  const rowContent = $(row).find('.content').html();
  if (rowContent) {
    const d = new frappe.ui.Dialog({
      title: 'Edit Note',
      fields: [
        {
          label: 'Note',
          fieldname: 'note',
          fieldtype: 'Text Editor',
          default: rowContent,
        },
      ],
      primary_action: function () {
        const data = d.get_values();
        frappe.call({
          method: 'trusteeship_platform.custom_methods.edit_note',
          args: {
            doctype: frm.doctype,
            docname: frm.doc.name,
            note: data.note,
            rowId,
          },
          freeze: true,
          callback: function (r) {
            if (!r.exc) {
              d.hide();
              frm.reload_doc();
            }
          },
        });
      },
      primary_action_label: 'Done',
    });
    d.show();
  }
}

function deleteNote(deleteBtn, frm) {
  const rowId = $(deleteBtn).closest('.comment-content').attr('name');
  frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_note',
    args: {
      doctype: frm.doctype,
      docname: frm.doc.name,
      rowId,
    },
    freeze: true,
    callback: function (r) {
      if (!r.exc) {
        frm.refresh_fields();
      }
    },
  });
}

function showActivities(frm) {
  $(frm.fields_dict.open_activities_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;
  const crmActivities = new erpnext.utils.CRMActivities({
    frm,
    open_activities_wrapper: $(frm.fields_dict.open_activities_html.wrapper),
    all_activities_wrapper: $(frm.fields_dict.all_activities_html.wrapper),
    form_wrapper: $(frm.wrapper),
  });
  crmActivities.refresh();

  // remove extra activities table
  setTimeout(() => {
    if ($('.open-activities').length > 1) {
      crmActivities.refresh();
    }
  }, 400);
}

function renderAddressAndContacts(frm) {
  frappe.dynamic_link = {
    doc: frm.doc,
    fieldname: 'name',
    doctype: 'Canopi Lender',
  };

  frm.toggle_display(['address_html', 'contact_html'], !frm.doc.__islocal);

  if (frm.doc.__islocal) {
    frm.set_df_property('address_and_contact', 'hidden', 1);
    frappe.contacts.clear_address_and_contact(frm);
  } else {
    frm.set_df_property('address_and_contact', 'hidden', 0);
    frappe.contacts.render_address_and_contact(frm);
  }
}

function setDefaultBankAccountFilter(frm) {
  const filters = [['cnp_lender', '=', frm.doc.name]];
  if (frm.doc.default_bank) {
    filters.push(['bank', '=', frm.doc.default_bank]);
  } else {
    frm.set_value('default_bank_account', '');
  }
  frm.set_query('default_bank_account', filter => {
    return {
      filters,
    };
  });
}

function setDefaultBankFilter(frm) {
  frm.set_query('default_bank', filter => {
    return {
      filters: [['cnp_lender', '=', frm.doc.name]],
    };
  });
}

function getDefaultBank(frm) {
  if (frm.doc.default_bank_account) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.default_bank_account,
        doc_type: 'Bank Account',
      },
      freeze: true,
      callback: r => {
        frm.set_value('default_bank', r.message.bank);
      },
    });
  }
}

function renderTaxDocuments(frm) {
  setTimeout(() => {
    frm.set_df_property(
      'tax_documents_html',
      'options',
      frappe.render_template('tax_documents', {
        doc: getTaxDetailsValues(frm),
        id: frm.doc.__islocal === 1 ? '' : frm.doc.name + '-tax',
        docstatus: frm.doc.docstatus,
      }),
    );
    addTaxDocumentsEvents(frm);
  }, 100);
}

function addTaxDocumentsEvents(frm) {
  if (frm.doc.__islocal !== 1) {
    addSelectEvents(frm);
    addEditEvents(frm);
    addNewRowEvents(frm);
    addRemoveRowEvent(frm);
    addUploadEvents(frm);
    addDownloadEvents(frm);
  }
}

function addSelectEvents(frm) {
  $('.' + frm.doc.name + '-tax-select-all').unbind();
  $('.' + frm.doc.name + '-tax-select-all').click(function () {
    if (this.checked) {
      $('.' + frm.doc.name + '-tax-select-row').each((index, element) => {
        if ($(element).is(':hidden') === false) {
          $(element).prop('checked', true);
        }
      });
    } else {
      $('.' + frm.doc.name + '-tax-select-row').prop('checked', false);
    }
  });
}

function addNewRowEvents(frm) {
  $('.' + frm.doc.name + '-tax-add-row').unbind();
  $('.' + frm.doc.name + '-tax-add-row').on('click', function () {
    rowDialog(frm);
  });
}

function addEditEvents(frm) {
  $('.' + frm.doc.name + '-tax-edit-row').unbind();
  $('.' + frm.doc.name + '-tax-edit-row').on('click', function () {
    const index = $('.' + frm.doc.name + '-tax-edit-row').index(this);
    rowDialog(frm, index);
  });
}

function addRemoveRowEvent(frm) {
  $('.' + frm.doc.name + '-tax-remove-row').unbind();
  $('.' + frm.doc.name + '-tax-remove-row').on('click', function () {
    if (
      $(`input[class="${frm.doc.name}-tax-select-row"]:checked`).length <= 0
    ) {
      frappe.throw('Atleast one row must be selected to perform this action');
    }

    const s3Keys = [];
    let taxDetails = getTaxDetailsValues(frm);
    const removedIndex = [];

    $(`.${frm.doc.name}-tax-select-row`).each(function (i) {
      if (this.checked) {
        if (taxDetails[i].s3_key) {
          s3Keys.push(taxDetails[i].s3_key);
        }
        removedIndex.push(i);
      }
    });
    taxDetails = taxDetails.filter((v, i) => {
      return removedIndex.indexOf(i) === -1;
    });

    if (s3Keys.length > 0) {
      deleteMultipleFromS3(s3Keys).then(r => {
        frm.set_value('tax_documents_details', JSON.stringify(taxDetails));
        frm.save().then(() => {
          frm.scroll_to_field('tax_documents_html');
        });
      });
    } else {
      frm.set_value('tax_documents_details', JSON.stringify(taxDetails));
      frm.save().then(() => {
        frm.scroll_to_field('tax_documents_html');
      });
    }
  });
}

function addUploadEvents(frm) {
  addAttachFileEvent(frm);
  addPreviewEvent(frm);
  addRemoveFileEvent(frm);
  addUploadAllEvent(frm);
}

function addAttachFileEvent(frm) {
  let s3ButtonIndex;
  $('.' + frm.doc.name + '-tax-attach-file').unbind();
  $('.' + frm.doc.name + '-tax-attach-input').unbind();
  $('.' + frm.doc.name + '-tax-attach-file').on('click', function () {
    s3ButtonIndex = $('.' + frm.doc.name + '-tax-attach-file').index(this);
    $('.' + frm.doc.name + '-tax-attach-input')[s3ButtonIndex].click();
  });

  $('.' + frm.doc.name + '-tax-attach-input').change(async event => {
    const file = { ...event.target.files };
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    if (file) {
      event.target.value = '';
      const taxDetails = getTaxDetailsValues(frm);
      taxDetails[s3ButtonIndex].file_name = file[0].name;
      taxDetails[s3ButtonIndex].file = await getBase64(file[0]);
      frm.doc.tax_documents_details = JSON.stringify(taxDetails);
      renderTaxDocuments(frm);
    }
  });
}

function addPreviewEvent(frm) {
  $('.' + frm.doc.name + '-tax-preview-file').unbind();
  $('.' + frm.doc.name + '-tax-preview-file').on('click', function () {
    const taxDetails = getTaxDetailsValues(frm);
    const index = $('.' + frm.doc.name + '-tax-preview-file').index(this);
    const blob = convertBase64ToBlob(taxDetails[index].file);
    const blobUrl = URL.createObjectURL(blob);

    window.open(blobUrl, '_blank');
  });
}

function addRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-tax-remove-file').unbind();
  $('.' + frm.doc.name + '-tax-remove-file').on('click', function () {
    const taxDetails = getTaxDetailsValues(frm);
    const index = $('.' + frm.doc.name + '-tax-remove-file').index(this);
    taxDetails[index].file = '';
    taxDetails[index].file_name = taxDetails[index].s3_key
      ? taxDetails[index].s3_key.replace(/^.*\//, '')
      : '';
    frm.doc.tax_documents_details = JSON.stringify(taxDetails);
    renderTaxDocuments(frm);
  });
}

function addUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-tax-upload-all').unbind();
  $('.' + frm.doc.name + '-tax-upload-all').on('click', () => {
    if (getTaxDetailsValues(frm).some(x => x.file)) {
      taxDetailsS3Upload(frm);
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}

function addDownloadEvents(frm) {
  $('.' + frm.doc.name + '-tax-file-download').unbind();
  $('.' + frm.doc.name + '-tax-file-download').on('click', function () {
    const index = $('.' + frm.doc.name + '-tax-file-download').index(this);
    open_url_post(frappe.request.url, {
      cmd: 'trusteeship_platform.custom_methods.download_s3_file',
      key: getTaxDetailsValues(frm)[index].s3_key,
    });
  });
}

function taxDetailsS3Upload(frm) {
  frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform.doctype.canopi_lender.canopi_lender.tax_documents_s3_upload',
    freeze: true,
    args: {
      docname: frm.doc.name,
      tax_details: getTaxDetailsValues(frm),
    },
    callback: r => {
      frm.reload_doc().then(() => {
        renderTaxDocuments(frm);
      });
      frappe.show_alert(
        {
          message: 'Uploaded',
          indicator: 'green',
        },
        5,
      );
    },
  });
}

function rowDialog(frm, index = null) {
  const taxDetails = getTaxDetailsValues(frm);
  const d = new frappe.ui.Dialog({
    title: index !== null ? 'Edit' : 'Add',
    fields: [
      {
        label: 'Name Of Document',
        fieldname: 'name_of_document',
        fieldtype: 'Data',
        default: index === null ? '' : taxDetails[index].name_of_document,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      if (index !== null) {
        taxDetails[index].name_of_document = values.name_of_document;
      } else {
        taxDetails.push({
          name_of_document: values.name_of_document,
          file: '',
          file_name: '',
          s3_key: '',
        });
      }

      frm.set_value('tax_documents_details', JSON.stringify(taxDetails));

      d.hide();
      frm.save().then(() => {
        frm.scroll_to_field('tax_documents_html');
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });

  d.show();
}

function getTaxDetailsValues(frm) {
  frm.doc.tax_documents_details = frm.doc.tax_documents_details
    ? frm.doc.tax_documents_details
    : JSON.stringify([]);
  const details =
    frm.doc.tax_documents_details !== undefined
      ? JSON.parse(frm.doc.tax_documents_details)
      : [];

  return details;
}

function deleteMultipleFromS3(keys) {
  return frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_multiple_from_s3',
    args: { keys },
  });
}

function convertBase64ToBlob(base64Image) {
  // Split into two parts
  const parts = base64Image.split(';base64,');

  // Hold the content type
  const imageType = parts[0].split(':')[1];

  // Decode Base64 string
  const decodedData = window.atob(parts[1]);

  // Create UNIT8ARRAY of size same as row data length
  const uInt8Array = new Uint8Array(decodedData.length);

  // Insert all character code into uInt8Array
  for (let i = 0; i < decodedData.length; ++i) {
    uInt8Array[i] = decodedData.charCodeAt(i);
  }

  // Return BLOB image after conversion
  return new Blob([uInt8Array], { type: imageType });
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
