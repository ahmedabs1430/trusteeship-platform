# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import base64
import json
from datetime import datetime

import frappe
import pytz
from frappe.contacts.address_and_contact import load_address_and_contact
from frappe.model.document import Document
from frappe.utils import cstr

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    upload_file_to_s3,
    validate_s3_settings,
)


class CanopiLender(Document):
    def onload(self):
        load_address_and_contact(self)

    def validate(self):
        validate_file_attachments(self)


def validate_file_attachments(self):
    tax_details = json.loads(self.tax_documents_details)
    for tax in tax_details:
        if tax.get("file"):
            frappe.throw(
                "Cannot save document with some attached file. "
                + "Either remove or upload them."
            )


@frappe.whitelist()
def tax_documents_s3_upload(docname, tax_details):
    validate_s3_settings()
    settings_doc = frappe.get_single("Trusteeship Platform Settings")
    doc = frappe.get_doc("Canopi Lender", docname)
    tax_details = json.loads(tax_details)
    for tax in tax_details:
        if tax.get("file_name") and tax.get("file"):
            timezone = pytz.timezone(
                frappe.defaults.get_defaults().get("time_zone")
            )  # noqa: 501
            key = f"{cstr(frappe.local.site)}/{doc.doctype.replace('Canopi ', '')}/{doc.name}/Tax Documents/{str(datetime.now(tz=timezone))}/{tax['file_name']}"  # noqa: 501

            base64_file_str = tax["file"]
            base64_file_str = base64_file_str[
                base64_file_str.find(",") + 1 :  # noqa: E203
            ]  # noqa: 501
            file_data = base64.b64decode(base64_file_str)
            upload_file_to_s3(settings_doc, key, file_data)
            tax["file"] = ""
            tax["s3_key"] = key

            doc.set("tax_documents_details", json.dumps(tax_details))
            doc.save()
