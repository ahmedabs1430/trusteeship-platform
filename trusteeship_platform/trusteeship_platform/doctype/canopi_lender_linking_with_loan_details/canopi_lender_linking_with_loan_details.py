# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

# import frappe
import frappe
from frappe.model.document import Document


class CanopiLenderLinkingWithLoanDetails(Document):
    pass


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def borrower_query(doctype, txt, searchfield, start, page_len, filters):
    return frappe.get_all(
        "Canopi Tranche SL Details",
        filters={
            "parent": ("=", filters.get("tranche")),
            "borrower_name": ("like", f"{txt}%"),
        },
        fields=["borrower_name"],
        as_list=1,
    )


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def lender_query(doctype, txt, searchfield, start, page_len, filters):
    return frappe.get_all(
        "Canopi Tranche SL Details",
        filters={
            "parent": ("=", filters.get("tranche")),
            "lender_name": ("like", f"{txt}%"),
        },
        fields=["lender_name"],
        as_list=1,
    )
