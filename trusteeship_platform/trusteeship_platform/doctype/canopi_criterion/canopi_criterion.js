// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe */
frappe.ui.form.on('Canopi Criterion', {
  refresh: frm => {
    cal_sub_total(frm);
    cal_total_emp_cost(frm);
    cal_total_cost(frm);
    cal_total_per_hour_cost(frm);
    cal_bcg_employee_mandate_cost(frm);
    enable_disable_read_only(frm, !frm.doc.auto_calculate);
  },

  auto_calculate: frm => {
    enable_disable_read_only(frm, !frm.doc.auto_calculate);
    cal_sub_total(frm);
  },

  total_cost_legal_compliance: frm => {
    cal_sub_total(frm);
  },

  total_cost_operations: frm => {
    cal_sub_total(frm);
  },

  total_cost_admin: frm => {
    cal_sub_total(frm);
  },

  sub_total: frm => {
    cal_total_emp_cost(frm);
  },

  bcg_employee_total_cost: frm => {
    cal_total_emp_cost(frm);
    cal_bcg_employee_mandate_cost(frm);
  },

  total_employee_cost: frm => {
    cal_total_cost(frm);
  },

  operating_total_cost: frm => {
    cal_total_cost(frm);
  },

  per_hour_legal_compliance: frm => {
    cal_total_per_hour_cost(frm);
  },

  per_hour_operations: frm => {
    cal_total_per_hour_cost(frm);
  },

  per_hour_admin: frm => {
    cal_total_per_hour_cost(frm);
  },

  no_of_mandates: frm => {
    cal_bcg_employee_mandate_cost(frm);
  },
});

function cal_sub_total(frm) {
  if (frm.doc.auto_calculate) {
    frm.doc.sub_total =
      (frm.doc.total_cost_legal_compliance
        ? frm.doc.total_cost_legal_compliance
        : 0) +
      (frm.doc.total_cost_operations ? frm.doc.total_cost_operations : 0) +
      (frm.doc.total_cost_admin ? frm.doc.total_cost_admin : 0);
  }
  frm.trigger('sub_total');

  frm.refresh_fields();
}

function cal_total_emp_cost(frm) {
  if (frm.doc.auto_calculate) {
    frm.doc.total_employee_cost =
      (frm.doc.sub_total ? frm.doc.sub_total : 0) +
      (frm.doc.bcg_employee_total_cost ? frm.doc.bcg_employee_total_cost : 0);
  }
  frm.trigger('total_employee_cost');

  frm.refresh_fields();
}

function cal_total_cost(frm) {
  if (frm.doc.auto_calculate) {
    frm.doc.total_cost =
      (frm.doc.total_employee_cost ? frm.doc.total_employee_cost : 0) +
      (frm.doc.operating_total_cost ? frm.doc.operating_total_cost : 0);
  }
  frm.trigger('total_cost');

  frm.refresh_fields();
}

function cal_total_per_hour_cost(frm) {
  if (frm.doc.auto_calculate) {
    frm.doc.total_per_hour_cost =
      (frm.doc.per_hour_legal_compliance
        ? frm.doc.per_hour_legal_compliance
        : 0) +
      (frm.doc.per_hour_operations ? frm.doc.per_hour_operations : 0) +
      (frm.doc.per_hour_admin ? frm.doc.per_hour_admin : 0);
  }
  frm.trigger('total_per_hour_cost');

  frm.refresh_fields();
}

function cal_bcg_employee_mandate_cost(frm) {
  if (frm.doc.auto_calculate) {
    frm.doc.bcg_employee_mandate_cost =
      frm.doc.bcg_employee_total_cost / frm.doc.no_of_mandates
        ? frm.doc.bcg_employee_total_cost / frm.doc.no_of_mandates
        : 0;
  }
  frm.trigger('bcg_employee_mandate_cost');

  frm.refresh_fields();
}

function enable_disable_read_only(frm, toggle) {
  frm.toggle_enable(
    [
      'sub_total',
      'total_employee_cost',
      'total_cost',
      'total_per_hour_cost',
      'bcg_employee_mandate_cost',
    ],
    toggle,
  );
  frm.refresh_fields();
}
