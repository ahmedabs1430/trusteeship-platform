// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, locals, showActivities, showNotes */
/* eslint-env jquery */

frappe.ui.form.on('Canopi Transaction Details-FA', {
  refresh: function (frm) {
    loadInterestScheduleTables(frm);
    setSelectFieldReadonly(frm, 'borrower_details', 'is_multi_borrower');
    setSelectFieldReadonly(frm, 'lender_details', 'is_multi_lender');
    setSelectFieldReadonly(frm, 'facility_details', 'is_multi_facility');
    setSelectFieldReadonly(frm, 'tranche_details', 'is_multi_facility');

    showActivitiesAndNotes(frm);
    setFilters(frm);
    addDrawdownNoticeButton(frm);
    headerButtons(frm);
  },

  operations: frm => {
    populateForm(frm);
  },

  interest_schedule_details: frm => {
    loadInterestScheduleTables(frm);
  },
});

frappe.ui.form.on('Canopi Borrower Details', {
  borrower_details_add: function (frm, cdt, cdn) {
    validateRow(frm, 'borrower_details', 'is_multi_borrower');
  },

  borrower_details_remove: function (frm, cdt, cdn) {
    validateRow(frm, 'borrower_details', 'is_multi_borrower');
  },

  borrower_code: async function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    if (row.borrower_code) {
      const doc = [
        {
          doctype: 'Customer',
          docname: row.borrower_code,
        },
        {
          doctype: 'Quotation',
          docname: frm.doc.quotation,
        },
      ];
      const response = await getDocsWithLinkedChilds(doc);
      row.borrower_name = response.customer.customer_name;
      row.borrower_group = response.customer.customer_group;
      row.address = response.quotation?.customer_address;
      row.contact = response.quotation?.contact_person;

      frm.refresh_fields();
    }
  },
});

frappe.ui.form.on('Canopi Lender Details', {
  lender_details_add: function (frm, cdt, cdn) {
    validateRow(frm, 'lender_details', 'is_multi_lender');
  },

  lender_details_remove: function (frm, cdt, cdn) {
    validateRow(frm, 'lender_details', 'is_multi_lender');
  },

  lender_code: async function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];

    if (row.lender_code) {
      const doc = [
        {
          doctype: 'Canopi Lender',
          docname: row.lender_code,
        },
        {
          doctype: 'Bank',
          parent_doctype: 'Canopi Lender',
          child_link_field: 'default_bank',
        },
      ];
      const response = await getDocsWithLinkedChilds(doc);
      const lender = response?.canopi_lender;
      const bank = response?.bank;
      row.lender_name = lender.lender_name;
      row.bank_account_details = lender.default_bank_account;
      row.bank = lender.default_bank;
      row.currency_of_loan = lender.currency;
      row.swift_number = bank?.swift_number;
      frm.refresh_fields();
    }
  },

  status_of_tax_documents_of_banks: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    if (row?.lender_code) {
      localStorage.setItem('scroll_to_tax_docs', '1');
      frappe.set_route('Form', 'Canopi Lender', row?.lender_code);
    }
  },
});

frappe.ui.form.on('Canopi Facility Details', {
  facility_details_add: function (frm, cdt, cdn) {
    validateRow(frm, 'facility_details', 'is_multi_facility');
  },

  facility_details_remove: function (frm, cdt, cdn) {
    validateRow(frm, 'facility_details', 'is_multi_facility');
  },

  facility_id: async function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    const response = await getDoc('Canopi FA Details', row.facility_id);
    row.facility_name = response?.facility_name;
    frm.refresh_fields();
  },

  from_date: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    row.no_of_days = noOfDays(new Date(row.from_date), new Date(row.upto_date));
    frm.refresh_fields();

    validateNoOfDays(frm, row);
  },

  upto_date: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    row.no_of_days = noOfDays(new Date(row.from_date), new Date(row.upto_date));
    frm.refresh_fields();

    validateNoOfDays(frm, row);
  },
});

frappe.ui.form.on('Canopi Tranche FA Details', {
  tranche_details_add: function (frm, cdt, cdn) {
    validateRow(frm, 'tranche_details', 'is_multi_facility');
  },

  tranche_details_remove: function (frm, cdt, cdn) {
    validateRow(frm, 'tranche_details', 'is_multi_facility');
  },

  tranche_details: async function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    const response = await getDoc(
      'Canopi Tranche Details',
      row.tranche_details,
    );
    row.no_of_tranches = response?.no_of_tranches;
    frm.refresh_fields();
  },
});

frappe.ui.form.on('Canopi Lender Linking With Loan Details', {
  lender_linking_with_loan_details_add: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    setBorrowerName(frm, row);
    setLenderName(frm, row);
  },

  interest_period_from: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    row.no_of_days = noOfDays(
      new Date(row.interest_period_from),
      new Date(row.interest_period_to),
    );
    frm.refresh_fields();

    validateNoOfDays(frm, row);
  },

  interest_period_to: function (frm, cdt, cdn) {
    const row = locals[cdt][cdn];
    row.no_of_days = noOfDays(
      new Date(row.interest_period_from),
      new Date(row.interest_period_to),
    );
    frm.refresh_fields();

    validateNoOfDays(frm, row);
  },
});

function addDrawdownNoticeButton(frm) {
  if (frm.doc.tranche_details?.length > 0) {
    frm.add_custom_button('Generate Drawdown Notice', () => {});
  }
}

function setBorrowerName(frm, row) {
  if (!frm.doc.is_multi_borrower) {
    row.borrower_name = frm.doc.borrower_details?.find(x => x)?.borrower_code;
    frm.refresh_fields();
  }
}

function setLenderName(frm, row) {
  if (!frm.doc.is_multi_lender) {
    row.lender_name = frm.doc.lender_details?.find(x => x)?.lender_code;
    frm.refresh_fields();
  }
}

function setFilters(frm) {
  frm.fields_dict.borrower_details.grid.get_field('borrower_code').get_query =
    function (doc, cdt, cdn) {
      return {
        filters: [['Customer', 'opportunity_name', '=', frm.doc.opportunity]],
      };
    };

  frm.fields_dict.borrower_details.grid.get_field('address').get_query =
    function (doc, cdt, cdn) {
      let filters = [];
      if (frm.doc.lead) {
        filters = [
          ['Dynamic Link', 'link_doctype', '=', 'Lead'],
          ['Dynamic Link', 'link_name', '=', frm.doc.lead],
        ];
      } else {
        filters = [
          ['Dynamic Link', 'link_doctype', '=', 'Customer'],
          ['Dynamic Link', 'link_name', '=', frm.doc.cnp_customer_code],
        ];
      }
      return {
        filters,
      };
    };

  frm.fields_dict.borrower_details.grid.get_field('contact').get_query =
    function (doc, cdt, cdn) {
      let filters = [];
      if (frm.doc.lead) {
        filters = [
          ['Dynamic Link', 'link_doctype', '=', 'Lead'],
          ['Dynamic Link', 'link_name', '=', frm.doc.lead],
        ];
      } else {
        filters = [
          ['Dynamic Link', 'link_doctype', '=', 'Customer'],
          ['Dynamic Link', 'link_name', '=', frm.doc.cnp_customer_code],
        ];
      }
      return {
        filters,
      };
    };

  frm.fields_dict.facility_details.grid.get_field('facility_id').get_query =
    function (doc, cdt, cdn) {
      return {
        filters: [['Canopi FA Details', 'facility_agent', '=', frm.doc.name]],
      };
    };

  frm.fields_dict.tranche_details.grid.get_field('facility_name').get_query =
    function (doc, cdt, cdn) {
      const filters = frm.doc.facility_details?.map(x => x.facility_id);
      return {
        filters: [['Canopi FA Details', 'name', 'in', filters]],
      };
    };

  frm.fields_dict.lender_linking_with_loan_details.grid.get_field(
    'facility_name',
  ).get_query = function (doc, cdt, cdn) {
    const filters = frm.doc.facility_details?.map(x => x.facility_id);
    return {
      filters: [['Canopi FA Details', 'name', 'in', filters]],
    };
  };

  frm.fields_dict.lender_linking_with_loan_details.grid.get_field(
    'borrower_name',
  ).get_query = (doc, cdt, cdn) => {
    const row = locals[cdt][cdn];
    const filters = frm.doc.borrower_details?.map(x => x.borrower_code);

    if (row?.tranche_sl) {
      const query =
        'trusteeship_platform.trusteeship_platform.doctype.canopi_lender_linking_with_loan_details.' +
        'canopi_lender_linking_with_loan_details.borrower_query';
      return {
        query,
        filters: { tranche: row.tranche_sl },
      };
    } else {
      return {
        filters: [['Customer', 'name', 'in', filters]],
      };
    }
  };

  frm.fields_dict.lender_linking_with_loan_details.grid.get_field(
    'lender_name',
  ).get_query = (doc, cdt, cdn) => {
    const row = locals[cdt][cdn];

    if (row?.tranche_sl) {
      const query =
        'trusteeship_platform.trusteeship_platform.doctype.canopi_lender_linking_with_loan_details.' +
        'canopi_lender_linking_with_loan_details.lender_query';
      return {
        query,
        filters: { tranche: row.tranche_sl },
      };
    }
  };

  frm.fields_dict.utilization_details.grid.get_field(
    'facility_name',
  ).get_query = function (doc, cdt, cdn) {
    const filters = frm.doc.facility_details?.map(x => x.facility_id);
    return {
      filters: [['Canopi FA Details', 'name', 'in', filters]],
    };
  };

  frm.fields_dict.utilization_details.grid.get_field('tranche_sl').get_query =
    function (doc, cdt, cdn) {
      const row = locals[cdt][cdn];
      return {
        filters: [
          ['Canopi Tranche Details', 'facility_id', '=', row?.facility_name],
        ],
      };
    };
}

function validateNoOfDays(frm, row) {
  if (row.no_of_days < 0) {
    row.no_of_days = '';

    // for facility details table
    if (row.upto_date) {
      row.upto_date = '';
    }

    // for lender linking table
    if (row.interest_period_to) {
      row.interest_period_to = '';
    }

    frm.refresh_fields();
    frappe.throw('Upto Date must be after From Date');
  }
}

function noOfDays(fromDate, toDate) {
  const diff = toDate.getTime() - fromDate.getTime();
  const totalDays = Math.ceil(diff / (1000 * 3600 * 24));
  return isNaN(totalDays) ? '' : totalDays;
}

function validateRow(frm, tableName, validateFrom) {
  if (!frm.doc[validateFrom] && frm.doc[tableName].length > 1) {
    frm.doc[tableName].splice(-1, 1);
    frm.refresh_fields();
    frappe.throw('Cannot add more than one rows');
  }
  setSelectFieldReadonly(frm, tableName, validateFrom);
}

function setSelectFieldReadonly(frm, tableName, validateFrom) {
  frm.set_df_property(
    validateFrom,
    'read_only',
    frm.doc[tableName]?.length > 1,
  );
}

async function populateForm(frm) {
  if (frm.doc.operations) {
    const docs = [
      {
        doctype: 'ATSL Mandate List',
        docname: frm.doc.operations,
      },
      {
        doctype: 'Customer',
        parent_doctype: 'ATSL Mandate List',
        child_link_field: 'cnp_customer_code',
      },
      {
        doctype: 'Opportunity',
        parent_doctype: 'ATSL Mandate List',
        child_link_field: 'opportunity',
      },
      {
        doctype: 'Quotation',
        parent_doctype: 'ATSL Mandate List',
        child_link_field: 'quotation',
      },
    ];
    const response = await getDocsWithLinkedChilds(docs);
    const customer = response?.customer;
    const operationsMaster = response?.atsl_mandate_list;
    const opportunity = response?.opportunity;
    const quotation = response?.quotation;

    frm.set_value('cnp_customer_code', operationsMaster.cnp_customer_code);
    frm.set_value('product_name', operationsMaster.product_name);
    frm.set_value('mandate_name', operationsMaster.mandate_name);
    frm.set_value('opportunity', operationsMaster.opportunity);
    frm.set_value('quotation', operationsMaster.quotation);
    frm.set_value(
      'lead',
      opportunity.opportunity_from === 'Lead' ? opportunity.party_name : '',
    );

    if (customer) {
      frm.doc.borrower_details = [];
      frm.add_child('borrower_details', {
        borrower_name: customer.customer_name,
        borrower_code: customer.name,
        borrower_group: customer.customer_group,
        address: quotation?.customer_address,
        contact: quotation?.contact_person,
      });
    }
    frm.refresh_fields();
  }
}

async function getDocsWithLinkedChilds(docs) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_docs_with_linked_childs',
    args: {
      docs,
    },
    freeze: true,
  });

  return response.message;
}

function showActivitiesAndNotes(frm) {
  frappe.require('assets/trusteeship_platform/js/common.js', () => {
    showActivities(frm);
    showNotes(frm);
  });
}

async function getDoc(doctype, docname) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: doctype,
      doc_name: docname,
    },
    freeze: true,
  });

  return response.message;
}

function loadInterestScheduleTables(frm) {
  renderTemplate(frm);
}

function renderTemplate(frm) {
  $(frm.fields_dict.interest_schedule_html.wrapper).empty();

  frm.set_df_property(
    'interest_schedule_html',
    'options',
    frappe.render_template('interest_schedule_tables', {
      doc: getInterestScheduleDetails(frm),
      docstatus: frm.doc.docstatus,
    }),
  );
  const { keys, details } = getInterestScheduleDetails(frm);
  frm.refresh_fields();

  addSelectEvents(frm, keys);
  addNewRowEvents(frm, keys);
  addEditEvents(frm, keys);
  addRemoveRowEvents(frm, keys, details);
}

function addNewRowEvents(frm, facilityNames) {
  facilityNames?.forEach(name => {
    $('.' + name + '-add-row').unbind();
    $('.' + name + '-add-row').on('click', function () {
      rowDialog(frm, name);
    });
  });
}

function addEditEvents(frm, facilityNames) {
  facilityNames?.forEach(name => {
    $('.' + name + '-edit-row').unbind();
    $('.' + name + '-edit-row').on('click', function () {
      const index = $('.' + name + '-edit-row').index(this);
      rowDialog(frm, name, index);
    });

    $('.' + name + '-index-edit-row').unbind();
    $('.' + name + '-index-edit-row').on('click', function () {
      const index = $('.' + name + '-index-edit-row').index(this);
      rowDialog(frm, name, index);
    });
  });
}

function addSelectEvents(frm, facilityNames) {
  facilityNames?.forEach(name => {
    $('.' + name + '-select-all').click(function () {
      $('.' + name + '-select-row').prop('checked', this.checked);

      if (this.checked) {
        $('.' + name + '-remove-row').removeClass('hidden');
      } else {
        $('.' + name + '-remove-row').addClass('hidden');
      }
    });

    // show/hide delete row button
    $('.' + name + '-select-row').click(function () {
      const selectElements = $('.' + name + '-select-row')?.toArray();
      if (selectElements?.some(x => x.checked)) {
        $('.' + name + '-remove-row').removeClass('hidden');
      } else {
        $('.' + name + '-remove-row').addClass('hidden');
        $('.' + name + '-select-all').prop('checked', false);
      }
    });
  });
}

function addRemoveRowEvents(frm, facilityNames, details) {
  const removeFromIndex = [];
  facilityNames?.forEach(name => {
    $('.' + name + '-remove-row').unbind();
    $('.' + name + '-remove-row').on('click', function () {
      if ($(`input[class="${name}-select-row"]:checked`).length <= 0) {
        frappe.throw('Atleast one row must be selected to perform this action');
      }

      $('.' + name + '-select-row').each(function (i) {
        if (this.checked) {
          removeFromIndex.push(i);
        }
      });

      details[name].docs = details[name].docs.filter((value, index) => {
        return removeFromIndex.indexOf(index) === -1;
      });
      frm.set_value('interest_schedule_details', JSON.stringify(details));
      frm.save_or_update();
    });
  });
}

function rowDialog(frm, facilityName, index = null) {
  const details = getInterestScheduleDetails(frm).details;
  const defaultValues = details[facilityName].docs[index];
  const d = new frappe.ui.Dialog({
    title: index !== null ? 'Edit' : 'Add',
    fields: getDialogFields(frm, defaultValues),
    primary_action_label: 'Confirm',
    primary_action: values => {
      if (values.repayment_installment === 0) {
        frappe.throw('<b>Repayment Installment %</b> cannot be 0');
      }
      if (index === null) {
        details[facilityName].docs.push(values);
      } else {
        details[facilityName].docs[index] = values;
      }
      frm.set_value('interest_schedule_details', JSON.stringify(details));

      d.hide();
      setTimeout(() => {
        frm.save_or_update();
      }, 100);
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });

  d.show();
  d.fields_dict.tranche_sl.get_query = function () {
    return {
      filters: [['Canopi Tranche Details', 'facility_id', '=', facilityName]],
    };
  };
  d.fields_dict.lender_name.get_query = function () {
    if (frm.doc.if_tranche_required === 'Yes') {
      const query =
        'trusteeship_platform.trusteeship_platform.doctype.canopi_lender_linking_with_loan_details.' +
        'canopi_lender_linking_with_loan_details.lender_query';
      return {
        query,
        filters: { tranche: d.get_value('tranche_sl') },
      };
    } else {
      return {
        filters: [
          [
            'Canopi Lender',
            'name',
            'in',
            frm.doc.lender_details?.map(x => x.lender_code),
          ],
        ],
      };
    }
  };
}

function getDialogFields(frm, doc) {
  const fields = [
    {
      label: 'Tranche SL',
      fieldname: 'tranche_sl',
      fieldtype: 'Link',
      options: 'Canopi Tranche Details',
      default: doc?.tranche_sl,
      read_only: frm.doc.docstatus !== 0,
      reqd: frm.doc.if_tranche_required === 'Yes',
    },

    {
      label: 'Lender Name',
      fieldname: 'lender_name',
      fieldtype: 'Link',
      options: 'Canopi Lender',
      default: doc?.lender_name,
      read_only: frm.doc.docstatus !== 0,
    },

    {
      label: 'Loan Link',
      fieldname: 'loan_link',
      fieldtype: 'Link',
      options: 'Loan',
      default: doc?.loan_link,
      read_only: frm.doc.docstatus !== 1,
      depends_on: frm.doc.docstatus !== 0,
    },

    {
      label: 'Repayment Installment %',
      fieldname: 'repayment_installment',
      fieldtype: 'Float',
      default: doc?.repayment_installment,
      read_only: frm.doc.docstatus !== 0,
      reqd: 1,
    },

    {
      label: 'Repayment Date (Months)',
      fieldname: 'repayment_in_months',
      fieldtype: 'Int',
      default: doc?.repayment_in_months,
      read_only: frm.doc.docstatus !== 0,
      reqd: 1,
    },

    {
      label: 'Repayment Date',
      fieldname: 'repayment_date',
      fieldtype: 'Date',
      default: doc?.repayment_date,
      read_only: frm.doc.docstatus !== 0,
      reqd: 1,
    },

    {
      label: 'Repayment Amount',
      fieldname: 'repayment_amount',
      fieldtype: 'Int',
      default: doc?.repayment_amount,
      read_only: frm.doc.docstatus !== 0,
      reqd: 1,
    },

    {
      label: 'Select Entry',
      fieldname: 'select_entry',
      fieldtype: 'Select',
      options: '\nY\nN',
      default: doc?.select_entry,
      read_only: frm.doc.docstatus !== 1,
      depends_on: frm.doc.docstatus !== 0,
    },
  ];

  return fields;
}

function getInterestScheduleDetails(frm) {
  frm.doc.interest_schedule_details = frm.doc.interest_schedule_details
    ? frm.doc.interest_schedule_details
    : JSON.stringify({});
  const details =
    frm.doc.interest_schedule_details !== undefined
      ? JSON.parse(frm.doc.interest_schedule_details)
      : {};
  return { keys: Object.keys(details), details };
}

function headerButtons(frm) {
  if (frm.doc.operations) {
    frm.add_custom_button('Main', () => {
      window.open(`/app/atsl-mandate-list/${frm.doc.operations}`);
    });
  }
  if (!frm.doc.__islocal && frm.doc.operations) {
    let transactionDetails;
    if (frm.doc.product_name.toUpperCase() === 'FA') {
      transactionDetails = 'Canopi Transaction Details-FA';
    } else {
      transactionDetails = 'Transaction Details';
    }
    frm.add_custom_button(
      transactionDetails,
      () => {
        frm.scroll_to_field('operations');
      },
      'Operations',
    );
    frm.add_custom_button(
      'Pre-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Pre Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Pre Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Documentation',
      () => {
        const linkedDoctypes = {
          'Canopi Documentation': {
            fieldname: ['operations'],
          },
        };

        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Documentation', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Post-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Post Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Post Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
  }
}

function redirectDoctype(frm, doctype, response) {
  if (response.message[doctype]) {
    const outputString = doctype
      .replace(/([a-z0-9])([A-Z])/g, '$1 $2') // Insert space before capital letters if not already separated
      .toLowerCase() // Convert the entire string to lowercase
      .replace(/\s+/g, '-'); // Replace spaces with hyphens
    window.open(`/app/${outputString}/${response.message[doctype][0].name}`);
  } else {
    frappe.model.with_doctype(doctype, function () {
      const source = frm.doc;
      const targetDoc = frappe.model.get_new_doc(doctype);

      targetDoc.operations = source.operations;
      targetDoc.person_name = source.person_name;
      targetDoc.cnp_customer_code = source.cnp_customer_code;
      targetDoc.mandate_name = source.mandate_name;
      targetDoc.opportunity = source.opportunity;
      targetDoc.product_name = source.product_name;

      frappe.ui.form.make_quick_entry(doctype, null, null, targetDoc);
    });
  }
}
