# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json

from frappe.model.document import Document


class CanopiTransactionDetailsFA(Document):
    def before_save(self):
        update_interest_schedule_table(self)


def update_interest_schedule_table(self):
    interest_schedule_details = (
        json.loads(self.interest_schedule_details)
        if self.interest_schedule_details
        else {}
    )

    add_remove_row(self, interest_schedule_details)
    calculate_total(interest_schedule_details)

    self.interest_schedule_details = json.dumps(interest_schedule_details)


def add_remove_row(self, interest_schedule_details):
    # check for new rows and add them from html table json
    for entry in self.utilization_details:
        if (entry.facility_name) and (
            not interest_schedule_details.get(entry.facility_name)
        ):
            interest_schedule_details[entry.facility_name] = {
                "docs": [],
                "total_repayment_installment": 0,
                "total_repayment_amount": 0,
            }

    # check for deleted rows and delete them from html table json
    remove_keys = []
    for key in interest_schedule_details:
        if not any(
            x for x in self.utilization_details if x.facility_name == key
        ):  # noqa: 501
            remove_keys.append(key)

    for key in remove_keys:
        if key in interest_schedule_details:
            del interest_schedule_details[key]


def calculate_total(interest_schedule_details):
    for key in interest_schedule_details:
        total_installment = 0
        total_amount = 0
        for detail in interest_schedule_details[key]["docs"]:
            total_installment += int(detail["repayment_installment"])
            total_amount += int(detail["repayment_amount"])
        interest_schedule_details[key][
            "total_repayment_installment"
        ] = total_installment
        interest_schedule_details[key][
            "total_repayment_amount"
        ] = total_amount  # noqa: 501
