from frappe import _


def get_data():
    return {
        "heatmap": True,
        "fieldname": "facility_agent",
        "transactions": [
            {
                "label": _("Facility Agent"),
                "items": ["Canopi FA Details"],
            },
        ],
    }
