# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import cint


class TransactionDetails(Document):
    def validate(self):
        set_asset_id(self)


def set_asset_id(self):
    series_number = frappe.db.sql(
        """SELECT MAX(series_number) FROM `tabMap Asset For The Debenture INE`
                    WHERE parent = %s""",
        self.name,
    )
    series_number = cint(series_number[0][0])
    for d in self.get("map_asset_for_the_debenture_ine"):
        if not d.asset_id:
            series_number = series_number + 1
            d.series_number = series_number
            d.asset_id = (
                self.operations + "" + str(series_number).rjust(2, "0")
            )  # noqa: E501


def asset_owner_set_asset_id(docname):
    doc = frappe.get_doc("Transaction Details", docname)
    series_number = 1
    return doc.operations + "" + str(series_number).rjust(2, "0")  # noqa: E501


@frappe.whitelist()
def save_immovable_asset(
    docname,
    as_on_date="",
    asset_type="",
    asset_sub_type="",
    property_type="",
    asset_short_name="",
    percentage_of_ownership="",
    insurance_policy_number="",
    name_of_insurance_company="",
    amount="",
    beneficiary_name="",
    period_of_insurance="",
    area_of_property="",
    area_unit="",
    negative_lien_on_asset="",
    nsdl="",
    code_in_far="",
    code_description_in_far="",
    latitudelongitude="",
    registration_number="",
    type_of_cover="",
    details_of_the_tenancyleaselicence="",
    value_of_asset_as_per_balance_sheet="",
):
    if frappe.db.exists("Transaction Details Immovable Asset", docname):
        doc = frappe.get_doc("Transaction Details Immovable Asset", docname)
        doc.as_on_date = as_on_date
        doc.asset_type = asset_type
        doc.asset_sub_type = asset_sub_type
        doc.property_type = property_type
        doc.asset_short_name = asset_short_name
        doc.percentage_of_ownership = percentage_of_ownership
        doc.insurance_policy_number = insurance_policy_number
        doc.name_of_insurance_company = name_of_insurance_company
        doc.amount = amount
        doc.beneficiary_name = beneficiary_name
        doc.period_of_insurance = period_of_insurance
        doc.area_of_property = area_of_property
        doc.area_unit = area_unit
        doc.negative_lien_on_asset = negative_lien_on_asset
        doc.nsdl = nsdl
        doc.code_in_far = code_in_far
        doc.code_description_in_far = code_description_in_far
        doc.latitudelongitude = latitudelongitude
        doc.registration_number = registration_number
        doc.type_of_cover = type_of_cover
        doc.details_of_the_tenancyleaselicence = (
            details_of_the_tenancyleaselicence  # noqa: 501
        )
        doc.value_of_asset_as_per_balance_sheet = (
            value_of_asset_as_per_balance_sheet  # noqa: 501
        )
        doc.save()


@frappe.whitelist()
def save_motor_vehicle(
    docname,
    type_of_asset="",
    asset_sub_type="",
    asset_short_name="",
    asset_description="",
    vehical_number="",
    chasis_number="",
    valuation_agency="",
    valuation_amount="",
    valuation_date="",
    existing_document_list="",
    type_of_cover="",
    negative_lien_on_asset="",
    nsdl="",
    code_in_far="",
    code_description_in_far="",
    registration_number="",
    insurance_policy_number="",
    name_of_insurance_company="",
    amount="",
    beneficiary_name="",
    value_of_the_asset_as_per_balance_sheet="",
):
    if frappe.db.exists("Transaction Details Motor Vehicle", docname):
        doc = frappe.get_doc("Transaction Details Motor Vehicle", docname)
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.asset_short_name = asset_short_name
        doc.asset_description = asset_description
        doc.vehical_number = vehical_number
        doc.chasis_number = chasis_number
        doc.valuation_agency = valuation_agency
        doc.valuation_amount = valuation_amount
        doc.valuation_date = valuation_date
        doc.existing_document_list = existing_document_list
        doc.type_of_cover = type_of_cover
        doc.negative_lien_on_asset = negative_lien_on_asset
        doc.nsdl = nsdl
        doc.code_in_far = code_in_far
        doc.code_description_in_far = code_description_in_far
        doc.registration_number = registration_number
        doc.insurance_policy_number = insurance_policy_number
        doc.name_of_insurance_company = name_of_insurance_company
        doc.amount = amount
        doc.beneficiary_name = beneficiary_name
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.save()


@frappe.whitelist()
def save_intangible_asset(
    docname,
    type_of_asset="",
    asset_sub_type="",
    intangible_asset_type="",
    asset_short_name="",
    ipr_details="",
    trademark_idcopyrights_id="",
    owner_name="",
    valuation_amount="",
    valuation_agency="",
    valuation_date="",
    registration_number="",
    negative_lien_on_asset="",
    nsdl="",
    value_of_the_asset_as_per_balance_sheet="",
):
    if frappe.db.exists("Transaction Details Intangible Asset", docname):
        doc = frappe.get_doc("Transaction Details Intangible Asset", docname)
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.asset_short_name = asset_short_name
        doc.intangible_asset_type = intangible_asset_type
        doc.ipr_details = ipr_details
        doc.trademark_idcopyrights_id = trademark_idcopyrights_id
        doc.owner_name = owner_name
        doc.valuation_amount = valuation_amount
        doc.valuation_date = valuation_date
        doc.valuation_agency = valuation_agency
        doc.registration_number = registration_number
        doc.negative_lien_on_asset = negative_lien_on_asset
        doc.nsdl = nsdl
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.save()


@frappe.whitelist()
def save_financial_asset(
    docname,
    type_of_asset="",
    sub_asset_type="",
    asset_short_name="",
    coverage="",
    asset_description="",
    valuation="",
    valuation_date="",
    value_of_the_asset_as_per_balance_sheet="",
    nsdl="",
    remarks="",
):
    if frappe.db.exists("Transaction Details Financial Asset", docname):
        doc = frappe.get_doc("Transaction Details Financial Asset", docname)
        doc.type_of_asset = type_of_asset
        doc.asset_short_name = asset_short_name
        doc.sub_asset_type = sub_asset_type
        doc.coverage = coverage
        doc.asset_description = asset_description
        doc.valuation = valuation
        doc.valuation_date = valuation_date
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.nsdl = nsdl
        doc.remarks = remarks
        doc.save()


@frappe.whitelist()
def save_assignment_of_rights(
    docname,
    type_of_asset="",
    asset_sub_type="",
    asset_description="",
    asset_value="",
    value_of_the_asset_as_per_balance_sheet="",
    negative_lien_on_asset="",
    nsdl="",
    remarks="",
):
    if frappe.db.exists("Transaction Details Assignment of Rights", docname):
        doc = frappe.get_doc(
            "Transaction Details Assignment of Rights", docname
        )  # noqa: 501
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.asset_description = asset_description
        doc.asset_value = asset_value
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.negative_lien_on_asset = negative_lien_on_asset
        doc.nsdl = nsdl
        doc.remarks = remarks
        doc.save()


@frappe.whitelist()
def save_guarantee(
    docname,
    type_of_asset="",
    asset_sub_type="",
    pan_number="",
    asset_short_name="",
    guarantor_type="",
    guarantor_name="",
    address_of_guarantor="",
    net_worth_amount="",
    certificate_date="",
    valuation_agency="",
    guarantee_provided_to="",
    guarantee_amount="",
    documents_upload="",
    negative_lien_on_asset="",
    nsdl="",
    remarks="",
    value_of_the_asset_as_per_balance_sheet="",
    government_order_id="",
):
    if frappe.db.exists("Transaction Details Guarantee", docname):
        doc = frappe.get_doc("Transaction Details Guarantee", docname)
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.pan_number = pan_number
        doc.asset_short_name = asset_short_name
        doc.guarantor_type = guarantor_type
        doc.guarantor_name = guarantor_name
        doc.address_of_guarantor = address_of_guarantor
        doc.net_worth_amount = net_worth_amount
        doc.certificate_date = certificate_date
        doc.valuation_agency = valuation_agency
        doc.guarantee_provided_to = guarantee_provided_to
        doc.guarantee_amount = guarantee_amount
        doc.documents_upload = documents_upload
        doc.negative_lien_on_asset = negative_lien_on_asset
        doc.nsdl = nsdl
        doc.remarks = remarks
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.government_order_id = government_order_id
        doc.save()


@frappe.whitelist()
def save_movable_fixed_asset(
    docname,
    type_of_asset="",
    asset_sub_type="",
    asset_short_name="",
    asset_description="",
    value_of_the_asset_as_per_balance_sheet="",
    valuation_date="",
    negative_lien_on_assets="",
    code_in_erpfar="",
    nsdl="",
    remarks="",
    code_description_in_erp="",
    code_in_far="",
    code_description_in_far="",
    insurance_policy_number="",
    name_of_insurance_company="",
    amount="",
    beneficiary_name="",
    type_of_cover="",
    asset_value="",
    details_of_tenancyleaselicense="",
):
    if frappe.db.exists("Transaction Details Movable Fixed Asset", docname):
        doc = frappe.get_doc(
            "Transaction Details Movable Fixed Asset", docname
        )  # noqa: 501
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.asset_short_name = asset_short_name
        doc.asset_description = asset_description
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.valuation_date = valuation_date
        doc.negative_lien_on_assets = negative_lien_on_assets
        doc.code_in_erpfar = code_in_erpfar
        doc.nsdl = nsdl
        doc.remarks = remarks
        doc.code_description_in_erp = code_description_in_erp
        doc.code_in_far = code_in_far
        doc.code_description_in_far = code_description_in_far
        doc.insurance_policy_number = insurance_policy_number
        doc.name_of_insurance_company = name_of_insurance_company
        doc.amount = amount
        doc.beneficiary_name = beneficiary_name
        doc.type_of_cover = type_of_cover
        doc.asset_value = asset_value
        doc.details_of_tenancyleaselicense = details_of_tenancyleaselicense
        doc.save()


@frappe.whitelist()
def save_current_asset(
    docname,
    type_of_asset="",
    asset_sub_type="",
    asset_short_name="",
    asset_description="",
    value_of_the_asset_as_per_balance_sheet="",
    valuation_date="",
    negative_lien_on_assets="",
    nsdl="",
    remarks="",
    insurance_policy_number="",
    name_of_insurance_company="",
    amount="",
    beneficiary_name="",
    type_of_cover="",
):
    if frappe.db.exists("Transaction Details Current Asset", docname):
        doc = frappe.get_doc("Transaction Details Current Asset", docname)
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.asset_short_name = asset_short_name
        doc.asset_description = asset_description
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.valuation_date = valuation_date
        doc.negative_lien_on_assets = negative_lien_on_assets
        doc.nsdl = nsdl
        doc.remarks = remarks
        doc.insurance_policy_number = insurance_policy_number
        doc.name_of_insurance_company = name_of_insurance_company
        doc.amount = amount
        doc.beneficiary_name = beneficiary_name
        doc.type_of_cover = type_of_cover
        doc.save()


@frappe.whitelist()
def save_dsra(
    docname,
    type_of_asset="",
    asset_sub_type="",
    value_of_the_asset_as_per_balance_sheet="",
    escrow_account_no="",
    escrow_bank_name="",
    amount_to_be_maintained="",
):
    if frappe.db.exists("Transaction Details DSRA", docname):
        doc = frappe.get_doc("Transaction Details DSRA", docname)
        doc.type_of_asset = type_of_asset
        doc.asset_sub_type = asset_sub_type
        doc.value_of_the_asset_as_per_balance_sheet = (
            value_of_the_asset_as_per_balance_sheet
        )
        doc.escrow_account_no = escrow_account_no
        doc.escrow_bank_name = escrow_bank_name
        doc.amount_to_be_maintained = amount_to_be_maintained
        doc.save()


@frappe.whitelist()
def save_assets_owner_details(
    transaction_details_name,
    type_of_asset_holder="",
    relationship_with_asset_owner="",
    type_of_asset_ownership="",
    leasor_name="",
    lease_end_date="",
    asset_address="",
    property_id="",
    asset_address_1="",
    asset_address_2="",
    asset_address_3="",
    asset_owner_name="",
    pan_number="",
    lei_name="",
    iso_country_code="",
    city="",
    state="",
    pincode="",
    country="",
    landmark="",
):
    if frappe.db.exists(
        "Transaction Details Assets Owner Details",
        {"transaction_details_name": transaction_details_name},
    ):
        doc = frappe.get_doc(
            "Transaction Details Assets Owner Details",
            {"transaction_details_name": transaction_details_name},
        )
        doc.transaction_details_name = transaction_details_name
        doc.type_of_asset_holder = type_of_asset_holder
        doc.relationship_with_asset_owner = relationship_with_asset_owner
        doc.type_of_asset_ownership = type_of_asset_ownership
        doc.leasor_name = leasor_name
        doc.lease_end_date = lease_end_date
        doc.asset_address = asset_address
        doc.property_id = property_id
        doc.asset_address_1 = asset_address_1
        doc.asset_address_2 = asset_address_2
        doc.asset_address_3 = asset_address_3
        doc.asset_owner_name = asset_owner_name
        doc.pan_number = pan_number
        doc.lei_name = lei_name
        doc.iso_country_code = iso_country_code
        doc.city = city
        doc.state = state
        doc.pincode = pincode
        doc.country = country
        doc.landmark = landmark
        doc.save()
    else:
        doc = frappe.new_doc("Transaction Details Assets Owner Details")
        doc.transaction_details_name = transaction_details_name
        doc.type_of_asset_holder = type_of_asset_holder
        doc.relationship_with_asset_owner = relationship_with_asset_owner
        doc.type_of_asset_ownership = type_of_asset_ownership
        doc.leasor_name = leasor_name
        doc.lease_end_date = lease_end_date
        doc.asset_address = asset_address
        doc.property_id = asset_owner_set_asset_id(transaction_details_name)
        doc.asset_address_1 = asset_address_1
        doc.asset_address_2 = asset_address_2
        doc.asset_address_3 = asset_address_3
        doc.asset_owner_name = asset_owner_name
        doc.pan_number = pan_number
        doc.lei_name = lei_name
        doc.iso_country_code = iso_country_code
        doc.city = city
        doc.state = state
        doc.pincode = pincode
        doc.country = country
        doc.landmark = landmark
        doc.insert()
