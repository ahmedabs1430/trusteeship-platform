// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs, erpnext, locals, cur_frm, open_url_post */
/* eslint-env jquery */
let immovableAsset = {};
let motorVehicle = {};
let intangibleAsset = {};
let financialAsset = {};
let assignmentOfRights = {};
let guarantee = {};
let movableFixedAsset = {};
let currentAsset = {};
let dsra = {};
let assetsOwnerDetails = {};

frappe.ui.form.on('Transaction Details', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    hideUnhideSections(frm);
    loadAssetsOwnerDetails(frm);
    setFilters(frm);
    fileUpload(frm);
    showActivities(frm);
    showNotes(frm);
    headerButtons(frm);
    renderButtons(frm);
  },
  opportunity: frm => {
    if (
      frm.doc.product_name.toUpperCase() === 'REIT' ||
      frm.doc.product_name.toUpperCase() === 'INVIT'
    ) {
      fetchOpportunityField(frm);
    }
  },
  operations: frm => {
    hideUnhideSections(frm);
    populateForm(frm);
  },

  transaction_immovable: frm => {
    loadImmovableAsset(frm);
  },

  transaction_motor: frm => {
    loadMotorVehicle(frm);
  },

  transaction_intangible: frm => {
    loadIntangibleAsset(frm);
  },

  transaction_financial: frm => {
    loadFinancialAsset(frm);
  },

  transaction_assignment: frm => {
    loadAssignmentOfRights(frm);
  },

  transaction_guarantee: frm => {
    loadGuarantee(frm);
  },

  transaction_movable_fixed: frm => {
    loadMovableFixedAsset(frm);
  },

  transaction_current: frm => {
    loadCurrentAsset(frm);
  },

  transaction_dsra: frm => {
    loadDsra(frm);
  },
});

function fileUpload(frm) {
  $(frm.fields_dict.document_type.wrapper).empty();
  frm.refresh_field('document_type');
  setTimeout(() => {
    const file = ' ';
    renderAttachmentTemplate(frm, file);
  }, 500);
}
function renderAttachmentTemplate(frm, file) {
  frm.set_df_property(
    'document_type',
    'options',
    frappe.render_template('transaction_details_attachment', {
      doc: frm.doc,
      id: frm.doc.name,
      docstatus: frm.doc.docstatus,
    }),
  );
  frm.refresh_field('document_type');
  addUploadEvents(frm);
}
let details = {};
let fileIndex = 0;
function addUploadEvents(frm) {
  addTransactionDetailsAttachFileEvent(frm);
  addTransactionDetailsUploadAllEvent(frm);
}

function addTransactionDetailsAttachFileEvent(frm) {
  fileIndex = 0;
  details = {};
  let s3ButtonIndex;
  $('.' + frm.doc.name + '-attach-file').on('click', function () {
    s3ButtonIndex = 0;
    $('.' + frm.doc.name + '-attach-input')[s3ButtonIndex].click();
  });
  if (frm.doc.file_name) {
    const fileNames = frm.doc.file_name.split(',');
    for (let i = 0; i < fileNames.length; i++) {
      if (fileNames[i] !== '') {
        const rowName = 'f_' + fileIndex;
        $('#file_list').append(
          '<tr id="' +
            rowName +
            '"><td><a id="' +
            fileNames[i] +
            '" class="' +
            frm.doc.name +
            '-s3-download" style="display: inline-block; ">' +
            fileNames[i] +
            '</a>&nbsp;<a alt="' +
            fileNames[i] +
            '" class="' +
            frm.doc.name +
            '-delete-file"><span>' +
            frappe.utils.icon('delete', 'sm') +
            '</a></td></tr>',
        );
      }
    }
    addDownloadEvents(frm);
    addTransactionDetailsDeleteFileEvent(frm);
  }
  $('.' + frm.doc.name + '-attach-input').change(async event => {
    const files = [...event.target.files];

    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      if (file) {
        event.target.value = '';

        const rowName = 'f_' + fileIndex;

        $('#file_list').append(
          '<tr id="' +
            rowName +
            '"><td><a class="' +
            frm.doc.name +
            '-preview-file" style="display: inline-block; ">' +
            file.name +
            '</a>&nbsp;<a class="' +
            frm.doc.name +
            '-remove-file"><span>' +
            frappe.utils.icon('close', 'sm') +
            '</a></td></tr>',
        );

        details[rowName] = {};
        details[rowName].file = await getBase64(file);
        details[rowName].file_name = file.name;
        addTransactionDetailsPreviewEvent(frm);
        addTransactionDetailsRemoveFileEvent(frm);
        fileIndex++;
      }
    }
  });
}

function addDownloadEvents(frm) {
  $('.' + frm.doc.name + '-s3-download').on('click', function () {
    let s3Key = this.id;
    if (s3Key) {
      s3Key =
        window.location.hostname +
        '/Transaction Details/' +
        frm.doc.name +
        '/' +
        s3Key;
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: s3Key,
      });
    }
  });
}
function addTransactionDetailsPreviewEvent(frm) {
  $('.' + frm.doc.name + '-preview-file').on('click', function () {
    const rowName = $(this).closest('tr').attr('id');

    const blob = convertBase64ToBlob(details[rowName].file);
    const blobUrl = URL.createObjectURL(blob);

    window.open(blobUrl, '_blank');
  });
}
function addTransactionDetailsRemoveFileEvent(frm) {
  $('.' + frm.doc.name + '-remove-file').on('click', function () {
    const rowName = $(this).closest('tr').attr('id');
    $(this).parent().remove();
    details[rowName] = {};
  });
}
function addTransactionDetailsDeleteFileEvent(frm) {
  $('.' + frm.doc.name + '-delete-file').on('click', function () {
    const rowName = $(this).closest('tr').attr('id');

    const fileName = $(this).attr('alt');
    $(this).parent().remove();
    details[rowName] = {};

    frappe.call({
      method:
        'trusteeship_platform.custom_methods.transaction_details_delete_file',
      freeze: true,
      args: {
        docname: frm.doc.name,
        fileName,
        doctype: 'Transaction Details',
      },
      callback: r => {
        frm.reload_doc().then(() => {
          setTimeout(() => {
            frm.fields_dict.unencumbered_section.collapse(false);
          }, 500);
        });
        frappe.show_alert(
          {
            message: 'Deleted',
            indicator: 'Red',
          },
          5,
        );
      },
    });
  });
}
function addTransactionDetailsUploadAllEvent(frm) {
  $('.' + frm.doc.name + '-upload-all').on('click', () => {
    if (Object.keys(details).length > 0) {
      const transactionDetails = JSON.stringify(details);
      frappe.call({
        method:
          'trusteeship_platform.custom_methods.transaction_details_s3_upload',
        freeze: true,
        args: {
          docname: frm.doc.name,
          details: transactionDetails,
          doctype: 'Transaction Details',
        },
        callback: r => {
          frm.reload_doc().then(() => {
            setTimeout(() => {
              frm.fields_dict.unencumbered_section.collapse(false);
            }, 500);
          });

          frappe.show_alert(
            {
              message: 'Uploaded',
              indicator: 'green',
            },
            5,
          );
        },
      });
    } else {
      frappe.throw({
        message: 'No Attachments found.',
        title: 'Missing Attachments',
      });
    }
  });
}
function convertBase64ToBlob(base64Image) {
  // Split into two parts
  const parts = base64Image.split(';base64,');

  // Hold the content type
  const imageType = parts[0].split(':')[1];

  // Decode Base64 string
  const decodedData = window.atob(parts[1]);

  // Create UNIT8ARRAY of size same as row data length
  const uInt8Array = new Uint8Array(decodedData.length);

  // Insert all character code into uInt8Array
  for (let i = 0; i < decodedData.length; ++i) {
    uInt8Array[i] = decodedData.charCodeAt(i);
  }

  // Return BLOB image after conversion
  return new Blob([uInt8Array], { type: imageType });
}

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}
function populateForm(frm) {
  if (frm.doc.operations) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'ATSL Mandate List',
        doc_name: frm.doc.operations,
      },
      freeze: true,
      callback: r => {
        frm.set_value('cnp_customer_code', r.message.cnp_customer_code);
        frm.set_value('product_name', r.message.product_name);
        frm.set_value('person_name', r.message.person_name);
        frm.set_value('mandate_name', r.message.mandate_name);
        frm.set_value('opportunity', r.message.opportunity);
        frm.set_value('quotation', r.message.quotation);
      },
    });
  }
}
function fetchOpportunityField(frm) {
  if (frm.doc.opportunity) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Opportunity',
        doc_name: frm.doc.opportunity,
      },
      freeze: true,
      callback: r => {
        frm.set_value('name_of_trust', r.message.cnp_name_of_trust);
        frm.set_value('no_of_spvs', r.message.cnp_number_of_spv);
        frm.set_value('number_of_projects', r.message.cnp_number_of_projects);
        frm.set_value('product_type', r.message.cnp_product_type);
        frm.set_value(
          'trust_size_in_crores',
          r.message.cnp_trust_size_in_crores,
        );
        frm.set_value('sector', r.message.cnp_sectors);
        frm.set_value('initial_fee', r.message.cnp_initial_fee);
        frm.set_value('annual_fee', r.message.cnp_annual_fee);
        frm.set_value('one_time_fee', r.message.cnp_one_time_fee);
      },
    });
  }
}
function hideUnhideSections(frm) {
  frm.fields_dict.transaction_immovable.tab.df.hidden = true;
  frm.fields_dict.transaction_motor.tab.df.hidden = true;
  frm.fields_dict.transaction_intangible.tab.df.hidden = true;
  frm.fields_dict.transaction_financial.tab.df.hidden = true;
  frm.fields_dict.transaction_assignment.tab.df.hidden = true;
  frm.fields_dict.transaction_guarantee.tab.df.hidden = true;
  frm.fields_dict.transaction_movable_fixed.tab.df.hidden = true;
  frm.fields_dict.transaction_current.tab.df.hidden = true;
  frm.fields_dict.transaction_dsra.tab.df.hidden = true;
  frm.refresh_fields();
  // frm.toggle_display("immovable_asset_section", false);
  // frm.toggle_display("motor_vehicle_section", false);
  // frm.toggle_display("intangible_asset_section", false);
  // frm.toggle_display("financial_asset_section", false);
  // frm.toggle_display("assignment_of_rights_section", false);
  // frm.toggle_display("guarantee_section", false);
  // frm.toggle_display("movable_fixed_asset_section", false);
  // frm.toggle_display("current_asset_section", false);
  // frm.toggle_display("debt_service_reserve_account_dsra_section", false);
  if (frm.doc.operations) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'ATSL Mandate List',
        doc_name: frm.doc.operations,
      },
      freeze: true,
      callback: r => {
        for (let i = 0; i < r.message.security_type.length; i++) {
          let securityType;

          if (r.message.product_name === 'DTE') {
            frm.toggle_display('map_asset_for_the_debenture_ine_section', true);
          } else {
            frm.toggle_display(
              'map_asset_for_the_debenture_ine_section',
              false,
            );
          }

          if (r.message.security_type[i].change_in_security === 'Yes') {
            securityType = r.message.security_type[i].new_security_type;
          } else {
            securityType = r.message.security_type[i].existing_security_type;
          }

          if (securityType === 'Immovable Asset') {
            frm.fields_dict.transaction_immovable.tab.df.hidden = false;
            frm.refresh_fields();
            loadImmovableAsset(frm);
          }
          if (securityType === 'Motor Vehicle') {
            frm.fields_dict.transaction_motor.tab.df.hidden = false;
            frm.refresh_fields();
            loadMotorVehicle(frm);
          }
          if (securityType === 'Intangible Asset') {
            frm.fields_dict.transaction_intangible.tab.df.hidden = false;
            frm.refresh_fields();
            loadIntangibleAsset(frm);
          }
          if (securityType === 'Financial Asset') {
            frm.fields_dict.transaction_financial.tab.df.hidden = false;
            frm.refresh_fields();
            loadFinancialAsset(frm);
          }
          if (securityType === 'Assignment of Rights') {
            frm.fields_dict.transaction_assignment.tab.df.hidden = false;
            frm.refresh_fields();
            loadAssignmentOfRights(frm);
          }
          if (securityType === 'Corporate Guarantee') {
            frm.fields_dict.transaction_guarantee.tab.df.hidden = false;
            frm.refresh_fields();
            loadGuarantee(frm);
          }
          if (securityType === 'Movable Asset') {
            frm.fields_dict.transaction_movable_fixed.tab.df.hidden = false;
            frm.refresh_fields();
            loadMovableFixedAsset(frm);
          }
          if (securityType === 'Current Asset') {
            frm.fields_dict.transaction_current.tab.df.hidden = false;
            frm.refresh_fields();
            loadCurrentAsset(frm);
          }
          if (securityType === 'Debt Service Reserve Account (DSRA)') {
            frm.fields_dict.transaction_dsra.tab.df.hidden = false;
            frm.refresh_fields();
            loadDsra(frm);
          }
        }
      },
    });
  }
}

function setFilters(frm) {
  // immovable
  frm.set_query('transaction_immovable', filter => {
    return {
      filters: [
        [
          'Transaction Details Immovable Asset',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // motor vehicle
  frm.set_query('transaction_motor', filter => {
    return {
      filters: [
        [
          'Transaction Details Motor Vehicle',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // intangible
  frm.set_query('transaction_intangible', filter => {
    return {
      filters: [
        [
          'Transaction Details Intangible Asset',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // financial
  frm.set_query('transaction_financial', filter => {
    return {
      filters: [
        [
          'Transaction Details Financial Asset',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // assignment of rights
  frm.set_query('transaction_assignment', filter => {
    return {
      filters: [
        [
          'Transaction Details Assignment of Rights',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // guarantee
  frm.set_query('transaction_guarantee', filter => {
    return {
      filters: [
        [
          'Transaction Details Guarantee',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // movable fixed
  frm.set_query('transaction_movable_fixed', filter => {
    return {
      filters: [
        [
          'Transaction Details Movable Fixed Asset',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // current
  frm.set_query('transaction_current', filter => {
    return {
      filters: [
        [
          'Transaction Details Current Asset',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });

  // dsra
  frm.set_query('transaction_dsra', filter => {
    return {
      filters: [
        [
          'Transaction Details DSRA',
          'cnp_customer_code',
          '=',
          frm.doc.cnp_customer_code,
        ],
      ],
    };
  });
}

function loadImmovableAsset(frm) {
  $(frm.fields_dict.immovable_asset_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_immovable) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Immovable Asset',
          doc_name: frm.doc.transaction_immovable,
        },
        freeze: true,
        callback: r => {
          immovableAsset = r.message;
          frm.set_df_property(
            'immovable_asset_html',
            'options',
            frappe.render_template('tr_immovable_asset', {
              doc: immovableAsset,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('immovable_asset_html');
          addImmovableAssetEvent(frm);
        },
      });
    }
  }
}

function loadMotorVehicle(frm) {
  $(frm.fields_dict.motor_vehicle_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_motor) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Motor Vehicle',
          doc_name: frm.doc.transaction_motor,
        },
        freeze: true,
        callback: r => {
          motorVehicle = r.message;
          frm.set_df_property(
            'motor_vehicle_html',
            'options',
            frappe.render_template('motor_vehicle', {
              doc: motorVehicle,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('motor_vehicle_html');
          addMotorVehicleEvent(frm);
        },
      });
    }
  }
}

function loadIntangibleAsset(frm) {
  $(frm.fields_dict.intangible_asset_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_intangible) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Intangible Asset',
          doc_name: frm.doc.transaction_intangible,
        },
        freeze: true,

        callback: r => {
          intangibleAsset = r.message;

          frm.set_df_property(
            'intangible_asset_html',
            'options',
            frappe.render_template('intangible_asset', {
              doc: intangibleAsset,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('intangible_asset_html');
          addIntangibleAssetEvent(frm);
        },
      });
    }
  }
}

function loadFinancialAsset(frm) {
  $(frm.fields_dict.financial_asset_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_financial) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Financial Asset',
          doc_name: frm.doc.transaction_financial,
        },
        freeze: true,

        callback: r => {
          financialAsset = r.message;
          frm.set_df_property(
            'financial_asset_html',
            'options',
            frappe.render_template('financial_asset', {
              doc: financialAsset,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('financial_asset_html');
          addFinancialAssetEvent(frm);
        },
      });
    }
  }
}

function loadAssignmentOfRights(frm) {
  $(frm.fields_dict.assignment_of_rights_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_assignment) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Assignment of Rights',
          doc_name: frm.doc.transaction_assignment,
        },
        freeze: true,

        callback: r => {
          assignmentOfRights = r.message;
          frm.set_df_property(
            'assignment_of_rights_html',
            'options',
            frappe.render_template('assignment_of_rights', {
              doc: assignmentOfRights,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('assignment_of_rights_html');
          addAssignmentOfRightsEvent(frm);
        },
      });
    }
  }
}

function loadGuarantee(frm) {
  $(frm.fields_dict.guarantee_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_guarantee) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Guarantee',
          doc_name: frm.doc.transaction_guarantee,
        },
        freeze: true,
        callback: r => {
          guarantee = r.message;
          frm.set_df_property(
            'guarantee_html',
            'options',
            frappe.render_template('guarantee', {
              doc: guarantee,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('guarantee_html');
          addGuaranteeEvent(frm);
        },
      });
    }
  }
}

function loadMovableFixedAsset(frm) {
  $(frm.fields_dict.movable_fixed_asset_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_movable_fixed) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Movable Fixed Asset',
          doc_name: frm.doc.transaction_movable_fixed,
        },
        freeze: true,

        callback: r => {
          movableFixedAsset = r.message;
          $(frm.fields_dict.movable_fixed_asset_html.wrapper).empty();
          frm.set_df_property(
            'movable_fixed_asset_html',
            'options',
            frappe.render_template('movable_fixed_asset', {
              doc: movableFixedAsset,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('movable_fixed_asset_html');
          addMovableFixedAssetEvent(frm);
        },
      });
    }
  }
}

function loadCurrentAsset(frm) {
  $(frm.fields_dict.current_asset_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_current) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details Current Asset',
          doc_name: frm.doc.transaction_current,
        },
        freeze: true,

        callback: r => {
          currentAsset = r.message;
          frm.set_df_property(
            'current_asset_html',
            'options',
            frappe.render_template('current_asset', {
              doc: currentAsset,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('current_asset_html');
          addCurrentAssetEvent(frm);
        },
      });
    }
  }
}

function loadDsra(frm) {
  $(frm.fields_dict.dsra_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    if (frm.doc.transaction_dsra) {
      frappe.call({
        method: 'trusteeship_platform.custom_methods.get_doc',
        args: {
          doc_type: 'Transaction Details DSRA',
          doc_name: frm.doc.transaction_dsra,
        },
        freeze: true,

        callback: r => {
          dsra = r.message;
          frm.set_df_property(
            'dsra_html',
            'options',
            frappe.render_template('dsra', {
              doc: dsra,
              id: frm.doc.name,
              docstatus: frm.doc.docstatus,
            }),
          );
          frm.refresh_field('dsra_html');
          addDsraEvent(frm);
        },
      });
    }
  }
}

function loadAssetsOwnerDetails(frm) {
  $(frm.fields_dict.assets_owner_details_html.wrapper).empty();
  if (!frm.doc.__islocal) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: {
        doctype: 'Transaction Details Assets Owner Details',
        filters: [['transaction_details_name', '=', frm.doc.name]],
      },
      freeze: true,

      callback: r => {
        assetsOwnerDetails = r.message[0];

        if (r.message.length === 0) {
          assetsOwnerDetails = {};
        }

        $(frm.fields_dict.assets_owner_details_html.wrapper).empty();

        const assetsOwnerDetailsTemplate = $(
          frm.fields_dict.assets_owner_details_html.wrapper,
        ).html(
          frappe.render_template('assets_owner_details', {
            doc: assetsOwnerDetails,
            id: frm.doc.name,
            docstatus: frm.doc.docstatus,
          }),
        );
        assetsOwnerDetailsTemplate
          .find('.' + frm.doc.name + '-assets_owner_details-add')
          .unbind();
        assetsOwnerDetailsTemplate
          .find('.' + frm.doc.name + '-assets_owner_details-add')
          .on('click', function () {
            addAssetsOwnerDetails(frm);
          });
        frm.refresh_field('assets_owner_details_html');
      },
    });
  }
}

function getAssetSubType(curDialog, assetType, fieldName) {
  if (assetType !== '' && assetType !== undefined) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: assetType,
        doc_type: 'Canopi Asset Type',
      },
      freeze: true,
      callback: r => {
        const assetSubType = r.message.asset_sub_type;
        const options = assetSubType.map(x => x.asset_sub_type);
        curDialog.set_df_property(fieldName, 'options', options);
      },
    });
  }
}

function addImmovableAssetEvent(frm) {
  $('.' + frm.doc.name + '_immovable_asset_edit').unbind();
  $('.' + frm.doc.name + '_immovable_asset_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Immovable Asset',
      fields: [
        {
          label: 'As on date',
          options: '',
          reqd: 0,
          fieldname: 'as_on_date',
          fieldtype: 'Date',
          default: immovableAsset.as_on_date,
        },
        {
          label: 'Asset Type',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'asset_type',
          fieldtype: 'Link',
          default: immovableAsset.asset_type,
          onchange: () => {
            if (cur_dialog?.get_field('asset_type')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('asset_type')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: immovableAsset.asset_sub_type,
        },
        {
          label: 'Property Type',
          options: [],
          reqd: 0,
          fieldname: 'property_type',
          fieldtype: 'Select',
          default: immovableAsset.property_type,
        },
        {
          label: 'Asset Short Name',
          options: '',
          reqd: 0,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: immovableAsset.asset_short_name
            ? immovableAsset.asset_short_name
            : '',
        },
        {
          label: 'Percentage of Ownership',
          options: '',
          reqd: 0,
          fieldname: 'percentage_of_ownership',
          fieldtype: 'Float',
          default: immovableAsset.percentage_of_ownership
            ? immovableAsset.percentage_of_ownership
            : '',
        },
        {
          label: 'Insurance Policy Number',
          options: '',
          reqd: 0,
          fieldname: 'insurance_policy_number',
          fieldtype: 'Data',
          default: immovableAsset.insurance_policy_number
            ? immovableAsset.insurance_policy_number
            : '',
        },
        {
          label: 'Name of Insurance company',
          options: '',
          reqd: 0,
          fieldname: 'name_of_insurance_company',
          fieldtype: 'Data',
          default: immovableAsset.name_of_insurance_company
            ? immovableAsset.name_of_insurance_company
            : '',
        },
        {
          label: 'Amount',
          options: '',
          reqd: 0,
          fieldname: 'amount',
          fieldtype: 'Data',
          default: immovableAsset.amount ? immovableAsset.amount : '',
        },
        {
          label: 'Beneficiary Name',
          options: '',
          reqd: 0,
          fieldname: 'beneficiary_name',
          fieldtype: 'Data',
          default: immovableAsset.beneficiary_name
            ? immovableAsset.beneficiary_name
            : '',
        },
        {
          label: 'Period of Insurance',
          options: '',
          reqd: 0,
          fieldname: 'period_of_insurance',
          fieldtype: 'Data',
          default: immovableAsset.period_of_insurance
            ? immovableAsset.period_of_insurance
            : '',
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Area of property',
          options: '',
          reqd: 0,
          fieldname: 'area_of_property',
          fieldtype: 'Data',
          default: immovableAsset.area_of_property
            ? immovableAsset.area_of_property
            : '',
        },
        {
          label: 'Area Unit',
          options: '',
          reqd: 0,
          fieldname: 'area_unit',
          fieldtype: 'Data',
          default: immovableAsset.area_unit ? immovableAsset.area_unit : '',
        },
        {
          label: 'Negative Lien on Asset',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_asset',
          fieldtype: 'Select',
          default: immovableAsset.negative_lien_on_asset,
        },
        {
          label: 'NSDL',
          options: [],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: immovableAsset.nsdl,
        },
        {
          label: 'Code in FAR',
          options: [],
          reqd: 0,
          fieldname: 'code_in_far',
          fieldtype: 'Select',
          default: immovableAsset.code_in_far,
        },
        {
          label: 'Code Description in FAR',
          options: '',
          reqd: 0,
          fieldname: 'code_description_in_far',
          fieldtype: 'Data',
          default: immovableAsset.code_description_in_far,
        },
        {
          label: 'Latitude/Longitude',
          options: '',
          reqd: 0,
          fieldname: 'latitudelongitude',
          fieldtype: 'Data',
          default: immovableAsset.latitudelongitude,
        },
        {
          label: 'Registration Number',
          options: [],
          reqd: 0,
          fieldname: 'registration_number',
          fieldtype: 'Select',
          default: immovableAsset.registration_number,
        },
        {
          label: 'Type of Cover',
          options: [],
          reqd: 0,
          fieldname: 'type_of_cover',
          fieldtype: 'Select',
          default: immovableAsset.type_of_cover,
        },
        {
          label: 'Details of the Tenancy/Lease/Licence',
          options: '',
          reqd: 0,
          fieldname: 'details_of_the_tenancyleaselicence',
          fieldtype: 'Data',
          default: immovableAsset.details_of_the_tenancyleaselicence,
        },
        {
          label: 'Value of Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: immovableAsset.value_of_asset_as_per_balance_sheet,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_immovable_asset',
          args: {
            docname: frm.doc.transaction_immovable,
            as_on_date: values.as_on_date,
            asset_type: values.asset_type,
            asset_sub_type: values.asset_sub_type,
            property_type: values.property_type,
            asset_short_name: values.asset_short_name,
            percentage_of_ownership: values.percentage_of_ownership,
            insurance_policy_number: values.insurance_policy_number,
            name_of_insurance_company: values.name_of_insurance_company,
            amount: values.amount,
            beneficiary_name: values.beneficiary_name,
            period_of_insurance: values.period_of_insurance,
            area_of_property: values.area_of_property,
            area_unit: values.area_unit,
            negative_lien_on_asset: values.negative_lien_on_asset,
            nsdl: values.nsdl,
            code_in_far: values.code_in_far,
            code_description_in_far: values.code_description_in_far,
            latitudelongitude: values.latitudelongitude,
            registration_number: values.registration_number,
            type_of_cover: values.type_of_cover,
            details_of_the_tenancyleaselicence:
              values.details_of_the_tenancyleaselicence,
            value_of_asset_as_per_balance_sheet:
              values.value_of_asset_as_per_balance_sheet,
          },
          freeze: true,
          callback: r => {
            loadImmovableAsset(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_immovable');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, immovableAsset.asset_type, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, immovableAsset.asset_type, 'asset_sub_type');
    }
  });
}

function addMotorVehicleEvent(frm) {
  $('.' + frm.doc.name + '_motor_vehicle_edit').unbind();
  $('.' + frm.doc.name + '_motor_vehicle_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Motor Vehicle',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: motorVehicle.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: motorVehicle.asset_sub_type,
        },
        {
          label: 'Asset Short Name',
          reqd: 1,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: motorVehicle.asset_short_name,
        },
        {
          label: 'Asset Description',
          reqd: 1,
          fieldname: 'asset_description',
          fieldtype: 'Data',
          default: motorVehicle.asset_description,
        },
        {
          label: 'Vehical Number',
          options: '',
          reqd: 1,
          fieldname: 'vehical_number',
          fieldtype: 'Data',
          default: motorVehicle.vehical_number,
        },
        {
          label: 'Chasis Number',
          options: '',
          reqd: 1,
          fieldname: 'chasis_number',
          fieldtype: 'Data',
          default: motorVehicle.chasis_number,
        },
        {
          label: 'Valuation Agency',
          options: '',
          reqd: 1,
          fieldname: 'valuation_agency',
          fieldtype: 'Data',
          default: motorVehicle.valuation_agency,
        },
        {
          label: 'Valuation Amount',
          options: '',
          reqd: 1,
          fieldname: 'valuation_amount',
          fieldtype: 'Data',
          default: motorVehicle.valuation_amount,
        },
        {
          label: 'Valuation Date',
          options: '',
          reqd: 1,
          fieldname: 'valuation_date',
          fieldtype: 'Date',
          default: motorVehicle.valuation_date,
        },
        {
          label: 'Existing Document List',
          options: [],
          reqd: 0,
          fieldname: 'beneficiary_name',
          fieldtype: 'Select',
          default: motorVehicle.existing_document_list,
        },
        {
          label: 'Type of Cover',
          options: [],
          reqd: 0,
          fieldname: 'period_of_insurance',
          fieldtype: 'Select',
          default: motorVehicle.type_of_cover,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Negative Lien on Asset',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_asset',
          fieldtype: 'Select',
          default: motorVehicle.negative_lien_on_asset,
        },
        {
          label: 'NSDL',
          options: [],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: motorVehicle.nsdl,
        },
        {
          label: 'Code in FAR',
          options: [],
          reqd: 0,
          fieldname: 'code_in_far',
          fieldtype: 'Select',
          default: motorVehicle.code_in_far,
        },
        {
          label: 'Code Description in FAR',
          options: '',
          reqd: 0,
          fieldname: 'code_description_in_far',
          fieldtype: 'Data',
          default: motorVehicle.code_description_in_far,
        },
        {
          label: 'Registration Number',
          options: [],
          reqd: 0,
          fieldname: 'registration_number',
          fieldtype: 'Select',
          default: motorVehicle.registration_number,
        },
        {
          label: 'Insurance Policy Number',
          options: '',
          reqd: 0,
          fieldname: 'insurance_policy_number',
          fieldtype: 'Data',
          default: motorVehicle.insurance_policy_number,
        },
        {
          label: 'Name of Insurance company',
          options: '',
          reqd: 0,
          fieldname: 'name_of_insurance_company',
          fieldtype: 'Data',
          default: motorVehicle.name_of_insurance_company,
        },
        {
          label: 'Amount',
          reqd: 0,
          fieldname: 'amount',
          fieldtype: 'Data',
          default: motorVehicle.amount,
        },
        {
          label: 'Beneficiary Name',
          reqd: 0,
          fieldname: 'beneficiary_name',
          fieldtype: 'Data',
          default: motorVehicle.beneficiary_name,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: motorVehicle.value_of_the_asset_as_per_balance_sheet,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_motor_vehicle',
          args: {
            docname: frm.doc.transaction_motor,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            asset_short_name: values.asset_short_name,
            asset_description: values.asset_description,
            vehical_number: values.vehical_number,
            chasis_number: values.chasis_number,
            valuation_agency: values.valuation_agency,
            valuation_amount: values.valuation_amount,
            valuation_date: values.valuation_date,
            existing_document_list: values.existing_document_list,
            type_of_cover: values.type_of_cover,
            negative_lien_on_asset: values.negative_lien_on_asset,
            nsdl: values.nsdl,
            code_in_far: values.code_in_far,
            code_description_in_far: values.code_description_in_far,
            registration_number: values.registration_number,
            insurance_policy_number: values.insurance_policy_number,
            name_of_insurance_company: values.name_of_insurance_company,
            amount: values.amount,
            beneficiary_name: values.beneficiary_name,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
          },
          freeze: true,
          callback: r => {
            loadMotorVehicle(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });

    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_motor');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, motorVehicle.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, motorVehicle.type_of_asset, 'asset_sub_type');
    }
  });
}

function addIntangibleAssetEvent(frm) {
  $('.' + frm.doc.name + '_intangible_asset_edit').unbind();
  $('.' + frm.doc.name + '_intangible_asset_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Intangible Asset',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: intangibleAsset.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: intangibleAsset.asset_sub_type,
        },
        {
          label: 'Intangible Asset Type',
          options: [],
          reqd: 0,
          fieldname: 'intangible_asset_type',
          fieldtype: 'Select',
          default: intangibleAsset.intangible_asset_type,
        },
        {
          label: 'Asset Short Name',
          reqd: 0,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: intangibleAsset.asset_short_name,
        },
        {
          label: 'IPR Details',
          options: '',
          reqd: 0,
          fieldname: 'ipr_details',
          fieldtype: 'Data',
          default: intangibleAsset.ipr_details,
        },
        {
          label: 'Trademark ID/Copyrights ID',
          options: '',
          reqd: 1,
          fieldname: 'trademark_idcopyrights_id',
          fieldtype: 'Data',
          default: intangibleAsset.trademark_idcopyrights_id,
        },
        {
          label: 'Owner Name',
          options: '',
          reqd: 1,
          fieldname: 'owner_name',
          fieldtype: 'Data',
          default: intangibleAsset.owner_name,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Valuation Amount',
          reqd: 0,
          fieldname: 'valuation_amount',
          fieldtype: 'Data',
          default: intangibleAsset.valuation_amount,
        },

        {
          label: 'Valuation Agency',
          reqd: 0,
          fieldname: 'valuation_agency',
          fieldtype: 'Data',
          default: intangibleAsset.valuation_agency,
        },
        {
          label: 'Valuation Date',
          reqd: 0,
          fieldname: 'valuation_date',
          fieldtype: 'Date',
          default: intangibleAsset.valuation_date,
        },
        {
          label: 'Registration Number',
          options: [],
          reqd: 0,
          fieldname: 'registration_number',
          fieldtype: 'Select',
          default: intangibleAsset.registration_number,
        },

        {
          label: 'Negative Lien on Asset',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_asset',
          fieldtype: 'Select',
          default: intangibleAsset.negative_lien_on_asset,
        },
        {
          label: 'NSDL',
          options: [],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: intangibleAsset.nsdl,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: intangibleAsset.value_of_the_asset_as_per_balance_sheet,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_intangible_asset',
          args: {
            docname: frm.doc.transaction_intangible,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            intangible_asset_type: values.intangible_asset_type,
            asset_short_name: values.asset_short_name,
            ipr_details: values.ipr_details,
            trademark_idcopyrights_id: values.trademark_idcopyrights_id,
            owner_name: values.owner_name,
            valuation_amount: values.valuation_amount,
            valuation_agency: values.valuation_agency,
            valuation_date: values.valuation_date,
            registration_number: values.registration_number,
            negative_lien_on_asset: values.negative_lien_on_asset,
            nsdl: values.nsdl,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
          },
          freeze: true,
          callback: r => {
            loadIntangibleAsset(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });

    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_intangible');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, intangibleAsset.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, intangibleAsset.type_of_asset, 'asset_sub_type');
    }
  });
}

function addFinancialAssetEvent(frm) {
  $('.' + frm.doc.name + '_financial_asset_edit').unbind();
  $('.' + frm.doc.name + '_financial_asset_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Financial Asset',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: financialAsset.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'sub_asset_type',
              );
            }
          },
        },
        {
          label: 'Sub Asset Type',
          options: [],
          reqd: 0,
          fieldname: 'sub_asset_type',
          fieldtype: 'Select',
          default: financialAsset.sub_asset_type,
        },
        {
          label: 'Coverage',
          reqd: 0,
          fieldname: 'coverage',
          fieldtype: 'Data',
          default: financialAsset.coverage,
        },
        {
          label: 'Asset Short Name',
          options: '',
          reqd: 0,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: financialAsset.asset_short_name,
        },
        {
          label: 'Asset Description',
          options: '',
          reqd: 0,
          fieldname: 'asset_description',
          fieldtype: 'Data',
          default: financialAsset.asset_description,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Valuation',
          reqd: 0,
          fieldname: 'valuation',
          fieldtype: 'Data',
          default: financialAsset.valuation,
        },

        {
          label: 'Valuation Date',
          reqd: 0,
          fieldname: 'valuation_date',
          fieldtype: 'Date',
          default: financialAsset.valuation_date,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: financialAsset.value_of_the_asset_as_per_balance_sheet,
        },
        {
          label: 'NSDL',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: financialAsset.nsdl,
        },
        {
          label: 'Remarks',
          options: '',
          reqd: 0,
          fieldname: 'remarks',
          fieldtype: 'Small Text',
          default: financialAsset.remarks,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_financial_asset',
          args: {
            docname: frm.doc.transaction_financial,
            type_of_asset: values.type_of_asset,
            sub_asset_type: values.sub_asset_type,
            asset_short_name: values.asset_short_name,
            coverage: values.coverage,
            asset_description: values.asset_description,
            valuation: values.valuation,
            valuation_date: values.valuation_date,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
            nsdl: values.nsdl,
            remarks: values.remarks,
          },
          freeze: true,
          callback: r => {
            loadFinancialAsset(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });

    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_financial');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, financialAsset.type_of_asset, 'sub_asset_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, financialAsset.type_of_asset, 'sub_asset_type');
    }
  });
}

function addAssignmentOfRightsEvent(frm) {
  $('.' + frm.doc.name + '_assignment_of_rights_edit').unbind();
  $('.' + frm.doc.name + '_assignment_of_rights_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Assignment Of Rights',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: assignmentOfRights.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: assignmentOfRights.asset_sub_type,
        },
        {
          label: 'Asset Description',
          options: '',
          reqd: 0,
          fieldname: 'asset_description',
          fieldtype: 'Data',
          default: assignmentOfRights.asset_description,
        },
        {
          label: 'Asset Value',
          options: '',
          reqd: 0,
          fieldname: 'asset_value',
          fieldtype: 'Data',
          default: assignmentOfRights.asset_value,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: assignmentOfRights.value_of_the_asset_as_per_balance_sheet,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Negative Lien on Asset',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_asset',
          fieldtype: 'Select',
          default: assignmentOfRights.negative_lien_on_asset,
        },

        {
          label: 'NSDL',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: assignmentOfRights.nsdl,
        },
        {
          label: 'Remarks',
          reqd: 0,
          fieldname: 'remarks',
          fieldtype: 'Data',
          default: assignmentOfRights.remarks,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_assignment_of_rights',
          args: {
            docname: frm.doc.transaction_assignment,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            asset_description: values.asset_description,
            asset_value: values.asset_value,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
            negative_lien_on_asset: values.negative_lien_on_asset,
            nsdl: values.nsdl,
            remarks: values.remarks,
          },
          freeze: true,
          callback: r => {
            loadAssignmentOfRights(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_assignment');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, assignmentOfRights.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, assignmentOfRights.type_of_asset, 'asset_sub_type');
    }
  });
}

function addGuaranteeEvent(frm) {
  $('.' + frm.doc.name + '_guarantee_edit').unbind();
  $('.' + frm.doc.name + '_guarantee_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Guarantee',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: guarantee.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: guarantee.asset_sub_type,
        },
        {
          label: 'PAN Number',
          reqd: 0,
          fieldname: 'pan_number',
          fieldtype: 'Data',
          default: guarantee.pan_number,
        },
        {
          label: 'Asset Short Name',
          options: '',
          reqd: 0,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: guarantee.asset_short_name,
        },
        {
          label: 'Guarantor Type',
          options: '',
          reqd: 0,
          fieldname: 'guarantor_type',
          fieldtype: 'Data',
          default: guarantee.guarantor_type,
        },
        {
          label: 'Certificate Date',
          options: '',
          reqd: 0,
          fieldname: 'certificate_date',
          fieldtype: 'Date',
          default: guarantee.certificate_date,
        },
        {
          label: 'Guarantor Name',
          options: '',
          reqd: 0,
          fieldname: 'guarantor_name',
          fieldtype: 'Data',
          default: guarantee.guarantor_name,
        },
        {
          label: 'Address of Guarantor',
          options: '',
          reqd: 0,
          fieldname: 'address_of_guarantor',
          fieldtype: 'Data',
          default: guarantee.address_of_guarantor,
        },
        {
          label: 'Net worth amount',
          options: '',
          reqd: 0,
          fieldname: 'net_worth_amount',
          fieldtype: 'Data',
          default: guarantee.net_worth_amount,
        },

        {
          label: 'Valuation Agency',
          options: '',
          reqd: 0,
          fieldname: 'valuation_agency',
          fieldtype: 'Data',
          default: guarantee.valuation_agency,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Guarantee Provided to',
          reqd: 0,
          fieldname: 'guarantee_provided_to',
          fieldtype: 'Data',
          default: guarantee.guarantee_provided_to,
        },

        {
          label: 'Guarantee Amount',
          reqd: 0,
          fieldname: 'guarantee_amount',
          fieldtype: 'Data',
          default: guarantee.guarantee_amount,
        },
        {
          label: 'Documents Upload',
          options: ['Networth'],
          reqd: 0,
          fieldname: 'documents_upload',
          fieldtype: 'Select',
          default: guarantee.documents_upload,
        },
        {
          label: 'Negative Lien on Asset',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_asset',
          fieldtype: 'Select',
          default: guarantee.negative_lien_on_asset,
        },
        {
          label: 'NSDL',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: guarantee.nsdl,
        },
        {
          label: 'Remarks',
          options: '',
          reqd: 0,
          fieldname: 'remarks',
          fieldtype: 'Small Text',
          default: guarantee.remarks,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: guarantee.value_of_the_asset_as_per_balance_sheet,
        },
        {
          label: 'Government Order Id',
          options: '',
          reqd: 0,
          fieldname: 'government_order_id',
          fieldtype: 'Data',
          default: guarantee.government_order_id,
        },
      ],

      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_guarantee',
          args: {
            docname: frm.doc.transaction_guarantee,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            pan_number: values.pan_number,
            asset_short_name: values.asset_short_name,
            guarantor_type: values.guarantor_type,
            guarantor_name: values.guarantor_name,
            address_of_guarantor: values.address_of_guarantor,
            net_worth_amount: values.net_worth_amount,
            certificate_date: values.certificate_date,
            valuation_agency: values.valuation_agency,
            guarantee_provided_to: values.guarantee_provided_to,
            guarantee_amount: values.guarantee_amount,
            documents_upload: values.documents_upload,
            negative_lien_on_asset: values.negative_lien_on_asset,
            nsdl: values.nsdl,
            remarks: values.remarks,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
            government_order_id: values.government_order_id,
          },
          freeze: true,
          callback: r => {
            validatePan(values);
            loadGuarantee(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_guarantee');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, guarantee.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, guarantee.type_of_asset, 'asset_sub_type');
    }
  });
}

function addMovableFixedAssetEvent(frm) {
  $('.' + frm.doc.name + '_movable_fixed_asset_edit').unbind();
  $('.' + frm.doc.name + '_movable_fixed_asset_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Movable Fixed Asset',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: movableFixedAsset.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Sub Asset Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: movableFixedAsset.asset_sub_type,
        },
        {
          label: 'Asset Short Name',
          options: '',
          reqd: 0,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: movableFixedAsset.asset_short_name,
        },
        {
          label: 'Asset Description',
          options: '',
          reqd: 0,
          fieldname: 'asset_description',
          fieldtype: 'Data',
          default: movableFixedAsset.asset_description,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: movableFixedAsset.value_of_the_asset_as_per_balance_sheet,
        },
        {
          label: 'Valuation Date',
          options: '',
          reqd: 0,
          fieldname: 'valuation_date',
          fieldtype: 'Date',
          default: movableFixedAsset.valuation_date,
        },
        {
          label: 'Negative Lien on Assets',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_assets',
          fieldtype: 'Select',
          default: movableFixedAsset.negative_lien_on_assets,
        },
        {
          label: 'Code in ERP/FAR',
          options: '',
          reqd: 0,
          fieldname: 'code_in_erpfar',
          fieldtype: 'Data',
          default: movableFixedAsset.code_in_erpfar,
        },
        {
          label: 'NSDL',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: movableFixedAsset.nsdl,
        },
        {
          label: 'Remarks',
          options: '',
          reqd: 0,
          fieldname: 'remarks',
          fieldtype: 'Small Text',
          default: movableFixedAsset.remarks,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Code Description in ERP',
          reqd: 0,
          fieldname: 'code_description_in_erp',
          fieldtype: 'Data',
          default: movableFixedAsset.code_description_in_erp,
        },

        {
          label: 'Code in FAR',
          options: [],
          reqd: 0,
          fieldname: 'code_in_far',
          fieldtype: 'Select',
          default: movableFixedAsset.code_in_far,
        },
        {
          label: 'Code Description in FAR',
          reqd: 0,
          fieldname: 'code_description_in_far',
          fieldtype: 'Data',
          default: movableFixedAsset.code_description_in_far,
        },
        {
          label: 'Insurance Policy Number',
          reqd: 0,
          fieldname: 'insurance_policy_number',
          fieldtype: 'Data',
          default: movableFixedAsset.insurance_policy_number,
        },
        {
          label: 'Name of Insurance company',
          options: '',
          reqd: 0,
          fieldname: 'name_of_insurance_company',
          fieldtype: 'Data',
          default: movableFixedAsset.name_of_insurance_company,
        },
        {
          label: 'Amount',
          options: '',
          reqd: 0,
          fieldname: 'amount',
          fieldtype: 'Data',
          default: movableFixedAsset.amount,
        },
        {
          label: 'Beneficiary Name',
          options: '',
          reqd: 0,
          fieldname: 'beneficiary_name',
          fieldtype: 'Data',
          default: movableFixedAsset.beneficiary_name,
        },
        {
          label: 'Type of Cover',
          options: [],
          reqd: 0,
          fieldname: 'type_of_cover',
          fieldtype: 'Select',
          default: movableFixedAsset.type_of_cover,
        },
        {
          label: 'Asset Value',
          options: '',
          reqd: 0,
          fieldname: 'asset_value',
          fieldtype: 'Data',
          default: movableFixedAsset.asset_value,
        },
        {
          label: 'Details of Tenancy/Lease/License',
          options: '',
          reqd: 0,
          fieldname: 'details_of_tenancyleaselicense',
          fieldtype: 'Data',
          default: movableFixedAsset.details_of_tenancyleaselicense,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_movable_fixed_asset',
          args: {
            docname: frm.doc.transaction_movable_fixed,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            asset_short_name: values.asset_short_name,
            asset_description: values.asset_description,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
            valuation_date: values.valuation_date,
            negative_lien_on_assets: values.negative_lien_on_assets,
            code_in_erpfar: values.code_in_erpfar,
            nsdl: values.nsdl,
            remarks: values.remarks,
            code_description_in_erp: values.code_description_in_erp,
            code_in_far: values.code_in_far,
            code_description_in_far: values.code_description_in_far,
            insurance_policy_number: values.insurance_policy_number,
            name_of_insurance_company: values.name_of_insurance_company,
            amount: values.amount,
            beneficiary_name: values.beneficiary_name,
            type_of_cover: values.type_of_cover,
            asset_value: values.asset_value,
            details_of_tenancyleaselicense:
              values.details_of_tenancyleaselicense,
          },
          freeze: true,
          callback: r => {
            loadMovableFixedAsset(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_movable_fixed');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, movableFixedAsset.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, movableFixedAsset.type_of_asset, 'asset_sub_type');
    }
  });
}

function addCurrentAssetEvent(frm) {
  $('.' + frm.doc.name + '_current_asset_edit').unbind();
  $('.' + frm.doc.name + '_current_asset_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'Current Asset',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: currentAsset.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: currentAsset.asset_sub_type,
        },
        {
          label: 'Asset Short Name',
          options: '',
          reqd: 0,
          fieldname: 'asset_short_name',
          fieldtype: 'Data',
          default: currentAsset.asset_short_name,
        },
        {
          label: 'Asset Description',
          options: '',
          reqd: 0,
          fieldname: 'asset_description',
          fieldtype: 'Data',
          default: currentAsset.asset_description,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          options: '',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: currentAsset.value_of_the_asset_as_per_balance_sheet,
        },
        {
          label: 'Valuation Date',
          options: '',
          reqd: 0,
          fieldname: 'valuation_date',
          fieldtype: 'Date',
          default: currentAsset.valuation_date,
        },
        {
          label: 'Negative Lien on Assets',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'negative_lien_on_assets',
          fieldtype: 'Select',
          default: currentAsset.negative_lien_on_assets,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },

        {
          label: 'NSDL',
          options: ['Yes', 'No'],
          reqd: 0,
          fieldname: 'nsdl',
          fieldtype: 'Select',
          default: currentAsset.nsdl,
        },
        {
          label: 'Remarks',
          options: '',
          reqd: 0,
          fieldname: 'remarks',
          fieldtype: 'Small Text',
          default: currentAsset.remarks,
        },
        {
          label: 'Insurance Policy Number',
          reqd: 0,
          fieldname: 'insurance_policy_number',
          fieldtype: 'Data',
          default: currentAsset.insurance_policy_number,
        },

        {
          label: 'Name of Insurance company',
          reqd: 0,
          fieldname: 'name_of_insurance_company',
          fieldtype: 'Data',
          default: currentAsset.name_of_insurance_company,
        },
        {
          label: 'Amount',
          reqd: 0,
          fieldname: 'amount',
          fieldtype: 'Data',
          default: currentAsset.amount,
        },
        {
          label: 'Beneficiary Name',
          reqd: 0,
          fieldname: 'beneficiary_name',
          fieldtype: 'Data',
          default: currentAsset.beneficiary_name,
        },
        {
          label: 'Type of Cover',
          options: [],
          reqd: 0,
          fieldname: 'type_of_cover',
          fieldtype: 'Select',
          default: currentAsset.type_of_cover,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_current_asset',
          args: {
            docname: frm.doc.transaction_current,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            asset_short_name: values.asset_short_name,
            asset_description: values.asset_description,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
            valuation_date: values.valuation_date,
            negative_lien_on_assets: values.negative_lien_on_assets,
            nsdl: values.nsdl,
            remarks: values.remarks,
            insurance_policy_number: values.insurance_policy_number,
            name_of_insurance_company: values.name_of_insurance_company,
            amount: values.amount,
            beneficiary_name: values.beneficiary_name,
            type_of_cover: values.type_of_cover,
          },
          freeze: true,
          callback: r => {
            loadCurrentAsset(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_current');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, currentAsset.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, currentAsset.type_of_asset, 'asset_sub_type');
    }
  });
}

function addDsraEvent(frm) {
  $('.' + frm.doc.name + '_dsra_edit').unbind();
  $('.' + frm.doc.name + '_dsra_edit').on('click', function () {
    const d = new frappe.ui.Dialog({
      title: 'DSRA',
      fields: [
        {
          label: 'Type of Asset',
          options: 'Canopi Asset Type',
          reqd: 0,
          fieldname: 'type_of_asset',
          fieldtype: 'Link',
          default: dsra.type_of_asset,
          onchange: () => {
            if (cur_dialog?.get_field('type_of_asset')?.value) {
              getAssetSubType(
                cur_dialog,
                cur_dialog?.get_field('type_of_asset')?.value,
                'asset_sub_type',
              );
            }
          },
        },
        {
          label: 'Asset Sub Type',
          options: [],
          reqd: 0,
          fieldname: 'asset_sub_type',
          fieldtype: 'Select',
          default: dsra.asset_sub_type,
        },
        {
          label: 'Value of the Asset as per Balance sheet',
          reqd: 0,
          fieldname: 'value_of_the_asset_as_per_balance_sheet',
          fieldtype: 'Data',
          default: dsra.value_of_the_asset_as_per_balance_sheet,
        },
        {
          label: '',
          options: '',
          reqd: 0,
          fieldname: 'column_break_1',
          fieldtype: 'Column Break',
        },
        {
          label: 'Escrow Account No',
          reqd: 0,
          fieldname: 'escrow_account_no',
          fieldtype: 'Data',
          default: dsra.escrow_account_no,
        },

        {
          label: 'Escrow Bank Name',
          reqd: 0,
          fieldname: 'escrow_bank_name',
          fieldtype: 'Data',
          default: dsra.escrow_bank_name,
        },
        {
          label: 'Amount to be maintained',
          reqd: 0,
          fieldname: 'amount_to_be_maintained',
          fieldtype: 'Data',
          default: dsra.amount_to_be_maintained,
        },
      ],
      primary_action_label: 'Confirm',
      primary_action: values => {
        frappe.call({
          method:
            'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_dsra',
          args: {
            docname: frm.doc.transaction_dsra,
            type_of_asset: values.type_of_asset,
            asset_sub_type: values.asset_sub_type,
            value_of_the_asset_as_per_balance_sheet:
              values.value_of_the_asset_as_per_balance_sheet,
            escrow_account_no: values.escrow_account_no,
            escrow_bank_name: values.escrow_bank_name,
            amount_to_be_maintained: values.amount_to_be_maintained,
          },
          freeze: true,
          callback: r => {
            loadDsra(frm);
            d.hide();
          },
        });
      },
      secondary_action_label: 'Cancel',
      secondary_action: () => {
        d.hide();
      },
    });
    if (frm.is_dirty()) {
      frm.save().then(() => {
        frm.scroll_to_field('transaction_dsra');
        d.show();
        d.$wrapper.find('.modal-dialog').css('max-width', '80%');
        getAssetSubType(d, dsra.type_of_asset, 'asset_sub_type');
      });
    } else {
      d.show();
      d.$wrapper.find('.modal-dialog').css('max-width', '80%');
      getAssetSubType(d, dsra.type_of_asset, 'asset_sub_type');
    }
  });
}

function addAssetsOwnerDetails(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Assets Owner Details',
    fields: [
      {
        label: 'Type of Asset Holder',
        options: ['Owned', 'Third Party'],
        reqd: 0,
        fieldname: 'type_of_asset_holder',
        fieldtype: 'Select',
        default: assetsOwnerDetails.type_of_asset_holder,
      },
      {
        label: 'Relationship with Asset Owner',
        options: ['Self'],
        reqd: 0,
        fieldname: 'relationship_with_asset_owner',
        fieldtype: 'Select',
        default: assetsOwnerDetails.relationship_with_asset_owner,
      },
      {
        label: 'Type of Asset Ownership',
        options: ['Financial Asset'],
        reqd: 0,
        fieldname: 'type_of_asset_ownership',
        fieldtype: 'Select',
        default: assetsOwnerDetails.type_of_asset_ownership,
      },
      {
        label: 'Leasor Name',
        reqd: 0,
        fieldname: 'leasor_name',
        fieldtype: 'Data',
        default: assetsOwnerDetails.leasor_name,
      },
      {
        label: 'Lease end Date',
        reqd: 0,
        fieldname: 'lease_end_date',
        fieldtype: 'Date',
        default: assetsOwnerDetails.lease_end_date,
      },
      {
        label: '',
        options: '',
        reqd: 0,
        fieldname: 'column_break_1',
        fieldtype: 'Column Break',
      },
      {
        label: 'Asset Owner Name',
        reqd: 0,
        fieldname: 'asset_owner_name',
        fieldtype: 'Data',
        default: assetsOwnerDetails.asset_owner_name,
      },

      {
        label: 'PAN Number',
        reqd: 1,
        fieldname: 'pan_number',
        fieldtype: 'Data',
        default: assetsOwnerDetails.pan_number,
      },
      {
        label: 'LEI Name',
        reqd: 0,
        fieldname: 'lei_name',
        fieldtype: 'Data',
        default: assetsOwnerDetails.lei_name,
      },
      {
        label: 'ISO Country Code',
        reqd: 0,
        fieldname: 'iso_country_code',
        fieldtype: 'Data',
        default: assetsOwnerDetails.iso_country_code,
      },
      {
        label: 'Asset Address',
        options: '',
        reqd: 0,
        fieldname: 'asset_address_section',
        fieldtype: 'Section Break',
      },
      {
        label: 'Asset Address',
        reqd: 0,
        fieldname: 'asset_address',
        fieldtype: 'Small Text',
        default: assetsOwnerDetails.asset_address,
      },
      {
        label: 'Asset ID',
        reqd: 0,
        fieldname: 'property_id',
        fieldtype: 'Data',
        default: assetsOwnerDetails.property_id,
      },
      {
        label: 'Asset Address 1',
        reqd: 0,
        fieldname: 'asset_address_1',
        fieldtype: 'Data',
        default: assetsOwnerDetails.asset_address_1,
      },
      {
        label: 'Asset Address 2',
        reqd: 0,
        fieldname: 'asset_address_2',
        fieldtype: 'Data',
        default: assetsOwnerDetails.asset_address_2,
      },
      {
        label: 'Asset Address 3',
        reqd: 0,
        fieldname: 'asset_address_3',
        fieldtype: 'Data',
        default: assetsOwnerDetails.asset_address_3,
      },
      {
        label: '',
        options: '',
        reqd: 0,
        fieldname: 'column_break_1',
        fieldtype: 'Column Break',
      },

      {
        label: 'City',
        reqd: 0,
        fieldname: 'city',
        fieldtype: 'Data',
        default: assetsOwnerDetails.city,
      },
      {
        label: 'State',
        reqd: 0,
        fieldname: 'state',
        fieldtype: 'Data',
        default: assetsOwnerDetails.state,
      },
      {
        label: 'Pincode',
        reqd: 0,
        fieldname: 'pincode',
        fieldtype: 'Data',
        default: assetsOwnerDetails.pincode,
      },
      {
        label: 'Country',
        reqd: 0,
        fieldname: 'country',
        fieldtype: 'Data',
        default: assetsOwnerDetails.country,
      },
      {
        label: 'Landmark',
        reqd: 0,
        fieldname: 'landmark',
        fieldtype: 'Data',
        default: assetsOwnerDetails.landmark,
      },
    ],
    primary_action_label: 'Confirm',
    primary_action: values => {
      validatePan(values);
      validateAssetId(values);
      frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.transaction_details.transaction_details.save_assets_owner_details',
        args: {
          transaction_details_name: frm.doc.name,
          type_of_asset_holder: values.type_of_asset_holder,
          relationship_with_asset_owner: values.relationship_with_asset_owner,
          type_of_asset_ownership: values.type_of_asset_ownership,
          leasor_name: values.leasor_name,
          lease_end_date: values.lease_end_date,
          asset_address: values.asset_address,
          property_id: values.property_id,
          asset_address_1: values.asset_address_1,
          asset_address_2: values.asset_address_2,
          asset_address_3: values.asset_address_3,
          asset_owner_name: values.asset_owner_name,
          pan_number: values.pan_number.toUpperCase(),
          lei_name: values.lei_name,
          iso_country_code: values.iso_country_code,
          city: values.city,
          state: values.state,
          pincode: values.pincode,
          country: values.country,
          landmark: values.landmark,
        },
        freeze: true,
        callback: r => {
          loadAssetsOwnerDetails(frm);
          d.hide();
        },
      });
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  d.show();
  d.$wrapper.find('.modal-dialog').css('max-width', '80%');
}
frappe.ui.form.on('Canopi Transaction Details Sponsor', {
  address_of_sponsor: function (frm, cdt, cdn) {
    const d = locals[cdt][cdn];
    const address = d.address_of_sponsor;
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Address',
        doc_name: address,
      },
      freeze: true,
      callback: r => {
        frappe.model.set_value(
          cdt,
          cdn,
          'gst_number_of_the_sponsor',
          r.message.gstin,
        );
        cur_frm.refresh_fields('gst_number_of_the_sponsor');
      },
    });
  },
});

function showNotes(frm) {
  $(frm.fields_dict.notes_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;

  const crmNotes = new erpnext.utils.CRMNotes({
    frm,
    notes_wrapper: $(frm.fields_dict.notes_html.wrapper),
  });
  crmNotes.refresh();

  $('.new-note-btn').unbind();
  $('.new-note-btn').on('click', function () {
    addNote(frm);
  });

  $('.notes-section').find('.edit-note-btn').unbind();
  $('.notes-section')
    .find('.edit-note-btn')
    .on('click', function () {
      editNote(this, frm);
    });

  $('.notes-section').find('.delete-note-btn').unbind();
  $('.notes-section')
    .find('.delete-note-btn')
    .on('click', function () {
      deleteNote(this, frm);
    });
}

function addNote(frm) {
  const d = new frappe.ui.Dialog({
    title: 'Add a Note',
    fields: [
      {
        label: 'Note',
        fieldname: 'note',
        fieldtype: 'Text Editor',
        reqd: 1,
        enable_mentions: true,
      },
    ],
    primary_action: function () {
      const data = d.get_values();
      frappe.call({
        method: 'trusteeship_platform.custom_methods.add_note',
        args: {
          doctype: frm.doctype,
          docname: frm.doc.name,
          note: data.note,
        },
        freeze: true,
        callback: function (r) {
          if (!r.exc) {
            d.hide();
            frm.reload_doc();
          }
        },
      });
    },
    primary_action_label: 'Add',
  });
  d.show();
}

function editNote(editBtn, frm) {
  const row = $(editBtn).closest('.comment-content');
  const rowId = row.attr('name');
  const rowContent = $(row).find('.content').html();
  if (rowContent) {
    const d = new frappe.ui.Dialog({
      title: 'Edit Note',
      fields: [
        {
          label: 'Note',
          fieldname: 'note',
          fieldtype: 'Text Editor',
          default: rowContent,
        },
      ],
      primary_action: function () {
        const data = d.get_values();
        frappe.call({
          method: 'trusteeship_platform.custom_methods.edit_note',
          args: {
            doctype: frm.doctype,
            docname: frm.doc.name,
            note: data.note,
            rowId,
          },
          freeze: true,
          callback: function (r) {
            if (!r.exc) {
              d.hide();
              frm.reload_doc();
            }
          },
        });
      },
      primary_action_label: 'Done',
    });
    d.show();
  }
}

function deleteNote(deleteBtn, frm) {
  const rowId = $(deleteBtn).closest('.comment-content').attr('name');
  frappe.call({
    method: 'trusteeship_platform.custom_methods.delete_note',
    args: {
      doctype: frm.doctype,
      docname: frm.doc.name,
      rowId,
    },
    freeze: true,
    callback: function (r) {
      if (!r.exc) {
        frm.refresh_fields();
      }
    },
  });
}
function showActivities(frm) {
  $(frm.fields_dict.open_activities_html.wrapper).empty();

  if (frm.doc.docstatus === 1) return;
  const crmActivities = new erpnext.utils.CRMActivities({
    frm,
    open_activities_wrapper: $(frm.fields_dict.open_activities_html.wrapper),
    all_activities_wrapper: $(frm.fields_dict.all_activities_html.wrapper),
    form_wrapper: $(frm.wrapper),
  });
  crmActivities.refresh();

  // remove extra activities table
  setTimeout(() => {
    if ($('.open-activities').length > 1) {
      crmActivities.refresh();
    }
  }, 400);
}

function validatePan(values) {
  const panMandatory = values.pan_number.toUpperCase();
  const panNumber = panMandatory;
  const patternPAN = /[A-Z]{5}[0-9]{4}[A-Z]{1}/;
  if (panNumber.match(patternPAN) && panNumber.length === 10) {
  } else frappe.throw('Invalid Pan number');
}

function validateAssetId(values) {
  const assetId = values.property_id;
  if (assetId !== undefined) {
    if (assetId.length > 9) {
      frappe.throw('Invalid Asset ID');
    }
  }
}

function headerButtons(frm) {
  if (frm.doc.operations) {
    frm.add_custom_button('Main', () => {
      window.open(`/app/atsl-mandate-list/${frm.doc.operations}`);
    });
  }
  if (!frm.doc.__islocal && frm.doc.operations) {
    let transactionDetails;
    if (frm.doc.product_name.toUpperCase() === 'FA') {
      transactionDetails = 'Canopi Transaction Details-FA';
    } else {
      transactionDetails = 'Transaction Details';
    }
    frm.add_custom_button(
      transactionDetails,
      () => {
        frm.scroll_to_field('operations');
      },
      'Operations',
    );
    frm.add_custom_button(
      'Pre-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Pre Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Pre Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Documentation',
      () => {
        const linkedDoctypes = {
          'Canopi Documentation': {
            fieldname: ['operations'],
          },
        };

        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Documentation', r);
          },
        });
      },
      'Operations',
    );
    frm.add_custom_button(
      'Post-Execution Checklist',
      () => {
        const linkedDoctypes = {
          'Canopi Post Execution Checklist': {
            fieldname: ['operations'],
          },
        };
        frappe.call({
          method: 'trusteeship_platform.custom_methods.get_linked_docs',
          args: {
            doctype: 'ATSL Mandate List',
            name: frm.doc.operations,
            linkinfo: linkedDoctypes,
          },
          freeze: true,
          callback: r => {
            redirectDoctype(frm, 'Canopi Post Execution Checklist', r);
          },
        });
      },
      'Operations',
    );
  }
}

function redirectDoctype(frm, doctype, response) {
  if (response.message[doctype]) {
    const outputString = doctype
      .replace(/([a-z0-9])([A-Z])/g, '$1 $2') // Insert space before capital letters if not already separated
      .toLowerCase() // Convert the entire string to lowercase
      .replace(/\s+/g, '-'); // Replace spaces with hyphens
    window.open(`/app/${outputString}/${response.message[doctype][0].name}`);
  } else {
    frappe.model.with_doctype(doctype, function () {
      const source = frm.doc;
      const targetDoc = frappe.model.get_new_doc(doctype);

      targetDoc.operations = source.operations;
      targetDoc.person_name = source.person_name;
      targetDoc.cnp_customer_code = source.cnp_customer_code;
      targetDoc.mandate_name = source.mandate_name;
      targetDoc.opportunity = source.opportunity;
      targetDoc.product_name = source.product_name;

      frappe.ui.form.make_quick_entry(doctype, null, null, targetDoc);
    });
  }
}

function renderButtons(frm) {
  if (frm.doc.quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_type: 'Quotation',
        doc_name: frm.doc.quotation,
      },
      freeze: true,
      callback: r => {
        const filename = r.message.cnp_s3_key
          ? r.message.cnp_s3_key.split('/').pop()
          : '';
        let viewOfferLetteButton;

        if (filename) {
          viewOfferLetteButton =
            `<div class="form-group">
            <div class="clearfix">
                <label class="control-label" style="padding-right: 0px"
                  >Download Accepted Offer Letter</label
                  >
            </div>
              <div class="control-input-wrapper">
                <div class="control-input" style="display: none"></div>
                <div class="control-value like-disabled-input bold">
                  <a class="` +
            frm.doc.name +
            '-download-offer-letter">' +
            filename +
            `</a>
                </div>
            </div></div>`;
        } else {
          viewOfferLetteButton =
            '<a class="btn btn-xs btn-default ' +
            frm.doc.name +
            '-view-offer-letter">View Offer Letter</a>';
        }

        frm.set_df_property(
          'view_offer_letter',
          'options',
          frappe.render_template(viewOfferLetteButton, {
            frm,
          }),
        );
      },
    });
  }
  viewButtons(frm);
}

function viewButtons(frm) {
  const viewOfferLetterId = '.' + frm.doc.name + '-view-offer-letter';
  const download = '.' + frm.doc.name + '-download-offer-letter';
  $(document)
    .off('click', download)
    .on('click', download, function () {
      downloadOfferLetter(frm);
    });

  $(document)
    .off('click', viewOfferLetterId)
    .on('click', viewOfferLetterId, function () {
      viewOfferLetter(frm);
    });
}

function downloadOfferLetter(frm) {
  if (frm.doc.quotation) {
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: {
        doc_name: frm.doc.quotation,
        doc_type: 'Quotation',
      },
      freeze: true,
      callback: r => {
        if (r.message.cnp_s3_key) {
          open_url_post(frappe.request.url, {
            cmd: 'trusteeship_platform.custom_methods.download_s3_file',
            key: r.message.cnp_s3_key,
          });
        }
      },
    });
  }
}

function viewOfferLetter(frm) {
  if (frm.doc.quotation) {
    window.open(
      `/api/method/trusteeship_platform.custom_methods.download_signed_pdf?doctype=Quotation&docname=${frm.doc.quotation}`,
      '_blank',
    );
  }
}
