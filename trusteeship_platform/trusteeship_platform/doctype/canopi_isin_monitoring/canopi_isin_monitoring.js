// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe, updateBreadcrumbs, cur_frm, in_list */
/* eslint-env jquery */
/* eslint no-tabs: ["error", { allowIndentationTabs: true }] */
frappe.ui.form.on('Canopi ISIN Monitoring', {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });

    addCustomEmailIcon(frm);
  },
  isin_name: function (frm) {
    setFields(frm);
  },
});
let insurerName;
function setFields(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: { doc_name: frm.doc.isin_name, doc_type: 'Initial Investor ISIN' },
    freeze: true,
    callback: r => {
      const clientName = r.message.client_name;
      const isin = r.message.isin;
      const operations = r.message.operations;
      insurerName = r.message.name1;
      frm.doc.client_name = clientName;
      frm.doc.isin = isin;
      frm.doc.operations = operations;
      frm.refresh_field('client_name');
      frm.refresh_field('isin');
      frm.refresh_field('operations');
    },
  });
}
frappe.ui.form.on('Canopi ISIN Issuer Details', {
  initial_investor_isin: function (frm, cdt, cdn) {
    const isin_id = frappe.model.get_value(cdt, cdn, 'initial_investor_isin');
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: isin_id, doc_type: 'Initial Investor ISIN' },
      freeze: true,
      callback: r => {
        frappe.model.set_value(cdt, cdn, 'isin', r.message.isin);
      },
    });
  },
});

frappe.ui.form.on('Canopi ISIN Instrument Details', {
  initial_investor_isin: function (frm, cdt, cdn) {
    const isin_id = frappe.model.get_value(cdt, cdn, 'initial_investor_isin');
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: isin_id, doc_type: 'Initial Investor ISIN' },
      freeze: true,
      callback: r => {
        frappe.model.set_value(cdt, cdn, 'isin', r.message.isin);
      },
    });
  },
});

frappe.ui.form.on('Canopi ISIN Asset Cover', {
  initial_investor_isin: function (frm, cdt, cdn) {
    const isin_id = frappe.model.get_value(cdt, cdn, 'initial_investor_isin');
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: isin_id, doc_type: 'Initial Investor ISIN' },
      freeze: true,
      callback: r => {
        frappe.model.set_value(cdt, cdn, 'isin', r.message.isin);
      },
    });
  },
});

frappe.ui.form.on('Canopi ISIN Coupon Details', {
  initial_investor_isin: function (frm, cdt, cdn) {
    const isin_id = frappe.model.get_value(cdt, cdn, 'initial_investor_isin');
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: isin_id, doc_type: 'Initial Investor ISIN' },
      freeze: true,
      callback: r => {
        frappe.model.set_value(cdt, cdn, 'isin', r.message.isin);
      },
    });
  },
});

frappe.ui.form.on('Canopi ISIN Cashflow', {
  initial_investor_isin: function (frm, cdt, cdn) {
    const isin_id = frappe.model.get_value(cdt, cdn, 'initial_investor_isin');
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: isin_id, doc_type: 'Initial Investor ISIN' },
      freeze: true,
      callback: r => {
        frappe.model.set_value(cdt, cdn, 'isin', r.message.isin);
      },
    });
  },
});

frappe.ui.form.on('Canopi ISIN Redemption Details', {
  initial_investor_isin: function (frm, cdt, cdn) {
    const isin_id = frappe.model.get_value(cdt, cdn, 'initial_investor_isin');
    frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc',
      args: { doc_name: isin_id, doc_type: 'Initial Investor ISIN' },
      freeze: true,
      callback: r => {
        frappe.model.set_value(cdt, cdn, 'isin', r.message.isin);
      },
    });
  },
});

frappe.ui.form.on(
  'Canopi ISIN Issuer Details',
  'issuer_details_table_add',
  function (frm, cdt, cdn) {
    frappe.model.set_value(cdt, cdn, 'issuer_name', insurerName);
    frm.refresh_fields('issuer_name');
    frm.fields_dict.issuer_details_section.collapse(1);
  },
);

function addCustomEmailIcon(frm) {
  frm.page.add_action_icon('mail', () => {
    emailDialog();
    setEmailBody(frm);
  });
}
function setEmailBody(frm) {
  const isin = frm.doc.isin;

  const couponRate = frm.doc.coupon_details_table[0].coupon_rate;
  const clientName = frm.doc.issuer_details_table[0].issuer_name;
  let allotmentDate = frm.doc.instrument_details_table[0].allotment_date;
  const categoryOfInstrument =
    frm.doc.instrument_details_table[0].category_of_instrument;
  const securedUnsecured =
    frm.doc.instrument_details_table[0].secured_unsecured;
  const series = frm.doc.instrument_details_table[0].series;
  const faceValue = frm.doc.instrument_details_table[0].face_value;
  const frequencyOfInterestPayment =
    frm.doc.coupon_details_table[0].frequency_of_interest_payment;
  let redemptionDate = frm.doc.instrument_details_table[0].redemption_date;
  let nextDate = '';
  let frequency = '';

  let monthAdd = 0;
  if (frequencyOfInterestPayment === 'Monthly') {
    monthAdd = 1;
    frequency = 'month';
  } else if (frequencyOfInterestPayment === 'Quarterly') {
    monthAdd = 3;
    frequency = 'quarter';
  } else if (frequencyOfInterestPayment === 'Half yearly') {
    monthAdd = 6;
    frequency = 'half year';
  } else if (frequencyOfInterestPayment === 'Yearly') {
    monthAdd = 12;
    frequency = 'year';
  }
  const myDate = new Date(allotmentDate);
  monthAdd += 1;
  myDate.setMonth(myDate.getMonth() + monthAdd);
  nextDate =
    myDate.getFullYear() + '-' + myDate.getMonth() + '-' + myDate.getDate();

  nextDate = dateFormat(nextDate);
  allotmentDate = dateFormat(allotmentDate);
  redemptionDate = dateFormat(redemptionDate);

  let emailBody = 'Dear Credit Rating Team,<br><br>';

  emailBody +=
    'Axis Trustee Services Limited is acting as Debenture Trustee for ' +
    couponRate +
    '% ' +
    clientName +
    ' ' +
    categoryOfInstrument +
    ', Redeemable, ' +
    securedUnsecured +
    '<br>';

  emailBody +=
    'Sector Bonds Series ' +
    series +
    ' in the nature of debentures of face value ₹' +
    faceValue +
    '/- per bond each- ISIN ' +
    isin +
    ' issued and allotted by ' +
    clientName +
    ' on ' +
    allotmentDate +
    ' (“the Debentures”).<br><br>';

  emailBody +=
    'We understand CRISIL Ratings Ltd and ICRA Ltd is one of the rating agency for the captioned ISIN ' +
    isin +
    ' of the Series ' +
    series +
    ' bond issued by ' +
    clientName +
    '.<br><br>';

  emailBody +=
    'In terms of SEBI Minutes dated July 14, 2022, please find below';

  emailBody += '<table>';
  emailBody +=
    '<tr><td>Coupon Payment Frequency</td><td>' +
    frequencyOfInterestPayment +
    '</td></tr>';
  emailBody +=
    '<tr><td>Coupon Payment Dates</td><td>First coupon payment shall be made on ' +
    nextDate +
    ' and every ' +
    frequency +
    ' thereafter, as per the coupon payment frequency mentioned above, till the redemption of the bonds.</td></tr>';
  emailBody +=
    '<tr><td>Redemption Date</td><td>' + redemptionDate + '</td></tr>';
  emailBody += '</table>';

  emailBody += 'Thanks and Regards,</br>';
  emailBody += frappe.session.user + '</br>';
  emailBody +=
    '<b>Axis Trustee Services Limited</b> I The Ruby I 2nd Floor I SW I 29 Senapati Bapat Marg I Dadar west, Mumbai - 400 028</br>';
  emailBody += 'Tel: (Direct) +91 022-6230 0440 I Mob: 9702174956';

  dialog.fields_dict.content.set_value(emailBody);
}
function dateFormat(date) {
  const nth = function (d) {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
      case 1:
        return 'st';
      case 2:
        return 'nd';
      case 3:
        return 'rd';
      default:
        return 'th';
    }
  };
  const fortnightAway = new Date(date);
  const day = fortnightAway.getDate();
  const month = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ][fortnightAway.getMonth()];
  date = day + nth(day) + ' ' + month + ' ' + fortnightAway.getFullYear();
  return date;
}
// email dialig
const dialog = new frappe.ui.Dialog({
  title: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
  no_submit_on_enter: true,
  fields: getFields(),
  primary_action_label: 'Send',
  primary_action() {
    sendAction();
  },
  secondary_action_label: 'Discard',
  secondary_action() {
    dialog.hide();
  },
  size: 'large',
  minimizable: true,
});

function emailDialog() {
  $(dialog.$wrapper.find('.form-section').get(0)).addClass('to_section');
  prepare();
  dialog.show();
}

function prepare() {
  setupMultiselectQueries();
  setupAttach();
  setupEmail();
  setupEmailTemplate();
}

function setupMultiselectQueries() {
  ['recipients', 'cc', 'bcc'].forEach(field => {
    dialog.fields_dict[field].get_data = () => {
      const data = dialog.fields_dict[field].get_value();
      const txt = data.match(/[^,\s*]*$/)[0] || '';

      frappe.call({
        method: 'frappe.email.get_contact_list',
        args: { txt },
        callback: r => {
          dialog.fields_dict[field].set_data(r.message);
        },
      });
    };
  });
}

let attachments = [];

function setupAttach() {
  const fields = dialog.fields_dict;
  const attach = $(fields.select_attachments.wrapper);

  if (!attachments) {
    attachments = [];
  }

  let args = {
    folder: 'Home/Attachments',
    on_success: attachment => {
      attachments.push(attachment);
      renderAttachmentRows(attachment);
    },
  };

  if (cur_frm) {
    args = {
      doctype: cur_frm.doctype,
      docname: cur_frm.docname,
      folder: 'Home/Attachments',
      on_success: attachment => {
        cur_frm.attachments.attachment_uploaded(attachment);
        renderAttachmentRows(attachment);
      },
    };
  }

  $(
    `<label class="control-label">
    ${'Select Attachments'}
	  </label>
	  <div class='attach-list'></div>
	  <p class='add-more-attachments'>
		<button class='btn btn-xs btn-default'>
		  ${frappe.utils.icon('small-add', 'xs')}&nbsp;
		  ${'Add Attachment'}
		</button>
	  </p>`,
  ).appendTo(attach.empty());

  attach
    .find('.add-more-attachments button')
    .on('click', () => new frappe.ui.FileUploader(args));
  renderAttachmentRows();
}

function renderAttachmentRows(attachment = null) {
  const selectAttachments = dialog.fields_dict.select_attachments;
  const attachmentRows = $(selectAttachments.wrapper).find('.attach-list');
  if (attachment) {
    attachmentRows.append(getAttachmentRow(attachment, true));
  } else {
    let files = [];
    if (attachments && attachments.length) {
      files = files.concat(attachments);
    }
    if (cur_frm) {
      files = files.concat(cur_frm.get_files());
    }

    if (files.length) {
      $.each(files, (i, f) => {
        if (!f.file_name) return;
        if (!attachmentRows.find(`[data-file-name="${f.name}"]`).length) {
          f.file_url = frappe.urllib.get_full_url(f.file_url);
          attachmentRows.append(getAttachmentRow(f));
        }
      });
    }
  }
}

function getAttachmentRow(attachment, checked = null) {
  return $(`<p class="checkbox flex">
	  <label class="ellipsis" title="${attachment.file_name}">
		<input
		  type="checkbox"
		  data-file-name="${attachment.name}"
		  ${checked ? 'checked' : ''}>
		</input>
		<span class="ellipsis">${attachment.file_name}</span>
	  </label>
	  &nbsp;
	  <a href="${attachment.file_url}" target="_blank" class="btn-linkF">
		${frappe.utils.icon('link-url')}
	  </a>
	</p>`);
}

function setupEmail() {
  // email
  const fields = dialog.fields_dict;

  $(fields.send_me_a_copy.input).on('click', () => {
    // update send me a copy (make it sticky)
    const val = fields.send_me_a_copy.get_value();
    frappe.db.set_value('User', frappe.session.user, 'send_me_a_copy', val);
    frappe.boot.user.send_me_a_copy = val;
  });
}

function setupEmailTemplate() {
  dialog.fields_dict.email_template.df.onchange = () => {
    const emailTemplate = dialog.fields_dict.email_template.get_value();
    if (!emailTemplate) return;

    frappe.call({
      method:
        'frappe.email.doctype.email_template.email_template.get_email_template',
      args: {
        template_name: emailTemplate,
        doc: cur_frm.doc,
        _lang: dialog.get_value('language_sel'),
      },
      callback(r) {
        prependReply(r.message, emailTemplate);
      },
    });
  };
}

let replyAdded = '';
function prependReply(reply, emailTemplate) {
  if (replyAdded === emailTemplate) return;
  const contentField = dialog.fields_dict.content;
  const subjectField = dialog.fields_dict.subject;

  contentField.set_value(`${reply.message}`);
  subjectField.set_value(reply.subject);

  replyAdded = emailTemplate;
}

function sendAction() {
  const formValues = getValues();
  if (!formValues) return;

  const selectedAttachments = $.map(
    $(dialog.wrapper).find('[data-file-name]:checked'),
    function (element) {
      return $(element).attr('data-file-name');
    },
  );
  sendEmail(formValues, selectedAttachments);
}

function getValues() {
  const formValues = dialog.get_values();

  // cc
  for (let i = 0, l = dialog.fields.length; i < l; i++) {
    const df = dialog.fields[i];

    if (df.is_cc_checkbox) {
      // concat in cc
      if (formValues[df.fieldname]) {
        formValues.cc =
          (formValues.cc ? formValues.cc + ', ' : '') + df.fieldname;
        formValues.bcc =
          (formValues.bcc ? formValues.bcc + ', ' : '') + df.fieldname;
      }

      delete formValues[df.fieldname];
    }
  }

  return formValues;
}

function sendEmail(formValues, selectedAttachments) {
  dialog.hide();

  if (!formValues.recipients) {
    frappe.msgprint('Enter Email Recipient(s)');
    return;
  }

  if (cur_frm && !frappe.model.can_email(cur_frm.doc.doctype, cur_frm)) {
    frappe.msgprint(
      'You are not allowed to send emails related to this document',
    );
    return;
  }

  frappe.call({
    method: 'trusteeship_platform.custom_methods.isin_send_mail',
    args: {
      recipients: formValues.recipients,
      cc: formValues.cc,
      bcc: formValues.bcc,
      subject: formValues.subject,
      content: formValues.content,
      send_me_a_copy: formValues.send_me_a_copy,
      attachments: selectedAttachments,
      read_receipt: formValues.send_read_receipt,
      sender: formValues.sender,
    },
    freeze: true,
    freeze_message: 'Sending',

    callback: r => {
      if (!r.exc) {
        frappe.utils.play_sound('email');

        if (r.message?.emails_not_sent_to) {
          frappe.msgprint(
            ('Email not sent to {0} (unsubscribed / disabled)',
            [frappe.utils.escape_html(r.message.emails_not_sent_to)]),
          );
        }

        if (cur_frm) {
          cur_frm.reload_doc().then(() => {
            setTimeout(() => {}, 100);
          });
        }
      } else {
        frappe.msgprint(
          'There were errors while sending email. Please try again.',
        );
      }
    },
  });
}

function getFields() {
  const fields = [
    {
      label: 'To',
      fieldtype: 'MultiSelect',
      reqd: 0,
      fieldname: 'recipients',
    },
    {
      fieldtype: 'Button',
      label: frappe.utils.icon('down'),
      fieldname: 'option_toggle_button',
      click: () => {
        toggleMoreOptions(false);
      },
    },
    {
      fieldtype: 'Section Break',
      hidden: 1,
      fieldname: 'more_options',
    },
    {
      label: 'CC',
      fieldtype: 'MultiSelect',
      fieldname: 'cc',
    },
    {
      label: 'BCC',
      fieldtype: 'MultiSelect',
      fieldname: 'bcc',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Email Template',
      fieldtype: 'Link',
      options: 'Email Template',
      fieldname: 'email_template',
    },
    {
      label: 'Subject',
      fieldtype: 'Data',
      reqd: 1,
      fieldname: 'subject',
      default: cur_frm.doc.doctype + ': ' + cur_frm.doc.name,
      length: 524288,
    },
    {
      label: 'Message',
      fieldtype: 'Text Editor',
      fieldname: 'content',
    },
    { fieldtype: 'Section Break' },
    {
      label: 'Send me a copy',
      fieldtype: 'Check',
      fieldname: 'send_me_a_copy',
      default: frappe.boot.user.send_me_a_copy,
    },
    {
      label: 'Send Read Receipt',
      fieldtype: 'Check',
      fieldname: 'send_read_receipt',
    },

    { fieldtype: 'Column Break' },
    {
      label: 'Select Attachments',
      fieldtype: 'HTML',
      fieldname: 'select_attachments',
    },
  ];

  // add from if user has access to multiple email accounts
  const emailAccounts = frappe.boot.email_accounts.filter(account => {
    return (
      !in_list(
        ['All Accounts', 'Sent', 'Spam', 'Trash'],
        account.email_account,
      ) && account.enable_outgoing
    );
  });

  if (emailAccounts.length) {
    fields.unshift({
      label: 'From',
      fieldtype: 'Select',
      reqd: 1,
      fieldname: 'sender',
      options: emailAccounts.map(function (e) {
        return e.email_id;
      }),
    });
  }

  return fields;
}

function toggleMoreOptions(showOptions) {
  showOptions = showOptions || dialog.fields_dict.more_options.df.hidden;
  dialog.set_df_property('more_options', 'hidden', !showOptions);

  const label = frappe.utils.icon(showOptions ? 'up-line' : 'down');
  dialog.get_field('option_toggle_button').set_label(label);
}
