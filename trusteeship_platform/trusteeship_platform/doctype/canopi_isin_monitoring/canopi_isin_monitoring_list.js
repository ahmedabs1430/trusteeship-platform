// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
frappe.listview_settings['Canopi ISIN Monitoring'] = {
  refresh: function (listview) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
    if (listview.views_list.current_view === 'List') {
      listview.page.add_inner_button('ISIN Calendar', function () {
        window.open('/app/canopi-isin-monitoring/view/calendar/default');
      });
    }
  },
};
