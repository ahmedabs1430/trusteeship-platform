frappe.views.calendar['Canopi ISIN Monitoring'] = {
  field_map: {
    start: 'start',
    end: 'end',
    id: 'name',
    title: 'title',
    status: 'status',
    allDay: 'all_day',
  },
  get_events_method:
    'trusteeship_platform.trusteeship_platform.doctype.canopi_isin_monitoring.canopi_isin_monitoring.get_calendar',
};
