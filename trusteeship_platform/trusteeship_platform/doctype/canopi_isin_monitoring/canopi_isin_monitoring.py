# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from frappe.model.document import Document


class CanopiISINMonitoring(Document):
    def validate(self):
        set_first_coupon_payment_date(self)
        set_first_principle_payment_date(self)

    def on_update(self):
        delete_dates(self)
        delete_priciple_payment_dates(self)
        set_interest_payment_dates(self)
        set_principle_payment_dates(self)


def set_interest_payment_dates(self):
    if self.coupon_details_table and self.instrument_details_table:
        interest_payment_date = ""
        redemption_date = ""
        if self.coupon_details_table[0].interest_payment_date is not None:
            interest_payment_date = parse(
                self.coupon_details_table[0].interest_payment_date
            )
        fRow = self.coupon_details_table[0]
        frequency_of_interest_payment = fRow.frequency_of_interest_payment
        if self.instrument_details_table[0].redemption_date is not None:
            redemption_date = parse(
                self.instrument_details_table[0].redemption_date
            )  # noqa: 501
        if interest_payment_date != "" and interest_payment_date is not None:
            create_dates(self, interest_payment_date)

        if (
            frequency_of_interest_payment == "Monthly"
            and interest_payment_date != ""
            and redemption_date != ""
        ):
            interest_payment_date = interest_payment_date + relativedelta(
                months=+1
            )  # noqa: 501
            while interest_payment_date <= redemption_date:
                create_dates(self, interest_payment_date)
                interest_payment_date += relativedelta(months=+1)

        if (
            frequency_of_interest_payment == "Quarterly"
            and interest_payment_date != ""
            and redemption_date != ""
        ):
            interest_payment_date += relativedelta(months=+3)
            while interest_payment_date <= redemption_date:
                create_dates(self, interest_payment_date)
                interest_payment_date += relativedelta(months=+3)

        if (
            frequency_of_interest_payment == "Half yearly"
            and interest_payment_date != ""
            and redemption_date != ""
        ):
            interest_payment_date += +relativedelta(months=+6)
            while interest_payment_date <= redemption_date:
                create_dates(self, interest_payment_date)
                interest_payment_date += relativedelta(months=+6)

        if (
            frequency_of_interest_payment == "Yearly"
            and interest_payment_date != ""
            and redemption_date != ""
        ):
            interest_payment_date += relativedelta(months=+12)
            while interest_payment_date <= redemption_date:
                create_dates(self, interest_payment_date)
                interest_payment_date += relativedelta(months=+12)


def set_principle_payment_dates(self):
    if self.coupon_details_table and self.instrument_details_table:
        redemption_date = ""
        allotment_date = ""
        fRow = self.coupon_details_table[0]
        sRow = self.instrument_details_table[0]
        frequency = fRow.frequency_of_principle_payment_date
        if sRow.redemption_date is not None:
            redemption_date = parse(sRow.redemption_date)
        if sRow.allotment_date is not None:
            allotment_date = parse(sRow.allotment_date)

        if (
            frequency == "Monthly"  # noqa: 501
            and allotment_date != ""  # noqa: 501
            and redemption_date != ""  # noqa: 501
        ):
            principle_payment_date = allotment_date + relativedelta(months=+1)
            while principle_payment_date <= redemption_date:
                create_principle_payment_dates(self, principle_payment_date)
                principle_payment_date += relativedelta(months=+1)

        if (
            frequency == "Quarterly"  # noqa: 501
            and allotment_date != ""  # noqa: 501
            and redemption_date != ""  # noqa: 501
        ):
            principle_payment_date = allotment_date + relativedelta(months=+3)
            while principle_payment_date <= redemption_date:
                create_principle_payment_dates(self, principle_payment_date)
                principle_payment_date += relativedelta(months=+3)

        if (
            frequency == "Half yearly"
            and allotment_date != ""
            and redemption_date != ""
        ):
            principle_payment_date = allotment_date + relativedelta(months=+6)
            while principle_payment_date <= redemption_date:
                create_principle_payment_dates(self, principle_payment_date)
                principle_payment_date += relativedelta(months=+6)

        if (
            frequency == "Yearly"  # noqa: 501
            and allotment_date != ""  # noqa: 501
            and redemption_date != ""  # noqa: 501
        ):
            principle_payment_date = allotment_date + relativedelta(months=+12)
            while principle_payment_date <= redemption_date:
                create_principle_payment_dates(self, principle_payment_date)
                principle_payment_date += relativedelta(months=+12)


def set_first_coupon_payment_date(self):
    if self.instrument_details_table:
        if self.instrument_details_table[0].allotment_date is not None:
            allotment_date = self.instrument_details_table[0].allotment_date
            allotment_date = parse(allotment_date)
            first_payment_date = allotment_date + relativedelta(months=+1)
            self.instrument_details_table[
                0
            ].first_coupon_payment_date = first_payment_date


def set_first_principle_payment_date(self):
    if self.coupon_details_table and self.instrument_details_table:
        fRow = self.coupon_details_table[0]
        sRow = self.instrument_details_table[0]
        frequency = fRow.frequency_of_principle_payment_date
        allotment_date = sRow.allotment_date
        if allotment_date is not None:
            allotment_date = parse(allotment_date)
            if frequency == "Monthly":
                p_date = allotment_date + relativedelta(months=+1)
                self.coupon_details_table[0].principle_payment_date = p_date

            if frequency == "Quarterly":
                p_date = allotment_date + relativedelta(months=+3)
                self.coupon_details_table[0].principle_payment_date = p_date

            if frequency == "Half yearly":
                p_date = allotment_date + relativedelta(months=+6)
                self.coupon_details_table[0].principle_payment_date = p_date

            if frequency == "Yearly":
                p_date = allotment_date + relativedelta(months=+12)
                self.coupon_details_table[0].principle_payment_date = p_date


def get_max_idx(self):
    """Returns the highest `idx`"""
    max_idx = frappe.db.sql(
        """select max(idx) from `tabCanopi ISIN Interest Payment Date` where parent = %s""",  # noqa: 501
        self.name,
    )
    return max_idx and max_idx[0][0] or 0


def get_max_idx_priciple_payment(self):
    """Returns the highest `idx`"""
    max_idx = frappe.db.sql(
        """select max(idx) from `tabCanopi ISIN Principle Payment Date` where parent = %s""",  # noqa: 501
        self.name,
    )
    return max_idx and max_idx[0][0] or 0


def delete_dates(self):
    frappe.db.sql(
        """delete from `tabCanopi ISIN Interest Payment Date` where parent = %s""",  # noqa: E501
        self.name,
    )


def delete_priciple_payment_dates(self):
    frappe.db.sql(
        """delete from `tabCanopi ISIN Principle Payment Date` where parent = %s""",  # noqa: E501
        self.name,
    )


def create_dates(self, interest_payment_date):
    new_doc = frappe.new_doc("Canopi ISIN Interest Payment Date")
    new_doc.interest_payment_date = interest_payment_date
    new_doc.parent = self.name
    new_doc.parenttype = "Canopi ISIN Monitoring"
    new_doc.parentfield = "interest_payment_date_table"
    new_doc.owner = "Administrator"
    new_doc.idx = get_max_idx(self) + 1
    new_doc.insert()


def create_principle_payment_dates(self, principle_payment_date):
    new_doc = frappe.new_doc("Canopi ISIN Principle Payment Date")
    new_doc.principle_payment_date = principle_payment_date
    new_doc.parent = self.name
    new_doc.parenttype = "Canopi ISIN Monitoring"
    new_doc.parentfield = "principle_payment_date_table"
    new_doc.owner = "Administrator"
    new_doc.idx = get_max_idx_priciple_payment(self) + 1
    new_doc.insert()


@frappe.whitelist()
def get_calendar(start, end):
    holidays = frappe.db.sql_list(
        """select holiday_date from `tabHoliday`
        where
        holiday_date between %(start)s and %(end)s
        """,
        {"start": start, "end": end},
        as_dict=True,
    )

    get_cl = frappe.db.sql(
        """select
        m.interest_payment_schedule,
        '' as principle_payment_schedule,
        c.interest_payment_date as start,
        c.interest_payment_date as end,
        m.name,
        concat(m.isin, '-', m.operations) as title,
        1 as status,
        1 as all_day
        from `tabCanopi ISIN Interest Payment Date` c left join
        `tabCanopi ISIN Monitoring` m on m.name = c.parent
        where c.interest_payment_date between %(start)s and %(end)s
                union
                select
        '' as interest_payment_schedule,
        m.principle_payment_schedule,
        c.principle_payment_date as start,
                c.principle_payment_date as end,
                m.name,
                concat(m.isin, '-', m.operations) as title,
                1 as status,
                1 as all_day
        from `tabCanopi ISIN Principle Payment Date` c left join
        `tabCanopi ISIN Monitoring` m on m.name = c.parent
        where c.principle_payment_date between %(start)s and %(end)s
                union
                select
        '' as interest_payment_schedule,
        '' as principle_payment_schedule,
        c.allotment_date as start,
                c.allotment_date as end,
                m.name,
                concat(m.isin, '-', m.operations) as title,
                1 as status,
                1 as all_day
        from `tabCanopi ISIN Instrument Details` c left join
        `tabCanopi ISIN Monitoring` m on m.name = c.parent
        where c.allotment_date between %(start)s and %(end)s
                union
                select
        '' as interest_payment_schedule,
        '' as principle_payment_schedule,
        c.redemption_date as start,
                c.redemption_date as end,
                m.name,
                concat(m.isin, '-', m.operations) as title,
                1 as status,
                1 as all_day
        from `tabCanopi ISIN Instrument Details` c left join
        `tabCanopi ISIN Monitoring` m on m.name = c.parent
        where c.redemption_date between %(start)s and %(end)s
                union
                select
        '' as interest_payment_schedule,
        '' as principle_payment_schedule,
        c.holiday_date as start,
                c.holiday_date as end,
                'Holiday' as name,
                concat('Holiday', '\n', c.description) as title,
                1 as status,
                1 as all_day
        from `tabHoliday` c
        where c.holiday_date between %(start)s and %(end)s
        """,
        {"start": start, "end": end},
        as_dict=True,
    )

    for gt in get_cl:
        if gt["start"] in holidays:
            if gt["interest_payment_schedule"] == "Prepone":
                gt["start"] = getPrevDate(holidays, gt["start"])
                gt["end"] = gt["start"]

            if gt["interest_payment_schedule"] == "Postpone":
                gt["start"] = getNextDate(holidays, gt["start"])
                gt["end"] = gt["start"]

            if gt["principle_payment_schedule"] == "Prepone":
                gt["start"] = getPrevDate(holidays, gt["start"])
                gt["end"] = gt["start"]

            if gt["principle_payment_schedule"] == "Postpone":
                gt["start"] = getNextDate(holidays, gt["start"])
                gt["end"] = gt["start"]

    return get_cl


def getNextDate(Holidays, curDate):
    curDate = curDate + relativedelta(days=+1)

    if curDate in Holidays:
        return getNextDate(Holidays, curDate)
    else:
        return curDate


def getPrevDate(Holidays, curDate):
    curDate = curDate + relativedelta(days=-1)
    if curDate in Holidays:
        return getPrevDate(Holidays, curDate)
    else:
        return curDate
