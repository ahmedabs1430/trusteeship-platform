# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt
import calendar
from datetime import date, datetime, timedelta

import frappe
from dateutil import relativedelta
from frappe.model.document import Document

from trusteeship_platform.custom_methods import get_doc_list


class CanopiAmortization(Document):
    def validate(self):
        set_fields(self)


def set_fields(self):
    sdoc = frappe.get_doc("Sales Invoice", self.sales_invoice)
    invoice_date = sdoc.posting_date
    quotation = sdoc.cnp_from_quotation
    for item in sdoc.items:
        if "Annual Fee" in item.cnp_type_of_fee:
            annual_fee = item.amount

    qdoc = frappe.get_doc("Quotation", quotation)
    for item in qdoc.items:
        if "Annual Fee" in item.cnp_type_of_fee:
            frequency = item.cnp_frequency

    self.start_date = invoice_date
    self.annual_fee = annual_fee
    self.frequency = frequency

    fiscal_doc_list = get_doc_list(
        "Fiscal Year", fields=('["name"]'), filters=[]
    )  # noqa: 501
    for fiscal in fiscal_doc_list:
        fiscal_doc = frappe.get_doc("Fiscal Year", fiscal)
        if (
            fiscal_doc.year_start_date <= date.today()
            and fiscal_doc.year_end_date >= date.today()
        ):
            start_date = invoice_date
            if self.frequency == "Y":
                self.end_date = fiscal_doc.year_end_date + timedelta(days=0)
            if self.frequency == "H":
                half_year = (
                    fiscal_doc.year_start_date
                    + relativedelta.relativedelta(months=6)
                    + timedelta(days=-1)
                )
                if start_date <= half_year:
                    self.end_date = half_year
                else:
                    self.end_date = fiscal_doc.year_end_date + timedelta(
                        days=0
                    )  # noqa: 501
            if self.frequency == "Q":
                quarter_1 = (
                    fiscal_doc.year_start_date
                    + relativedelta.relativedelta(months=3)
                    + timedelta(days=-1)
                )
                quarter_2 = (
                    fiscal_doc.year_start_date
                    + relativedelta.relativedelta(months=6)
                    + timedelta(days=-1)
                )
                quarter_3 = (
                    fiscal_doc.year_start_date
                    + relativedelta.relativedelta(months=9)
                    + timedelta(days=-1)
                )
                if start_date <= quarter_1:
                    self.end_date = quarter_1
                elif start_date <= quarter_2:
                    self.end_date = quarter_2
                elif start_date <= quarter_3:
                    self.end_date = quarter_3
                else:
                    self.end_date = fiscal_doc.year_end_date + timedelta(
                        days=0
                    )  # noqa: 501
            if self.frequency == "M":
                days_in_month = calendar.monthrange(
                    self.start_date.year, self.start_date.month
                )[1]
                self.end_date = datetime(
                    self.start_date.year, self.start_date.month, days_in_month
                ).date()

    delta = self.end_date - self.start_date
    self.no_of_days = delta.days
    self.amount = round(annual_fee / self.no_of_days, 2)


@frappe.whitelist()
def get_journal_entry(id):
    return frappe.db.sql(
        """
    select j.name as journal_entry_name, j.posting_date, je.*
    from `tabJournal Entry Account` je left join `tabJournal Entry` j
    on j.name = je.parent
    where je.canopi_reference_type = 'Canopi Amortization'
    and je.canopi_reference_name =%s order by j.posting_date""",
        id,
        as_dict=1,
    )
