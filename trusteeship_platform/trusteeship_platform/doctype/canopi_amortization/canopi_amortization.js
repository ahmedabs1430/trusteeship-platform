// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt

/* global frappe */
/* eslint-env jquery */

frappe.ui.form.on('Canopi Amortization', {
  refresh: frm => {
    frm.set_query('sales_invoice', filter => {
      return {
        filters: [
          ['Sales Invoice', 'company', '=', frm.doc.company],
          ['Sales Invoice', 'docstatus', '=', 1],
          ['Sales Invoice Item', 'cnp_type_of_fee', '=', 'Annual Fee'],
        ],
      };
    });

    frm.set_query('account_to_be_debited', filter => {
      return {
        filters: [['Account', 'company', '=', frm.doc.company]],
      };
    });

    frm.set_query('account_to_be_credited', filter => {
      return {
        filters: [['Account', 'company', '=', frm.doc.company]],
      };
    });

    frm.doc.account_to_be_debited =
      '1010010005 - Income Received in Advance - ATSL';
    frm.refresh_field('account_to_be_debited');

    frm.doc.account_to_be_credited = '6020001001 - Annual Fee - ATSL';
    frm.refresh_field('account_to_be_credited');

    addJournalEntry(frm);
    getJournalEntry(frm);
  },
  sales_invoice: frm => {
    setClCode(frm);
  },
  before_save: function (frm) {
    if (frm.doc.account_to_be_debited === frm.doc.account_to_be_credited) {
      frappe.throw(
        'Amount to be debited  and Amount to be credited cannot be the same account',
      );
    }
  },
});

function setClCode(frm) {
  frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: 'Sales Invoice',
      doc_name: frm.doc.sales_invoice,
    },
    callback: r => {
      frm.set_value('cnp_cl_code', r.message.cnp_cl_code);
    },
  });
}

function getJournalEntry(frm) {
  frm.set_df_property('section_break_accounts', 'hidden', true);

  if (!frm.doc.__islocal) {
    frm.set_df_property('section_break_accounts', 'hidden', false);
    frappe.call({
      method:
        'trusteeship_platform.trusteeship_platform.doctype.canopi_amortization.canopi_amortization.get_journal_entry',
      args: {
        id: frm.doc.name,
      },
      freeze: true,
      callback: r => {
        const template = `{% let k = 1 %}
          <table class="table">
          <tbody>
          </thead>
          <tr>
          <th>No.</th>
          <th>Link</th>
          <th>Date</th>
          <th>Account</th>
          <th>Debit</th>
          <th>Credit</th>
          </tr>
          </thead>
          {%  for (let row in r.message) { %}
          <tr>
          <td>{{k}}</td>
          <td><a href="{{ base_url }}/app/journal-entry/{{r.message[row].journal_entry_name}}"
          data-doctype="Journal Entry" data-name="{{r.message[row].journal_entry_name}}">
          {{r.message[row].journal_entry_name}}
          </a>
          </td>
          <td>{{ frappe.format(r.message[row].posting_date, {"fieldtype": "Date"}) }}</td>
          <td>{{r.message[row].account}}</td>
          <td>{{r.message[row].debit_in_account_currency}}</td>
          <td>{{r.message[row].credit_in_account_currency}}</td>
          </tr>{%  k = k + 1 %} {% }%}
          </tbody>
          </table>`;

        frm.set_df_property(
          'accounts',
          'options',
          frappe.render(template, { r, base_url: window.location.origin }),
        );
        frm.refresh_field('accounts');
      },
    });
  }
}

function addJournalEntry(frm) {
  $("button[data-doctype='Journal Entry']").unbind();
  $("button[data-doctype='Journal Entry']").on('click', function () {
    frappe.model.open_mapped_doc({
      method: 'trusteeship_platform.custom_methods.make_journal_entry',
      frm,
    });
  });
}
