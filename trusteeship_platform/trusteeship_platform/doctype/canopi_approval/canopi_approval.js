// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
/* eslint-env jquery */

let transactionDocumentsPostExecution = [];
let securityDocumentsPostExecution = [];
let otherDocumentsPostExecution = [];
let postExecutionDocuments = [];
let loa = {};
frappe.ui.form.on('Canopi Approval', {
  refresh: async function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
    setFacilityAmtFixHeight(frm);
    await Promise.all([
      loadTransactionDocumentsPostExecution(frm),
      loadSecurityDocumentsPostExecution(frm),
      loadOtherDocumentsPostExecution(frm),
      loadPostExecutionDocuments(frm),
      loadLOA(frm),
    ]);
    addActionButtons(frm);
    showDocumentType(frm);
    showDeviationException(frm);
    configureViewAnnexureABtn(frm);
    configureViewAnnexureBBtn(frm);
    renderAnnexureAForm(frm);
    renderAnnexureBForm(frm);
    $("button[data-label='Save']").hide();
  },

  view_annexure_a: frm => {
    getAnnexureA(frm);
  },

  view_annexure_b: frm => {
    getAnnexureB(frm);
  },
});

async function getAnnexureA(frm) {
  if (!frm.doc.__islocal && frm.doc.is_annexure_a) {
    const preExecution = await getDoc(
      'Canopi Pre Execution Checklist',
      frm.doc.pre_execution,
    );

    if (preExecution.annexure_a_s3_key) {
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: preExecution.annexure_a_s3_key,
      });
    } else {
      await frappe.call({
        method:
          // eslint-disable-next-line max-len
          'trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.update_annexure_a_draft_status',
        args: {
          docname: frm.doc.pre_execution,
        },
        freeze: true,
      });
      frm.reload_doc();
      window.open(
        `/api/method/trusteeship_platform.custom_methods.download_pre_execution_annexure_a?docname=${frm.doc.pre_execution}`,
        '_blank',
      );
    }
  }
}

async function getAnnexureB(frm) {
  if (!frm.doc.__islocal && frm.doc.is_annexure_b) {
    const documentation = await getDoc(
      'Canopi Documentation',
      frm.doc.documentation,
    );

    if (documentation.annexure_b_s3_key) {
      open_url_post(frappe.request.url, {
        cmd: 'trusteeship_platform.custom_methods.download_s3_file',
        key: documentation.annexure_b_s3_key,
      });
    } else {
      await frappe.call({
        method:
          'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.update_annexure_b_draft_status',
        args: { docname: frm.doc.documentation },
        freeze: true,
      });
      frm.reload_doc();
      window.open(
        `/api/method/trusteeship_platform.custom_methods.download_documentation_annexure_b?docname=${frm.doc.documentation}`,
        '_blank',
      );
    }
  }
}

function configureViewAnnexureABtn(frm) {
  if (frm.doc.is_annexure_a) {
    frappe.call({
      method:
        // eslint-disable-next-line max-len
        'trusteeship_platform.trusteeship_platform.doctype.canopi_pre_execution_checklist.canopi_pre_execution_checklist.is_annexure_a_approval_completed',
      args: { docname: frm.doc.pre_execution },
      freeze: true,
      callback: r => {
        if (frm.doc.status.includes('Approved') && r.message === true) {
          frm.set_df_property(
            'view_annexure_a',
            'label',
            'Download Annexure A',
          );
        }
      },
    });
  }
}

function configureViewAnnexureBBtn(frm) {
  if (frm.doc.is_annexure_b) {
    frappe.call({
      method:
        // eslint-disable-next-line max-len
        'trusteeship_platform.trusteeship_platform.doctype.canopi_documentation.canopi_documentation.is_annexure_b_approval_completed',
      args: { docname: frm.doc.documentation },
      freeze: true,
      callback: r => {
        if (frm.doc.status.includes('Approved') && r.message === true) {
          frm.set_df_property(
            'view_annexure_b',
            'label',
            'Download Annexure B',
          );
        }
      },
    });
  }
}

function showDeviationException(frm) {
  if (
    (frm.doc.is_annexure_b || frm.doc.is_annexure_a) &&
    (frappe.user.has_role('Ops Head') || frappe.user.has_role('Ops Checker')) &&
    Number(frm.doc.cnp_facility_amt) < 15000000000
  ) {
    frm.set_df_property('is_deviation_exception', 'read_only', 0);
  } else {
    frm.set_df_property('is_deviation_exception', 'read_only', 1);
  }
}

function showDocumentType(frm) {
  frm.set_df_property('is_annexure_a', 'hidden', !frm.doc.is_annexure_a);
  frm.set_df_property(
    'is_post_execution',
    'hidden',
    !frm.doc.is_post_execution,
  );
  frm.set_df_property('is_loa', 'hidden', !frm.doc.is_loa);
  frm.set_df_property('is_annexure_b', 'hidden', !frm.doc.is_annexure_b);
}

async function renderAnnexureAForm(frm) {
  if (frm.doc.is_annexure_a) {
    $(frm.fields_dict.annexure_a_display.wrapper).empty();
    const preExecution = await getDoc(
      'Canopi Pre Execution Checklist',
      frm.doc.pre_execution,
    );
    frm.set_df_property(
      'annexure_a_display',
      'options',
      frappe.render_template('annexure_a_form', {
        doc: preExecution,
      }),
    );
    frm.refresh_field('annexure_a_display');
  }
}

async function renderAnnexureBForm(frm) {
  if (frm.doc.is_annexure_b) {
    $(frm.fields_dict.annexure_b_display.wrapper).empty();
    const documentation = await getDoc(
      'Canopi Documentation',
      frm.doc.documentation,
    );
    frm.set_df_property(
      'annexure_b_display',
      'options',
      frappe.render_template('annexure_b_form', {
        doc: documentation,
      }),
    );
    frm.refresh_field('annexure_b_display');
  }
}

function setFacilityAmtFixHeight(frm) {
  const field = frm.get_field('cnp_facility_amt');
  if (field.input) {
    field.input.style.height = 'calc(1.5em + 0.75rem + 2px)';
    field.input.style.resize = 'none';
  }
}

function isApprover(frm) {
  /* eslint-disable indent */
  return frappe.user_info().name === frm.doc.approver
    ? 1
    : frappe.user.has_role(frm.doc.approver_role)
    ? 1
    : frappe.user.has_role('Admin Approver')
    ? 1
    : 0;
  /* eslint-enable indent */
}

function isApproveAllButtonVisible(frm) {
  const allDocStatusSet = new Set(
    [
      ...transactionDocumentsPostExecution,
      ...securityDocumentsPostExecution,
      ...otherDocumentsPostExecution,
      ...postExecutionDocuments,
    ].map(x => x.status),
  );
  const allDocStatusArr = [...allDocStatusSet];
  return isApprover(frm)
    ? allDocStatusArr.some(x => x.includes('Pending'))
      ? allDocStatusArr.some(x => x.includes('Rejected'))
        ? 0
        : 1
      : 0
    : 0;
}

function isRejectAllButtonVisible(frm) {
  const allDocStatusSet = new Set(
    [
      ...transactionDocumentsPostExecution,
      ...securityDocumentsPostExecution,
      ...otherDocumentsPostExecution,
      ...postExecutionDocuments,
    ].map(x => x.status),
  );
  const allDocStatusArr = [...allDocStatusSet];
  return isApprover(frm)
    ? allDocStatusArr.some(x => x.includes('Pending'))
      ? allDocStatusArr.some(x => x.includes('Approved'))
        ? 0
        : 1
      : 0
    : 0;
}

function getApprovalCountCond(frm) {
  return 1;
}

function isSendForApprovalVisible(frm) {
  return frm.doc.status?.includes('Approved By') &&
    frm.doc.approval_count < getApprovalCountCond(frm)
    ? 1
    : 0;
}

function addActionButtons(frm) {
  if (frm.doc.is_post_execution) {
    addPostExecutionActions(frm);
  } else if (frm.doc.is_annexure_a) {
    addAnnexureAActions(frm);
  } else if (frm.doc.is_loa) {
    addLoaActions(frm);
  } else if (frm.doc.is_annexure_b) {
    addAnnexureBActions(frm);
  }
}

function addLoaActions(frm) {
  if (
    isApprover(frm) &&
    !frm.doc.status?.includes('Approved By') &&
    !frm.doc.status?.includes('Rejected By')
  ) {
    frm.add_custom_button(
      'Approve',
      () => {
        updateApproval(
          frm,
          [
            {
              doctype: 'Canopi Documentation',
              docname: frm.doc.documentation,
            },
          ],
          `Approved By ${frm.doc.approver_role}`,
          frm.doc.approver,
          frm.doc.approver_role,
        ).then(() => {
          frm.reload_doc();
        });
      },
      'Actions',
    );

    frm.add_custom_button(
      'Reject',
      () => {
        updateApproval(
          frm,
          [
            {
              doctype: 'Canopi Documentation',
              docname: frm.doc.documentation,
            },
          ],
          `Rejected By ${frm.doc.approver_role}`,
          frm.doc.approver,
          frm.doc.approver_role,
        ).then(() => {
          frm.reload_doc();
        });
      },
      'Actions',
    );
  }
  actionButtonStyle();
}

function addPostExecutionActions(frm) {
  if (isApproveAllButtonVisible(frm)) {
    frm.add_custom_button(
      'Approve All',
      () => {
        updateApproval(
          frm,
          getAllPostExecutionDocs(),
          `Approved By ${frm.doc.approver_role}`,
          frm.doc.approver,
          frm.doc.approver_role,
        ).then(() => {
          frm.reload_doc();
        });
      },
      'Actions',
    );
  }

  if (isRejectAllButtonVisible(frm)) {
    frm.add_custom_button(
      'Reject All',
      () => {
        updateApproval(
          frm,
          getAllPostExecutionDocs(),
          `Rejected By ${frm.doc.approver_role}`,
          frm.doc.approver,
          frm.doc.approver_role,
        ).then(() => {
          frm.reload_doc();
        });
      },
      'Actions',
    );
  } else if (isSendForApprovalVisible(frm)) {
    frm.add_custom_button(
      'Send For Approval',
      () => {
        approvalDialog(frm, getAllPostExecutionDocs());
      },
      'Actions',
    );
  }
  actionButtonStyle();
}

function addAnnexureAActions(frm) {
  if (
    isApprover(frm) &&
    !frm.doc.status?.includes('Approved By') &&
    !frm.doc.status?.includes('Rejected By')
  ) {
    frm.add_custom_button(
      'Approve',
      () => {
        updateApproval(
          frm,
          [
            {
              doctype: 'Canopi Pre Execution Checklist',
              docname: frm.doc.pre_execution,
            },
          ],
          `Approved By ${frm.doc.approver_role}`,
          frm.doc.approver,
          frm.doc.approver_role,
        ).then(() => {
          frm.reload_doc();
        });
      },
      'Actions',
    );

    if (!frm.doc.status.includes('Rejected')) {
      frm.add_custom_button(
        'Reject',
        () => {
          updateApproval(
            frm,
            [
              {
                doctype: 'Canopi Pre Execution Checklist',
                docname: frm.doc.pre_execution,
              },
            ],
            `Rejected By ${frm.doc.approver_role}`,
            frm.doc.approver,
            frm.doc.approver_role,
          ).then(() => {
            frm.reload_doc();
          });
        },
        'Actions',
      );
    }
  } else if (
    !frm.doc.status?.includes('Pending for Approval') &&
    ((frm.doc.is_deviation_exception && frm.doc.approval_count < 4) ||
      (!frm.doc.is_deviation_exception && frm.doc.approval_count < 3))
  ) {
    frm.add_custom_button(
      'Send For Approval',
      () => {
        approvalDialog(frm, [
          {
            doctype: 'Canopi Pre Execution Checklist',
            docname: frm.doc.pre_execution,
          },
        ]);
      },
      'Actions',
    );
  }
  actionButtonStyle();
}

function addAnnexureBActions(frm) {
  if (
    isApprover(frm) &&
    !frm.doc.status?.includes('Approved By') &&
    !frm.doc.status?.includes('Rejected By')
  ) {
    frm.add_custom_button(
      'Approve',
      () => {
        updateApproval(
          frm,
          [
            {
              doctype: 'Canopi Documentation',
              docname: frm.doc.documentation,
            },
          ],
          `Approved By ${frm.doc.approver_role}`,
          frm.doc.approver,
          frm.doc.approver_role,
        ).then(() => {
          frm.reload_doc();
        });
      },
      'Actions',
    );

    if (!frm.doc.status.includes('Rejected')) {
      frm.add_custom_button(
        'Reject',
        () => {
          updateApproval(
            frm,
            [
              {
                doctype: 'Canopi Documentation',
                docname: frm.doc.documentation,
              },
            ],
            `Rejected By ${frm.doc.approver_role}`,
            frm.doc.approver,
            frm.doc.approver_role,
          ).then(() => {
            frm.reload_doc();
          });
        },
        'Actions',
      );
    }
  } else if (
    !frm.doc.status?.includes('Pending for Approval') &&
    ((frm.doc.is_deviation_exception && frm.doc.approval_count < 4) ||
      (!frm.doc.is_deviation_exception && frm.doc.approval_count < 3))
  ) {
    frm.add_custom_button(
      'Send For Approval',
      () => {
        approvalDialog(frm, [
          {
            doctype: 'Canopi Documentation',
            docname: frm.doc.documentation,
          },
        ]);
      },
      'Actions',
    );
  }
  actionButtonStyle();
}

async function approvalDialog(frm, docs) {
  const d = new frappe.ui.Dialog({
    title: 'Approval details',
    fields: [
      {
        label: 'Approver Role',
        fieldname: 'approver_role',
        fieldtype: 'Link',
        options: 'Role',
        reqd: 1,
        onchange: function (e) {
          if (d.get_value('approver_role')) {
            d.set_df_property('approver', 'read_only', 0);
            d.fields_dict.approver.get_query = function () {
              return {
                query:
                  'trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.user_query',
                filters: { role: d.get_value('approver_role') },
              };
            };
          } else {
            d.set_df_property('approver', 'read_only', 1);
          }
        },
      },
      {
        label: 'Approver',
        fieldname: 'approver',
        fieldtype: 'Link',
        options: 'User',
        read_only: 1,
        reqd: 1,
      },
    ],
    primary_action_label: 'Send for Approval',
    primary_action: values => {
      updateApproval(
        frm,
        docs,
        'Pending for Approval',
        values.approver,
        values.approver_role,
      ).then(() => {
        frm.reload_doc();
      });
      d.hide();
    },
    secondary_action_label: 'Cancel',
    secondary_action: () => {
      d.hide();
    },
  });
  // set default roles for annexure a
  if (frm.doc.is_annexure_a || frm.doc.is_annexure_b) {
    /* eslint-disable indent */
    d.set_value(
      'approver_role',
      frm.doc.approval_count === 0
        ? 'Ops Checker'
        : frm.doc.approval_count === 1
        ? 'Ops Head'
        : frm.doc.approval_count === 2
        ? 'ATSL Compliance Maker'
        : 'COO Head',
    );
    /* eslint-enable indent */
    if (frm.doc.is_annexure_a) {
      const preExecution = await getDoc(
        'Canopi Pre Execution Checklist',
        frm.doc.pre_execution,
      );

      if (frm.doc.approval_count === 0) {
        d.set_value('approver', preExecution.tl_representative);
        d.set_df_property('approver', 'read_only', 1);
      }
    }

    if (frm.doc.is_annexure_b) {
      const documentation = await getDoc(
        'Canopi Documentation',
        frm.doc.documentation,
      );

      if (frm.doc.approval_count === 0) {
        d.set_value('approver', documentation.tl_representative);
        d.set_df_property('approver', 'read_only', 1);
      }
    }

    d.set_df_property('approver_role', 'read_only', 1);
  }

  d.show();
}

async function updateApproval(
  frm,
  docnames,
  status,
  approver,
  approverRole = undefined,
) {
  const response = await frappe.call({
    method:
      'trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.update_approval',
    args: {
      docname: frm.doc.name,
      docs: docnames,
      status,
      approver,
      approver_role: approverRole,
    },
    freeze: true,
  });

  return response.message;
}

function getAllPostExecutionDocs() {
  const transactionDocs = transactionDocumentsPostExecution.map(x => {
    return {
      doctype: 'Canopi Transaction Documents Post Execution',
      docname: x.name,
    };
  });

  const securityDocs = securityDocumentsPostExecution.map(x => {
    return {
      doctype: 'Canopi Security Documents Post Execution',
      docname: x.name,
    };
  });

  const otherDocs = otherDocumentsPostExecution.map(x => {
    return {
      doctype: 'Canopi Other Documents Post Execution',
      docname: x.name,
    };
  });

  const postExeDocs = postExecutionDocuments.map(x => {
    return {
      doctype: 'Canopi Post Execution Documents',
      docname: x.name,
    };
  });

  return [...transactionDocs, ...securityDocs, ...otherDocs, ...postExeDocs];
}

async function getDocList(frm, doctype, filters = [], orderBy = '') {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc_list',
    args: {
      doctype,
      filters,
      orderBy,
    },
    freeze: true,
  });
  return response.message;
}

async function getDoc(doctype, docname) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.get_doc',
    args: {
      doc_type: doctype,
      doc_name: docname,
    },
    freeze: true,
  });
  return response.message;
}

// transaction documents post execution
async function loadTransactionDocumentsPostExecution(frm) {
  $(frm.fields_dict.transaction_documents_post_execution_html.wrapper).empty();
  transactionDocumentsPostExecution = await getDocList(
    frm,
    'Canopi Transaction Documents Post Execution',
    [
      ['canopi_documentation_name', '=', frm.doc.documentation],
      ['approval', '=', frm.doc.name],
    ],
    'creation asc',
  );

  if (transactionDocumentsPostExecution.length === 0) {
    transactionDocumentsPostExecution = [];
  }

  $(frm.fields_dict.transaction_documents_post_execution_html.wrapper).empty();
  frm.set_df_property(
    'transaction_documents_post_execution_html',
    'options',
    frappe.render_template('approval_transaction_documents_post_execution', {
      doc: transactionDocumentsPostExecution,
      id: frm.doc.name,
      is_approver: isApprover(frm),
    }),
  );

  frm.refresh_field('transaction_documents_post_execution_html');

  addTransactionDocumentsPostSelectAllEvent();
  addTransactionDocumentsPostApproveEvent(frm);
  addTransactionDocumentsPostRejectEvent(frm);
}

function addTransactionDocumentsPostSelectAllEvent() {
  $('.approval_transaction_documents_post_execution_chkAll').click(function () {
    if (this.checked) {
      $('.approval_transaction_documents_post_execution_chk').prop(
        'checked',
        true,
      );
    } else {
      $('.approval_transaction_documents_post_execution_chk').prop(
        'checked',
        false,
      );
    }
  });
}

function addTransactionDocumentsPostApproveEvent(frm) {
  $(
    '.' +
      frm.doc.name +
      '-approval_transaction_documents_post_execution-approve',
  ).on('click', function () {
    const docnames = $(
      'input[class="approval_transaction_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Transaction Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    updateDocumentsStatus(
      docnames,
      `Approved By ${frm.doc.approver_role}`,
      frm.doc.approver_role,
      true,
    ).then(async () => {
      await loadTransactionDocumentsPostExecution(frm);
      updateCurrentStatus(frm, 'transaction_documents_post_execution_html');
    });
  });
}

function addTransactionDocumentsPostRejectEvent(frm) {
  $(
    '.' +
      frm.doc.name +
      '-approval_transaction_documents_post_execution-reject',
  ).on('click', function () {
    const docnames = $(
      'input[class="approval_transaction_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Transaction Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    updateDocumentsStatus(
      docnames,
      `Rejected By ${frm.doc.approver_role}`,
      frm.doc.approver_role,
      true,
    ).then(async () => {
      await loadTransactionDocumentsPostExecution(frm);
      updateCurrentStatus(frm, 'transaction_documents_post_execution_html');
    });
  });
}

// security documents post execution
async function loadSecurityDocumentsPostExecution(frm) {
  $(frm.fields_dict.security_documents_post_execution_html.wrapper).empty();
  securityDocumentsPostExecution = await getDocList(
    frm,
    'Canopi Security Documents Post Execution',
    [
      ['canopi_documentation_name', '=', frm.doc.documentation],
      ['approval', '=', frm.doc.name],
    ],
    'creation asc',
  );
  const securityDocumentsPostExecutionNew = [];
  const st = [];
  if (securityDocumentsPostExecution.length === 0) {
    securityDocumentsPostExecution = [];
  } else {
    for (let i = 0; i < securityDocumentsPostExecution.length; i++) {
      let securityType = securityDocumentsPostExecution[i].security_type;
      if (securityType === '') securityType = 'Others';
      if (securityDocumentsPostExecutionNew[securityType] === undefined) {
        securityDocumentsPostExecutionNew[securityType] = [];
      }
      if (st.indexOf(securityType) === -1) {
        st.push(securityType);
      }
      securityDocumentsPostExecutionNew[securityType].push(
        securityDocumentsPostExecution[i],
      );
    }
  }
  $(frm.fields_dict.security_documents_post_execution_html.wrapper).empty();
  frm.set_df_property(
    'security_documents_post_execution_html',
    'options',
    frappe.render_template('approval_security_documents_post_execution', {
      st,
      doc_data: securityDocumentsPostExecutionNew,
      id: frm.doc.name,
      is_approver: isApprover(frm),
    }),
  );

  frm.refresh_field('security_documents_post_execution_html');

  addSecurityDocumentsPostSelectAllEvent();
  addSecurityDocumentsPostApproveEvent(frm);
  addSecurityDocumentsPostRejectEvent(frm);
}

function addSecurityDocumentsPostSelectAllEvent() {
  $('.approval_security_documents_post_execution_chkAll').click(function () {
    if (this.checked) {
      $('.approval_security_documents_post_execution_chk').prop(
        'checked',
        true,
      );
    } else {
      $('.approval_security_documents_post_execution_chk').prop(
        'checked',
        false,
      );
    }
  });
}

function addSecurityDocumentsPostApproveEvent(frm) {
  $(
    '.' + frm.doc.name + '-approval_security_documents_post_execution-approve',
  ).on('click', function () {
    const docnames = $(
      'input[class="approval_security_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Security Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    updateDocumentsStatus(
      docnames,
      `Approved By ${frm.doc.approver_role}`,
      frm.doc.approver_role,
      true,
    ).then(async () => {
      await loadSecurityDocumentsPostExecution(frm);
      updateCurrentStatus(frm, 'security_documents_post_execution_html');
    });
  });
}

function addSecurityDocumentsPostRejectEvent(frm) {
  $(
    '.' + frm.doc.name + '-approval_security_documents_post_execution-reject',
  ).on('click', function () {
    const docnames = $(
      'input[class="approval_security_documents_post_execution_chk"]:checked',
    )
      .map(function () {
        return {
          doctype: 'Canopi Security Documents Post Execution',
          docname: $(this).val(),
        };
      })
      .get();
    if (docnames.length === 0) frappe.throw('Select atleast one row');
    updateDocumentsStatus(
      docnames,
      `Rejected By ${frm.doc.approver_role}`,
      frm.doc.approver_role,
      true,
    ).then(async () => {
      await loadSecurityDocumentsPostExecution(frm);
      updateCurrentStatus(frm, 'security_documents_post_execution_html');
    });
  });
}

// other documents post execution
async function loadOtherDocumentsPostExecution(frm) {
  $(frm.fields_dict.other_documents_post_execution_html.wrapper).empty();
  otherDocumentsPostExecution = await getDocList(
    frm,
    'Canopi Other Documents Post Execution',
    [
      ['canopi_documentation_name', '=', frm.doc.documentation],
      ['approval', '=', frm.doc.name],
    ],
    'creation asc',
  );
  if (otherDocumentsPostExecution.length === 0) {
    otherDocumentsPostExecution = [];
  }

  $(frm.fields_dict.other_documents_post_execution_html.wrapper).empty();
  frm.set_df_property(
    'other_documents_post_execution_html',
    'options',
    frappe.render_template('approval_other_documents_post_execution', {
      doc: otherDocumentsPostExecution,
      id: frm.doc.name,
      is_approver: isApprover(frm),
    }),
  );

  frm.refresh_field('other_documents_post_execution_html');
  addOtherDocumentsPostSelectAllEvent();
  addOtherDocumentsPostApproveEvent(frm);
  addOtherDocumentsPostRejectEvent(frm);
}

function addOtherDocumentsPostSelectAllEvent() {
  $('.approval_other_documents_post_execution_chkAll').click(function () {
    if (this.checked) {
      $('.approval_other_documents_post_execution_chk').prop('checked', true);
    } else {
      $('.approval_other_documents_post_execution_chk').prop('checked', false);
    }
  });
}

function addOtherDocumentsPostApproveEvent(frm) {
  $('.' + frm.doc.name + '-approval_other_documents_post_execution-approve').on(
    'click',
    function () {
      const docnames = $(
        'input[class="approval_other_documents_post_execution_chk"]:checked',
      )
        .map(function () {
          return {
            doctype: 'Canopi Other Documents Post Execution',
            docname: $(this).val(),
          };
        })
        .get();
      if (docnames.length === 0) frappe.throw('Select atleast one row');
      updateDocumentsStatus(
        docnames,
        `Approved By ${frm.doc.approver_role}`,
        frm.doc.approver_role,
        true,
      ).then(async () => {
        await loadOtherDocumentsPostExecution(frm);
        updateCurrentStatus(frm, 'other_documents_post_execution_html');
      });
    },
  );
}

function addOtherDocumentsPostRejectEvent(frm) {
  $('.' + frm.doc.name + '-approval_other_documents_post_execution-reject').on(
    'click',
    function () {
      const docnames = $(
        'input[class="approval_other_documents_post_execution_chk"]:checked',
      )
        .map(function () {
          return {
            doctype: 'Canopi Other Documents Post Execution',
            docname: $(this).val(),
          };
        })
        .get();
      if (docnames.length === 0) frappe.throw('Select atleast one row');
      updateDocumentsStatus(
        docnames,
        `Rejected By ${frm.doc.approver_role}`,
        frm.doc.approver_role,
        true,
      ).then(async () => {
        await loadOtherDocumentsPostExecution(frm);
        updateCurrentStatus(frm, 'other_documents_post_execution_html');
      });
    },
  );
}

// loa
async function loadLOA(frm) {
  if (frm.doc.is_loa) {
    await frappe.call({
      method: 'trusteeship_platform.custom_methods.get_doc_list',
      args: {
        doctype: 'Canopi Documentation',
        filters: [['name', '=', frm.doc.documentation]],
      },
      freeze: true,

      callback: r => {
        loa = r.message[0];
        const loa_history = loa.generated_loa_history
          ? JSON.parse(loa.generated_loa_history)
          : [];
        if (r.message.length === 0) {
          loa = {};
        }
        $(frm.fields_dict.loa_html.wrapper).empty();
        frm.set_df_property(
          'loa_html',
          'options',
          frappe.render_template('approval_loa', {
            doc: loa,
            loa_history,
            id: frm.doc.name,
            is_approver: isApprover(frm),
          }),
        );

        frm.refresh_field('loa_html');
        setTimeout(() => {
          $('.' + frm.doc.name + '_loa_download').unbind();
          $('.' + frm.doc.name + '_loa_download').click(function () {
            open_url_post(frappe.request.url, {
              cmd: 'trusteeship_platform.custom_methods.download_s3_file',
              key: this.id,
              download_type: 'download',
            });
          });
        }, 100);
      },
    });
  }
}

// post_execution_documents
async function loadPostExecutionDocuments(frm) {
  $(frm.fields_dict.post_execution_documents_html.wrapper).empty();
  postExecutionDocuments = await getDocList(
    frm,
    'Canopi Post Execution Documents',
    [
      ['canopi_documentation_name', '=', frm.doc.documentation],
      ['approval', '=', frm.doc.name],
    ],
    'creation asc',
  );

  if (postExecutionDocuments.length === 0) {
    postExecutionDocuments = [];
  }

  $(frm.fields_dict.post_execution_documents_html.wrapper).empty();
  frm.set_df_property(
    'post_execution_documents_html',
    'options',
    frappe.render_template('approval_post_execution_documents', {
      doc: postExecutionDocuments,
      id: frm.doc.name,
      is_approver: isApprover(frm),
    }),
  );

  frm.refresh_field('post_execution_documents_html');

  addPostDocumentsSelectAllEvent();
  addPostDocumentsApproveEvent(frm);
  addPostDocumentsRejectEvent(frm);
}

function addPostDocumentsSelectAllEvent() {
  $('.approval_post_execution_documents_chkAll').click(function () {
    if (this.checked) {
      $('.approval_post_execution_documents_chk').prop('checked', true);
    } else {
      $('.approval_post_execution_documents_chk').prop('checked', false);
    }
  });
}

function addPostDocumentsApproveEvent(frm) {
  $('.' + frm.doc.name + '-approval_post_execution_documents-approve').on(
    'click',
    function () {
      const docnames = $(
        'input[class="approval_post_execution_documents_chk"]:checked',
      )
        .map(function () {
          return {
            doctype: 'Canopi Post Execution Documents',
            docname: $(this).val(),
          };
        })
        .get();
      if (docnames.length === 0) frappe.throw('Select atleast one row');
      updateDocumentsStatus(
        docnames,
        `Approved By ${frm.doc.approver_role}`,
        frm.doc.approver_role,
        true,
      ).then(async () => {
        await loadPostExecutionDocuments(frm);
        updateCurrentStatus(frm, 'post_execution_documents_html');
      });
    },
  );
}

function addPostDocumentsRejectEvent(frm) {
  $('.' + frm.doc.name + '-approval_post_execution_documents-reject').on(
    'click',
    function () {
      const docnames = $(
        'input[class="approval_post_execution_documents_chk"]:checked',
      )
        .map(function () {
          return {
            doctype: 'Canopi Post Execution Documents',
            docname: $(this).val(),
          };
        })
        .get();
      if (docnames.length === 0) frappe.throw('Select atleast one row');
      updateDocumentsStatus(
        docnames,
        `Rejected By ${frm.doc.approver_role}`,
        frm.doc.approver_role,
        true,
      ).then(async () => {
        await loadPostExecutionDocuments(frm);
        updateCurrentStatus(frm, 'post_execution_documents_html');
      });
    },
  );
}

function updateCurrentStatus(frm, scrollTo) {
  const allDocStatusSet = new Set(
    [
      ...transactionDocumentsPostExecution,
      ...securityDocumentsPostExecution,
      ...otherDocumentsPostExecution,
      ...postExecutionDocuments,
    ].map(x => x.status),
  );
  const allDocStatusArr = [...allDocStatusSet];
  if (
    allDocStatusArr.length === 1 &&
    allDocStatusArr[0] !== 'Pending for Approval'
  ) {
    frm.set_value('status', allDocStatusArr[0]);
    frm.doc.approval_status[frm.doc.approval_count - 1].status =
      allDocStatusArr[0].split(' ')[0];
  } else if (allDocStatusArr.some(x => x.includes('Approved'))) {
    frm.set_value('status', 'Partially Approved');
    frm.doc.approval_status[frm.doc.approval_count - 1].status =
      'Partially Approved';
  }

  frm.dirty();
  frm.save().then(() => {
    frm.scroll_to_field(scrollTo);
  });
}

async function updateDocumentsStatus(
  docnames,
  status,
  approver_role,
  isPostExecution = false,
  isAnnexureA = false,
) {
  const response = await frappe.call({
    method: 'trusteeship_platform.custom_methods.update_documents_status',
    args: {
      docs: docnames,
      status,
      approver_role,
      is_post_execution: isPostExecution,
      is_annexure_a: isAnnexureA,
    },
    freeze: true,
  });

  return response.message;
}

function actionButtonStyle() {
  const buttons = document.querySelectorAll('.ellipsis');

  buttons.forEach(element => {
    if (element.innerHTML.includes('Actions')) {
      element.style.backgroundColor = '#ed1164';
      element.style.color = 'white';
    }
  });
}
