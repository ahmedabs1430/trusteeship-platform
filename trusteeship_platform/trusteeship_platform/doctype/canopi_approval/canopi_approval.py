# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt


import json
from datetime import datetime, timedelta

import frappe
from frappe.model.document import Document
from frappe.utils.data import get_url

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    update_documents_status,
    validate_user_and_get_status,
    upload_file_to_s3,
    get_signed_loa,
)


class CanopiApproval(Document):
    def before_insert(self):
        validate_pre_execution_approval(self)


def validate_pre_execution_approval(self):
    if self.is_annexure_a:
        if (
            frappe.db.count(
                "Canopi Approval", [["pre_execution", "=", self.pre_execution]]
            )
            > 0
        ):
            frappe.throw("Approval already created for selected document")


@frappe.whitelist()
def create_approval(
    docname,
    is_post_execution=False,
    is_annexure_a=False,
    is_annexure_b=False,
):

    # set default filters
    approval_filters = [
        ["is_post_execution", "=", is_post_execution],
        ["is_annexure_a", "=", is_annexure_a],
        ["is_annexure_b", "=", is_annexure_b],
    ]

    documentation_doc = None
    pre_execution_doc = None

    # set filters based on approval type
    if is_post_execution or is_annexure_b:
        approval_filters.append(["documentation", "=", docname])
        documentation_doc = frappe.get_doc("Canopi Documentation", docname)

    if is_annexure_a:
        approval_filters.append(["pre_execution", "=", docname])
        pre_execution_doc = frappe.get_doc(
            "Canopi Pre Execution Checklist", docname
        )  # noqa: 501

    # get approval doc
    approval_list = frappe.get_all("Canopi Approval", approval_filters)

    if approval_list:
        approval = frappe.get_doc(
            "Canopi Approval", next(iter(approval_list))["name"]
        )  # noqa: 501
        approval.status = "Pending for Approval"
        approval.email_token = frappe.generate_hash()
        approval.email_token_expiry = datetime.now() + timedelta(days=90)
    else:
        approval = insert_approval(
            is_post_execution=is_post_execution,
            is_annexure_b=is_annexure_b,
            is_annexure_a=is_annexure_a,
            pre_execution_doc=pre_execution_doc,
            documentation_doc=documentation_doc,
        )

    # update approval
    if is_post_execution:
        if approval_list:
            approval.append(
                "approval_status",
                {
                    "sent_by": frappe.session.user,
                    "approver_role": "Ops Checker",
                    "approver": documentation_doc.tl_representative,
                    "status": "Pending",
                },
            )
            approval.save()

        filters = [
            ["canopi_documentation_name", "=", documentation_doc.name],
            ["status", "=", "Submit to Checker"],
        ]

        update_post_executions_documents(
            doctype="Canopi Transaction Documents Post Execution",
            approval_docname=approval.name,
            filters=filters,
        )
        update_post_executions_documents(
            doctype="Canopi Security Documents Post Execution",
            approval_docname=approval.name,
            filters=filters,
        )
        update_post_executions_documents(
            doctype="Canopi Other Documents Post Execution",
            approval_docname=approval.name,
            filters=filters,
        )
        update_post_executions_documents(
            doctype="Canopi Post Execution Documents",
            approval_docname=approval.name,
            filters=filters,
        )

        send_approval_mail(approval)

    if is_annexure_a and approval_list:
        approval = update_approval(
            approval.name,
            [
                {
                    "doctype": "Canopi Pre Execution Checklist",
                    "docname": docname,
                },
            ],
            "Pending for Approval",
            pre_execution_doc.tl_representative,
            "Ops Checker",
        )

    if is_annexure_b and approval_list:
        approval = update_approval(
            approval.name,
            [
                {
                    "doctype": "Canopi Documentation",
                    "docname": docname,
                },
            ],
            "Pending for Approval",
            documentation_doc.tl_representative,
            "Ops Checker",
        )

    return approval.name


def insert_approval(
    is_post_execution=False,
    is_annexure_a=False,
    is_annexure_b=False,
    documentation_doc=None,
    pre_execution_doc=None,
):
    approval = frappe.new_doc("Canopi Approval")
    approval.status = "Pending for Approval"
    approval.email_token = frappe.generate_hash()
    approval.email_token_expiry = datetime.now() + timedelta(days=90)
    approval.is_post_execution = is_post_execution
    approval.is_annexure_a = is_annexure_a
    approval.is_annexure_b = is_annexure_b
    approval.approver_role = "Ops Checker"

    if is_post_execution or is_annexure_b:
        approval.approver = documentation_doc.tl_representative
        approval.documentation = documentation_doc.name
        approval.operations = documentation_doc.operations
        approval.client_name = documentation_doc.client_name
        approval.product_name = documentation_doc.product_name
        approval.cnp_facility_amt = documentation_doc.cnp_facility_amt
        approval.cnp_facility_amt_in_words = (
            documentation_doc.cnp_facility_amt_in_words
        )  # noqa: 501

        approval.cnp_facility_amt_in_words_millions = (
            documentation_doc.cnp_facility_amt_in_words_millions
        )  # noqa: 501

        approval.is_deviation_exception = (
            (int(approval.cnp_facility_amt) >= 15000000000)
            if is_annexure_b
            else 0  # noqa: 501
        )

    if is_annexure_a:
        approval.approver = pre_execution_doc.tl_representative
        approval.pre_execution = pre_execution_doc.name
        approval.client_name = pre_execution_doc.mandate_name
        approval.product_name = pre_execution_doc.product_name
        approval.cnp_facility_amt = frappe.db.get_value(
            "Opportunity", pre_execution_doc.opportunity, "cnp_facility_amt"
        )
        approval.is_deviation_exception = (
            int(approval.cnp_facility_amt) >= 15000000000
        )  # noqa: 501
        approval.cnp_facility_amt_in_words = frappe.db.get_value(
            "Opportunity",
            pre_execution_doc.opportunity,
            "cnp_facility_amt_in_words",  # noqa: 501
        )
        approval.cnp_facility_amt_in_words_millions = frappe.db.get_value(
            "Opportunity",
            pre_execution_doc.opportunity,
            "cnp_facility_amt_in_words_millions",  # noqa: 501
        )

    approval.append(
        "approval_status",
        {
            "sent_by": frappe.session.user,
            "approver_role": "Ops Checker",
            "approver": approval.approver,
            "status": "Pending",
        },
    )

    approval.insert()
    return approval


@frappe.whitelist()
def update_approval(docname, docs, status, approver, approver_role=None):
    doc = frappe.get_doc("Canopi Approval", docname)
    doc.email_token = ""
    doc.email_token_expiry = None
    if doc.status == status:
        frappe.throw("Cannot perform same action twice.")

    if status == "Pending for Approval":
        doc.email_token = frappe.generate_hash()
        doc.email_token_expiry = datetime.now() + timedelta(days=90)

        doc.approval_count = doc.approval_count + 1

        if doc.approval_count > get_approval_count_cond(doc):
            frappe.throw(
                ("Cannot Approve more than {} times").format(
                    get_approval_count_cond(doc)
                )
            )

        doc.approver = approver
        doc.approver_role = approver_role
        doc.append(
            "approval_status",
            {
                "sent_by": frappe.session.user,
                "approver_role": approver_role,
                "approver": approver,
            },
        )
        if doc.is_annexure_b:
            if doc.approval_count == get_approval_count_cond(doc):
                create_isin_monitoring(doc.documentation)

    is_post_execution = doc.is_post_execution
    is_annexure_a = doc.is_annexure_a
    is_loa = doc.is_loa
    is_annexure_b = doc.is_annexure_b
    doc.status = update_documents_status(
        docs,
        status,
        approver_role,
        is_post_execution,
        is_annexure_a,
        is_loa,
        is_annexure_b,  # noqa: 501
    )

    last_approval_status = len(doc.approval_status) - 1

    if (is_annexure_a or is_annexure_b) and ("Rejected" in status):
        doc.approval_count = 0

    doc.approval_status[last_approval_status].status = doc.status.split()[0]
    doc.save()

    if status == "Pending for Approval":
        send_approval_mail(doc)

    return doc


def get_approval_count_cond(doc):
    if doc.is_post_execution or doc.is_loa:
        return 1

    if doc.is_annexure_a or doc.is_annexure_b:
        return 4 if doc.is_deviation_exception else 3


def send_approval_mail(doc):
    doc_reference = ""
    if doc.is_post_execution:
        doc_reference = doc.documentation
    if doc.is_annexure_a:
        doc_reference = doc.pre_execution
    if doc.is_annexure_b:
        doc_reference = doc.documentation

    url = get_url()
    content1 = f"""<h2>Approval</h2>
    <p>Document Reference :{doc_reference}</p>
    <p>Client :{doc.client_name}</p>"""
    content2 = f""" <p><a href="{url}/app/canopi-approval/{doc.name}">{doc.name} </a></p>"""  # noqa: 501

    content3 = f"""
    <p><a href= "{url}/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.email_action?token={doc.email_token}&action_code=1&docname={doc.name}" class="button">Approve All</a>"""  # noqa: 501
    content3 += f"""
    <a href= "{url}/api/method/trusteeship_platform.trusteeship_platform.doctype.canopi_approval.canopi_approval.email_action?token={doc.email_token}&action_code=2&docname={doc.name}" class="button_1">Reject All</a></p>"""  # noqa: 501
    cont = """<style>
    .button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    }
    .button_1 {
    background-color: red;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    }
    </style>"""

    frappe.sendmail(
        recipients=doc.approver,
        subject="Approval",
        message=cont + content1 + content2 + content3,
    )


@frappe.whitelist(allow_guest=True)
def email_action(docname, action_code, token=None):
    doc = frappe.get_doc("Canopi Approval", docname)

    if doc.email_token != token or doc.email_token_expiry <= datetime.now():
        frappe.throw("The link you followed has expired")

    actions = {
        "1": f"""Approved By {doc.approver_role}""",
        "2": f"""Rejected By {doc.approver_role}""",
    }
    status = validate_user_and_get_status(
        actions[action_code], doc.approver_role
    )  # noqa: 501

    doc.email_token = ""
    doc.email_token_expiry = None

    url = get_url()

    doc.status = status
    doc.approval_status[len(doc.approval_status) - 1].status = status.split()[
        0
    ]  # noqa: 501

    if (doc.is_annexure_a or doc.is_annexure_b) and ("Rejected" in status):
        doc.approval_count = 0

    doc.save()

    if doc.is_post_execution:
        filters = [
            [
                "canopi_documentation_name",
                "=",
                frappe.db.get_value(
                    "Canopi Approval", docname, "documentation"
                ),  # noqa: 501
            ],
            [
                "approval",
                "=",
                frappe.db.get_value("Canopi Approval", docname, "name"),
            ],  # noqa: 501
        ]

        transaction_post_doc_list = frappe.db.get_list(
            "Canopi Transaction Documents Post Execution",
            pluck="name",
            filters=filters,  # noqa: 501
        )
        for name in transaction_post_doc_list:
            frappe.db.set_value(
                "Canopi Transaction Documents Post Execution",
                name,
                "status",
                status,  # noqa: 501
            )

        security_post_doc_list = frappe.db.get_list(
            "Canopi Security Documents Post Execution",
            pluck="name",
            filters=filters,  # noqa: 501
        )
        for name in security_post_doc_list:
            frappe.db.set_value(
                "Canopi Security Documents Post Execution",
                name,
                "status",
                status,  # noqa: 501
            )

        other_post_doc_list = frappe.db.get_list(
            "Canopi Other Documents Post Execution",
            pluck="name",
            filters=filters,  # noqa: 501
        )
        for name in other_post_doc_list:
            frappe.db.set_value(
                "Canopi Other Documents Post Execution", name, "status", status
            )

        post_exe_doc_list = frappe.db.get_list(
            "Canopi Post Execution Documents", pluck="name", filters=filters
        )
        for name in post_exe_doc_list:
            frappe.db.set_value(
                "Canopi Post Execution Documents", name, "status", status
            )  # noqa: 501

    if doc.is_annexure_a:
        frappe.db.set_value(
            "Canopi Pre Execution Checklist",
            doc.pre_execution,
            "annexure_a_status",
            "Approved" if "Approved" in status else "Rejected",
        )

    if doc.is_annexure_b:
        frappe.db.set_value(
            "Canopi Documentation",
            doc.documentation,
            "annexure_b_status",
            "Approved" if "Approved" in status else "Rejected",
        )
        if doc.approval_count == get_approval_count_cond(doc):
            create_isin_monitoring(doc.documentation)

    if doc.is_loa:
        doc = frappe.get_doc("Canopi Documentation", doc.documentation)
        loa_history = doc.generated_loa_history
        generated_loa_history = json.loads(loa_history) if loa_history else []
        loa_status = (
            "Approved"
            if "Approved" in status
            else "Rejected"
            if "Rejected" in status
            else status
        )
        update_loa = [generated_loa_history[-1]]
        generated_loa_history.pop()

        generated_loa_history.append(
            {
                "file_name": update_loa[0]["file_name"],
                "documents": update_loa[0]["documents"],
                "s3_key": update_loa[0]["s3_key"],
                "status": loa_status,
            }
        )
        for z in update_loa[0]["documents"]:
            if frappe.db.exists(z["doctype"], z["name"]):
                if loa_status == "Approved":
                    loa_doc_status = "LOA Generated"
                else:
                    loa_doc_status = "LOA Rejected"
                det_doc = frappe.get_doc(z["doctype"], z["name"])
                det_doc.status = loa_doc_status
                det_doc.save()

        doc.generated_loa_history = json.dumps(generated_loa_history)
        if loa_status == "Approved":
            doc.is_loa_draft = 0

        doc.save()
        if loa_status == "Approved":
            settings_doc = frappe.get_single("Trusteeship Platform Settings")
            upload_file_to_s3(
                settings_doc, update_loa[0]["s3_key"], get_signed_loa(doc.name)
            )

    frappe.db.commit()
    frappe.local.response["type"] = "redirect"
    frappe.local.response["location"] = (
        url + "/app/canopi-approval/" + docname
    )  # noqa: 501


def create_isin_monitoring(docname):
    documentation_doc = frappe.get_doc("Canopi Documentation", docname)
    filters = [["operations", "=", documentation_doc.operations]]
    isin_list = frappe.get_list(
        "Initial Investor ISIN",
        filters=filters,
        fields=["name", "name1", "isin"],
    )
    new_doc = frappe.new_doc("Canopi ISIN Monitoring")
    new_doc.operations = documentation_doc.operations
    new_doc.client_name = documentation_doc.client_name
    new_doc.interest_payment_schedule = "Postpone"
    new_doc.principle_payment_schedule = "Prepone"
    for row in isin_list:
        new_doc.append(
            "issuer_details_table",
            {
                "initial_investor_isin": row.name,
                "isin": row.isin,
                "issuer_name": row.name1,
            },
        )
        new_doc.append(
            "instrument_details_table",
            {
                "initial_investor_isin": row.name,
                "isin": row.isin,
            },
        )
        new_doc.append(
            "asset_cover_table",
            {
                "initial_investor_isin": row.name,
                "isin": row.isin,
            },
        )
        new_doc.append(
            "coupon_details_table",
            {
                "initial_investor_isin": row.name,
                "isin": row.isin,
            },
        )
        new_doc.append(
            "cashflow_table",
            {
                "initial_investor_isin": row.name,
                "isin": row.isin,
            },
        )
        new_doc.append(
            "redemption_details_table",
            {
                "initial_investor_isin": row.name,
                "isin": row.isin,
            },
        )
    new_doc.insert()
    documentation_doc.isin_monitoring = new_doc.name
    documentation_doc.save()


def update_post_executions_documents(
    doctype, approval_docname="", filters=[], status="Pending for Approval"
):
    doc_list = frappe.get_list(doctype, ('["name"]'), filters=filters)
    for doc in doc_list:
        document = frappe.get_doc(doctype, doc["name"])
        if approval_docname:
            document.approval = approval_docname
        document.status = status
        document.save()


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def user_query(doctype, txt, searchfield, start, page_len, filters):
    from frappe.desk.reportview import get_filters_cond, get_match_cond

    conditions = []

    user_type_condition = "and user_type != 'Website User'"
    if filters and filters.get("ignore_user_type"):
        user_type_condition = ""
        filters.pop("ignore_user_type")

    txt = f"%{txt}%"
    query = """SELECT `tabUser`.`name`, CONCAT_WS(' ', first_name,
    middle_name, last_name)
    FROM `tabUser`
    LEFT JOIN `tabHas Role`
    ON `tabHas Role`.`parent` = `tabUser`.`name`
    AND `tabHas Role`.`parenttype` = 'User'
    WHERE `tabUser`.`enabled` = 1
    {user_type_condition}
    AND `tabUser`.`docstatus` < 2
    AND `tabUser`.`name` NOT IN ({standard_users})
    AND ({key} LIKE %(txt)s
    OR CONCAT_WS(' ', first_name, middle_name, last_name) LIKE %(txt)s)
    {fcond} {mcond}
    ORDER BY
    CASE WHEN `tabUser`.`name` LIKE %(txt)s THEN 0 ELSE 1 END,
    CASE WHEN concat_ws(' ', first_name, middle_name, last_name) LIKE %(txt)s
    THEN 0 ELSE 1 END,
    NAME asc
    LIMIT %(page_len)s OFFSET %(start)s
    """.format(
        user_type_condition=user_type_condition,
        standard_users=", ".join(
            [frappe.db.escape(u) for u in frappe.STANDARD_USERS]
        ),  # noqa: 501
        key=f"`tabUser`.`{searchfield}`",
        fcond=get_filters_cond(doctype, filters, conditions),
        mcond=get_match_cond(doctype),
    )

    return frappe.db.sql(query, dict(start=start, page_len=page_len, txt=txt))
