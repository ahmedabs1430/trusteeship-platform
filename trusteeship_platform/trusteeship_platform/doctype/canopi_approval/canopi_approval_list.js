// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
frappe.listview_settings['Canopi Approval'] = {
  refresh: function (frm) {
    frm.page.btn_primary.hide();
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
  },
  onload: function (list_view) {
    list_view.can_create = false;
    list_view.page.actions
      .find('[data-label="Delete"]')
      .parent()
      .parent()
      .remove();
  },
};
