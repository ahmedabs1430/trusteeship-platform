// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
frappe.ui.form.on('Canopi POA', {
  refresh: async function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
    validateExpiryDate(frm);
  },
});

function validateExpiryDate(frm) {
  frm.fields_dict.poa_expiry_date.datepicker.update({
    minDate: frappe.datetime.str_to_obj(frappe.datetime.get_today()),
  });
}
