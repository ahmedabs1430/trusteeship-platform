frappe.listview_settings['Canopi Transaction Documents Post Execution'] = {
  onload: function (list_view) {
    list_view.can_create = false;
    list_view.page.actions
      .find('[data-label="Delete"]')
      .parent()
      .parent()
      .remove();
  },
};
