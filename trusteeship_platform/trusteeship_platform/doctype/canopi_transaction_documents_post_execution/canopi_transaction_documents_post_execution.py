# Copyright (c) 2022, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import cstr

from trusteeship_platform.custom_methods import get_s3_client


class CanopiTransactionDocumentsPostExecution(Document):
    def on_trash(self):
        docname = self.canopi_documentation_name
        document_code = self.document_code
        directory = (
            cstr(frappe.local.site)
            + "/Canopi Documentation/Canopi Transaction "
            + "Documents Post Execution/"
            + docname
            + "/"
            + document_code
        )
        settings_doc = frappe.get_single("Trusteeship Platform Settings")
        s3_client = get_s3_client(settings_doc)
        response = s3_client.list_objects_v2(
            Bucket=settings_doc.s3_bucket, Prefix=directory
        )
        if "Contents" in response:
            for object in response["Contents"]:
                s3_client.delete_object(
                    Bucket=settings_doc.s3_bucket, Key=object["Key"]
                )
