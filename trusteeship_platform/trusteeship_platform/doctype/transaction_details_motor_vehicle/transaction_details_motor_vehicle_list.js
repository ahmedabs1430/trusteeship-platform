// Copyright (c) 2022, Castlecraft and contributors
// For license information, please see license.txt
/* global frappe, updateBreadcrumbs */
frappe.listview_settings['Transaction Details Motor Vehicle'] = {
  refresh: function (frm) {
    frappe.require('assets/trusteeship_platform/js/common.js', () => {
      updateBreadcrumbs('Operations and Servicing');
    });
  },
};
