// Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt
/* global frappe, ic */

frappe.query_reports['Canopi GSTR-1'] = {
  filters: [
    {
      fieldname: 'company',
      label: 'Company',
      fieldtype: 'Link',
      options: 'Company',
      reqd: 1,
      default: frappe.defaults.get_user_default('Company'),
    },
    {
      fieldname: 'company_address',
      label: 'Address',
      fieldtype: 'Link',
      options: 'Address',
      get_query: function () {
        const company = frappe.query_report.get_filter_value('company');
        if (company) {
          return {
            query: 'frappe.contacts.doctype.address.address.address_query',
            filters: { link_doctype: 'Company', link_name: company },
          };
        }
      },
    },
    {
      fieldname: 'company_gstin',
      label: 'Company GSTIN',
      fieldtype: 'Autocomplete',
      get_query: function () {
        const company = frappe.query_report.get_filter_value('company');
        return ic.get_gstin_query(company);
      },
    },
    {
      fieldname: 'from_date',
      label: 'From Date',
      fieldtype: 'Date',
      reqd: 1,
      default: frappe.datetime.add_months(frappe.datetime.get_today(), -3),
      width: '80',
    },
    {
      fieldname: 'to_date',
      label: 'To Date',
      fieldtype: 'Date',
      reqd: 1,
      default: frappe.datetime.get_today(),
    },
    {
      fieldname: 'type_of_business',
      label: 'Type of Business',
      fieldtype: 'Select',
      reqd: 1,
      options: [
        { value: 'B2B', label: 'B2B Invoices - 4A, 4B, 4C, 6B, 6C' },
        { value: 'B2C Large', label: 'B2C(Large) Invoices - 5A, 5B' },
        { value: 'B2C Small', label: 'B2C(Small) Invoices - 7' },
        { value: 'EXPORT', label: 'Export Invoice - 6A' },
        {
          value: 'Advances',
          label: 'Tax Liability (Advances Received) - 11A(1), 11A(2)',
        },
        { value: 'NIL Rated', label: 'NIL RATED/EXEMPTED Invoices' },
        { value: 'GSTR-1 Zero Rated', label: 'GSTR-1 Zero Rated' },
        {
          value: 'GSTR-1 More Than 2.5 lac',
          label: 'GSTR-1 More Than 2.5 lac',
        },
      ],
      default: 'B2B',
    },
  ],
};
