# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns(filters)
    data = get_data(filters)
    return columns, data


def get_columns(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    return [
        _("Particulars") + ":Data:300",
        _("Quarter Ended Sep 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended June 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended Sep 30, " + start_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Period Ended Sep 30, " + end_year) + ":Data:200" + "editable: true",
        _("Period Ended Sep 30, " + start_year)
        + ":Data:200"
        + "editable: true",  # noqa: 501
        _("Year Ended Mar 31, " + end_year) + ":Data:200" + "editable: true",
    ]


def get_data(filters):
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "note_note8to10_" + formatted_string

        if frappe.db.exists("Note", formatted_string):
            note = frappe.get_doc("Note", formatted_string)
            note_details = json.loads(note.cnp_note_details)
            data = note_details
        else:
            note = frappe.new_doc("Note")
            note.title = formatted_string
            note.cnp_note_details = note_json(filters)
            note.cnp_calc_details = note_calc()
            note.cnp_type = "Note 8 to 10"
            note.cnp_fiscal_year = filters.get("fiscal_year")
            note.cnp_company = filters.get("company")
            note.public = 1
            note.insert()
            frappe.db.commit()
            data = json.loads(note.cnp_note_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_json(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note8to10_details = [
        {
            "particulars": "8. Investments",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 1,
        },
        {
            "particulars": "Current Non- Trade Investments",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 2,
        },
        {
            "particulars": "Investments in Liquid Funds",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 3,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 4,
        },
        {
            "particulars": "8.1  Loans and Advances",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 5,
        },
        {
            "particulars": "Non-Current",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 6,
        },
        {
            "particulars": "Advance Payament of Taxes",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 7,
        },
        {
            "particulars": "Less Shown Separtaely in other current liabilities",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 8,
        },
        {
            "particulars": "a",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 9,
        },
        {
            "particulars": "Current",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 10,
        },
        {
            "particulars": "Prepaid Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 11,
        },
        {
            "particulars": "Deposit with Central Registry",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 12,
        },
        {
            "particulars": "Advance with Corporate Credit Card",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 13,
        },
        {
            "particulars": "Other Advances",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 14,
        },
        {
            "particulars": "Service Tax/GST Receivable",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 15,
        },
        {
            "particulars": "Advance to Reliance Commercial Finance",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 16,
        },
        {
            "particulars": "b",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 17,
        },
        {
            "particulars": "(a+b)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 18,
        },
        {
            "particulars": "Note 9:  Trade Receivables",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 19,
        },
        {
            "particulars": "Current",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 20,
        },
        {
            "particulars": "Unsecured, considered good unless stated otherwise",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 21,
        },
        {
            "particulars": "Outstanding for a period exceeding six months from ",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 22,
        },
        {
            "particulars": "the date they are due for payment",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 23,
        },
        {
            "particulars": "Secured, Considered good",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 24,
        },
        {
            "particulars": "Unsecured, Considered good",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 25,
        },
        {
            "particulars": "Doubtful",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 26,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 27,
        },
        {
            "particulars": "Provision for doubtful receivables",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 28,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 29,
        },
        {
            "particulars": "Other receivables( less than 180 days)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 30,
        },
        {
            "particulars": "Secured, Considered good",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 31,
        },
        {
            "particulars": "Unsecured, Considered good",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 32,
        },
        {
            "particulars": "Doubtful",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 33,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 34,
        },
        {
            "particulars": "Provision for doubtful receivables",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 35,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 36,
        },
        {
            "particulars": "Total",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 37,
        },
        {
            "particulars": "9.2 Other Assets",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 38,
        },
        {
            "particulars": "Non-Current",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 39,
        },
        {
            "particulars": "Unsecured, considered good unless stated otherwise",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 40,
        },
        {
            "particulars": "Non-current Bank Balances (FD maturing after 12 months)",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 41,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 42,
        },
        {
            "particulars": "Current",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 43,
        },
        {
            "particulars": "Due from the Holding Company - Axis Bank Limited",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 44,
        },
        {
            "particulars": "Due from other subsidiaries",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 45,
        },
        {
            "particulars": "Other Deposits",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 46,
        },
        {
            "particulars": "Interest Accrued on Fixed Deposits",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 47,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 48,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 49,
        },
        {
            "particulars": "Note 10: Cash & Bank Balances",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 50,
        },
        {
            "particulars": "Current",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 51,
        },
        {
            "particulars": "Cash and Cash Equivalents",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 52,
        },
        {
            "particulars": "Balances with banks:",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 53,
        },
        {
            "particulars": "On current Account",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 54,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 55,
        },
        {
            "particulars": "Other Bank Balances :",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 56,
        },
        {
            "particulars": "Deposit with remaining maturity for less than 12 months",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 57,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 58,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 59,
        },
    ]
    note8to10_details = json.dumps(note8to10_details)
    return note8to10_details


def note_calc():
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    calc_details = {
        "note8_quarter1": 0,
        "note8_quarter2": 0,
        "note8_quarter3": 0,
        "note8_period1": 0,
        "note8_period2": 0,
        "note8_year": 0,
        "note8_1_quarter1": 0,
        "note8_1_quarter2": 0,
        "note8_1_quarter3": 0,
        "note8_1_period1": 0,
        "note8_1_period2": 0,
        "note8_1_year": 0,
        "note8_2_quarter1": 0,
        "note8_2_quarter2": 0,
        "note8_2_quarter3": 0,
        "note8_2_period1": 0,
        "note8_2_period2": 0,
        "note8_2_year": 0,
        "note9_1_quarter1": 0,
        "note9_1_quarter2": 0,
        "note9_1_quarter3": 0,
        "note9_1_period1": 0,
        "note9_1_period2": 0,
        "note9_1_year": 0,
        "note9_2_quarter1": 0,
        "note9_2_quarter2": 0,
        "note9_2_quarter3": 0,
        "note9_2_period1": 0,
        "note9_2_period2": 0,
        "note9_2_year": 0,
        "note9_2_non_current_quarter1": 0,
        "note9_2_non_current_quarter2": 0,
        "note9_2_non_current_quarter3": 0,
        "note9_2_non_current_period1": 0,
        "note9_2_non_current_period2": 0,
        "note9_2_non_current_year": 0,
        "note9_2_current_quarter1": 0,
        "note9_2_current_quarter2": 0,
        "note9_2_current_quarter3": 0,
        "note9_2_current_period1": 0,
        "note9_2_current_period2": 0,
        "note9_2_current_year": 0,
        "sub1_note9_quarter1": 0,
        "sub1_note9_quarter2": 0,
        "sub1_note9_quarter3": 0,
        "sub1_note9_period1": 0,
        "sub1_note9_period2": 0,
        "sub1_note9_year": 0,
        "sub2_note9_quarter1": 0,
        "sub2_note9_quarter2": 0,
        "sub2_note9_quarter3": 0,
        "sub2_note9_period1": 0,
        "sub2_note9_period2": 0,
        "sub2_note9_year": 0,
        "note10_1_quarter1": 0,
        "note10_1_quarter2": 0,
        "note10_1_quarter3": 0,
        "note10_1_period1": 0,
        "note10_1_period2": 0,
        "note10_1_year": 0,
        "note10_2_quarter1": 0,
        "note10_2_quarter2": 0,
        "note10_2_quarter3": 0,
        "note10_2_period1": 0,
        "note10_2_period2": 0,
        "note10_2_year": 0,
        "note9_2_interest_fd_period1": 0,
        "note9_2_interest_fd_year": 0,
    }
    calc_details = json.dumps(calc_details)
    return calc_details
