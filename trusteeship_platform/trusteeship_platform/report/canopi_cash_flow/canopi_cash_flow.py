# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns(filters)
    data = get_data(filters)
    return columns, data


def get_columns(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    end_year = str(end_year_date_object.year)
    return [
        _("Cash flow from operating activities") + ":Data:500",
        _("For the Quarter Ended Sep, " + end_year) + ":Data:300",
        _("For the Year Ended Mar 31, " + end_year) + ":Data:300",
    ]


def get_data(filters):
    # Retrieve the data from your JSON or use Frappe API to fetch the data
    # based on the filters and fiscal year.
    # For example purposes,
    # we'll use the data you provided as a list of dictionaries.
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "report_cash_flow_" + formatted_string
        if frappe.db.exists("Canopi Reports", formatted_string):
            canopi_reports = frappe.get_doc("Canopi Reports", formatted_string)
            report_details = json.loads(canopi_reports.report_details)
            data = report_details
        else:
            report_details = []
            report_calc_details = {}
            filter_year = frappe.get_doc(
                "Fiscal Year",
                filters.get("fiscal_year"),
            )
            date_format = "%Y-%m-%d"
            start_year_date_object = datetime.strptime(
                str(filter_year.year_start_date), date_format
            )
            end_year_date_object = datetime.strptime(
                str(filter_year.year_end_date), date_format
            )
            start_year = str(start_year_date_object.year)
            end_year = str(end_year_date_object.year)
            canopi_reports = frappe.new_doc("Canopi Reports")
            canopi_reports.title = formatted_string
            canopi_reports.fiscal_year = filters.get("fiscal_year")
            canopi_reports.company = filters.get("company")
            canopi_reports.type = "Cash Flow"
            canopi_reports.calc_details = report_calc(report_calc_details)
            canopi_reports.report_details = json.dumps(
                get_json(report_details, start_year, end_year)
            )
            canopi_reports.insert()
            frappe.db.commit()

            data = json.loads(canopi_reports.report_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def report_calc(report_calc_details):
    report_calc_details = {
        "cash_flow_operating_profit_quarter": 0,
        "cash_flow_operating_profit_year": 0,
        "cash_generated_quarter": 0,
        "cash_generated_year": 0,
        "net_cash_flow_quarter": 0,
        "net_cash_flow_year": 0,
        "maturity_bank_deposits_quarter": 0,
        "cash_flow_investing_profit_quarter": 0,
        "cash_flow_investing_profit_year": 0,
        "cash_flow_financing_profit_quarter": 0,
        "cash_flow_financing_profit_year": 0,
    }
    report_calc_details = json.dumps(report_calc_details)
    return report_calc_details


def get_json(report_details, start_year, end_year):
    report_details = [
        {
            "cash_flow_from_operating_activities": "Profit before tax from continuing operations",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 1,
        },
        {
            "cash_flow_from_operating_activities": "Adjustment to reconcile profit before tax to net cash flows",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 2,
        },
        {
            "cash_flow_from_operating_activities": "Depreciation/ amortization on continuing operation",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 3,
        },
        {
            "cash_flow_from_operating_activities": "Provision for doubtful debts (net)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 4,
        },
        {
            "cash_flow_from_operating_activities": "Loss/(Profit) on disposal/ write off on property,plant & equipment / intangible assets pertaining to   continuing operations ",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 5,
        },
        {
            "cash_flow_from_operating_activities": " ",
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 6,
        },
        {
            "cash_flow_from_operating_activities": "Net Gain on sale of current investments",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 2445,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 7,
        },
        {
            "cash_flow_from_operating_activities": "Interest  Income",
            "for_the_quarter_ended_sep,_" + end_year: 3334,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 8,
        },
        {
            "cash_flow_from_operating_activities": "Operating profit before working capital changes",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 60558,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 9,
        },
        {
            "cash_flow_from_operating_activities": "Movements in working capital :",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 10,
        },
        {
            "cash_flow_from_operating_activities": "Increase/ (decrease) in trade payables",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 11,
        },
        {
            "cash_flow_from_operating_activities": "Increase/ (decrease) in long-term provisions",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 12,
        },
        {
            "cash_flow_from_operating_activities": "Increase/ (decrease) in short-term provisions",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 13,
        },
        {
            "cash_flow_from_operating_activities": "Increase/ (decrease) in other current liabilities",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 14,
        },
        {
            "cash_flow_from_operating_activities": "Increase/ (decrease) in other long-term liabilities",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 15,
        },
        {
            "cash_flow_from_operating_activities": "Decrease / (increase) in trade receivables",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 16,
        },
        {
            "cash_flow_from_operating_activities": "Decrease / (increase) in Loans & Advances",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 17,
        },
        {
            "cash_flow_from_operating_activities": "Decrease / (increase) in other current assets",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: -218891,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 18,
        },
        {
            "cash_flow_from_operating_activities": "Cash generated from operations",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: -202775,
            "for_the_year_ended_mar_31,_" + end_year: 2475467040,
            "indent": 0,
            "id": 19,
        },
        {
            "cash_flow_from_operating_activities": "Direct taxes paid (net of refunds)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 20,
        },
        {
            "cash_flow_from_operating_activities": "Net cash_flow_from_operating_activities (A)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 21,
        },
        {
            "cash_flow_from_operating_activities": " ",
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 22,
        },
        {
            "cash_flow_from_operating_activities": "Cash flows from investing activities",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 23,
        },
        {
            "cash_flow_from_operating_activities": "Purchase of fixed assets, including CWIP and capital advances",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 24,
        },
        {
            "cash_flow_from_operating_activities": "Proceeds from Sale of Fixed Assets",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 25,
        },
        {
            "cash_flow_from_operating_activities": "Investments in bank deposits (having original maturity of more than twelve months)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 26,
        },
        {
            "cash_flow_from_operating_activities": "Redemption/ maturity of bank deposits (having original maturity of more than twelve months)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: -345552,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 27,
        },
        {
            "cash_flow_from_operating_activities": "Purchase of current investments",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 28,
        },
        {
            "cash_flow_from_operating_activities": "Proceeds from sale/maturity of current investments",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: -45553,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 29,
        },
        {
            "cash_flow_from_operating_activities": "Interest  income",
            "for_the_quarter_ended_sep,_" + end_year: 222225,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 30,
        },
        {
            "cash_flow_from_operating_activities": "Net cash flow from/ (used in) investing activities (B)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: -400179,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 31,
        },
        {
            "cash_flow_from_operating_activities": " ",
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 32,
        },
        {
            "cash_flow_from_operating_activities": "Cash flows from financing activities",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 33,
        },
        {
            "cash_flow_from_operating_activities": "Dividend paid on equity shares",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 223,
            "for_the_year_ended_mar_31,_" + end_year: 45452,
            "indent": 1,
            "id": 34,
        },
        {
            "cash_flow_from_operating_activities": "Tax on equity dividend paid",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 35,
        },
        {
            "cash_flow_from_operating_activities": "Net cash flow from/ (used in) in financing activities (C)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 223,
            "for_the_year_ended_mar_31,_" + end_year: 45452,
            "indent": 0,
            "id": 36,
        },
        {
            "cash_flow_from_operating_activities": " ",
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 37,
        },
        {
            "cash_flow_from_operating_activities": "Net increase/(decrease) in cash and cash equivalents (A + B + C)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 38,
        },
        {
            "cash_flow_from_operating_activities": "Cash and cash equivalents at the beginning of the year",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 39,
        },
        {
            "cash_flow_from_operating_activities": "Cash and cash equivalents at the end of the year",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 40,
        },
        {
            "cash_flow_from_operating_activities": " ",
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 41,
        },
        {
            "cash_flow_from_operating_activities": "Components of cash and cash equivalents",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: " ",
            "for_the_year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 42,
        },
        {
            "cash_flow_from_operating_activities": "With banks- on current account incl. Cash in Hand",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 43,
        },
        {
            "cash_flow_from_operating_activities": "Total cash and cash equivalents (note 15)",  # noqa: E501
            "for_the_quarter_ended_sep,_" + end_year: 0,
            "for_the_year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 44,
        },
    ]
    return report_details
