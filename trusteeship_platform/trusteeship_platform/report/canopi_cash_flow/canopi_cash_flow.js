// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt
/* eslint-disable */
let datatable_instance_global;
frappe.query_reports['Canopi Cash Flow'] = {
  onload: function (report) {
    report.page.set_primary_action(__('Export'), function () {
      report.export_report();
    });
  },
  after_datatable_render: function (datatable_instance) {
    datatable_instance_global = datatable_instance;
  },

  formatter: function (value, row, column, data, default_formatter) {
    console.log(column);
    value = default_formatter(value, row, column, data);
    value = value.toLocaleString('en-US', {
      style: 'currency',
      currency: 'INR',
    });
    if (data.indent === 0) {
      value = $(`<span>${value}</span>`);

      var $value = $(value).css('font-weight', 'bold');
      if (data.warn_if_negative && data[column.fieldname] < 0) {
        $value.addClass('text-danger');
      }

      value = $value.wrap('<p></p>').parent().html();
    }
    // Make the cell editable
    setTimeout(() => {
      makeCellEditable(
        datatable_instance_global,
        row.meta.rowIndex,
        column.colIndex,
      );
    }, 0);
    return value;
  },
  update_value: function (
    row,
    column,
    value,
    data,
    dataTableInstance,
    cell_data,
  ) {
    let row_data = dataTableInstance.datamanager.data[cell_data.rowIndex];
    const columnManager = dataTableInstance.columnmanager;
    const columnData = columnManager.getColumn(cell_data.colIndex);
    row_data[columnData.fieldname] = parseInt(value, 10);
    const fiscal_year = frappe.query_report.get_filter_value('fiscal_year');
    const company = frappe.query_report.get_filter_value('company');
    frappe.call({
      method:
        'trusteeship_platform.trusteeship_platform.doctype.canopi_reports.canopi_reports.reports_events',
      args: {
        row_data: row_data,
        fiscal_year: fiscal_year,
        company: company,
        report: 'Canopi Cash Flow',
        canopi_report_name:
          'report_cash_flow_' +
          title_to_snake_case(
            remove_parenthesis(check_dash(fiscal_year) + ' ' + company),
          ),
      },
      freeze: true,
      callback: r => {
        frappe.query_report.refresh();
      },
    });
  },
};

function check_dash(str) {
  if (str.search('-')) {
    return str.replace('-', '_');
  } else {
    return str;
  }
}
function title_to_snake_case(str) {
  if (!str) return;

  return str
    .split(' ')
    .map(word => word.toLowerCase())
    .join('_');
}

function remove_parenthesis(str) {
  if (!str) return;
  return str.replace(/ *\([^)]*\) */g, '');
}

function makeCellEditable(dataTableInstance, rowIndex, colIndex) {
  if (setReadonly(colIndex, rowIndex)) {
    return null;
  } else {
    const cell = dataTableInstance.getCell(rowIndex, colIndex);
    const $cell = $(
      "[data-row-index='" + rowIndex + "'][data-col-index='" + colIndex + "']",
    );
    const $editDiv = $cell.find('.dt-cell__content');
    $editDiv.on('click', function () {
      // <div class="dt-cell__edit dt-cell__edit--col-2"><input class="dt-input" type="text"></div>
      const initialValue = $cell.find('.dt-cell__content').text();
      const number = stringToInt(initialValue);
      if (!$cell.find('.editable-input').length) {
        $cell
          .find('.dt-cell__content')
          .html(
            '<input type="number" class="editable-input" value="' +
              number +
              '">',
          );
        $cell.find('.editable-input').focus();

        $cell.find('.editable-input').on('blur', function () {
          const newValue = $(this).val();
          $cell.find('.dt-cell__content').text(newValue);
          let cell_data = getCellData(dataTableInstance, rowIndex, colIndex);
          // Call the update_value function to save the updated value to the backend
          frappe.query_reports['Canopi Cash Flow'].update_value(
            null,
            null,
            newValue,
            null,
            dataTableInstance,
            cell_data,
          );
        });
      }
    });
  }
}

function getCellData(dataTable, rowIndex, colIndex) {
  // Access the data manager of the DataTable instance
  const dataManager = dataTable.datamanager;

  // Get the row data using the row index
  const rowData = dataManager.getRow(rowIndex);
  // Get the cell data using the column index
  const cellData = rowData[colIndex];
  return cellData;
}

function stringToInt(stringNumber) {
  let cleanedNumber = stringNumber.replace(/[,₹]/g, '').split('.')[0];
  return parseInt(cleanedNumber);
}

function setReadonly(colIndex, rowIndex) {
  if (colIndex == 1) {
    return true;
  }
  if (
    (colIndex == 2 && rowIndex == 0) ||
    rowIndex == 1 ||
    rowIndex == 2 ||
    rowIndex == 4 ||
    rowIndex == 5 ||
    rowIndex == 6 ||
    rowIndex == 7 ||
    rowIndex == 8 ||
    rowIndex == 9 ||
    rowIndex == 18 ||
    rowIndex == 19 ||
    rowIndex == 20 ||
    rowIndex == 21 ||
    rowIndex == 22 ||
    rowIndex == 30 ||
    rowIndex == 31 ||
    rowIndex == 32 ||
    rowIndex == 35 ||
    rowIndex == 36 ||
    rowIndex == 37 ||
    rowIndex == 38 ||
    rowIndex == 39 ||
    rowIndex == 40 ||
    rowIndex == 42 ||
    rowIndex == 43 ||
    (colIndex == 3 && rowIndex == 0) ||
    rowIndex == 1 ||
    rowIndex == 2 ||
    rowIndex == 4 ||
    rowIndex == 5 ||
    rowIndex == 6 ||
    rowIndex == 7 ||
    rowIndex == 8 ||
    rowIndex == 9 ||
    rowIndex == 18 ||
    rowIndex == 19 ||
    rowIndex == 20 ||
    rowIndex == 21 ||
    rowIndex == 22 ||
    rowIndex == 30 ||
    rowIndex == 31 ||
    rowIndex == 32 ||
    rowIndex == 35 ||
    rowIndex == 36 ||
    rowIndex == 37 ||
    rowIndex == 38 ||
    rowIndex == 39 ||
    rowIndex == 40 ||
    rowIndex == 42 ||
    rowIndex == 43
  ) {
    return true;
  } else {
    return false;
  }
}
