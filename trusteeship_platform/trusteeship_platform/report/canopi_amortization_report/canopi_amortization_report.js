// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports['Canopi Amortization Report'] = {
  filters: [
    {
      fieldname: 'company',
      label: __('Company'),
      fieldtype: 'Link',
      options: 'Company',
      reqd: 1,
      default: frappe.defaults.get_user_default('Company'),
    },
    {
      fieldname: 'cl_code',
      label: __('CL Code'),
      fieldtype: 'Link',
      options: 'ATSL Mandate List',
    },
    {
      fieldname: 'from_date',
      label: __('From Date'),
      fieldtype: 'Date',
      reqd: 1,
      default: frappe.datetime.add_months(frappe.datetime.get_today(), -3),
      width: '80',
    },
    {
      fieldname: 'to_date',
      label: __('To Date'),
      fieldtype: 'Date',
      reqd: 1,
      default: frappe.datetime.get_today(),
    },
  ],
};
