# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import frappe
from frappe import _


def execute(filters=None):
    columns, data = get_columns(), get_data(filters)
    return columns, data


def get_columns():
    return [
        {
            "label": _("CL Code"),
            "fieldtype": "data",
            "fieldname": "cnp_cl_code",
            "width": 200,
        },
        {
            "label": _("Cost center"),
            "fieldtype": "data",
            "fieldname": "cost_center",
            "width": 200,
        },
        {
            "label": _("Manadate Date"),
            "fieldtype": "date",
            "fieldname": "mandate_date",
            "width": 200,
        },
        {
            "label": _("Product"),
            "fieldtype": "data",
            "fieldname": "type_of_account",
            "width": 200,
        },
        {
            "label": _("Reference Number"),
            "fieldtype": "Link",
            "fieldname": "ref_no",
            "options": "Canopi Amortization",
            "width": 200,
        },
        {
            "label": _("Customer Name"),
            "fieldtype": "data",
            "fieldname": "customer_name",
            "width": 200,
        },
        {
            "label": _("BCGRM"),
            "fieldtype": "data",
            "fieldname": "srm_name",
            "width": 200,
        },
        {
            "label": _("Opsmaker"),
            "fieldtype": "data",
            "fieldname": "marketing_officer",
            "width": 200,
        },
        {
            "label": _("Ops checker"),
            "fieldtype": "data",
            "fieldname": "dealing_manager",
            "width": 200,
        },
        {
            "label": _("Bill No."),
            "fieldtype": "Link",
            "fieldname": "bill_no",
            "options": "Sales Invoice",
            "width": 200,
        },
        {
            "label": _("Posting date"),
            "fieldtype": "date",
            "fieldname": "posting_date",
            "width": 200,
        },
        {
            "label": _("Amortization Currency"),
            "fieldtype": "data",
            "fieldname": "currency",
            "width": 200,
        },
        {
            "label": _("Amortization Amount"),
            "fieldtype": "data",
            "fieldname": "amount",
            "width": 200,
        },
        {
            "label": _("Period From"),
            "fieldtype": "date",
            "fieldname": "start_date",
            "width": 200,
        },
        {
            "label": _("Period Upto"),
            "fieldtype": "date",
            "fieldname": "end_date",
            "width": 200,
        },
        {
            "label": _("Total Days"),
            "fieldtype": "data",
            "fieldname": "no_of_days",
            "width": 200,
        },
        {
            "label": _("Daily Amort Amount"),
            "fieldtype": "data",
            "fieldname": "amount",
            "width": 200,
        },
    ]


def get_data(filters):
    data = []
    if filters.from_date > filters.to_date:
        frappe.msgprint(_("From Date can not be greater than To Date"))
        return data

    condition = ""
    if filters.cl_code:
        condition = " and am.cnp_cl_code = %(cl_code)s"
    data = frappe.db.sql(
        """select
        am.*,
        si.name as bill_no,
        si.customer_name,
        si.cost_center,
        ops.owner as srm_name,
        ops.ops_servicing_rm as marketing_officer,
        ops.tl_representative as dealing_manager,
        date(ops.creation) as mandate_date,
        ops.mandate_closer_date,
        (select item_code from `tabSales Invoice Item` where
        parent = si.name limit 1) as type_of_account,
        (select account_currency from `tabJournal Entry Account`
        where canopi_reference_type = 'Canopi Amortization'
        and canopi_reference_name = am.name limit 1) as currency,
        round(amount) as amount
        from `tabCanopi Amortization` am
        left join `tabSales Invoice` si on si.name = am.sales_invoice
        left join `tabATSL Mandate List` ops on ops.name = am.cnp_cl_code
        where am.docstatus = 1 {condition}
        and am.company = %(company)s
        and am.posting_date between %(start)s and %(end)s
        """.format(
            condition=condition
        ),
        {
            "cl_code": filters.cl_code,
            "company": filters.company,
            "start": filters.from_date,
            "end": filters.to_date,
        },
        as_dict=True,
    )
    return data
