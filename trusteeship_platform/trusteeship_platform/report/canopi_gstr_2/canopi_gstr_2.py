# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt


from datetime import date

import frappe
from frappe.utils import flt, formatdate

from trusteeship_platform.trusteeship_platform.report.canopi_gstr_1.canopi_gstr_1 import (  # noqa: 501 isort:skip
    Gstr1Report,
)


def execute(filters=None):
    return Gstr2Report(filters).run()


class Gstr2Report(Gstr1Report):
    def __init__(self, filters=None):
        self.filters = frappe._dict(filters or {})
        self.columns = []
        self.data = []
        self.doctype = "Purchase Invoice"
        self.tax_doctype = "Purchase Taxes and Charges"
        self.select_columns = """
        name as invoice_number,
        supplier_name,
        posting_date,
        base_grand_total,
        base_rounded_total,
        supplier_gstin,
        place_of_supply,
        gst_category,
        return_against,
        is_return,
        gst_category,
        eligibility_for_itc,
        itc_integrated_tax,
        itc_central_tax,
        itc_state_tax,
        itc_cess_amount,
        cnp_composition_levy,
        credit_to
  """

    """ def run(self):
        self.get_columns()
        self.gst_accounts = get_gst_accounts_by_type(
            self.filters.company, "Output"
        )  # noqa: 501
        self.get_invoice_data()

        if self.invoices:
            self.get_invoice_items()
            self.get_items_based_on_tax_rate()
            self.invoice_fields = [
                d["fieldname"] for d in self.invoice_columns
            ]  # noqa: 501

        self.get_data()
        return self.columns, self.data """

    def get_data(self):
        self.get_igst_invoices()
        for inv, items_based_on_rate in self.items_based_on_tax_rate.items():
            invoice_details = self.invoices.get(inv)
            for rate, items in items_based_on_rate.items():
                if inv not in self.igst_invoices:
                    rate = rate / 2
                    row, taxable_value = self.get_row_data_for_invoice(
                        inv, invoice_details, rate, items
                    )
                    tax_amount = taxable_value * rate / 100
                    row += [0, tax_amount, tax_amount]
                else:
                    row, taxable_value = self.get_row_data_for_invoice(
                        inv, invoice_details, rate, items
                    )
                    tax_amount = taxable_value * rate / 100
                    row += [tax_amount, 0, 0]

                row += [
                    self.invoice_cess.get(inv),
                    invoice_details.get("eligibility_for_itc"),
                    invoice_details.get("itc_integrated_tax"),
                    invoice_details.get("itc_central_tax"),
                    invoice_details.get("itc_state_tax"),
                    invoice_details.get("itc_cess_amount"),
                ]
                if self.filters.get("type_of_business") == "CDNR":
                    row.append(
                        "Y"
                        if invoice_details.posting_date <= date(2017, 7, 1)
                        else "N"  # noqa: 501
                    )
                    row.append("C" if invoice_details.return_against else "R")

                self.data.append(row)

    def get_row_data_for_invoice(
        self, invoice, invoice_details, tax_rate, items
    ):  # noqa: 501
        row = []
        account_doc = frappe.get_doc("Account", invoice_details.credit_to)
        for fieldname in self.invoice_fields:
            if (
                self.filters.get("type_of_business")
                in ("CDNR-REG", "CDNR-UNREG")  # noqa: 501
                and fieldname == "invoice_value"
            ):
                row.append(
                    abs(invoice_details.base_rounded_total)
                    or abs(invoice_details.base_grand_total)
                )
            elif fieldname == "invoice_value":
                row.append(
                    invoice_details.base_rounded_total
                    or invoice_details.base_grand_total
                )
            elif fieldname in ("posting_date", "shipping_bill_date"):
                row.append(
                    formatdate(invoice_details.get(fieldname), "dd-MMM-YY")
                )  # noqa: 501
            elif fieldname == "export_type":
                export_type = (
                    "WPAY"
                    if invoice_details.get(fieldname) == "With Payment of Tax"
                    else "WOPAY"
                )
                row.append(export_type)
            elif (
                fieldname == "account_name"
                and self.filters.get("type_of_business") == "B2B"
            ):
                row.append(account_doc.company)

            else:
                row.append(invoice_details.get(fieldname))
        taxable_value = 0
        if invoice in self.cgst_sgst_invoices:
            division_factor = 2
        else:
            division_factor = 1

        for item_code, net_amount in self.invoice_items.get(invoice).items():
            if item_code in items:
                if self.item_tax_rate.get(
                    invoice
                ) and tax_rate / division_factor in self.item_tax_rate.get(
                    invoice, {}
                ).get(
                    item_code, []
                ):
                    taxable_value += abs(net_amount)
                elif not self.item_tax_rate.get(invoice):
                    taxable_value += abs(net_amount)
                elif tax_rate:
                    taxable_value += abs(net_amount)
                elif (
                    not tax_rate
                    and (
                        self.filters.get("type_of_business") == "EXPORT"
                        or invoice_details.get("gst_category") == "SEZ"
                    )
                    and invoice_details.get("export_type")
                    == "Without Payment of Tax"  # noqa: 501
                ):
                    taxable_value += abs(net_amount)

        row += [tax_rate or 0, taxable_value]

        for column in self.other_columns:
            if column.get("fieldname") == "cess_amount":
                row.append(flt(self.invoice_cess.get(invoice), 2))

        return row, taxable_value

    def get_igst_invoices(self):
        self.igst_invoices = []
        self.invoices = frappe._dict()
        conditions = self.get_conditions()
        invoice_data = frappe.db.sql(
            """
   select {select_columns}
   from `tab{doctype}`
   where docstatus = 1 {where_conditions}
   and is_opening = 'No'
   order by posting_date desc""".format(
                select_columns=self.select_columns,
                doctype=self.doctype,
                where_conditions=conditions,
            ),
            self.filters,
            as_dict=1,
        )

        for d in invoice_data:
            self.invoices.setdefault(d.invoice_number, d)
        self.get_items_based_on_tax_rate()
        # tax details
        for d in self.tax_details:
            is_igst = True if "IGST" in d[1] else False
            if is_igst and d[0] not in self.igst_invoices:
                self.igst_invoices.append(d[0])

    def get_conditions(self):
        conditions = ""

        for opts in (
            ("company", " and company=%(company)s"),
            ("from_date", " and posting_date>=%(from_date)s"),
            ("to_date", " and posting_date<=%(to_date)s"),
        ):
            if self.filters.get(opts[0]):
                conditions += opts[1]

        if self.filters.get("type_of_business") == "B2B":
            conditions += (
                "AND IFNULL(gst_category, '')"
                + "in ('Registered Regular', 'Registered Composition', 'Deemed Export', 'SEZ')"  # noqa: 501
                + " and is_return != 1 "
            )
        elif self.filters.get("type_of_business") == "CDNR":
            conditions += """ and is_return = 1 """

        return conditions

    def get_columns(self):
        self.tax_columns = [
            {
                "fieldname": "rate",
                "label": "Rate",
                "fieldtype": "Int",
                "width": 60,
            },  # noqa: 501
            {
                "fieldname": "taxable_value",
                "label": "Taxable Value",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "integrated_tax_paid",
                "label": "Integrated Tax Paid",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "central_tax_paid",
                "label": "Central Tax Paid",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "state_tax_paid",
                "label": "State/UT Tax Paid",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "cess_amount",
                "label": "Cess Paid",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "eligibility_for_itc",
                "label": "Eligibility For ITC",
                "fieldtype": "Data",
                "width": 100,
            },
            {
                "fieldname": "itc_integrated_tax",
                "label": "IGST Amount",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "itc_central_tax",
                "label": "CGST Amount",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "itc_state_tax",
                "label": "SGST Amount",
                "fieldtype": "Currency",
                "width": 100,
            },
            {
                "fieldname": "itc_cess_amount",
                "label": "Cess Amount",
                "fieldtype": "Currency",
                "width": 100,
            },
        ]
        self.other_columns = []

        if self.filters.get("type_of_business") == "B2B":
            self.invoice_columns = [
                {
                    "fieldname": "supplier_gstin",
                    "label": "GSTIN of Supplier",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_number",
                    "label": "Invoice Number",
                    "fieldtype": "Link",
                    "options": "Purchase Invoice",
                    "width": 120,
                },
                {
                    "fieldname": "posting_date",
                    "label": "Invoice date",
                    "fieldtype": "Date",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_value",
                    "label": "Invoice Value",
                    "fieldtype": "Currency",
                    "width": 120,
                },
                {
                    "fieldname": "place_of_supply",
                    "label": "Place of Supply",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "reverse_charge",
                    "label": "Reverse Charge",
                    "fieldtype": "Data",
                    "width": 80,
                },
                {
                    "fieldname": "gst_category",
                    "label": "Invoice Type",
                    "fieldtype": "Data",
                    "width": 80,
                },
                {
                    "fieldname": "cnp_composition_levy",
                    "label": "Composition Levy",
                    "fieldtype": "Data",
                    "width": 80,
                },
                {
                    "fieldname": "account_name",
                    "label": "Account name/ GL description",
                    "fieldtype": "Data",
                    "width": 100,
                },
            ]
        elif self.filters.get("type_of_business") == "CDNR":
            self.invoice_columns = [
                {
                    "fieldname": "supplier_gstin",
                    "label": "GSTIN of Supplier",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_number",
                    "label": "Note/Refund Voucher Number",
                    "fieldtype": "Link",
                    "options": "Purchase Invoice",
                },
                {
                    "fieldname": "posting_date",
                    "label": "Note/Refund Voucher date",
                    "fieldtype": "Date",
                    "width": 120,
                },
                {
                    "fieldname": "return_against",
                    "label": "Invoice/Advance Payment Voucher Number",
                    "fieldtype": "Link",
                    "options": "Purchase Invoice",
                    "width": 120,
                },
                {
                    "fieldname": "posting_date",
                    "label": "Invoice/Advance Payment Voucher date",
                    "fieldtype": "Date",
                    "width": 120,
                },
                {
                    "fieldname": "reason_for_issuing_document",
                    "label": "Reason For Issuing document",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "supply_type",
                    "label": "Supply Type",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_value",
                    "label": "Invoice Value",
                    "fieldtype": "Currency",
                    "width": 120,
                },
            ]
            self.other_columns = [
                {
                    "fieldname": "pre_gst",
                    "label": "PRE GST",
                    "fieldtype": "Data",
                    "width": 50,
                },
                {
                    "fieldname": "document_type",
                    "label": "Document Type",
                    "fieldtype": "Data",
                    "width": 50,
                },
            ]
        self.columns = (
            self.invoice_columns + self.tax_columns + self.other_columns
        )  # noqa: 501
