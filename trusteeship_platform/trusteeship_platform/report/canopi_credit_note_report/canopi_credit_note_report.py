# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt


import json
from datetime import date

import frappe
from frappe import _
from frappe.utils import flt, formatdate

from india_compliance.gst_india.utils import (  # isort:skip
    get_gst_accounts_by_type,
    is_overseas_transaction,
)


def execute(filters=None):
    return CrditNoteReport(filters).run()


class CrditNoteReport:
    def __init__(self, filters=None):
        self.filters = frappe._dict(filters or {})
        if self.filters.get("from_date") > self.filters.get("to_date"):
            frappe.throw("To Date cannot be before From Date..")
        self.columns = []
        self.data = []
        self.doctype = "Sales Invoice"
        self.tax_doctype = "Sales Taxes and Charges"
        self.select_columns = """
        name as invoice_number,
        customer_name,
        posting_date,
        base_grand_total,
        base_rounded_total,
        NULLIF(billing_address_gstin, '') as billing_address_gstin,
        place_of_supply,
        ecommerce_gstin,
        is_reverse_charge,
        return_against,
        is_return,
        is_debit_note,
        gst_category,
        is_export_with_gst as export_type,
        port_code,
        shipping_bill_number,
        shipping_bill_date,
        reason_for_issuing_document,
        company_gstin,
        cnp_composition_levy,
        cnp_cl_code,
        cnp_cgst,
        cnp_sgst,
        cnp_igst
        """

    def run(self):
        self.get_columns()
        self.gst_accounts = get_gst_accounts_by_type(
            self.filters.company, "Output"
        )  # noqa: 501
        self.get_invoice_data()

        if self.invoices:
            self.get_invoice_items()
            self.get_items_based_on_tax_rate()
            self.invoice_fields = [
                d["fieldname"] for d in self.invoice_columns
            ]  # noqa: 501

        self.get_data()

        return self.columns, self.data

    def get_data(self):
        if self.invoices:
            for (
                inv,
                items_based_on_rate,
            ) in self.items_based_on_tax_rate.items():  # noqa: 501
                invoice_doc = frappe.get_doc("Sales Invoice", inv)
                invoice_details = self.invoices.get(inv)
                for rate, items in items_based_on_rate.items():
                    row, taxable_value = self.get_row_data_for_invoice(
                        inv,
                        invoice_details,
                        rate,
                        items,
                        invoice_doc.taxes_and_charges,  # noqa: 501
                    )

                    if self.filters.get("type_of_business") in (
                        "CDNR-REG",
                        "CDNR-UNREG",
                    ):
                        row.append(
                            "Y"
                            if invoice_details.posting_date <= date(2017, 7, 1)
                            else "N"
                        )
                        row.append(
                            "Credit Note"
                            if invoice_details.is_return
                            else "D"  # noqa: E501
                        )  # noqa: E501

                    if taxable_value:
                        self.data.append(row)

    def get_advance_data(self):
        advances_data = {}
        advances = self.get_advance_entries()
        for entry in advances:
            # only consider IGST and SGST so as to avoid duplication of
            # taxable amount
            if (
                entry.account_head == self.gst_accounts.igst_account
                or entry.account_head == self.gst_accounts.sgst_account
            ):
                advances_data.setdefault(
                    (entry.place_of_supply, entry.rate), [0.0, 0.0]
                )
                advances_data[(entry.place_of_supply, entry.rate)][0] += (
                    entry.amount * 100 / entry.rate
                )
            elif entry.account_head == self.gst_accounts.cess_account:
                advances_data[(entry.place_of_supply, entry.rate)][
                    1
                ] += entry.amount  # noqa: 501

        for key, value in advances_data.items():
            row = [key[0], key[1], value[0], value[1]]
            self.data.append(row)

    def get_row_data_for_invoice(
        self, invoice, invoice_details, tax_rate, items, taxes_and_charges
    ):
        if invoice_details.return_against:
            credit_aginast_invoice = frappe.get_doc(
                self.doctype, invoice_details.return_against
            )
        taxable_value = 0
        if invoice in self.cgst_sgst_invoices:
            division_factor = 2
        else:
            division_factor = 1

        for item_code, net_amount in self.invoice_items.get(invoice).items():
            if item_code in items:
                if self.item_tax_rate.get(
                    invoice
                ) and tax_rate / division_factor in self.item_tax_rate.get(
                    invoice, {}
                ).get(
                    item_code, []
                ):
                    taxable_value += abs(net_amount)
                elif not self.item_tax_rate.get(invoice):
                    taxable_value += abs(net_amount)
                elif tax_rate:
                    taxable_value += abs(net_amount)
                elif (
                    not tax_rate
                    and (
                        self.filters.get("type_of_business") == "EXPORT"
                        or invoice_details.get("gst_category") == "SEZ"
                    )
                    and not invoice_details.get("is_export_with_gst")
                ):
                    taxable_value += abs(net_amount)

        row = []
        for fieldname in self.invoice_fields:
            if (
                self.filters.get("type_of_business")
                in ("CDNR-REG", "CDNR-UNREG")  # noqa: 501
                and fieldname == "invoice_value"
            ):
                row.append(
                    abs(invoice_details.base_rounded_total)
                    or abs(invoice_details.base_grand_total)
                )
            elif (
                self.filters.get("type_of_business")
                in ("CDNR-REG", "CDNR-UNREG")  # noqa: 501
                and fieldname == "invoice_date"
                and invoice_details.return_against
            ):
                row.append(credit_aginast_invoice.posting_date)
            elif fieldname == "invoice_value":
                if (
                    self.filters.get("type_of_business")
                    == "GSTR-1 Zero Rated"  # noqa: 501
                    and not tax_rate
                ):
                    row.append(taxable_value)
                else:
                    row.append(
                        invoice_details.base_rounded_total
                        or invoice_details.base_grand_total
                    )
            elif fieldname in ("posting_date", "shipping_bill_date"):
                row.append(
                    formatdate(invoice_details.get(fieldname), "dd-MMM-YY")
                )  # noqa: 501
            elif fieldname == "export_type" and self.filters.get(
                "type_of_business"
            ) not in (
                "CDNR-REG",
                "CDNR-UNREG",
            ):
                export_type = (
                    "WPAY" if invoice_details.get(fieldname) else "WOPAY"
                )  # noqa: 501
                row.append(export_type)
            elif self.filters.get("type_of_business") in (
                "CDNR-REG",
                "CDNR-UNREG",
            ) and (  # noqa: 501
                fieldname == "cnp_cgst"
                or fieldname == "cnp_igst"
                or fieldname == "cnp_sgst"
            ):
                row.append(abs(invoice_details.get(fieldname)))
            else:
                row.append(invoice_details.get(fieldname))
            """ elif self.filters.get("type_of_business") in (
                "CDNR-REG",
            ):
                if(fieldname == 'invoice_date'):
                    row.append(invoice_details.get('return_against'))
                if(fieldname == 'invoice_type'):
                    row.append("Credit Note") """

        if (
            self.filters.get("type_of_business") == "GSTR-1 Zero Rated"
            and not tax_rate  # noqa: 501
        ):
            row += [taxable_value]
        else:
            row += [tax_rate or 0, taxable_value]

        for column in self.other_columns:
            if column.get("fieldname") == "cess_amount":
                row.append(flt(self.invoice_cess.get(invoice), 2))
        if (
            self.filters.get("type_of_business") != "CDNR-UNREG"
            or self.filters.get("type_of_business") != "EXPORT"
        ):
            row += [invoice_details.billing_address_gstin]
        return row, taxable_value

    def get_zero_rated_invoices(self):
        if self.invoices:
            for (
                inv,
                items_based_on_rate,
            ) in self.items_based_on_tax_rate.items():  # noqa: 501
                invoice_doc = frappe.get_doc("Sales Invoice", inv)
                invoice_details = self.invoices.get(inv)
                for rate, items in items_based_on_rate.items():
                    if not rate:
                        row, taxable_value = self.get_row_data_for_invoice(
                            inv,
                            invoice_details,
                            rate,
                            items,
                            invoice_doc.taxes_and_charges,  # noqa: 501
                        )

                        if taxable_value:
                            self.data.append(row)

    def get_invoice_for_large_invoices(self):
        if self.invoices:
            for (
                inv,
                items_based_on_rate,
            ) in self.items_based_on_tax_rate.items():  # noqa: 501
                invoice_doc = frappe.get_doc("Sales Invoice", inv)
                invoice_details = self.invoices.get(inv)
                for rate, items in items_based_on_rate.items():
                    if rate:
                        row, taxable_value = self.get_row_data_for_invoice(
                            inv,
                            invoice_details,
                            rate,
                            items,
                            invoice_doc.taxes_and_charges,  # noqa: 501
                        )

                        if taxable_value:
                            self.data.append(row)

    def get_invoice_data(self):
        self.invoices = frappe._dict()
        conditions = self.get_conditions()

        self.invoice_data = frappe.db.sql(
            """
            select
            {select_columns}
            from `tab{doctype}`
            where docstatus = 1 {where_conditions}
            and is_opening = 'No'
            order by posting_date desc
            """.format(
                select_columns=self.select_columns,
                doctype=self.doctype,
                where_conditions=conditions,
            ),
            self.filters,
            as_dict=1,
        )

        for d in self.invoice_data:
            d.is_reverse_charge = "Y" if d.is_reverse_charge else "N"
            self.invoices.setdefault(d.invoice_number, d)

    def get_advance_entries(self):
        return frappe.db.sql(
            """
            SELECT SUM(a.base_tax_amount)
            as
            amount, a.account_head, a.rate, p.place_of_supply
            FROM `tabPayment Entry` p, `tabAdvance Taxes and Charges` a
            WHERE p.docstatus = 1
            AND p.name = a.parent
            AND posting_date between %s and %s
            GROUP BY a.account_head, p.place_of_supply, a.rate
            """,
            (self.filters.get("from_date"), self.filters.get("to_date")),
            as_dict=1,
        )

    def get_conditions(self):
        conditions = ""

        for opts in (
            ("company", " and company=%(company)s"),
            ("from_date", " and posting_date>=%(from_date)s"),
            ("to_date", " and posting_date<=%(to_date)s"),
            ("company_address", " and company_address=%(company_address)s"),
            ("company_gstin", " and company_gstin=%(company_gstin)s"),
        ):
            if self.filters.get(opts[0]):
                conditions += opts[1]

        if self.filters.get("type_of_business") == "CDNR-REG":
            conditions += """ AND (is_return = 1 OR is_debit_note = 1)
            AND IFNULL(gst_category, '') in
            (
                'Registered Regular', 'Deemed Export', 'SEZ'
            )"""

        elif self.filters.get("type_of_business") == "CDNR-UNREG":
            conditions += """ AND ifnull(SUBSTR(place_of_supply, 1, 2),'') !=
            ifnull(SUBSTR(company_gstin, 1, 2),'')
            AND (is_return = 1 OR is_debit_note = 1)
            AND IFNULL(gst_category, '') in ('Unregistered', 'Overseas')"""

        conditions += " AND IFNULL(billing_address_gstin, '') "
        conditions += "!= company_gstin"

        return conditions

    def get_invoice_items(self):
        self.invoice_items = frappe._dict()
        self.item_tax_rate = frappe._dict()
        self.item_hsn_map = frappe._dict()
        self.nil_exempt_non_gst = {}

        items = frappe.db.sql(
            """
        select item_code, parent, taxable_value,
        base_net_amount, item_tax_rate, is_nil_exempt,
        is_non_gst from `tab%s Item`
        where parent in (%s)
        """
            % (self.doctype, ", ".join(["%s"] * len(self.invoices))),
            tuple(self.invoices),
            as_dict=1,
        )

        for d in items:
            self.invoice_items.setdefault(d.parent, {}).setdefault(
                d.item_code, 0.0
            )  # noqa: 501 isort:skip
            self.item_hsn_map.setdefault(d.item_code, d.gst_hsn_code)
            self.invoice_items[d.parent][d.item_code] += d.get(
                "taxable_value", 0
            ) or d.get("base_net_amount", 0)

            item_tax_rate = {}

            if d.item_tax_rate:
                item_tax_rate = json.loads(d.item_tax_rate)

                for account, rate in item_tax_rate.items():
                    tax_rate_dict = self.item_tax_rate.setdefault(
                        d.parent, {}
                    ).setdefault(d.item_code, [])
                    tax_rate_dict.append(rate)

            if d.is_nil_exempt:
                self.nil_exempt_non_gst.setdefault(d.parent, [0.0, 0.0, 0.0])
                if item_tax_rate:
                    self.nil_exempt_non_gst[d.parent][0] += d.get(
                        "taxable_value", 0
                    )  # noqa: 501
                else:
                    self.nil_exempt_non_gst[d.parent][1] += d.get(
                        "taxable_value", 0
                    )  # noqa: 501
            elif d.is_non_gst:
                self.nil_exempt_non_gst.setdefault(d.parent, [0.0, 0.0, 0.0])
                self.nil_exempt_non_gst[d.parent][2] += d.get(
                    "taxable_value", 0
                )  # noqa: 501

    def get_items_based_on_tax_rate(self):
        if self.invoices:
            self.tax_details = frappe.db.sql(
                """
                select
                    parent,
                    account_head,
                    item_wise_tax_detail,
                    base_tax_amount_after_discount_amount
                from `tab%s`
                where
                    parenttype = %s and docstatus = 1
                    and parent in (%s)
                order by account_head
            """
                % (
                    self.tax_doctype,
                    "%s",
                    ", ".join(["%s"] * len(self.invoices.keys())),
                ),  # noqa: 501
                tuple([self.doctype] + list(self.invoices.keys())),
            )

            self.items_based_on_tax_rate = {}
            self.invoice_cess = frappe._dict()
            self.cgst_sgst_invoices = []

            unidentified_gst_accounts = []
            unidentified_gst_accounts_invoice = []
            for (
                parent,
                account,
                item_wise_tax_detail,
                tax_amount,
            ) in self.tax_details:  # noqa: 501
                if account == self.gst_accounts.cess_account:
                    self.invoice_cess.setdefault(parent, tax_amount)
                else:
                    if item_wise_tax_detail:
                        try:
                            item_wise_tax_detail = json.loads(
                                item_wise_tax_detail
                            )  # noqa: 501
                            cgst_or_sgst = False
                            if (
                                account == self.gst_accounts.cgst_account
                                or account == self.gst_accounts.sgst_account
                            ):
                                cgst_or_sgst = True

                            if not (
                                cgst_or_sgst
                                or account
                                == self.gst_accounts.igst_account  # noqa: 501
                            ):
                                if (
                                    "gst" in account.lower()
                                    and account
                                    not in unidentified_gst_accounts  # noqa: 501
                                ):
                                    unidentified_gst_accounts.append(account)
                                    unidentified_gst_accounts_invoice.append(
                                        parent
                                    )  # noqa: 501
                                continue

                            for (
                                item_code,
                                tax_amounts,
                            ) in item_wise_tax_detail.items():  # noqa: 501
                                tax_rate = tax_amounts[0]
                                if (
                                    not tax_rate
                                    and parent
                                    not in self.nil_exempt_non_gst  # noqa: 501
                                ):  # noqa: 501
                                    continue

                                if cgst_or_sgst:
                                    tax_rate *= 2
                                    if parent not in self.cgst_sgst_invoices:
                                        self.cgst_sgst_invoices.append(parent)

                                rate_based_dict = self.items_based_on_tax_rate.setdefault(  # noqa: 501
                                    parent, {}
                                ).setdefault(
                                    tax_rate, []
                                )
                                if item_code not in rate_based_dict:
                                    rate_based_dict.append(item_code)

                        except ValueError:
                            continue

        if unidentified_gst_accounts:
            frappe.msgprint(
                _("Following accounts might be selected in GST Settings:")
                + "<br>"
                + "<br>".join(unidentified_gst_accounts),
                alert=True,
            )

        # Build itemised tax for export invoices where tax table is blank
        for invoice_no, items in self.invoice_items.items():
            if (
                invoice_no in self.items_based_on_tax_rate
                or invoice_no in unidentified_gst_accounts_invoice
            ):
                continue

            invoice = self.invoices.get(invoice_no, {})
            if not invoice.get(
                "is_export_with_gst"
            ) and is_overseas_transaction(  # noqa: 501
                "Sales Invoice", invoice.gst_category, invoice.place_of_supply
            ):
                self.items_based_on_tax_rate.setdefault(
                    invoice_no, {}
                ).setdefault(  # noqa: 501
                    0, []
                ).extend(
                    items
                )

            # Show invoice with all items are in nil exempt or non gst
            if invoice_no in self.nil_exempt_non_gst:
                self.items_based_on_tax_rate.setdefault(
                    invoice_no, {}
                ).setdefault(  # noqa: 501
                    0, []
                ).extend(
                    items
                )

    def get_columns(self):
        self.other_columns = []
        self.tax_columns = []

        if self.filters.get("type_of_business") == "CDNR-REG":
            self.invoice_columns = [
                {
                    "fieldname": "billing_address_gstin",
                    "label": "GSTIN/UIN of Customer",
                    "fieldtype": "Data",
                    "width": 150,
                },
                {
                    "fieldname": "customer_name",
                    "label": "Customer Name",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "return_against",
                    "label": "Invoice/Advance receipt number",
                    "fieldtype": "Link",
                    "options": "Sales Invoice",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_date",
                    "label": "Invoice/Advance Receipt date",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_number",
                    "label": "Invoice/Advance credit note number",
                    "fieldtype": "Link",
                    "options": "Sales Invoice",
                    "width": 120,
                },
                {
                    "fieldname": "posting_date",
                    "label": "Credit Note date",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "is_reverse_charge",
                    "label": "Reverse Charge",
                    "fieldtype": "Data",
                },
                {
                    "fieldname": "export_type",
                    "label": "Export Type",
                    "fieldtype": "Data",
                    "hidden": 1,
                },
                {
                    "fieldname": "reason_for_issuing_document",
                    "label": "Reason For Issuing document",
                    "fieldtype": "Data",
                    "width": 140,
                },
                {
                    "fieldname": "cnp_cl_code",
                    "label": "CL Number",
                    "fieldtype": "Data",
                    "width": 100,
                },
                {
                    "fieldname": "place_of_supply",
                    "label": "Place Of Supply",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "gst_category",
                    "label": "GST Category",
                    "fieldtype": "Data",
                },
                {
                    "fieldname": "invoice_value",
                    "label": "Invoice Value",
                    "fieldtype": "Currency",
                    "width": 120,
                },
                {
                    "fieldname": "cnp_cgst",
                    "label": "CGST",
                    "fieldtype": "Currency",
                    "width": 100,
                },
                {
                    "fieldname": "cnp_sgst",
                    "label": "SGST",
                    "fieldtype": "Currency",
                    "width": 100,
                },
                {
                    "fieldname": "cnp_igst",
                    "label": "IGST",
                    "fieldtype": "Currency",
                    "width": 100,
                },
            ]
            self.other_columns = [
                {
                    "fieldname": "cess_amount",
                    "label": "Cess Amount",
                    "fieldtype": "Currency",
                    "width": 100,
                },
                {
                    "fieldname": "gstin_number",
                    "label": "Current GSTIN",
                    "fieldtype": "Data",
                    "width": 150,
                },
                {
                    "fieldname": "pre_gst",
                    "label": "PRE GST",
                    "fieldtype": "Data",
                    "width": 80,
                },
                {
                    "fieldname": "document_type",
                    "label": "Invoice Type",
                    "fieldtype": "Data",
                    "width": 80,
                },
            ]
        elif self.filters.get("type_of_business") == "CDNR-UNREG":
            self.invoice_columns = [
                {
                    "fieldname": "customer_name",
                    "label": "Customer Name",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "return_against",
                    "label": "Issued Against",
                    "fieldtype": "Link",
                    "options": "Sales Invoice",
                    "width": 120,
                },
                {
                    "fieldname": "posting_date",
                    "label": "Note Date",
                    "fieldtype": "Date",
                    "width": 120,
                },
                {
                    "fieldname": "invoice_number",
                    "label": "Note Number",
                    "fieldtype": "Link",
                    "options": "Sales Invoice",
                    "width": 120,
                },
                {
                    "fieldname": "export_type",
                    "label": "Export Type",
                    "fieldtype": "Data",
                    "hidden": 1,
                },
                {
                    "fieldname": "reason_for_issuing_document",
                    "label": "Reason For Issuing document",
                    "fieldtype": "Data",
                    "width": 140,
                },
                {
                    "fieldname": "place_of_supply",
                    "label": "Place Of Supply",
                    "fieldtype": "Data",
                    "width": 120,
                },
                {
                    "fieldname": "gst_category",
                    "label": "GST Category",
                    "fieldtype": "Data",
                },
                {
                    "fieldname": "invoice_value",
                    "label": "Invoice Value",
                    "fieldtype": "Currency",
                    "width": 120,
                },
            ]
            self.other_columns = [
                {
                    "fieldname": "cess_amount",
                    "label": "Cess Amount",
                    "fieldtype": "Currency",
                    "width": 100,
                },
                {
                    "fieldname": "pre_gst",
                    "label": "PRE GST",
                    "fieldtype": "Data",
                    "width": 80,
                },
                {
                    "fieldname": "document_type",
                    "label": "Document Type",
                    "fieldtype": "Data",
                    "width": 80,
                },
            ]

        self.columns = (
            self.invoice_columns + self.tax_columns + self.other_columns
        )  # noqa: 501
