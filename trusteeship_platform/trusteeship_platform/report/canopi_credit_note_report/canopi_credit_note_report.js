// Copyright (c) 2023, Castlecraft and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports['Canopi Credit Note Report'] = {
  filters: [
    {
      fieldname: 'company',
      label: 'Company',
      fieldtype: 'Link',
      options: 'Company',
      reqd: 1,
      default: frappe.defaults.get_user_default('Company'),
    },
    {
      fieldname: 'company_address',
      label: 'Address',
      fieldtype: 'Link',
      options: 'Address',
      get_query: function () {
        const company = frappe.query_report.get_filter_value('company');
        if (company) {
          return {
            query: 'frappe.contacts.doctype.address.address.address_query',
            filters: { link_doctype: 'Company', link_name: company },
          };
        }
      },
    },
    {
      fieldname: 'company_gstin',
      label: 'Company GSTIN',
      fieldtype: 'Autocomplete',
      get_query: function () {
        const company = frappe.query_report.get_filter_value('company');
        return ic.get_gstin_query(company);
      },
    },
    {
      fieldname: 'from_date',
      label: 'From Date',
      fieldtype: 'Date',
      reqd: 1,
      default: frappe.datetime.add_months(frappe.datetime.get_today(), -3),
      width: '80',
    },
    {
      fieldname: 'to_date',
      label: 'To Date',
      fieldtype: 'Date',
      reqd: 1,
      default: frappe.datetime.get_today(),
    },
    {
      fieldname: 'type_of_business',
      label: 'Type of Business',
      fieldtype: 'Select',
      reqd: 1,
      options: [
        {
          value: 'CDNR-REG',
          label: 'Credit/Debit Notes (Registered) - 9B',
        },
        {
          value: 'CDNR-UNREG',
          label: 'Credit/Debit Notes (Unregistered) - 9B',
        },
      ],
      default: 'CDNR-REG',
    },
  ],
};
