# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns(filters)
    data = get_data(filters)
    return columns, data


def get_columns(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    return [
        _("Particulars") + ":Data:300",
        _("Quarter Ended Sep 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended June 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended Sep 30, " + start_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Period Ended Sep 30, " + end_year) + ":Data:200" + "editable: true",
        _("Period Ended Sep 30, " + start_year)
        + ":Data:200"
        + "editable: true",  # noqa: 501
        _("Year Ended Mar 31, " + end_year) + ":Data:200" + "editable: true",
    ]


def get_data(filters):
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "note_note11to12_" + formatted_string

        if frappe.db.exists("Note", formatted_string):
            note = frappe.get_doc("Note", formatted_string)
            note_details = json.loads(note.cnp_note_details)
            data = note_details
        else:
            note = frappe.new_doc("Note")
            note.title = formatted_string
            note.cnp_note_details = note_json(filters)
            note.cnp_calc_details = note_calc()
            note.cnp_type = "Note 11 to 12"
            note.cnp_fiscal_year = filters.get("fiscal_year")
            note.cnp_company = filters.get("company")
            note.public = 1
            note.insert()
            frappe.db.commit()
            data = json.loads(note.cnp_note_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_json(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note11to12_details = [
        {
            "particulars": "Note 11: Revenue from Operations",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 1,
        },
        {
            "particulars": "Trusteeship Fees",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 2,
        },
        {
            "particulars": "Initial Acceptance Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 3,
        },
        {
            "particulars": "Annual Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 4,
        },
        {
            "particulars": "Servicing Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 5,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 6,
        },
        {
            "particulars": "Note 12 : Other Income",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 7,
        },
        {
            "particulars": "Interest on Fixed Deposits",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 8,
        },
        {
            "particulars": "Capital Gain on Mutual Fund",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 9,
        },
        {
            "particulars": "Profit of FA",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 10,
        },
        {
            "particulars": "FC Exchange Gain",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 11,
        },
        {
            "particulars": "Insurance claim received",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 12,
        },
        {
            "particulars": "Recovery of Bad Debts",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 13,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 14,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 15,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 16,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 17,
        },
        {
            "particulars": "Initial Acceptance Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 18,
        },
        {
            "particulars": "Annual Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 19,
        },
        {
            "particulars": "Servicing Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 20,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 21,
        },
        {
            "particulars": "Adjusted Operating Income",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 22,
        },
        {
            "particulars": "Add GST @ 12%",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 23,
        },
        {
            "particulars": "Total",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 24,
        },
    ]
    note11to12_details = json.dumps(note11to12_details)
    return note11to12_details


def note_calc():
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    calc_details = {
        "note12_period1_cash_flow_interest": 0,
        "note12_year_cash_flow_interest": 0,
        "note12_period1_cash_flow_net_gain": 0,
        "note12_year_cash_flow_net_gain": 0,
        "note11_quarter1": 0,
        "note11_quarter2": 0,
        "note11_quarter3": 0,
        "note11_period1": 0,
        "note11_period2": 0,
        "note11_year": 0,
        "note12_quarter1": 0,
        "note12_quarter2": 0,
        "note12_quarter3": 0,
        "note12_period1": 0,
        "note12_period2": 0,
        "note12_year": 0,
    }
    calc_details = json.dumps(calc_details)
    return calc_details
