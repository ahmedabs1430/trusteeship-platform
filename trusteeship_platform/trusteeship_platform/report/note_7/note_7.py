# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns()
    data = get_data(filters)
    return columns, data


def get_columns():
    return [
        _("Particulars") + ":Data:200",
        _("Computer Software") + ":Data:200",
    ]  # noqa: 501


def get_data(filters):
    # Retrieve the data from your JSON or use Frappe API to fetch the data
    # based on the filters and fiscal year.
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "note_note7_" + formatted_string

        if frappe.db.exists("Note", formatted_string):
            note = frappe.get_doc("Note", formatted_string)
            note_details = json.loads(note.cnp_note_details)
            data = note_details
        else:
            note = frappe.new_doc("Note")
            note.title = formatted_string
            note.cnp_note_details = note_json(filters)
            note.cnp_calc_details = note_calc()
            note.cnp_type = "Note 7"
            note.cnp_fiscal_year = filters.get("fiscal_year")
            note.cnp_company = filters.get("company")
            note.public = 1
            note.insert()
            frappe.db.commit()
            data = json.loads(note.cnp_note_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_json(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    end_year = str(end_year_date_object.year)
    note7_details = [
        {
            "particulars": "Gross block",
            "computer_software": " ",
            "indent": 0,
            "id": 1,
        },  # noqa: 501
        {
            "particulars": "At April 1, " + end_year,
            "computer_software": 0,
            "indent": 1,
            "id": 2,
        },
        {
            "particulars": "Additions",
            "computer_software": 0,
            "indent": 2,
            "id": 3,
        },  # noqa: 501
        {
            "particulars": "Disposals",
            "computer_software": 0,
            "indent": 2,
            "id": 4,
        },
        {
            "particulars": "At Sep 30, " + end_year,
            "computer_software": 0,
            "indent": 0,
            "id": 5,
        },
        {"particulars": " ", "computer_software": " ", "indent": 0, "id": 6},
        {
            "particulars": "Depreciation",
            "computer_software": " ",
            "indent": 0,
            "id": 7,
        },
        {
            "particulars": "At April 1, " + end_year,
            "computer_software": 0,
            "indent": 1,
            "id": 8,
        },
        {
            "particulars": "Charge for the period",
            "computer_software": 0,
            "indent": 2,
            "id": 9,
        },
        {
            "particulars": "Disposals",
            "computer_software": 0,
            "indent": 2,
            "id": 10,
        },
        {
            "particulars": "At Sep 30, " + end_year,
            "computer_software": 0,
            "indent": 0,
            "id": 11,
        },
        {"particulars": " ", "computer_software": " ", "indent": 0, "id": 12},
        {
            "particulars": "Net Block",
            "computer_software": " ",
            "indent": 0,
            "id": 13,
        },
        {
            "particulars": "At Sep 30, " + end_year,
            "computer_software": 0,
            "indent": 1,
            "id": 14,
        },
    ]
    note7_details = json.dumps(note7_details)
    return note7_details


def note_calc():
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    calc_details = {
        "note7_gross_block_computer_software": 0,
        "note7_depreciation_computer_software": 0,
        "note7_net_block_computer_software": 0,
    }
    calc_details = json.dumps(calc_details)
    return calc_details
