# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns(filters)
    data = get_data(filters)
    return columns, data


def get_columns(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    return [
        _("Particulars") + ":Data:300",
        _("Quarter Ended Sep 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended June 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended Sep 30, " + start_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Period Ended Sep 30, " + end_year) + ":Data:200" + "editable: true",
        _("Period Ended Sep 30, " + start_year)
        + ":Data:200"
        + "editable: true",  # noqa: 501
        _("Year Ended Mar 31, " + end_year) + ":Data:200" + "editable: true",
    ]


def get_data(filters):
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "note_note1to5_" + formatted_string

        if frappe.db.exists("Note", formatted_string):
            note = frappe.get_doc("Note", formatted_string)
            note_details = json.loads(note.cnp_note_details)
            data = note_details
        else:
            note = frappe.new_doc("Note")
            note.title = formatted_string
            note.cnp_note_details = note_json(filters)
            note.cnp_calc_details = note_calc()
            note.cnp_type = "Note 1 to 5"
            note.cnp_fiscal_year = filters.get("fiscal_year")
            note.cnp_company = filters.get("company")
            note.public = 1
            note.insert()
            frappe.db.commit()
            data = json.loads(note.cnp_note_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_json(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note1to5_details = [
        {
            "particulars": "Note 1: Share Capital",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 1,
        },
        {
            "particulars": "Authorised",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 2,
        },
        {
            "particulars": "50,00,000 Equity Shares of Rs.10/- each",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 3,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 4,
        },
        {
            "particulars": "Subscribed and Fully Paid-up",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 5,
        },
        {
            "particulars": "1,500,000 Equity Shares of Rs. 10/- each",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 6,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 7,
        },
        {
            "particulars": "The above Equity Shares are entirely held by Holding Company and its nominees.",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 8,
        },
        {
            "particulars": "Note 2: Reserves And Surplus",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 9,
        },
        {
            "particulars": "General Reserve",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 10,
        },
        {
            "particulars": "As per last Account",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 11,
        },
        {
            "particulars": "Add: Transfer from Profit & Loss Account",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 12,
        },
        {
            "particulars": "a",
            "Sep 30,  2023": 3,
            "June 30,  2023": 0,
            "Sep 30,  2022": 0,
            "Sep 30, 2023": 0,
            "Sep 30, 2022": 0,
            "Mar 31, 2023": 0,
            "indent": 1,
            "id": 13,
        },
        {
            "particulars": "Surplus",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 14,
        },
        {
            "particulars": "As per last Account",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 15,
        },
        {
            "particulars": "Profit/(Loss) during the period",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 16,
        },
        {
            "particulars": "Proposed Dividend",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 17,
        },
        {
            "particulars": "Transfer to General Reserve",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 18,
        },
        {
            "particulars": "b",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 19,
        },
        {
            "particulars": "(a+b)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 20,
        },
        {
            "particulars": "Note 3: Other Long Term Liabilities",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 21,
        },
        {
            "particulars": "Unearned Revenue",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 22,
        },
        {
            "particulars": "Lease Equalisation Account",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 23,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 24,
        },
        {
            "particulars": "Note 4: Provisions",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 25,
        },
        {
            "particulars": "Long Term",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 26,
        },
        {
            "particulars": "Provision for gratuity",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 27,
        },
        {
            "particulars": "Provision for Leave Benefits",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 28,
        },
        {
            "particulars": "Retention Pay",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 29,
        },
        {
            "particulars": "Provision for Employee Benefits (Variable Pay)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 30,
        },
        {
            "particulars": "a",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 31,
        },
        {
            "particulars": "Short Term",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 32,
        },
        {
            "particulars": "Provision for Employee Benefits (Variable Pay)Incl.Deputed",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 33,
        },
        {
            "particulars": "Provision for gratuity",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 34,
        },
        {
            "particulars": "Provision for Leave Benefits",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 35,
        },
        {
            "particulars": "Provision For Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 36,
        },
        {
            "particulars": "b",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 37,
        },
        {
            "particulars": "(a + b)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 38,
        },
        {
            "particulars": "Note 5: Other Current Liabilites",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 39,
        },
        {
            "particulars": "Trades Payable",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 40,
        },
        {
            "particulars": "a",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 41,
        },
        {
            "particulars": "Other Liabilities",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 42,
        },
        {
            "particulars": "Unearned Revenue",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 43,
        },
        {
            "particulars": "Other Advances",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 44,
        },
        {
            "particulars": "Employees Contribution to PF/ESIC",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 45,
        },
        {
            "particulars": "Professional Tax",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 46,
        },
        {
            "particulars": "Due to the Holding Company - Axis Bank Limited",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 47,
        },
        {
            "particulars": "TDS Payable",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 48,
        },
        {
            "particulars": "Service/GST Tax Payable",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 49,
        },
        {
            "particulars": "Income Tax Liability",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 50,
        },
        {
            "particulars": "b",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 51,
        },
        {
            "particulars": "(a + b)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 52,
        },
    ]
    note1to5_details = json.dumps(note1to5_details)
    return note1to5_details


def note_calc():
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    calc_details = {
        "note1_quarter1": 0,
        "note1_quarter2": 0,
        "note1_quarter3": 0,
        "note1_period1": 0,
        "note1_period2": 0,
        "note1_year": 0,
        "note2_quarter1": 0,
        "note2_quarter2": 0,
        "note2_quarter3": 0,
        "note2_period1": 0,
        "note2_period2": 0,
        "note2_year": 0,
        "note2_quarter1_a": 0,
        "note2_quarter2_a": 0,
        "note2_quarter3_a": 0,
        "note2_period1_a": 0,
        "note2_period2_a": 0,
        "note2_year_a": 0,
        "note2_quarter1_b": 0,
        "note2_quarter2_b": 0,
        "note2_quarter3_b": 0,
        "note2_period1_b": 0,
        "note2_period2_b": 0,
        "note2_year_b": 0,
        "note3_quarter1": 0,
        "note3_quarter2": 0,
        "note3_quarter3": 0,
        "note3_period1": 0,
        "note3_period2": 0,
        "note3_year": 0,
        "note4_quarter1_long_term": 0,
        "note4_quarter2_long_term": 0,
        "note4_quarter3_long_term": 0,
        "note4_period1_long_term": 0,
        "note4_period2_long_term": 0,
        "note4_year_long_term": 0,
        "note4_quarter1_short_term": 0,
        "note4_quarter2_short_term": 0,
        "note4_quarter3_short_term": 0,
        "note4_period1_short_term": 0,
        "note4_period2_short_term": 0,
        "note4_year_short_term": 0,
        "note5_quarter1_trade_payables": 0,
        "note5_quarter2_trade_payables": 0,
        "note5_quarter3_trade_payables": 0,
        "note5_period1_trade_payables": 0,
        "note5_period2_trade_payables": 0,
        "note5_year_trade_payables": 0,
        "note5_quarter1_other_liabilities": 0,
        "note5_quarter2_other_liabilities": 0,
        "note5_quarter3_other_liabilities": 0,
        "note5_period1_other_liabilities": 0,
        "note5_period2_other_liabilities": 0,
        "note5_year_other_liabilities": 0,
        "note5_period1_income_tax_liabilty": 0,
        "note5_year_income_tax_liabilty": 0,
        "note2_proposed_dividend_period1": 0,
        "note2_proposed_dividend_year": 0,
    }
    calc_details = json.dumps(calc_details)
    return calc_details
