# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns()
    data = get_data(filters)
    return columns, data


def get_columns():
    return [
        _("Particulars") + ":Data:200",
        _("Computers") + ":Data:200",
        _("Computer Server") + ":Data:200",
        _("Mobile") + ":Data:200",
        _("Furniture and fixtures") + ":Data:200",
        _("Office Equipment") + ":Data:200",
        _("Total") + ":Data:250",
    ]


def get_data(filters):
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "note_note6_" + formatted_string

        if frappe.db.exists("Note", formatted_string):
            note = frappe.get_doc("Note", formatted_string)
            note_details = json.loads(note.cnp_note_details)
            data = note_details
        else:
            note = frappe.new_doc("Note")
            note.title = formatted_string
            note.cnp_note_details = note_json(filters)
            note.cnp_calc_details = note_calc()
            note.cnp_type = "Note 6"
            note.cnp_fiscal_year = filters.get("fiscal_year")
            note.cnp_company = filters.get("company")
            note.public = 1
            note.insert()
            frappe.db.commit()
            data = json.loads(note.cnp_note_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_json(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    end_year = str(end_year_date_object.year)
    note6_details = [
        {
            "particulars": "Gross block",
            "computers": " ",
            "computer_server": " ",
            "mobile": " ",
            "furniture_and_fixtures": " ",
            "office_equipment": " ",
            "total": " ",
            "indent": 0,
            "id": 1,
        },
        {
            "particulars": "At April 1, " + end_year,
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 1,
            "id": 2,
        },
        {
            "particulars": "Additions",
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 2,
            "id": 3,
        },
        {
            "particulars": "Disposals",
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 2,
            "id": 4,
        },
        {
            "particulars": "At Sep 30, " + end_year,
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 0,
            "id": 5,
        },
        {
            "particulars": " ",
            "computers": " ",
            "computer_server": " ",
            "mobile": " ",
            "furniture_and_fixtures": " ",
            "office_equipment": " ",
            "total": " ",
            "indent": 0,
            "id": 6,
        },
        {
            "particulars": "Depreciation",
            "computers": " ",
            "computer_server": " ",
            "mobile": " ",
            "furniture_and_fixtures": " ",
            "office_equipment": " ",
            "total": " ",
            "indent": 0,
            "id": 7,
        },
        {
            "particulars": "At April 1, " + end_year,
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 1,
            "id": 8,
        },
        {
            "particulars": "Charge for the period",
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 2,
            "id": 9,
        },
        {
            "particulars": "Disposals",
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 2,
            "id": 10,
        },
        {
            "particulars": "At Sep 30, " + end_year,
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 0,
            "id": 11,
        },
        {
            "particulars": " ",
            "computers": " ",
            "computer_server": " ",
            "mobile": " ",
            "furniture_and_fixtures": " ",
            "office_equipment": " ",
            "total": " ",
            "indent": 0,
            "id": 12,
        },
        {
            "particulars": "Net Block",
            "computers": " ",
            "computer_server": " ",
            "mobile": " ",
            "furniture_and_fixtures": " ",
            "office_equipment": " ",
            "total": " ",
            "indent": 0,
            "id": 13,
        },
        {
            "particulars": "At Sep 30, " + end_year,
            "computers": 0,
            "computer_server": 0,
            "mobile": 0,
            "furniture_and_fixtures": 0,
            "office_equipment": 0,
            "total": 0,
            "indent": 1,
            "id": 14,
        },
    ]
    note6_details = json.dumps(note6_details)
    return note6_details


def note_calc():
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    calc_details = {
        "note6_gross_block_computers": 0,
        "note6_gross_block_computer_server": 0,
        "note6_gross_block_mobile": 0,
        "note6_gross_block_furniture_and_fixture": 0,
        "note6_gross_block_office_equipment": 0,
        "note6_gross_block_total": 0,
        "note6_depreciation_computers": 0,
        "note6_depreciation_computer_server": 0,
        "note6_depreciation_mobile": 0,
        "note6_depreciation_furniture_and_fixture": 0,
        "note6_depreciation_office_equipment": 0,
        "note6_depreciation_total": 0,
        "note6_net_block_computers": 0,
        "note6_net_block_computer_server": 0,
        "note6_net_block_mobile": 0,
        "note6_net_block_furniture_and_fixture": 0,
        "note6_net_block_office_equipment": 0,
        "note6_net_block_total": 0,
    }
    calc_details = json.dumps(calc_details)
    return calc_details
