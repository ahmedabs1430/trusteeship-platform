# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

# import frappe


import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns(filters)
    data = get_data(filters)
    return columns, data


def get_columns(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    return [
        _("Particulars") + ":Data:300",
        _("Quarter Ended Sep 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: E501
        _("Quarter Ended June 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: E501
        _("Quarter Ended Sep 30, " + start_year)
        + ":Data:250"
        + "editable: true",  # noqa: E501
        _("Period Ended Sep 30, " + end_year)
        + ":Data:200"
        + "editable: true",  # noqa: E501
        _("Period Ended Sep 30, " + start_year)
        + ":Data:200"
        + "editable: true",  # noqa: E501
        _("Year Ended Mar 31, " + end_year)
        + ":Data:200"
        + "editable: true",  # noqa: E501
    ]


def get_data(filters):
    # Retrieve the data from your JSON or
    # use Frappe API to fetch the data
    # based on the filters and fiscal year.
    # For example purposes,
    # we'll use the data you provided as a list of dictionaries.
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "report_balance_sheet_" + formatted_string
        if frappe.db.exists("Canopi Reports", formatted_string):
            canopi_reports = frappe.get_doc("Canopi Reports", formatted_string)
            report_details = json.loads(canopi_reports.report_details)
            data = report_details
        else:
            report_details = []
            report_calc_details = {}
            filter_year = frappe.get_doc(
                "Fiscal Year",
                filters.get("fiscal_year"),
            )
            date_format = "%Y-%m-%d"
            start_year_date_object = datetime.strptime(
                str(filter_year.year_start_date), date_format
            )
            end_year_date_object = datetime.strptime(
                str(filter_year.year_end_date), date_format
            )
            start_year = str(start_year_date_object.year)
            end_year = str(end_year_date_object.year)
            canopi_reports = frappe.new_doc("Canopi Reports")
            canopi_reports.title = formatted_string
            canopi_reports.fiscal_year = filters.get("fiscal_year")
            canopi_reports.company = filters.get("company")
            canopi_reports.type = "Balance Sheet"
            canopi_reports.calc_details = report_calc(report_calc_details)
            canopi_reports.report_details = json.dumps(
                get_json(report_details, start_year, end_year)
            )
            canopi_reports.insert()
            frappe.db.commit()
            data = json.loads(canopi_reports.report_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    # Replace the '-' in fiscal_year with '_' and concatenate
    # with formattedCompany
    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def report_calc(report_calc_details):
    report_calc_details = {
        "bs_non_current_assets_quarter1": 0,
        "bs_non_current_assets_quarter2": 0,
        "bs_non_current_assets_quarter3": 0,
        "bs_non_current_assets_period1": 0,
        "bs_non_current_assets_period2": 0,
        "bs_non_current_assets_year": 0,
        "bs_current_assets_quarter1": 0,
        "bs_current_assets_quarter2": 0,
        "bs_current_assets_quarter3": 0,
        "bs_current_assets_period1": 0,
        "bs_current_assets_period2": 0,
        "bs_current_assets_year": 0,
        "bs_fixed_assets_period1": 0,
        "bs_fixed_assets_year": 0,
    }
    report_calc_details = json.dumps(report_calc_details)
    return report_calc_details


def get_json(report_details, start_year, end_year):
    report_details = [
        {
            "particulars": "Equity and liabilities",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 1,
        },
        {
            "particulars": "Shareholders funds",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 2,
        },
        {
            "particulars": "Share capital",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 3,
        },
        {
            "particulars": "Reserves & surplus",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 4,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 5,
        },
        {
            "particulars": "",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 6,
        },
        {
            "particulars": "Non-current liabilites",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 7,
        },
        {
            "particulars": "Other long term liabilities",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 8,
        },
        {
            "particulars": "Long term provisions",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 9,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 10,
        },
        {
            "particulars": "",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 11,
        },
        {
            "particulars": "Current liabilities",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 12,
        },
        {
            "particulars": "Trade payables",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 13,
        },
        {
            "particulars": "Other current liabilities",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 14,
        },
        {
            "particulars": "Short term provisions",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 15,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 16,
        },
        {
            "particulars": "Total",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 17,
        },
        {
            "particulars": "Assets",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 18,
        },
        {
            "particulars": "Non-current assets",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 19,
        },
        {
            "particulars": "Fixed assets",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 20,
        },
        {
            "particulars": "Tangible assets",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 21,
        },
        {
            "particulars": "Intangible assets",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 22,
        },
        {
            "particulars": "Intangible asset under development",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 2,
            "id": 23,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 24,
        },
        {
            "particulars": "Deferred Tax Asset (net)",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 25,
        },
        {
            "particulars": "Other non-current assets",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 26,
        },
        {
            "particulars": "Loans &  Advances",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 27,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 28,
        },
        {
            "particulars": "Current assets",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 29,
        },
        {
            "particulars": "Current Investments",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 30,
        },
        {
            "particulars": "Loans &  Adavances",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 31,
        },
        {
            "particulars": "Trade receivables",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 32,
        },
        {
            "particulars": "Cash and Bank balances",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 33,
        },
        {
            "particulars": "Other current assets",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 34,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 35,
        },
        {
            "particulars": "Total",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 36,
        },
    ]
    return report_details
