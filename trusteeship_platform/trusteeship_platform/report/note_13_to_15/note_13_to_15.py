# Copyright (c) 2023, Castlecraft and contributors
# For license information, please see license.txt

import json
import re
from datetime import datetime

import frappe
from frappe import _


def execute(filters=None):
    columns, data = [], []
    columns = get_columns(filters)
    data = get_data(filters)
    return columns, data


def get_columns(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    return [
        _("Particulars") + ":Data:300",
        _("Quarter Ended Sep 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended June 30, " + end_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Quarter Ended Sep 30, " + start_year)
        + ":Data:250"
        + "editable: true",  # noqa: 501
        _("Period Ended Sep 30, " + end_year) + ":Data:200" + "editable: true",
        _("Period Ended Sep 30, " + start_year)
        + ":Data:200"
        + "editable: true",  # noqa: 501
        _("Year Ended Mar 31, " + end_year) + ":Data:200" + "editable: true",
    ]


def get_data(filters):
    if filters.get("fiscal_year") and filters.get("company"):
        formatted_string = format_company_name(
            filters.get("fiscal_year"), filters.get("company")
        )
        formatted_string = "note_note13to15_" + formatted_string

        if frappe.db.exists("Note", formatted_string):
            note = frappe.get_doc("Note", formatted_string)
            note_details = json.loads(note.cnp_note_details)
            data = note_details
        else:
            note = frappe.new_doc("Note")
            note.title = formatted_string
            note.cnp_note_details = note_json(filters)
            note.cnp_calc_details = note_calc()
            note.cnp_type = "Note 13 to 15"
            note.cnp_fiscal_year = filters.get("fiscal_year")
            note.cnp_company = filters.get("company")
            note.public = 1
            note.insert()
            frappe.db.commit()
            data = json.loads(note.cnp_note_details)
    else:
        data = []
    return data


def format_company_name(fiscal_year, company):
    # Split string by spaces
    words = company.split(" ")

    # Replace spaces with underscores and convert to lowercase
    formatted_company = "_".join(word.lower() for word in words)
    formatted_company = re.sub(
        r"[^a-z0-9_]", "", formatted_company
    )  # Remove any non-alphanumeric and non-underscore characters

    formatted_string = f"{fiscal_year.replace('-', '_')}_{formatted_company}"

    return formatted_string


def note_json(filters):
    filter_year = frappe.get_doc("Fiscal Year", filters.get("fiscal_year"))
    date_format = "%Y-%m-%d"
    start_year_date_object = datetime.strptime(
        str(filter_year.year_start_date), date_format
    )
    end_year_date_object = datetime.strptime(
        str(filter_year.year_end_date), date_format
    )
    start_year = str(start_year_date_object.year)
    end_year = str(end_year_date_object.year)
    note13to15_details = [
        {
            "particulars": "Note 13 : Employee Benefit Expenses",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 1,
        },
        {
            "particulars": "Salaries & Wages",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 2,
        },
        {
            "particulars": "Contribution to Provident, Gratuity and Other Funds",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 3,
        },
        {
            "particulars": "Staff Welfare Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 4,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 5,
        },
        {
            "particulars": "Note 14 : Other Expenses",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 6,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 1,
            "id": 7,
        },
        {
            "particulars": "Rent",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 8,
        },
        {
            "particulars": "Rates and Taxes",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 9,
        },
        {
            "particulars": "Printing and Stationery",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 10,
        },
        {
            "particulars": "Travelling & Conveyance",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 11,
        },
        {
            "particulars": "Conference Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 12,
        },
        {
            "particulars": "Communication Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 13,
        },
        {
            "particulars": "Legal & Professional Charges",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 14,
        },
        {
            "particulars": "Auditors Remuneration:",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 15,
        },
        {
            "particulars": "As Auditors",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 16,
        },
        {
            "particulars": "In other capacity for Certificates and other Services",  # noqa: 501
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 17,
        },
        {
            "particulars": "Directors Sitting Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 18,
        },
        {
            "particulars": "Electricity Charges",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 19,
        },
        {
            "particulars": "Bank Charges",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 20,
        },
        {
            "particulars": "Doubtful Debts Written Off",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 21,
        },
        {
            "particulars": "Provision for Doubtful Debts",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 22,
        },
        {
            "particulars": "DP Charges",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 23,
        },
        {
            "particulars": "Office Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 24,
        },
        {
            "particulars": "Advertisement & Business Promotion Expenses",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 25,
        },
        {
            "particulars": "Annual Maintainence Charges of Software",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 26,
        },
        {
            "particulars": "Registeration Fee",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 27,
        },
        {
            "particulars": "Contribution for CSR activities",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 28,
        },
        {
            "particulars": "Royalty Charges",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 29,
        },
        {
            "particulars": "Referral Fees",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 30,
        },
        {
            "particulars": "Other Payments",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 31,
        },
        {
            "particulars": "Exchange Loss",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 32,
        },
        {
            "particulars": "Microsoft Licence Fees / website Development",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 33,
        },
        {
            "particulars": "Bad debts recognised",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 34,
        },
        {
            "particulars": "Less : write Off",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 35,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 36,
        },
        {
            "particulars": "Note 15: Depreciation & Amortisation",
            "quarter_ended_sep_30,_" + end_year: " ",
            "quarter_ended_june_30,_" + end_year: " ",
            "quarter_ended_sep_30,_" + start_year: " ",
            "period_ended_sep_30,_" + end_year: " ",
            "period_ended_sep_30,_" + start_year: " ",
            "year_ended_mar_31,_" + end_year: " ",
            "indent": 0,
            "id": 37,
        },
        {
            "particulars": "On Tangible Fixed Assets",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 38,
        },
        {
            "particulars": "On Intangible Assets",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 1,
            "id": 39,
        },
        {
            "particulars": " ",
            "quarter_ended_sep_30,_" + end_year: 0,
            "quarter_ended_june_30,_" + end_year: 0,
            "quarter_ended_sep_30,_" + start_year: 0,
            "period_ended_sep_30,_" + end_year: 0,
            "period_ended_sep_30,_" + start_year: 0,
            "year_ended_mar_31,_" + end_year: 0,
            "indent": 0,
            "id": 40,
        },
    ]
    note13to15_details = json.dumps(note13to15_details)
    return note13to15_details


def note_calc():
    cnp_calc_details = "{}"
    calc_details = json.loads(cnp_calc_details)
    calc_details = {
        "note14_period1_provisions_doubtful_debts": 0,
        "note14_year_provisions_doubtful_debts": 0,
        "note13_quarter1": 0,
        "note13_quarter2": 0,
        "note13_quarter3": 0,
        "note13_period1": 0,
        "note13_period2": 0,
        "note13_year": 0,
        "note14_quarter1": 0,
        "note14_quarter2": 0,
        "note14_quarter3": 0,
        "note14_period1": 0,
        "note14_period2": 0,
        "note14_year": 0,
        "note15_quarter1": 0,
        "note15_quarter2": 0,
        "note15_quarter3": 0,
        "note15_period1": 0,
        "note15_period2": 0,
        "note15_year": 0,
    }
    calc_details = json.dumps(calc_details)
    return calc_details
