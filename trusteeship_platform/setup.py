import csv
import json
import os.path
from datetime import date, datetime

import frappe
from frappe import _
from frappe.model import no_value_fields
from frappe.utils import cint, cstr, duration_to_seconds, flt

INVALID_VALUES = ("", None)


def setup_wizard_complete(v):
    create_items()


def create_items():
    app_path = frappe.get_app_path("trusteeship_platform", "public")
    path = app_path + "/Item_List.csv"
    if os.path.isfile(path):
        fields = frappe.get_meta("Item").fields
        fieldName = {}
        fieldType = {}
        for df in fields:
            fieldtype = df.fieldtype or "Data"
            if fieldtype in no_value_fields:
                continue
            label = (df.label or "").strip()
            translated_label = _(label)
            fieldName[translated_label] = df.fieldname
            fieldType[df.fieldname] = df.fieldtype

        with open(path) as csvfile:
            reader = csv.DictReader(csvfile)

            for row in reader:
                if all(v in INVALID_VALUES for v in row):
                    # empty row
                    continue
                if not frappe.db.exists("Item", row["Item Code"]):
                    item_doc = frappe.new_doc("Item")
                    data = {}
                    for key in row:
                        fieldname = fieldName.get(key.strip())
                        fieldtype = fieldType.get(fieldname)
                        value = row[key]
                        value = parse_value(fieldtype, value)
                        data[fieldname] = value

                    json_data = json.dumps(data)
                    item_doc.update(json.loads(json_data))
                    item_doc.insert()


def parse_value(fieldtype, value):

    if isinstance(value, (datetime, date)) and fieldtype in [
        "Date",
        "Datetime",
    ]:  # noqa: 501
        return value

    value = cstr(value)

    # convert boolean values to 0 or 1
    valid_check_values = ["t", "f", "true", "false", "yes", "no", "y", "n"]
    if fieldtype == "Check" and value.lower().strip() in valid_check_values:
        value = value.lower().strip()
        value = 1 if value in ["t", "true", "y", "yes"] else 0

    if fieldtype in ["Int", "Check"]:
        value = cint(value)
    elif fieldtype in ["Float", "Percent", "Currency"]:
        value = flt(value)
    elif fieldtype in ["Date", "Datetime"]:
        value = get_date(value)
    elif fieldtype == "Duration":
        value = duration_to_seconds(value)

    return value


def get_date(value):
    if isinstance(value, (datetime, date)):
        return value
