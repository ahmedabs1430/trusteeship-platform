import datetime
import hashlib
import hmac
import json
import time

import frappe
import requests
from frappe import _


@frappe.whitelist(allow_guest=True)
def store_event_data(**data):
    payout_datetime = frappe.utils.now_datetime()
    merchant_id = frappe.request.args.get("merchant_id")
    header_data = dict(frappe.request.headers)
    event_id = header_data.get("X-Razorpay-Event-Id")
    data["event_id"] = event_id
    data["merchant_id"] = merchant_id
    validated = validate_webhook(
        header_data.get("X-Razorpay-Signature"), merchant_id=merchant_id
    )  # noqa
    if not validated:
        message = f"Webhook Not Authenticated,\nHeader-{frappe.request.headers}, data-{frappe.request.data}"  # noqa
        return create_error_log("Payout Pending", "Critical", message)
    if data.get("event") == "payout.pending":
        # store_payout_pending_event(data)
        data = frappe.parse_json(data)
        frappe.enqueue(
            "trusteeship_platform.api._authorize_payout",
            data=data,
            merchant_id=merchant_id,
            payout_datetime=payout_datetime,
            queue="short",
        )
    elif data.get("event") == "transaction.created":
        store_transactions(data)
    return "Event Data Stored !"


def _authorize_payout(data, merchant_id, payout_datetime):
    payout = data.get("payload", {}).get("payout", {}).get("entity", {})
    contact = payout.get("fund_account", {}).get("contact", {})
    approval_status = ""
    if contact.get("type") and (
        contact.get("type").lower() not in ["borrower", "investor"]
    ):
        approval_status = "reject"
    else:
        approval_status = "approve"
    authorize_payout(
        merchant_id, payout.get("id"), data, approval_status, payout_datetime
    )


def store_payout_pending_event(data):
    try:
        # Parse the JSON data
        event_data = frappe.parse_json(data)
        frappe.enqueue(
            "trusteeship_platform.api._insert_payout_pending_document",
            event_data=event_data,
        )
        return _("Payout Pending Event stored successfully.")
    except Exception as e:
        return create_error_log("Payout Pending", "Warn", e)


def store_transactions(data):
    try:
        # Parse the JSON data
        event_data = frappe.parse_json(data)
        frappe.enqueue(
            "trusteeship_platform.api._insert_p2p_transactions",
            event_data=event_data,
        )  # noqa
        return _("Transactions stored successfully.")
    except Exception as e:
        create_error_log("Transactions", "Warn", e)


# Insert Payout Pending Payload (Webhook API)
def _insert_payout_pending_document(event_data):
    try:
        if frappe.db.exists(
            "CP2P Payout Pending Event",
            {"event_id": event_data.get("event_id")},  # noqa
        ):
            return create_error_log("Payout Pending", "Warn", "Duplicate Entry")  # noqa
        payout = (
            event_data.get("payload", {}).get("payout", {}).get("entity", {})
        )  # noqa
        created_at = event_data.get("created_at")
        dt = convert_string_to_datetime(created_at)

        # Store Contact Information
        merchant = frappe.db.get_value(
            "CP2P Merchant", {"merchant_id": event_data.get("merchant_id")}
        )
        payout_datetime = event_data.get("payout_datetime")
        authorization_datetime = event_data.get("authorization_datetime")
        event_data.pop("payout_datetime")
        event_data.pop("authorization_datetime")

        contact_id = store_contact_information(payout, merchant)
        fund_account_id = payout.get("fund_account", {}).get("id", {})
        create_fund_account(merchant, fund_account_id)
        # Create a new Payout Pending Event document
        data_dict = {
            "doctype": "CP2P Payout Pending Event",
            "merchant": merchant,
            "event_id": event_data.get("event_id"),
            "payment_platform": "",  # TODO: Payment Platform Addition
            "account_id": event_data.get("account_id"),
            "event": event_data.get("event"),
            "payout_id": payout.get("id"),
            "amount": payout.get("amount") / 100,
            "currency": payout.get("currency"),
            "status": payout.get("status"),
            "notes": payout.get("notes") if payout.get("notes") else {},
            "purpose": payout.get("purpose"),
            "narration": payout.get("narration"),
            "status_details": payout.get("status_details", {}),
            "fund_account_id": payout.get("fund_account", {}).get("id", {}),  # noqa
            "contact_id": contact_id,
            "created_at": dt,
            "fees": payout.get("fees"),
            "tax": payout.get("tax"),
            "utr": payout.get("utr"),
            "mode": payout.get("mode"),
            "reference_id": payout.get("reference_id"),
            "batch_id": payout.get("batch_id"),
            "description": payout.get("status_details", {}).get("description"),  # noqa
            "source": payout.get("status_details", {}).get("source"),
            "reason": payout.get("status_details", {}).get("reason"),
            "payload": event_data,
        }
        new_event = frappe.get_doc(data_dict)
        # Save the document
        new_event.insert(ignore_permissions=True)
        frappe.db.set_value(
            "CP2P Payout Pending Event",
            new_event.name,
            "payout_datetime",
            payout_datetime,
        )
        frappe.db.set_value(
            "CP2P Payout Pending Event",
            new_event.name,
            "authorization_datetime",
            authorization_datetime,
        )

        contact = payout.get("fund_account", {}).get("contact", {})
        approval_status = ""
        if contact.get("type") and (
            contact.get("type").lower() not in ["borrower", "investor"]
        ):
            approval_status = "reject"
        else:
            approval_status = "approve"

        cache = frappe.cache()
        auth_response = cache.get(payout.get("id"))
        if auth_response:
            data = json.loads(auth_response.decode("utf-8"))
        else:
            data = {}
        if data.get("id"):
            status = "Accepted" if approval_status == "approve" else "Rejected"  # noqa
        else:
            create_error_log(
                "Payout Pending",
                "Critical",
                f"Payout Not Authorized \n{data}",  # noqa
            )
            status = "Pending"
        _set_approval_status(status, new_event.name, data_dict)

    except Exception as e:
        frappe.logger("utils").exception(e)
        create_error_log("Payout Pending", "Warn", e)


def authorize_payout(
    merchant, payout_id, data, approval_status, payout_datetime
):  # noqa
    try:
        api_data = frappe.db.get_value(
            "CP2P Merchant",
            {"merchant_id": merchant},
            ["access_token", "razorpay_url"],
            as_dict=True,  # noqa
        )
        if api_data:
            url = (
                api_data.get("razorpay_url")
                + f"/payouts/{payout_id}/{approval_status}"  # noqa
            )  # noqa
            if approval_status == "approve":
                remarks = "Approved"
            else:
                remarks = "Contact Type Is Invalid"
            payload = json.dumps({"remarks": remarks})
            headers = {
                "Content-Type": "application/json",
                "Authorization": f"Bearer {api_data.get('access_token')}",
            }  # no qa
            response = requests.request(
                "POST", url, headers=headers, data=payload
            )  # noqa
            response_data = response.json()

            cache = frappe.cache()
            cache_key = payout_id
            value = json.dumps(response_data)
            cache.set(cache_key, value)

            event_data = frappe.parse_json(data)
            event_data["payout_datetime"] = payout_datetime
            event_data["authorization_datetime"] = frappe.utils.now_datetime()

            # Long Queue the Insertion Part
            frappe.enqueue(
                "trusteeship_platform.api._insert_payout_pending_document",
                event_data=event_data,
                queue="long",
            )
    except Exception as e:
        create_error_log("Payout Pending-Approval", "Warn", e)


def _set_approval_status(status, payout_pending, data_dict):
    if status == "Accepted":
        _store_approval_accepted(data_dict)
    frappe.db.set_value(
        "CP2P Payout Pending Event",
        payout_pending,
        "approval_status",
        status,  # noqa
    )
    frappe.db.set_value(
        "CP2P Payout Pending Event",
        payout_pending,
        "approval_status_time",
        frappe.utils.now_datetime(),  # noqa
    )


def _store_approval_accepted(data_dict):
    data_dict["doctype"] = "CP2P Authorized Payouts"
    data_dict["approval_status_time"] = frappe.utils.now_datetime()
    new_doc = frappe.get_doc(data_dict)
    new_doc.insert(ignore_permissions=True)


def create_fund_account(merchant_id, fund_account_id):
    if frappe.db.exists("CP2P Fund Accounts", {"fund_id": fund_account_id}):
        return
    api_data = frappe.db.get_value(
        "CP2P Merchant",
        merchant_id,
        ["access_token", "razorpay_url"],
        as_dict=True,  # noqa
    )
    if api_data:
        url = api_data.get("razorpay_url") + f"/fund_accounts/{fund_account_id}"  # noqa
        payload = {}
        headers = {"Authorization": f"Bearer {api_data.get('access_token')}"}

        response = requests.request("GET", url, headers=headers, data=payload)
        item = response.json()
        item["merchant"] = merchant_id
        _function = (
            "trusteeship_platform.api._insert_fund_account_by_account_type"  # noqa
        )
        frappe.enqueue(_function, item=item)
        return "Payout authentication initiated."


def store_contact_information(payout, merchant):
    try:
        contact = payout.get("fund_account", {}).get("contact", {})
        if not contact:
            return
        if frappe.db.exists("CP2P Contacts", contact.get("id")):
            return contact.get("id")
        dt = convert_string_to_datetime(contact.get("created_at"))
        new_contact = frappe.get_doc(
            {
                "doctype": "CP2P Contacts",
                "merchant": merchant,
                "id": contact.get("id"),
                "entity": contact.get("entity"),
                "name1": contact.get("name"),
                "contact": contact.get("contact"),
                "email": contact.get("email"),
                "type": contact.get("type"),
                "reference_id": contact.get("reference_id"),
                "batch_id": contact.get("batch_id"),
                "active": contact.get("active"),
                "created_at": dt,
            }
        )
        new_contact.insert(ignore_permissions=True)
        return contact.get("id")
    except Exception as e:
        create_error_log("CP2P Contact", "Warn", e)


def convert_string_to_datetime(date_str):
    dt = datetime.datetime.fromtimestamp(date_str)
    return dt


# Insert Contacts Via PULL API
def _insert_contacts(data):
    try:
        for item in data["items"]:
            if not frappe.db.exists("CP2P Contacts", item.get("id")):
                dt = convert_string_to_datetime(item.get("created_at"))
                item["doctype"] = "CP2P Contacts"
                new_contact = frappe.get_doc(
                    {
                        "doctype": "CP2P Contacts",
                        "merchant_id": data.get("merchant_id"),
                        "id": item.get("id"),
                        "entity": item.get("entity"),
                        "name1": item.get("name"),
                        "contact": item.get("contact"),
                        "email": item.get("email"),
                        "type": item.get("type"),
                        "reference_id": item.get("reference_id"),
                        "batch_id": item.get("batch_id"),
                        "active": item.get("active"),
                        "created_at": dt,
                    }
                )
                new_contact.insert(ignore_permissions=True)
    except Exception as e:
        create_error_log("Transactions", "Warn", e)


# Insert Transaction Created Event Payload (Webhook API)
def _insert_p2p_transactions(event_data):
    try:
        if frappe.db.exists(
            "CP2P Transactions", {"event_id": event_data.get("event_id")}
        ):  # noqa
            return create_error_log("Payout Pending", "Warn", "Duplicate Entry")  # noqa
        event_id = event_data.get("event_id")
        account_id = event_data.get("account_id")
        event = event_data.get("event")
        transaction = (
            event_data.get("payload", {})
            .get("transaction", {})
            .get("entity", {})  # noqa
        )
        transaction_id = transaction.get("id")
        account_number = transaction.get("account_number")
        amount = transaction.get("amount") / 100
        currency = transaction.get("currency")
        credit = transaction.get("credit") / 100
        debit = transaction.get("debit") / 100
        balance = transaction.get("balance") / 100
        source = transaction.get("source", {})
        source_id = source.get("id")
        source_entity = source.get("entity")
        fund_account_id = source.get("fund_account_id")
        source_amount = source.get("amount") / 100
        fees = source.get("fees") / 100
        tax = source.get("tax") / 100
        status = source.get("status")
        utr = source.get("utr")
        mode = source.get("mode")
        created_at = event_data.get("created_at")
        source_created_at = source.get("created_at")
        dt = convert_string_to_datetime(created_at)
        source_dt = convert_string_to_datetime(source_created_at)
        # Create a new CP2P Transactions document
        merchant = frappe.db.get_value(
            "CP2P Merchant", {"merchant_id": event_data.get("merchant_id")}
        )
        new_transaction = frappe.get_doc(
            {
                "doctype": "CP2P Transactions",
                "merchant": merchant,
                "event_id": event_id,
                "payment_platform": "",  # TODO: Payment Platform Addition
                "account_id": account_id,
                "event": event,
                "transaction_id": transaction_id,
                "account_number": account_number,
                "transaction_amount": amount,
                "currency": currency,
                "credit": credit,
                "debit": debit,
                "balance": balance,
                "source_id": source_id,
                "source_entity": source_entity,
                "fund_account_id": fund_account_id,
                "source_amount": source_amount,
                "fees": fees,
                "tax": tax,
                "status": status,
                "utr": utr,
                "mode": mode,
                "created_at": dt,
                "source_created_at": source_dt,
                "payload": event_data,
            }
        )

        # Save the document
        new_transaction.insert(ignore_permissions=True)
        _fetch_utr_from_transaction(merchant, transaction_id)
        if not frappe.db.exists(
            "CP2P Payout Pending Event", {"payout_id": source_id}
        ):  # noqa
            create_error_log(
                "Transactions",
                "Critical",
                f"Transaction Invalid, ID-{new_transaction.name}",
            )  # noqa
        return "Data stored successfully!"
    except Exception as e:
        create_error_log("Transactions", "Warn", f"{transaction_id}-{e}")  # noqa


def _fetch_utr_from_transaction(merchant, transaction_id):
    api_data = frappe.db.get_value(
        "CP2P Merchant",
        merchant,
        ["access_token", "razorpay_url"],
        as_dict=True,  # noqa
    )
    if api_data:
        frappe.logger("utils").exception(api_data)
        url = api_data.get("razorpay_url") + f"/transactions/{transaction_id}"  # noqa
        payload = {}
        headers = {"Authorization": f"Bearer {api_data.get('access_token')}"}
        response = requests.request("GET", url, headers=headers, data=payload)
        data = response.json()
        frappe.logger("utils").exception(data)
        if data.get("id"):
            utr = data.get("source", {}).get("utr", {})
            frappe.db.set_value(
                "CP2P Transactions",
                {"transaction_id": transaction_id},
                "utr",
                utr,  # noqa
            )


# Pull API to be run Via Schedular Every 4 hours.
def fetch_contacts(merchant_id):
    api_data = frappe.db.get_value(
        "CP2P Merchant",
        merchant_id,
        ["access_token", "razorpay_url"],
        as_dict=True,  # noqa
    )

    if api_data:
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_data.get('access_token')}",
        }  # no qa
        url = api_data.get("razorpay_url") + "/contacts"
        to_time = int(time.time())  # get current timestamp
        from_time = to_time - 14400  # get timestamp of 4 hours ago
        params = {"from": from_time, "to": to_time}
        response = requests.get(url, headers=headers, data=params)  # noqa
        data = response.json()
        data["merchant_id"] = merchant_id
        frappe.enqueue("trusteeship_platform.api._insert_contacts", data=data)
        return "Payout authentication initiated."


# Pull API to be run Via Schedular Every 4 hours to get transactions.
def fetch_transactions(merchant_id):
    api_data = frappe.db.get_value(
        "CP2P Merchant",
        merchant_id,
        ["access_token", "razorpay_url"],
        as_dict=True,  # noqa
    )
    if api_data:
        url = api_data.get("razorpay_url") + "/transactions"  # noqa
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_data.get('access_token')}",
        }  # no qa
        account_number = api_data.get("account_number")
        to_time = int(time.time())  # get current timestamp
        from_time = to_time - 14400  # get timestamp of 4 hours ago
        params = {"account_number": account_number}
        params["from"] = from_time
        params["to"] = to_time
        payload = json.dumps(params)
        response = requests.get(url, headers=headers, data=payload)  # noqa
        data = response.json()
        data["merchant_id"] = merchant_id
        _function = "trusteeship_platform.api._insert_transactions"
        frappe.enqueue(_function, data=data)
        return "Payout authentication initiated."


# Pull API to be run Via Schedular Every 4 hours to get payouts.
def fetch_payouts(
    merchant_id, contact_id=None
):  # Contact ID is optional, if not provided, all payouts for the account will be fetched.  # noqa
    api_data = frappe.db.get_value(
        "CP2P Merchant",
        merchant_id,
        ["access_token", "razorpay_url"],
        as_dict=True,  # noqa
    )

    if api_data:
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_data.get('access_token')}",
        }  # no qa
        url = api_data.get("razorpay_url") + "/payouts"
        acount_number = api_data.get("account_number")
        to_time = int(time.time())  # get current timestamp
        from_time = to_time - 14400  # get timestamp of 4 hours ago
        params = {
            "account_number": acount_number,
            "from": from_time,
            "to": to_time,
            "contact_id": contact_id,
        }
        response = requests.get(url, headers=headers, data=params)  # noqa
        data = response.json()
        data["merchant_id"] = merchant_id
        frappe.enqueue("trusteeship_platform.api._insert_payouts", data=data)
        return "Payout authentication initiated."


# Pull API to be run Via Schedular Every 4 hours.
def fetch_fund_accounts(merchant_id):
    api_data = frappe.db.get_value(
        "CP2P Merchant",
        merchant_id,
        ["access_token", "razorpay_url"],
        as_dict=True,  # noqa
    )
    if api_data:
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_data.get('access_token')}",
        }  # no qa
        url = api_data.get("razorpay_url") + "/fund_accounts"
        to_time = int(time.time())  # get current timestamp
        from_time = to_time - 14400  # get timestamp of 4 hours ago
        params = {"from": from_time, "to": to_time}
        response = requests.get(url, headers=headers, data=params)  # noqa
        data = response.json()
        data["merchant"] = merchant_id
        _function = "trusteeship_platform.api._insert_fund_accounts"
        frappe.enqueue(_function, data=data)
        return "Payout authentication initiated."


# Insert Transactions Via PULL API
def _insert_transactions(data):
    try:
        for item in data["items"]:
            if not frappe.db.exists(
                "CP2P Transactions", {"transaction_id": item.get("id")}
            ):  # noqa
                dt = convert_string_to_datetime(item.get("created_at"))
                item["doctype"] = "CP2P Transactions"
                transaction_id = item.get("id")
                account_number = item.get("account_number")
                amount = item.get("amount") / 100  # Convert Paisa Into Rupees
                currency = item.get("currency")
                credit = item.get("credit")
                debit = item.get("debit")
                balance = item.get("balance")
                source = item.get("source", {})
                source_id = source.get("id")
                source_entity = source.get("entity")
                created_at = item.get("created_at")
                dt = convert_string_to_datetime(created_at)

                if source_entity == "payout":
                    fund_account_id = source.get("fund_account_id")
                    source_amount = (
                        source.get("amount") / 100
                    )  # Convert Paisa Into Rupees
                    fees = source.get("fees")
                    tax = source.get("tax")
                    status = source.get("status")
                    utr = source.get("utr")
                    mode = source.get("mode")

                    # Create a new CP2P Transactions document
                    new_transaction = frappe.get_doc(
                        {
                            "doctype": "CP2P Transactions",
                            "merchant_id": data.get("merchant_id"),
                            "transaction_id": transaction_id,
                            "account_number": account_number,
                            "transaction_amount": amount,
                            "currency": currency,
                            "credit": credit,
                            "debit": debit,
                            "balance": balance,
                            "source_id": source_id,
                            "source_entity": source_entity,
                            "fund_account_id": fund_account_id,
                            "source_amount": source_amount,
                            "fees": fees,
                            "tax": tax,
                            "status": status,
                            "utr": utr,
                            "mode": mode,
                            "created_at": dt,
                        }
                    )
                    # Save the document
                    new_transaction.insert(ignore_permissions=True)
        return "Data stored successfully!"
    except Exception as e:
        create_error_log("Transactions", "Warn", e)


# Insert Payouts Via PULL API
def _insert_payouts(data):
    try:
        for item in data["items"]:
            if not frappe.db.exists(
                "CP2P Payout Pending Event", {"payout_id": item.get("id")}
            ):  # noqa
                created_at = item.get("created_at")
                dt = convert_string_to_datetime(created_at)
                # Create a new Payout Pending Event document
                new_event = frappe.get_doc(
                    {
                        "doctype": "CP2P Payout Pending Event",
                        "merchant_id": data.get("merchant_id"),
                        "payout_id": item.get("id"),
                        "amount": item.get("amount") / 100,
                        "currency": item.get("currency"),
                        "status": item.get("status"),
                        "purpose": item.get("purpose"),
                        "narration": item.get("narration"),
                        "status_details": item.get("status_details", {}),
                        "fund_account_id": item.get("fund_account_id"),
                        "created_at": dt,
                        "fees": item.get("fees"),
                        "tax": item.get("tax"),
                        "utr": item.get("utr"),
                        "mode": item.get("mode"),
                        "reference_id": item.get("reference_id"),
                        "batch_id": item.get("batch_id"),
                        "description": item.get("status_details", {}).get(
                            "description"
                        ),  # noqa
                        "source": item.get("status_details", {}).get("source"),
                        "reason": item.get("status_details", {}).get("reason"),
                        "payload": item,
                    }
                )
                # Save the document
                new_event.insert(ignore_permissions=True)
    except Exception as e:
        create_error_log("Payout Pending", "Warn", e)


# Insert Fund Accounts Via PULL API
def _insert_fund_accounts(data):
    try:
        for item in data["items"]:
            if not frappe.db.exists(
                "CP2P Fund Accounts", {"fund_id": item.get("id")}
            ):  # noqa
                item["merchant"] = data.get("merchant")
                _insert_fund_account_by_account_type(item)
    except Exception as e:
        create_error_log("Fund Accounts", "Warn", e)


def _insert_fund_account_by_account_type(item):
    created_at = item.get("created_at")
    dt = convert_string_to_datetime(created_at)
    # Create a new Fund Account Event document
    if item.get("account_type") == "bank_account":
        bank_account = item.get("bank_account", {})
        name = bank_account.get("name")
        ifsc = bank_account.get("ifsc")
        bank_name = bank_account.get("bank_name")
        account_number = bank_account.get("account_number")
        new_event = frappe.get_doc(
            {
                "doctype": "CP2P Fund Accounts",
                "merchant": item.get("merchant"),
                "fund_id": item.get("id"),
                "entity": item.get("entity"),
                "contact_id": item.get("contact_id"),
                "account_type": "Bank Account",
                "active": item.get("active"),
                "batch_id": item.get("batch_id"),
                "created_at": dt,
                "name1": name,
                "ifsc": ifsc,
                "bank_name": bank_name,
                "account_number": account_number,
            }
        )
        # Save the document
        new_event.insert(ignore_permissions=True)

    elif item.get("account_type") == "vpa":
        new_event = frappe.get_doc(
            {
                "doctype": "CP2P Fund Accounts",
                "merchant": item.get("merchant"),
                "fund_id": item.get("id"),
                "entity": item.get("entity"),
                "contact_id": item.get("contact_id"),
                "account_type": "VPA",
                "active": item.get("active"),
                "batch_id": item.get("batch_id"),
                "created_at": dt,
                "username": item.get("vpa", {}).get("username"),
                "handle": item.get("vpa", {}).get("handle"),
                "address": item.get("vpa", {}).get("address"),
            }
        )
        # Save the document
        new_event.insert(ignore_permissions=True)

    elif item.get("account_type") == "card":
        new_event = frappe.get_doc(
            {
                "doctype": "CP2P Fund Accounts",
                "merchant": item.get("merchant"),
                "fund_id": item.get("id"),
                "entity": item.get("entity"),
                "contact_id": item.get("contact_id"),
                "account_type": "Card",
                "active": item.get("active"),
                "batch_id": item.get("batch_id"),
                "created_at": dt,
                "name1": item.get("card", {}).get("name"),
                "last4": item.get("card", {}).get("last4"),
                "network": item.get("card", {}).get("network"),
                "type": item.get("card", {}).get("type"),
                "issuer": item.get("card", {}).get("issuer"),
            }
        )
        # Save the document
        new_event.insert(ignore_permissions=True)

    elif item.get("account_type") == "wallet":
        new_event = frappe.get_doc(
            {
                "doctype": "CP2P Fund Accounts",
                "merchant": item.get("merchant"),
                "fund_id": item.get("id"),
                "entity": item.get("entity"),
                "contact_id": item.get("contact_id"),
                "account_type": "Wallet",
                "active": item.get("active"),
                "batch_id": item.get("batch_id"),
                "created_at": dt,
                "name1": item.get("wallet", {}).get("name"),
                "phone": item.get("wallet", {}).get("phone"),
                "provider": item.get("wallet", {}).get("provider"),
                "email": item.get("wallet", {}).get("email"),
            }
        )
        # Save the document
        new_event.insert(ignore_permissions=True)


def validate_webhook(webhook_signature, merchant_id):
    try:
        api_data = frappe.db.get_value(
            "CP2P Merchant",
            {"merchant_id": merchant_id},
            ["webhook_secret"],
            as_dict=True,
        )  # noqa
        expected_signature = hmac.new(
            api_data.get("webhook_secret").encode("utf-8"),
            frappe.request.data,
            hashlib.sha256,
        ).hexdigest()
        if expected_signature != webhook_signature:
            return False
        else:
            return True
    except Exception:
        return False


def create_error_log(title, type, error):
    err_log = frappe.get_doc(
        {
            "doctype": "CP2P Error Log",
            "type": type,
            "title": title,
            "error": error,
        }  # noqa
    )
    err_log.insert(ignore_permissions=True)
    return "Error Occured"
