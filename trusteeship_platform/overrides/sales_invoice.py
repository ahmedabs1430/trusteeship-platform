import calendar
import io
import os
from datetime import date, datetime, timedelta

import frappe
from dateutil import relativedelta
from frappe.utils.data import get_datetime_str, get_url
from frappe.utils.pdf import get_pdf
from pyhanko.pdf_utils.incremental_writer import IncrementalPdfFileWriter
from pyhanko.sign import signers
from pyhanko.sign.fields import SigFieldSpec, append_signature_field

from trusteeship_platform.custom_methods import (  # noqa: 501 isort: skip
    generate_qrcode,
    get_doc_list,
    get_next_series_number,
    get_pfx_from_s3,
    validate_user,
    asign_task,
)

from trusteeship_platform.public.india.e_invoice.utils import (  # noqa: 501 isort: skip
    generate_irn,
    validate_eligibility,
)


def validate(self, method=None):
    validate_quotation(self)
    validate_currency(self)
    validate_address(self)
    set_tax_amounts(self)
    validate_account(self)


def on_submit(self, method=None):
    validate_address(self)
    if self.cnp_action == 1:
        email_action(
            self.doctype,
            self.name,
            "1",
            "sales-invoice",
            redirect=False,
            email_token=self.cnp_email_token,
        )
        create_amortization(docname=self.name)
        doc = frappe.get_doc("Sales Invoice", self.name)
        if validate_eligibility(doc):
            generate_irn(self.doctype, self.name)


def on_update(self, method=None):
    if (self.cnp_approval == "Approved") or (
        self.cnp_approval == "Rejected"
    ):  # noqa: 501
        if (
            frappe.db.get_value(
                "Sales Invoice", self.name, "cnp_is_approved_rejected"
            )  # noqa: 501
            == 0
        ):
            frappe.db.set_value(
                "Sales Invoice", self.name, "cnp_is_approved_rejected", 1
            )


def before_insert(self, method=None):
    self.cnp_is_approved_rejected = 0


def after_insert(self, method=None):
    if self.cnp_from_quotation:
        doc = frappe.get_doc("Quotation", self.cnp_from_quotation)
        time = datetime.now()
        current_time = time.strftime("%H:%M:%S")
        doc.append(
            "cnp_payment_schedule",
            {
                "sales_invoice": self.name,
                "date": datetime.today(),
                "time": current_time,
                "total_amount": self.grand_total,
            },
        )
        oppo_doc = frappe.get_doc("Opportunity", doc.cnp_opportunity)
        frappe.db.set_value(
            "Opportunity", doc.cnp_opportunity, "status", "Converted"
        )  # noqa: 501
        total_tenor_months = (
            oppo_doc.cnp_tenor_years * 12
            + oppo_doc.cnp_tenor_months
            + (1 if oppo_doc.cnp_tenor_days else 0)
        )
        if not doc.cnp_invoice_schedule_details:
            for items in doc.items:
                next_date = date.today()
                if items.cnp_type_of_fee == "Annual Fee":
                    doc.append(
                        "cnp_invoice_schedule_details",
                        {
                            "sales_invoice": "Sales Invoice 1",
                            "scheduled_date": date.today(),
                        },
                    )
                    if items.cnp_frequency == "M":
                        total_tenor_months -= 1
                        i = 2
                        for months in range(total_tenor_months):
                            next_date += relativedelta.relativedelta(
                                months=+1
                            )  # noqa: 501
                            doc.append(
                                "cnp_invoice_schedule_details",
                                {
                                    "sales_invoice": "Sales Invoice "
                                    + str(i),  # noqa: 501
                                    "scheduled_date": next_date,
                                },
                            )
                            i += 1
                    if items.cnp_frequency == "Q":
                        total_tenor_months -= 3
                        i = 2
                        invoices = int(total_tenor_months / 3)
                        for months in range(invoices):
                            next_date += relativedelta.relativedelta(
                                months=+3
                            )  # noqa: 501
                            doc.append(
                                "cnp_invoice_schedule_details",
                                {
                                    "sales_invoice": "Sales Invoice "
                                    + str(i),  # noqa: 501
                                    "scheduled_date": next_date,
                                },
                            )
                            i += 1
                    if items.cnp_frequency == "H":
                        total_tenor_months -= 6
                        i = 2
                        invoices = int(total_tenor_months / 6) + 1
                        for months in range(invoices):
                            next_date += relativedelta.relativedelta(
                                months=+6
                            )  # noqa: 501
                            doc.append(
                                "cnp_invoice_schedule_details",
                                {
                                    "sales_invoice": "Sales Invoice "
                                    + str(i),  # noqa: 501
                                    "scheduled_date": next_date,
                                },
                            )
                            i += 1
                    if items.cnp_frequency == "Y":
                        total_tenor_months -= 12
                        i = 2
                        invoices = int(total_tenor_months / 12) + 1
                        for months in range(invoices):
                            next_date += relativedelta.relativedelta(
                                months=+12
                            )  # noqa: E501
                            doc.append(
                                "cnp_invoice_schedule_details",
                                {
                                    "sales_invoice": "Sales Invoice "
                                    + str(i),  # noqa: 501
                                    "scheduled_date": next_date,
                                },
                            )
                            i += 1

        if doc.cnp_is_sales_invoice_generated == 0:
            doc.cnp_is_sales_invoice_generated = 1
        doc.save()


def on_trash(self, method=None):
    quot_list = get_doc_list("Quotation")
    new_list = []
    for quot in quot_list:
        if quot.name == self.cnp_from_quotation:
            quot_doc = frappe.get_doc("Quotation", quot.name)
            for items in quot_doc.cnp_payment_schedule:
                if items.sales_invoice != self.name:
                    new_list.append(items)
            if not new_list:
                quot_doc.cnp_invoice_schedule_details = []
            quot_doc.cnp_payment_schedule = new_list
            quot_doc.save()


def before_save(self, method=None):
    self.cnp_is_chargeable = frappe.db.get_value(
        "Quotation", self.cnp_from_quotation, "cnp_is_chargeable"
    )
    if self.is_return == 0:
        self.cnp_is_partial_credit_note = 0


def set_value_cnp_proforma_no_serial_no(doctype, docname, doc):
    filtered_items = [
        item for item in doc.items if item.cnp_type_of_fee == "Annual Fee"
    ]
    if doc.docstatus == 1 and filtered_items and doc.is_return == 0:
        fiscal_year = frappe.get_doc(
            "Fiscal Year", frappe.defaults.get_user_default("fiscal_year")
        )
        year_start_date = fiscal_year.year_start_date.strftime("%y")
        series_type = "AT/CO/" + year_start_date
        filters = [["cnp_proforma_no", "Like", "%" + series_type + "%"]]
        custom_naming_series = series_type + "/PSI/{:04d}".format(
            get_next_series_number("Sales Invoice", "cnp_proforma_no", filters)
        )
        frappe.db.set_value(
            doctype, docname, "cnp_proforma_no", custom_naming_series
        )  # noqa: 501


def set_tax_amounts(self):
    for item in self.taxes:
        if "CGST" in item.account_head:
            self.cnp_cgst = item.tax_amount
        if "SGST" in item.account_head:
            self.cnp_sgst = item.tax_amount
        if "IGST" in item.account_head:
            self.cnp_igst = item.tax_amount


@frappe.whitelist()
def send_approval_mail(doc_name, doc_type, doctype_name, doc_disable=""):
    url = get_url()
    my_attachments = []

    if doc_type == "Sales Invoice":
        sales_doc = frappe.get_doc(doc_type, doc_name)
        sales_doc.save()
    if doc_type == "Purchase Invoice":
        purchase_doc = frappe.get_doc(doc_type, doc_name)
        purchase_doc.save()

    token = frappe.generate_hash()
    token_expiry = datetime.now() + timedelta(days=90)
    frappe.db.set_value(
        doc_type, doc_name, "workflow_state", "Pending for Approval"
    )  # noqa: 501
    if (
        doc_type == "Canopi Fixed Deposit Opening Entry"
        or doc_type == "Canopi Fixed Deposit Maturity"
        or doc_type == "Canopi Fund Closure Entry"
        or doc_type == "Canopi Mutual Funds Opening Entry"
        or doc_type == "Canopi Redemption Accounting"
    ):
        frappe.db.set_value(doc_type, doc_name, "email_token", token)
        frappe.db.set_value(
            doc_type, doc_name, "email_token_expiry", token_expiry
        )  # noqa: 501
    if (
        doc_type == "Sales Invoice"
        or doc_type == "Journal Entry"
        or doc_type == "Account"
        or doc_type == "Payment Entry"
        or doc_type == "Purchase Invoice"
    ):  # noqa: 501
        frappe.db.set_value(doc_type, doc_name, "cnp_email_token", token)
        frappe.db.set_value(
            doc_type, doc_name, "cnp_email_token_expiry", token_expiry
        )  # noqa: 501
    frappe.db.commit()
    sales_doc = frappe.get_doc(doc_type, doc_name)
    content1 = f"""<h2>{doc_type} Approval</h2>"""
    if (
        doc_type == "Canopi Fixed Deposit Opening Entry"
        or doc_type == "Canopi Mutual Funds Opening Entry"
        or doc_type == "Canopi Redemption Accounting"
    ):  # noqa: 501
        content3 = f"""
        <p><a href= "{url}/api/method/trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.canopi_fixed_deposit_opening_entry.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}&email_token={sales_doc.email_token}" class="button">Approve</a>
        <a href= "{url}/api/method/trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_opening_entry.canopi_fixed_deposit_opening_entry.email_action?doctype={doc_type}&docname={doc_name}&action=2&doctype_name={doctype_name}&email_token={sales_doc.email_token}" class="button_1">Reject</a></p>"""  # noqa: 501
    elif doc_type == "Canopi Fund Closure Entry":  # noqa: 501
        content3 = f"""
        <p><a href= "{url}/api/method/trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fund_closure_entry.canopi_fund_closure_entry.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}&email_token={sales_doc.email_token}" class="button">Approve</a>
        <a href= "{url}/api/method/trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fund_closure_entry.canopi_fund_closure_entry.email_action?doctype={doc_type}&docname={doc_name}&action=2&doctype_name={doctype_name}&email_token={sales_doc.email_token}" class="button_1">Reject</a></p>"""  # noqa: 501
    elif doc_type == "Canopi Fixed Deposit Maturity":
        content3 = f"""
        <p><a href= "{url}/api/method/trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_maturity.canopi_fixed_deposit_maturity.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}&email_token={sales_doc.email_token}" class="button">Approve</a>
        <a href= "{url}/api/method/trusteeship_platform.trusteeship_platform___accounts.doctype.canopi_fixed_deposit_maturity.canopi_fixed_deposit_maturity.email_action?doctype={doc_type}&docname={doc_name}&action=2&doctype_name={doctype_name}&email_token={sales_doc.email_token}" class="button_1">Reject</a></p>"""  # noqa: 501
    elif (
        doc_type == "Journal Entry"
        or doc_type == "Account"
        or doc_type == "Payment Entry"
        or doc_type == "Purchase Invoice"
    ):  # noqa: 501
        content3 = f"""
            <p><a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}&email_token={sales_doc.cnp_email_token}" class="button">Approve</a>
            <a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=2&doctype_name={doctype_name}&email_token={sales_doc.cnp_email_token}" class="button_1">Reject</a></p>"""  # noqa: 501
    elif doc_type == "Sales Invoice":  # noqa: 501
        if sales_doc.is_return == 1:
            content3 = f"""
                <p><a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}&email_token={sales_doc.cnp_email_token}" class="button">Approve</a>
                <a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action_reject?doctype={doc_type}&docname={doc_name}&doctype_name={doctype_name}&email_token={sales_doc.cnp_email_token}" class="button_1">Reject</a></p>"""  # noqa: 501
        else:
            content3 = f"""
            <p><a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}&email_token={sales_doc.cnp_email_token}" class="button">Approve</a>
            <a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=2&doctype_name={doctype_name}&email_token={sales_doc.cnp_email_token}" class="button_1">Reject</a></p>"""  # noqa: 501
    else:
        content3 = f"""
            <p><a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=1&doctype_name={doctype_name}" class="button">Approve</a>
            <a href= "{url}/api/method/trusteeship_platform.overrides.sales_invoice.email_action?doctype={doc_type}&docname={doc_name}&action=2&doctype_name={doctype_name}" class="button_1">Reject</a></p>"""  # noqa: 501

    cont = """<style>
    .button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    }
    .button_1 {
    background-color: red;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    }
    </style>"""

    recipient = []

    Users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Accounts Manager"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for usr in Users:
        recipient.append(usr.email)
        asign_task(
            {
                "assign_to": [usr.email],
                "doctype": doc_type,
                "name": doc_name,
                "description": "",
                "priority": "High",
                "notify": 1,
                "action": "",
            }
        )
    content2 = f""" <p><a href="{url}/app/{doctype_name}/{sales_doc.name}">{sales_doc.name} </a></p>"""  # noqa: 501
    if doc_disable and doc_type == "Account":
        content2 += f""" {doc_name} is disabled"""
    elif not doc_disable and doc_type == "Account":
        content2 += f""" {doc_name} is enabled"""

    frappe.sendmail(
        recipients=recipient,
        subject=doc_type + " Approval",
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )


def validate_quotation(self):
    if (
        self.customer
        != frappe.db.get_value(
            "Quotation", self.cnp_from_quotation, "cnp_customer_code"
        )
        and not self.cnp_is_debit_note
    ):
        frappe.throw("Quotation does not exists for selected customer")


def validate_currency(self):
    if self.currency != "INR":
        transaction_date = self.posting_date
        filters = [
            ["from_currency", "=", self.currency],
            ["to_currency", "=", "INR"],
            ["for_selling", "=", "1"],
        ]
        entries = frappe.get_all(
            "Currency Exchange",
            fields=["exchange_rate"],
            filters=filters,
            order_by="date desc",
            limit=1,
        )
        if entries:
            filters = [
                ["date", "=", get_datetime_str(transaction_date)],
                ["from_currency", "=", self.currency],
                ["to_currency", "=", "INR"],
                ["for_selling", "=", "1"],
            ]
            entries = frappe.get_all(
                "Currency Exchange",
                fields=["exchange_rate"],
                filters=filters,
                order_by="date desc",
                limit=1,
            )
            if not entries:
                frappe.throw(
                    "The exchange rate for this currency has not been "
                    + "updated today. Kindly update the exchange rate "
                    + "to proceed with the transaction"
                )


def validate_address(self):
    customer_address = self.customer_address
    if not customer_address:
        frappe.throw("Please select an address to proceed")

    doc = frappe.get_doc("Address", customer_address)

    if (
        not doc.address_title
        or not doc.address_type
        or not doc.address_line1
        or not doc.pincode
        # or not doc.gstin
        or not doc.city
        or not doc.state
    ):
        frappe.throw(
            "Address Lines, City, State, Pin code, GSTIN are mandatory "
            + "for address. Please set them and try again"
        )


@frappe.whitelist()
def email_action(
    doctype,
    docname,
    action,
    doctype_name,
    redirect=True,
    email_token="",
):  # noqa: 501
    doc = frappe.get_doc(doctype, docname)
    validate_user("Accounts Manager")

    if (
        doc.cnp_email_token != email_token
        or doc.cnp_email_token_expiry <= datetime.now()  # noqa: 501
    ):  # noqa: 501
        frappe.throw("The link you followed has expired")
    frappe.db.set_value(doctype, docname, "cnp_email_token", "")
    frappe.db.set_value(doctype, docname, "cnp_email_token_expiry", None)  # noqa: 501

    if redirect and action == "1":
        if doctype == "Account":
            frappe.db.set_value(doctype, docname, "disabled", 0)
        else:
            frappe.db.set_value(doctype, docname, "docstatus", 1)

    if redirect and action == "2":
        if doctype == "Account":
            frappe.db.set_value(doctype, docname, "disabled", 1)
        else:
            frappe.db.set_value(doctype, docname, "docstatus", 2)

    if action == "1":
        status = "Approved"
    if action == "2":
        status = "Rejected"
    if doctype == "Sales Invoice":
        set_value_cnp_proforma_no_serial_no(doctype, docname, doc)

    frappe.db.set_value(doctype, docname, "workflow_state", status)

    frappe.db.commit()

    url = get_url()
    my_attachments = []

    content1 = f"""<h2>{doctype} {status}</h2>"""
    content3 = f"""
        <p>{docname} in {doctype} is {status}</p>"""
    cont = """<style>

    </style>"""

    recipient = []

    Users = frappe.get_all(
        "User",
        filters=[
            ["Has Role", "role", "=", "Accounts User"],
            ["User", "name", "!=", "Administrator"],
            ["User", "name", "!=", "Guest"],
        ],
        fields=["email"],
    )
    for usr in Users:
        recipient.append(usr.email)
        asign_task(
            {
                "assign_to": [usr.email],
                "doctype": doctype,
                "name": docname,
                "description": "",
                "priority": "High",
                "notify": 1,
                "action": status,
            }
        )

    content2 = f""" <p><a href="{url}/app/{doctype_name}/{doc.name}">{doc.name} </a></p>"""  # noqa: 501
    frappe.sendmail(
        recipients=recipient,
        subject=doctype + " " + status,
        attachments=my_attachments,
        message=cont + content1 + content2 + content3,
    )

    if redirect is True:
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = (
            url + "/app/" + doctype_name + "/" + docname
        )  # noqa: 501


@frappe.whitelist()
def email_action_reject(
    doctype,
    docname,
    doctype_name,
    redirect=True,
    email_token="",
):  # noqa: 501
    doc = frappe.get_doc(doctype, docname)
    validate_user("Accounts Manager")
    if (
        doc.cnp_email_token != email_token
        or doc.cnp_email_token_expiry <= datetime.now()
    ):  # noqa: 501
        frappe.throw("The link you followed has expired")
    frappe.db.set_value(doctype, docname, "cnp_email_token", "")
    frappe.db.set_value(doctype, docname, "cnp_email_token_expiry", None)  # noqa: 501
    frappe.db.set_value(doctype, docname, "cnp_is_reject_reason", 1)  # noqa: 501
    frappe.db.commit()
    url = get_url()
    if redirect is True:
        frappe.local.response["type"] = "redirect"
        frappe.local.response["location"] = (
            url + "/app/" + doctype_name + "/" + docname
        )  # noqa: 501


def create_amortization(docname):
    new_amortization = False
    sdoc = frappe.get_doc("Sales Invoice", docname)
    invoice_date = sdoc.posting_date
    cl_code = sdoc.cnp_cl_code
    quotation = sdoc.cnp_from_quotation
    for item in sdoc.items:
        if "Annual Fee" in item.cnp_type_of_fee:
            annual_fee = item.amount
            new_amortization = True

    qdoc = frappe.get_doc("Quotation", quotation)
    for item in qdoc.items:
        if "Annual Fee" in item.cnp_type_of_fee:
            frequency = item.cnp_frequency
    if new_amortization:
        amtz_doc = frappe.new_doc("Canopi Amortization")
        amtz_doc.posting_date = datetime.today()
        amtz_doc.sales_invoice = docname
        amtz_doc.account_to_be_debited = (
            "1010010005 - Income Received in Advance - ATSL"
        )
        amtz_doc.account_to_be_credited = "6020001001 - Annual Fee - ATSL"
        amtz_doc.company = sdoc.company
        amtz_doc.start_date = invoice_date
        amtz_doc.annual_fee = annual_fee
        amtz_doc.frequency = frequency
        amtz_doc.cnp_cl_code = cl_code

        fiscal_doc_list = get_doc_list(
            "Fiscal Year", fields=('["name"]'), filters=[]
        )  # noqa: 501
        for fiscal in fiscal_doc_list:
            fiscal_doc = frappe.get_doc("Fiscal Year", fiscal)
            if (
                fiscal_doc.year_start_date <= date.today()
                and fiscal_doc.year_end_date >= date.today()
            ):
                start_date = invoice_date
                if amtz_doc.frequency == "Y":
                    amtz_doc.end_date = fiscal_doc.year_end_date + timedelta(
                        days=0
                    )  # noqa: 501
                if amtz_doc.frequency == "H":
                    half_year = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=6)
                        + timedelta(days=-1)
                    )
                    if start_date <= half_year:
                        amtz_doc.end_date = half_year
                    else:
                        amtz_doc.end_date = fiscal_doc.year_end_date
                        +timedelta(days=0)
                if amtz_doc.frequency == "Q":
                    quarter_1 = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=3)
                        + timedelta(days=-1)
                    )
                    quarter_2 = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=6)
                        + timedelta(days=-1)
                    )
                    quarter_3 = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=9)
                        + timedelta(days=-1)
                    )
                    if start_date <= quarter_1:
                        amtz_doc.end_date = quarter_1
                    elif start_date <= quarter_2:
                        amtz_doc.end_date = quarter_2
                    elif start_date <= quarter_3:
                        amtz_doc.end_date = quarter_3
                    else:
                        amtz_doc.end_date = fiscal_doc.year_end_date
                        +timedelta(days=0)
                if amtz_doc.frequency == "M":
                    days_in_month = calendar.monthrange(
                        amtz_doc.start_date.year, amtz_doc.start_date.month
                    )[1]
                    amtz_doc.end_date = datetime(
                        amtz_doc.start_date.year,
                        amtz_doc.start_date.month,
                        days_in_month,
                    ).date()

        delta = amtz_doc.end_date - amtz_doc.start_date
        amtz_doc.no_of_days = delta.days
        amtz_doc.amount = round(annual_fee / amtz_doc.no_of_days, 2)
        amtz_doc.docstatus = 1
        amtz_doc.insert()


@frappe.whitelist()
def get_debit_note_qrcode(docname):
    doc = frappe.get_doc("Sales Invoice", docname)
    doc.cnp_debit_note_qr_code = generate_qrcode(docname)
    doc.save()


@frappe.whitelist()
def download_debit_note_invoice(docname):
    doc = frappe.get_doc("Sales Invoice", docname)
    company = frappe.get_doc("Company", doc.company)
    html = frappe.get_print("Sales Invoice", docname, "Canopi Debit Note")
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)

    out = None
    if company.cnp_sign_secret:
        file_path = get_pfx_from_s3(company.cnp_s3_key)
        signer = signers.SimpleSigner.load_pkcs12(
            pfx_file=file_path,
            passphrase=(company.get_password("cnp_sign_secret")).encode(),
        )
        x1 = 380
        y1 = 70
        x2 = x1 + 140
        y2 = y1 + 54
        append_signature_field(
            w,
            SigFieldSpec(
                sig_field_name=company.name,
                box=(x1, y1, x2, y2),
            ),
        )
        out = signers.sign_pdf(
            w,
            signers.PdfSignatureMetadata(field_name=company.name),
            signer=signer,  # noqa: 501
        )
        os.remove(file_path)

    frappe.local.response.filename = "{name}-debit_note.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )

    frappe.local.response.filecontent = (
        out.getbuffer() if out else get_pdf(html)
    )  # noqa: 501
    frappe.local.response.type = "pdf"


@frappe.whitelist()
def get_credit_note_qrcode(docname):
    doc = frappe.get_doc("Sales Invoice", docname)
    doc.cnp_credit_note_qr_code = generate_qrcode(docname)
    doc.save()


@frappe.whitelist()
def download_credit_note_invoice(docname):
    doc = frappe.get_doc("Sales Invoice", docname)
    company = frappe.get_doc("Company", doc.company)
    html = frappe.get_print("Sales Invoice", docname, "Canopi Credit Note")
    file = get_pdf(html)
    file = io.BytesIO(file)
    w = IncrementalPdfFileWriter(file)

    out = None
    if company.cnp_sign_secret:
        file_path = get_pfx_from_s3(company.cnp_s3_key)
        signer = signers.SimpleSigner.load_pkcs12(
            pfx_file=file_path,
            passphrase=(company.get_password("cnp_sign_secret")).encode(),
        )
        x1 = 380
        y1 = 100
        x2 = x1 + 140
        y2 = y1 + 54
        append_signature_field(
            w,
            SigFieldSpec(
                sig_field_name=company.name,
                box=(x1, y1, x2, y2),
            ),
        )
        out = signers.sign_pdf(
            w,
            signers.PdfSignatureMetadata(field_name=company.name),
            signer=signer,  # noqa: 501
        )
        os.remove(file_path)

    frappe.local.response.filename = "{name}-credit_note.pdf".format(
        name=docname.replace(" ", "-").replace("/", "-")
    )

    frappe.local.response.filecontent = (
        out.getbuffer() if out else get_pdf(html)
    )  # noqa: 501
    frappe.local.response.type = "pdf"


def validate_account(self):
    for item in self.items:
        if item.income_account:
            income_doc = frappe.get_doc("Account", item.income_account)
            if income_doc.disabled == 1:
                frappe.throw(
                    "Cannot create accounting entries against disabled account:"  # noqa: 501
                    + income_doc.name
                )
        if item.expense_account:
            expense_doc = frappe.get_doc("Account", item.expense_account)
            if expense_doc.disabled == 1:
                frappe.throw(
                    "Cannot create accounting entries against disabled account :"  # noqa: 501
                    + expense_doc.name
                )


@frappe.whitelist()
def default_acct():
    account_list = get_doc_list("Account")
    income_received = ""
    initial_fee_account = ""
    one_time_account = ""
    for account in account_list:
        if "1010010005 - Income Received in Advance - ATSL" in account.name:
            income_received = account.name
        if "6020001010 - Initial Acceptance Fee - ATSL" in account.name:
            initial_fee_account = account.name
        if "1010010005 - 6020001015 - One Time Fee - ATSL" in account.name:
            one_time_account = account.name

    return {
        "income_received": income_received,
        "initial_fee_account": initial_fee_account,
        "one_time_account": one_time_account,
    }


@frappe.whitelist()
def is_mandate_flow_done(quotation):
    quotation_doc = frappe.get_doc("Quotation", quotation)

    # add more products as per requirement
    mandate_flow_products = ["DTE", "STE"]

    if quotation_doc.cnp_product.upper() in mandate_flow_products and any(
        item.cnp_type_of_fee == "Annual Fee" for item in quotation_doc.items
    ):
        documentation = frappe.get_all(
            "Canopi Documentation", filters=[["quotation", "=", quotation]]
        )
        if len(documentation):
            # get first `name` as there will only be
            # one documentation per quotation

            documentation_name = next(doc.name for doc in documentation)

            transaction_doc_filters = [
                ["canopi_documentation_name", "=", documentation_name],
                ["status", "like", "%Approved%"],
            ]

            if quotation_doc.cnp_product.upper() == "DTE":
                transaction_doc_filters.append(["is_dta", "=", 1])
            elif quotation_doc.cnp_product.upper() == "STE":
                transaction_doc_filters.append(
                    ["is_for_document_execution", "=", 1]
                )  # noqa: 501

            transaction_doc_post = frappe.get_all(
                "Canopi Transaction Documents Post Execution",
                filters=transaction_doc_filters,
            )

            return True if len(transaction_doc_post) else False
        else:
            return False

    return True
