from frappe import _


def get_data(data=None):
    return {
        "fieldname": "opportunity",
        "transactions": [
            {
                "items": [
                    "Quotation",
                ],
            },
            {
                "label": _("Operations Flow"),
                "items": ["ATSL Mandate List"],
            },
        ],
    }
