from datetime import date, timedelta

import frappe
from dateutil import relativedelta
from frappe.utils.data import get_url, getdate

from erpnext.selling.doctype.quotation.quotation import (  # noqa: 501 isort:skip
    _make_customer,
)

from trusteeship_platform.custom_methods import (  # noqa: 501 isort:skip
    get_doc_list,
    get_next_series_number,
    get_signed_pdf,
)


def before_save(self, method=None):
    add_approval_role(self)


def before_insert(self, method=None):
    set_value_cnp_proforma_no_serial_no(self)


def on_cancel(self, method=None):
    frappe.db.set_value(
        "Quotation", self.name, "workflow_state", "Cancelled"
    )  # noqa: 501
    frappe.db.commit()


def after_insert(self, method=None):
    customer = _make_customer(self.name)
    customer = frappe.get_doc("Customer", customer.name)
    if not customer.opportunity_name:
        customer.opportunity_name = self.cnp_opportunity
        customer.save()
    frappe.db.set_value(
        "Quotation", self.name, "cnp_customer_code", customer.name
    )  # noqa: 501
    frappe.db.commit()
    self.reload()
    update_opportunity(self, self.cnp_opportunity, customer.name)


def validate(self, method=None):
    update_tranche_flow_history(self)
    validate_next_fee_date(self)
    validate_address(self)
    set_offer_letter_serial_no(self)


def on_submit(self, method=None):
    validate_address(self)


def on_update_after_submit(self, method=None):
    if self.workflow_state == "Accepted By Client":
        if not self.cnp_s3_key:
            frappe.throw("Accepted Offer Letter Not Uploaded")
        if not self.cnp_offer_acceptence_date:
            frappe.throw(
                "Offer Acceptance/Renegotiation Date is not there"
            )  # noqa: 501


def add_approval_role(self):
    approval_settings = frappe.get_single("Approval Settings")
    if approval_settings.deviation_conditions:
        row = next(
            row
            for row in approval_settings.deviation_conditions
            if is_approval_deviation_rules_valid(
                row,
                self.cnp_deviation_change,
            )
        )
        user_roles = frappe.get_roles(frappe.session.user)
        is_bcgrm = row.role == "BCG RM" and "BCG RM" in user_roles
        if (
            self.docstatus == 0
            and self.workflow_state != "Pending for Approval"
            and self.cnp_send_for_approval != 1
            and is_bcgrm
        ):
            self.cnp_approval_role = row.role
        elif (
            self.docstatus == 0
            and row.approval in user_roles
            and self.workflow_state == "Draft"
        ):
            self.cnp_approval_role = row.role
        elif row.role in user_roles:
            self.cnp_approval_role = row.role
        else:
            self.cnp_approval_role = ""


def is_approval_deviation_rules_valid(row, deviation):
    return (
        (row.condition == "Less than" and deviation < row.deviation)
        or (
            row.condition == "Range"
            and row.from_dev <= deviation
            and row.to_dev >= deviation
        )
        or (row.condition == "Greater than" and deviation > row.deviation)
        or (row.condition == "Equal to" and deviation == row.deviation)
        or (row.condition == "Not Equal to" and deviation != row.deviation)
    )


@frappe.whitelist()
def calculate_next_fee_date(type_of_fee, frequency):
    fiscal_doc_list = get_doc_list(
        "Fiscal Year", fields=('["name"]'), filters=[]
    )  # noqa: 501
    next_fee_date = None
    for fiscal in fiscal_doc_list:
        fiscal_doc = frappe.get_doc("Fiscal Year", fiscal)
        if (
            fiscal_doc.year_start_date <= date.today()
            and fiscal_doc.year_end_date >= date.today()
        ):
            if type_of_fee == "Annual Fee":
                if frequency == "Y":
                    next_fee_date = fiscal_doc.year_end_date + timedelta(
                        days=1
                    )  # noqa: 501
                if frequency == "H":
                    half_year = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=6)
                    )
                    now = date.today()
                    if date.today() < half_year:
                        next_fee_date = (
                            date.today()
                            + relativedelta.relativedelta(  # noqa: 501
                                months=date_diff_in_months(now, half_year)
                            )
                        )
                    else:
                        next_fee_date = fiscal_doc.year_end_date + timedelta(
                            days=1
                        )  # noqa: 501
                if frequency == "Q":
                    quarter_1 = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=3)
                    )
                    quarter_2 = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=6)
                    )
                    quarter_3 = (
                        fiscal_doc.year_start_date
                        + relativedelta.relativedelta(months=9)
                    )
                    now = date.today()
                    if date.today() < quarter_1:
                        next_fee_date = (
                            date.today()
                            + relativedelta.relativedelta(  # noqa: 501
                                months=date_diff_in_months(now, quarter_1)
                            )
                        )
                    elif date.today() < quarter_2:
                        next_fee_date = (
                            date.today()
                            + relativedelta.relativedelta(  # noqa: 501
                                months=date_diff_in_months(now, quarter_2)
                            )
                        )
                    elif date.today() < quarter_3:
                        next_fee_date = (
                            date.today()
                            + relativedelta.relativedelta(  # noqa: 501
                                months=date_diff_in_months(now, quarter_3)
                            )
                        )
                    else:
                        next_fee_date = fiscal_doc.year_end_date + timedelta(
                            days=1
                        )  # noqa: 501
                if frequency == "M":
                    next_fee_date = date.today() + relativedelta.relativedelta(
                        months=1
                    )  # noqa: 501
    return next_fee_date


def date_diff_in_months(start_date, end_date):
    return (
        end_date.month
        - start_date.month
        + 12 * (end_date.year - start_date.year)  # noqa: 501
    )  # noqa: 501


def update_opportunity(self, docname, customer_name):
    frappe.db.set_value("Opportunity", docname, "status", "Quotation")
    frappe.db.set_value(
        "Opportunity", docname, "cnp_customer_code", customer_name
    )  # noqa: 501
    frappe.db.commit()


def validate_next_fee_date(self):
    current_date = self.transaction_date
    for item in self.items:
        if item.cnp_next_fee_date:
            if (getdate(current_date) > getdate(item.cnp_next_fee_date)) and (
                getdate(current_date) != getdate(item.cnp_next_fee_date)
            ):
                frappe.throw(
                    "Next Fee date should be after " + str(current_date)
                )  # noqa: 501


def validate_address(self):
    customer_address = self.customer_address
    if not customer_address:
        frappe.throw("Please select an address to proceed")

    doc = frappe.get_doc("Address", customer_address)

    if doc.gst_category != "Unregistered" and (
        not doc.address_title
        or not doc.address_type
        or not doc.address_line1
        or not doc.pincode
        # or not doc.gstin
        or not doc.city
        or not doc.state
    ):
        frappe.throw(
            "Address Lines, City, State, Pin code, GSTIN are mandatory "
            + "for address. Please set them and try again"
        )


def update_tranche_flow_history(self):
    if self.cnp_is_tranche_initiated:
        operations_master = frappe.get_doc(
            "ATSL Mandate List", self.cnp_cl_code
        )  # noqa: 501

        if len(operations_master.tranche_history) == 0:
            operations_master.append(
                "tranche_history",
                {
                    "quotation": self.amended_from,
                    "status": "Initial Quotation",
                },  # noqa: 501
            )
        else:
            operations_master.append(
                "tranche_history",
                {
                    "quotation": self.amended_from,
                    "status": f"Tranche {len(operations_master.tranche_history)}",  # noqa: 501
                },
            )

        # allow linking cancelled document
        operations_master.flags.ignore_links = True
        operations_master.save()
    self.cnp_is_tranche_initiated = 0
    self.cnp_cl_code = ""


@frappe.whitelist()
def offer_acceptance_notification(doctype, docname, acceptance_date):
    offer_acceptance_date = getdate(acceptance_date) + timedelta(days=2)
    url = get_url()
    file = get_signed_pdf(doctype, docname)
    my_attachments = []
    my_attachments.append(
        {
            "fname": "{docname}.pdf".format(
                docname=docname.replace(" ", "-").replace("/", "-")
            ),
            "fcontent": file,
        }
    )
    quot_doc = frappe.get_doc(doctype, docname)
    head_content = """<h2>Offer Acceptance Notification </h2>"""
    content = f"""<p> Invoice should be raised for quotation
                <a href="{url}/app/quotation/{quot_doc.name}">{quot_doc.name} </a> # noqa: 501
                on/before
                {offer_acceptance_date}
                </p>"""
    recipient = []
    Users = frappe.get_all("User")
    for user in Users:
        role = frappe.get_roles(user["name"])
        if "Accounts User" in role:
            eid = frappe.get_doc("User", user["name"])
            recipient.append(eid.email)
    frappe.sendmail(
        recipients=recipient,
        subject="Test offer Acceptance",
        attachments=my_attachments,
        message=head_content + content,
    )


def set_offer_letter_serial_no(self):
    doc_before_save = self.get_doc_before_save()
    if (
        doc_before_save
        and self.cnp_letter_from != doc_before_save.cnp_letter_from
        and self.cnp_offer_letter_serial_no
    ) or (not self.cnp_offer_letter_serial_no and self.cnp_letter_from):
        set_value_cnp_offer_letter_serial_no(self)


def set_value_cnp_offer_letter_serial_no(self):
    fiscal_year = frappe.get_doc(
        "Fiscal Year", frappe.defaults.get_user_default("fiscal_year")
    )
    year = fiscal_year.year.split("-")
    y1 = year[0]
    y2 = year[1]
    year_start_date = fiscal_year.year_start_date.strftime("%Y-%m-%d")
    year_end_date = fiscal_year.year_end_date.strftime("%Y-%m-%d")

    max_seq_no = 0
    if self.cnp_letter_from == "ATSL":
        last_seq_no = frappe.db.sql(
            """
        select
        max(cnp_offer_letter_sequence_atsl) as max_seq_no
        from
        `tabQuotation`
        where cnp_letter_from = 'ATSL'
        and creation >= %s
        and creation <= %s
        """,
            (year_start_date, year_end_date),
            as_dict=1,
        )
        if last_seq_no:
            if last_seq_no[0].max_seq_no is not None:
                max_seq_no = last_seq_no[0].max_seq_no
        self.cnp_offer_letter_sequence_atsl = max_seq_no + 1
        ss = str(self.cnp_offer_letter_sequence_atsl)

    if self.cnp_letter_from == "AXIS":
        last_seq_no = frappe.db.sql(
            """
        select
        max(cnp_offer_letter_sequence_axis) as max_seq_no
        from
        `tabQuotation`
        where cnp_letter_from = 'AXIS'
        and creation >= %s
        and creation <= %s """,
            (year_start_date, year_end_date),
            as_dict=1,
        )
        if last_seq_no:
            if last_seq_no[0].max_seq_no is not None:
                max_seq_no = last_seq_no[0].max_seq_no
        self.cnp_offer_letter_sequence_axis = max_seq_no + 1
        ss = str(self.cnp_offer_letter_sequence_axis)

    ss = ss.rjust(4, "0")
    self.cnp_offer_letter_serial_no = (
        self.cnp_letter_from + "/CO/" + str(y1) + "-" + str(y2) + "/" + ss
    )


def set_value_cnp_proforma_no_serial_no(self):
    fiscal_year = frappe.get_doc(
        "Fiscal Year", frappe.defaults.get_user_default("fiscal_year")
    )
    year_start_date = fiscal_year.year_start_date.strftime("%y")
    series_type = "AT/CO/" + year_start_date
    filters = [["cnp_proforma_no", "Like", "%" + series_type + "%"]]
    custom_naming_series = series_type + "/P/{:04d}".format(
        get_next_series_number("Quotation", "cnp_proforma_no", filters)
    )
    self.cnp_proforma_no = custom_naming_series
