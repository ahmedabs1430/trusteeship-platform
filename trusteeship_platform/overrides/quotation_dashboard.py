from frappe import _


def get_data(data=None):
    return {
        "fieldname": "quotation",
        "non_standard_fieldnames": {
            "Auto Repeat": "reference_document",
            "Sales Invoice": "cnp_from_quotation",
            "Operations Flow": "quotation",
        },
        "transactions": [
            {"label": _("Sales Invoice"), "items": ["Sales Invoice"]},
            {
                "label": _("Operations Flow"),
                "items": ["ATSL Mandate List"],
            },
        ],
    }
