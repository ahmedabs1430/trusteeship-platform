import frappe


def validate(self, method=None):
    validate_default_reg_address(self)


def validate_default_reg_address(doc):
    is_company_linked = any(
        link.link_doctype == "Company" for link in doc.links
    )  # isort:skip

    if not is_company_linked or doc.address_type != "Registered Office":
        doc.cnp_is_default_offer_letter_address = 0

    if doc.cnp_is_default_offer_letter_address == 1:
        linked_companies = [
            link.link_name
            for link in doc.links
            if link.link_doctype == "Company"  # isort:skip
        ]

        filters = [
            ["name", "!=", doc.name],
            ["address_type", "=", "Registered Office"],
            ["cnp_is_default_offer_letter_address", "=", 1],
            ["Dynamic Link", "link_doctype", "=", "Company"],
            ["Dynamic Link", "link_name", "in", ", ".join(linked_companies)],
        ]

        reg_add_list = frappe.get_all("Address", filters=filters)

        if reg_add_list:
            frappe.throw(
                "Registered Address for selected company already exists",
                title="Duplicate Entry",
            )
