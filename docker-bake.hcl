variable "FRAPPE_BRANCH" {
  default = "version-14"
}

variable "ERPNEXT_BRANCH" {
  default = "version-14"
}

variable "REGISTRY_NAME" {
  default = "registry.gitlab.com/prabalkrishna/trusteeship-platform"
}

variable "IMAGE_NAME" {
  default = "erp"
}

variable "VERSION" {
  default = "latest"
}

target "default" {
  dockerfile = "containers/Dockerfile"
  tags = ["${REGISTRY_NAME}/${IMAGE_NAME}:${VERSION}"]
  args = {
    "FRAPPE_BRANCH" = FRAPPE_BRANCH
    "ERPNEXT_BRANCH" = ERPNEXT_BRANCH
  }
}
